import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginMerchantRoutingModule } from './login-merchant-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../component-libs/form-builder-table/form-builder-table.module';
import { BsComponentModule } from '../layout/modules/bs-component/bs-component.module';
import { LoginMerchantComponent } from './login-merchant.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';


@NgModule({
  declarations: [LoginMerchantComponent],
  imports: [
    CommonModule,
    LoginMerchantRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatListModule,
  ]
})
export class LoginMerchantModule { }
