## HOW TO GO

cd to the directory first
ng g m {module_name} --routing=true && ng g c {module_name} --skip-tests=true -m={module_name}

ng g m slot-machine-module --routing=true && ng g c slot-machine-module --skip-tests=true -m=slot-machine-module
ng g m ws-test --routing=true && ng g c ws-test --skip-tests=true -m=ws-test
ng g m merchant-add-product --routing=true && ng g c merchant-add-product --skip-tests=true -m=merchant-add-product
