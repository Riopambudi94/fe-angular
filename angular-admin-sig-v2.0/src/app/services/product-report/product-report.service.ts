import { Injectable,Component, OnInit } from '@angular/core';

import { HttpClient, HttpHeaders, HttpParams, HttpEventType } from '@angular/common/http';
import { Observable , of} from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import {api_url, getTokenHeader} from '../../../environments/environment';
import { MasterService } from '../master.service';
//import { ConsoleReporter } from 'jasmine';

interface Testproduct {
  error: any,
  result: any
}

@Injectable({
  providedIn: 'root',
  
})

export class ProductReportService {
  courses$: Observable<Testproduct[]>;

  api_url: string = '';
  public httpReq: any = false;
  constructor(private http: HttpClient, public myService:MasterService) { 
    this.api_url = api_url();
  }
  
  public async detailTestproductReport(_id){
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'testproduct/report/' +_id;
      result  = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  public async detailTestproduct(_id){
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'testproduct/' +_id;
      result  = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  public async getTestproductReportLint(){
    let result;
    const customHeaders = getTokenHeader();
    try{
      let url = 'testproduct/all';
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  /* public async getTestproductLint(){
    let result;
    try{
      let url = 'testproduct/findAll';
      result = await this.myService.get(null,url);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  } */

  public async searchTestproductLint(params){
    let result;
    const customHeaders = getTokenHeader();
    try{
      let url = 'testproduct/all';
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  public async updateTestproduct(params){
    let url = 'testproduct/' +params.previous_id;
    const customHeaders = getTokenHeader();
    // console.log(params);
    let result = await this.myService.update(params, url, customHeaders);
    return  result;
  }

  public async updateTestproductReport(params){
      const customHeaders = getTokenHeader();
    try{
      let url = 'testproduct/report/' + params.process_id;
      // console.log(params);
      let result = await this.myService.update(params, url, customHeaders);
      return  result;
    } catch(error){
      throw new TypeError(error);
    }
  }

  public async delete(params){
    let result;
    const customHeaders = getTokenHeader();
    try{
      let url = 'testproduct/' +params.id;
      // console.log(params);
      result = await this.myService.delete(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  public async upload(file, obj){
    let myStatus = localStorage.getItem('isLoggedin');
    let myToken;
    if(myStatus){
      myToken = localStorage.getItem('tokenlogin');
    }
    const fd = new FormData();
    console.log('file detail : ', file.type);
    fd.append('process', file, file.name)
    let url: string = api_url()+'testproduct/upload';
    //console.log('form data : ', fd);
    this.httpReq = this.http.post(url,fd,{
      headers : new HttpHeaders({
        'Authorization': myToken,
      }),
      reportProgress: true,
      observe: 'events'
    }).subscribe(async(event) => {
      //console.log("event",event);
      let c = JSON.parse(JSON.stringify(event));

      if(c.body){
        console.log("it has body");
        obj.Testproduct=c.body.result.data;
        //console.log(obj.Testproduct);
        obj.process_id = c.body.result.proc_id;
        obj.prodOnUpload = true;
        obj.failed = c.body.result.failed.length;
        //console.log(c.body);
      }
      if(event.type == HttpEventType.UploadProgress){
        let prgval = Math.round( event.loaded / event.total * 100);
        obj.updateProgressBar(prgval);
        if(prgval==100){
          console.log(event);
        }
        console.log(obj.cancel);
        if(obj.cancel == true){
          this.httpReq.unsubscribe();
        }
        console.log('upload Progress: ' +prgval+"%");
      }

    },
    result =>{
      console.log("result",result);
      if(result.error)
        { obj.errorFile = result.error.error;
          obj.cancel    = true;
        }
    }
    );
    
  }
}
