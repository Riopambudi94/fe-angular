// import { environment } from './../../../environments/environtment.development_tobereward';
import { error } from 'protractor';
import { Injectable, Component, OnInit } from '@angular/core';
import { HttpClient, HttpEventType, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, ReplaySubject } from 'rxjs';
import { api_url, getTokenHeader , getTokenHeaderRetailer, environment} from '../../../environments/environment';

import { MasterService } from '../master.service';
import Swal from 'sweetalert2';

interface Orderhistory {
  error: any;
  result: any;
}

@Injectable({
  providedIn: 'root',
})



export class OrderhistoryService {
  courses$: Observable<Orderhistory[]>;

  api_url = '';

  public httpReq: any = false;

  constructor(private http: HttpClient, public myService: MasterService, ) {
    this.api_url = api_url();

  }

  getOrderhistory(params): Observable<Orderhistory[]> {
    // const uri = 'http://192.168.1.198/f3/v1/orderhistory/all';
    // console.log(uri);
    // return this.http.get<Orderhistory[]>(uri);
    const myToken = localStorage.getItem('tokenlogin');
    const myStatus = localStorage.getItem('isLoggedin');

    if (myStatus == 'true') {
      try {
        if (params) {

          let httpParams = new HttpParams();
          params.search = Object.assign({}, params.search);
          params.order_by = Object.assign({}, params.order_by);
          const arrayParams = Object.assign({}, params);
          httpParams = httpParams.append('request', JSON.stringify(arrayParams));

          const httpOpt = {
            headers: new HttpHeaders(
              {
                'Content-Type': 'application/json',
                'Authorization': myToken,
              }
            ),
            params: httpParams
          };

          const uri = this.api_url + 'order-invoice/report';
          console.log('URI order history', uri);
          return this.http.get<Orderhistory[]>(uri, httpOpt);
        }
      } catch (error) {
        throw new TypeError(error);
      }
    } else {
      localStorage.removeItem('isLoggedin');
      window.location.reload();
    }
  }

  public async getHtmlElement(url) {
    console.warn("masuk")
    const headers =
				{
          'Accept': 'text/html',
					'Access-Control-Allow-Origin': '*',
          'Content-Type': 'text/html',
          'Sec-Fetch-Mode': 'no-cors'
				}

		const uri = url;
		console.log('URI order history', uri);
		return await this.myService.get(null,uri, headers);
  }

  public async getAllreportSalesOrder(status:any = null){

    let result:any;

    const customHeaders = getTokenHeaderRetailer();
    try {
      let url:any;
      if( status == "pending"){
        url = 'order-invoice/all/report?request={"search":{"status":"PENDING"},"limit_per_page":50,"current_page":1,"order_by":{"request_date":-1}}';
      } else if( status == "cancel"){
        url = 'order-invoice/all/report?request={"search":{"status":"CANCEL"},"limit_per_page":50,"current_page":1,"order_by":{"request_date":-1}}';
      } else if( status == "process"){
        url = 'order-invoice/all/report?request={"search":{"status":"PROCESSED"},"limit_per_page":50,"current_page":1,"order_by":{"request_date":-1}}';
      } else {
        url = 'order-invoice/all/report?request={"search":{},"limit_per_page":50,"current_page":1,"order_by":{"request_date":-1}}';
      }

      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async getOrderDetail(order_id) {
    let result;
    const customHeaders = getTokenHeaderRetailer();
    try{
      const url = `order-invoice/detail?order_id=${order_id}`;
      result = await this.myService.get(null, url, customHeaders);
    }catch(error){
      throw new TypeError(error);
    }
    return result;
  }

  public async searchShippingReport(params){
    console.warn("params", params)
    let result;
    const customHeaders = getTokenHeaderRetailer();
    try{
      const url = 'delivery/report';
      result = await this.myService.searchResult(params, url, customHeaders);
    }catch(error){
      throw new TypeError(error);
    }
    return result;
  }

  public async getShippingDetail(order_id) {
    let result;
    const customHeaders = getTokenHeaderRetailer();
    try{
      const url = `delivery/report?request={"search":{"order_id":"${order_id}"}}`;
      result = await this.myService.get(null, url, customHeaders);
    }catch(error){
      throw new TypeError(error);
    }
    return result;
  }

  public async getShippingDetailByRef(reference_no) {
    let result;
    const customHeaders = getTokenHeaderRetailer();
    try{
      const url = `delivery/report?request={"search":{"reference_no":"${reference_no}"}}`;
      result = await this.myService.get(null, url, customHeaders);
    }catch(error){
      throw new TypeError(error);
    }
    return result;
  }

  public async getReportBasedOnPO(status:any = null){

    let result:any;

    const customHeaders = getTokenHeaderRetailer();
    try {
      let url:any;
      url = 'order-invoice/report/sales-order?request={"search":{"based_on":"po_no"},"limit_per_page":50,"current_page":1,"order_by":{"request_date":-1}}';

      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async searchReportBasedOnPO(params:any){
    // console.log(params)
    params.order_by = {request_date:-1}
    params.search.based_on = "po_no";
    let result:any;
    const customHeaders = getTokenHeaderRetailer();
    try{
      const url = 'order-invoice/report/sales-order';
      result = await this.myService.searchResult(params, url, customHeaders);
    }catch(error){
      throw new TypeError(error);
    }
    return result;
  }

  public async searchAllReportSalesOrder(params:any){
    // console.log(params)
    params.order_by = {request_date:-1}
    let result:any;
    const customHeaders = getTokenHeaderRetailer();
    try{
      const url = 'order-invoice/all/report';
      result = await this.myService.searchResult(params, url, customHeaders);
    }catch(error){
      throw new TypeError(error);
    }
    return result;
  }

  public async searchAllReportSalesOrderPackingList(params:any){
    params.order_by = {request_date:-1}
    params.search['additional_info.label_printed'] = 'ya'
    params.search['status'] = 'PROCESSED'
    let result:any;
    const customHeaders = getTokenHeaderRetailer();
    try{
      const url = 'order-invoice/all/report';
      result = await this.myService.searchResult(params, url, customHeaders);
    }catch(error){
      throw new TypeError(error);
    }
    return result;
  }

  public async getOrderhistoryLint() {
    let result;

    const customHeaders = getTokenHeader();
    try {
      const url = 'order-invoice/report?request={"search":{},"limit_per_page":"50","current_page":"1"}';
      result = await this.myService.get(null, url, customHeaders);
      // console.log("resultnya om", result.result.values);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }
/* langkah ke 2 bikin fungsi api */

  public async getSalesOrdeReport() {
    let result;
    

    // {"search":{"type":"evoucher"},"order_by":{},"limit_per_page":50,"current_page":1}

    const customHeaders = getTokenHeader();
    try {
      const url = 'sales-order/report?request={"search":{"status":"PAID"},"limit_per_page":"50","current_page":"1"}';
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;

  }

  public async processOrder(params:any){
    let result:any;

    const customHeaders = getTokenHeaderRetailer();

    try {
      const url = 'order-invoice/process';
      result = await this.myService.post(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async updateStatusOrder(params:any, status:any){
    let result:any;

    const customHeaders = getTokenHeaderRetailer();
    try {
      const url = 'order-invoice/process/'+status+'?method=single';
      result = await this.myService.post(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async updatDeliveryProcess(params:any){
    let result:any;

    const customHeaders = getTokenHeaderRetailer();
    try {
      const url = 'delivery/process?method=single';
      result = await this.myService.post(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async updateStatusOrderCompleted(params:any,obj){
    let result:any;

    const customHeaders = getTokenHeaderRetailer();
    try {
      const url = 'order-invoice/process/complete';
      result = await this.myService.post(params, url, customHeaders);
      console.warn("result??", result);
      if (result && result.status == "success") {
        obj.startUploading = false;
        obj.selectedFile = null;
        obj.cancel = false;

        Swal.fire(
          'Your file has been uploaded!',
          'success'
        )
      }
    } catch (error) {
      // throw new TypeError(error);
      obj.startUploading = false;
      obj.selectedFile = null;
      let errorMsg = (<Error>error).message;
      Swal.fire(
        'Failed!',
        errorMsg,
        'warning'
      )
    }
    return result;
  }

  public async updateDeliveredProcess(params:any){
    let result:any;

    const customHeaders = getTokenHeaderRetailer();
    try {
      const url = 'delivery/complete?method=single';
      result = await this.myService.post(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async processRedeliver(params:any){
    let result:any;

    const customHeaders = getTokenHeaderRetailer();
    try {
      const url = 'delivery/return?method=single';
      result = await this.myService.post(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async processOrderRedelivery(params:any){
    let result:any;

    const customHeaders = getTokenHeaderRetailer();
    try {
      const url = 'delivery/redelivery?method=single';
      result = await this.myService.post(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async cancelOrder(params:any){
    let result:any;

    const customHeaders = getTokenHeaderRetailer();

    try {
      const url = 'order-invoice/cancel';
      result = await this.myService.post(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async cancelShipping(params:any){
    let result:any;

    const customHeaders = getTokenHeaderRetailer();

    try {
      const url = 'delivery/pickup/cancel?method=single';
      result = await this.myService.post(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async processPackaging(params:any){
    let result:any;

    const customHeaders = getTokenHeaderRetailer();
    try {
      const url = 'delivery/packaging?method=single';
      result = await this.myService.post(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }


  
  public async getShippingReportint(params) {
    let result;
    

    // {"search":{"type":"evoucher"},"order_by":{},"limit_per_page":50,"current_page":1}

    const customHeaders = getTokenHeader();
    try {
      // const url = 'orderhistory/shipping-report?request={"search":{},"limit_per_page":50,"current_page":1}'
      let url = ''
      const url3 = 'orderhistory/shipping-report?request={"search":{"based_on": "products"},"limit_per_page":50,"current_page":1}'
      const url2 = 'orderhistory/shipping-report?request={"search":{"based_on": "order_id"},"limit_per_page":50,"current_page":1}'
      if (params == 'order_id'){
        url = url2
      } else {
        url = url3
      }

      result = await this.myService.get(null, url, customHeaders);
      // console.log("resultnya om", result.result.values);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;

  }
  public async searchShippingProductReportint(params){
    console.log(params)
    params.search.based_on = "products"
    let result;
    const customHeaders = getTokenHeader();
    try{
      const url = 'orderhistory/shipping-report';
      result = await this.myService.searchResult(params, url, customHeaders);
    }catch(error){
      throw new TypeError(error);
    }
    return result;
  }

  public async searchShippingidReportint(params){
    console.log(params)
    params.search.based_on = "order_id"
    let result;
    const customHeaders = getTokenHeader();
    try{
      const url = 'orderhistory/shipping-report';
      result = await this.myService.searchResult(params, url, customHeaders);
    }catch(error){
      throw new TypeError(error);
    }
    return result;
  }

  public async searchShippingReportint(params){
    console.log(params)
    let result;
    const customHeaders = getTokenHeader();
    try{
      const url = 'orderhistory/shipping-report';
      result = await this.myService.searchResult(params, url, customHeaders);
    }catch(error){
      throw new TypeError(error);
    }
    return result;
  }

  public async getSalesOrderMerchantInt(params) {
    let result;
    

    // {"search":{"type":"evoucher"},"order_by":{},"limit_per_page":50,"current_page":1}

    const customHeaders = getTokenHeader();
    try {
      // const url = 'orderhistory/shipping-report?request={"search":{},"limit_per_page":50,"current_page":1}'
      let url = 'sales-order/report?request={"search":{"merchant_username":"'+params+'"},"limit_per_page":50,"current_page":1}'
     

      result = await this.myService.get(null, url, customHeaders);
      // console.log("resultnya om", result.result.values);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;

  }
  public async searchMerchantSalesOrderReportint(params){
    
    let merchant = await localStorage.getItem('merchantsales');
    let result;
    const customHeaders = getTokenHeader();
    params.search.merchant_username = merchant;
    try{
      const url = 'sales-order/report';
      result = await this.myService.searchResult(params, url, customHeaders);
    }catch(error){
      throw new TypeError(error);
    }
    return result;
  } 
  public async getSalesOrderReportint(params) {
    let result;
    

    // {"search":{"type":"evoucher"},"order_by":{},"limit_per_page":50,"current_page":1}

    const customHeaders = getTokenHeader();
    try {
      let url = ''
      const url3 = 'sales-order/admin?request={"search":{"based_on": "products"},"limit_per_page":50,"current_page":1}'
      const url2 = 'sales-order/admin?request={"search":{"based_on": "invoice_id"},"limit_per_page":50,"current_page":1}'
      if (params == 'order_id'){
        url = url2
      } else {
        url = url3
      }

      result = await this.myService.get(null, url, customHeaders);
      // console.log("resultnya om", result.result.values);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;

  }

  public async searchSalesOrderReportint(params){
    console.log(params)
    params.search.based_on = "products"
    let result;
    const customHeaders = getTokenHeader();
    try{
      const url = 'sales-order/admin';
      result = await this.myService.searchResult(params, url, customHeaders);
    }catch(error){
      throw new TypeError(error);
    }
    return result;
  }

   public async searchSalesOrderReport2int(params){
    console.log(params)
    params.search.based_on = "invoice_id"
    let result;
    const customHeaders = getTokenHeader();
    try{
      const url = 'sales-order/admin';
      result = await this.myService.searchResult(params, url, customHeaders);
    }catch(error){
      throw new TypeError(error);
    }
    return result;
  }

  public async searchSalesOrderReport(params){
    console.log(params)
    let result;
    const customHeaders = getTokenHeader();
    try{
      const url = 'sales-order/report';
      result = await this.myService.searchResult(params, url, customHeaders);
    }catch(error){
      throw new TypeError(error);
    }
    return result;
  }

  public async searchOrderhistoryLint(params) {
    let result;
    try {
      // const url = 'order-invoice/report'
      // result = await this.getOrderhistory(params).toPromise();
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async detailOrderhistory(historyID) {
    const customHeaders = getTokenHeader();
    let result;
    try {
      const url = 'orderhistory/detail'
      result = await this.myService.post({order_id: historyID}, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async getDetailOrderhistory(params) {
    const customHeaders = getTokenHeader();
    let result;
    // console.log("hasil", booking_id)
    try {
      const url = 'orderhistory/booking_detail/' + params
      // console.log(url)
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async getOrderHistoryMerchant() {
    const customHeaders = getTokenHeader();
    let result;
    try {
      const url = 'orderhistory/merchant?request={%22search%22:{},%22current_page%22:1,%22order_by%22:{%22order_date%22:-1}}';
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async getOrderHistoryMerchantFilter(params) {
    console.log(params)
    const customHeaders = getTokenHeader();
    let result;
    try {
      const url = 'orderhistory/merchant';
      result = await this.myService.searchResult(params, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async updateOrderHistory(params) {
    const customHeaders = getTokenHeader();
    let result;
    try {
      const url = 'orderhistory/update_status/' + params.status;
      result = await this.myService.update(params, url, customHeaders);
      console.log(result, url);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async getMerchantOrderHistory() {
    const customHeaders = getTokenHeader();
    let params = {'status':'waiting for confirmation'}
    let result;
    try {
      const url = 'order-invoice/report';
      result = await this.myService.searchResult(params, url, customHeaders);
      // console.log('orderhistory/merchant', result);
    } catch (error) {
      throw new TypeError(error);
    }
    console.log(result);
    return result;
  }

  public async getOrderhistorysummaryLint() {
    const customHeaders = getTokenHeader();
    let result;
    try {
      const url = 'orderhistory/alls';
      result = await this.myService.get(null, url, customHeaders);
      console.log('orderhistory/alls', 'result');
    } catch (error) {
      throw new TypeError(error);
    }
    console.log(result);
    return result;
  }

  public async searchOrderhistorysummaryLint(params) {
    console.log(params);
    let result;
    const customHeaders = getTokenHeader();

    try {
      const url = 'orderhistory/alls';
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async detailOrderhistorysummary(orderhistory_id) {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url = 'orderhistory/' + orderhistory_id;
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async updateOrderHistorySummary(params) {
    let result;
    const customHeaders = getTokenHeader();

    try {
      const url = 'orderhistory/update/' + params._id;
      const result = await this.myService.update(params, url, customHeaders);
      console.log(result, url);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async getOrderhistoryallhistoryLint() {
    let result;

    const customHeaders = getTokenHeader();


    try {
      // const url = 'orderhistory/';
      const url = 'orderhistory/alls';
      result = await this.myService.get(null, url, customHeaders);
      // console.log('this all result', result.result[0].results);
      // result = result.result;
    } catch (error) {
      throw new TypeError(error);
    }
    console.log(result);
    return result;
  }

  public async getOrderHistorySummaryByDate(curDate) {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'order-invoice/report/summary';
      result  = await this.myService.get({request: JSON.stringify({date: curDate})}, url, customHeaders);

    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  public async getAmountOrderHistorySummaryByDate(curDate) {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'order-invoice/report/summary-amount';
      result  = await this.myService.get({request: JSON.stringify({date: curDate})}, url, customHeaders);

    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }


  public async searchOrderhistoryallhistoryLint(params) {

    let result;
    const customHeaders = getTokenHeader();
    try {
      const url = 'order-invoice/report';
      result = await this.myService.searchResult(params, url, customHeaders);
      console.log(result)
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async detailOrderhistoryallhistory(orderhistory_id) {
    let result;
    const customHeaders = getTokenHeader();

    try {
      const url = 'orderhistory/' + orderhistory_id;
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async updateOrderHistoryAllHistory(params) {
    let result;
    const customHeaders = getTokenHeader();
    console.log(params);
    try {
      const url = 'orderhistory/update/' + params._id;
      result = await this.myService.update(params, url, customHeaders);
      console.log(url, result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async updateOrderHistoryAllHistoryCancel(orderhistory_id,params) {
    let result;
    const customHeaders = getTokenHeader();
    console.log(params);
    try {
      const url = 'orderhistory/update/' + orderhistory_id;
      result = await this.myService.update(params, url, customHeaders);
      console.log(url, result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }
  // public async cancelOrder(params) {
  //   let result;
  //   const customHeaders = getTokenHeader();
  //   console.log();
  //   try {
  //     const url = 'orderhistory/cancel';
  //     result = await this.myService.update(params, url, customHeaders);
  //     console.log(url, result);
  //   } catch (error) {
  //     throw new TypeError(error);
  //   }
  //   return result;
  // }

  public async requesting(params){
    let result;
    const customHeaders = getTokenHeader();
    try{
      const url = 'orderhistory/courier_pickup';
      result = await this.myService.post(params, url, customHeaders);
      console.log(url, result);
    } catch(err){
      throw new TypeError(err)
    }
    return result;
  }

  public async updateOrderHistoryAllHistoryMerchant(params) {
    let result
    const customHeaders = getTokenHeader();
    console.log(params);
    try {
      const url = 'orderhistory/booking_detail/' + params.booking_id;
      result = await this.myService.update(params, url, customHeaders);
      console.log(url, result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async getOrderhistorysuccessLint() {
    let result;
    // console.log(result);
    const customHeaders = getTokenHeaderRetailer();

    try {
      const url = 'order-invoice/all/report?request={"search": {"status":"PROCESSED"},"limit_per_page":50,"current_page":1,"order_by":{"request_date":-1}}';
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    console.log(result);
    return result;
  }

  public async getOrderhistorycompleteLint() {
    let result;
    // console.log(result);
    const customHeaders = getTokenHeaderRetailer();

    try {
      const url = 'order-invoice/all/report?request={"search": {"status":"COMPLETED"},"limit_per_page":50,"current_page":1,"order_by":{"request_date":-1}}';
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    console.log(result);
    return result;
  }

  public async getOrderhistoryreturnLint() {
    let result;
    // console.log(result);
    const customHeaders = getTokenHeaderRetailer();

    try {
      const url = 'order-invoice/all/report?request={"search": {"last_shipping_info":{"in":["return","redelivery"]}},"limit_per_page":50,"current_page":1,"order_by":{"request_date":-1}}';
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    console.log(result);
    return result;
  }

  public async getCourier() {
    let result;
    // console.log(result);
    const customHeaders = getTokenHeader();

    try {
      const url = 'courier/list/all';
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async searchOrderhistorysuccessLint(params) {
    params.search.status="PROCESSED";
    params.order_by = {request_date:-1}
    // console.log(params);
    const customHeaders = getTokenHeaderRetailer();
    let result;
    try {
      const url = 'order-invoice/all/report';
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async searchOrderhistorycompleteLint(params) {
    params.search.status="COMPLETED";
    params.order_by = {request_date:-1}
    // console.log(params);
    const customHeaders = getTokenHeaderRetailer();
    let result;
    try {
      const url = 'order-invoice/all/report';
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async searchOrderhistoryreturnLint(params) {
    params.search.last_shipping_info = {
      in:["return","redelivery"]
    };
    params.order_by = {request_date:-1}
    // console.log(params);
    const customHeaders = getTokenHeaderRetailer();
    let result;
    try {
      const url = 'order-invoice/all/report';
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async detailOrderhistorysuccess(orderhistory_id) {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url = 'orderhistory/' + orderhistory_id;
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async updateOrderHistorySuccess(params) {
    let result;
    const customHeaders = getTokenHeader();
    console.log(params);
    try {
      const url = 'orderhistory/update/' + params._id;
      result = await this.myService.update(params, url, customHeaders);
      console.log(url, result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async getOrderhistorypendingLint() {
    let result;
    const customHeaders = getTokenHeader();
    
    console.log(result);
    try {
      const url = 'order-invoice/report?request={"search": {"status":"pending"},"limit_per_page":50,"current_page":1}';
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    console.log(result);
    return result;
  }

  public async searchOrderhistorypendingLint(params) {
    // console.log(params);
    params.search.status="pending";
    const customHeaders = getTokenHeader();
    let result;
    try {
      const url = 'order-invoice/report';
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async detailOrderhistorypending(orderhistory_id) {
    let result;
    const customHeaders = getTokenHeader();

    try {
      const url = 'orderhistory/' + orderhistory_id;
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async updateOrderHistoryPending(params) {
    let result;
    const customHeaders = getTokenHeader();

    console.log(params);
    try {
      const url = 'orderhistory/update/' + params._id;
      result = await this.myService.update(params, url, customHeaders);
      console.log(url, result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async getOrderhistorycancelLint() {
    let result;
    console.log(result);
    const customHeaders = getTokenHeaderRetailer();
    try {
      const url = 'order-invoice/all/report?request={"search": {"status":"CANCEL"},"limit_per_page":50,"current_page":1,"order_by":{"request_date":-1}}';
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    console.log(result);
    return result;
  }

  public async searchOrderhistorycancelLint(params) {
    params.search.status="CANCEL";
    params.order_by = {request_date:-1}
    //console.log(params);
    const customHeaders = getTokenHeaderRetailer();
    let result;

    try {
      const url = 'order-invoice/all/report';
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async searchAllreportSalesOrder(params) {
    //console.log(params);
    const customHeaders = getTokenHeaderRetailer();
    let result;

    try {
      const url = 'order-invoice/all/report';
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async detailOrderhistorycancel(orderhistory_id) {
    let result;
    const customHeaders = getTokenHeader();

    try {
      const url = 'orderhistory/' + orderhistory_id;
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async updateOrderHistoryCancel(params) {
    let result;
    const customHeaders = getTokenHeader();
    console.log('params', params)
    try {
      const url = 'orderhistory/' + params.id +'/order_status';
      result = await this.myService.update(params, url, customHeaders);

    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async getOrderhistoryfailedLint() {
    let result;
    const customHeaders = getTokenHeader();
    console.log(result);
    try {
      const url = 'order-invoice/report?request={"search": {"status":"failed"},"limit_per_page":50,"current_page":1}';
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    console.log(result);
    return result;
  }

  public async searchOrderhistoryfailedLint(params) {
    //console.log(params);
    params.search.status="failed";
    const customHeaders = getTokenHeader();
    let result;
    try {
      const url = 'order-invoice/report';
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async detailOrderhistoryfailed(orderhistory_id) {
    let result;
    try {
      const url = 'orderhistory/' + orderhistory_id;
      result = await this.myService.get(null, url);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async updateOrderHistoryFailed(params) {
    const customHeaders = getTokenHeader();
    let result;
    console.log(params);
    try {
      const url = 'orderhistory/update/' + params._id;
      result = await this.myService.update(params, url, customHeaders);
      console.log(url, result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async getOrderhistoryerrorLint() {
    let result;
    const customHeaders = getTokenHeader();
    console.log(result);
    try {
      const url = 'order-invoice/report?request={"search": {"status":"error"},"limit_per_page":50,"current_page":1}';
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    console.log(result);
    return result;
  }

  public async searchOrderhistoryerrorLint(params) {
    //console.log(params);
    params.search.status="error";
    const customHeaders = getTokenHeader();
    let result;
    try {
      const url = 'order-invoice/report';
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async detailOrderhistoryerror(orderhistory_id) {
    const customHeaders = getTokenHeader();
    let result;
    try {
      const url = 'orderhistory/' + orderhistory_id;
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async updateOrderHistoryError(params) {
    let result;
    console.log(params);
    const customHeaders = getTokenHeader();

    try {
      const url = 'orderhistory/update/' + params._id;
      result = await this.myService.update(params, url, customHeaders);
      console.log(url, result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  //baru

  public async getOrderhistorywaitingLint() {
    let result;
    const customHeaders = getTokenHeaderRetailer();
    console.log(result);
    try {
      const url = 'order-invoice/all/report?request={"search": {"status":"PENDING"},"limit_per_page":50,"current_page":1,"order_by":{"request_date":-1}}';
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    console.log(result);
    return result;
  }

  public async searchOrderhistorywaitingLint(params) {
    //console.log(params);
    params.search.status="PENDING";
    params.order_by = {request_date:-1}
    const customHeaders = getTokenHeaderRetailer();
    let result;
    try {
      const url = 'order-invoice/all/report';
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async setPrintedMark(payload) {
    const customHeaders = getTokenHeaderRetailer();
    let result;
    try {
      const url = 'delivery/mark_as_printed';
      result = await this.myService.update(payload, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async detailOrderhistorywaiting(orderhistory_id) {
    const customHeaders = getTokenHeader();
    let result;
    try {
      const url = 'orderhistory/' + orderhistory_id;
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async updateOrderHistoryWaiting(params) {
    let result;
    console.log(params);
    const customHeaders = getTokenHeader();

    try {
      const url = 'orderhistory/update/' + params._id;
      result = await this.myService.update(params, url, customHeaders);
      console.log(url, result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async getCRR(period, params={}){
    let result;
    const customHeaders = getTokenHeader();

    try {
      const url = 'order-history/report/crr/' + period
      result = await this.myService.get(params, url, customHeaders);
      console.log("DISINI BRO:", result)
    } catch (error){
      throw new TypeError(error);
    }
    return result;
  }

  public async getCNR(period, params={}){
    let result;
    const customHeaders = getTokenHeader();

    try{
      const url = 'order-history/report/cnr/' + period
      result = await this.myService.get(params, url, customHeaders);
      console.log("CNR", result);

    } catch(error){
      throw new TypeError(error);
    }
    return result;
  }

  public async holdMerchant(booking_id,param) {
    const customHeaders = getTokenHeader();
    let result;
    try {
      const url = 'orderhistory/hold/' + booking_id
      result = await this.myService.post(param, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }
  public async holdMerchantRefund(booking_id,param) {
    const customHeaders = getTokenHeader();
    let result;
    try {
      const url = 'orderhistory/refund/' + booking_id
      result = await this.myService.post(param, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }
  public async holdMerchantRelease(booking_id,param) {
    const customHeaders = getTokenHeader();
    let result;
    try {
      const url = 'orderhistory/release/' + booking_id
      result = await this.myService.post(param, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  //processed sales redemption
  public async getRedemptionReportint() {
    let result;
    const customHeaders = getTokenHeaderRetailer();
    try {
      let url = 'order-invoice/report/sales-order?request={"search":{"product_status":"process", "status":{"in":["PROCESSED","COMPLETED"]}},"limit_per_page":50,"current_page":1,"order_by":{"request_date":-1}}'
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async searchRedemptionReportint(params){
    params.order_by = {request_date:-1};
    params.search.product_status = "process";
    params.search.status = {
      in:["PROCESSED","COMPLETED"]
    };
    let result;
    const customHeaders = getTokenHeaderRetailer();
    try{
      const url = 'order-invoice/report/sales-order';
      result = await this.myService.searchResult(params, url, customHeaders);
    }catch(error){
      throw new TypeError(error);
    }
    return result;
  }

  //canceled sales redemption
  public async getRedemptionCanceledReportint() {
    let result;
    const customHeaders = getTokenHeaderRetailer();
    try {
      let url = 'order-invoice/report/sales-order?request={"search":{"product_status":"cancel", "status":{"in":["PROCESSED","COMPLETED"]}},"limit_per_page":50,"current_page":1,"order_by":{"request_date":-1}}'
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async searchRedemptionCanceledReportint(params){
    params.order_by = {request_date:-1};
    params.search.product_status = "cancel";
    params.search.status = {
      in:["PROCESSED","COMPLETED"]
    };
    let result;
    const customHeaders = getTokenHeaderRetailer();
    try{
      const url = 'order-invoice/report/sales-order';
      result = await this.myService.searchResult(params, url, customHeaders);
    }catch(error){
      throw new TypeError(error);
    }
    return result;
  }

  // SEARCH SALES REDEMPTION FROM EWALLET REPORT
  public async searchAllRedemptionPerOrder(params){
    let result;
    const customHeaders = getTokenHeaderRetailer();
    try{
      const url = `order-invoice/report/sales-order?request={"search":{"order_id":"${params}"},"current_page":1}`;
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result
  }

  //Sales Report Evoucher
  public async getEvoucherSalesint() {
    let result;
    const customHeaders = getTokenHeaderRetailer();
    try {
      let url = 'order-invoice/report/sales-order?request={"search":{"product_type":"evoucher"},"limit_per_page":50,"current_page":1,"order_by":{"request_date":-1}}'
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async searchEvoucherSalesint(params){
    params.search.product_type = "evoucher";
    params.order_by = {request_date:-1}
    // params.search.status = {
    //   in:["PROCESSED","COMPLETED"]
    // };
    let result;
    const customHeaders = getTokenHeaderRetailer();
    try{
      const url = 'order-invoice/report/sales-order';
      result = await this.myService.searchResult(params, url, customHeaders);
    }catch(error){
      throw new TypeError(error);
    }
    return result;
  }

  //Sales Report Ewallet
  public async getEwalletSalesReportint() {
    let result;
    const customHeaders = getTokenHeaderRetailer();
    try {
      let url = 'order-invoice/report/sales-order?request={"search":{"product_type":"topup"},"limit_per_page":50,"current_page":1,"order_by":{"request_date":-1}}'
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async searchEwalletSalesReportint(params){
    params.search.product_type = "topup";
    params.order_by = {request_date:-1}
    // params.search.status = {
    //   in:["PROCESSED","COMPLETED"]
    // };
    let result;
    const customHeaders = getTokenHeaderRetailer();
    try{
      const url = 'order-invoice/report/sales-order';
      result = await this.myService.searchResult(params, url, customHeaders);
    }catch(error){
      throw new TypeError(error);
    }
    return result;
  }

  //Sales Report Ewallet based on Receipt
  public async getEwalletReceiptReportint() {
    let result;
    const customHeaders = getTokenHeaderRetailer();
    try {
      let url = 'order-invoice/report/sales-order?request={"search":{"based_on":"receipt", "product_type":"topup"},"limit_per_page":50,"current_page":1,"order_by":{"request_date":-1}}'
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async searchEwalletReceiptReportint(params){
    params.search.based_on = "receipt";
    params.search.product_type = "topup";
    params.order_by = {request_date:-1}
    // params.search.status = {
    //   in:["PROCESSED","COMPLETED"]
    // };
    let result;
    const customHeaders = getTokenHeaderRetailer();
    try{
      const url = 'order-invoice/report/sales-order';
      result = await this.myService.searchResult(params, url, customHeaders);
    }catch(error){
      throw new TypeError(error);
    }
    return result;
  }

  // New BAST
  public async getBastReportint(params) {
    let result;
    
    const customHeaders = getTokenHeaderRetailer();
    try {
      // const url = 'orderhistory/shipping-report?request={"search":{},"limit_per_page":50,"current_page":1}'
      let url = 'order-invoice/all/report?request={"search":{"status":{"in":["PROCESSED","COMPLETED"]}},"limit_per_page":50,"current_page":1,"order_by":{"request_date":-1}}'
      

      result = await this.myService.get(null, url, customHeaders);
      // console.log("resultnya om", result.result.values);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;

  }
  public async searchBastReportint(params){
    // console.log(params)
    params.order_by = {request_date:-1}
    params.search.status = {
      in:["PROCESSED","COMPLETED"]
    };
    let result;
    const customHeaders = getTokenHeaderRetailer();
    try{
      const url = 'order-invoice/all/report';
      result = await this.myService.searchResult(params, url, customHeaders);
    }catch(error){
      throw new TypeError(error);
    }
    return result;
  }

  public async searchOrderHistory(params){
    let result;
    const customHeaders = getTokenHeaderRetailer();
    try{
      const url = 'delivery/report';
      result = await this.myService.searchResult(params, url, customHeaders);
    }catch(error){
      throw new TypeError(error);
    }
    return result;
  }

  public async uploadPackagingBulk(file, obj,payload, type){
    let myStatus = localStorage.getItem('isLoggedin');
    let myToken;
    if(myStatus){
      myToken = localStorage.getItem('tokenlogin');
    }

    const fd = new FormData();
    //fd.append('image', file, file.name)
    fd.append('type',payload.type)
    fd.append('document', file)
    let url: string = api_url()+'delivery/packaging?method=bulk';
    let result;
    console.warn("url", url)
    
    this.httpReq = this.http.post(url,fd,{
      headers: getTokenHeaderRetailer(),
      // headers : new HttpHeaders({
      //   'Authorization': myToken,
      //   'app-label': 'retailer_prg'
      // }),
      reportProgress: true,
      observe: 'events'
    }).subscribe(async(event) => {
      let c = JSON.parse(JSON.stringify(event));

      if(c.body){
          obj.Report=c.body._id;
          // obj.prodOnUpload = true;

          if (c.status == 200) {
            obj.selectedFile = null;
            obj.startUploading = false;
            Swal.fire(
              'Your file has been uploaded!',
              'success'
            )
          }
          // obj.prodOnUpload = true;
      }
      if(event.type == HttpEventType.UploadProgress){
        let prgval = Math.round( event.loaded / event.total * 100);
        obj.updateProgressBar(prgval);
        //console.log(obj.cancel);
        if(prgval==100){
          console.log(event);
          
        }
        if(obj.cancel == true){
          this.httpReq.unsubscribe();
        }
        // if(prgval == 100){
        //   if(obj.onUploadSuccess) 
        //     obj.onUploadSuccess();
        // }
        console.log('upload Progress: ' +prgval+"%");
      }
    },
    result =>{
      // console.warn("RESULT", result);
      if(result.error) {
          obj.errorFile = result.error.error;
          obj.cancel    = true;
          obj.selectedFile = null;
          Swal.fire(
            'Failed!',
            result.error.error,
            'warning'
          )
        }
    }
    );
  }

  public async uploadShippingProcessBulk(file, obj,payload, type){
    let myStatus = localStorage.getItem('isLoggedin');
    let myToken;
    if(myStatus){
      myToken = localStorage.getItem('tokenlogin');
    }

    const fd = new FormData();
    //fd.append('image', file, file.name)
    fd.append('type',payload.type)
    fd.append('document', file)
    let url: string = api_url()+'delivery/process?method=bulk';
    let result;
    console.warn("url", url)
    
    this.httpReq = this.http.post(url,fd,{
      headers: getTokenHeaderRetailer(),
      // headers : new HttpHeaders({
      //   'Authorization': myToken,
      //   'app-label': 'retailer_prg'
      // }),
      reportProgress: true,
      observe: 'events'
    }).subscribe(async(event) => {
      let c = JSON.parse(JSON.stringify(event));

      if(c.body){
          obj.Report=c.body._id;
          // obj.prodOnUpload = true;

          if (c.status == 200) {
            obj.selectedFile = null;
            obj.startUploading = false;
            Swal.fire(
              'Your file has been uploaded!',
              'success'
            )
          }
          // obj.prodOnUpload = true;
      }
      if(event.type == HttpEventType.UploadProgress){
        let prgval = Math.round( event.loaded / event.total * 100);
        obj.updateProgressBar(prgval);
        //console.log(obj.cancel);
        if(prgval==100){
          console.log(event);
          
        }
        if(obj.cancel == true){
          this.httpReq.unsubscribe();
        }
        // if(prgval == 100){
        //   if(obj.onUploadSuccess) 
        //     obj.onUploadSuccess();
        // }
        console.log('upload Progress: ' +prgval+"%");
      }
    },
    result =>{
      // console.warn("RESULT", result);
      if(result.error) {
          obj.errorFile = result.error.error;
          obj.cancel    = true;
          obj.selectedFile = null;
          Swal.fire(
            'Failed!',
            result.error.error,
            'warning'
          )
        }
    }
    );
  }

  public async uploadCancelPickupBulk(file, obj,payload, type){
    let myStatus = localStorage.getItem('isLoggedin');
    let myToken;
    if(myStatus){
      myToken = localStorage.getItem('tokenlogin');
    }

    const fd = new FormData();
    //fd.append('image', file, file.name)
    fd.append('type',payload.type)
    fd.append('document', file)
    let url: string = api_url()+'delivery/pickup/cancel?method=bulk';
    let result;
    console.warn("url", url)
    
    this.httpReq = this.http.post(url,fd,{
      headers: getTokenHeaderRetailer(),
      // headers : new HttpHeaders({
      //   'Authorization': myToken,
      //   'app-label': 'retailer_prg'
      // }),
      reportProgress: true,
      observe: 'events'
    }).subscribe(async(event) => {
      let c = JSON.parse(JSON.stringify(event));

      if(c.body){
          obj.Report=c.body._id;
          // obj.prodOnUpload = true;

          if (c.status == 200) {
            obj.selectedFile = null;
            obj.startUploading = false;
            Swal.fire(
              'Your file has been uploaded!',
              'success'
            )
          }
          // obj.prodOnUpload = true;
      }
      if(event.type == HttpEventType.UploadProgress){
        let prgval = Math.round( event.loaded / event.total * 100);
        obj.updateProgressBar(prgval);
        //console.log(obj.cancel);
        if(prgval==100){
          console.log(event);
          
        }
        if(obj.cancel == true){
          this.httpReq.unsubscribe();
        }
        // if(prgval == 100){
        //   if(obj.onUploadSuccess) 
        //     obj.onUploadSuccess();
        // }
        console.log('upload Progress: ' +prgval+"%");
      }
    },
    result =>{
      // console.warn("RESULT", result);
      if(result.error) {
          obj.errorFile = result.error.error;
          obj.cancel    = true;
          obj.selectedFile = null;
          Swal.fire(
            'Failed!',
            result.error.error,
            'warning'
          )
        }
    }
    );
  }

  public async uploadShippingCompleteBulk(file, obj,payload, type){
    let myStatus = localStorage.getItem('isLoggedin');
    let myToken;
    if(myStatus){
      myToken = localStorage.getItem('tokenlogin');
    }

    const fd = new FormData();
    //fd.append('image', file, file.name)
    fd.append('type',payload.type)
    fd.append('document', file)
    let url: string = api_url()+'delivery/complete?method=bulk';
    let result;
    
    this.httpReq = this.http.post(url,fd,{
      headers: getTokenHeaderRetailer(),
      // headers : new HttpHeaders({
      //   'Authorization': myToken,
      //   'app-label': 'retailer_prg'
      // }),
      reportProgress: true,
      observe: 'events'
    }).subscribe(async(event) => {
      let c = JSON.parse(JSON.stringify(event));

      if(c.body){
          obj.Report=c.body._id;
          // obj.prodOnUpload = true;

          if (c.status == 200) {
            obj.selectedFile = null;
            obj.startUploading = false;
            Swal.fire(
              'Your file has been uploaded!',
              'success'
            )
          }
          // obj.prodOnUpload = true;
      }
      if(event.type == HttpEventType.UploadProgress){
        let prgval = Math.round( event.loaded / event.total * 100);
        obj.updateProgressBar(prgval);
        //console.log(obj.cancel);
        if(prgval==100){
          console.log(event);
          
        }
        if(obj.cancel == true){
          this.httpReq.unsubscribe();
        }
        // if(prgval == 100){
        //   if(obj.onUploadSuccess) 
        //     obj.onUploadSuccess();
        // }
        console.log('upload Progress: ' +prgval+"%");
      }
    },
    result =>{
      // console.warn("RESULT", result);
      if(result.error) {
          obj.errorFile = result.error.error;
          obj.cancel    = true;
          obj.selectedFile = null;
          Swal.fire(
            'Failed!',
            result.error.error,
            'warning'
          )
        }
    }
    );
  }


  public async uploadShippingProcessCompleteBulk(payload){
    let result;
    const customHeaders = getTokenHeaderRetailer();
    try{
      let url = 'order-invoice/process/complete?method=bulk';
      result = await this.myService.add(payload, url, customHeaders);

      Swal.fire(
        'Your file has been uploaded!',
        'success'
      )
    } catch (error) {
      let errorMsg = (<Error>error).message;

      Swal.fire(
        'Failed!',
        errorMsg,
        'warning'
      )
      // throw new TypeError((<Error>error).message);
    }
    return  result;
  }

  // Finance Invoice Report
  public async getFinanceInvoiceReport() {
    let result;

    const customHeaders = getTokenHeaderRetailer();
    try {
      let url = 'order-invoice/report/sales-order?request={"search":{"product_status":"process", "based_on":"bast", "status":"COMPLETED"},"limit_per_page":50,"current_page":1,"order_by":{"additional_info.bast.created_date":-1}}'
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  // Finance Invoice Report Complete without BAST based on
  public async getCompleteFinanceInvoiceReport() {
    let result;

    const customHeaders = getTokenHeaderRetailer();
    try {
      let url = 'order-invoice/report/sales-order?request={"search":{"product_status":"process", "status":"COMPLETED"},"limit_per_page":50,"current_page":1,"order_by":{"additional_info.bast.created_date":-1}}'
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async searchFinanceInvoiceReport(params){
    params.search.based_on = "bast";
    params.search.product_status = "process";
    if (params.order_by) {
      params.order_by = {"additional_info.bast.created_date":-1}
    }
    // params.order_by.additonal_info.bast = {created_date:-1};
    params.search.status = "COMPLETED";
    console.warn("params order by", params);

    let result;
    const customHeaders = getTokenHeaderRetailer();
    try{
      const url = 'order-invoice/report/sales-order';
      result = await this.myService.searchResult(params, url, customHeaders);
    }catch(error){
      throw new TypeError(error);
    }
    return result;
  }

  public async searchCompleteFinanceInvoiceReport(params){
    if (params.order_by) {
      params.order_by = {"additional_info.bast.created_date":-1}
    }
    // params.order_by.additonal_info.bast = {created_date:-1};
    params.search.status = "COMPLETED";
    params.search.product_status = "process";
    console.warn("params order by", params);

    let result;
    const customHeaders = getTokenHeaderRetailer();
    try{
      const url = 'order-invoice/report/sales-order';
      result = await this.myService.searchResult(params, url, customHeaders);
    }catch(error){
      throw new TypeError(error);
    }
    return result;
  }

  public async getFinanceInvoiceReportByID(order_id) {
    let result;

    const customHeaders = getTokenHeaderRetailer();
    try {
      let url = 'order-invoice/report/sales-order?request={"search":{"order_id":"'+String(order_id)+'", "based_on":"bast", "status":"COMPLETED"},"limit_per_page":50,"current_page":1,"order_by":{"request_date":-1}}'
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async uploadAdditionalImage(file:any, obj:any, payload:any, orderID:any, fileValue:any){
    // let myStatus = localStorage.getItem('isLoggedin');
    // let myToken;
    // if(myStatus){
    //   myToken = localStorage.getItem('tokenlogin');
    // }
    // let fileName = file.name.
    // const extentionsName = 
    console.warn("payload", payload);
    const fd = new FormData();
    fd.append('type',payload.type);
    if(file){
      const programName = localStorage.getItem('programName');
      let extens = file.name.split('.');
      let newFileName = orderID+'-'+programName + '-' + fileValue+'-'+Date.now()+'.'+extens[extens.length - 1];
      if(environment.production == false) newFileName = 'DEV-'+newFileName;
      fd.append(fileValue, file, newFileName);
    }
    // else {
    //   fd.append(fileValue,"dummy.png");
    // }
    if(payload.option){
      fd.append('option',payload.option);
      if(payload.id) {
        fd.append('id', payload.id);
      } else if(payload.receipt_id) {
        fd.append('receipt_id', payload.receipt_id);
      }
    }
    let url: string = api_url()+'order-invoice/additional_info?order_id='+orderID;
    
    return new Promise((resolve,reject) => {
    this.httpReq = this.http.put(url,fd,{
      headers: getTokenHeaderRetailer(),
      reportProgress: true,
      observe: 'events'
    }).subscribe(async(event) => {
      let c = JSON.parse(JSON.stringify(event));
      if(c){
          if (c.status && c.status == 200) {
            Swal.fire(
              'Success!',
              payload.option && payload.option == 'delete' ? 'Hapus gambar berhasil' : 'Your file has been uploaded.',
              'success'
            )
            resolve(true);
          }         
      }
      // if(event.type == HttpEventType.UploadProgress){
      //   let prgval = Math.round( event.loaded / event.total * 100);
      //   if(prgval==100){
          
      //   }
      //   if(obj.cancel == true){
      //     this.httpReq.unsubscribe();
      //   }
      // }
    },
    error => {
      // console.warn("RESULT error", error);
      if(error.error) {
          obj.errorFile = error.error.error;
          obj.cancel    = true;
          Swal.fire(
            'Failed!',
            error.error.error,
            'warning'
          );
          resolve(false);
        }
    }
    );
  });
}

public async uploadInvoiceNumber(file, obj,payload,order_id){
  let myStatus = localStorage.getItem('isLoggedin');
  let myToken;
  if(myStatus){
    myToken = localStorage.getItem('tokenlogin');
  }

  const fd = new FormData();
  //fd.append('image', file, file.name)
  fd.append('type',payload.type)
  fd.append('invoice_no', file)
  let url: string = api_url()+`order-invoice/additional_info?order_id=${order_id}`;
  console.log('now',fd);
  let result;
  
  this.httpReq = this.http.put(url,fd,{
    headers: getTokenHeaderRetailer(),
    // headers : new HttpHeaders({
    //   'Authorization': myToken,
    //   'app-label': 'retailer_prg'
    // }),
    reportProgress: true,
    observe: 'events'
  }).subscribe(async(event) => {
    console.log("event",event);
    console.log(event);
    let c = JSON.parse(JSON.stringify(event));

    if(c.body){
      console.log('this is body');
        // obj.firstLoad();
        // console.warn("c body", c.body)
        // console.warn("obj", obj)
        obj.invoiceNumber = file;
        obj.firstLoad();
        // obj.prodOnUpload = true;

        if (c.body && c.body.status == "success") {
          Swal.fire({
            title: 'Success',
            text: 'Berhasil update No. invoice',
            icon: 'success',
            confirmButtonText: 'Ok',
          }).then((result) => {
            if(result.isConfirmed){
              obj.backToDetail();
            }
          });
        }
        //untuk mengecek data pertama kali dan resultnya data uploadan terbaru
        // obj.process_id = c.body;
        // console.log("process_id",obj.Report);
        obj.prodOnUpload = true;
        // console.log(c.body.failed);
        // obj.failed = c.body.failed.length;
       
    }
    if(event.type == HttpEventType.UploadProgress){
      let prgval = Math.round( event.loaded / event.total * 100);
      obj.updateProgressBar(prgval);
      //console.log(obj.cancel);
      if(prgval==100){
        console.log(event);
        
      }
      if(obj.cancel == true){
        this.httpReq.unsubscribe();
      }
      // if(prgval == 100){
      //   if(obj.onUploadSuccess) 
      //     obj.onUploadSuccess();
      // }
      console.log('upload Progress: ' +prgval+"%");
    }
  },
  result =>{
    // console.warn("RESULT", result);
    if(result.error) {
        obj.errorFile = result.error.error;
        obj.invoiceNumber = "";
        obj.cancel    = true;
        Swal.fire(
          'Failed!',
          result.error.error,
          'warning'
        )
      }
  }
  );
}

public async updateBulkInvoice(file, obj,payload, option){
  let myStatus = localStorage.getItem('isLoggedin');
  let myToken;
  if(myStatus){
    myToken = localStorage.getItem('tokenlogin');
  }

  const fd = new FormData();
  //fd.append('image', file, file.name)
  fd.append('type',payload.type)
  fd.append('document', file)
  let url: string = api_url()+`order-invoice/invoice_no?option=${option}`;
  let result;
  
  this.httpReq = this.http.put(url,fd,{
    headers: getTokenHeaderRetailer(),
    reportProgress: true,
    observe: 'events'
  }).subscribe(async(event) => {
    console.log("event",event);
    console.log(event);
    let c = JSON.parse(JSON.stringify(event));

    if(c.body){
      obj.Report=c.body._id;
      // obj.prodOnUpload = true;

      if (c.status == 200) {
        obj.selectedFile = null;
        obj.startUploading = false;
        Swal.fire(
          'Your file has been uploaded!',
          'success'
        )
      }
      // obj.prodOnUpload = true;
  }
  if(event.type == HttpEventType.UploadProgress){
    let prgval = Math.round( event.loaded / event.total * 100);
    obj.updateProgressBar(prgval);
    //console.log(obj.cancel);
    if(prgval==100){
      console.log(event);
      
    }
    if(obj.cancel == true){
      this.httpReq.unsubscribe();
    }
    // if(prgval == 100){
    //   if(obj.onUploadSuccess) 
    //     obj.onUploadSuccess();
    // }
    console.log('upload Progress: ' +prgval+"%");
  }
  },
  result =>{
    //console.log(result);
    if(result.error)
      { obj.errorFile = result.error.error;
        obj.cancel    = true;
        obj.selectedFile = null;
        Swal.fire(
          'Failed!',
          result.error.error,
          'warning'
        )
      }
  }
  );
}

public async orderBulk(file, obj,payload){
  let myStatus = localStorage.getItem('isLoggedin');
  let myToken;
  if(myStatus){
    myToken = localStorage.getItem('tokenlogin');
  }

  const fd = new FormData();
  //fd.append('image', file, file.name)
  fd.append('type',payload.type)
  fd.append('document', file)
  let url: string = api_url()+`order-invoice/order/bulk`;
  let result;
  
  this.httpReq = this.http.post(url,fd,{
    headers: getTokenHeaderRetailer(),
    reportProgress: true,
    observe: 'events'
  }).subscribe(async(event) => {
    console.log("event",event);
    console.log(event);
    let c = JSON.parse(JSON.stringify(event));

    if(c.body){
      obj.Report=c.body._id;
      // obj.prodOnUpload = true;

      if (c.status == 200) {
        obj.selectedFile = null;
        obj.startUploading = false;
        Swal.fire(
          'Your file has been uploaded!',
          'success'
        )
      }
      // obj.prodOnUpload = true;
  }
  if(event.type == HttpEventType.UploadProgress){
    let prgval = Math.round( event.loaded / event.total * 100);
    obj.updateProgressBar(prgval);
    //console.log(obj.cancel);
    if(prgval==100){
      console.log(event);
      
    }
    if(obj.cancel == true){
      this.httpReq.unsubscribe();
    }
    // if(prgval == 100){
    //   if(obj.onUploadSuccess) 
    //     obj.onUploadSuccess();
    // }
    console.log('upload Progress: ' +prgval+"%");
  }
  },
  result =>{
    //console.log(result);
    if(result.error)
      { obj.errorFile = result.error.error;
        obj.cancel    = true;
        obj.selectedFile = null;
        Swal.fire(
          'Failed!',
          result.error.error,
          'warning'
        )
      }
    });
  }

  // PACKING LIST //
  public async getPackingListReport(param) {
    let result:any;
    const customHeaders = getTokenHeaderRetailer();
    try{
      // const url = 'packing_list/find';
      const url = 'packing_list/find?request={"search":{"status":"' + param + '"}}';
      result = await this.myService.get(null, url, customHeaders);
    }catch(error){
      throw new TypeError(error);
    }
    return result;
  }

  public async searchPackingList(params) {
    let result:any;
    params.search.status = "ACTIVE";
    const customHeaders = getTokenHeaderRetailer();
    try{
      const url = 'packing_list/find';
      result = await this.myService.searchResult(params, url, customHeaders);
    }catch(error){
      throw new TypeError(error);
    }
    return result;
  }

  public async searchPackingList2(params) {
    let result:any;
    params.search.status = "PROCESSED";
    const customHeaders = getTokenHeaderRetailer();
    try{
      const url = 'packing_list/find';
      result = await this.myService.searchResult(params, url, customHeaders);
    }catch(error){
      throw new TypeError(error);
    }
    return result;
  }

  public async createPackingList(data:any) {
    let result:any;
    const customHeaders = getTokenHeaderRetailer();
    try{
      const url = 'packing_list/create';
      result = await this.myService.post(data, url, customHeaders);
    }catch(error){
      throw new TypeError(error);
    }
    return result;
  }

  public async removePackingList(data:any) {
    let result:any;
    const customHeaders = getTokenHeaderRetailer();
    try{
      const url = 'packing_list/remove';
      result = await this.myService.post(data, url, customHeaders);
    }catch(error){
      throw new TypeError(error);
    }
    return result;
  }

  public async processPackingList(data:any) {
    let result:any;
    const customHeaders = getTokenHeaderRetailer();
    try{
      const url = 'packing_list/process/delivery';
      result = await this.myService.post(data, url, customHeaders);
    }catch(error){
      throw new TypeError(error);
    }
    return result;
  }

  public async searchPackingListDetailReport(params) {
    let result:any;
    console.warn("params", params)
    const customHeaders = getTokenHeaderRetailer();
    try{
      const url = `packing_list/find?request={"search":{"packing_no":"${params}"},"current_page":1}`;
      result = await this.myService.get(null, url, customHeaders);
    }catch(error){
      throw new TypeError(error);
    }
    return result;
  }

  public async cancelDigitalProductOrder(data:any) {
    let result:any;
    const customHeaders = getTokenHeaderRetailer();
    try{
      const url = 'order-invoice/cancel/products';
      result = await this.myService.post(data, url, customHeaders);
    }catch(error){
      throw new TypeError(error);
    }
    return result;
  }


}
