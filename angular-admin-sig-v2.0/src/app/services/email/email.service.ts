import { Injectable, Component, OnInit } from '@angular/core';
import { MasterService } from '../master.service';
import {getTokenHeader} from '../../../environments/environment';

interface Email {
  error: any ,
  result: any
}

@Injectable({
  providedIn: 'root',

})



export class EmailService {

  public gerro:  string = 'test';
  constructor(public myService:MasterService) { 
  }

  public async getEmailLint() {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'emails/all';
      result  = await this.myService.get(null, url, customHeaders);
      
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  public async addNewEmail(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'emails/';
      result  = await this.myService.add(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  public async updateEmail(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'emails/all';
      result  = await this.myService.update(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      return  result;
  }

  public async detailEmail(_id) {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'emails/' + _id;
     
      result  = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  public async deleteEmail(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'emails/all';
      result  = await this.myService.delete(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      return  result;
  }

  public async searchEmailLint(params) {
    // console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'emails/all';
      result  = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      return  result;
  }
  
  public async updateEmailID(params) {
    let result: any;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'emails/update/' +params.email_id;
      result  = await this.myService.update(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      // console.log(result);
    return  result;
  }

  public async delete(params) {
    let url, result: any;
    const customHeaders = getTokenHeader();
    try {
    url = 'emails/remove/' + params.email_id;
    // console.log(params);
    result = await this.myService.delete(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }
}
