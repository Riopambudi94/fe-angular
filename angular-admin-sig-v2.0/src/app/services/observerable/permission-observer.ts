import { Injectable, Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { api_url, getTokenHeader } from '../../../environments/environment';

import { MasterService } from '../master.service';

interface Orderhistory {
  error: any;
  result: any;
}

@Injectable({
  providedIn: 'root',
})



export class PermissionObserver {
  public currentPermission = new BehaviorSubject<any>({});
  public castCurrentPermission = this.currentPermission.asObservable();

  api_url = '';

  constructor() {
    

  }

  updateCurrentPermission(data){
    this.currentPermission.next(data);
  }

}
