import { Injectable,Component, OnInit } from '@angular/core';
import { catchError, map, tap } from 'rxjs/operators';

import {api_url, getTokenHeaderRetailer, getTokenHeader} from '../../../environments/environment';
import { MasterService } from '../master.service';

interface Category {
  error:any,
  result:any
}

@Injectable({
  providedIn: 'root',
  
})

export class CategoryService {

  api_url:string ="";
  constructor(public myService:MasterService) { 
    this.api_url = api_url();
  }

  public async detailCategory(_id){
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'category/detail/'+_id;
      result  = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  public async getCategoryLint(){
    let result;
    const customHeaders = getTokenHeaderRetailer();;
    try{
      let url = 'category/report';
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  public async searchCategoryLint(params){
    let result;
    const customHeaders = getTokenHeaderRetailer();;
    try{
      let url = 'category/report';
      result = await this.myService.searchResult(params, url, customHeaders);
      //result = await this.getCategory(params).toPromise();
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  public async getCategoryByID(_id) {
    console.log("_id", _id);
    let result;
    const customHeaders = getTokenHeaderRetailer();;
    try {
      const url = 'category/report?request={"search": {"_id":"'+String(_id)+'"}}';
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async addNewCategory(params){
    let result;
    const customHeaders = getTokenHeaderRetailer();;
    try{
      let url = 'category';
      result = await this.myService.add(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }


  public async getCategoryList(params?){
    let result;
    const customHeaders = getTokenHeader();
    try{
      let url = 'category/list';
      result = await this.myService.get(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  public async updateCategoryID(params){

    let url = 'category/'+ params.code;
    const customHeaders = getTokenHeaderRetailer();;
    try{
      let result = await this.myService.update(params, url, customHeaders);
      return  result;
    }
    catch (error){
      throw new TypeError(error);
    }
  }

  public async deleteCategory(params){
    let result;
    const customHeaders = getTokenHeaderRetailer();;
    try{
      let url = 'category/'+params.code;
      // console.log(params);
      result = await this.myService.delete(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }
}
