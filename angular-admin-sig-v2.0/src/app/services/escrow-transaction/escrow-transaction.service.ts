import { Injectable, Component, OnInit } from '@angular/core';

import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { api_url, getTokenHeader } from '../../../environments/environment';
import { MasterService } from '../master.service';

interface EscrowTransaction {
  error: any,
  result: any,
}

@Injectable({
  providedIn: 'root',
})



export class EscrowTransactionService {
  courses$: Observable<EscrowTransaction[]>;
  api_url: string = "";
  constructor(private http: HttpClient, public myService: MasterService) {
    this.api_url = api_url();
  }

  getEscrowtransaction(params): Observable<EscrowTransaction[]> {
    // const uri = 'http://192.168.1.198/f3/v1/orderhistory/all';
    // console.log(uri);
    // return this.http.get<Escrowtransaction[]>(uri);
    let myToken = localStorage.getItem('tokenlogin');
    let myStatus = localStorage.getItem('isLoggedin');

    if (myStatus == 'true') {
      try {
        if (params) {

          let httpParams = new HttpParams();
          params.search = Object.assign({}, params.search);
          params.order_by = Object.assign({}, params.order_by);
          let arrayParams = Object.assign({}, params);
          httpParams = httpParams.append('request', JSON.stringify(arrayParams));

          const httpOpt = {
            headers: new HttpHeaders(
              {
                'Content-Type': 'application/json',
                'Authorization': myToken,
              }
            ),
            params: httpParams
          };

          const uri = this.api_url + 'escrows/report';
          console.log("URI points transaction history", uri)
          return this.http.get<EscrowTransaction[]>(uri, httpOpt);
        }
      } catch (error) {
        throw new TypeError(error);
      }
    } else {
      localStorage.removeItem('isLoggedin');
      window.location.reload();
    }
  }
  public async getEscrowtransactionLint() {
    let result;

    const customHeaders = getTokenHeader();


    try {
      const url = 'escrows/report?request={"search":{},"limit_per_page":50,"current_page":1}';
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    console.log(result);
    return result;
  }
  public async getEscrowtransactionByMerchantLint(params) {
    let result;

    const customHeaders = getTokenHeader();


    try {
      const url = 'escrows/report?request={"search":{"merchant_username":"'+params+'"},"limit_per_page":50,"current_page":1}';
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    console.log(result);
    return result;
  }
  public async searchEscrowtransactionByMerchantLint(params) {
    let result;
    let merchant = await localStorage.getItem('merchantescrow');
    const customHeaders = getTokenHeader();
    let x : number = +params.limit_per_page
    params.limit_per_page = x
    params.search.merchant_username = merchant
    try {
      const url = 'escrows/report';
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }
  public async getBalanceAllMerchant() {
    let result;

    const customHeaders = getTokenHeader();


    try {
      const url = 'escrows/get-all-merchants-balances?request={"search":{},"limit_per_page":50,"current_page":1}';
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    console.log(result);
    return result;
  }

  public async searchEscrowtransactionLint(params) {
    let result;
    const customHeaders = getTokenHeader();
    let x : number = +params.limit_per_page
    params.limit_per_page = x
    try {
      const url = 'escrows/report';
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }
  public async getEscrowReporttransactionLint() {
    let result;

    const customHeaders = getTokenHeader();


    try {
      const url = 'escrows-admin/report?request={"search":{},"limit_per_page":50,"current_page":1}';
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    console.log(result);
    return result;
  }

  public async searchEscrowReporttransactionLint(params) {
    let result;
    const customHeaders = getTokenHeader();
    let x : number = +params.limit_per_page
    params.limit_per_page = x
    try {
      const url = 'escrows-admin/report';
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async escrowWithdrawalRequest(params: any) {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url = 'escrows/withdrawal';
      return await this.myService.post(params, url, customHeaders)
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async escrowRefundRequest ( params : any ){
    let result;
    const customHeaders = getTokenHeader();

    try {
      const url = 'escrows/refund';
      result = await this.myService.post(params,url,customHeaders)
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }
  public async escrowWithdrawalReleaseRequest(pointstransaction_id ,params : any){
    let result;
    const customHeaders = getTokenHeader();

    try {
      const url = 'escrows/withdrawal/release/' + pointstransaction_id;
      result= await this.myService.post(params,url,customHeaders)
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async escrowWithdrawalRejectRequest(pointstransaction_id ,params : any ){
    let result;
    const customHeaders = getTokenHeader();

    try {
      const url = 'escrows/withdrawal/reject/' + pointstransaction_id;
      result = await this.myService.post(params,url,customHeaders)
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async orderHistoryHoldRequest(pointstransaction_id, params){
    let result;
    const customHeaders = getTokenHeader();
    try { 
      const url = 'orderHistory/hold/' + pointstransaction_id;
      result = await this.myService.post(params,url,customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async orderHistoryRefundRequest(pointstransaction_id, params){
    let result;
    const customHeaders = getTokenHeader();
    try { 
      const url = 'orderHistory/refund/' + pointstransaction_id;
      result = await this.myService.post(params,url,customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async orderHistoryReleaseRequest(pointstransaction_id, params){
    let result;
    const customHeaders = getTokenHeader();
    try { 
      const url = 'orderHistory/release/' + pointstransaction_id;
      result = await this.myService.post(params,url,customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async detailEscrowtransaction(pointstransaction_id) {
    let result;
    const customHeaders = getTokenHeader();

    try {
      const url = 'escrows/' + pointstransaction_id;
      // const url = 'escrows/report?request={"search": {"_id":"'+pointstransaction_id+'"}}';
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async getEscrowBalance() {
    let result;
    const customHeaders = getTokenHeader();

    try {
      const url = 'escrows/get-balance/';
      // const url = 'escrows/report?request={"search": {"_id":"'+pointstransaction_id+'"}}';
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async updateEscrowtransaction(params) {
    let result;
    const customHeaders = getTokenHeader();
    console.log(params);
    try {
      const url = 'escrows/update/' + params._id;
      result = await this.myService.update(params, url, customHeaders);
      console.log(url, result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async additionalTransaction(pointstransaction_id, params){
    let result;
    const customHeaders = getTokenHeader();
    try { 
      const url = 'escrows/new/' + pointstransaction_id;
      result = await this.myService.post(params,url,customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

}
