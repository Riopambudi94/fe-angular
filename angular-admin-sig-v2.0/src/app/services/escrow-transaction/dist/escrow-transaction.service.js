"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.EscrowTransactionService = void 0;
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var environment_1 = require("../../../environments/environment");
var EscrowTransactionService = /** @class */ (function () {
    function EscrowTransactionService(http, myService) {
        this.http = http;
        this.myService = myService;
        this.api_url = "";
        this.api_url = environment_1.api_url();
    }
    EscrowTransactionService.prototype.getEscrowtransaction = function (params) {
        // const uri = 'http://192.168.1.198/f3/v1/orderhistory/all';
        // console.log(uri);
        // return this.http.get<Escrowtransaction[]>(uri);
        var myToken = localStorage.getItem('tokenlogin');
        var myStatus = localStorage.getItem('isLoggedin');
        if (myStatus == 'true') {
            try {
                if (params) {
                    var httpParams = new http_1.HttpParams();
                    params.search = Object.assign({}, params.search);
                    params.order_by = Object.assign({}, params.order_by);
                    var arrayParams = Object.assign({}, params);
                    httpParams = httpParams.append('request', JSON.stringify(arrayParams));
                    var httpOpt = {
                        headers: new http_1.HttpHeaders({
                            'Content-Type': 'application/json',
                            'Authorization': myToken
                        }),
                        params: httpParams
                    };
                    var uri = this.api_url + 'escrows/report';
                    console.log("URI points transaction history", uri);
                    return this.http.get(uri, httpOpt);
                }
            }
            catch (error) {
                throw new TypeError(error);
            }
        }
        else {
            localStorage.removeItem('isLoggedin');
            window.location.reload();
        }
    };
    EscrowTransactionService.prototype.getEscrowtransactionLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'escrows/report?request={"search":{},"limit_per_page":50,"current_page":1}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_1 = _a.sent();
                        throw new TypeError(error_1);
                    case 4:
                        console.log(result);
                        return [2 /*return*/, result];
                }
            });
        });
    };
    EscrowTransactionService.prototype.searchEscrowtransactionLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, x, url, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        x = +params.limit_per_page;
                        params.limit_per_page = x;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'escrows/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_2 = _a.sent();
                        throw new TypeError(error_2);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EscrowTransactionService.prototype.escrowWithdrawalRequest = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'escrows/withdrawal';
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2: return [2 /*return*/, _a.sent()];
                    case 3:
                        error_3 = _a.sent();
                        throw new TypeError(error_3);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EscrowTransactionService.prototype.escrowRefundRequest = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'escrows/refund';
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_4 = _a.sent();
                        throw new TypeError(error_4);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EscrowTransactionService.prototype.escrowWithdrawalReleaseRequest = function (pointstransaction_id, params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'escrows/withdrawal/release/' + pointstransaction_id;
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_5 = _a.sent();
                        throw new TypeError(error_5);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EscrowTransactionService.prototype.escrowWithdrawalRejectRequest = function (pointstransaction_id, params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'escrows/withdrawal/reject/' + pointstransaction_id;
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_6 = _a.sent();
                        throw new TypeError(error_6);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EscrowTransactionService.prototype.orderHistoryHoldRequest = function (pointstransaction_id, params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_7;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderHistory/hold/' + pointstransaction_id;
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_7 = _a.sent();
                        throw new TypeError(error_7);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EscrowTransactionService.prototype.orderHistoryRefundRequest = function (pointstransaction_id, params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_8;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderHistory/refund/' + pointstransaction_id;
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_8 = _a.sent();
                        throw new TypeError(error_8);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EscrowTransactionService.prototype.orderHistoryReleaseRequest = function (pointstransaction_id, params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_9;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'orderHistory/release/' + pointstransaction_id;
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_9 = _a.sent();
                        throw new TypeError(error_9);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EscrowTransactionService.prototype.detailEscrowtransaction = function (pointstransaction_id) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_10;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'escrows/' + pointstransaction_id;
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        // const url = 'escrows/report?request={"search": {"_id":"'+pointstransaction_id+'"}}';
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_10 = _a.sent();
                        throw new TypeError(error_10);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EscrowTransactionService.prototype.getEscrowBalance = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_11;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'escrows/get-balance/';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        // const url = 'escrows/report?request={"search": {"_id":"'+pointstransaction_id+'"}}';
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_11 = _a.sent();
                        throw new TypeError(error_11);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EscrowTransactionService.prototype.updateEscrowtransaction = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_12;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        console.log(params);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'escrows/update/' + params._id;
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(url, result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_12 = _a.sent();
                        throw new TypeError(error_12);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EscrowTransactionService.prototype.additionalTransaction = function (pointstransaction_id, params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_13;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = environment_1.getTokenHeader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'escrows/new/' + pointstransaction_id;
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_13 = _a.sent();
                        throw new TypeError(error_13);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EscrowTransactionService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], EscrowTransactionService);
    return EscrowTransactionService;
}());
exports.EscrowTransactionService = EscrowTransactionService;
