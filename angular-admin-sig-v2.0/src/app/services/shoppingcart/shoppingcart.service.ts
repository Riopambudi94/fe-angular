import { Injectable, Component, OnInit } from '@angular/core';
import { MasterService } from '../master.service';
import {getTokenHeader} from '../../../environments/environment';

interface Shoppingcart {
  error: any;
  result: any;
}

@Injectable({
  providedIn: 'root',

})


export class ShoppingcartService {

  public gerro = 'test';
  constructor(public myService: MasterService) {
  }


  public async getShoppingcartLint() {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'shoppingcart/all';
      result  = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  public async searchShoppingcartLint(params) {
    //console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'shoppingcart/all';
      result  = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return  result;
  }

  public async detailShoppingcart(status) {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'shoppingcart/' + status;
      result  = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    //console.log(result);
    return  result;
  }

}
