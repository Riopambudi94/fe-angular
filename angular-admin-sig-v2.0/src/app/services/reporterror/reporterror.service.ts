import { Injectable, Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { api_url, getTokenHeader } from '../../../environments/environment';
import { MasterService } from '../master.service';
import { async } from 'q';

interface Reporterror{
  error: any;
  result: any;
}

@Injectable({
  providedIn: 'root'
})



export class ReporterrorService {
  courses$: Observable<Reporterror[]>;

  api_url = '';

  constructor(private http: HttpClient, public myService: MasterService) {
    this.api_url = api_url();

   }

   getReporterror(params): Observable<Reporterror[]>{
     

    const myToken = localStorage.getItem('tokenlogin');
    const myStatus = localStorage.getItem('isLoggedin');

    if (myStatus == 'true') {
      try {
        if (params) {

          let httpParams = new HttpParams();
          params.search = Object.assign({}, params.search);
          params.order_by = Object.assign({}, params.order_by);
          const arrayParams = Object.assign({}, params);
          httpParams = httpParams.append('request', JSON.stringify(arrayParams));

          const httpOpt = {
            headers: new HttpHeaders(
              {
                'Content-Type': 'application/json',
                'Authorization': myToken,
              }
            ),
            params: httpParams
          };

          const uri = this.api_url + 'admin/report/logs/error/latest';
          console.log('URI order history', uri);
          return this.http.get<Reporterror[]>(uri, httpOpt);
        }
      } catch (error) {
        throw new TypeError(error);
      }
    } else {
      localStorage.removeItem('isLoggedin');
      window.location.reload();
    }

   }

   public async getReporterrorLint() {
    let result;

    const customHeaders = getTokenHeader();
    try {
      const url = 'admin/report/logs/error/latest';
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async searchReporterrorLint(params) {
    let result;
    try {
      console.log('params', params);
      // result = await this.getOrderhistory(params).toPromise();
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async detailReporterror(status) {
    const customHeaders = getTokenHeader();
    let result;
    try {
      const url = 'admin/logs/error/' + status;
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async updateReporterror(params) {
    let result;
    const customHeaders = getTokenHeader();
    console.log(params);
    try {
      const url = 'admin/logs/error/' + params._id;
      result = await this.myService.update(params, url, customHeaders);
      console.log(url, result);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

}
