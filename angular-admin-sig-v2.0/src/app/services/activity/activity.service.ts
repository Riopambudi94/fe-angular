import { Injectable,Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpEventType } from '@angular/common/http';
import { MasterService } from '../master.service';
import {api_url, getTokenHeader} from '../../../environments/environment';

interface Banner {
  error: any,
  result: any
}

@Injectable({
  providedIn: 'root',
  
})


export class ActivityService {

  api_url: string = '';
  constructor(private http: HttpClient, public myService:MasterService) { 
    this.api_url = api_url();
  }
  
  public addLog(params:any){
    let result;
    const customHeaders = getTokenHeader();
    
    params.platform = "browser";
    params.app = "admin-panel";
    
    try{
      const url   = 'activity/add-log';

      this.myService.post(params, url, customHeaders).then((r)=>{}).catch((e)=>{});
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    console.log(result);
    return  result;
  }

  public async getActivityReport(){
    let result;
    const customHeaders = getTokenHeader();
    
    try{
      const url   = 'activity/report';
      result = await this.myService.get({}, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    console.log(result);
    return  result;
  }

}
