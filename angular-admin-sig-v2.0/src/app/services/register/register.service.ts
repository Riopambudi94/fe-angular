import { Injectable,Component, OnInit } from '@angular/core';
import { MasterService } from '../master.service';
import {getTokenHeader, api_url, getAPIKeyHeader} from '../../../environments/environment';

interface Register {
  error: any,
  result: any
}

@Injectable({
  providedIn: 'root',
  
})



export class RegisterService {

  public gerro: string = "test";
  constructor(public myService:MasterService) { 
  }

  
  // public async getMembersLint(){
   
  //   const url   = 'members/all';
  //   let result  = await this.myService.get(null, url);
   
  //   return  result;
  // }

  public async register(params){
    let result;
    const customHeaders = getTokenHeader();
    try {
      params.permission = 'admin';
      const url   = 'member/register';
      result  = await this.myService.add(params, url, customHeaders);
      //console.log("user data :: ",params);
    } catch (error) {
        throw new TypeError(error);
    }
    
    return  result;
  }

  public async registerOwner(params) {
    let result;
    const customHeaders = getAPIKeyHeader();
    console.log(customHeaders, " header");

    try {
      const route_url = api_url() + "member/register";
      result = await this.myService.post(params, route_url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return result;
  }
  
}
