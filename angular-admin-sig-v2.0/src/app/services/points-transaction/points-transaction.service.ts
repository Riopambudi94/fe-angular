import { Injectable, Component, OnInit } from '@angular/core';

import { HttpClient, HttpEventType, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { api_url, getTokenHeader, getTokenHeaderRetailer } from '../../../environments/environment';
import { MasterService } from '../master.service';
import Swal from 'sweetalert2';

interface Pointstransaction {
  error: any,
  result: any,
}

@Injectable ({
  providedIn: 'root',
})



export class PointsTransactionService {
    courses$: Observable<Pointstransaction[]>;
    api_url : string = "";
    public httpReq: any = false;
    constructor(private http: HttpClient, public myService: MasterService) {
        this.api_url = api_url();
    }
    addMonths(date, months) {
      date.setMonth(date.getMonth() + months);
      return date;
    }

    getPointstransaction(params): Observable<Pointstransaction[]> {
        // const uri = 'http://192.168.1.198/f3/v1/orderhistory/all';
        // console.log(uri);
        // return this.http.get<Pointstransaction[]>(uri);
        let myToken = localStorage.getItem('tokenlogin');
        let myStatus = localStorage.getItem('isLoggedin');

        if (myStatus == 'true') {
          try {
            if (params) {

              let httpParams = new HttpParams();
              params.search = Object.assign({}, params.search);
              params.order_by = Object.assign({}, params.order_by);
              let arrayParams = Object.assign({}, params);
              httpParams = httpParams.append('request', JSON.stringify(arrayParams));

              const httpOpt = {
                headers: new HttpHeaders(
                  {
                    'Content-Type': 'application/json',
                    'Authorization': myToken,
                  }
                ),
                params: httpParams
              };

              const uri = this.api_url + 'point-transaction/report';
              console.log("URI points transaction history", uri)
              return this.http.get<Pointstransaction[]>(uri, httpOpt);
            }
          } catch (error) {
            throw new TypeError(error);
          }
        } else {
          localStorage.removeItem('isLoggedin');
          window.location.reload();
        }
    }
    public async getPointReportLint(params) {
      let result;
      console.log('param',params)
      const customHeaders = getTokenHeader();


      try {
        const url = 'point-transaction/report';
        result = await this.myService.get({request: JSON.stringify(params)}, url, customHeaders);
        console.log(result);
      } catch (error) {
        throw new TypeError(error);
      }
      console.log(result);
      return result;
    }
    public async searchPointReportLint(params) {
      let result;
      const customHeaders = getTokenHeader();
      console.log('params ', params)
      if(!params.search.start_date && !params.search.end_date){
        params.search.start_date = this.addMonths(new Date(), -6)
        params.search.end_date = new Date()
      }
      try {
        const url = 'point-transaction/report';
        result = await this.myService.searchResult(params, url, customHeaders);
      } catch (error) {
        throw new TypeError(error);
      }
      return result;
  }

  //POINT TRANSACTION REPORT
  public async getPointstransactionReportRequestedLint(param) {
    let result;
    const customHeaders = getTokenHeaderRetailer();
    try {
      const url = 'points-transaction/all/report?request={"search":{"order_by":{"created_date":-1}, "based_on":"process_number","status":"' + param + '"}}';
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    // console.log(result);
    return result;
  }

  public async getPointstransactionReportProcessLint(param1, param2, param3) {
    let result;
    const customHeaders = getTokenHeaderRetailer();
    try {
      const url = 'points-transaction/all/report?request={"search":{"order_by":{"created_date":-1}, "based_on":"process_number","status":{"in":["'+param1+'","'+ param2+'","'+param3+'"]}}}';
      // const url = 'points-transaction/all/report?request={"search":{"order_by":{"created_date":-1}, "based_on":"process_number","status":{"in":["PROCESSED","CANCELED","CANCELLED"]}}}';
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    // console.log(result);
    return result;
  }
  
  // REPORT APPROVAL
  public async getPointstransactionApprovalProcessLint(param) {
    let result;
    const customHeaders = getTokenHeaderRetailer();
    try {
      const url = 'points-transaction/all/report?request={"search":{"status":"' + param + '"},"current_page":1,"limit_per_page":50}';
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    // console.log(result);
    return result;
  }

  // SEARCH POINT TRANSACTION REPORT
  public async searchPointstransactionProcessReport(params){
    let result;
    const customHeaders = getTokenHeaderRetailer();
    try{
      const url = `points-transaction/all/report?request={"search":{"process_number":"${params}"},"current_page":1}`;
      // const url = 'points-transaction/all/report'
      result = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result
  }

  //POINT APPROVAL REPORT based on per batch
  public async searchPointstransactionApprovalProcessLint(params){
    params.search.status = "REQUESTED";
    let result;
    const customHeaders = getTokenHeaderRetailer();
    try{
      const url = 'points-transaction/all/report'
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result
  }

  public async searchPointstransactionApprovalProcessLint2(params){
    params.search.status = "PROCESSED";
    let result;
    const customHeaders = getTokenHeaderRetailer();
    try{
      const url = 'points-transaction/all/report'
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result
  }

  //POINT TRANSACTION REPORT based on per batch
  public async searchPointstransactionReportProcessLint(params){
    console.warn("params", params)

    params.search.status = "REQUESTED";
    let result;
    const customHeaders = getTokenHeaderRetailer();
    try{
      const url = 'points-transaction/all/report'
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result
  }

  public async searchPointstransactionReportProcessLint2(params){
    params.search.status = "PROCESSED";
    let result;
    const customHeaders = getTokenHeaderRetailer();
    try{
      const url = 'points-transaction/all/report'
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result
  }

  //POINT TRANSACTION REPORT based on member
  public async getPointstransactionReportLint() {
    let result;
    const customHeaders = getTokenHeaderRetailer();
    try {
      // const url = 'points-transaction/all/report?request={"search":"current_page":1,"limit_per_page":50}';
      const url = 'points-transaction/all/report?request={"search":{"based_on":"member"},"current_page":1,"limit_per_page":50}';
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    // console.log(result);
    return result;
  }

  public async searchPointstransactionreportLint(params) {
    let result;
    const customHeaders = getTokenHeaderRetailer();
    params.search.based_on = "member";
    console.log("params", params);
    try {
      const url = 'points-transaction/all/report';
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  public async uploadPointsCodeBulk(file, obj,payload, type){
    let myStatus = localStorage.getItem('isLoggedin');
    let myToken;
    if(myStatus){
      myToken = localStorage.getItem('tokenlogin');
    }

    const fd = new FormData();
    //fd.append('image', file, file.name)
    fd.append('type',payload.type)
    fd.append('document', file)
    let url: string = api_url()+'points-transaction/new?process_type='+type;
    let result;
    
    this.httpReq = this.http.post(url,fd,{
      headers: getTokenHeaderRetailer(),
      // headers : new HttpHeaders({
      //   'Authorization': myToken,
      //   'app-label': 'retailer_prg'
      // }),
      reportProgress: true,
      observe: 'events'
    }).subscribe(async(event) => {
      let c = JSON.parse(JSON.stringify(event));

      if(c.body){
          obj.Report=c.body._id;
          // obj.prodOnUpload = true;

          if (c.status == 200 && c.body && c.body.result && c.body.result.process_number) {
            obj.selectedFile = null;
            obj.startUploading = false;
            Swal.fire(
              'Your file has been uploaded!',
              'Process Number: ' + c.body.result.process_number,
              'success'
            )
          }
          // obj.prodOnUpload = true;
      }
      if(event.type == HttpEventType.UploadProgress){
        let prgval = Math.round( event.loaded / event.total * 100);
        obj.updateProgressBar(prgval);
        //console.log(obj.cancel);
        if(prgval==100){
          console.log(event);
          
        }
        if(obj.cancel == true){
          this.httpReq.unsubscribe();
        }
        // if(prgval == 100){
        //   if(obj.onUploadSuccess) 
        //     obj.onUploadSuccess();
        // }
        console.log('upload Progress: ' +prgval+"%");
      }
    },
    result =>{
      // console.warn("RESULT", result);
      if(result.error) {
          obj.errorFile = result.error.error;
          obj.cancel    = true;
          obj.selectedFile = null;
          Swal.fire(
            'Failed!',
            result.error.error,
            'warning'
          )
        }
    }
    );
  }

  public async processPoint(params){
    let result;
    const customHeaders = getTokenHeaderRetailer();;
    try{
      const url = `points-transaction/process`
      result = await this.myService.post(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result
  }

    public async getPointstransactionLint(params) {
        let result;

        const customHeaders = getTokenHeader();


        try {
          const url = 'point-transaction/report';
          result = await this.myService.get({request: JSON.stringify(params)}, url, customHeaders);
          console.log(result);
        } catch (error) {
          throw new TypeError(error);
        }
        console.log(result);
        return result;
      }

    public async searchPointstransactionLint(params) {
        let result;
        const customHeaders = getTokenHeader();
        console.log('params ', params)
        // if(!params.search.start_date && !params.search.end_date){
        //   params.search.start_date = this.addMonths(new Date(), -6)
        //   params.search.end_date = new Date()
        // }
        try {
          const url = 'point-transaction/report';
          result = await this.myService.searchResult(params, url, customHeaders);
        } catch (error) {
          throw new TypeError(error);
        }
        return result;
    }

    public async detailPointstransaction(pointstransaction_id) {
        let result;
        const customHeaders = getTokenHeader();

        try {
          const url = 'point-transaction/' + pointstransaction_id;
          // const url = 'point-transaction/report?request={"search": {"_id":"'+pointstransaction_id+'"}}';
          result = await this.myService.get(null, url, customHeaders);
          console.log(result);
        } catch (error) {
          throw new TypeError(error);
        }
        return result;
  }

   public async updatePointstransaction(params) {
        let result;
        const customHeaders = getTokenHeader();
        console.log(params);
        try {
          const url = 'point-transaction/update/' + params._id;
          result = await this.myService.update(params, url, customHeaders);
          console.log(url, result);
        } catch (error) {
          throw new TypeError(error);
        }
        return result;
  }

  // POINT MOVEMENT REPORT BASED ON PRODUCT
  public async getPointMovementBasedOnProduct() {
    let result;
    const customHeaders = getTokenHeaderRetailer();

    try {
      // const url = 'order-invoice/report/sales-order?request={"limit_per_page":50,"current_page":1,"search":{"status":"in":["PROCESSED","COMPLETED"]}}';
      const url = 'order-invoice/report/sales-order?request={"limit_per_page":50,"current_page":1,"search":{"last_shipping_info":{"in":["on_delivery","delivered","redelivery"]}}}';
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    // console.log(result);
    return result;
  }

  public async searchPointMovementBasedOnProduct(params) {
    // params.search.status = {
    //   in:["PROCESSED","COMPLETED"]
    // };
    params.search.last_shipping_info = {
      in:["on_delivery","delivered","redelivery"]
    };

    let result;
    const customHeaders = getTokenHeaderRetailer();
    
    try {
      const url = 'order-invoice/report/sales-order';
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }

  // POINT MOVEMENT REPORT BASED ON ORDER
  public async getPointMovementBasedOnOrder(params) {
    let result;

    const customHeaders = getTokenHeader();


    try {
      const url = 'point-transaction/report?request={"limit_per_page":50,"current_page":1,"search":{"based_on":"invoice"}}';
      result = await this.myService.get(null, url, customHeaders);
      console.log(result);
    } catch (error) {
      throw new TypeError(error);
    }
    // console.log(result);
    return result;
  }

  public async searchPointMovementBasedOnOrder(params) {
    params.search.based_on = "invoice"
    let result;
    const customHeaders = getTokenHeader();
    // console.log('params ', params)
    // if(!params.search.start_date && !params.search.end_date){
    //   params.search.start_date = this.addMonths(new Date(), -6)
    //   params.search.end_date = new Date()
    // }
    try {
      const url = 'point-transaction/report';
      result = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    return result;
  }



}
