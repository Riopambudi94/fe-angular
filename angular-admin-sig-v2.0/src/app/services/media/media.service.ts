import { Injectable,Component, OnInit } from '@angular/core';
import { MasterService } from '../master.service';
import { Observable , of} from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import {api_url, getTokenHeader} from '../../../environments/environment';
import { HttpClient, HttpHeaders, HttpParams, HttpEventType } from '@angular/common/http';
interface Media {
  error: any,
  result: any
}

@Injectable({
  providedIn: 'root',
  
})


export class MediaService {

  courses$: Observable<Media[]>;

  api_url: string = '';
  public httpReq: any = false;

  public gerro: string = "test";
  constructor(private http: HttpClient,public myService:MasterService) { 
    this.api_url = api_url();
  }

  public async getMediaLint() {
    let result;
    const customHeaders = getTokenHeader();
    try {
    const url   = 'media/all';
    result  = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  public async searchMediaLint(params) {
    //console.log(params);
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'media/all';
      result  = await this.myService.searchResult(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      return  result;
      //console.log(result);
  }

  public async detailMedia(product_code) {
    let result;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'media/' +product_code;
      result  = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    console.log(result);

    return  result;
  }

  public async upload(file, obj){
    const fd = new FormData();
    fd.append('image', file, file.name)
    let url: string = api_url()+'media/upload';
    console.log(url);
    let result;
    
    this.httpReq = this.http.post(url,fd,{
      reportProgress: true,
      observe: 'events'
    }).subscribe(event => {
      console.log(event);
      if(event.type == HttpEventType.UploadProgress){
        let prgval = Math.round( event.loaded / event.total * 100);
        obj.updateProgressBar(prgval);
        console.log(obj.cancel);
        if(obj.cancel == true){
          this.httpReq.unsubscribe();
        }
        console.log('upload Progress: ' +prgval+"%");
      }
    },
    result =>{
      console.log(result);
      if(result.error)
        { obj.errorFile = result.error.error;
          obj.cancel    = true;
        }
    }
    );
  }

  public async updateMediaProductCode(params) {
    let result: any;
    const customHeaders = getTokenHeader();
    try {
      const url   = 'media/update_file/' +params.product_code;
      result  = await this.myService.update(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
      // console.log(result);
    return  result;
  }

}
