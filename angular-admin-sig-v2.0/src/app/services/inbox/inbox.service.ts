import { Injectable,Component, OnInit } from '@angular/core';
import { MasterService } from '../master.service';
import { Observable , of} from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import {api_url, getTokenHeader} from '../../../environments/environment';
import { HttpClient, HttpHeaders, HttpParams, HttpEventType } from '@angular/common/http';
interface Media {
  error: any,
  result: any
}

@Injectable({
  providedIn: 'root',
  
})


export class InboxService {

  courses$: Observable<Media[]>;

  api_url: string = '';
  public httpReq: any = false;

  public gerro: string = "test";
  constructor(private http: HttpClient,public myService:MasterService) { 
    this.api_url = api_url();
  }

  public async getInbox() {
    let result;
    const customHeaders = getTokenHeader();
    try {
    const url   = 'notifications-portal';
    result  = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    // console.log("test", result)

    return  result;
  }

  public async detailInbox(_id) {
    let result;
    const customHeaders = getTokenHeader();
    try {
    const url   = 'notifications/' +_id;
    result  = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    // console.log("test", result)

    return  result;
  }
  
  public async readInbox(_id) {
    let result;
    const customHeaders = getTokenHeader();
    try {
    const url   = 'notifications/read/' +_id;
    result  = await this.myService.get(null, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }
    console.log("test", result)
    return  result;
  }

  public async deleteInbox(params, _id) {
    let result;
    const customHeaders = getTokenHeader();
    try {
    const url   = 'notifications/delete' + _id;
    result  = await this.myService.get(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  public async multipleDelete(params, _id) {
    let result;
    const customHeaders = getTokenHeader();
    try {
    const url   = 'notifications/delete' + _id;
    result  = await this.myService.get(params, url, customHeaders);
    } catch (error) {
      throw new TypeError(error);
    }

    return  result;
  }

  // public async getInboxAll() {
  //   let result;
  //   const customHeaders = getTokenHeader();
  //   try {
  //   const url   = 'notifications/list';
  //   result  = await this.myService.get(null, url, customHeaders);
  //   } catch (error) {
  //     throw new TypeError(error);
  //   }

  //   return  result;
  // }

  

}
