import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './app-login-routing.module';
import { AppLoginComponent } from './app-login.component';
import { PageHeaderModule } from '../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../component-libs/form-builder-table/form-builder-table.module';
import { BsComponentModule } from '../layout/modules/bs-component/bs-component.module';
import { LogoutComponent } from './app-logout.component';
// import { HeaderComponent } from '../layout/components/header/header.component';
import { HeaderModule } from '../layout/components/header/header.module';
import { PortalGuard } from '../shared/guard/portal.guard';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    imports: [
        CommonModule,
        LoginRoutingModule,
        PageHeaderModule, 
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        FormBuilderTableModule,
        HeaderModule,
        TranslateModule,
        BsComponentModule,
    ],
    declarations: [AppLoginComponent, LogoutComponent],
    providers:[
        PortalGuard
    ]
})
export class AppLoginModule {}
