import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { LoginService } from '../services/login/login.service';
import { errorConvertToMessage } from '../../environments/environment';
import { setTime } from 'ngx-bootstrap/chronos/utils/date-setters';
import { clearSession } from '../object-interface/common.function';

@Component({
    selector: 'app-login',
    templateUrl: './app-login.component.html',
    styleUrls: ['./app-login.component.scss'],
    animations: [routerTransition()]
})
export class LogoutComponent implements OnInit {
    public name: string = "";
    Login: any = [];
    service: any;
    loginOpened = false;
    imageLoginData = [
        {img: 'url("/assets/images/malte-wingen-PDX_a_82obo-unsplash-mini.jpg")', text: "The Amazing way to start anything is Today."},
        {img: 'url("/assets/images/alexander-rotker-l8p1aWZqHvE-unsplash.jpg")',    text: "Keep your eyes on the stars."},
        {img: 'url("/assets/images/curology-sR1oAhAT_Uw-unsplash.jpg")',        text: "Your Journey is never ending."},
        {img: 'url("/assets/images/vincent-branciforti-mGh2rjPgUyA-unsplash.jpg")',     text: "Not Impossible<br/> But I'm-possible."},
        {img: 'url("/assets/images/nick-van-den-berg-2gaNlcD75sk-unsplash.jpg")',         text: "Everyhting is not as easy as they said."},
       
    ]
    choosenImage;
    errorLabel : any = false;
    email:any;
    password:any;
    form;

     getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
      }

    constructor(public loginService:LoginService, private router: Router) {
       
        this.choosenImage = this.imageLoginData[this.getRandomInt(0,4)];
        console.log("CHOOSEN IMAGE", this.choosenImage);
        let form_add     : any = [
            { label:"Email",  type: "text",   data_binding: 'email'  },
            { label:"Password",  type: "password",  data_binding: 'password'  }
          ];
    }

    async ngOnInit() {
        await clearSession();
        this.router.navigateByUrl("/login");
    }

    async firstLoad()
    {

    }

    // openLogin(){
    //     this.loginOpened = !this.loginOpened;
    // }
    onLoggedin() {
        localStorage.setItem('isLoggedin', 'true');
    }

    async formSubmitAddMember(form){
        this.errorLabel = false;
        if(this.loginOpened == false){
            this.loginOpened = !this.loginOpened;
            return false;
        }
        // console.log(JSON.stringify(form));
        try 
        {
                // this.service    = this.loginService;
                let result: any  = await this.loginService.login(form);
                this.Login = result.result;
               
                localStorage.setItem('isLoggedin', 'true');
                localStorage.setItem('tokenlogin',this.Login.token);

                if(typeof this.Login.permission == 'string' &&  this.Login.permission == 'merchant'){
                    this.router.navigateByUrl('/merchant-portal');
                }
                else if(typeof this.Login.permission == 'string' &&  this.Login.permission == 'admin'){
                    this.router.navigateByUrl('/dashboard');
                }
                else if(Array.isArray(this.Login.permission) &&  this.Login.permission.includes('merchant')){
                    this.router.navigateByUrl('/merchant-portal');
                }
                // return false;
        } 
        catch (e) 
        {
            setTimeout(()=>{
                this.errorLabel = (<Error>e).message;
            }, 1000)
        }
        return false;
      }
}