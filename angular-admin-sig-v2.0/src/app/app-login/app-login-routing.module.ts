import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppLoginComponent } from './app-login.component';
import { AuthGuard } from '../shared';
// import { LogoutComponent } from './app-logout.component';
// import { OrderhistoryComponent } from '../layout/modules/orderhistory/orderhistory.component';

const routes: Routes = [
    {
        path: '',
        component: AppLoginComponent,
        canActivate: [AuthGuard]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LoginRoutingModule {}
