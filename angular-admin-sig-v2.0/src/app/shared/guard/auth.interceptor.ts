import { Injectable } from '@angular/core';

import {
    HttpInterceptor,
    HttpRequest,
    HttpResponse,
    HttpHandler,
    HttpEvent,
    HttpErrorResponse
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    expired = false;
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token: string = localStorage.getItem('token');

        if (token) {
            request = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + token) });
        }

        // if (!request.headers.has('Content-Type')) {
        //     request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
        // }

        request = request.clone({ headers: request.headers.set('Accept', 'application/json') });
        console.log(" here");
        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    // console.log('event--->>>', event);
                    // this.errorDialogService.openDialog(event);
                }
                return event;
            }),
            catchError((error: HttpErrorResponse) => {
                let data = {};
                console.warn(error)
                data = {
                    reason: error && error.error && error.error.reason ? error.error.reason : '',
                    status: error.status
                };
                if (error.status === 401 && error.error.error == "INV_TOKEN"){
                    this.expired = true;
                    if(this.expired = true){
                        alert("Akun Anda Sedang Dipakai")
                        this.expired =false;
                    }
                    window.location.href = "/login";
                } else if (error.status === 400 && error.error.error == "Unauthorized, authorization token expired"){
                    this.expired = true;
                    if(this.expired = true){
                        alert("Unauthorized, login expired. Please re-login")
                        this.expired =false;
                    }
                    window.location.href = "/login";
                }
                return throwError(error);
                
            }));
           
    }
    
}