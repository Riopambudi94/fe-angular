import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';
import { MediaService } from '../../../services/media/media.service';

@Component({
  selector: 'app-media',
  templateUrl: './media.component.html',
  styleUrls: ['./media.component.scss'],
  animations: [routerTransition()]
})

export class MediaComponent implements OnInit {

  Media         : any = [];

    tableFormat: TableFormat = {
        title: 'Active Media Detail',
        label_headers: [
            { label: 'Nama Gambar', visible: true, type: 'string', data_row_name: 'name' },
            { label: 'Product Code', visible: true, type: 'string', data_row_name: 'product_code' }

        ],
        row_primary_key: '_id',
        formOptions: {
            row_id: '_id',
            this: this,
            result_var_name: 'Media',
            detail_function: [this, 'callDetail'],
            type: 'image',
            image_data_property: 'image_url'
        }
    };


  form_input    : any = {};
  errorLabel : any = false;
  mediaDetail : any = false;
  service      : any ;
  selectedFile  = null;
  progressBar   :number= 0;
  cancel        :boolean = false;
  errorFile     : any = false;
  url = '';

  constructor(public mediaService:MediaService) {}

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad(){
    try{
      this.service    = this.mediaService;
      let result: any  = await this.mediaService.getMediaLint();
      //console.log(result);
      this.Media = result.result;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }


  public async callDetail(product_code)
  {
      try{
      let result: any;
      this.service    = this.mediaService;
      result          = await this.mediaService.detailMedia(product_code);
      if(!result.error && result.error == false){
        this.mediaDetail = result.result[0];
      }
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async backToHere(obj){
    obj.mediaDetail = false;
  }

  onFileSelected(event){
    this.errorFile = false;
    let fileMaxSize = 3000000;// let say 3Mb
    Array.from(event.target.files).forEach((file: any) => {
        if(file.size > fileMaxSize){
          this.errorFile="Maximum File Is 3Mb";
        }
    });
    this.selectedFile = event.target.files[0];
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => { // called once readAsDataURL is completed
        let _event: any = event;
        this.url = _event.target.result;
      }
    }
  }

  updateProgressBar(value){
    this.progressBar = value;
  }

  cancelThis(){
    this.cancel = !this.cancel;
  }

  async onUpload(){
      try{
      this.cancel = false;
      if(this.selectedFile){
        await this.mediaService.upload(this.selectedFile,this);
        }
      } catch (e) {
        this.errorLabel = ((<Error>e).message);//conversion to Error type
      }
  }


}
