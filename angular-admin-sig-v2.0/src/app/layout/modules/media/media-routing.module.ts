import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MediaComponent } from './media.component';
// import { MemberAddComponent } from './add/member.add.component';
import { MediaDetailComponent } from './detail/media.detail.component';
import { MediaEditComponent } from './edit/media.edit.component';

const routes: Routes = [
  {
      path: '', component: MediaComponent,
     
  },
  // {
  //     path:'add', component: MemberAddComponent
  // },
  {
    path:'detail', component: MediaDetailComponent
  },
  {
    path:'edit', component: MediaEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MediaRoutingModule { }
