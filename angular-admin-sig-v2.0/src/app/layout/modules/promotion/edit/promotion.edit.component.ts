import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { PromotionService } from '../../../../services/promotion/promotion.service';

@Component({
    selector: 'app-promotion-edit',
  templateUrl: './promotion.edit.component.html',
  styleUrls: ['./promotion.edit.component.scss'],
  animations: [routerTransition()]
})

export class PromotionEditComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  public loading = false;
  public status: any = [
    { label: 'ACTIVE', value: 1 },
    { label: 'INACTIVE', value: 0 }
  ];
  public use: any = [
    { label: 'CURRENTLY USED', value: 1 },
    { label: 'UNUSED', value: 0 }
  ];
  public type: any = [
    { label: 'PERCENTAGE', value: 1 },
    { label: 'NOMINAL', value: 0 }
  ];
  public category: any = [
    { label: 'SEMUA', value: 1 },
    { label: 'TRANSAKSI PEMBELIAN', value: 2 },
    { label: 'ONGKOS KIRIM', value: 3 }
  ];
  form: any = {
		type: 'product',
		min_order: 1,
		product_code: '',
		product_name: '',
		category: 'public',
		price: 0,
		fixed_price: 0,
		qty: 0,
		description: '',
		tnc: '',
		location_for_redeem: '',
		dimensions: {
			width: 0,
			length: 0,
			height: 0
		},
		weight: 1,
		images_gallery: [],
		weightInGram: 1000,
		pic_file_name: '',
		need_outlet_code: 1,
	};
  public transactionStatus: any = [
    {label: 'PENDING', value: 'PENDING'}
   , {label: 'CANCEL', value: 'CANCEL'}
   , {label: 'FAILED', value: 'FAILED'}
   , {label: 'SUCCESS', value: 'SUCCESS'}
   , {label: 'ERROR', value: 'ERROR'}
];
  errorLabel: any = false;
  transaction_status: any;


  constructor(public promotionService: PromotionService) {}

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {

    try {
      this.detail.previous_status = await this.detail.status;
      this.transactionStatus.forEach((element, index) => {
      if (element.value == this.detail.transaction_status) {
              this.transactionStatus[index].selected = 1;
          }
      });
    } catch (e) {
      this.errorLabel = ((<Error>e).message); // conversion to Error type
    }

    // this.detail.previous_member_id = this.detail.member_id;
    // this.memberStatus.forEach((element, index) => {
    //     if(element.value == this.detail.member_status){
    //         this.memberStatus[index].selected = 1;
    //     }
    // });

  }

  backToDetail() {
    // console.log(this.back);
    this.back[0][this.back[1]]();
  }

  async saveThis() {
    try {
      this.loading = !this.loading;
      await this.promotionService.updatePromotion(this.detail);
      this.loading = !this.loading;
    } catch (e) {
      this.errorLabel = ((<Error>e).message); // conversion to Error type
    }
  }

}
