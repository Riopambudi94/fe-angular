import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NotificationComponent } from './notification.component';
import { NotificationAddComponent } from './add/notification.add.component';
import { FormInputComponent }  from '../../../component-libs/form-input/form-input.component';
import { PageHeaderModule } from '../../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { FormSelectComponent } from '../../../component-libs/form-select/form-select.component';
import { NotificationDetailComponent } from './detail/notification.detail.component';
import { NotificationRoutingModule } from './notification-routing.module';
import { NotificationEditComponent } from './edit/notification.edit.component';
import { BsComponentModule } from '../bs-component/bs-component.module';

@NgModule({
  imports: [CommonModule,
    NotificationRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule
],
  
  declarations: [
    NotificationComponent, NotificationAddComponent, NotificationDetailComponent, NotificationEditComponent] 
})
export class NotificationModule { }
