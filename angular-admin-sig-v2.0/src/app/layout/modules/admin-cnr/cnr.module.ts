import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormInputComponent }  from '../../../component-libs/form-input/form-input.component';
import { PageHeaderModule } from '../../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { FormSelectComponent } from '../../../component-libs/form-select/form-select.component';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { ChartsModule as Ng2Charts, ChartsModule } from 'ng2-charts';
import { CnrRoutingModule } from './cnr-routing.module';
import { CnrComponent } from './cnr.component';

@NgModule({
  imports: [CommonModule,
    CnrRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule,
    ChartsModule
],
  
  declarations: [
    CnrComponent]
})
export class CnrModule { }
