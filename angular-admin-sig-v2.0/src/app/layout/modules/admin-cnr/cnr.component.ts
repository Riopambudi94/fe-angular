import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { OrderhistoryService } from '../../../services/orderhistory/orderhistory.service';
import { DomSanitizer } from '@angular/platform-browser';
import {TopBarMenuItem} from '../../../object-interface/common.object';

@Component({
  selector: 'app-order-history-summary-cnr',
  templateUrl: './cnr.component.html',
  styleUrls: ['./cnr.component.scss'],
  animations: [routerTransition()]
})

export class CnrComponent implements OnInit {

  data       : any = [];
  service    : OrderhistoryService
  errorLabel : string
  // getCRR : any;
  dataTable:any;
  topBarMenu : TopBarMenuItem[] =[
  ];
  topMembers : []
  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    // elements: {
    //     line: {
    //         tension: 0 // disables bezier curves
    //     }
    // }
  };
  public barChartLabels: any = [];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;

  avgData = {
    monthly: {
      all: 0,
      pending: 0,
      paid: 0,
      checkout: 0,
      waiting: 0,
      cancel: 0
    },
  }

  public optO = {fill:false , borderWidth: 1,}
  public analitycsData:any = {
    crrData : {
      barChartData : [
        { data: [], label: 'Crr' ,
        ...this.optO},
      ],
      barChartLabels : []
    },
  }
  // private options = new RequestOptions(
  //   { headers: new Headers({ 'Content-Type': 'application/json' }) });
  constructor(public OrderhistoryService:OrderhistoryService , public sanitizer:DomSanitizer) {
    // this.firstLoad();
    // this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl("");
  }


  async ngOnInit() {
    let currentDate = new Date();
    console.log(this.barChartLabels);
    // this.onUpdateCart();
    this.firstLoad();

  }

  generateChartData(dataValues){

    let barChartLabels = [];
    let newData        = [];


    let allData:any;
    allData = dataValues;

    let total = 0;
    for(let data in allData){
      // console.log('allData data', allData[data], data)
      barChartLabels.push(data)
      total += parseInt(allData[data].CNR);
      newData.push(allData[data].CNR)

    }
    allData = null; 
   
    
    return [barChartLabels, newData, total]
  }

  async firstLoad() {

    try {
      this.service       = this.OrderhistoryService;
      const result: any  = await this.OrderhistoryService.getCNR('daily');
      this.data          = result.result;
      console.log("CNR", this.data);

      this.dataTable = [];
      for(let[key,element] of Object.entries(this.data.values)){
        let c:any = element;

        c.date = key;
        this.dataTable.push(c);
      }
      
      
      
      if(this.data.values){
        
        const convertedCrrData:any    = this.generateChartData(this.data.values);
        

        
        
        
        // console.log("convertedDataMonthly", convertedDataMonthly);
        const clone   = JSON.parse(JSON.stringify(this.analitycsData.crrData.barChartData));
        clone[0].data = convertedCrrData[1];
        // console.log('convertedDataMonthlyPending', convertedDataMonthlyPending);
        // clone.push({
        //   data : convertedDataMonthlyPending[1],
        //   label: "pending",
        //   fill:false 
        // })
        

        console.log("convertedDataMonthly clone", clone);
        
        // this.barChartLabels = convertedCrrData[0]
        this.analitycsData.crrData.barChartLabels = convertedCrrData[0]

        // this.barChartLabels = convertedDataMonthly[0]
        // this.analitycsData.monthly.barChartLabels = convertedDataMonthly[0]
        // this.barChartData   = clone;
        this.analitycsData.crrData.barChartData = clone;
        
      }
      
    } catch (e) {
      console.log("this e result", e)

      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

  }

 
  



}
