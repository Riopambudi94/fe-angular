import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PackagebundleComponent } from './packagebundle.component';
import { PackagebundleRoutingModule } from './packagebundle.routing';
import { PageHeaderModule } from '../../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { Ng2CompleterModule } from 'ng2-completer';
import {PackagebundleDetailComponent} from './detail/packagebundle.detail.component';
import {PackagebundleEditComponent} from './edit/packagebundle.edit.component';
import { PackagebundleGenerateComponent } from './generate/packagebundle.generate.component';
// import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
@NgModule({
  declarations: [PackagebundleComponent, PackagebundleDetailComponent, PackagebundleEditComponent, PackagebundleGenerateComponent],
  // imports: [CommonModule, FormRoutingModule, PageHeaderModule],
  imports: [
    // NgMultiSelectDropDownModule.forRoot(),
    CommonModule,
    PackagebundleRoutingModule,
    PageHeaderModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule,
    Ng2CompleterModule
  ]
})
export class PackagebundleModule { }
