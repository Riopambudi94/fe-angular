import { PackagebundleService } from '../../../services/packagebundle/packagebundle.service';
import { Component, OnInit } from '@angular/core';
import {routerTransition} from '../../../router.animations';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';


@Component({
  selector: 'app-packagebundle',
  templateUrl: './packagebundle.component.html',
  styleUrls: ['./packagebundle.component.scss'],
    animations: [routerTransition()]
})
export class PackagebundleComponent implements OnInit {

    Packagebundle: any = [];
  tableFormat: TableFormat = {
                                  title           : 'Package Bundle Detail Page',
                                  label_headers   : [
                                    // {label: 'Promo Deals ID', visible: false, type: 'date', data_row_name: '_id'},
                                    {label: 'Title', visible: true, type: 'string', data_row_name: 'title'},
                                    {label: 'Description', visible: true, type: 'string', data_row_name: 'description'},
                                    {label: 'Discount (decimal)', visible: true, type: 'number', data_row_name: 'discount'},
                                    // {label: 'Type', visible: false, type: 'string', data_row_name: 'type'},
                                    {label: 'Created Date', visible: true, type: 'date', data_row_name: 'created_date'},
                                    {label: 'Value', visible: true, type: "number", data_row_name: 'fixed_value'},
                                    // {label: 'Updated Date', visible: false, type: "date", data_row_name: 'updated_date'},

                                  ],
                                  row_primary_key : '_id',
                                  formOptions     : {
                                    addFormGenerate   : true,
                                                    row_id: '_id',
                                                    this  : this,
                                                    result_var_name: 'Packagebundle',
                                                    detail_function: [this, 'callDetail']
                                                  }
  };
  form_input: any = {};
  errorLabel: any = false;

  PackagebundleDetail: any = false;
  checkDetails: any;
  service: any;

  constructor(public PackagebundleService: PackagebundleService) { }

  ngOnInit() {
      this.firstLoad();
  }

  async firstLoad() {
    try {
      this.service    = this.PackagebundleService;
      const result: any  = await this.PackagebundleService.getPackagebundleLint({});
      this.Packagebundle = result.result.values;
    } catch (e) {
      this.errorLabel = ((<Error>e).message); // conversion to Error type
    }
  }

  public async callDetail(Packagebundle_id) {
    try {
      let result: any;
      this.service    = this.PackagebundleService;
      result          = await this.PackagebundleService.detailPackagebundle(Packagebundle_id)
      if(result.result.length == 0){
        alert('Data Has Been Removed');
      }
      else{
        this.PackagebundleDetail = result.result[0];
      }
    } catch (e) {
      this.errorLabel = ((<Error>e).message); // conversion to Error type
    }
  }

  public async backToHere(obj) {
    obj.PackagebundleDetail = false;
  }


}
