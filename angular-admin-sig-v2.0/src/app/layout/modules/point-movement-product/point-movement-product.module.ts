import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PointMovementProductRoutingModule } from './point-movement-product-routing.module';
import { PointMovementProductComponent } from './point-movement-product.component';
import { PageHeaderModule } from '../../../shared';
import { FormBuilderTableComponent }  from '../../../component-libs/form-builder-table/form-builder-table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
// import { BastComponent } from './bast/bast.component';

import { BsComponentModule } from '../bs-component/bs-component.module';

@NgModule({
  
  imports: [
    CommonModule,
    PointMovementProductRoutingModule,
    PageHeaderModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule],
    declarations: [PointMovementProductComponent],
  
})
export class PointMovementProductModule { }
