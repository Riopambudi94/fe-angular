import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PointsTransationReportComponent } from './points-transaction-report.component';
import { PointsTransationReportAddComponent } from './add/points-transaction-report.add.component';
import { FormInputComponent }  from '../../../component-libs/form-input/form-input.component';
import { PageHeaderModule } from '../../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { PointsTransationReportRoutingModule } from './points-transaction-report.routing';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { PointsTransactionReportDetailComponent } from './detail/points-transaction-report.detail.component';

@NgModule({
  imports: [CommonModule,
    PointsTransationReportRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule
  ],
  
  declarations: [
    PointsTransationReportComponent, 
    PointsTransationReportAddComponent,
    PointsTransactionReportDetailComponent,
  ],

  // exports : [MemberComponent]
})
export class PointsTransactionReportModule { }
