import { Component, OnInit, Input, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { routerTransition } from '../../../../router.animations';
import { EVoucherService } from '../../../../services/e-voucher/e-voucher.service';
import { PointsTransactionService } from '../../../../services/points-transaction/points-transaction.service';

@Component({
  selector: 'app-points-transaction-report-add',
  templateUrl: './points-transaction-report.add.component.html',
  styleUrls: ['./points-transaction-report.add.component.scss'],
  animations: [routerTransition()]
})

export class PointsTransationReportAddComponent implements OnInit {
  @Input() public back;
  @Input() public detail: any;
  @ViewChild('addBulk', {static: false}) addBulk;

  public name: string = "";
  Evouchers: any = [];
  service: any;
  // type:any;
  Report:any;
  member_status:any;
  activation_status:any;
  
  errorLabel : any = false;
 
  isBulkUpdate : any = false;
  selectedFile  = null;
  cancel = false;
  progressBar = 0;
  errorFile: any = false;
  prodOnUpload: any     = false;
  startUploading: any = false;
  showUploadButton: any = false;
  getPassword: any;
  successUpload: any = false;
  urlDownload: any = "";

  warnIDPelanggan: any = false;
  warnNamaPemilik: any = false;
  warnTelpPemilik: any = false;
  warnWAPemilik: any = false;

  showPasswordPage: any = false;

  pointType = [
    { label: 'Add', value: 'add', selected:1 },
    { label: 'Adjust In', value: 'adj_in' },
    { label: 'Adjust Out', value: 'adj_out'}
  ];

  type:any = "add";


  constructor(public evoucherService:EVoucherService, public pointsTransactionService:PointsTransactionService) {}

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad(){
    this.selectedFile = null;
    this.startUploading = false;
  }

  async updateDataBulk(){
    try
    {
        this.startUploading = true;
        this.cancel = false;
        let payload = {
          type : 'application/form-data',
        }
        if(this.selectedFile) {
          const result: any = await this.pointsTransactionService.uploadPointsCodeBulk(this.selectedFile,this,payload, this.type);
          if (result) {
            this.firstLoad();
          }
        }
      } 
      catch (e) {
        this.startUploading = false;
        this.errorLabel = ((<Error>e).message);//conversion to Error type
      }
  }

  // async formSubmitAddMember(){
  //     let form_add = this.form;
  //     try {
  //       this.service    = this.evoucherService;
  //       const result: any  = await this.evoucherService.uploadEvoucherCodeBulk(form_add);
  //       console.warn("result add member", result)
  
  //       if (result && result.data) {
  //         this.getPassword = result.data.password;
  //       }

  //       this.data.username = this.form.id_pel;
  //       this.data.password = this.getPassword;
  //       Swal.fire({
  //         title: 'Success',
  //         text: 'Pelanggan berhasil ditambahkan',
  //         icon: 'success',
  //         confirmButtonText: 'Ok',
  //       }).then((result) => {
  //         if(result.isConfirmed){
  //           this.showThisPassword();
  //         }
  //       });
  //     } catch (e) {
  //       console.warn("error", e.message)
  //       Swal.fire({
  //         title: 'Failed',
  //         text: e.message,
  //         icon: 'warning',
  //         confirmButtonText: 'Ok',
  //       }).then((result) => {
  //         if(result.isConfirmed){
  //           this.firstLoad();
  //         }
  //       });
  //       this.errorLabel = ((<Error>e).message);//conversion to Error type
  //     }
  // }

  onFileSelected(event){
    this.errorFile = false;
    let fileMaxSize = 3000000;// let say 3Mb
    // var reader = new FileReader();
    // reader.readAsDataURL(event.target.files[0]); //
    Array.from(event.target.files).forEach((file: any) => {
        if(file.size > fileMaxSize){
          this.errorFile="maximum file is 3Mb";
        }
    });
    
    this.selectedFile = event.target.files[0];
    this.addBulk.nativeElement.value = '';
    
  } 

  cancelThis(){
    this.cancel = !this.cancel;
  }

  actionShowUploadButton() {
    this.showUploadButton = !this.showUploadButton;
  }

  updateProgressBar(value){
    this.progressBar = value;
  }

  backTo(){
    window.history.back();
  }

}
