import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { PointsGroupService } from '../../../../services/points.group/pointsgroup.service';

@Component({
  selector: 'app-points-group-add',
  templateUrl: './points-group.add.component.html',
  styleUrls: ['./points-group.add.component.scss'],
  animations: [routerTransition()]
})

export class PointsGroupAddComponent implements OnInit {
  public name: string = "";
  PointsGroup: any = [];
  service: any;
  errorLabel : any = false;
  org_id:any;
  campaign_name:any;
  campaign_id:any;
  group_name:any;
  group_code:any;
  description:any;
  status:any;
  pts_model_id:any;
  start_date:any;
  end_date:any;


  public groupStatus: any = [
    {label: 'ACTIVE', value: 'ACTIVE'},
    {label: 'INACTIVE', value: 'INACTIVE'}
  ];
                
  constructor(public pointsgroupService:PointsGroupService) {
    
   }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad(){
    // this.service = this.memberService;
    // let result: any = await this.memberService.getMembersLint();
    // this.Members = result.result;

  }
  formSubmitAddGroup(form){
    // console.log(form);
    // console.log(JSON.stringify(form));

    try {
      this.service    = this.pointsgroupService;
      const result: any  = this.pointsgroupService.addNewOutlet(form);
      this.PointsGroup = result.result;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

}


}