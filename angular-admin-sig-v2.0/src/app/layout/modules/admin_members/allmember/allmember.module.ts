import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AllMemberComponent } from './allmember.component';
import { AllMemberAddComponent } from './add/allmember.add.component';
import { FormInputComponent }  from '../../../../component-libs/form-input/form-input.component';
import { PageHeaderModule } from '../../../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../../../../component-libs/form-builder-table/form-builder-table.module';
import { FormSelectComponent } from '../../../../component-libs/form-select/form-select.component';
import { AllMemberDetailComponent } from './detail/allmember.detail.component';
import { AllMemberRoutingModule } from './allmember-routing.module';
import { AllMemberEditComponent } from './edit/allmember.edit.component';
import { BsComponentModule } from '../../bs-component/bs-component.module';

@NgModule({
  imports: [CommonModule,
    AllMemberRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule,
],
  
  declarations: [
    AllMemberComponent, AllMemberAddComponent, AllMemberDetailComponent,AllMemberEditComponent],
  // bootstrap: [AllMemberComponent]
})
export class AllMemberModule { }
