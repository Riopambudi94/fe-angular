import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../../../router.animations';
import { MemberService } from '../../../../../services/member/member.service';

@Component({
  selector: 'app-all-member-add',
  templateUrl: './allmember.add.component.html',
  styleUrls: ['./allmember.add.component.scss'],
  animations: [routerTransition()]
})

export class AllMemberAddComponent implements OnInit {
  public name   :string = "";
  AllMember     :any = [];
  service       :any;
  errorLabel    : any = false;
  type          : string;
  maritalStatus : string;
  full_name: any;
  email:any;
  password:any;
  pin:any;
  cell_phone:any;
  card_number:any;
  dob:any;
  mcity:any;
  mstate:any;
  mcountry:any;
  maddress1:any;
  mpostcode:any;
  gender:any;

  public member_type = [
                       {label:"superuser", value:"superuser"}
                      ,{label:"member", value:"member"}
                      ,{label:"marketing", value:"marketing"}
                      ,{label:"admin", value:"admin"}
                      ,{label:"merchant", value:"merchant"}
                    ];
                
  constructor(public memberService:MemberService) {
    let form_add     :any = [
      { label:"Full Name",  type: "text",  value: "", data_binding: 'full_name'  },
      { label:"Email",  type: "text",  value: "", data_binding: 'email'  },
      { label:"Password",  type: "password",  value: "", data_binding: 'password'  },
      { label:"PIN",  type: "password",  value: "", data_binding: 'pin'  },
      { label:"Cell Phone",  type: "password",  value: "", data_binding: 'cell_phone'  },
      { label:"Card Number ",  type: "text",  value: "", data_binding: 'card_number' },
      { label:"Date of Birth",  type: "text",  value: "", data_binding: 'dob' },
      { label:"City",  type: "text",  value: "", data_binding: 'mcity' },
      { label:"State",  type: "text",  value: "", data_binding: 'mstate' },
      { label:"Country",  type: "text",  value: "", data_binding: 'mcountry' },
      { label:"Address",  type: "textarea",  value: "", data_binding: 'maddress1' },
      { label:"Post Code",  type: "text",  value: "", data_binding: 'mpostcode' },
      { label:"Gender",  type: "text",  value: "", data_binding: 'gender' },
      { label:"Marital Status",  type: "text",  value: "", data_binding: 'marital_status' },
      { label:"type ",  type: "textarea",  value: [{label:"label", value:"label", selected:1},{label:"label1", value:"label2"}], data_binding: 'address2' },
      { label:"address 2 ",  type: "textarea",  value: "", data_binding: 'address2' },
    ];
   }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad(){
    // this.service = this.memberService;
    // let result:any = await this.memberService.getMembersLint();
    // this.Members = result.result;

  }
  formSubmitAddMember(form){
    // console.log(form);
    // console.log(JSON.stringify(form));
    console.log("pass : ",form.password)
    try {
      this.service    = this.memberService;
      const result: any  = this.memberService.addNewMember(form);
      this.AllMember = result.result;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

}


}
