import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotificationMessageComponent } from './notificationmessage.component';
import { NotificationMessageAddComponent } from './add/notificationmessage.add.component';


const routes: Routes = [
  {
      path: '', component: NotificationMessageComponent,

  },
  {
      path:'add', component: NotificationMessageAddComponent
  },
  // {
  //   path:'detail', component: NotificationDetailComponent
  // }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotificationMessageRoutingModule { }
