
import { Component, OnInit }  from '@angular/core';
import { routerTransition }   from '../../../router.animations';
import { ProductService }     from '../../../services/product/product.service';
import { count } from 'rxjs/operators';
import {FormOptions, TableFormat, productsTableFormat} from '../../../object-interface/common.object';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import * as content from '../../../../assets/json/content.json';

@Component({
  selector    : 'app-product',
  templateUrl : './product.component.html',
  styleUrls   : ['./product.component.scss'],
  animations  : [routerTransition()]
})

export class ProductComponent implements OnInit {

  // this section belongs to product item list
  Products      : any = [];
  tableFormat   : TableFormat = {
    title           : 'Products Data',
    label_headers   : [
      {label: 'Product Name', visible: true, type: 'string', data_row_name: 'product_name'},
      {label: 'Product Code', visible: true, type: 'string', data_row_name: 'product_code'},
      {label: 'Variation', visible: true, type: 'variation', data_row_name: 'variation'},
      {label: 'Type', visible: true, type: 'string', data_row_name: 'type'},
      {label: 'Total Quantity', visible: true, type: 'string', data_row_name: 'total_qty'},
      {label: 'Category', visible: true, type: 'string', data_row_name: 'category'},
      {label: 'Status',visible:true, type: 'string', data_row_name: 'status'},
      {label: 'Weight (gr)', visible: true, type: 'string', data_row_name: 'weight'},
      {label: 'Dimensions', visible: true, type: 'dimensions', data_row_name: 'dimensions'},
      {label: 'Images Gallery', visible: true, type: 'images_gallery', data_row_name: 'images_gallery'},
      {label: 'Created Date', visible: true, type: 'string', data_row_name: 'created_date'},
      {label: 'Updated Date', visible: true, type: 'string', data_row_name: 'updated_date'},
    ],
    row_primary_key : 'product_code',
    formOptions     : {
                      row_id: 'product_code',
                      addForm: true,
                      addBulk: true,
                      this  : this,
                      result_var_name: 'Products',
                      detail_function: [this, 'callDetail'],
                      customButtons:[
                        {label: 'Approve', func:(f)=>{this.approval(f)}},
                      ]
                    },
    show_checkbox_options: true
  }

  tableFormat2   : TableFormat = {
    title           : 'Products Data',
    label_headers   : [
      {label: 'Product Name', visible: true, type: 'string', data_row_name: 'product_name'},
      {label: 'Product Code', visible: true, type: 'string', data_row_name: 'product_code'},
      {label: 'Variation', visible: true, type: 'variation', data_row_name: 'variation'},
      {label: 'Type', visible: true, type: 'string', data_row_name: 'type'},
      {label: 'Total Quantity', visible: true, type: 'string', data_row_name: 'total_qty'},
      {label: 'Category', visible: true, type: 'string', data_row_name: 'category'},
      {label: 'Status',visible:true, type: 'string', data_row_name: 'status'},
      {label: 'Weight (gr)', visible: true, type: 'string', data_row_name: 'weight'},
      {label: 'Dimensions', visible: true, type: 'dimensions', data_row_name: 'dimensions'},
      {label: 'Images Gallery', visible: true, type: 'images_gallery', data_row_name: 'images_gallery'},
      {label: 'Created Date', visible: true, type: 'string', data_row_name: 'created_date'},
      {label: 'Updated Date', visible: true, type: 'string', data_row_name: 'updated_date'},
    ],
    row_primary_key : 'product_code',
    formOptions     : {
                      row_id: 'product_code',
                      addForm: true,
                      addBulk: true,
                      this  : this,
                      result_var_name: 'Products',
                      detail_function: [this, 'callDetail'],
                      customButtons:[
                        {label: 'Approve', func:(f)=>{this.approval(f)}},
                      ]
                    },
    show_checkbox_options: true
  }

  public contentList : any = (content as any).default;

  form_input    : any = {};
  errorLabel : any = false;
  errorMessage  : any = false;
 

  prodDetail              : any = false;
  service                 : any;

  selectedFile            : File    = null;
  progressBar             :number   = 0;
  cancel                  :boolean  = false;
  errorFile               : any     = false;
  
  totalPage  = 0;
  page = 1;
  pageSize = 50;

  mci_project: any = false;

  constructor(public productService:ProductService, private router: Router) { }

  ngOnInit() {
    this.firstLoad();
  }

  async approval(listedData:any[]){
    // console.log("masuk coy")
    if(listedData == undefined || listedData.length==0){
      return false;
    }
    let listOfData=[];
//
    listedData.forEach((element, index) => {
      listOfData.push(element._id);
    });
    listedData = [];
    if(listOfData.length == 0) return false;
    this.service        = this.productService;
    let result          = await this.productService.approvedProducts({product_ids: listOfData});
    if(result.result.status == 'success'){
      alert("the Current Data Below has been approved");
      this.firstLoad();
    }
  }
  
  async firstLoad(){
    try{
      const program = localStorage.getItem('programName');
      let _this = this;
      this.contentList.filter(function(element) {
          if(element.appLabel == program) {
              if(element.type == "reguler") {
                  _this.mci_project = true;
              } else {
                  _this.mci_project = false;
              }
          }
      });
      

      let result: any;
      this.service    = this.productService;
      result          = await this.productService.getProductsReport();
      this.totalPage  = result.total_page;
      this.Products   = result.values;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type

      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
      }
    }
  }

  public async callDetail(_id)
  {
    this.router.navigate(['administrator/productadmin/edit'], {queryParams: {product_code:_id}})
    // try{
    //   let result: any;
    //   this.service    = this.productService;
    //   result          = await this.productService.detailProducts(_id);
    //   if(!result.error && result.error == false){
    //     this.prodDetail = result.result[0];
    //   }
    // } catch (e) {
    //   this.errorLabel = ((<Error>e).message);//conversion to Error type
    // }
  }

  public async backToHere(obj){
    obj.prodDetail = false;
    // delete obj.prodDetail;
    
  }

  onFileSelected(event){
    this.errorFile = false;
    let fileMaxSize = 3000000;// let say 3Mb
    Array.from(event.target.files).forEach((file: any) => {
        if(file.size > fileMaxSize){
          this.errorFile="maximum file is 3Mb";
        }
    });
    this.selectedFile = event.target.files[0];

  } 

  updateProgressBar(value){
    this.progressBar = value;
  }

  cancelThis(){
    
    this.cancel = !this.cancel;
  }
  callAfterUpload(result) {
    console.log("THE RESULT", result);

  }

  async onUpload(){
    try{
      this.cancel = false;
      if(this.selectedFile){
        await this.productService.uploadFile(this.selectedFile,this,'product', this.callAfterUpload);
        }
        alert("Upload Success")
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type

      let message = this.errorLabel;
      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
      }
      Swal.fire({
        icon: 'error',
        title: message,
      });
    }
    if(this.selectedFile) {}
  }



  // valuechange(event, input_name, download?: boolean) {

  //   if (this.onSearchActive == false) {
  //     this.onSearchActive = true;
  //     let myVar = setTimeout(async () => {
  //       if (this.form_input) {
  //         let clearFormInput = this.form_input
  //         let searchDate = {};

  //         if (clearFormInput.order_date) {
  //           if (clearFormInput.order_date.to) {
  //             let order_date_from_to = {
  //               order_date_from: clearFormInput.order_date.from,
  //               order_date_to: clearFormInput.order_date.to
  //             }
  //             Object.assign(clearFormInput, order_date_from_to)
  //           }
  //           else {
  //             let order_date_from = {
  //               order_date_from: clearFormInput.order_date.from,
  //             }
  //             Object.assign(clearFormInput, order_date_from)
  //           }

  //           delete clearFormInput.order_date
  //         }

  //         this.showLoading = true;
  //         console.log("clearFormInput", clearFormInput)
  //         let filter = {
  //           search: clearFormInput,
  //           order_by: this.orderByResult,
  //           limit_per_page: this.pageLimits,
  //           current_page: this.currentPage,
  //           download: false
  //         }
  //         // console.log(filter)

  //         // let result = await searchCallback[0][searchCallback[1]](filter);
  //         let result = await this.orderhistoryService.getOrderHistoryMerchantFilter(filter);
  //         this.allData = result.result.values;
  //         this.buildPagesNumbers(result.result.total_page)
  //       } else {
  //         this.firstLoad();
  //       }
  //       this.onSearchActive = false;
  //       this.showLoading = false;

  //     }, 2000);
  //   }

  // }



}
