import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductRoutingModule } from './product-routing.module';
import { ProductComponent } from './product.component';
import { PageHeaderModule } from './../../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { ProductAddComponent } from './add/product.add.component';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
// import { NgxEditorModule } from 'ngx-editor';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { ProductDetailComponent } from './detail/product.detail.component';
import { ProductEditComponent } from './edit/product.edit.component';
import { ProductAddBulkComponent } from './add-bulk/product.add-bulk.component';
import { MerchantEditProductComponent } from '../../merchant-portal/separated-modules/merchant-products/products/merchant-edit-product/merchant-edit-product.component';
import { MerchantAddNewProductComponent } from '../../merchant-portal/separated-modules/merchant-products/products/merchant-add-new-product/merchant-add-new-product.component';
import { ProductsModule } from '../../merchant-portal/separated-modules/merchant-products/products.module';


@NgModule({
  imports: [
    CommonModule, 
    ProductRoutingModule, 
    PageHeaderModule, 
    FormsModule, 
    FormBuilderTableModule, 
    ReactiveFormsModule,
    // NgxEditorModule,
    NgbModule,
    HttpClientModule,
    CKEditorModule,
    ProductsModule
  ],
  declarations: [ProductComponent, ProductAddComponent, 
    ProductDetailComponent,
    ProductEditComponent,
    ProductAddBulkComponent
  ]
})
export class ProductModule { }
