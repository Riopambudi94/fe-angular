import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrderhistorycancelComponent } from './orderhistorycancel.component';
import { OrderhistorycancelDetailComponent } from './detail/orderhistorycancel.detail.component';

// import { OrderhistorysummaryDetailComponent } from './detail/orderhistorysummary.detail.component';
// import { OrderhistorysummaryEditComponent } from './edit/orderhistorysummary.edit.component';


const routes: Routes = [
  {
      path: '', component: OrderhistorycancelComponent
  },
  // {
  //   path:'detail', component: OrderhistorysuccessDetailComponent
  // },
  // {
  //   path:'edit', component: OrderhistorysummaryEditComponent
  // }
  {
    path: 'edit', component: OrderhistorycancelDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderhistorycancelRoutingModule { }
