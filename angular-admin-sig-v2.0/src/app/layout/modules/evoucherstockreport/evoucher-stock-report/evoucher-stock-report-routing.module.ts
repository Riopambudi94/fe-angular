import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EvoucherStockReportComponent } from './evoucher-stock-report.component';
import { EvoucherStockAddComponent } from './add/evoucher-stock-report.add.component';
import { EVStockDetailComponent } from './detail/evoucher-stock-report.detail.component';

const routes: Routes = [
  {
      path: '', component: EvoucherStockReportComponent
  },
  {
    path:'add', component: EvoucherStockAddComponent
  },
  {
    path:'detail', component: EVStockDetailComponent
  },
  // {
  //   path: 'edit', component: ProductEditComponent
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EvoucherStockReportRoutingModule { }
