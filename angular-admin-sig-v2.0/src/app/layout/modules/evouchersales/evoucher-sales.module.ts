import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EvoucherSalesRoutingModule } from './evoucher-sales-routing.module';
import { EvoucherSalesComponent } from './evoucher-sales.component';
import { PageHeaderModule } from '../../../shared';
import { FormBuilderTableComponent }  from '../../../component-libs/form-builder-table/form-builder-table.component';
import { EvoucherSalesDetailComponent } from './detail/evoucher-sales.detail.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
// import { BastComponent } from './bast/bast.component';

import { BsComponentModule } from '../bs-component/bs-component.module';

@NgModule({
  
  imports: [
    CommonModule,
    EvoucherSalesRoutingModule,
    PageHeaderModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule,
    ProgressbarModule.forRoot(),
  ],
    declarations: [
      EvoucherSalesComponent,
      EvoucherSalesDetailComponent
    ],
  
})
export class EvoucherSalesModule { }
