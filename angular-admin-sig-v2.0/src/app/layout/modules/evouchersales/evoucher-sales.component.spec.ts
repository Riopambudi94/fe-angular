import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EvoucherSalesComponent } from './evoucher-sales.component';

describe('EvoucherSalesComponent', () => {
  let component: EvoucherSalesComponent;
  let fixture: ComponentFixture<EvoucherSalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EvoucherSalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EvoucherSalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
