import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { OrderhistoryService } from '../../../services/orderhistory/orderhistory.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-update-po',
  templateUrl: './po-bulk-update.component.html',
  styleUrls: ['./po-bulk-update.component.scss'],
  animations: [routerTransition()]
})

export class POBulkUpdateComponent implements OnInit {
  @ViewChild('updateBulk', {static: false}) updateBulk;
  
  errorLabel : any = false;

  isBulkUpdate : any = false;
  showUploadButton: any = false;
  selectedFile  = null;
  cancel = false;
  progressBar = 0;
  errorFile: any = false;
  prodOnUpload: any = false;
  startUploading: any = false;

  constructor(public OrderhistoryService:OrderhistoryService) {
    
  }

  ngOnInit() {
    this.firstLoad();
  }

  async firstLoad(){
    this.selectedFile  = null;

    this.prodOnUpload = false;
  }

  onFileSelected(event){
    this.errorFile = false;
    let fileMaxSize = 3000000;// let say 3Mb
    // var reader = new FileReader();
    // reader.readAsDataURL(event.target.files[0]); //
    Array.from(event.target.files).forEach((file: any) => {
        if(file.size > fileMaxSize){
          this.errorFile="maximum file is 3Mb";
        }
    });
    
    this.selectedFile = event.target.files[0];
    this.updateBulk.nativeElement.value = '';
    
  } 
  
  cancelThis(){
    this.cancel = !this.cancel;
  }

  async updateDataBulk(){
    try
      {
        this.startUploading = true;
        this.cancel = false;
        let payload = {
          type : 'application/form-data',
        }
        if(this.selectedFile) {
          console.log('file', this.selectedFile,this)
          const result: any = await this.OrderhistoryService.updateBulkInvoice(this.selectedFile,this,payload, "po_no");
          if (result) {
            this.firstLoad();
          }
        }
        // this.firstLoad();
      } 
      catch (e) 
      {
        this.startUploading = false;
        this.errorLabel = ((<Error>e).message);//conversion to Error type

        let message = this.errorLabel;
        if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
          message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
        }
        Swal.fire({
          icon: 'error',
          title: message,
        });
      }
  }

  updateProgressBar(value){
    this.progressBar = value;
  }

  backTo(){
    window.history.back();
  }

}
