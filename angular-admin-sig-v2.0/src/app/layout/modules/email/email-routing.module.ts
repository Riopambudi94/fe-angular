import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmailComponent } from './email.component';
import { EmailAddComponent } from './add/email.add.component';
import { EmailDetailComponent } from './detail/email.detail.component';

const routes: Routes = [
  {
      path: '', component: EmailComponent,

  },
   {
      path:'add', component: EmailAddComponent
  },
  {
    path:'detail', component: EmailDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmailRoutingModule { }
