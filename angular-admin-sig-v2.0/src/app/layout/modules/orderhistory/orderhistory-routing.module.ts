import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrderhistoryComponent } from './orderhistory.component';
import { OrderhistoryDetailComponent } from './detail/orderhistory.detail.component';

const routes: Routes = [
  {
      path: '', component: OrderhistoryComponent
  },
  {
    path:'detail', component: OrderhistoryDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderhistoryRoutingModule { }
