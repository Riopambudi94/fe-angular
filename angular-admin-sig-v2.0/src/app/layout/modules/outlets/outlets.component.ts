import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { OutletsService } from '../../../services/outlets/outlets.service';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';

@Component({
  selector: 'app-moutlet',
  templateUrl: './outlets.component.html',
  styleUrls: ['./outlets.component.scss'],
  animations: [routerTransition()]
})

export class OutletsComponent implements OnInit {
  @Input() public detail: any;

  // Outlets       : any = [];
    Products   : any         = [];
    tableFormat: TableFormat = {
        title: 'Outlets',
        label_headers: [
            { label: 'Merchant Name', visible: true, type: 'string', data_row_name: 'merchant_name' },
            { label: 'Outlet Name', visible: true, type: 'string', data_row_name: 'outlet_name' },
            { label: 'Outlet Code', visible: true, type: 'string', data_row_name: 'outlet_code' },
            // { label: 'Outlet Type', visible: false, type: 'string', data_row_name: 'outlet_type' },
            // { label: 'Contact Person', visible: false, type: 'string', data_row_name: 'contact_person' },
            // { label: 'Email', visible: true, type: 'string', data_row_name: 'email' },
            // { label: 'Work Phone', visible: false, type: 'string', data_row_name: 'work_phone' },
            // { label: 'Cell Phone', visible: false, type: 'string', data_row_name: 'cell_phone' },
            { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
            { label: 'Added Date', visible: true, type: 'string', data_row_name: 'added_date' }
        ],
        // row_primary_key: '_id',
        row_primary_key : 'merchant_username',
        formOptions     : {
            // row_id: '_id',
            row_id          : 'merchant_username',
            addForm         : true,
            this            : this,
            result_var_name : 'Products',
            // result_var_name : 'Outlets',
            detail_function : [this, 'callDetail'],
        }
    };
  // row_id        : any = "_id";
  // form_input    : any = {};
  // errorLabel : any = false;
  // outletsDetail: any = false;
  // service     : any ;
  form_input              : any     = {};
  errorLabel              : any     = false;
  totalPage               : 0;
  outlet_username         : any;
  prodDetail              : any     = false;
  service                 : any;
  selectedFile            : File    = null;
  progressBar             : number  = 0;
  cancel                  : boolean = false;
  errorFile               : any     = false;


  constructor(public outletsService:OutletsService) {}

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    // try {

    //   this.service    = this.outletsService;
    //   const result: any  = await this.outletsService.getAllOutletsLint();
    //   this.Outlets = result.result;
    // } catch (e) {
    //   this.errorLabel = ((<Error>e).message);//conversion to Error type
    // }
    try{
      let params = {
        'type':'merchant'
      }
      let result: any;
      this.service    = this.outletsService;
      result          = await this.outletsService.getMerchant(params);
      this.totalPage  = result.result.total_page;
      console.log(result)
      this.Products   = result.result.values;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

  }

  public async callDetail(_id) {

    try {

      this.service    = this.outletsService;
      let result: any = await this.outletsService.detailMerchant(_id);
      this.prodDetail = result.result[0];
      //console.log(result.result);
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async backToHere(obj) {
    obj.prodDetail = false;
  }

}
