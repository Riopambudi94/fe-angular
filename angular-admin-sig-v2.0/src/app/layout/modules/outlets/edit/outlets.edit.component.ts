import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { OutletsService } from '../../../../services/outlets/outlets.service';


@Component({
  selector: 'app-moutlet-edit',
  templateUrl: './outlets.edit.component.html',
  styleUrls: ['./outlets.edit.component.scss'],
  animations: [routerTransition()]
})

export class OutletsEditComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;
  errorLabel : any = false;
  status     : any;

  public loading        : boolean = false;

  public outletStatus: any = [
    {label: 'ACTIVE', value: 'ACTIVE'},
    {label: 'INACTIVE', value: 'INACTIVE'}
  ];

  constructor(public outletsService: OutletsService) {
  }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {

    this.detail.previous_member_id = this.detail.member_id;
    this.outletStatus.forEach((element, index) => {
        if (element.value == this.detail.member_status) {
            this.outletStatus[index].selected = 1;
        }
    });

  }

  backToDetail() {
    // console.log(this.back);
    this.back[0][this.back[1]]();
  }

  async saveThis() {
    try {
      this.loading = !this.loading;
      await this.outletsService.updateMerchant(this.detail);
      this.loading = !this.loading;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

}
