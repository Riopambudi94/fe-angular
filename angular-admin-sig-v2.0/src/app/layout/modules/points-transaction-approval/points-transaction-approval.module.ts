import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PointsTransationApprovalComponent } from './points-transaction-approval.component';
import { FormInputComponent }  from '../../../component-libs/form-input/form-input.component';
import { PageHeaderModule } from '../../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { PointsTransationApprovalRoutingModule } from './points-transaction-approval-routing.module';
import { BsComponentModule } from '../bs-component/bs-component.module';

@NgModule({
  imports: [CommonModule,
    PointsTransationApprovalRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule
  ],
  
  declarations: [
    PointsTransationApprovalComponent, 
  ],

  // exports : [MemberComponent]
})
export class PointsTransactionApprovalModule { }
