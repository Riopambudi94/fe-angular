import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminBastRoutingModule } from './admin-bast-routing.module';
import { AdminBastComponent } from './admin-bast.component';
import { PageHeaderModule } from '../../../shared';
import { FormBuilderTableComponent }  from '../../../component-libs/form-builder-table/form-builder-table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
// import { BastComponent } from './bast/bast.component';

import { BsComponentModule } from '../bs-component/bs-component.module';

@NgModule({
  
  imports: [
    CommonModule,
    AdminBastRoutingModule,
    PageHeaderModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule],
    declarations: [AdminBastComponent],
  
})
export class AdminBastModule { }
