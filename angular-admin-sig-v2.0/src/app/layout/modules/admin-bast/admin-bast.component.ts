import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { OrderhistoryService } from '../../../services/orderhistory/orderhistory.service';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';
import * as content from '../../../../assets/json/content.json';

@Component({
  selector: 'app-admin-bast',
  templateUrl: './admin-bast.component.html',
  styleUrls: ['./admin-bast.component.scss']
})
export class AdminBastComponent implements OnInit {

  BastReport: any = [];
  tableFormat        : TableFormat = {
                                  title           : 'Sales Redemption Report + BAST',
                                  label_headers   : [
                                    {label: 'Tanggal Order', visible: true, type: 'date', data_row_name: 'request_date'},
                                    {label: 'Tanggal Approve', visible: true, type: 'date', data_row_name: 'approve_date'},
                                    {label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id'},
                                    {label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'username'},
                                    // {label: 'Member', visible: true, type: 'string', data_row_name: 'member_name'},
                                    {label: 'Nama Pemilik', visible: true, type: 'form_nama_pemilik', data_row_name: 'member_detail'},
                                    {label: 'Nama Penerima', visible: true, type: 'form_nama_penerima', data_row_name: 'member_detail'},
                                    {label: 'Nama Toko', visible: true, type: 'form_nama_toko', data_row_name: 'member_detail'},
                                    {label: 'Type Customer', visible: true, type: 'form_group', data_row_name: 'member_detail'},
                                    {label: 'Alamat Kirim', visible: true, type: 'form_alamat_penerima', data_row_name: 'member_detail'},
                                    {label: 'No WA Penerima', visible: true, type: 'form_wa_penerima', data_row_name: 'member_detail'},
                                    {label: 'Hadiah Dikuasakan', visible: true, type: 'form_hadiah_dikuasakan', data_row_name: 'member_detail'},
                                    {label: 'Total Quantity', visible: true, type: 'string', data_row_name: 'total_quantity'},
                                    {label: 'Hadiah Redeem', visible: true, type: 'product_name', data_row_name: 'products'},
                                    {label: 'SKU', visible: true, type: 'product_code', data_row_name: 'products'},
                                    {label: 'Points Redeem', visible: true, type: 'string', data_row_name: 'sum_total'},
                                  ],
                                  row_primary_key : '_id',
                                  formOptions     : {
                                                    row_id: '_id',
                                                    this  : this,
                                                    result_var_name: 'BastReport',
                                                    detail_function: [this, 'callDetail'],
                                                    bastReport : true,
                                                  },
                                                  show_checkbox_options: true
  };
  tableFormat2        : TableFormat = {
    title           : 'Sales Redemption Report + BAST',
    label_headers   : [
      {label: 'Tanggal Order', visible: true, type: 'date', data_row_name: 'request_date'},
      {label: 'Tanggal Approve', visible: true, type: 'date', data_row_name: 'approve_date'},
      {label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'username'},
      { label: 'Nama Program', visible: true, type: 'form_cluster', data_row_name: 'member_detail' },
      // {label: 'Member', visible: true, type: 'string', data_row_name: 'member_name'},
      {label: 'Nama PIC', visible: true, type: 'form_nama_pemilik', data_row_name: 'member_detail'},
      {label: 'Nama Penerima', visible: true, type: 'form_nama_penerima', data_row_name: 'member_detail'},
      {label: 'Nama Entitas', visible: true, type: 'form_nama_toko', data_row_name: 'member_detail'},
      {label: 'Type Customer', visible: true, type: 'form_group', data_row_name: 'member_detail'},
      {label: 'Alamat Kirim', visible: true, type: 'form_alamat_penerima', data_row_name: 'member_detail'},
      {label: 'No WA Penerima', visible: true, type: 'form_wa_penerima', data_row_name: 'member_detail'},
      {label: 'Hadiah Dikuasakan', visible: true, type: 'form_hadiah_dikuasakan', data_row_name: 'member_detail'},
      {label: 'Total Quantity', visible: true, type: 'string', data_row_name: 'total_quantity'},
      {label: 'Hadiah Redeem', visible: true, type: 'product_name', data_row_name: 'products'},
      {label: 'SKU', visible: true, type: 'product_code', data_row_name: 'products'},
      {label: 'Points Redeem', visible: true, type: 'string', data_row_name: 'sum_total'},
    ],
    row_primary_key : '_id',
    formOptions     : {
                      row_id: '_id',
                      this  : this,
                      result_var_name: 'BastReport',
                      detail_function: [this, 'callDetail'],
                      bastReport : true,
                    },
                    show_checkbox_options: true
  };
  form_input    : any = {};
  errorLabel : any = false;
  totalPage  = 0;

  SalesRedemptionDetail: any = false;
  service: any;

  programType: any = "";

  public contentList : any = (content as any).default;

  constructor(public OrderhistoryService:OrderhistoryService) { }

  ngOnInit() {
    this.firstLoad();

  }
  
  async firstLoad(){
    try{
      var params = {
         search: {start_date:this.addMonths(new Date(), -6),end_date:new Date()},
         current_page:1,
         limit_per_page:50
        }
      this.service    = this.OrderhistoryService;
      let result: any  = await this.OrderhistoryService.getBastReportint(params);
      this.totalPage = result.total_page
      this.BastReport = result.values;

      const program = localStorage.getItem('programName');
      let _this = this;
      this.contentList.filter(function(element) {
        if(element.appLabel == program) {
            if(element.type == "reguler") {
                _this.programType = "reguler";
            } else if(element.type == "custom_kontraktual") {
              _this.programType = "custom_kontraktual";
            } else {
                _this.programType = "custom";
            }
        }
      });
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }
  
  public async callDetail(SalesRedemption_id){
    try{
      // let result: any;
      // this.service    = this.OrderhistoryService;
      // result          = await this.OrderhistoryService.detailPointstransaction(SalesRedemption_id);
      // console.log(result);

      // this.SalesRedemptionDetail = result.result[0];

    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async backToHere(obj){
    obj.SalesRedemptionDetail = false;
  }
  addMonths(date, months) {
    date.setMonth(date.getMonth() + months);
    return date;
  }
}
