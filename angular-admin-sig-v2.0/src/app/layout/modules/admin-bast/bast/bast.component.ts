import { Component, OnInit, Input  } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { OrderhistoryService } from '../../../../services/orderhistory/orderhistory.service';
import { async } from 'q';
import { MemberService } from '../../../../services/member/member.service';
import * as content from '../../../../../assets/json/content.json';
@Component({
  selector: 'app-bast',
  templateUrl: './bast.component.html',
  styleUrls: ['./bast.component.scss']
})
export class BastComponent implements OnInit {
	@Input() public data: any;
	@Input() public thisParent;
  public contentList : any = (content as any).default;
  titleProgram: any = "";
  service;
  getData = [];
  totalResult = [];
  totalWeight = [];
  perWeight = [];
  endTotalResult = [];
  special = [];
  loading = false;
  same = false;
  merchant_data ={};
  memberCity:any;
  courier_list: any;
  current_courier: any;
  courier_image:any;
  courier_image_array: any = [];
  sig_project: any = false;
  sig_kontraktual: any = false;
  sig_promotion_2: any = false;
    
  errorMessage: string;
  constructor(private OrderhistoryService: OrderhistoryService, private router: Router, private route: ActivatedRoute,private memberService: MemberService,) {
    // this.getData = route.snapshot.params['invoiceIds']
    //   .split(',');
  }

  ngOnInit() {
    this.firstload();
    // this.print();
  }

  async firstload() {
    const program = localStorage.getItem('programName');
    let _this = this;
    this.contentList.filter(function(element) {
      if(element.appLabel == program) {
          _this.titleProgram = element.title;
          if (program && program == "retail_poin_sahabat") {
            _this.sig_project = true;
          } else if (program && program == "sig_kontraktual") {
            _this.sig_kontraktual = true;
          } else if (program && program == "sig_promotion_2") {
            _this.sig_promotion_2 = true;
          }
        }
    });

    console.warn('this data', this.data)
    this.route.queryParams.subscribe(async (params) => {
      this.getData = params.data
      this.service = this.OrderhistoryService
      // console.warn("this getdata", this.getData)
    
    })
     
    

      this.data.forEach(element => {
        console.log("data", element)
        if(element.member_detail.hadiah_dikuasakan == 'ya' || element.member_detail.hadiah_dikuasakan == 'KUASA' || element.member_detail.hadiah_dikuasakan == 'true' ){
         this.special.push(element)
        }
      });
    // window.print();
    this.loading = false;
  }

  public prints(){
    window.print()
  }

  public backToTable(){
    this.thisParent.checkBox = [];
    this.thisParent.bast_mode = false;
    this.thisParent.salesRedemp = [];
  }
  
}
