import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CourierServicePageComponent } from './courier-service-page.component';

const routes: Routes = [
    {
        path: '',
        component: CourierServicePageComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CourierServicePageRoutingModule {}