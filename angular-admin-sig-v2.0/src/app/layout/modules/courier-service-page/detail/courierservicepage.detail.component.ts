import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { CourierServicePageService } from '../../../../services/courierservicepage/courier-service-page.service';

@Component({
  selector: 'app-courierservicepage-detail',
  templateUrl: './courierservicepage.detail.component.html',
  styleUrls: ['./courierservicepage.detail.component.scss'],
  animations: [routerTransition()]
})

export class CourierservicepageDetailComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  edit:boolean = false;
  
  constructor(public courierservicepageService:CourierServicePageService) {
  }

  ngOnInit() {
    this.firstLoad();
    
  }

  async firstLoad(){
    
    
  }

  editThis(){
    console.log(this.edit );
    this.edit = !this.edit;
    console.log(this.edit );
  }
  backToTable(){
    console.log(this.back);
    this.back[1](this.back[0]);
  }

}
