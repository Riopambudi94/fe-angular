import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BannerRoutingModule } from './banner-routing.module';
import { BannerComponent } from './banner.component';
import { PageHeaderModule } from '../../../shared';
import { FormBuilderTableComponent }  from '../../../component-libs/form-builder-table/form-builder-table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { BannerDetailComponent } from './detail/banner.detail.component';
import { BannerEditComponent } from './edit/banner.edit.component';
import { BannerAddComponent } from './add/banner.add.component';
import {MatTabsModule} from '@angular/material/tabs';


@NgModule({
  imports: [
    CommonModule, 
    MatTabsModule,
    BannerRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    CKEditorModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule],
  declarations: [BannerComponent,BannerDetailComponent,BannerEditComponent,BannerAddComponent]
})
export class BannerModule { }
