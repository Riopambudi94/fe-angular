import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { BannerService } from '../../../../services/banner/banner.service';
import { MediaService } from '../../../../services/media/media.service'
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { isString } from '../../../../../app/object-interface/common.object';
import { isArray } from 'ngx-bootstrap/chronos';

@Component({
  selector: 'app-banner-add',
  templateUrl: './banner.add.component.html',
  styleUrls: ['./banner.add.component.scss'],
  animations: [routerTransition()]
})

export class BannerAddComponent implements OnInit {
  @Input() public ok
  public name: string = "";
  Banner: any = [];
  service: any;
  errorLabel: any = false;
  selectedFile = null;
  selFile;
  progressBar: number = 0;
  cancel: boolean = false;
  errorFile: any = false;
  imageUrl: any = false;
  hashUrl: any;
  description: any;
  indexes: any;
  url: '';
  location: any;
  active: any;
  public Editor = ClassicEditor;
  public config = {
    toolbar: [ 'heading', '|', 'bold', 'italic', 'bulletedList', 'numberedList', 'blockQuote' ],
    heading: {
        options: [
            { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
            { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
            { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' }
        ]
    }
}
  public status: any = [
    { label: 'ACTIVE', value: 1 },
    { label: 'INACTIVE', value: 0 }
  ];
  public position: any = [
    { label: 'Halaman Home', value: 'front' },
    { label: 'Halaman Discovery Flash Popup', value: 'flash-popup' },
    { label: 'Halaman Discovery Deals', value: 'deals' },
    { label: 'Halaman Discovery Products', value: 'product' }
  ];
  public index: any = [
    { label: 'Row no. 1', value: '1' },
    { label: 'Row no. 2', value: '2' },
    { label: 'Row no. 3', value: '3' },
    { label: 'Row no. 4', value: '4' },
    { label: 'Row no. 5', value: '5' },
  ];
  labels = {
    // type: 'product type',
    // min_order: 'minimum order',
    // product_code: 'product code',
    // product_name: 'product name',
    // merchant_username: 'merchant username',
    // category: 'category',
    // price: 'price',
    // fixed_price: 'public price',
    // qty: 'quantity',
    // description: 'description',
    // tnc: 'term and conditions',
    // dimensions: 'dimensions',
    // weight: 'weight'
  }
  // public member_type = [
  //                      {label:"superuser", value:"superuser"}
  //                     ,{label:"member", value:"member", selected:1}
  //                     ,{label:"marketing", value:"marketing"}
  //                     ,{label:"admin", value:"admin"}
  //                     ,{label:"merchant", value:"merchant"}
  //                   ];
  isFileUploaded = false;
  constructor(public bannerService: BannerService, public mediaService: MediaService) {

    // let form_add: any = [
    //   { label: "Image URL", type: "text", value: "", data_binding: 'imageUrl' },
    //   { label: "Hash URL", type: "text", value: "", data_binding: 'hashUrl' },
    //   { label: "Location", type: "text", value: "", data_binding: 'location' },
    //   { label: "active", type: "number", value: "", data_binding: 'active' }
    // ];
  }
  locations = {
    'Halaman Depan': 'front',
    'Halaman Discovery Flash Popup': 'flash-popup',
    'Halaman Discovery Deals': 'deals',
    'Halaman Discovery Products': 'product'
  }
  form_add: any = {
    title:'',
    imageUrl:'',
    hashUrl: 'cls',
    location: '',
    active: undefined,
    target:{
      imageURL:'',
      description: '',
      value : '',
      type:'',
      index:undefined,
    }
  }

  ngOnInit() {
    this.firstLoad();

  }
  stringToNumber(str: any) {
    if (typeof str == 'number') {
      str = str + '';
    }

    let s = str.toLowerCase().replace(/[^0-9]/g, '');

    return parseInt(s);
  }


  parseStringToArray(str: any) {
    let splitR;
    if (!isString(str)) return [];
    else {
      splitR = str.split(/[\,\.]/);
    }
    return splitR;
  }
  async firstLoad() {
    // this.service = this.memberService;
    // let result: any = await this.memberService.getMembersLint();
    // this.Members = result.result;
    console.log('baseurl', this.form_add.base_url)
  }
  formSubmitAddBanner() {
    // form.imageUrl = this.imageUrl;
    // console.log(JSON.stringify(form));
    // this.form_add.location = this.locations[this.form_add.location];
    let form_add = this.form_add;
    
    form_add.active = this.stringToNumber(form_add.active)
    form_add.target.index = this.stringToNumber(form_add.target.index)
    try {
      console.log('form : ', form_add);

      const result: any = this.bannerService.addNewBanner(form_add);
      this.Banner = result.result;
      this.ok();
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
    

  }

  onFileSelected(event) {
    this.isFileUploaded = false;
    this.errorFile = false;
    let fileMaxSize = 3000000;// let say 3Mb
    var reader = new FileReader();

    reader.onload = (event: any) => {
      this.selFile = event.target.result;
    }
    reader.readAsDataURL(event.target.files[0]);

    Array.from(event.target.files).forEach((file: any) => {
      if (file.size > fileMaxSize) {
        this.errorFile = "maximum file is 3Mb";
      }
    });

    // console.log('INI FILENYA ', this.selectedFile)
    if (this.errorLabel == false) {
      this.selectedFile = event.target.files[0];

      setTimeout(() => {
        this.onUpload();
      }, 1500)
    }
  }
  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => { // called once readAsDataURL is completed
        let _event: any = event;
        this.url = _event.target.result;
      }
    }
  }
  updateProgressBar(value) {
    this.progressBar = value;
  }

  cancelThis() {
    this.cancel = !this.cancel;
  }


  async onUpload() {
    try {
      this.cancel = false;
      let result
      console.log('INI YANG AKAN ',this.selectedFile)
      if (this.selectedFile) {
        result = await this.bannerService.upload(this.selectedFile, this, 'image', (result) => { this.callAfterUpload(result) });
      }
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
    //   this.onUpload.
    return false;
  }
  callAfterUpload(result) {
    if (result) {
      // let file = result.base_url.concat(result.pic_medium_path);
      // let banner = this.form_add
      this.form_add.imageUrl =this.form_add.target.imageURL = result.base_url.concat(result.pic_big_path);
      console.log('ini hasilnya', this.form_add)
      this.isFileUploaded = true;
    }
  }


  validateForm(form) {
    for (var prop in form) {

      if (!form.hasOwnProperty(prop) || prop == 'tnc' || prop == 'category' || prop == 'weightInGram' || prop == 'hashtags')
        continue;

      if (prop == 'base_url') {
        if (!form[prop]) {
          throw new Error("Please upload new image for your product ")
        }
      }
      // console.log(prop, form[prop] == undefined, form[prop] == '')
      if (form[prop] == undefined)
        throw new Error("you need to completing this " + this.labels[prop])

      if (isString(form[prop]) && form[prop].trim() == '')
        throw new Error("you need to completing this input " + this.labels[prop])

      if (isArray(form[prop]) && form[prop].length == 0) {
        throw new Error("Hi, you need to completing this " + this.labels[prop])

      }

    }
  }

}
