import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminGroupComponent } from './admin-group.component';
import { AdminGroupAddComponent } from './add/admin-group.add.component';
import { FormInputComponent }  from '../../../component-libs/form-input/form-input.component';
import { PageHeaderModule } from '../../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { AdminRoutingModule } from './admin-group-routing.module';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { AdminGroupDetailComponent } from './detail/admin-group.detail.component';
import { AdminGroupEditComponent } from './edit/admin-group.edit.component';
// import { UserPointHistoryComponent } from './detail/user-point-history/member-point-history.component';

@NgModule({
  imports: [CommonModule,
    AdminRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule
  ],
  
  declarations: [
    AdminGroupComponent, AdminGroupAddComponent, AdminGroupDetailComponent, AdminGroupEditComponent, 
    // MemberPointHistoryComponent
  ],

  // exports : [MemberComponent]
})
export class AdminGroupModule { }
