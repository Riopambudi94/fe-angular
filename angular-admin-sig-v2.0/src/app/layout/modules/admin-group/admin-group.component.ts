import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { AdminService } from '../../../services/admin/admin.service';
import { DomSanitizer } from '@angular/platform-browser';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';

@Component({
  selector: 'app-admin-group',
  templateUrl: './admin-group.component.html',
  styleUrls: ['./admin-group.component.scss'],
  animations: [routerTransition()]
})

export class AdminGroupComponent implements OnInit {

  Members       : any = [];
  row_id        : any = "_id";

  tableFormat   : TableFormat = {
    title           : 'Admin  Group',
    label_headers   : [
                  {label: 'group name', visible: true, type: 'string', data_row_name: 'group_name'},
                  {label: 'description',     visible: true, type: 'string', data_row_name: 'description'},
                  {label: 'status',     visible: true, type: 'string', data_row_name: 'status'},
                  {label: 'access list',     visible: true, type: 'usr_mngt', data_row_name: 'access_list'},
                ],
    row_primary_key : '_id',
    formOptions     : {
                    addForm   : true,
                    row_id    : this.row_id,
                    this      : this,
                    result_var_name : 'Members',
                    detail_function : [this, 'callDetail'] 
                    }
                  };

  form_input    : any = {};
  errorLabel    : any = false;
  errorMessage  : any = false;
  totalPage     : 0;
 
  memberDetail  : any = false;
  service       : any ;
  srcDownload   : any ;
  
  constructor(public adminService:AdminService , public sanitizer:DomSanitizer) {
    this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl("");
  }

  ngOnInit() {
    this.firstLoad();
  }

  async firstLoad() {
    try {
      const result: any  = await this.adminService.getAdminGroup();
      this.Members = result;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type

      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
      }
    }

  }

  public async callDetail(data, rowData) {
    try {
      this.memberDetail = this.Members.find(member => member._id == rowData._id);
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async backToHere(obj) {
    obj.memberDetail = false;
    obj.firstLoad();
  }

  onDownload(downloadLint)
  {
    let srcDownload = downloadLint;
    //let srcDownload = "http://localhost:8888/f3/assets/csv/members_report.csv";
    this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl(srcDownload);
    //console.log(this.srcDownload);

    this.getDownloadFileLint();
  }

  public async getDownloadFileLint() {
    try
    {

      let result: any;
      this.service    = this.adminService;
      result= await this.adminService.getDownloadFileLint();
      //console.log(result.result);
      let downloadLint = result.result;
      //console.log(downloadLint);

      //To running other subfunction with together automatically
      this.onDownload(downloadLint);
    } 
    catch (e) 
    {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  } 

  // public async pagination() {
  //   try
  //   {
  //     let result: any;
  //     this.service = this.memberService;
  //     result = await this.memberService.getDownloadFileLint();
  //     //console.log(result.result);
  //     let downloadLint = result.result;
  //     //console.log(downloadLint);

  //     //To running other subfunction with together automatically
  //     this.onDownload(downloadLint);
  //   }
  //   catch (e) {
  //     this.errorLabel = ((<Error>e).message);//conversion to Error type
  //   }  
  // }

}
