import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { AdminService } from '../../../../services/admin/admin.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-admin-group-edit',
  templateUrl: './admin-group.edit.component.html',
  styleUrls: ['./admin-group.edit.component.scss'],
  animations: [routerTransition()]
})

export class AdminGroupEditComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;
  errorLabel : any = false;
  errorMessage : any = false;
  member_status:any;
  activation_status:any;
  
  public loading        : boolean = false;

  public memberStatus: any = [
    {label: 'ACTIVE', value: 'ACTIVE'},
    {label: 'INACTIVE', value: 'INACTIVE'}
  ];

  public groupNameData: any = [];

  public alertGroupname:boolean = false;
  public alertDescription:boolean = false;
  public alertStatus:boolean = false;
  public alertAccessList:boolean = false;

  public usrMngtData: any = [];
  public accessList: any;

  // public usrMngtForm: any = [];

  constructor(public adminService : AdminService) {
  }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    try {
      this.memberStatus.forEach((element, index) => {
        if (element.value == this.detail.status) {
            this.memberStatus[index].selected = 1;
        }
      });

      // let _usr_mngt = this.detail.access_list.usr_mngt.replace(/\|/g, ', ');
      let _access_list = this.detail.access_list;

      await this.adminService.getAccessListApp().then((result) => {
        this.accessList = result;

        for (const key in this.accessList) {
          this.accessList[key].forEach((element) => {
            if(key in _access_list && _access_list[key].includes(element.name)) {
              element.checked = true;
            }
            // this.usrMngtData.push(
            //   {
            //     name : element.name,
            //     checked : _usr_mngt.includes(element.name),
            //   }
            // );
          });
        }



        // result.usr_mngt.forEach((element) => {
        //   this.usrMngtData.push(
        //     {
        //       name : element.name,
        //       checked : _usr_mngt.includes(element.name),
        //     }
        //   );
        // });
      });
    } catch(err) {
      console.log("err", err);
      this.errorLabel = ((<Error>err).message);
      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
      }
    }
  }

  backToDetail() {
    // console.log(this.back);
    this.back[0][this.back[1]]();
  }

  async saveThis() {
    if(!this.detail.group_name  || this.detail.group_name.length == 0){
      this.alertGroupname = true;
    } else {
      this.alertGroupname = false;
    }

    if(!this.detail.description  || this.detail.description.length == 0){
      this.alertDescription = true;
    } else {
      this.alertDescription = false;
    }

    if(!this.detail.status  || this.detail.status.length == 0){
      this.alertStatus = true;
    } else {
      this.alertStatus = false;
    }

    let usrMngtForm: any = {};

    for (const key in this.accessList) {
      // this.accessList[key].forEach((element) => {
        // if(key in _access_list && _access_list[key].includes(element.name)) {
        //   element.checked = true;
        // }
        // this.usrMngtData.push(
        //   {
        //     name : element.name,
        //     checked : _usr_mngt.includes(element.name),
        //   }
        // );
      // });

      this.accessList[key].forEach((element) => {
        if(element.checked == true) {
          if(!(key in usrMngtForm)) usrMngtForm[key] = [];

          usrMngtForm[key].push(element.name);
        }
      });
    }

    // this.usrMngtData.forEach((element) => {
    //   if(element.checked == 1) usrMngtForm.push(element.name);
    // });

    if(Object.keys(usrMngtForm).length === 0 && usrMngtForm.constructor === Object){
      this.alertAccessList = true;
    } else {
      this.alertAccessList = false;
    }
    
    if(!this.alertGroupname && !this.alertDescription && !this.alertStatus && !this.alertAccessList) {
      // this.detail.access_list.usr_mngt = usrMngtForm.join("|");

      let new_form = {
        "description" : this.detail.description,
        "status" : this.detail.status,
        "access_list" : usrMngtForm
      }
      
      try {
        await this.adminService.updateAdminGroup(this.detail.group_name, new_form).then(() => {
          Swal.fire("Group Admin Has Been Edited!")
          .then(() => {
            this.backToDetail();
          });
        }).catch(error => {
          this.errorLabel = ((<Error>error).message);
          let message = this.errorLabel;
          if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
            message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
          }
          Swal.fire({
            icon: 'error',
            title: message,
          });
        });

      } catch (e) {
        this.errorLabel = ((<Error>e).message);//conversion to Error type
        alert("Error");
      }
    }
  }

  checkList(key , index) { 
    if(this.accessList[key][index].checked == true) {
      this.accessList[key][index].checked = false;
    } else if(this.accessList[key][index].checked != true) {
      this.accessList[key][index].checked = true;
    }
  }
}
