import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { OrderhistoryService } from '../../../services/orderhistory/orderhistory.service';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';
import { ActivatedRoute, Router } from '@angular/router';
import { ProgramNameService } from '../../../services/program-name.service';
import * as content from '../../../../assets/json/content.json';

@Component({
  selector: 'app-orderhistorycomplete',
  templateUrl: './orderhistorycomplete.component.html',
  styleUrls: ['./orderhistorycomplete.component.scss'],
  animations: [routerTransition()]
})

export class OrderhistorycompleteComponent implements OnInit {

  Orderhistorycomplete: any = [];
  tableFormat        : TableFormat = {
                                  title           : 'Order History Complete Detail Page',
                                  label_headers   : [
                                    { label: 'Request Date', visible: true, type: 'date', data_row_name: 'request_date' },
                                    { label: 'Approve Date', visible: true, type: 'date', data_row_name: 'approve_date' },
                                    { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
                                    { label: 'ID Toko', visible: true, type: 'string', data_row_name: 'username' },
                                    { label: 'Nama Toko', visible: true, type: 'form_nama_toko', data_row_name: 'member_detail' },
                                    { label: 'Nama Pemilik', visible: true, type: 'form_nama_pemilik', data_row_name: 'member_detail' },
                                    { label: 'Total Quantity', visible: true, type: 'string', data_row_name: 'total_quantity' },
                                    { label: 'Total Price', visible: true, type: 'string', data_row_name: 'sum_total' },
                                    { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
                                  ],
                                  row_primary_key : '_id',
                                  formOptions     : {
                                                    row_id: 'order_id',
                                                    // addMultipleLable : true,
                                                    this  : this,
                                                    result_var_name: 'Orderhistorycomplete',
                                                    detail_function: [this, 'callDetail']
                                                  },
                                  show_checkbox_options: true
  };

  tableFormat2        : TableFormat = {
    title           : 'Order History Complete Detail Page',
    label_headers   : [
      { label: 'Request Date', visible: true, type: 'date', data_row_name: 'request_date' },
      { label: 'Approve Date', visible: true, type: 'date', data_row_name: 'approve_date' },
      { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
      { label: 'Nomor PO', visible: true, type: 'value_po', data_row_name: 'po_no'},
      { label: 'ID Bisnis', visible: true, type: 'form_business_id', data_row_name: 'member_detail' },
      { label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'username' },
      { label: 'Nama Pelanggan', visible: true, type: 'form_nama_pemilik', data_row_name: 'member_detail' },
      { label: 'Total Quantity', visible: true, type: 'string', data_row_name: 'total_quantity' },
      { label: 'Total Price', visible: true, type: 'string', data_row_name: 'sum_total' },
      { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
    ],
    row_primary_key : '_id',
    formOptions     : {
                      row_id: 'order_id',
                      // addMultipleLable : true,
                      this  : this,
                      result_var_name: 'Orderhistorycomplete',
                      detail_function: [this, 'callDetail']
                    },
    show_checkbox_options: true
  };

  tableFormat3        : TableFormat = {
    title           : 'Order History Complete Detail Page',
    label_headers   : [
      { label: 'Request Date', visible: true, type: 'date', data_row_name: 'request_date' },
      { label: 'Approve Date', visible: true, type: 'date', data_row_name: 'approve_date' },
      { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
      { label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'username' },
      { label: 'Nama Program', visible: true, type: 'form_cluster', data_row_name: 'member_detail' },
      { label: 'Nama Entitas', visible: true, type: 'form_nama_toko', data_row_name: 'member_detail' },
      { label: 'Nama PIC', visible: true, type: 'form_nama_pemilik', data_row_name: 'member_detail' },
      { label: 'Total Quantity', visible: true, type: 'string', data_row_name: 'total_quantity' },
      { label: 'Total Price', visible: true, type: 'string', data_row_name: 'sum_total' },
      { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
    ],
    row_primary_key : '_id',
    formOptions     : {
                      row_id: 'order_id',
                      // addMultipleLable : true,
                      this  : this,
                      result_var_name: 'Orderhistorycomplete',
                      detail_function: [this, 'callDetail']
                    },
    show_checkbox_options: true
};

  public contentList : any = (content as any).default;

  form_input    : any = {};
  errorLabel : any = false;
  totalPage: 0;

  orderhistorycompleteDetail: any = false;
  service: any;
  mci_project: any = false;

  errorMessage: any = false;

  programType:any = "";

  constructor(public orderhistoryService:OrderhistoryService, private router: Router, private activatedRoute: ActivatedRoute, private programNameService: ProgramNameService) { }

  // ngOnInit() {
  //   this.orderhistoryService.getOrderhistory().subscribe(data=>{
  //     let result: any = data;
  //     console.log(result.result);
  //     this.Orderhistory = result.result;

  //     //display and convert shoppingcart_id in foreach because type data is ObjectID
  //     this.Orderhistory.forEach((data, index)=>{this.Orderhistory[index].shoppingcart_id = this.Orderhistory[index].shoppingcart_id.$oid})
  //   });
  // }

  ngOnInit() {
    this.firstLoad();
    
  }

  public ngOnDestroy() {
    const programName = localStorage.getItem('programName');
    this.programNameService.setData(programName);
  }

  async firstLoad(){
    try{
      let paramId = this.activatedRoute.snapshot.queryParamMap.get('order_id');
      let paramAppLabel = this.activatedRoute.snapshot.queryParamMap.get('app_label');

      if(paramId && paramAppLabel) {
        // localStorage.setItem('programName', paramAppLabel);
        this.programNameService.setData(paramAppLabel);
        
        const resultDetail: any = await this.orderhistoryService.getOrderDetail(paramId);
        if(resultDetail && resultDetail.order_id) this.orderhistorycompleteDetail = resultDetail;
      }

      const program = localStorage.getItem('programName');
      let _this = this;
      this.contentList.filter(function(element) {
        if(element.appLabel == program) {
          if(element.type == "reguler") {
              _this.mci_project = true;
              _this.programType = "reguler";
          } else if(element.type == "custom_kontraktual") {
            _this.mci_project = false;
            _this.programType = "custom_kontraktual";
          } else {
              _this.mci_project = false;
              _this.programType = "custom";
          }
        }
      });
      
      this.service    = this.orderhistoryService;
      let result: any  = await this.orderhistoryService.getOrderhistorycompleteLint();
      this.totalPage = result.total_page;
      console.log(result);
      
      this.Orderhistorycomplete = result.values;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type

      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
				this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
			}
    }
  }

  public async callDetail(data, rowData){
    this.orderhistorycompleteDetail = this.Orderhistorycomplete.find(order => order.order_id == rowData.order_id);

    // this.router.navigate(['administrator/orderhistoryallhistoryadmin/edit'], {queryParams: {id:orderhistory_id}})
  }

  public async backToHere(obj){
    const programName = localStorage.getItem('programName');
    obj.programNameService.setData(programName);
    obj.orderhistorycompleteDetail = false;
    obj.router.navigate([]).then(() => {
      obj.firstLoad();
    });
  }


}
