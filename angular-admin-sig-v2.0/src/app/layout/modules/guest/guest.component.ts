import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { MemberService } from '../../../services/member/member.service';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';

@Component({
  selector: 'app-guest',
  templateUrl: './guest.component.html',
  styleUrls: ['./guest.component.scss'],
  animations: [routerTransition()]
})

export class GuestComponent implements OnInit {

  Guest       :any = [];
  tableFormat        : TableFormat = {
                                  title           : 'Active Guests Detail',
                                  label_headers   : [
                                    {label: 'Guest ID', visible: true, type: 'string', data_row_name: '_id'},
                                    {label: 'Permission', visible: true, type: 'string', data_row_name: 'permission'},
                                    {label: 'Guest Name', visible: true, type: 'string', data_row_name: 'full_name'},
                                    {label: 'Membership', visible: true, type: 'string', data_row_name: 'membership'},
                                    {label: 'User Type', visible: true, type: 'string', data_row_name: 'type'},
                                    {label: 'Join Date', visible: true, type: 'date', data_row_name: 'created_date'},
                                  ],
                                  row_primary_key : '_id',
                                  formOptions     : {
                                                    row_id: '_id',
                                                    this  : this,
                                                    result_var_name: 'Guest',
                                                    detail_function: [this, 'callDetail']
                                                  }
  };
  form_input    : any = {};
  errorLabel : any = false;
  totalPage:0;
  // table_title   :any = "Active guest detail";
  // table_headers :any = ["Join at","Full Name","User ID",'Point Balance', 'Type', "Organization ID"];
  // table_rows    :any = ["created_date","full_name","user",'point_balance', 'type', 'org_id'];
  // row_id        :any = "_id";
  // errorLabel : any = false;
  // formOptions   :any = {
  //                     addForm   : true,
  //                     row_id    : this.row_id,
  //                     this      : this,
  //                     result_var_name : 'Guest',
  //                     detail_function : [this,'callDetail']
  //                   }
  guestDetail: any = false;
  service     : any ;


  constructor(public memberService:MemberService) {}

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    try {
      // let params = {
      //   'type':'guest'
      // }
      this.service    = this.memberService;
      const result: any  = await this.memberService.getGuestLint();
      //console.log(result.result);
      this.totalPage = result.result.total_page;
      this.Guest = result.result.values;
      console.log("Guest",this.Guest);
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

  }

  public async callDetail(_id) {
    
    try {
      this.service    = this.memberService;
      let result: any = await this.memberService.detailMember(_id);
      //console.log(result);
      this.guestDetail = result.result[0];
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async backToHere(obj) {
    obj.guestDetail = false;
  }

}
