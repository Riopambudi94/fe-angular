import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { PromotionService } from '../../../services/promotion/promotion.service';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-promo-statistic',
  templateUrl: './admin-promo-statistic.component.html',
  styleUrls: ['./admin-promo-statistic.component.scss']
})
export class AdminPromoStatisticComponent implements OnInit {

  EscrowData: any = [];
  Pointstransaction: any = [];
  tableFormat        : TableFormat = {
                                  title           : 'Promo Statistic Report Page',
                                  label_headers   : [
                                    {label: 'Created Date', visible: true, type: 'date', data_row_name: 'created_date'},
                                    {label: 'Promo Code', visible: true, type: 'string', data_row_name: 'promo_code'},
                                    {label: 'Promo Used', visible: true, type: 'string', data_row_name: 'promo_used'},
                                    {label: 'Max Member Used', visible: true, type: 'string', data_row_name: 'max_member_used'},
                                    {label: 'Max Transaction', visible: true, type: 'string', data_row_name: 'max_transaction'},
                                    {label: 'User Member Transaction', visible: true, type: 'string', data_row_name: 'use_member_transaction'},
                                    {label: 'User Reach Transaction', visible: true, type: 'string', data_row_name: 'use_reach_transaction'},
                                    {label: 'Used By Member', visible: true, type: 'string', data_row_name: 'used_by_member'},
                                    // {label: 'Status', visible: true, type: 'string', data_row_name: 'status'},
                    
                                  ],
                                  row_primary_key : '_id',
                                  formOptions     : {
                                                    row_id: '_id',
                                                    this  : this,
                                                    result_var_name: 'EscrowData',
                                                    detail_function: [this, 'callDetail']
                                                  }
  };
  form_input    : any = {};
  errorLabel : any = false;
  totalPage  = 0;
  page = 1;
  pageSize = 50;
  pointstransactionDetail: any = false;
  service: any;
  pages: any;
  currentPage= 1;
  pageNumbering: any[];
  total_page: any;
  orderBy: any;
  searchCallback: any;
  onSearchActive: boolean;
  showLoading: boolean;
  options: any;
  table_data: any;
  pageLimits: any;

  constructor(public PromotionService:PromotionService, private router: Router) { }

  ngOnInit() {
    this.firstLoad();

  }
  
  async firstLoad(){
    // console.log(' admin payment')
    try{
      this.service    = this.PromotionService;
      let result: any = await this.PromotionService.getPromotionStatisticLint();
      console.log("result", result)
      this.totalPage  = result.result.total_page
      this.pages = result.result.total_page
      console.log(" pages",this.pages)
      this.EscrowData = result.result.values;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }
  
  public async callDetail(pointstransaction_id){
    // try{
    //   let result: any;
    //   this.service    = this.EscrowTransactionService;
    //   result          = await this.EscrowTransactionService.detailEscrowtransaction(pointstransaction_id);
    //   console.log(result);

    //   this.pointstransactionDetail = result.result[0];
    //   console.log('ini',this.pointstransactionDetail)
      
    //   this.router.navigate(['administrator/escrow-transaction/detail'],  {queryParams: {id: pointstransaction_id }})
      
    // } catch (e) {
    //   this.errorLabel = ((<Error>e).message);//conversion to Error type
    // }
  }

  public async backToHere(obj){
    obj.pointstransactionDetail = false;
  }
  addMonths(date, months) {
    date.setMonth(date.getMonth() + months);
    return date;
  }
}

