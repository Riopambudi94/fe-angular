import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EwalletSalesReportComponent } from './ewallet-sales-report.component';

describe('EwalletSalesReportComponent', () => {
  let component: EwalletSalesReportComponent;
  let fixture: ComponentFixture<EwalletSalesReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EwalletSalesReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EwalletSalesReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
