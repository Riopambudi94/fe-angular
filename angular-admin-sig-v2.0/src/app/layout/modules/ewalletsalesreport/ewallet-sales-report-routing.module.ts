import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EwalletSalesReportComponent } from './ewallet-sales-report.component'
// import { BastComponent} from './bast/bast.component'


const routes: Routes = [
  {
    path: '', component: EwalletSalesReportComponent
},
// {
//   path:'bast', component: BastComponent
// }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EwalletSalesReportRoutingModule { }
