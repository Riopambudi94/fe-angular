import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrderhistorysummaryComponent } from './orderhistorysummary.component';
import { OrderhistorysummaryDetailComponent } from './detail/orderhistorysummary.detail.component';
import { OrderhistorysummaryEditComponent } from './edit/orderhistorysummary.edit.component';


const routes: Routes = [
  {
      path: '', component: OrderhistorysummaryComponent
  },
  {
    path:'detail', component: OrderhistorysummaryDetailComponent
  },
  {
    path:'edit', component: OrderhistorysummaryEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderhistorysummaryRoutingModule { }
