import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderhistorysummaryRoutingModule } from './orderhistorysummary-routing.module';
import { OrderhistorysummaryComponent } from './orderhistorysummary.component';
import { PageHeaderModule } from './../../../shared';
import { FormBuilderTableComponent }  from '../../../component-libs/form-builder-table/form-builder-table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { OrderhistorysummaryDetailComponent } from './detail/orderhistorysummary.detail.component';
import { OrderhistorysummaryEditComponent } from './edit/orderhistorysummary.edit.component';


@NgModule({
  imports: [
    CommonModule, 
    OrderhistorysummaryRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule],
  declarations: [OrderhistorysummaryComponent, OrderhistorysummaryDetailComponent, OrderhistorysummaryEditComponent]
})
export class OrderhistorysummaryModule { }
