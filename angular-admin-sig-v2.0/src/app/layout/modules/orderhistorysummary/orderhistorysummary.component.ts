import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { OrderhistoryService } from '../../../services/orderhistory/orderhistory.service';
import { Form } from '@angular/forms';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';

@Component({
  selector: 'app-orderhistorysummary',
  templateUrl: './orderhistorysummary.component.html',
  styleUrls: ['./orderhistorysummary.component.scss'],
  animations: [routerTransition()]
})

export class OrderhistorysummaryComponent implements OnInit {

  Orderhistorysummary: any = [];
  tableFormat        : TableFormat = {
                                  title           : 'Summary of Order History',
                                  label_headers   : [
                                    {label: 'Order date', visible: true, type: 'date', data_row_name: 'created_date'},
                                    {label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id'},
                                    {label: 'Shoppingcart ID', visible: true, type: 'string', data_row_name: 'shoppingcart_id'},
                                    {label: 'Name', visible: true, type: 'string', data_row_name: 'name'},
                                    {label: 'Total Price', visible: true, type: 'number', data_row_name: 'total_price'},
                                    // {label: 'Total Quantity', visible: false, type: 'number', data_row_name: 'total_quantity'},
                                    {
                                      label   : 'Status',
                                      options:['order', 
                                              'checkout', 
                                              'incomplete', 
                                              'paid', 
                                              'pending', 
                                              'waiting for confirmation'],
                                      type    : 'list',
                                      visible : false,
                                      data_row_name: 'status'
                                    },
                                  ],
                                  row_primary_key : '_id',
                                  formOptions     : {
                                                    row_id: '_id',
                                                    this  : this,
                                                    result_var_name: 'Orderhistorysummary',
                                                    detail_function: [this, 'callDetail']
                                                  },
                                  show_checkbox_options: true
  };

  form_input    : any = {};
  errorLabel    : any = false;
  
  orderhistorysummaryDetail: any = false;
  service: any;
  constructor(public orderhistoryService: OrderhistoryService) { }

  // ngOnInit() {
  //   this.orderhistoryService.getOrderhistory().subscribe(data=>{
  //     let result: any = data;
  //     console.log(result.result);
  //     this.Orderhistory = result.result;

  //     //display and convert shoppingcart_id in foreach because type data is ObjectID
  //     this.Orderhistory.forEach((data, index)=>{this.Orderhistory[index].shoppingcart_id = this.Orderhistory[index].shoppingcart_id.$oid})
  //   });
  // }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    try {
      this.service = this.orderhistoryService;
      let result: any = await this.orderhistoryService.getOrderhistorysummaryLint();
      // console.log(result);

      this.Orderhistorysummary = result.result;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async callDetail(orderhistory_id) {
    try {
      let result: any;
      this.service = this.orderhistoryService;
      result = await this.orderhistoryService.detailOrderhistorysummary(orderhistory_id);
      console.log(result);

      this.orderhistorysummaryDetail = result.result[0];

    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async backToHere(obj) {
    obj.orderhistorysummaryDetail = false;
  }

}


