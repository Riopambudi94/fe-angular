
import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { CategoryService } from '../../../services/category/category.service';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss'],
  animations: [routerTransition()]
})

export class CategoryComponent implements OnInit {

  Category: any = [];
  tableFormat: TableFormat = {
    title: 'Category',
    label_headers: [
      { label: 'Category ID', visible: true, type: 'string', data_row_name: '_id' },
      { label: 'Name', visible: true, type: 'string', data_row_name: 'name' },
      { label: 'Code', visible: true, type: 'string', data_row_name: 'code' },
      { label: 'Key', visible: true, type: 'string', data_row_name: 'key' },
      { label: 'Type', visible: true, type: 'string', data_row_name: 'type' }
    ],
    row_primary_key: '_id',
    formOptions: {
      row_id: '_id',
      addForm: true,
      this: this,
      result_var_name: 'Category',
      detail_function: [this, 'callDetail'],
    }
  };
  form_input: any = {};
  row_id: any = "_id";
  errorLabel: any = false;
  errorMessage  : any = false;
  cancel: boolean;
  progressBar: any;
  service: CategoryService;
  categoryDetail: any = false;

  constructor(public categoryService: CategoryService) { }

  ngOnInit() {
    this.firstLoad();
  }

  async firstLoad() {
    try {
      let result: any;
      this.service = this.categoryService;
      result = await this.categoryService.getCategoryLint();
      this.Category = result.values;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type

			if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
				this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
			}
    }
  }

  public async callDetail(_id) {
    try {
      let result: any;
      result = await this.categoryService.getCategoryByID(_id);
      console.log(result);
      if (result && !result.error) {
        this.categoryDetail = result.values[0];
      }
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type

			if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
				this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
			}
    }
  }

  public async backToHere(obj) {
    obj.categoryDetail = false;
    // delete obj.prodDetail;

  }

  updateProgressBar(value) {
    this.progressBar = value;
  }

  cancelThis() {
    this.cancel = !this.cancel;
  }
}
