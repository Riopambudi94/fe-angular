import { Component, OnInit, Input } from '@angular/core';
import Swal from 'sweetalert2';
import { routerTransition } from '../../../../router.animations';
import { CategoryService } from '../../../../services/category/category.service';

@Component({
  selector: 'app-category-detail',
  templateUrl: './category.detail.component.html',
  styleUrls: ['./category.detail.component.scss'],
  animations: [routerTransition()]
})

export class CategoryDetailComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;
  errorLabel    : any = false;
  edit:boolean = false;
  toggleDelete:boolean;
  
  constructor(public categoryService:CategoryService) {
  }

  ngOnInit() {
    this.firstLoad();
    
  }

  async firstLoad(){
    
    
  }

  editThis(){
    //console.log(this.edit );
    this.edit = !this.edit;
    //console.log(this.edit );
  }
  backToTable(){
    //console.log(this.back);
    this.back[1](this.back[0]);
  }

  async deleteThis(){
    try{
      let delResult: any = await this.categoryService.deleteCategory(this.detail);
      //console.log(delResult);
      if(!delResult.error || delResult.error==false){
          Swal.fire("Category has been deleted!").then(() => {
            this.back[0].categoryDetail=false;
            this.back[0].firstLoad();
          });
          // delete this.back[0].categoryDetail;
      } 
    } catch (e) {
        this.errorLabel = ((<Error>e).message);//conversion to Error type

        let message = this.errorLabel;
        if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
          message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
        }
        Swal.fire({
          icon: 'error',
          title: message,
        })
    }      
    
  }

  toggleDeleteConfirm(l: boolean) {
		this.toggleDelete = l;
	}
}
