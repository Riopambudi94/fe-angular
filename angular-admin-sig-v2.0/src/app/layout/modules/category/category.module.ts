import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoryRoutingModule } from './category-routing.module';
import { CategoryComponent } from './category.component';
import { PageHeaderModule } from '../../../shared';
import { FormBuilderTableComponent }  from '../../../component-libs/form-builder-table/form-builder-table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { CategoryAddComponent } from './add/category.add.component';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormInputComponent } from '../../../component-libs/form-input/form-input.component';
import { CategoryDetailComponent } from './detail/category.detail.component';
import { CategoryEditComponent } from './edit/category.edit.component';
// import { NgxEditorModule } from 'ngx-editor';
import { BsComponentModule } from '../bs-component/bs-component.module';


@NgModule({
  imports: [
    CommonModule, 
    CategoryRoutingModule, 
    PageHeaderModule, 
    FormsModule, 
    FormBuilderTableModule, 
    ReactiveFormsModule,
    // NgxEditorModule,
    BsComponentModule,
    NgbModule,
  ],
  declarations: [CategoryComponent, CategoryAddComponent, CategoryDetailComponent, CategoryEditComponent]
})
export class CategoryModule { }
