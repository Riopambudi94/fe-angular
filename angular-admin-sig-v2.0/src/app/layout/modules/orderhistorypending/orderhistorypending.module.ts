import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderhistorypendingRoutingModule } from './orderhistorypending-routing.module';
import { OrderhistorypendingComponent } from './orderhistorypending.component';
import { PageHeaderModule } from './../../../shared';
import { FormBuilderTableComponent }  from '../../../component-libs/form-builder-table/form-builder-table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { OrderhistorypendingDetailComponent } from './detail/orderhistorypending.detail.component';
import { OrderhistorypendingEditComponent } from './edit/orderhistorypending.edit.component';





@NgModule({
  imports: [
    CommonModule, 
    OrderhistorypendingRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule],
  declarations: [OrderhistorypendingComponent,OrderhistorypendingDetailComponent,OrderhistorypendingEditComponent]
})
export class OrderhistorypendingModule { }
