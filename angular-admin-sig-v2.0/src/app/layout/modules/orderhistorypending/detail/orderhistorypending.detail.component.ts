import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { OrderhistoryService } from '../../../../services/orderhistory/orderhistory.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-orderhistorypending-detail',
  templateUrl: './orderhistorypending.detail.component.html',
  styleUrls: ['./orderhistorypending.detail.component.scss'],
  animations: [routerTransition()]
})

export class OrderhistorypendingDetailComponent implements OnInit {
  orderhistoryDetail: any;
  service;
  errorLabel;

  constructor(
    private orderhistoryService: OrderhistoryService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.firstLoad();
  }

  async firstLoad() {
    this.route.queryParams.subscribe(async (params) => {
      let historyID = params.id;
      this.loadHistoryDetail(historyID)


    })


  }

  private async loadHistoryDetail(historyID: string) {
    try {
      this.service = this.orderhistoryService;
      let result: any = await this.orderhistoryService.detailOrderhistory(historyID);
      // console.log("result", result);
      this.orderhistoryDetail = result.result[0];

      // this.orderhistoryDetail.merchant = true;
    }
    catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

}
