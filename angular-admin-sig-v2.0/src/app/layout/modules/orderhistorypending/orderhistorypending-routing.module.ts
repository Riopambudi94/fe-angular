import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrderhistorypendingComponent } from './orderhistorypending.component';
import { OrderhistorypendingDetailComponent } from './detail/orderhistorypending.detail.component';

// import { OrderhistorysummaryDetailComponent } from './detail/orderhistorysummary.detail.component';
// import { OrderhistorysummaryEditComponent } from './edit/orderhistorysummary.edit.component';


const routes: Routes = [
  {
      path: '', component: OrderhistorypendingComponent
  },
  // {
  //   path:'detail', component: OrderhistorysuccessDetailComponent
  // },
  // {
  //   path:'edit', component: OrderhistorysummaryEditComponent
  // }

  {
    path: 'edit', component: OrderhistorypendingDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderhistorypendingRoutingModule { }
