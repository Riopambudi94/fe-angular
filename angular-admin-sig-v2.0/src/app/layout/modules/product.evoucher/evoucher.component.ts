import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { ProductService } from '../../../services/product/product.service';
import { EVoucherService } from '../../../services/e-voucher/e-voucher.service';
import { FormOptions, TableFormat } from '../../../object-interface/common.object';
import { Router, ActivatedRoute } from '@angular/router';
import { element } from 'protractor';
import { async } from 'q';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-evoucher',
  templateUrl: './evoucher.component.html',
  styleUrls: ['./evoucher.component.scss'],
  animations: [routerTransition()]
})

export class EvoucherComponent implements OnInit {

  Evouchers: any = [];
  tableFormat: TableFormat = {
    title: 'E-Voucher Summary Report',
    label_headers: [
      // {label: 'Product Code', visible: true, type: 'string', data_row_name: 'ev_product_code'},
      { label: 'Voucher Name', visible: true, type: 'string', data_row_name: 'product_name' },
      { label: 'Voucher Code', visible: true, type: 'string', data_row_name: 'product_code' },
      { label: 'Expiry Date', visible: true, type: 'date', data_row_name: 'expiry_date' },
      { label: 'Available Stock', visible: true, type: 'string', data_row_name: 'qty_available' },
      { label: 'Bought by User', visible: true, type: 'string', data_row_name: 'qty_sold' },
      { label: 'Used by User', visible: true, type: 'string', data_row_name: 'qty_used' },
      { label: 'Inactive Stock', visible: true, type: 'string', data_row_name: 'qty_inactive' },
      { label: 'Expired Stock', visible: true, type: 'string', data_row_name: 'qty_expired' },
      // { label: 'Voucher Type', visible: false, type: 'string', data_row_name: 'redeem_type' },

    ],
    row_primary_key: '_id',
    formOptions: {
      addFormGenerate: false,
      row_id: '_id',
      this: this,
      result_var_name: 'Evouchers',
      detail_function: [this, 'callDetail'],
    },
    show_checkbox_options: true
  };
  form_input: any = {};
  errorLabel: any = true;
  errorMessage  : any = false;
  totalPage: 0;

  popUpForm: boolean = false;

  data: any = []

  evoucherDetail: any = false;
  service: any;
  selectedFile = null;
  progressBar: number = 0;
  cancel: boolean = false;
  errorFile: any = false;
  inputedVoucher: number = 0;
  approveAll: boolean = false;
  evoucherID;


  constructor(public evoucherService: EVoucherService, public productService: ProductService, private router: Router, private route: ActivatedRoute) {


  }

  ngOnInit() {
    this.firstLoad();
  }


  //

  async firstLoad() {
    try{
      console.log("test");
      this.service = this.evoucherService;
      const result: any = await this.evoucherService.getEvoucherSummaryReport();
      this.totalPage = result.total_page;
      this.Evouchers = result.values;
    } catch (e) {
      console.log("kena catch");
      this.errorLabel = ((<Error>e).message);

      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
      }
    }

    // this.service = this.evoucherService
    // this.route.queryParams.subscribe(async (params) => {
    //   this.evoucherID = params.id;
    //   let result = await this.evoucherService.getEvoucherSummaryReport();
    //   this.totalPage = result.total_page;
    //   this.Evouchers = result.values;

      //NEED TO DISABLE THIS FOR A WHILE, CANNOT ACTIVATE VOUCHER IF THERE IS NO EXPIRY DATE, REFACTOR IS THE BEST CHOICE
      // this.Evouchers.forEach((el) =>{
      //   if(el.redeem_type == 'voucher_link'){
      //     el.expiry_date = ''
      //   }
      // })
      


      // console.log(this.Evouchers)
    // })
  }



  // // Get the button that opens the modal
  // var btn = document.getElementById("myBtn");

  // // Get the <span> element that closes the modal
  // var span = document.getElementsByClassName("close")[0];

  popUpClose() {
    this.popUpForm = false;
  }

  // When the user clicks anywhere outside of the modal, close it
  // window.onclick = function (event) {
  //   if (event.target == modal) {
  //     modal.style.display = "none";
  //   }
  // }

  openDetail(evoucher){
    // let ev_prod = this.Evouchers.ev_product_code
    this.data = evoucher
    console.log("result", evoucher)
    this.popUpForm = true;
    // let expiry_date = this.expiry_date
  }

  public async callDetail(_id, evoucherData) {
    // this.openDetail(evoucherData)
    // console.log("detail", evoucherData, _id)
  }

  public async backToHere(obj) {
    obj.evoucherDetail = false;

  }

  onFileSelected(event) {
    this.errorFile = false;
    let fileMaxSize = 3000000;
    // let say 3Mb
    Array.from(event.target.files).forEach((file: any) => {
      if (file.size > fileMaxSize) {
        this.errorFile = 'Maximum File is 3 MB';
      }
    });
    this.selectedFile = event.target.files[0];

  }

  updateProgressBar(value) {
    this.progressBar = value;
  }

  cancelThis() {
    // this.cancel = !this.cancel;
    this.cancel = true;
  }

  async onUpload() {
    try {
      this.cancel = false;
      if (this.selectedFile) {
        await this.evoucherService.upload(this.selectedFile, this);
        alert("File Has Been Uploaded Successfully!")
      }
    } catch (e) {
      this.errorLabel = ((<Error>e).message);
      // conversion to Error type

      let message = this.errorLabel;
      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
      }
      Swal.fire({
        icon: 'error',
        title: message,
      });
    }
  }

  approveAllClicked(data){
    this.approveAll = true;
    this.approval(data);
  }

  async approval(evoucherData) {
    console.log(evoucherData);
    if (evoucherData == undefined) {
      return false;
    }
    
    let totalVoucher;
    let expiry_date = new Date(evoucherData.expiry_date)

    let month = (expiry_date.getMonth()+ 1).toString().padStart(2,"0");
    let day = (expiry_date.getDate() + 0).toString().padStart(2,"0")
    let stringDate = expiry_date.getFullYear()+'-'+month+'-'+day;

    if(this.inputedVoucher != 0 && this.approveAll == false){
      totalVoucher = this.inputedVoucher
    }
    else if(this.approveAll == true){
      this.approveAll = false;
      totalVoucher = evoucherData.qty_inactive
    }

    let payloadData = {
      product_code: evoucherData.ev_product_code,
      expiry_date:stringDate,
      qty: totalVoucher,
      method: 2
    };

    this.service = this.evoucherService;
    let result = await this.evoucherService.approvedEvoucher(payloadData);
    if (result.result.status == 'success') {
      alert("the Current Data Below has been approved");
      this.firstLoad();
    }
  }

  async disabled(evoucherData){
    console.log(evoucherData);
    if (evoucherData == undefined) {
      return false;
    }

    let totalVoucher;
    
    let expiry_date = new Date(evoucherData.expiry_date)

    let month = (expiry_date.getMonth()+ 1).toString().padStart(2,"0");
    let day = (expiry_date.getDate() + 0).toString().padStart(2,"0")
    let stringDate = expiry_date.getFullYear()+'-'+month+'-'+day;
    
    if(this.inputedVoucher != 0){
      totalVoucher = this.inputedVoucher
    }
    else if(this.inputedVoucher == 0){
      totalVoucher = evoucherData.qty_inactive
    }

    console.log(this.inputedVoucher);


    let payloadData = {
      product_code: evoucherData.ev_product_code,
      expiry_date:stringDate,
      qty: totalVoucher,
      method: 1
    };

    console.log("payload", payloadData)
    this.service = this.evoucherService;
    let result = await this.evoucherService.approvedEvoucher(payloadData);
    if (result.result.status == 'success') {
      alert("The stock has been inactive");
      this.firstLoad();
    }
  }
}

interface standardDropDown {
  label
  value
  selected
}
