import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPromoUsageComponent } from './admin-promo-usage.component';

describe('AdminPromoUsageComponent', () => {
  let component: AdminPromoUsageComponent;
  let fixture: ComponentFixture<AdminPromoUsageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPromoUsageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPromoUsageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
