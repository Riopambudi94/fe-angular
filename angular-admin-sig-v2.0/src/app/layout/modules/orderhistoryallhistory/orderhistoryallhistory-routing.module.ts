import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrderhistoryallhistoryComponent } from './orderhistoryallhistory.component';
import { OrderhistoryallhistoryDetailComponent } from './detail/orderhistoryallhistory.detail.component';
import { ReceiptComponent } from './receipt/receipt.component';
// import { OrderhistorysummaryDetailComponent } from './detail/orderhistorysummary.detail.component';
// import { OrderhistorysummaryEditComponent } from './edit/orderhistorysummary.edit.component';


const routes: Routes = [
  {
      path: '', component: OrderhistoryallhistoryComponent
  },
  // {
  //   path:'detail', component: OrderhistorysummaryDetailComponent
  // },
  {
    path:'edit', component: OrderhistoryallhistoryDetailComponent
  },
  {
    path: 'receipt', component: ReceiptComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderhistoryallhistoryRoutingModule { }
