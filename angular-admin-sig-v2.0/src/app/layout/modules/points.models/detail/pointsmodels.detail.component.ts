import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { PointsModelsService } from '../../../../services/points.models/pointsmodels.service';
import { del } from 'selenium-webdriver/http';

@Component({
  selector: 'app-points-models-detail',
  templateUrl: './pointsmodels.detail.component.html',
  styleUrls: ['./pointsmodels.detail.component.scss'],
  animations: [routerTransition()]
})

export class PointsModelsDetailComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  public ruleType: any = [];
  public calcType: any = [];
  public transactionType: any = [];

  rule : any = false;
  calculation : any = false;
  transaction : any = false;

  edit: boolean = false;
  errorLabel : any = false;
  constructor(public pointsmodelsService: PointsModelsService) {}

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad(){

  }

  editThis() {
    // console.log(this.edit );
    this.getParameters();
    // console.log(this.edit );
  }
  backToTable() {
    // console.log(this.back);
    this.back[1](this.back[0]);
  }

  async getParameters(){
    try{
      this.rule = await this.pointsmodelsService.getRuleType();
      this.rule.result.forEach((element) => {
        this.ruleType.push({label:element.description, value:element.value});
      });
      this.calculation = await this.pointsmodelsService.getCalcType();
      this.calculation.result.forEach((element) => {
        this.calcType.push({label:element.description, value:element.value});
      });
      this.transaction = await this.pointsmodelsService.getTransactionType();
      this.transaction.result.forEach((element) => {
        this.transactionType.push({label:element.description, value:element.value});
      });
      
      this.edit = !this.edit;
    }
    catch (e){
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  async deleteThis(){
    try {
      const delResult: any = await this.pointsmodelsService.delete(this.detail);
      console.log(delResult);
      if (delResult.error == false) {
        console.log(this.back[0]);
        this.back[0].pointsgroupDetail = false;
        this.back[0].firstLoad();
      // delete this.back[0].prodDetail;
      }
    } catch (error) {
      throw new TypeError(error.error.error);
    }
  } catch (e) {
    this.errorLabel = ((<Error>e).message);//conversion to Error type
  }

}
