import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PromoCampaignComponent } from './promo-campaign.component';
import { PromoCampaignAddComponent } from './add/promo-campaign-add.component';
import { PromoCampaignDetailComponent } from './detail/promo-campaign-detail.component';

const routes: Routes = [
  {
      path: '', component: PromoCampaignComponent,

  },
  {
      path:'add', component: PromoCampaignAddComponent
  },
  {
    path:'detail', component: PromoCampaignDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PromoCampaignRoutingModule { }
