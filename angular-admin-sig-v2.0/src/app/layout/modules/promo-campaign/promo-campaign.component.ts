import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { BroadcastService } from '../../../services/notification.broadcast/broadcast.service';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';
import { PromotionService } from '../../../services/promotion/promotion.service'

@Component({
  selector: 'app-promo-campaign',
  templateUrl: './promo-campaign.component.html',
  styleUrls: ['./promo-campaign.component.scss']
})
export class PromoCampaignComponent implements OnInit {
  Broadcast       : any = [];
  tableFormat        : TableFormat = {
                                  title           : 'Promo Campaigns',
                                  label_headers   : [
                                    {label: 'Promo Code', visible: true, type: 'string', data_row_name: 'promo_code'},
                                    {label: 'Promo Name', visible: true, type: 'string', data_row_name: 'promo_name'},
                                      // {label: 'Setting ID', visible: false, type: 'string', data_row_name: 'setting_id'},
                                      {label: 'Active', visible: true, type: 'string', data_row_name: 'active'},
                                      // {label: 'Template ID', visible: false, type: 'string', data_row_name: 'template_id'},
                                      {label: 'Status',
                                          options: [
                                            'active',
                                            'inactive',
                                          ],
                                          visible: true, type: 'list', data_row_name: 'active'},
                                      ],
                                  row_primary_key : '_id',
                                  formOptions     : {
                                                    row_id: '_id',
                                                      addForm: true,
                                                    this  : this,
                                                    result_var_name: 'Broadcast',
                                                    detail_function: [this, 'callDetail']
                                                  }
  };
  form_input    : any = {};
  errorLabel : any = false;

  broadcastDetail: any = false;
  service     : any ;

  constructor(public promotionService:PromotionService) {}

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    try {
      this.service    = this.promotionService;
      const result: any  = await this.promotionService.getOngoingPromo();
      this.Broadcast = result.result;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

  }

  public async callDetail(_id) {
    
    try {
      this.service    = this.promotionService;
      // let result: any = await this.broadcastService.detailBroadcast(_id);
      // this.broadcastDetail = result.result;
      // console.log('detail', this.broadcastDetail);
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async backToHere(obj) {
    obj.broadcastDetail = false;
  }

}