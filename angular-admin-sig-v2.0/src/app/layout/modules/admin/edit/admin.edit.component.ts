import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { AdminService } from '../../../../services/admin/admin.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-admin-edit',
  templateUrl: './admin.edit.component.html',
  styleUrls: ['./admin.edit.component.scss'],
  animations: [routerTransition()]
})

export class AdminEditComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;
  errorLabel : any = false;
  member_status:any;
  activation_status:any;
  
  public loading        : boolean = false;

  public memberStatus: any = [
    {label: 'ACTIVE', value: 'ACTIVE'},
    {label: 'INACTIVE', value: 'INACTIVE'}
  ];

  public groupNameData: any = [];

  public alertFullname:boolean = false;
  public alertEmail:boolean = false;
  public alertPassword:boolean = false;
  public alertGroupname:boolean = false;
  public alertStatus:boolean = false;

  errorMessage  : any = false;

  constructor(public adminService : AdminService) {
  }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    console.log("this.detail",this.detail);
    try {
      this.memberStatus.forEach((element, index) => {
        if (element.value == this.detail.status) {
            this.memberStatus[index].selected = 1;
        }
      });

      await this.adminService.getAdminGroup().then((result) => {
        result.forEach((element, index) => {
          this.groupNameData.push(
            {
              label : element.group_name,
              value: element.group_name,
            }
          );
          if (element.group_name == this.detail.group_name) {
            this.groupNameData[index].selected = 1;
            console.log("this.groupNameData[index].selected", this.groupNameData[index].selected);
          }
        });
      });
    } catch(err) {
      this.errorLabel = ((<Error>err).message);//conversion to Error type

      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
      }
    }
  }

  backToDetail() {
    // console.log(this.back);
    this.back[0][this.back[1]]();
  }

  async saveThis() {
    if(!this.detail.email  || this.detail.email.length == 0){
      this.alertEmail = true;
    } else {
      this.alertEmail = false;
    }
    
    if(!this.detail.password || this.detail.password.length == 0){
      this.alertPassword = true;
    } else {
      this.alertPassword = false;
    }
    
    if(!this.detail.group_name  || this.detail.group_name.length == 0){
      this.alertGroupname = true;
    } else {
      this.alertGroupname = false;
    }

    if(!this.detail.status  || this.detail.status.length == 0){
      this.alertStatus = true;
    } else {
      this.alertStatus = false;
    }
    
    if(!this.detail.full_name  || this.detail.full_name.length == 0){
      this.alertFullname = true;
    } else {
      this.alertFullname = false;
    }
    
    if(!this.alertFullname && !this.alertPassword && !this.alertEmail && !this.alertPassword && !this.alertGroupname) {
      let new_form = {
        "full_name" : this.detail.full_name,
        "email" : this.detail.email,
        "group_name" : this.detail.group_name,
        "status" : this.detail.status,
      }
      
      try {
        await this.adminService.updateAdmin(this.detail.username, new_form).then(() => {
          Swal.fire("Admin Has Been Edited!")
          .then(() => {
            this.backToDetail();
          });
        }).catch(error => {
          this.errorLabel = ((<Error>error).message);
          let message = this.errorLabel;
          if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
            message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
          }
          Swal.fire({
            icon: 'error',
            title: message,
          })
          console.log(error);
        });
      } catch (e) {
        this.errorLabel = ((<Error>e).message);//conversion to Error type
        alert("Error");
      }
    }
  }
}
