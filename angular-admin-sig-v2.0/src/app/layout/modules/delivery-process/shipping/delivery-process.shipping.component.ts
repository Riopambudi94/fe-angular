import { Component, OnInit, Input } from '@angular/core';
import { HttpClient, HttpEventType, HttpHeaders, HttpParams } from '@angular/common/http';
import { routerTransition } from '../../../../router.animations';
import { OrderhistoryService } from '../../../../services/orderhistory/orderhistory.service';
// import { isFunction } from '../../../../object-interface/common.function';
// import { NgxBarcodeModule } from 'ngx-barcode';
import { Router } from '@angular/router';
// import { MemberService } from '../../../../services/member/member.service';
import Swal from 'sweetalert2';
import { NgbDate, NgbModal, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { ThrowStmt } from '@angular/compiler';
import { timeStamp } from 'console';
import { throwIfEmpty } from 'rxjs/operators';

interface RedeliveryAttributes {
	date?: NgbDate,
	time?: NgbTimeStruct,
	receiver_name?: string,
	delivery_status?: string,
	remark?: string,
	redelivery_date?: NgbDate,
	redelivery_time?: NgbTimeStruct,
	redelivery_courier?: string,
	redelivery_awb_number?: string,
	redelivery_remarks?: string,
	redelivery_method?: string,
	redelivery_services?: string,
	redelivered_return_date?: NgbDate,
	redelivered_return_time: NgbTimeStruct,
	redelivered_return_courier?: string,
	redelivered_return_awb_number?: string,
	redelivered_return_remarks?: string
}

@Component({
	selector: 'app-delivery-process-shipping',
	templateUrl: './delivery-process.shipping.component.html',
	styleUrls: [ './delivery-process.shipping.component.scss' ],
	animations: [ routerTransition() ]
})

export class DeliveryProcessShippingComponent implements OnInit {
	@Input() public detail: any;
	@Input() public back;
	@Input() public propsDetail;

	deliveryHistoryDetail:any;
	calculate:any;
	otherCourValue: any;
	detailPage:any;
	awb_check = false;
	infoDetail = [];
	moreOrderDetail: any;
	openMoreDetail = false;
	urlReceipt: any;
	public loading: boolean = false;
	urlLabel: any;
	editThis = false;
	public done: boolean = false;
	public notesCategory: any = [
		{
			label: 'Product out of stock',
			value: false
		},
		{
			label: 'Color options are not available',
			value: false
		},
		{
			label: 'Product size is not available',
			value: false
		},
		{
			label: 'Other',
			value: false
		}
	];
	categoryNotes: any;
	public shippingStatusList: any = [
		{
			label: 'on checking and processing',
			value: 'on_processing',
			id: 'on-process',
			checked: true,
			hovered: false,
			values: 'on_processing'
		},
		{
			label: 'On Delivery process',
			value: 'on_delivery',
			id: 'on-delivery',
			hovered: false,
			values: 'on_delivery'
		},
		{
			label: 'Delivered',
			value: 'delivered',
			id: 'delivered',
			hovered: false,
			values: 'delivered'
		}
	];

	/*public redeliverList: Array<any> = [
		{
			label: 'Return Order Process',
			value: 'return',
			checked: true,
			hovered: false,
		},
		{
			label: 'Return Order Redelivery',
			value: 'redelivery',
			checked: false,
			hovered: false,
		},
		{
			label: 'Delivered',
			value: 'delivered',
			id: 'delivered',
			hovered: false,
			values: 'delivered'
		}
	];*/

	public redeliverList: Array<any> = [];

	packaging = false;

	statusValue: any;
	statusLabel: any;

	public transactionStatus: any = [
		{ label: 'PAID', value: 'PAID' },
		{ label: 'CANCEL', value: 'CANCEL' },
		{ label: 'WAITING FOR CONFIRMATION', value: 'WAITING FOR CONFIRMATION' }
	];
	service_courier: any = [
		{ label: 'Drop', value: 'DROP'},
		{ label: 'Pickup', value: 'PICKUP'}
	]
	errorLabel: any = false;
	transaction_status: any;
	merchantMode = false;
	sum_total: any;
	currentPermission;
	info = [];
	change = false;
	notes: any;
	isClicked = false;
	isClicked2 = false;
	courier_list = [];
	tempstatus = '';
	openNotes = false;
	editAwb = false;
	openNotes2: boolean;
	holdvalue: any;
	infoDetailLabel: any;
	othercour = false;
	choose_service: any;
	supported= false;
	method_delivery :any;
	courier:any = '' ;
    delivery_service:any = '';
    delivery_method:any = '';
	courier_name:any = '';
    awb_number:any;
	changeAWB: Boolean = false;
	delivery_remarks: any = "";

	isChangeLoading : Boolean = false;
	isChangeAWBLoading : Boolean = false;
	// detailToLowerCase: string = "";

	isEverDelivered : Boolean = false;

	date: NgbDate;
	time: NgbTimeStruct;
	receiver_name: string;
	delivery_status: string;
	remark: string;

	redelivered_date: Array<NgbDate | null> = [];
	redelivered_time: Array<NgbTimeStruct | null>  = [];
	redelivered_receiver_name: Array<string> = [];
	redelivered_delivery_status: Array<string> = [];
	redelivered_remark: Array<string> = [];

	completeButtonConfirmation: Boolean = false;
	isCompleteOrder: Boolean = false;
	disableFormShipping: Array<Boolean> = [];


	returnType:any = "redeliver";

	isRedeliver:boolean = false;
	isChangeRedeliverLoading : Boolean = false;
	redeliverButtonConfirmation : boolean = false;

	return_date: Array<NgbDate | null> = [];
	return_time: Array<NgbTimeStruct | null> = [];
	return_courier: Array<string> = [];
	return_awb_number: Array<string> = [];
	return_remarks: Array<string> = [];


	isProcessRedelivery:boolean = false;
	isChangeProcessRedeliveryLoading : Boolean = false;
	processOrderRedeliveryButtonConfirmation : boolean = false;

	redelivery_date: Array<NgbDate | null> = [];
	redelivery_time: Array<NgbTimeStruct | null> = [];
	redelivery_courier: Array<string> = [];
	redelivery_awb_number: Array<string> = [];
	redelivery_remarks: Array<string> = [];
	redelivery_method: Array<string> = [];
	redelivery_services: Array<string> = [];

	isRedeliveryCompleted:boolean = false;

	reorderProductCheck: Array<boolean> = []; 


	isReorder:boolean = false;
	isChangeReorderLoading : Boolean = false;
	reorderButtonConfirmation : boolean = false;
	reorder_date: NgbDate;
	reorder_time: NgbTimeStruct;
	reorder_courier:any;
	reorder_awb_number:any;
	reorder_remarks:any;

	package_weight:any;
	package_length:any;
	package_width:any;
	package_height:any;

	isPackaging:boolean = false;
	isSubmitPackagingLoading:boolean = false;

	reference_no: any;

	redeliveryShippingInfo: Array<any>;

	lastIndexRedelivery : number;

	constructor(
		public orderhistoryService: OrderhistoryService,
		// private route: ActivatedRoute,
		private router: Router,
		// public memberService: MemberService
		private modalService: NgbModal
	) {}

	async ngOnInit() {
		this.currentPermission = 'admin';
		// this.deliveryHistoryDetail = this.detail;

		// console.log("this.detail", this.detail)
		this.reference_no = this.propsDetail.reference_no;
		await this.firstLoad();
	}

	async firstLoad() {
		try {
			let result = await this.orderhistoryService.getShippingDetailByRef(this.reference_no);
			
			if(result && result.values) {
				this.deliveryHistoryDetail = result.values[0];
				this.reference_no = this.deliveryHistoryDetail.reference_no;
			}

			if(!this.deliveryHistoryDetail.shipping_info) this.deliveryHistoryDetail.shipping_info = [];
			
			this.changeAWB = false;
			if(this.deliveryHistoryDetail.available_courier){
				this.deliveryHistoryDetail.available_courier.push(
					{
						courier:"others",
						courier_code:"others"
					}
				);
			} else {
				this.courier = 'others'
			}
			if(this.deliveryHistoryDetail.courier){
				this.courier = this.deliveryHistoryDetail.courier;
				this.delivery_service = this.deliveryHistoryDetail.delivery_service;
				this.delivery_method = this.deliveryHistoryDetail.delivery_method;
				this.awb_number = this.deliveryHistoryDetail.awb_number;
				if(this.deliveryHistoryDetail.courier == 'others') this.courier_name = this.deliveryHistoryDetail.courier_name;
			}

			this.redeliverList = [];

			this.redelivery_date = [];
			this.redelivery_time = [];
			this.redelivery_courier = [];
			this.redelivery_awb_number = [];
			this.redelivery_remarks = [];
			this.redelivery_method = [];
			this.redelivery_services = [];

			this.return_date = [];
			this.return_time = [];
			this.return_courier = [];
			this.return_awb_number = [];
			this.return_remarks = [];

			this.redelivered_date = [];
			this.redelivered_time = [];
			this.redelivered_receiver_name = [];
			this.redelivered_delivery_status = [];
			this.redelivered_remark = [];

			this.disableFormShipping = [];

			this.completeButtonConfirmation = false;
			this.processOrderRedeliveryButtonConfirmation = false;
			this.redeliverButtonConfirmation = false;

			this.reorderProductCheck = [];
			this.deliveryHistoryDetail.product_list.forEach((product : any) => {
				this.reorderProductCheck.push(false);
			});

			this.deliveryHistoryDetail.shipping_info.forEach((element:any, index: number) => {
				if(element.label == "on_delivery") {
					this.delivery_remarks = element.remarks != null? element.remarks : "";

					this.isPackaging = true;
				}

				else if(element.label == "return") {
					this.reorder_remarks = element.remarks;
					if(index > 2){
						
						const matchReturnDetail = this.deliveryHistoryDetail.return_detail.find((dReturn:any) => dReturn.return_id == element.id);
						
						this.return_remarks.push(element.remarks);
						this.return_date.push(this.convertStringToNgDate(matchReturnDetail.return_date));
						this.return_time.push(this.convertStringToNgbTimeStruct(matchReturnDetail.return_date));
						this.return_courier.push(matchReturnDetail.courier);
						this.return_awb_number.push(matchReturnDetail.awb_number);

						this.redelivery_date.push(null);
						this.redelivery_time.push(null);
						this.redelivery_courier.push(null);
						this.redelivery_awb_number.push(null);
						this.redelivery_remarks.push(null);
						this.redelivery_method.push(null);
						this.redelivery_services.push(null);

						this.redelivered_date.push(null);
						this.redelivered_time.push(null);
						this.redelivered_receiver_name.push(null);
						this.redelivered_delivery_status.push(null);
						this.redelivered_remark.push(null);

						this.disableFormShipping.push(true);

						this.redeliverList.push(
							{
								label: 'Return Order Process',
								value: 'return',
								checked: true,
								hovered: false,
							}
						);
					};

					if(index == (this.deliveryHistoryDetail.shipping_info.length - 1) && this.deliveryHistoryDetail.status != 'REORDER'){
						this.addArraysRedelivery();
						this.redeliverList.push(
							{
								label: 'Return Order Redelivery',
								value: 'redelivery',
								checked: true,
								hovered: false,
							}
						);
					}
				}

				else if(element.label == "redelivery") {
					if(index > 2){

						const matchRedeliveryDetail: any = this.deliveryHistoryDetail.redelivery_detail.find((dRedelivery:any) => dRedelivery.delivery_id == element.id);
						
						this.redelivery_date.push(this.convertStringToNgDate(matchRedeliveryDetail.delivery_date));
						this.redelivery_time.push(this.convertStringToNgbTimeStruct(matchRedeliveryDetail.delivery_date));
						this.redelivery_courier.push(matchRedeliveryDetail.courier);
						this.redelivery_awb_number.push(matchRedeliveryDetail.awb_number);
						this.redelivery_remarks.push(element.remarks);
						this.redelivery_method.push(matchRedeliveryDetail.delivery_method);
						this.redelivery_services.push(matchRedeliveryDetail.delivery_service);

						this.return_date.push(null);
						this.return_time.push(null);
						this.return_courier.push(null);
						this.return_awb_number.push(null);
						this.return_remarks.push(null);

						this.redelivered_date.push(null);
						this.redelivered_time.push(null);
						this.redelivered_receiver_name.push(null);
						this.redelivered_delivery_status.push(null);
						this.redelivered_remark.push(null);

						this.disableFormShipping.push(true);

						this.redeliverList.push(
							{
								label: 'Return Order Redelivery',
								value: 'redelivery',
								checked: true,
								hovered: false,
							}
						);
					}

					if(index == (this.deliveryHistoryDetail.shipping_info.length - 1)){
						this.addArraysRedelivery();
						this.redeliverList.push(
							{
								label: 'Delivered',
								value: 'delivered',
								id: 'delivered',
								hovered: false,
								values: 'delivered',
								checked:false,
							}
						);
					}
				}

				else if(element.label == "delivered") {
					this.isEverDelivered = true;
					this.isCompleteOrder = true;

					
					if(index > 2){
						this.redelivered_date.push(this.convertStringToNgDate(element.created_date));
						this.redelivered_time.push(this.convertStringToNgbTimeStruct(element.created_date));
						this.redelivered_receiver_name.push(element.receiver_name);
						this.redelivered_delivery_status.push(element.delivery_status);
						this.redelivered_remark.push(element.remarks);

						this.redeliverList.push(
							{
								label: 'Delivered',
								value: 'delivered',
								id: 'delivered',
								hovered: false,
								values: 'delivered',
								checked:true,
							}
						);

						this.return_date.push(null);
						this.return_time.push(null);
						this.return_courier.push(null);
						this.return_awb_number.push(null);
						this.return_remarks.push(null);


						this.redelivery_date.push(null);
						this.redelivery_time.push(null);
						this.redelivery_courier.push(null);
						this.redelivery_awb_number.push(null);
						this.redelivery_remarks.push(null);
						this.redelivery_method.push(null);
						this.redelivery_services.push(null);

						this.disableFormShipping.push(true);

					} else {
						this.date = this.convertStringToNgDate(element.delivered_date);
						this.time = this.convertStringToNgbTimeStruct(element.delivered_date);
						this.receiver_name = element.receiver_name;
						this.delivery_status = element.delivery_status;
						this.remark = element.remarks;
					}
					if(index == (this.deliveryHistoryDetail.shipping_info.length - 1)){
						this.addArraysRedelivery();
						this.redeliverList.push(
							{
								label: 'Return Order Process',
								value: 'return',
								checked: true,
								hovered: false,
							}
						);
					}
				}
			});

			this.lastIndexRedelivery = this.redeliverList.length - 1;

			// if(this.deliveryHistoryDetail.status == "RETURN" && this.deliveryHistoryDetail.return_detail) {
			// 	this.isRedeliver = true;

			// 	// this.return_date = this.convertStringToNgDate(this.deliveryHistoryDetail.return_detail.return_date);
			// 	// this.return_time = this.convertStringToNgbTimeStruct(this.deliveryHistoryDetail.return_detail.return_date);
			// 	// this.return_courier = this.deliveryHistoryDetail.return_detail.courier;
			// 	// this.return_awb_number=this.deliveryHistoryDetail.return_detail.awb_number;
			// 	// // this.return_remarks=this.deliveryHistoryDetail.return_detail.remarks;
			// }

			// if(this.deliveryHistoryDetail.status == "ACTIVE" && this.deliveryHistoryDetail.redelivery_detail && this.deliveryHistoryDetail.return_detail) {
			// 	this.isRedeliver = true;

			// 	// this.return_date = this.convertStringToNgDate(this.deliveryHistoryDetail.return_detail.return_date);
			// 	// this.return_time = this.convertStringToNgbTimeStruct(this.deliveryHistoryDetail.return_detail.return_date);
			// 	// this.return_courier = this.deliveryHistoryDetail.return_detail.courier;
			// 	// this.return_awb_number=this.deliveryHistoryDetail.return_detail.awb_number;
			// 	// // this.return_remarks=this.deliveryHistoryDetail.return_detail.remarks;

			// 	this.isProcessRedelivery = true;
			// 	// this.redeliverList[1].checked = true;
			// 	// this.redelivery_date = this.convertStringToNgDate(this.deliveryHistoryDetail.redelivery_detail.delivery_date);
			// 	// this.redelivery_time = this.convertStringToNgbTimeStruct(this.deliveryHistoryDetail.redelivery_detail.delivery_date);
			// 	// this.redelivery_courier = this.deliveryHistoryDetail.redelivery_detail.courier;
			// 	// this.redelivery_awb_number = this.deliveryHistoryDetail.redelivery_detail.awb_number;
			// 	// this.redelivery_remarks = this.deliveryHistoryDetail.redelivery_detail.remarks;
			// 	// this.redelivery_method = this.deliveryHistoryDetail.redelivery_detail.delivery_method;
			// 	// this.redelivery_services = this.deliveryHistoryDetail.redelivery_detail.delivery_service;
			// }





			// if(this.deliveryHistoryDetail.status == "DELIVERED" && this.deliveryHistoryDetail.redelivery_detail && this.deliveryHistoryDetail.return_detail) {
			// 	// this.isRedeliver = true;

			// 	// this.return_date = this.convertStringToNgDate(this.deliveryHistoryDetail.return_detail.return_date);
			// 	// this.return_time = this.convertStringToNgbTimeStruct(this.deliveryHistoryDetail.return_detail.return_date);
			// 	// this.return_courier = this.deliveryHistoryDetail.return_detail.courier;
			// 	// this.return_awb_number=this.deliveryHistoryDetail.return_detail.awb_number;
			// 	// // this.return_remarks=this.deliveryHistoryDetail.return_detail.remarks;

			// 	this.isProcessRedelivery = true;
			// 	// this.redeliverList[1].checked = true;
			// 	// this.redeliverList[2].checked = true;

			// 	// this.redelivery_date = this.convertStringToNgDate(this.deliveryHistoryDetail.redelivery_detail.delivery_date);
			// 	// this.redelivery_time = this.convertStringToNgbTimeStruct(this.deliveryHistoryDetail.redelivery_detail.delivery_date);
			// 	// this.redelivery_courier = this.deliveryHistoryDetail.redelivery_detail.courier;
			// 	// this.redelivery_awb_number = this.deliveryHistoryDetail.redelivery_detail.awb_number;
			// 	// this.redelivery_remarks = this.deliveryHistoryDetail.redelivery_detail.remarks;
			// 	// this.redelivery_method = this.deliveryHistoryDetail.redelivery_detail.delivery_method;
			// 	// this.redelivery_services = this.deliveryHistoryDetail.redelivery_detail.delivery_service;

			// 	// const completedReturn = this.deliveryHistoryDetail.shipping_info[this.deliveryHistoryDetail.shipping_info.length - 1];
			// 	// this.date = this.convertStringToNgDate(completedReturn.created_date);
			// 	// this.time = this.convertStringToNgbTimeStruct(completedReturn.created_date);
			// 	// this.receiver_name = completedReturn.receiver_name;
			// 	// this.delivery_status = completedReturn.delivery_status;
			// 	// this.remark = completedReturn.remarks;

			// 	// this.isRedeliveryCompleted = true;
			// }


			// if(this.deliveryHistoryDetail.status == "CANCEL" && this.deliveryHistoryDetail.return_detail) {
			// 	// this.isReorder = true;
			// 	// this.returnType = "reorder";

			// 	// this.reorder_date = this.convertStringToNgDate(this.deliveryHistoryDetail.return_detail.return_date);
			// 	// this.reorder_time = this.convertStringToNgbTimeStruct(this.deliveryHistoryDetail.return_detail.return_date);
			// 	// this.reorder_courier = this.deliveryHistoryDetail.return_detail.courier;
			// 	// this.reorder_awb_number=this.deliveryHistoryDetail.return_detail.awb_number;
			// 	// // this.return_remarks=this.deliveryHistoryDetail.return_detail.remarks;
			// }

			
			// STEP_3_GET_MEMBER_DETAIL_AND_SETUP_CURRENT_PERMISSION: {
			// 	/** I need to get Member detail for getting their permission status */


			// 	if (this.currentPermission == 'admin') {
			// 		this.setShippingStatusList();
			// 	} else if (this.currentPermission == 'merchant') {
			// 		this.setShippingStatus();
			// 	} else {
			// 		console.log('error');
			// 	}

			// 	if (this.currentPermission == 'admin') {
			// 		this.deliveryHistoryDetail.order_list.forEach((element, index) => {
			// 			if (element.shipping_info == null) {
			// 				// let no_history = "No History"
			// 				this.info.push('No History');
			// 			} else {
			// 				let array_info = element.shipping_info.length - 1;
			// 				console.log('array', element.shipping_info);
			// 				this.info.push(element.shipping_info[array_info].title);
			// 			}
			// 		});
			// 	}
			// }

			// console.log("test", this.currentPermission)

			// try {
				// this.deliveryHistoryDetailToLowerCase = this.deliveryHistoryDetail.status.toLowerCase();
				if (typeof this.deliveryHistoryDetail.billings != 'undefined') {
					let billings = this.deliveryHistoryDetail.billings;
					let additionalBillings = [];
					if (billings.discount) { 
						additionalBillings.push({
							product_name: billings.discount.product_name,
							value: billings.discount.value,
							discount: true
						});
					}
					if (this.deliveryHistoryDetail.status == 'CHECKOUT') {
						// if (this.deliveryHistoryDetail.payment_detail.payment_via == 'manual banking') {
						if (billings.unique_amount) {
							billings.unique_amount;
						}
						// }
					} else {
						delete billings.unique_amount;
					}
					for (let prop in billings) {
						// console.log("PROP", prop);
						if (prop == 'shipping_fee' || prop == 'unique_amount') {
							additionalBillings.push({
								product_name: prop,
								value: billings[prop],
								discount: false
							});
						}
					}
					this.deliveryHistoryDetail.additionalBillings = additionalBillings;
				} else if (typeof this.deliveryHistoryDetail.billings == 'undefined') {
					// let additionalBillings = [];
					// let shipping_services

					// additionalBillings.push(
					//   {
					//     product_name: "Shipping Fee",
					//     value: this.deliveryHistoryDetail.shipping_fee,
					//     discount: false
					//   }
					// )
					// let shipping_service = {
					// 	shipping: {
					// 		service: this.deliveryHistoryDetail.delivery_detail.delivery_service
					// 	}
					// };

					// let sum_total = {
					// 	sum_total: this.deliveryHistoryDetail.total_price
					// };

					// let detail_courier = this.deliveryHistoryDetail.courier
					// 	? this.deliveryHistoryDetail.courier.courier
					// 	: this.deliveryHistoryDetail.shipping_service;

					// let courier = {
					// 	shipping_services: detail_courier
					// };

					// let courier_detail = {
					//   courier : {
					//     courier : "",
					//     awb_number : "",
					//     courier_code : "",
					//   }
					// }
					ASSIGN_OBJECT_TO_ORDER_HISTORY_DETAIL: {
						// Object.assign(this.deliveryHistoryDetail, courier);
						// Object.assign(this.deliveryHistoryDetail, courier_detail)
						// Object.assign(this.deliveryHistoryDetail, sum_total);
						// Object.assign(this.deliveryHistoryDetail, shipping_service);
						// this.deliveryHistoryDetail.additionalBillings = additionalBillings;
					}

					// console.log('Detail', this.deliveryHistoryDetail);
				}

				// if (this.deliveryHistoryDetail.merchant) {
				// 	this.merchantMode = true;
				// }
				// if (!this.deliveryHistoryDetail.shipping.service) {
				// 	this.deliveryHistoryDetail.shipping.service = 'no-services';
				// 	// console.log("Result", this.deliveryHistoryDetail.shipping)
				// }
				// if (this.deliveryHistoryDetail.status) {
				// 	this.deliveryHistoryDetail.status = this.deliveryHistoryDetail.status.trim().toUpperCase();
				// }

				// if (this.deliveryHistoryDetail.products) {
				// 	// console.log("disini", this.deliveryHistoryDetail.products)
				// 	this.deliveryHistoryDetail.products.forEach((element, index) => {});
				// }

				if (this.deliveryHistoryDetail.shipping_info) {
					// console.log('this.deliveryHistoryDetail.shipping_info', this.deliveryHistoryDetail.shipping_info)
					this.shippingStatusList.forEach((element, index) => {
						let check = this.isShippingValueExists(element.value);
						if (check) {
							this.shippingStatusList[index].checked = check[0];
							this.shippingStatusList[index].created_date = check[1];
						} else {
							this.shippingStatusList[index].checked = false;
						}
					});
				}
				// if (this.deliveryHistoryDetail.courier_list.supported){
					
				// 	this.courier_list = this.deliveryHistoryDetail.courier_list.supported.concat(this.deliveryHistoryDetail.courier_list.unsupported);
				// 	this.listService()
				// }

				// this.deliveryHistoryDetail.previous_status = this.deliveryHistoryDetail.status;
				// this.transactionStatus.forEach((element, index) => {
				// 	if (element.value == this.deliveryHistoryDetail.transaction_status) {
				// 		this.transactionStatus[index].selected = 1;
				// 	}
				// });
			// } catch (e) {
			// 	this.errorLabel = (<Error>e).message; //conversion to Error type
			// }
			// console.log("type of delivery detail ", this.deliveryHistoryDetail)

			if(this.deliveryHistoryDetail.weight && this.deliveryHistoryDetail.dimensions) {
				this.package_height = this.deliveryHistoryDetail.dimensions.height;
				this.package_length = this.deliveryHistoryDetail.dimensions.length;
				this.package_width = this.deliveryHistoryDetail.dimensions.width;
				this.package_weight = this.deliveryHistoryDetail.weight * 1000;
				// this.isPackaging = true;
			}
		} catch (error) {
			console.log(error);
		}
	}


	addArrayAttributes(redeliv : RedeliveryAttributes) : void {

	}

	addArraysRedelivery() : void {
		this.return_remarks.push(null);
		this.return_date.push(null);
		this.return_time.push(null);
		this.return_courier.push(null);
		this.return_awb_number.push(null);

		this.redelivery_date.push(null);
		this.redelivery_time.push(null);
		this.redelivery_courier.push(null);
		this.redelivery_awb_number.push(null);
		this.redelivery_remarks.push(null);
		this.redelivery_method.push(null);
		this.redelivery_services.push(null);

		this.redelivered_date.push(null);
		this.redelivered_time.push(null);
		this.redelivered_receiver_name.push(null);
		this.redelivered_delivery_status.push(null);
		this.redelivered_remark.push(null);

		this.disableFormShipping.push(false);
	}

	isArray(obj : any ) {
		return Array.isArray(obj)
	 }
	
	openScrollableContent(longContent) {
		this.modalService.open(longContent, { centered: true });
	}

	openScrollableTrackingContent(trackingContent) {
		this.modalService.open(trackingContent, { centered: true });
	}

	replaceVarian(value) {
		return JSON.stringify(value).replace('{','').replace('}','').replace(/[',]+/g, ', ').replace(/['"]+/g, '')
	}

	async updateComplete(index : number) {
		try {
			this.isChangeLoading = true;
			const delivered_date  = index ? this.convertDateToString(this.redelivered_date[index], this.redelivered_time[index]) : this.convertDateToString(this.date, this.time);

			let payloadData = {
				"ref_no":this.reference_no,
				"delivered_date":delivered_date,
				"receiver_name":index ? this.redelivered_receiver_name[index] : this.receiver_name,
				"delivery_status":index ? this.redelivered_delivery_status[index] : this.delivery_status,
				"remarks":index ? this.redelivered_remark[index] : this.remark
			}

			const result = await this.orderhistoryService.updateDeliveredProcess(payloadData);

			if(result.status == "success") {
				this.deliveryHistoryDetail = await this.orderhistoryService.getShippingDetailByRef(payloadData.ref_no);
				await this.updateDetailOrder();
				await this.firstLoad();
			}
			
			this.isChangeLoading = false;

		} catch (error) {
			this.isChangeLoading = false;
		}
	}

	convertDateToString(date: NgbDate, time: NgbTimeStruct) {
		return date.year.toString().padStart(2, "0") + '-' + date.month.toString().padStart(2, "0") + '-' + date.day.toString().padStart(2, "0") + " " + time.hour.toString().padStart(2, "0") + ':' + time.minute.toString().padStart(2, "0") + ':' + time.second.toString().padStart(2, "0")
	}

	convertDateOnlyToString(date: NgbDate) {
		return date.year.toString().padStart(2, "0") + '-' + date.month.toString().padStart(2, "0") + '-' + date.day.toString().padStart(2, "0")
	}

	convertStringToNgDate(value: String) {
		const _value = value.split(" ")[0].split("-");

		let result: NgbDate = new NgbDate(0,0,0);
		result.year = parseInt(_value[0]);
		result.month = parseInt(_value[1]);
		result.day = parseInt(_value[2]);

		return result;
	}

	convertStringToNgbTimeStruct(value: String) {
		const _value = value.split(" ")[1].split(":");
		let result: NgbTimeStruct = { hour: parseInt(_value[0]), minute: parseInt(_value[1]), second: parseInt(_value[2]) }
		return result;
	}

	
	backToTable() {
		this.back[1](this.back[0]);
	}

	async cancelShipping(){
		if(!this.reference_no) return;
		Swal.fire({
			title: 'Anda akan membatalkan proses pickup dari : '+this.reference_no,
			text: 'Alasan pembatalan : ',
			icon: 'error',
			confirmButtonText: 'Ok',
			cancelButtonText:"cancel",
			showCancelButton:true,
			input: 'text',
		  }).then(async (result) => {
			if(result.isConfirmed){
				const payload = {
					ref_no:this.reference_no,
					description:result.value,
				};
				try {
					const result = await this.orderhistoryService.cancelShipping(payload);
					Swal.fire({
						title:"Cancel",
						text: 'Pickup dengan no. referensi : '+this.reference_no+' telah dibatalkan',
						icon: 'error',
						confirmButtonText: 'Ok',
						showCancelButton:false,
					}).then((result) => {
						if(result.isConfirmed){
							// this.backToTable();
							this.firstLoad();
					}});
				} catch (e) {
					this.errorLabel = ((<Error>e).message);

					Swal.fire({
						title:"Error",
						text: this.errorLabel,
						icon: 'error',
						confirmButtonText: 'Ok',
					});
				}
			}
		  });
	}

	async processOrder(){
		if(!this.detail.order_id) return;
		Swal.fire({
			title: 'Confirmation',
			text: 'Apakah anda yakin ingin memproses order : '+this.detail.order_id,
			icon: 'success',
			confirmButtonText: 'process',
			cancelButtonText:"cancel",
			showCancelButton:true
		  }).then(async (result) => {
			if(result.isConfirmed){
				const payload = {
					order_id:this.detail.order_id
				}
				try {
					const result = await this.orderhistoryService.processOrder(payload);
					Swal.fire({
						title:"Success",
						text: 'Order : '+this.detail.order_id+ ' telah disetujui',
						icon: 'success',
						confirmButtonText: 'Ok',
						showCancelButton:false,
					}).then((result) => {
						if(result.isConfirmed){
							this.backToTable();
					}});
				} catch (e) {
					this.errorLabel = ((<Error>e).message);
				}
			}
		});
	}

	calculateSumPrice(index) {
		let sum_price =
			this.deliveryHistoryDetail.products[index].fixed_price * this.deliveryHistoryDetail.products[index].quantity;

		return sum_price;
	}

	// async getCourier() {
	// 	let couriers = await this.orderhistoryService.getCourier();
	// 	this.courier_list = couriers.result;
	// 	console.log('result', this.courier_list);

	// 	this.courier_list.forEach((element, index) => {
	// 		let values = {
	// 			value: element.courier
	// 		};
	// 		Object.assign(this.courier_list[index], values);
	// 	});
	// 	console.log(this.courier_list);
	// }

	// backToTable() {
	// 	if (this.back[0][this.back[1]] && !isFunction(this.back[0])) {
	// 		this.back[0][this.back[1]];
	// 	} else if (isFunction(this.back[0])) {
	// 		this.back[0]();
	// 	} else {
	// 		window.history.back();
	// 	}
	// }

	backToList() {
		console.log(true);

		if (this.openMoreDetail) {
			this.openMoreDetail = false;
		}
	}

	// setShippingStatusList() {
	// 	// console.log("here", this.deliveryHistoryDetail.shipping_info);
	// 	this.deliveryHistoryDetail.order_list.forEach((element, index) => {
	// 		if (this.shippingStatusList[index].id == 'on-process') {
	// 			this.shippingStatusList[index].checked = true;
	// 		} else if (this.shippingStatusList[index].id == 'warehouse-packaging') {
	// 			this.shippingStatusList[index].checked = true;
	// 		} else if (this.shippingStatusList[index].id == 'on-delivery') {
	// 			this.shippingStatusList[index].checked = true;
	// 		} else if (this.shippingStatusList[index].id == 'delivered') {
	// 			this.shippingStatusList[index].checked = true;
	// 		}
	// 	});
	// }

	setShippingStatus() {
		// console.log("here", this.deliveryHistoryDetail.shipping_info);
		this.deliveryHistoryDetail.shipping_info.forEach((element, index) => {
			if (this.shippingStatusList[index].id == 'on-process') {
				this.shippingStatusList[index].checked = true;
			} else if (this.shippingStatusList[index].id == 'warehouse-packaging') {
				this.shippingStatusList[index].checked = true;
			} else if (this.shippingStatusList[index].id == 'on-delivery') {
				this.shippingStatusList[index].checked = true;
			} else if (this.shippingStatusList[index].id == 'delivered') {
				this.shippingStatusList[index].checked = true;
			}
		});
	}

	changeshippingStatusList(index,) {
		//  
		for (let n = 0; n <= index; n++) {
			if (this.shippingStatusList[n].id == 'on-delivery') {
				// console.log("on_delivery");
				// this.shippingStatusList[n].checked = true;
				if(this.deliveryHistoryDetail.shipping_info.at(-1).label == 'on_processing' || this.deliveryHistoryDetail.shipping_info.at(-1).label == 'on_delivery'){
					// console.log("on_delivery 2");
					if(this.deliveryHistoryDetail.shipping_info.at(-1).label == 'on_processing') this.changeAWB = true;
					this.shippingStatusList[n].checked = true;
				}

				// if (this.deliveryHistoryDetail.shipping_services) {
				// 	this.shippingStatusList[n].checked = true;
				// }	
				// if (!this.deliveryHistoryDetail.shipping){
				// 	this.shippingStatusList[n].checked = true;
				// }
			} else if (this.shippingStatusList[n].id == 'delivered') {
				console.log("delivered");
				if (this.deliveryHistoryDetail.awb_number) {
					console.log("delivered 2");
					this.shippingStatusList[n].checked = true;
				}
			}
			//  else if(this.shippingStatusList[n].id == 'on_processing'){

			// 	// else if (this.shippingStatusList[n].id == 'warehouse-packaging') {
			// 	//   let warehouse_packaging = (this.deliveryHistoryDetail.billings != undefined) ? this.deliveryHistoryDetail.shipping_services : this.deliveryHistoryDetail.courier.courier
			// 	//   if (warehouse_packaging) {
			// 	//     this.shippingStatusList[n].checked = true;
			// 	//   }
			// 	// }
			// 	this.shippingStatusList[n].checked = true;
			// }

			// if (this.shippingStatusList[n].id == 'warehouse-packaging') {
			// 	this.packaging = true;
			// 	// this.shippingStatusList[n].checked = true;
			// } else {
			// 	this.packaging = false;
			// }
		}

		// for (let n = index + 1; n < this.shippingStatusList.length; n++) {
		// 	this.shippingStatusList[n].checked = false;
		// }
	}

	hoverShippingStatusList(index, objectHover) {
		for (let n = 0; n <= index; n++) {
			this.shippingStatusList[n].hovered = true;
		}

		for (let n = index + 1; n < this.shippingStatusList.length; n++) {
			this.shippingStatusList[n].hovered = false;
		}
	}

	convertShippingStatusToShippingInfo() {
		let newData = [];
		this.shippingStatusList.forEach((element, index) => {
			if (element.checked) {
				newData.push({ value: element.values });
			}
		});
		return newData;
	}
	async saveThis() {
		if (this.currentPermission == 'admin' && this.isClicked2) {
			this.deliveryHistoryDetail.status = this.tempstatus;
			this.cancelMerchant();
		} else {
			try {
				this.loading = !this.loading;
				this.deliveryHistoryDetail.status = this.tempstatus;
				if (this.deliveryHistoryDetail.billings != undefined) {
					let frm = JSON.parse(JSON.stringify(this.deliveryHistoryDetail));
					frm.shipping_info = this.convertShippingStatusToShippingInfo();
					// this.deliveryHistoryDetail.shipping_status = this.valu
					// if (frm.status != "CANCEL") {
					delete frm.buyer_detail;
					delete frm.payment_expire_date;
					delete frm.products;
					delete frm.billings;
					delete frm.user_id;
					console.log('frm', frm);
					await this.orderhistoryService.updateOrderHistoryAllHistory(frm);
					
					// setTimeout(function(){window.location.href}, 3000);
					// }
				} else {
					let all_shipping_label = this.convertShippingStatusToShippingInfo();
					let shipping_label = all_shipping_label[all_shipping_label.length - 1].value;
					let awb = this.deliveryHistoryDetail.awb_receipt
						? this.deliveryHistoryDetail.awb_receipt
						: this.deliveryHistoryDetail.courier ? this.deliveryHistoryDetail.courier.awb_number : null;
					let courier = this.deliveryHistoryDetail.shipping_services
						? this.deliveryHistoryDetail.shipping_services
						: 'no-services';
					console.log('courier', this.deliveryHistoryDetail.billings, this.deliveryHistoryDetail.courier);
					if (
						this.deliveryHistoryDetail.billings == undefined &&
						this.deliveryHistoryDetail.courier != undefined &&
						this.deliveryHistoryDetail.courier.courier_code
					) {
						courier = this.deliveryHistoryDetail.courier.courier_code;
					}
					let payload = {
						booking_id: this.deliveryHistoryDetail.booking_id,
						shipping_label: shipping_label,
						courier_code: courier,
						awb_number: awb,
						delivery_method: this.choose_service,
						status: this.deliveryHistoryDetail.status
					};
					console.log('payload', payload);
					if( this.othercour){
						let newpayload = {
							booking_id: this.deliveryHistoryDetail.booking_id,
							courier: this.otherCourValue,
							courier_support :0,
							awb_number: awb,
							shipping_label: shipping_label
						}
						await this.orderhistoryService.updateOrderHistoryAllHistoryMerchant(newpayload);
					} else {
						if(shipping_label == 'on_packaging' && payload.courier_code == 'LO-CR-GR'){ // special case for INSTANT courier , add courier_support =0 and courier name 
							let temp_payload = {
								courier_support : 0,
								courier: 'GRAB'
							}
							Object.assign(payload,temp_payload)
						}
						await this.orderhistoryService.updateOrderHistoryAllHistoryMerchant(payload);
						console.log('choose',this.choose_service)
						if ( this.choose_service == 'PICKUP'){
							Swal.fire(
								{
									title: 'Pemberitahuan Pickup',
									html : 'Barang anda akan segera di pickup oleh kurir minimal<br/> 2 jam setelah anda melakukan permintaan pickup <br/> <a style="color:red">Baca Term of Service pickup Courier</a>',
									imageUrl: 'assets/images/alarm.png',
									imageWidth: 115,
									imageHeight: 129
								}
							);
						}
						
					}
					

					console.log(this.convertShippingStatusToShippingInfo());
					console.log(this.deliveryHistoryDetail);
					console.log(payload);
				}
				// else{
				//   await this.orderhistoryService.updateOrderHistoryAllHistory(frm.status);
				// }
				this.loading = !this.loading;
				this.done = true;
				this.change = false;
				this.editThis = false;
				alert('Edit submitted');
				
				
				this.firstLoad();

				// window.location.reload();
			} catch (e) {
				this.errorLabel = (<Error>e).message; //conversion to Error type
			}
		}
	}

	receipt() {
		this.urlReceipt = this.deliveryHistoryDetail.uploaded_receipt;
		window.open(this.urlReceipt);
	}

	openLabel() {
		this.router.navigate([ 'administrator/orderhistoryallhistoryadmin/receipt' ], {
			queryParams: { id: this.deliveryHistoryDetail._id }
		});
	}

	openNote() {
		this.openNotes = true;
		// this.deliveryHistoryDetail.status = 'CANCEL'

		// if(this.notesCategory[0] == true){
		// 	console.log(this.notesCategory[0].label);
		// } else if(this.notesCategory[1]){
		// 	value: true;
		// } else if(this.notesCategory[2]){
		// 	value: true;
		// }
	}

	onAWBchange(){
		console.log(' here',this.deliveryHistoryDetail.courier.awb_number)
		if(this.deliveryHistoryDetail.courier.awb_number){
			if (this.deliveryHistoryDetail.courier.awb_number.length == 0){
				this.awb_check = false;
			}
			if (this.deliveryHistoryDetail.courier.awb_number.length > 0){
				this.awb_check = true;
			}
		}
	}
	openNote2() {
		this.openNotes2 = true;
		// this.deliveryHistoryDetail.status = 'CANCEL'
	}

	clickCategory(index) {
		this.notesCategory[index].value = true;
		this.notesCategory.forEach((element, i) => {
			if ((this.notesCategory[index].value = true)) {
				this.notesCategory[i].value = false;
				this.notesCategory[index].value = true;
			}
		});
		this.notes = this.notesCategory[index].label;
		console.log(this.notesCategory);
	}

	noteClose() {
		this.openNotes = this.openNotes2 = false;
	}
	async hold() {
		let form = {
			value: this.holdvalue,
			description: this.notes,
			remark: 'hold transaction process'
		};
		try {
			const holdMerchant = await this.orderhistoryService.holdMerchant(this.detailPage.booking_id, form);

			this.closeNote2();
			this.firstLoad();
		} catch (e) {
			this.errorLabel = (<Error>e).message; //conversion to Error type
		}
	}

	closeNote() {
		this.openNotes = this.openNotes2 = false;
		this.firstLoad();
		Swal.fire('Cancelled', 'Your order has been Cancelled', 'success');
	}
	closeNote2() {
		this.openNotes = this.openNotes2 = false;
		this.firstLoad();
		Swal.fire('HOLD', 'The order has been hold', 'success');
	}

	async cancelOrders() {
		let payload;

		if (this.currentPermission == 'merchant') {
			payload = {
				booking_id: this.deliveryHistoryDetail.booking_id,
				note: this.notes
			};
		} else {
			payload = {
				booking_id: this.detailPage.booking_id,
				note: this.notes
			};
			console.log('payload', this.deliveryHistoryDetail);
		}
		let cancel = await this.orderhistoryService.cancelOrder(payload);

		this.closeNote();
	}

	async cancelMerchant() {
		let payload = {
			status: this.tempstatus
		};

		console.log('payload', payload.status);
		try {
			let cancel = await this.orderhistoryService.updateOrderHistoryAllHistoryCancel(
				this.deliveryHistoryDetail._id,
				payload
			);
			if (cancel) {
				this.loading = !this.loading;
				this.done = true;
				this.change = false;
				this.editThis = false;
				alert('Edit submitted');
				this.firstLoad();
			}
		} catch (e) {
			this.errorLabel = (<Error>e).message; //conversion to Error type
		}
	}

	edit() {
		this.editThis = true;
		console.log('edit true');
	}

	doneEdit() {
		this.editThis = false;
		console.log('done');
	}
	changestatus() {
		this.change = true;
	}
	onpaidclick() {
		//   this.deliveryHistoryDetail.status = "PAID";
		this.tempstatus = 'PAID';
		this.isClicked = true;
		this.isClicked2 = false;
		console.log(this.deliveryHistoryDetail);
	}
	oncancelclick() {
		// this.deliveryHistoryDetail.status = "CANCEL";
		this.tempstatus = 'CANCEL';
		this.isClicked2 = true;
		this.isClicked = false;
		console.log(this.deliveryHistoryDetail);
	}

	async openDetail(bookingID,order) {
		this.openMoreDetail = true;

		let result = await this.orderhistoryService.getDetailOrderhistory(bookingID);
		this.moreOrderDetail = order;
		console.log('moreorderdetail', this.moreOrderDetail)
		this.detailPage = result.result[0];

		console.log('detail', this.detailPage);

		// this.deliveryHistoryDetail.order_list.forEach((element, index) =>{

		// })
		if(this.detailPage.shipping_info){
			this.detailPage.shipping_info.forEach((element, index) => {
				if (element == null) {
					console.log('part 1', element);
					this.infoDetail.push('No Service');
				} else {
					console.log('part 2', element);
					let array = this.detailPage.shipping_info.length - 1;
					console.log('test', array);
					this.infoDetail = this.detailPage.shipping_info[array].title;
					this.infoDetailLabel = this.detailPage.shipping_info[array].label;
					console.log('this2', this.infoDetail);
				}
			});
		}
		

		// if(this.detailPage.shipping_info){
		// 	this.detailPage.shipping_info.forEach((element, index) => {
		// 		console.log(element.shipping_info)
		// 	})
		// }

		if (
			this.openMoreDetail &&
			this.deliveryHistoryDetail.status == 'PAID' &&
			this.deliveryHistoryDetail.status != 'COMPLETED' &&
			(this.infoDetailLabel == 'on_delivery' || this.infoDetailLabel == 'delivered')
		) {
			console.log('true bro');
		} else {
			console.log('salah cok');
		}
		console.log('lkawdj', this.deliveryHistoryDetail.status, this.infoDetailLabel); 
		console.log('booking id', bookingID);
	}
	other_cour(){
		console.log('dipilih',this.deliveryHistoryDetail.shipping_services)
		if(this.deliveryHistoryDetail.shipping_services == 'other'){
			console.log("othernih")
			this.othercour = true;
			this.supported = false;
		}else {
			this.supported = true;
			this.othercour = false;
			
		}
		console.log("courier_list",this.courier_list,"method",this.choose_service)
	}

	listService(){
		this.courier_list.forEach((entry) => {
			console.log(entry)
			console.log(entry.delivery_method)
			if(this.deliveryHistoryDetail.shipping_services == entry.courier_code || this.deliveryHistoryDetail.shipping_services == entry.courier){ 
				this.method_delivery = entry.delivery_method}
		})
	}

	checkStatus() {
		// console.log("Status", this.deliveryHistoryDetail.status)
		this.statusValue = this.deliveryHistoryDetail.status;
		this.statusLabel = this.deliveryHistoryDetail.status;
		if (this.statusValue != 'PAID' && this.statusValue != 'CANCEL') {
			this.transactionStatus.push({ label: this.statusLabel, value: this.statusValue });
		}
	}

	isShippingValueExists(value) {
		for (let element of this.deliveryHistoryDetail.shipping_info) {
			console.log('element', element.label);
			if (element.label.trim().toLowerCase() == value.trim().toLowerCase()) {
				return [ true, element.created_date ];
			}
		}
		return false;
	}




	async updateAWB(){
		if(this.deliveryHistoryDetail.shipping_info.at(-1).label == 'on_delivery' || this.deliveryHistoryDetail.shipping_info.at(-1).label == 'on_processing'){
			if( this.courier && 
				this.delivery_service &&
				this.delivery_method){
					let payload : any = {
						ref_no: this.deliveryHistoryDetail.reference_no,
						courier:this.courier,
						delivery_method:this.delivery_method,
						delivery_service:this.delivery_service,
						remarks: this.delivery_remarks,
						delivery_options:{},
					}

					if(this.courier == 'others' ){
						if(!this.courier_name){
							Swal.fire("Oops", "mohon masukan nama kurir terlebih dahulu", 'warning');
							return;
						}
						// if(!this.awb_number){
						// 	Swal.fire("Oops", "mohon masukan nomor resi terlebih dahulu", 'warning');
						// 	return;
						// }
						payload.courier_name = this.courier_name;
						payload.awb_number = this.awb_number ? this.awb_number : '';
					}

					try {
						this.isChangeAWBLoading = true;
						await this.orderhistoryService.updatDeliveryProcess(payload);
						await this.updateDetailOrder();
						this.isChangeAWBLoading = false;
						this.firstLoad();

					} catch (e) {
						this.isChangeAWBLoading = false;
						this.errorLabel = (<Error>e).message; //conversion to Error type	
						Swal.fire({
							title:"Error",
							text: this.errorLabel,
							icon: 'error',
							confirmButtonText: 'Ok',
						});
					}
				}else {
					Swal.fire("Oops", "Service dan metode pengiriman harus di input terlebih dahulu", 'warning');
				}
		}
	}

	async updateDetailOrder(){
		try {
			this.deliveryHistoryDetail = await this.orderhistoryService.getOrderDetail(this.deliveryHistoryDetail.order_id);
			// let filter = {
			// 	search: {
			// 	  order_id: this.deliveryHistoryDetail.order_id
			// 	},
			// 	limit_per_page: 2,
			// 	download: false
			//   }

			//   let result = await this.orderhistoryService.searchOrderHistory(filter);
			//   this.deliveryHistoryDetail = result.values[0];
			//   console.log("result update detail", this.deliveryHistoryDetail);
		} catch (e) {
			this.errorLabel = (<Error>e).message; //conversion to Error type
		}
	}

	async updateStatusCompleted(){
		if(this.deliveryHistoryDetail.last_shipping_info == 'on_delivery'){
			try {
				const payload = {
					order_id: this.deliveryHistoryDetail.order_id,
				}
				await this.orderhistoryService.updateStatusOrder(payload, 'complete');
				await this.updateDetailOrder();
				this.firstLoad();
			} catch (e) {
				this.errorLabel = (<Error>e).message; //conversion to Error type

				Swal.fire({
					title:"Error",
					text: this.errorLabel,
					icon: 'error',
					confirmButtonText: 'Ok',
				});
			}
		}
	}



	async processReturn(index : number, returnType : string = 'redeliver'){

		this.isChangeRedeliverLoading = true;
		try {
			const payload = {
				"return_type":returnType.toUpperCase(),
				"ref_no":this.reference_no,
				"courier":this.return_courier[index],
				"return_date":this.convertDateOnlyToString(this.return_date[index]),
				"remarks":this.return_remarks[index],
				"awb_number":this.return_awb_number[index],
				'product_sku':[]
			};

			if(returnType == 'reorder'){
				console.log('checkbox', this.reorderProductCheck);
				this.reorderProductCheck.forEach((check:boolean, i:number)=>{
					if(check == true) payload.product_sku.push(this.deliveryHistoryDetail.product_list[i]['sku_code'])
				});
			}else {
				delete payload.product_sku;
			}
			await this.orderhistoryService.processRedeliver(payload);
			await this.updateDetailOrder();
			this.isChangeRedeliverLoading = false;
			this.firstLoad();
		} catch (e) {
			this.isChangeRedeliverLoading = false;
			this.errorLabel = (<Error>e).message; //conversion to Error type
		}
	}

	async processOrderRedelivery(index : number){
		try {
			this.isChangeProcessRedeliveryLoading = true;
			const payload = {
				"ref_no":this.reference_no,
				"courier":this.redelivery_courier[index],
				"delivery_method":this.redelivery_method[index],
				"delivery_service":this.redelivery_services[index],
				"delivery_date":this.convertDateOnlyToString(this.redelivery_date[index]),
				"remarks":this.redelivery_remarks[index],
				"awb_number":this.redelivery_awb_number[index],
			}

			await this.orderhistoryService.processOrderRedelivery(payload);
			await this.updateDetailOrder();
			this.isChangeProcessRedeliveryLoading = false;
			this.firstLoad();
		} catch (e) {
			this.isChangeProcessRedeliveryLoading = false;
			this.errorLabel = (<Error>e).message; //conversion to Error type
		}
	}



	async processReorder(){
		try {
			this.isChangeReorderLoading = true;
			const payload = {
				"return_type":"REORDER",
				"ref_no":this.reference_no,
				"courier":this.reorder_courier,
				"return_date":this.convertDateOnlyToString(this.reorder_date),
				"remarks":this.reorder_remarks,
				"awb_number":this.reorder_awb_number,
			};
			await this.orderhistoryService.processRedeliver(payload);
			await this.updateDetailOrder();
			this.isChangeReorderLoading = false;
			this.firstLoad();
		} catch (e) {
			this.isChangeReorderLoading = false;
			this.errorLabel = (<Error>e).message; //conversion to Error type
		}
	}


	// {
	// 	"order_id":"1234567890",
	// 	"courier":"JNE",
	// 	"delivery_method":"DROP"
	// 	"delivery_services":"Regular"
	// 	"delivery_date":"2021-08-30",
	// 	"remarks":"barang rusak",
	// 	"awb_number":"000000000"
	// }

	async submitPackaging() {
		try {
			// this.isChangeReorderLoading = true;
			const payload = {
				"ref_no":this.deliveryHistoryDetail.reference_no,
				"weight":this.package_weight,
				"dimensions":{
					"length":this.package_length,
					"width":this.package_width,
					"height":this.package_height
				}
			};
			this.isSubmitPackagingLoading = true;
			await this.orderhistoryService.processPackaging(payload);
			await this.updateDetailOrder();
			// this.isChangeReorderLoading = false;
			this.isSubmitPackagingLoading = false;;
			this.firstLoad();
		} catch (e) {
			this.isSubmitPackagingLoading = false;
			// this.isChangeReorderLoading = false;
			this.errorLabel = (<Error>e).message; //conversion to Error type
			Swal.fire({
				title:"Error",
				text: this.errorLabel,
				icon: 'error',
				confirmButtonText: 'Ok',
			});
		}
	}

	checkFileType(text : any){
    let splitFileName = text.split('.');
    const extensFile = splitFileName[splitFileName.length - 1];
    const extens = extensFile.toLowerCase();
		console.warn("extens", extens);
    return extens == 'pdf' ? 'document':'image';
  }

	lastPathURL(url:string){
    let resURL : any = url.split("/");
    console.log("resURL", resURL);
    let lastNumber : number = resURL.length - 1;
    return resURL[lastNumber];
  }

	downloadImage(url : any) {
    let filename : any;
    filename = this.lastPathURL(url);
    fetch(url).then(function(t) {
        return t.blob().then((b)=>{
					
          var a = document.createElement("a");
          a.href = URL.createObjectURL(b);
          a.setAttribute("download", filename);
          a.click();
        }
        );
    });
  }

	async getUrlElement(url:any) {
		// var request = new Request(url);

		// fetch(request, {mode: 'no-cors'})
		// .then(function(response) {
		// 	console.log("RESPONSE", response); 
		// }).catch(function(error) {  
		// 	console.log('Request failed', error)  
		// });
		try {
			let result = await this.orderhistoryService.getHtmlElement(url);
			console.warn("result", result);
		} catch {
			console.warn("Error");
		}
	}

	getUrlImg(url:any) {
		console.warn("MASUK");
		let imageTags = url.getElementsByTagName("img"); // Returns array of <img> DOM nodes
		let sources = [];
		for (var i in imageTags) {
			var src = imageTags[i].src;
			sources.push(src);
		}
		console.warn("sources img", sources);
	}
}
