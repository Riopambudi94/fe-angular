import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// import { BrowserModule } from '@angular/platform-browser';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { BsComponentModule } from '../../bs-component/bs-component.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DeliveryProcessShippingComponent } from './delivery-process.shipping.component';
import { TranslateModule } from '@ngx-translate/core';
// import { ReceiptComponent } from '../receipt/receipt.component';
import { NgxBarcodeModule } from 'ngx-barcode';
import {MatButtonModule} from '@angular/material/button';
import {MatRadioModule} from '@angular/material/radio';
import {MatCheckboxModule} from '@angular/material/checkbox';

@NgModule({
  imports:      [ 
    CommonModule,
    FormsModule, 
    TranslateModule,
    ReactiveFormsModule,
    NgbModule,
    BsComponentModule,
    NgxBarcodeModule,
    MatButtonModule,
    MatRadioModule,
    MatCheckboxModule
    // ReceiptComponent
  ],
  declarations: [ DeliveryProcessShippingComponent ],
  exports:[ DeliveryProcessShippingComponent ],
  bootstrap:    [ DeliveryProcessShippingComponent ],
})
export class OrderHistoriesShippingModule { }