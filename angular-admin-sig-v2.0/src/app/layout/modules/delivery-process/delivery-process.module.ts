import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DeliveryProcessComponent } from './delivery-process.component';
import { FormInputComponent }  from '../../../component-libs/form-input/form-input.component';
import { PageHeaderModule } from '../../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { DeliveryProcessRoutingModule } from './delivery-process-routing.module';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { NgxBarcodeModule } from 'ngx-barcode';
import { ShippingLabelModule } from './shipping-label/shipping-label.module';
import { ShippingLabelComponent } from './shipping-label/shipping-label.component';
// import { OrderHistoriesAddComponent } from './add/order-histories.add.component';
// import { OrderHistoriesDetailComponent } from './detail/order-histories.detail.component';
import { DeliveryProcessEditComponent } from './edit/delivery-process.edit.component';
import { DeliveryProcessShippingComponent } from './shipping/delivery-process.shipping.component';

@NgModule({
  imports: [
    CommonModule,
    DeliveryProcessRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule,
    MatCheckboxModule,
    NgxBarcodeModule,
    ShippingLabelModule
  ],
  
  declarations: [
    DeliveryProcessComponent,
    DeliveryProcessEditComponent,
    DeliveryProcessShippingComponent
    // ShippingLabelComponent
  ],

  // exports : [OrderHistoriesComponent]
})
export class DeliveryProcessModule { }
