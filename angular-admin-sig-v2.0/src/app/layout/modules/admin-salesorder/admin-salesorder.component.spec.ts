import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminSalesorderComponent } from './admin-salesorder.component';

describe('AdminSalesorderComponent', () => {
  let component: AdminSalesorderComponent;
  let fixture: ComponentFixture<AdminSalesorderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminSalesorderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminSalesorderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
