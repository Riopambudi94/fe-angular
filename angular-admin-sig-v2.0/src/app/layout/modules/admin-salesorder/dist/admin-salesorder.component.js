"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.AdminSalesorderComponent = void 0;
var core_1 = require("@angular/core");
var AdminSalesorderComponent = /** @class */ (function () {
    function AdminSalesorderComponent(OrderhistoryService) {
        this.OrderhistoryService = OrderhistoryService;
        this.swaper = true;
        this.Pointstransaction = [];
        this.tableFormat = {
            title: 'Sales Order Report Page (Product)',
            label_headers: [
                { label: 'Tgl. Order', visible: true, type: 'date', data_row_name: 'order_date' },
                { label: 'No Order', visible: true, type: 'string', data_row_name: 'invoice_id' },
                { label: 'Member ID', visible: true, type: 'string', data_row_name: 'member_email' },
                { label: 'Name', visible: true, type: 'string', data_row_name: 'member_name' },
                { label: 'Item Code', visible: true, type: 'string', data_row_name: 'product_code' },
                { label: 'Item Description', visible: true, type: 'string', data_row_name: 'description' },
                { label: 'Category', visible: true, type: 'string', data_row_name: 'categories' },
                { label: 'Merchant', visible: true, type: 'string', data_row_name: 'merchant_username' },
                { label: 'Qty', visible: true, type: 'string', data_row_name: 'quantity' },
                { label: 'Unit Price', visible: true, type: 'string', data_row_name: 'fixed_price' },
                { label: 'Total Order', visible: true, type: 'string', data_row_name: 'total_product_price' },
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: '_id',
                "this": this,
                result_var_name: 'Pointstransaction',
                detail_function: [this, 'callDetail']
            }
        };
        this.tableFormat2 = {
            title: 'Sales Order Report Page (Order ID)',
            label_headers: [
                { label: 'Order Date', visible: true, type: 'date', data_row_name: 'order_date' },
                { label: 'No Order', visible: true, type: 'string', data_row_name: 'invoice_id' },
                { label: 'Member ID', visible: true, type: 'string', data_row_name: 'member_email' },
                { label: 'Product', visible: true, type: 'product', data_row_name: 'products' },
                { label: 'Item Code', visible: true, type: 'productcode', data_row_name: 'products' },
                { label: 'Item Description', visible: true, type: 'productdesc', data_row_name: 'products' },
                { label: 'Category', visible: true, type: 'productcat', data_row_name: 'products' },
                { label: 'Merchant', visible: true, type: 'productmerc', data_row_name: 'products' },
                { label: 'Qty', visible: true, type: 'productqty', data_row_name: 'products' },
                { label: 'Unit Price', visible: true, type: 'productunit', data_row_name: 'products' },
                { label: 'Total Order', visible: true, type: 'producttotal', data_row_name: 'products' },
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: '_id',
                "this": this,
                result_var_name: 'Pointstransaction',
                detail_function: [this, 'callDetail']
            }
        };
        this.form_input = {};
        this.errorLabel = false;
        this.totalPage = 0;
        this.pointstransactionDetail = false;
    }
    AdminSalesorderComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    AdminSalesorderComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var params, result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 5, , 6]);
                        params = {
                            search: { based_on: "products" },
                            current_page: 1,
                            limit_per_page: 50
                        };
                        this.service = this.OrderhistoryService;
                        result = {};
                        if (!(this.swaper == true)) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.OrderhistoryService.getSalesOrderReportint('product')];
                    case 1:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 2: return [4 /*yield*/, this.OrderhistoryService.getSalesOrderReportint('order_id')];
                    case 3:
                        result = _a.sent();
                        _a.label = 4;
                    case 4:
                        this.totalPage = result.result.total_page;
                        this.Pointstransaction = result.result.values;
                        return [3 /*break*/, 6];
                    case 5:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    AdminSalesorderComponent.prototype.swapClick = function (bool) {
        this.swaper = bool;
        this.firstLoad();
    };
    AdminSalesorderComponent.prototype.callDetail = function (pointstransaction_id) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    AdminSalesorderComponent.prototype.backToHere = function (obj) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                obj.pointstransactionDetail = false;
                return [2 /*return*/];
            });
        });
    };
    AdminSalesorderComponent.prototype.addMonths = function (date, months) {
        date.setMonth(date.getMonth() + months);
        return date;
    };
    AdminSalesorderComponent = __decorate([
        core_1.Component({
            selector: 'app-admin-salesorder',
            templateUrl: './admin-salesorder.component.html',
            styleUrls: ['./admin-salesorder.component.scss']
        })
    ], AdminSalesorderComponent);
    return AdminSalesorderComponent;
}());
exports.AdminSalesorderComponent = AdminSalesorderComponent;
