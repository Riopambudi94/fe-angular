import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { MemberService } from '../../../../services/member/member.service';
import { del } from 'selenium-webdriver/http';
import { Router, ActivatedRoute } from '@angular/router';
import * as content from '../../../../../assets/json/content.json';

@Component({
  selector: 'app-data-member-detail',
  templateUrl: './data-member.detail.component.html',
  styleUrls: ['./data-member.detail.component.scss'],
  animations: [routerTransition()]
})

export class DataMemberDetailComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  public contentList : any = (content as any).default;

  mci_project: any = false;

  edit: boolean = false;
  isChangePassword: boolean = false;
  isShowPassword: boolean = false;
  errorLabel : any = false;
  errorMessage  : any = false;
  memberDetail : any;
  memberPassword : any;
  constructor(public memberService: MemberService,private router: Router,private route: ActivatedRoute) {}

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad(){
    const program = localStorage.getItem('programName');
    let _this = this;
    this.contentList.filter(function(element) {
        if(element.appLabel == program) {
            if(element.type == "reguler") {
                _this.mci_project = true;
            } else {
                _this.mci_project = false;
            }
        }
    });
    
    let id_pel = this.detail.username;
    this.memberPassword = this.detail.password;
    this.memberDetail = this.detail;
    try {
      const result : any= await this.memberService.getFormMemberByID(id_pel);
      if(result.length > 0){
        if(result[0].username == this.detail.username){
          this.memberDetail = result[0];
          this.memberDetail.password = this.memberPassword;
          // console.warn("member detail data", this.memberDetail)
        }
      }
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type

      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
      }
    }
  }

  downloadImage(url : any) {
    let filename : any;
    filename = this.lastPathURL(url);
    fetch(url).then(function(t) {
        return t.blob().then((b)=>{
            var a = document.createElement("a");
            a.href = URL.createObjectURL(b);
            a.setAttribute("download", filename);
            a.click();
        }
        );
    });
    }

  isArray(curVar : any){
    return Array.isArray(curVar) ? true : false;
  }

  convertDate(time){
    const date:any =  new Date(time);
    const dateString = date.toString();
    const retDate = dateString.split("GMT");
    return retDate[0];
  }

  lastPathURL(url:string){
    let resURL : any = url.split("/");
    console.log("resURL", resURL);
    let lastNumber : number = resURL.length - 1;
    return resURL[lastNumber];
  }

  editThis() {
    // console.log(this.edit );
    this.edit = !this.edit;
    if(this.edit == false){
      this.firstLoad();
    }
    // console.log(this.edit );
  }

  changePassword() {
    this.isChangePassword = !this.isChangePassword;
    if(this.isChangePassword == false){
      this.firstLoad();
    }
  }

  showPassword() {
    this.isShowPassword = !this.isShowPassword;
    if(this.isShowPassword == false){
      this.firstLoad();
    }
  }

  backToTable() {
    // console.log("back member detail",this.back);
    this.back[1](this.back[0]);
  }

  validURL(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
      '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
    return !!pattern.test(str);
  }

  async delete(){
    try {
      const delResult: any = await this.memberService.deleteMember(this.detail);
      // console.log(delResult);
      if (delResult.error == false) {
        console.log(this.back[0]);
        this.back[0].memberDetail = false;
        this.back[0].firstLoad();
      // delete this.back[0].prodDetail;
      }
    } catch (error) {
      throw new TypeError(error.error.error);
    }
  } catch (e) {
    this.errorLabel = ((<Error>e).message);//conversion to Error type
  }

  async pointHistory(){
    console.log('here')
    // this.router.navigate(['/member-point-history']);
    this.router.navigate(['administrator/memberadmin/point-history'],  {queryParams: {id: this.detail.email }})
  }

}
