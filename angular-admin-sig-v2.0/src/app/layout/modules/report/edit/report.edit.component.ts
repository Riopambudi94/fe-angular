import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { ReportService } from '../../../../services/report/report.service';


@Component({
  selector: 'app-report-edit',
  templateUrl: './report.edit.component.html',
  styleUrls: ['./report.edit.component.scss'],
  animations: [routerTransition()]
})

export class ReportEditComponent implements OnInit {
  @Input() public detail: any;
  // @Input() public back;

  public loading        :boolean = false;

//   public transactionStatus  : any = [
//     {label:"PENDING", value:"PENDING"}
//    ,{label:"CANCEL", value:"CANCEL"}
//    ,{label:"FAILED", value:"FAILED"}
//    ,{label:"SUCCESS", value:"SUCCESS"}
//    ,{label:"ERROR", value:"ERROR"}
// ];
  errorLabel    : any = false;


  constructor(public reportService:ReportService) {}

  ngOnInit() {
    this.firstLoad();
    
  }

  async firstLoad(){
    // this.detail.previous_status = this.detail.status;
    // this.transactionStatus.forEach((element, index) => {
    //     if(element.value == this.detail.transaction_status){
    //         this.transactionStatus[index].selected = 1;
    //     }
    // });

    // this.detail.previous_member_id = this.detail.member_id;
    // this.memberStatus.forEach((element, index) => {
    //     if(element.value == this.detail.member_status){
    //         this.memberStatus[index].selected = 1;
    //     }
    // });
    
  }

  // backToDetail(){
  //   //console.log(this.back);
  //   this.back[0][this.back[1]]();
  // }

  async saveThis(){

    // try 
    // {
    //   this.loading=!this.loading;
    //   await this.reportService.updateReport(this.detail);
    //   this.loading=!this.loading;
    // } 
    // catch (e) 
    // {
    //   this.errorLabel = ((<Error>e).message);//conversion to Error type
    // }

  }
  
}
