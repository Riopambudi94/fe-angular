import { versioningService } from '../../../../services/versioning/versioning.service';
import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';


@Component({
  selector: 'app-version-detail',
  templateUrl: './versioning.detail.component.html',
  styleUrls: ['./versioning.detail.component.scss'],
  animations: [routerTransition()]
})

export class VersioningDetailComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  edit:boolean = false;

  errorLabel    : any = false;
  constructor(public versioningService :versioningService ) {
  }

  ngOnInit() {
    this.firstLoad();
    
  }

  async firstLoad(){

  }

  async editThis(){
    this.edit = !this.edit;
    console.log(this.edit)
  }

  backToTable(){
    this.back[1](this.back[0]);
  }

  // async deleteThis(){
  //   try{
  //     let delResult: any = await this.versioningService.deleteVersioning
  //     console.log(delResult);
  //     if(delResult.error==false){
  //         console.log(this.back[0]);
  //         this.back[0].versionDetail=false;
  //         this.back[0].firstLoad();
  //         // delete this.back[0].prodDetail;
  //     } 
  //   } catch (e) {
  //       this.errorLabel = ((<Error>e).message);//conversion to Error type
  //   }      
  
}
