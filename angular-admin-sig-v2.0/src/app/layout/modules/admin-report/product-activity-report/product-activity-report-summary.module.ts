import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductActivityReportSummaryComponent } from './product-activity-report-summary.component';
import { FormInputComponent }  from '../../../../component-libs/form-input/form-input.component';
import { PageHeaderModule } from '../../../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../../../../component-libs/form-builder-table/form-builder-table.module';
import { FormSelectComponent } from '../../../../component-libs/form-select/form-select.component';
import { ProductActivityReportSummaryRoutingModule } from './product-activity-report-summary-routing.module';
import { BsComponentModule } from '../../bs-component/bs-component.module';
import { ChartsModule as Ng2Charts, ChartsModule } from 'ng2-charts';

@NgModule({
  imports: [CommonModule,
    ProductActivityReportSummaryRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule,
    ChartsModule
],
  
  declarations: [
    ProductActivityReportSummaryComponent]
})
export class ProductActivityReportSummaryModule { }
