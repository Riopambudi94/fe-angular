import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MemberDemographyReportComponent } from './member-demography-report.component';
import { PageHeaderModule } from '../../../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../../../../component-libs/form-builder-table/form-builder-table.module';
import { BsComponentModule } from '../../bs-component/bs-component.module';
import { ChartsModule } from 'ng2-charts';
import { MemberDemographyReportRoutingModule } from './member-demography-report-routing.module';

@NgModule({
  declarations: [MemberDemographyReportComponent],
  imports: [
    CommonModule,
    MemberDemographyReportRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule,
    ChartsModule
  ]
})
export class MemberDemographyReportModule { }
