import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { OrderhistoryService } from '../../../services/orderhistory/orderhistory.service';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';

@Component({
  selector: 'app-admin-shipping',
  templateUrl: './admin-shipping.component.html',
  styleUrls: ['./admin-shipping.component.scss']
})
export class AdminShippingComponent implements OnInit {
  Pointstransaction: any = [];
  tableFormat        : TableFormat = {
                                  title           : 'Shipping Report Page',
                                  label_headers   : [
                                    {label: 'Tgl. Order', visible: true, type: 'date', data_row_name: 'created_date'},
                                    {label: 'No. Order', visible: true, type: 'string', data_row_name: 'invoice_id'},
                                    {label: 'No. Resi', visible: true, type: 'string', data_row_name: 'awb_number'},
                                    {label: 'Destination', visible: true, type: 'string', data_row_name: 'destination'},
                                    {label: 'Member ID', visible: true, type: 'string', data_row_name: 'member_email'},
                                    {label: 'Name', visible: true, type: 'string', data_row_name: 'member_name'},
                                    {label: 'Item Code', visible: true, type: 'productcode', data_row_name: 'products'},
                                    // {label: 'Item Description', visible: true, type: 'productdesc', data_row_name: 'products'},
                                    {label: 'Category', visible: true, type: 'productcat', data_row_name: 'products'},
                                    {label: 'Weight', visible: true, type: 'productweight', data_row_name: 'products'},
                                    {label: 'Dimensi', visible: true, type: 'productdim', data_row_name: 'products'},
                                    {label: 'Qty', visible: true, type: 'productqty', data_row_name: 'products'},
                                    {label: 'Unit Price', visible: true, type: 'productunit', data_row_name: 'products'},  
                                    {label: 'Total Order', visible: true, type: 'producttotal', data_row_name: 'products'}, 
                                    // {label: 'Invoice ID', visible: true, type: 'string', data_row_name: 'invoice_id'},
                                    {label: 'Shipping Via', visible: true, type: 'string', data_row_name: 'courier'},
                                    {label: 'Shipping Cost', visible: true, type: 'string', data_row_name: 'shipping_fee'},
                                  ],
                                  row_primary_key : '_id',
                                  formOptions     : {
                                                    row_id: '_id',
                                                    this  : this,
                                                    result_var_name: 'Pointstransaction',
                                                    detail_function: [this, 'callDetail']
                                                  }
  };
  form_input    : any = {};
  errorLabel : any = false;
  totalPage  = 0;
  swaper= true;
  pointstransactionDetail: any = false;
  service: any;
  constructor(public OrderhistoryService:OrderhistoryService) { }

  ngOnInit() {
    this.firstLoad();

  }
  
  async firstLoad(){
    try{
      let result: any
      var params = {
         search: {start_date:this.addMonths(new Date(), -6),end_date:new Date()},
         current_page:1,
         limit_per_page:50
        }
      this.service    = this.OrderhistoryService;
      if(this.swaper == true){
        result = await this.OrderhistoryService.getShippingReportint('product');
      }else{
        result= await this.OrderhistoryService.getShippingReportint('order_id');
      }
    
      // result = await this.OrderhistoryService.getShippingReportint(params);
      this.totalPage = result.result.total_page
      this.Pointstransaction = result.result.values;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }
  swapClick(bool) {
    this.swaper = bool;
    this.firstLoad();
  }
  // public async callDetail(pointstransaction_id){
  //   try{
  //     let result: any;
  //     this.service    = this.OrderhistoryService;
  //     result          = await this.OrderhistoryService.detailPointstransaction(pointstransaction_id);
  //     console.log(result);

  //     this.pointstransactionDetail = result.result[0];

  //   } catch (e) {
  //     this.errorLabel = ((<Error>e).message);//conversion to Error type
  //   }
  // }

  public async backToHere(obj){
    obj.pointstransactionDetail = false;
  }
  addMonths(date, months) {
    date.setMonth(date.getMonth() + months);
    return date;
  }
}
