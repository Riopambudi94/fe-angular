import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EvoucherSalesReportComponent } from './evoucher-sales-report.component';
import { MerchantEvoucherSalesReportComponent } from '../../../merchant-portal/merchant-evoucher-sales-report/merchant-evoucher-sales-report.component';


const routes: Routes = [
  {path: '', component: MerchantEvoucherSalesReportComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EvoucherSalesReportRoutingModule { }
