import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { MembershipService } from '../../../services/membership/membership.service';
import { DomSanitizer } from '@angular/platform-browser';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';

// import { Http, Response, Headers, RequestOptions, RequestMethod, RequestOptionsArgs, URLSearchParams, ResponseContentType } from '@angular/http';
// import { Observable } from 'rxjs';
// import { map } from 'rxjs/operators';
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/catch';
// import 'rxjs/Rx' ;


@Component({
  selector:     'app-membership',
  templateUrl:  './membership.component.html',
  styleUrls:    ['./membership.component.scss'],
  animations:   [routerTransition()]
})

export class MembershipComponent implements OnInit {

  Memberships   : any = [];
  tableFormat   : TableFormat = {
                  title           : 'Active Memberships Detail',
                  label_headers   : [
                                {label: 'Name',              visible: true, type: 'string', data_row_name: 'name'},
                                {label: 'Transaction Reach', visible: true, type: 'string', data_row_name: 'transaction_reach'},
                                {label: 'Description',       visible: true, type: 'string', data_row_name: 'description'}],
                  row_primary_key : '_id',
                  formOptions     : {
                                    row_id: '_id',
                                    this  : this,
                                    result_var_name: 'Memberships',
                                    detail_function: [this, 'callDetail']
                                  }
                                };
  form_input    : any = {};
  errorLabel : any = false;
  // table_title   : any = 'Active memberships detail';
  // table_headers : any = ['Name', 'Transaction Reach', 'Descriptions'];
  // table_rows    : any = ['name', 'transaction_reach', 'description'];
  // row_id        : any = '_id';
  // form_input    : any = {};
  // errorLabel    : any = false;
  // formOptions   : any = {
  //                     addForm   : true,
  //                     row_id    : this.row_id,
  //                     this      : this,
  //                     result_var_name : 'Memberships',
  //                     detail_function : [this, 'callDetail']
  //                   }
  membershipDetail: any = false;
  service       : any ;
  srcDownload   : any ;
  

  // private options = new RequestOptions(
  //   { headers: new Headers({ 'Content-Type': 'application/json' }) });
  constructor(public membershipService:MembershipService , public sanitizer:DomSanitizer) {

    this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl('');
  }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    try {
      this.service        = this.membershipService;
      const result: any   = await this.membershipService.getMembershipsLint();
      this.Memberships    = result.result;
    } catch (e) {
      this.errorLabel     = ((<Error>e).message);//conversion to Error type
    }

  }

  public async callDetail(membership_id) {
    
    try {
      this.service    = this.membershipService;
      let result: any = await this.membershipService.detailMembership(membership_id);
      this.membershipDetail = result.result[0];
      console.log(this.membershipDetail);
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async backToHere(obj) {
    obj.membershipDetail = false;
  }

  onDownload(downloadLint)
  {
    let srcDownload   = downloadLint;
    this.srcDownload  = this.sanitizer.bypassSecurityTrustResourceUrl(srcDownload);

  }

  public async getDownloadFileLint() {
    try
    {

      let result: any;
      this.service     = this.membershipService;
          result       = await this.membershipService.getDownloadFileLint();
      let downloadLint = result.result;

      //To running other subfunction with together automatically
      this.onDownload(downloadLint);
    } 
    catch (e) 
    {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }

   

  }


}
