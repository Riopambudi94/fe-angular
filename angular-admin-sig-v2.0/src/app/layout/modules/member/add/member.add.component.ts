import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { MemberService } from '../../../../services/member/member.service';

@Component({
  selector: 'app-member-add',
  templateUrl: './member.add.component.html',
  styleUrls: ['./member.add.component.scss'],
  animations: [routerTransition()]
})

export class MemberAddComponent implements OnInit {
  public name: string = "";
  Members: any = [];
  service: any;
  full_name:any;
  email:any;
  password:any;
  cell_phone:any;
  card_number:any;
  dob:any;
  mcity:any;
  mstate:any;
  mcountry:any;
  maddress1:any;
  mpostcode:any;
  gender:any;
  marital_status:any;
  type:any;
  member_status:any;
  activation_status:any;
  public form: any = {
    username: "",
    id: "",
    full_name: "",
    email: "",
    password:"",
    cell_phone: "",
    card_number: "",
    dob: "",
    mcity:"",
    mcountry:"",
    maddress1:"",
    mpostcode:"",
    gender:"",
    member_status:"",
    marital_status:"",
    address2:"",
  }
  
  errorLabel : any = false;
  public member_status_type = [
                       {label:'Superuser', value:'superuser'}
                      ,{label:'Member', value:'member', selected:1}
                      ,{label:'Marketing', value:'marketing'}
                      ,{label:'Admin', value:'admin'}
                      ,{label:'Merchant', value:'merchant'}
                    ];
  
  public gender_type: any = [
                      {label:'Famele', value:'female'}
                      ,{label:'Male', value:'male'}
  ];

  public marital_status_type: any = [
                      {label:'Married', value:'married'}
                      ,{label:'Unmarried', value:'unmarried'}
];
                
  constructor(public memberService:MemberService) {
    // let form_add_test     : any = [
    //   { label:"Member ID",  type: "text",  value: "", data_binding: 'id'  },
    //   { label:"Full Name",  type: "text",  value: "", data_binding: 'full_name'  },
    //   { label:"Email",  type: "text",  value: "", data_binding: 'email'  },
    //   { label:"Password",  type: "password",  value: "", data_binding: 'password'  },
    //   { label:"Cell Phone",  type: "text",  value: "", data_binding: 'cell_phone'  },
    //   { label:"Card Number ",  type: "text",  value: "", data_binding: 'card_number' },
    //   { label:"Date of Birth",  type: "text",  value: "", data_binding: 'dob' },
    //   { label:"City",  type: "text",  value: "", data_binding: 'mcity' },
    //   { label:"State",  type: "text",  value: "", data_binding: 'mstate' },
    //   { label:"Country",  type: "text",  value: "", data_binding: 'mcountry' },
    //   { label:"Address",  type: "textarea",  value: "", data_binding: 'maddress1' },
    //   { label:"Post Code",  type: "text",  value: "", data_binding: 'mpostcode' },
    //   { label:"Gender",  type: "text",  value: "", data_binding: 'gender' },
    //   { label:"Member Status",  type: "text",  value: "", data_binding: 'member_status' },
    //   { label:"Marital Status",  type: "text",  value: "", data_binding: 'marital_status' },
    //   // { label:"type ",  type: "textarea",  value: [{label:"label", value:"label", selected:1},{label:"label1", value:"label2"}], data_binding: 'address2' },
    //   { label:"address 2 ",  type: "textarea",  value: "", data_binding: 'address2' },
    // ];
   }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad(){
    // this.service = this.memberService;
    // let result: any = await this.memberService.getMembersLint();
    // this.Members = result.result;  

  }
  formSubmitAddMember(){
    // console.log(JSON.stringify(form));
    // console.log(form.password)
    let form_add = this.form;
    if(!form_add.password || form_add.password.length < 8){
      alert("password must be longer than 8 character")
    }
    else{
      try {
        this.service    = this.memberService;
        let dob;
        let dataForm;
        if(form_add.dob){
          let month = form_add.dob.month.toString().padStart(2, '0')
          let day = form_add.dob.day.toString().padStart(2,'0')
          dob = form_add.dob.year + "-" + month +"-"+day
          dataForm = { ...form_add, ...{ dob } };
        }
        else{
          dataForm = form_add;
        }
        console.log(dataForm);
        
        const result: any  = this.memberService.addNewMember(dataForm);
        this.Members = result.result;
        alert("Member Has Been Added!")
      } catch (e) {
        this.errorLabel = ((<Error>e).message);//conversion to Error type
      }
    }

}

backTo(){
  window.history.back();
}

}
