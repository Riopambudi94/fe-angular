import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotificationGroupComponent } from './notificationgroup.component';
import { NotificationGroupAddComponent } from './add/notificationgroup.add.component';
// import { NotificationDetailComponent } from './detail/notification.detail.component';


const routes: Routes = [
  {
      path: '', component: NotificationGroupComponent,

  },
  {
      path:'add', component: NotificationGroupAddComponent
  },
  // {
  //   path:'detail', component: NotificationDetailComponent
  // }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotificationGroupRoutingModule { }
