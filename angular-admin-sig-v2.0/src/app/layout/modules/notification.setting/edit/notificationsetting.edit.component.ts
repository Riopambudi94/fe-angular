import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { NotificationsettingService } from '../../../../services/notificationsetting/notificationsetting.service';


@Component({
  selector: 'app-notificationsetting-edit',
  templateUrl: './notificationsetting.edit.component.html',
  styleUrls: ['./notificationsetting.edit.component.scss'],
  animations: [routerTransition()]
})

export class NotificationsettingEditComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  public loading        :boolean = false;
  status:any;
  
  public notificationStatus  : any = [
    {label:"ACTIVE", value:"ACTIVE"}
   ,{label:"iNACTIVE", value:"iNACTIVE"}
];
  errorLabel    : any = false;


  constructor(public notificationsettingService:NotificationsettingService) {}

  ngOnInit() {
    this.firstLoad();
  }

  async firstLoad(){
    this.detail.previous_status = this.detail.status;
    this.notificationStatus.forEach((element, index) => {
        if(element.value == this.detail.status){
            this.notificationStatus[index].selected = 1;
        }
    });

    // this.detail.previous_member_id = this.detail.member_id;
    // this.memberStatus.forEach((element, index) => {
    //     if(element.value == this.detail.member_status){
    //         this.memberStatus[index].selected = 1;
    //     }
    // });
    
  }

  backToDetail(){
    //console.log(this.back);
    this.back[0][this.back[1]]();
  }

  async saveThis(){
    try 
    {
      this.loading=!this.loading;
      await this.notificationsettingService.updateNotificationsettingID(this.detail);
      this.loading=!this.loading;
    } 
    catch (e) 
    {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }
  
}
