import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PointstransactionComponent } from './points-transaction.component';

const routes: Routes = [
    {
        path: '', component: PointstransactionComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PointstransactionRoutingModule {
}
