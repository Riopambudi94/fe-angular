import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { PointsTransactionService } from '../../../services/points-transaction/points-transaction.service';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';

@Component({
  selector: 'app-pointstransaction',
  templateUrl: './points-transaction.component.html',
  styleUrls: ['./points-transaction.component.scss'],
  animations: [routerTransition()]
})

export class PointstransactionComponent implements OnInit {

  Pointstransaction: any = [];
  tableFormat        : TableFormat = {
                                  title           : 'Report Point Transaction',
                                  label_headers   : [
                                    {label: 'Reference', visible: true, type: 'string', data_row_name: 'reference_no'},
                                    {label: 'Created Date', visible: true, type: 'date', data_row_name: 'created_date'},
                                    {label: 'Description', visible: true, type: 'string', data_row_name: 'description'},
                                    {label: 'Total Order', visible: true, type: 'string', data_row_name: 'total_order'},
                                    {label: 'Points', visible: true, type: 'string', data_row_name: 'points'},
                                    {label: 'Status Point', visible: true, type: 'string', data_row_name: 'category'},
                                    {label: 'Prev Balance', visible: true, type: 'string', data_row_name: 'prev_points_balance'},
                                    {label: 'Current Balance', visible: true, type: 'string', data_row_name: 'current_points_balance'},
                                    {label: 'Member Email', visible: true, type: 'string', data_row_name: 'member_email'},
                                    {label: 'Member Name', visible: true, type: 'string', data_row_name: 'member_name'},
                                    // {label: 'Order ID', visible: true, type: 'string', data_row_name: 'reference_no'},
                                    // {label: 'Description', visible: true, type: 'string', data_row_name: 'description'},
                                    
                                  
                                    // {label: 'Updated Date', visible: true, type: 'date', data_row_name: 'updated_date'},
                                  ],
                                  row_primary_key : '_id',
                                  formOptions     : {
                                                    row_id: '_id',
                                                    this  : this,
                                                    result_var_name: 'Pointstransaction',
                                                    detail_function: [this, 'callDetail']
                                                  }
  };
  form_input    : any = {};
  errorLabel : any = false;
  totalPage  = 0;

  pointstransactionDetail: any = false;
  service: any;
  constructor(public PointsTransactionService:PointsTransactionService) { }

  ngOnInit() {
    this.firstLoad();

  }
  
  async firstLoad(){
    try{
      var params = {
        //  search: {start_date:this.addMonths(new Date(), -6),end_date:new Date()},
         current_page:1,
         limit_per_page:50
        }
      this.service    = this.PointsTransactionService;
      let result: any  = await this.PointsTransactionService.getPointstransactionLint(params);
      this.totalPage = result.result.total_page
      

      this.Pointstransaction = result.result.values;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }
  
  public async callDetail(pointstransaction_id){
    try{
      let result: any;
      this.service    = this.PointsTransactionService;
      result          = await this.PointsTransactionService.detailPointstransaction(pointstransaction_id);
      console.log(result);

      this.pointstransactionDetail = result.result[0];

    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async backToHere(obj){
    obj.pointstransactionDetail = false;
  }
  addMonths(date, months) {
    date.setMonth(date.getMonth() + months);
    return date;
  }
}
