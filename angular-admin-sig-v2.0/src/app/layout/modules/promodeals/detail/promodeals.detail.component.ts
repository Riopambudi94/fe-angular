import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { PromodealsService } from '../../../../services/promodeals/promodeals.service';

@Component({
  selector: 'app-promodeals-detail',
  templateUrl: './promodeals.detail.component.html',
  styleUrls: ['./promodeals.detail.component.scss'],
  animations: [routerTransition()]
})

export class PromodealsDetailComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  edit = false;

  errorLabel:any;

  constructor(public PromodealsService: PromodealsService) {
  }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {


  }

  editThis() {
    console.log(this.edit );
    this.edit = !this.edit;
    console.log(this.edit );
  }

  backToTable() {
    console.log(this.back);
    this.back[1](this.back[0]);
  }

  async deleteThis(){
    try{
      let delResult: any = await this.PromodealsService.deletePromodeals(this.detail);
      //console.log(delResult);
      if(delResult.error == false){
          this.back[0].firstLoad();
          alert("Data has been removed");
          window.location.href = "/promodeals";
          // delete this.back[0].emailDetail;
      } 
    } catch (e) {
        this.errorLabel = ((<Error>e).message);//conversion to Error type
    }      
    
  }

}
