
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PaymentGatewayComponent } from './paymentgateway.component';
import { PaymentGatewayAddComponent } from './add/paymentgateway.add.component';
import { PaymentGatewayDetailComponent } from './detail/paymentgateway.detail.component';

const routes: Routes = [
  {
      path: '', component: PaymentGatewayComponent
  },
  {
    path:'add', component: PaymentGatewayAddComponent
  },
  {
    path:'detail', component: PaymentGatewayDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentGatewayRoutingModule { }
