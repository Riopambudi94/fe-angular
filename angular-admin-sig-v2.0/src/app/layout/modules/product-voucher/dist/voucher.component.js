"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.VouchersComponent = void 0;
var core_1 = require("@angular/core");
var router_animations_1 = require("../../../router.animations");
var VouchersComponent = /** @class */ (function () {
    function VouchersComponent(productService, router) {
        var _this = this;
        this.productService = productService;
        this.router = router;
        // this section belongs to product item list
        this.Products = [];
        this.tableFormat = {
            title: 'Voucher Data',
            label_headers: [
                { label: 'Merchant Name', visible: true, type: 'string', data_row_name: 'merchant_username' },
                { label: 'Product Name', visible: true, type: 'string', data_row_name: 'product_name' },
                { label: 'Product Code', visible: true, type: 'string', data_row_name: 'product_code' },
                // {label: 'Type', visible: true, type: 'string', data_row_name: 'type'},
                { label: 'Quantity', visible: true, type: 'string', data_row_name: 'qty' },
                { label: 'Active', visible: true, type: 'string', data_row_name: 'active' },
                { label: 'Approved', visible: true, type: 'string', data_row_name: 'approved' },
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: '_id',
                addForm: true,
                "this": this,
                result_var_name: 'Products',
                detail_function: [this, 'callDetail'],
                customButtons: [
                    { label: 'Approve', func: function (f) { _this.approval(f); } },
                ]
            },
            show_checkbox_options: true
        };
        this.form_input = {};
        this.errorLabel = false;
        this.totalPage = 0;
        this.page = 1;
        this.pageSize = 50;
        this.prodDetail = false;
        this.selectedFile = null;
        this.progressBar = 0;
        this.cancel = false;
        this.errorFile = false;
    }
    VouchersComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    VouchersComponent.prototype.approval = function (listedData) {
        return __awaiter(this, void 0, void 0, function () {
            var listOfData, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        // console.log("masuk coy")
                        if (listedData == undefined || listedData.length == 0) {
                            return [2 /*return*/, false];
                        }
                        listOfData = [];
                        //
                        listedData.forEach(function (element, index) {
                            listOfData.push(element._id);
                        });
                        listedData = [];
                        if (listOfData.length == 0)
                            return [2 /*return*/, false];
                        this.service = this.productService;
                        return [4 /*yield*/, this.productService.approvedProducts({ product_ids: listOfData })];
                    case 1:
                        result = _a.sent();
                        if (result.result.status == 'success') {
                            alert("the Current Data Below has been approved");
                            this.firstLoad();
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    VouchersComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        result = void 0;
                        this.service = this.productService;
                        return [4 /*yield*/, this.productService.getEvoucherReport()];
                    case 1:
                        result = _a.sent();
                        this.totalPage = result.result.total_page;
                        this.Products = result.result.values;
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    VouchersComponent.prototype.callDetail = function (_id) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.router.navigate(['administrator/productadmin/edit'], { queryParams: { id: _id } });
                return [2 /*return*/];
            });
        });
    };
    VouchersComponent.prototype.backToHere = function (obj) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                obj.prodDetail = false;
                return [2 /*return*/];
            });
        });
    };
    VouchersComponent.prototype.onFileSelected = function (event) {
        var _this = this;
        this.errorFile = false;
        var fileMaxSize = 3000000; // let say 3Mb
        Array.from(event.target.files).forEach(function (file) {
            if (file.size > fileMaxSize) {
                _this.errorFile = "maximum file is 3Mb";
            }
        });
        this.selectedFile = event.target.files[0];
    };
    VouchersComponent.prototype.updateProgressBar = function (value) {
        this.progressBar = value;
    };
    VouchersComponent.prototype.cancelThis = function () {
        this.cancel = !this.cancel;
    };
    VouchersComponent.prototype.callAfterUpload = function (result) {
        console.log("THE RESULT", result);
    };
    VouchersComponent.prototype.onUpload = function () {
        return __awaiter(this, void 0, void 0, function () {
            var e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        this.cancel = false;
                        if (!this.selectedFile) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.productService.uploadFile(this.selectedFile, this, 'product', this.callAfterUpload)];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        alert("Upload Success");
                        return [3 /*break*/, 4];
                    case 3:
                        e_2 = _a.sent();
                        this.errorLabel = (e_2.message); //conversion to Error type
                        return [3 /*break*/, 4];
                    case 4:
                        if (this.selectedFile) { }
                        return [2 /*return*/];
                }
            });
        });
    };
    VouchersComponent = __decorate([
        core_1.Component({
            selector: 'app-product',
            templateUrl: './voucher.component.html',
            styleUrls: ['./voucher.component.scss'],
            animations: [router_animations_1.routerTransition()]
        })
    ], VouchersComponent);
    return VouchersComponent;
}());
exports.VouchersComponent = VouchersComponent;
