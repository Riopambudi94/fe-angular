import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { ProductService } from '../../../../services/product/product.service';
import { del } from 'selenium-webdriver/http';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product.detail.component.html',
  styleUrls: ['./product.detail.component.scss'],
  animations: [routerTransition()]
})

export class ProductDetailComponent implements OnInit {
  // @Input() public detail: any;
  @Input() public back;

  productDetail;
  service;
  edit: boolean = false;
  errorLabel: any = false;
  constructor(public productService: ProductService, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    this.route.queryParams.subscribe(async (params)=>{
      let historyID = params.id;
      this.loadDetail(historyID)
    })
    // console.log("THIS data no data", this.detail);
  }

  private async loadDetail(historyID: string){
    try {
      this.service = this.productService;
      let result: any = await this.productService.detailProducts(historyID);
      this.productDetail = result.result[0];
      console.log(this.productDetail);
    } catch (error) {
      
    }
  }

  // async editThis(){
  //   this.edit = !this.edit;
  // }

  // backToHere() {
  //   history.back();
  // }

  // async deleteThis(){
  //   try{
  //     let delResult: any = await this.productService.deleteProduct(this.detail);
  //     console.log(delResult);
  //     if(delResult.error==false){
  //         console.log(this.back[0]);
  //         this.back[0].prodDetail=false;
  //         this.back[0].firstLoad();
  //         // delete this.back[0].prodDetail;
  //     } 
  //   } catch (e) {
  //       this.errorLabel = ((<Error>e).message);//conversion to Error type
  //   }      

  // }

}
