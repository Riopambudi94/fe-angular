import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageHeaderModule } from '../../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { ProductAddComponent } from './add/product.add.component';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
// import { NgxEditorModule } from 'ngx-editor';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { ProductDetailComponent } from './detail/product.detail.component';
import { ProductEditComponent } from './edit/product.edit.component';
import { MerchantEditProductComponent } from '../../merchant-portal/separated-modules/merchant-products/products/merchant-edit-product/merchant-edit-product.component';
import { MerchantAddNewProductComponent } from '../../merchant-portal/separated-modules/merchant-products/products/merchant-add-new-product/merchant-add-new-product.component';
import { ProductsModule } from '../../merchant-portal/separated-modules/merchant-products/products.module';
import { VouchersRoutingModule } from './voucher-routing.module';
import { VouchersComponent } from './voucher.component';


@NgModule({
  imports: [
    CommonModule, 
    VouchersRoutingModule, 
    PageHeaderModule, 
    FormsModule, 
    FormBuilderTableModule, 
    ReactiveFormsModule,
    // NgxEditorModule,
    NgbModule,
    HttpClientModule,
    CKEditorModule,
    ProductsModule
  ],
  declarations: [VouchersComponent, ProductAddComponent, 
    ProductDetailComponent,
    ProductEditComponent
  ]
})
export class VouchersModule { }
