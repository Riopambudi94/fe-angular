import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { OrderhistoryService } from '../../../services/orderhistory/orderhistory.service';
import { NumberFormatStyle } from '@angular/common';
import { FormOptions, TableFormat } from '../../../object-interface/common.object';

@Component({
  selector: 'app-salesorder',
  templateUrl: './salesorder.component.html',
  styleUrls: ['./salesorder.component.scss'],
  animations: [routerTransition()]
})

export class SalesorderComponent implements OnInit {
  Salesorder: any = [];
  tableFormat: TableFormat = {
    title: 'Sales Order Page',
    label_headers: [
      { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
      { label: 'Product Name', visible: true, type: 'string', data_row_name: 'product_name' },
      { label: 'Quantity', visible: true, type: 'string', data_row_name: 'quantity' },
      { label: 'Price', visible: true, type: 'string', data_row_name: 'price' },
      // { label: 'Discount Price', visible: false, type: 'string', data_row_name: 'discount_price' },
      // { label: 'Total Price', visible: false, type: 'string', data_row_name: 'total_price' },
      { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
      // { label: 'Email', visible: false, type: 'string', data_row_name: 'user_email' },
      // { label: 'Order Date', visible: false, type: 'string', data_row_name: 'order_date' },
      // { label: 'Username', visible: false, type: 'string', data_row_name: 'username' },
      // { label: 'Payment Method', visible: false, type: 'string', data_row_name: 'payment_method' },
      { label: 'Total Price', visible: true, type: 'string', data_row_name: 'total_price' },
      { label: 'Bank', visible: true, type: 'string', data_row_name: 'bank' },

      // unfinished
      // unfinished
      // unfinished
      {
        label: 'Type', visible: false,
        options: ['product', 'voucher'],
        type: 'list', data_row_name: 'type'
      },
      // { label: 'Payment Method', visible: true, type: 'string', data_row_name: 'payment_method' },
    ],
    row_primary_key: '_id',
    formOptions: {
      row_id: '_id',
      this: this,
      result_var_name: 'Salesorder',
      detail_function: [this, 'callDetail'],
    },
    show_checkbox_options: true


  };
  form_input: any = {};
  errorLabel: any = false;
  totalPage: 0;

  // salesorderDetail: any = false;
  service: any;
  constructor(public orderhistoryService: OrderhistoryService) { }

  ngOnInit() {
    this.firstLoad();

  }

  hello(f) {
    console.log("HELLO", f);
  }
  async firstLoad() {
    try {
      this.service = this.orderhistoryService;
      const result: any = await this.orderhistoryService.getSalesOrdeReport();
      this.totalPage = result.result.total_page;
      console.log(result);
      this.Salesorder = result.result.values;
    } catch (e) {
      this.errorLabel = ((<Error>e).message); // conversion to Error type
    }
  }

  // public async callDetail(orderhistory_id) {
  //   try {
  //     let result: any;
  //     this.service = this.orderhistoryService;
  //     // result       = await this.orderhistoryService.detailSalesorder(orderhistory_id);
  //     // console.log(result);
  //     this.salesorderDetail = result.result[0];

  //   } catch (e) {
  //     this.errorLabel = ((<Error>e).message); // conversion to Error type
  //   }
  // }

  // public async backToHere(obj) {
  //   obj.salesorderDetail = false;
  // }

}
