import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { MemberService } from '../../../services/member/member.service';
import { DomSanitizer } from '@angular/platform-browser';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';
import Swal from 'sweetalert2';
import * as content from '../../../../assets/json/content.json';

// import { Http, Response, Headers, RequestOptions, RequestMethod, RequestOptionsArgs, URLSearchParams, ResponseContentType } from '@angular/http';
// import { Observable } from 'rxjs';
// import { map } from "rxjs/operators";
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/catch';
// import 'rxjs/Rx' ;


@Component({
  selector: 'app-form-member',
  templateUrl: './form-member.component.html',
  styleUrls: ['./form-member.component.scss'],
  animations: [routerTransition()]
})

export class FormMemberComponent implements OnInit {
  mci_project: any = false;
  Members       : any = [];
  row_id        : any = "_id";

  tableFormat   : TableFormat = {
    title           : 'Form Member',
    label_headers   : [
                  {label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'username'},
                  {label: 'Data Complete', visible: true, type: 'data_complete', data_row_name: 'additional_info'},
                  {label: 'Nama Toko', visible: true, type: 'form_nama_toko', data_row_name: 'input_form_data'},
                  {label: 'Nama Distributor', visible: true, type: 'form_nama_distributor', data_row_name: 'input_form_data'},
                  {label: 'Alamat Toko',     visible: true, type: 'form_alamat_toko', data_row_name: 'input_form_data'},
                  {label: 'Nama Pemilik',     visible: true, type: 'form_nama_pemilik', data_row_name: 'input_form_data'},
                  {label: 'No. KTP Pemilik',     visible: true, type: 'form_ktp_pemilik', data_row_name: 'input_form_data'},
                  {label: 'Foto KTP Pemilik',     visible: true, type: 'form_foto_ktp_pemilik', data_row_name: 'input_form_data'},
                  {label: 'No. NPWP Pemilik',     visible: true, type: 'form_npwp_pemilik', data_row_name: 'input_form_data'},
                  {label: 'Foto NPWP Pemilik',     visible: true, type: 'form_foto_npwp_pemilik', data_row_name: 'input_form_data'},
                  {label: 'No. Telp Pemilik',     visible: true, type: 'form_telp_pemilik', data_row_name: 'input_form_data'},
                  {label: 'No. WA Pemilik',     visible: true, type: 'form_wa_pemilik', data_row_name: 'input_form_data'},
                  {label: 'Alamat Pengiriman',     visible: true, type: 'form_alamat_penerima', data_row_name: 'input_form_data'},
                  {label: 'Hadiah Dikuasakan',     visible: true, type: 'form_hadiah_dikuasakan', data_row_name: 'input_form_data'},
                  {label: 'Nama penerima',     visible: true, type: 'form_nama_penerima', data_row_name: 'input_form_data'},
                  {label: 'No. KTP Penerima',     visible: true, type: 'form_ktp_penerima', data_row_name: 'input_form_data'},
                  {label: 'Foto KTP Penerima',     visible: true, type: 'form_foto_ktp_penerima', data_row_name: 'input_form_data'},
                  {label: 'No. NPWP Penerima',     visible: true, type: 'form_npwp_penerima', data_row_name: 'input_form_data'},
                  {label: 'Foto NPWP Penerima',     visible: true, type: 'form_foto_npwp_penerima', data_row_name: 'input_form_data'},
                  {label: 'No. WA Penerima',     visible: true, type: 'form_wa_penerima', data_row_name: 'input_form_data'},
                  {label: 'No. Gopay Penerima',     visible: true, type: 'form_gopay_penerima', data_row_name: 'input_form_data'},
                  {label: 'Group',     visible: true, type: 'form_group', data_row_name: 'input_form_data'},
                  {label: 'Program/Cluster',     visible: true, type: 'form_cluster', data_row_name: 'input_form_data'},
                  {label: 'Created Date',     visible: true, type: 'date', data_row_name: 'created_date'},
                  {label: 'Updated Date',     visible: true, type: 'date', data_row_name: 'updated_date'},

                ],
    row_primary_key : '_id',
    formOptions     : {
                    addForm   : true,
                    bulkUpdate   : true,
                    row_id    : this.row_id,
                    this      : this,
                    result_var_name : 'Members',
                    detail_function : [this, 'callDetail'] ,
                    setCompleteMember: true
                    },
                    show_checkbox_options: true
  };
  tableFormat2   : TableFormat = {
    title           : 'Form Member',
    label_headers   : [
                  {label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'username'},
                  {label: 'Data Complete', visible: true, type: 'data_complete', data_row_name: 'additional_info'},
                  {label: 'ID Bisnis', visible: true, type: 'form_business_id', data_row_name: 'input_form_data'},
                  {label: 'Nama Pelanggan',     visible: true, type: 'form_nama_pemilik', data_row_name: 'input_form_data'},
                  {label: 'No. Telp Pelanggan',     visible: true, type: 'form_telp_pemilik', data_row_name: 'input_form_data'},
                  // {label: 'No. WA Pelanggan',     visible: true, type: 'form_wa_pemilik', data_row_name: 'input_form_data'},
                  {label: 'Email Pelanggan',     visible: true, type: 'form_email_pelanggan', data_row_name: 'input_form_data'},
                  {label: 'Alamat Pengiriman',     visible: true, type: 'form_alamat_penerima', data_row_name: 'input_form_data'},
                  // {label: 'Nama penerima',     visible: true, type: 'form_nama_penerima', data_row_name: 'input_form_data'},
                  // {label: 'No. WA Penerima',     visible: true, type: 'form_wa_penerima', data_row_name: 'input_form_data'},
                  {label: 'Program',     visible: true, type: 'form_group', data_row_name: 'input_form_data'},
                  {label: 'Created Date',     visible: true, type: 'date', data_row_name: 'created_date'},
                  {label: 'Updated Date',     visible: true, type: 'date', data_row_name: 'updated_date'},

                ],
    row_primary_key : '_id',
    formOptions     : {
                    addForm   : true,
                    bulkUpdate   : true,
                    row_id    : this.row_id,
                    this      : this,
                    result_var_name : 'Members',
                    detail_function : [this, 'callDetail'] ,
                    setCompleteMember: true
                    },
                    show_checkbox_options: true
  };
  tableFormat3   : TableFormat = {
    title           : 'Form Member',
    label_headers   : [
                  {label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'username'},
                  {label: 'Data Complete', visible: true, type: 'data_complete', data_row_name: 'additional_info'},
                  {label: 'Nama Program',     visible: true, type: 'form_cluster', data_row_name: 'input_form_data'},
                  {label: 'Nama Entitas', visible: true, type: 'form_nama_toko', data_row_name: 'input_form_data'},
                  // {label: 'Nama Distributor', visible: true, type: 'form_nama_distributor', data_row_name: 'input_form_data'},
                  {label: 'Alamat',     visible: true, type: 'form_alamat_toko', data_row_name: 'input_form_data'},
                  {label: 'Nama PIC',     visible: true, type: 'form_nama_pemilik', data_row_name: 'input_form_data'},
                  {label: 'No. KTP PIC',     visible: true, type: 'form_ktp_pemilik', data_row_name: 'input_form_data'},
                  {label: 'Foto KTP PIC',     visible: true, type: 'form_foto_ktp_pemilik', data_row_name: 'input_form_data'},
                  {label: 'No. NPWP PIC',     visible: true, type: 'form_npwp_pemilik', data_row_name: 'input_form_data'},
                  {label: 'Foto NPWP PIC',     visible: true, type: 'form_foto_npwp_pemilik', data_row_name: 'input_form_data'},
                  {label: 'No. Telp PIC',     visible: true, type: 'form_telp_pemilik', data_row_name: 'input_form_data'},
                  {label: 'No. WA PIC',     visible: true, type: 'form_wa_pemilik', data_row_name: 'input_form_data'},
                  {label: 'Alamat Pengiriman',     visible: true, type: 'form_alamat_penerima', data_row_name: 'input_form_data'},
                  {label: 'Hadiah Dikuasakan',     visible: true, type: 'form_hadiah_dikuasakan', data_row_name: 'input_form_data'},
                  {label: 'Nama penerima',     visible: true, type: 'form_nama_penerima', data_row_name: 'input_form_data'},
                  {label: 'Jabatan',     visible: true, type: 'form_group', data_row_name: 'input_form_data'},
                  {label: 'No. KTP Penerima',     visible: true, type: 'form_ktp_penerima', data_row_name: 'input_form_data'},
                  {label: 'Foto KTP Penerima',     visible: true, type: 'form_foto_ktp_penerima', data_row_name: 'input_form_data'},
                  {label: 'No. NPWP Penerima',     visible: true, type: 'form_npwp_penerima', data_row_name: 'input_form_data'},
                  {label: 'Foto NPWP Penerima',     visible: true, type: 'form_foto_npwp_penerima', data_row_name: 'input_form_data'},
                  {label: 'No. WA Penerima',     visible: true, type: 'form_wa_penerima', data_row_name: 'input_form_data'},
                  {label: 'No. E-Wallet Penerima',     visible: true, type: 'form_gopay_penerima', data_row_name: 'input_form_data'},
                  {label: 'Created Date',     visible: true, type: 'date', data_row_name: 'created_date'},
                  {label: 'Updated Date',     visible: true, type: 'date', data_row_name: 'updated_date'},

                ],
    row_primary_key : '_id',
    formOptions     : {
                    addForm   : true,
                    bulkUpdate   : true,
                    row_id    : this.row_id,
                    this      : this,
                    result_var_name : 'Members',
                    detail_function : [this, 'callDetail'] ,
                    setCompleteMember: true
                    },
                    show_checkbox_options: true
  };
  

  public contentList : any = (content as any).default;

  form_input    : any = {};
  errorLabel    : any = false;
  errorMessage  : any = false;
  totalPage     : 0;
 
  memberDetail  : any = false;
  service       : any ;
  srcDownload   : any ;

  currentPage   : any = 1;
  programType:any = "";
  

  // private options = new RequestOptions(
  //   { headers: new Headers({ 'Content-Type': 'application/json' }) });
  constructor(public memberService:MemberService , public sanitizer:DomSanitizer) {

    this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl("");
  }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad() {
    const program = localStorage.getItem('programName');
    let _this = this;
    this.contentList.filter(function(element) {
        if(element.appLabel == program) {
            if(element.type == "reguler") {
                _this.mci_project = true;
            } else if(element.type == "custom_kontraktual") {
              _this.programType = "custom_kontraktual";
            } else {
                _this.mci_project = false;
            }
        }
    });
    
    try {
      // let params = {
      //   'type':'member'
      // }
      this.service    = this.memberService;

      const result: any  = await this.memberService.getAutoformMember(this.currentPage);
      this.totalPage = result.total_page;
      this.Members = result.values;
      // console.warn("Members",result.result.values);
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type

      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
      }
    }

  }

  public doPageChange(currentPage: any):void {
    console.log('currentPage: ', currentPage);
    this.currentPage = currentPage;
  }

  public async callDetail(data, rowData) {
    
    try {
      // console.warn("id search",rowData.id_pel);
      // console.warn("members", this.Members);
      // this.service    = this.memberService;
      // let result: any = await this.memberService.detailMember(_id);
      // this.memberDetail = result.result[0];
      
      this.memberDetail = this.Members.find(member => member.username == rowData.username);
      // console.warn("Member detail",this.memberDetail);
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async backToHere(obj) {
    obj.memberDetail = false;
    obj.firstLoad();
  }

  onDownload(downloadLint)
  {
    let srcDownload = downloadLint;
    //let srcDownload = "http://localhost:8888/f3/assets/csv/members_report.csv";
    this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl(srcDownload);
    //console.log(this.srcDownload);

    this.getDownloadFileLint();
  }

  public async getDownloadFileLint() {
    try
    {

      let result: any;
      this.service    = this.memberService;
      result= await this.memberService.getDownloadFileLint();
      //console.log(result.result);
      let downloadLint = result.result;
      //console.log(downloadLint);

      //To running other subfunction with together automatically
      this.onDownload(downloadLint);
    } 
    catch (e) 
    {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  } 

  // public async pagination() {
  //   try
  //   {
  //     let result: any;
  //     this.service = this.memberService;
  //     result = await this.memberService.getDownloadFileLint();
  //     //console.log(result.result);
  //     let downloadLint = result.result;
  //     //console.log(downloadLint);

  //     //To running other subfunction with together automatically
  //     this.onDownload(downloadLint);
  //   }
  //   catch (e) {
  //     this.errorLabel = ((<Error>e).message);//conversion to Error type
  //   }  
  // }

  public async completeSelectedMembers(obj, members){
    console.log("members", members);
    if(members.length > 0){
      Swal.fire({
        title: 'Confirmation',
        text: 'Apakah anda yakin ingin memproses complete semua member yang dipilih?',
        // icon: 'success',
        confirmButtonText: 'Process',
        cancelButtonText:"Cancel",
        showCancelButton:true
        }).then(async (result) => {
        if(result.isConfirmed){
          try {
            let memberIds = [];
            await members.forEach(async(member) => {
              console.log("member", member);
              console.log("member._id", member.username);
              memberIds.push(member.username);
            });

            console.log("memberIds", memberIds);

            const payload = {
              "member_id":memberIds,
              "status":"ya"
            }

            await obj.memberService.setCompleteMember(payload);
          } catch (e) {
            console.log(e);
          }
          // console.log("member ids", memberIds);
          Swal.fire({
            title: 'Success',
            text: 'Semua member terpilih telah complete',
            icon: 'success',
            confirmButtonText: 'Ok',
            showCancelButton:false
            }).then(async (result) => {
              if(result.isConfirmed){
                obj.firstLoad();
              }
            });
        }
      });
    }
  }
}
