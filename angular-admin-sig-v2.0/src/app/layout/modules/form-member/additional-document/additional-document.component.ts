import { ProductService } from '../../../../services/product/product.service';
import { Component, OnInit, Input, NgZone } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { MemberService } from '../../../../services/member/member.service';
import { OrderhistoryService } from '../../../../services/orderhistory/orderhistory.service';
// import Swal from 'sweetalert2';
// import { basename } from 'path';
// import { ProductService } from '../../../../services/product/product.service';


@Component({
  selector: 'app-additional-document',
  templateUrl: './additional-document.component.html',
  styleUrls: ['./additional-document.component.scss'],
  animations: [routerTransition()]
})

export class AdditonalDocumentComponent implements OnInit {
  @Input() public detail;
  @Input() public back;
  errorLabel : any = false;
  errorMessage  : any = false;
  edit: boolean = false;
  selectedFile  = null;
  cancel = false;
  progressBar = 0;
  errorFile: any = false;
  memberDetail: any;

  private allBast : any = [];
  private allSuratKuasa : any = [];

  private updateSingle: any;
  
  public loading        : boolean = false;

  selFile: any;
  loadingBAST: any = [];
  loadingSuratKuasa: any = [];
  password: any =  "";
  listOrder:[];


  constructor(public memberService: MemberService,
    public productService:ProductService,
    private zone: NgZone,
    private orderHistoryService:OrderhistoryService
    ) {
  }

  ngOnInit() {
    this.firstLoad();

  }



  isArray(curVar : any){
    return Array.isArray(curVar) ? true : false;
  }

  assignArrayBoolean(curVar : any, varCont : any){
    if(this.isArray(curVar)){
      curVar.forEach(element => {
        varCont.push(false);
      });
    }
  }

  async firstLoad() {
    // console.warn("this detail", this.detail);
    this.memberDetail = this.detail;
    console.log("member detail",this.memberDetail);
    let query = {
      search: {
        username : this.memberDetail.username
      },
      limit_per_page:0
    }
     try {
       const result = await this.orderHistoryService.searchAllReportSalesOrder(query);
       console.log(result);
     } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
     }
    // this.updateSingle = JSON.parse(JSON.stringify(this.detail.input_form_data));
    // this.updateSingle = JSON.parse(JSON.stringify(this.detail.additional_info));
    // this.updateSingle.bast = this.isArray(this.updateSingle.bast) ? this.updateSingle.bast: [];
    // this.allBast = this.updateSingle.bast;
    // this.updateSingle.surat_kuasa = this.isArray(this.updateSingle.surat_kuasa) ? this.updateSingle.surat_kuasa: [];
    // this.allSuratKuasa = this.updateSingle.surat_kuasa;
    // this.assignArrayBoolean(this.allBast, this.loadingBAST);
    // this.assignArrayBoolean(this.allSuratKuasa, this.loadingSuratKuasa);
    
    // this.detail.previous_member_id = this.detail.member_id;

    // this.detail.previous_member_id = this.detail.member_id;
  }

  backToDetail() {
    // console.log("Edit member",this.back);
    this.back[0][this.back[1]]();
    // this.back[0]();
  }

  validURL(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
      '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
    return pattern.test(str);
  }

  checkFileType(text : any){
    let splitFileName = text.split('.');
    const extensFile = splitFileName[splitFileName.length - 1];
    const extens = extensFile.toLowerCase();
    return extens == 'pdf' ? 'document':'image';
  }

  async editThis() {
    this.edit = !this.edit;
    if(this.edit == false){
      try {
        const result : any= await this.memberService.getFormMemberByID(this.detail.username);
        if(result.length > 0){
          if(result[0].username == this.detail.username){
            this.detail = result[0];
            this.firstLoad();
          }
        }
      } catch (e) {
        this.errorLabel = ((<Error>e).message);//conversion to Error type

        if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
          this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
        }
      }
    }
  }

  downloadImage(url : any) {
    let filename : any;
    filename = this.lastPathURL(url);
    fetch(url).then(function(t) {
        return t.blob().then((b)=>{
          var a = document.createElement("a");
          a.href = URL.createObjectURL(b);
          a.setAttribute("download", filename);
          a.click();
        }
        );
    });
  }

  lastPathURL(url:string){
    let resURL : any = url.split("/");
    console.log("resURL", resURL);
    let lastNumber : number = resURL.length - 1;
    return resURL[lastNumber];
  }

  convertDate(time){
    const date:any =  new Date(time);
    const dateString = date.toString();
    const retDate = dateString.split("GMT");
    
    return retDate[0];
  }

  // openUpload(type:any, index:any){
  //   let upFile : any = document.getElementById(type+index);
  //   upFile.click();
  // }

  // onFileSelected(event){
  //   this.errorFile = false;
  //   let fileMaxSize = 3000000;// let say 3Mb
  //   // var reader = new FileReader();
  //   // reader.readAsDataURL(event.target.files[0]); //
  //   Array.from(event.target.files).forEach((file: any) => {
  //       if(file.size > fileMaxSize){
  //         this.errorFile="maximum file is 3Mb";
  //       }
  //   });
    
  //   this.selectedFile = event.target.files[0];
  //   console.log('file',this.selectedFile)
    
  // } 

  // cancelThis(){
  //   this.cancel = !this.cancel;
  // }

  // actionShowUploadButton() {
  //   this.showUploadButton = !this.showUploadButton;
  // }


  // async updateDataBulk(){
  //   try
  //     {
  //       this.cancel = false;
  //       let payload = {
  //         type : 'application/form-dataLogin',
  //       }
  //       if(this.selectedFile)
  //       {
  //         console.log('file', this.selectedFile,this)
  //         await this.memberService.registerBulkMember(this.selectedFile,this,payload);
  //       }
  //       this.firstLoad();
  //       //this.onRefresh();
  //     } 
  //     catch (e) 
  //     {
  //       this.errorLabel = ((<Error>e).message);//conversion to Error type
  //     }
  // }

  // updateProgressBar(value){
  //   this.progressBar = value;
  // }

}
