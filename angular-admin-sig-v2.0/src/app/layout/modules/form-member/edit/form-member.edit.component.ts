import { ProductService } from '../../../../services/product/product.service';
import { Component, OnInit, Input, NgZone } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { MemberService } from '../../../../services/member/member.service';
import Swal from 'sweetalert2';
import { basename } from 'path';
// import { ProductService } from '../../../../services/product/product.service';
import * as content from '../../../../../assets/json/content.json';


@Component({
  selector: 'app-form-member-edit',
  templateUrl: './form-member.edit.component.html',
  styleUrls: ['./form-member.edit.component.scss'],
  animations: [routerTransition()]
})

export class FormMemberEditComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  public contentList : any = (content as any).default;

  errorLabel : any = false;
  // mci_project: any = false;
  sig_kontraktual: any = false;
  programType: any = "";
  member_status:any;
  activation_status:any;

  private allBast : any = [];
  private allSuratKuasa : any = [];

  optionsKuasa1: any = [
    {label:"KUASA", value:"true"},
    {label:"TIDAK KUASA", value:"false"}
  ];
  optionsKuasa2 : any =  [
    {label:"KUASA", value:"KUASA"},
    {label:"TIDAK KUASA", value:"TIDAK KUASA"}
  ];
  optionsKuasa3 : any = [
    {label:"KUASA", value:"ya"},
    {label:"TIDAK KUASA", value:"tidak"}
  ];

  optionsGroup: any = [
    {label:"nonupfront", value:"nonupfront"},
    {label:"upfront", value:"upfront"}
  ];
  private updateSingle: any;
  
  public loading        : boolean = false;

  public activationStatus: any = [
    {label: 'ACTIVATED', value: 'ACTIVATED'},
    {label: 'INACTIVED', value: 'INACTIVED'}
  ];

  public memberStatus: any = [
    {label: 'ACTIVE', value: 'ACTIVE'},
    {label: 'INACTIVE', value: 'INACTIVE'}
  ];

  errorFile: any = false;
  selectedFile: any;
  selFile: any;
  loadingKTPPemilik: boolean = false;
  loadingNPWPPemilik: boolean = false;
  loadingKTPPenerima: boolean = false;
  loadingNPWPPenerima: boolean = false;
  loadingBAST: any = [];
  loadingSuratKuasa: any = [];

  keywordProvince = 'province';
  keywordCity = 'city';
  keywordSubDistrict = 'subdistrict';
  keywordVillage = 'village';

  dataProvince:any = [];
  dataCity:any = [];
  dataSubDistrict:any = [];
  dataVillage:any = [];

  province_code:any;
  city_code:any;
  subdistrict_code:any;
  village_code:any;
  
  isInputComplete: any;
  inputTimeOut: any;

  loadingProvince: boolean = false;
  loadingCity: boolean = false;
  loadingSubDistrict: boolean = false;
  loadingVillage: boolean = false;

  public placeholderProvince: string = 'Provinsi';
  public placeholderCity: string = 'Kota';
  public placeholderSubDistrict: string = 'Kecamatan';
  public placeholderVillage: string = 'Kelurahan';

  constructor(public memberService: MemberService, public productService:ProductService, private zone: NgZone) {
  }

  ngOnInit() {
    this.firstLoad();

  }

  checkValueKuasa(){
    if(this.updateSingle["hadiah_dikuasakan"] == "true" || this.updateSingle["hadiah_dikuasakan"] == "false"){
      let options = this.optionsKuasa1.map(el => {
        if(el.value == this.updateSingle["hadiah_dikuasakan"]){
          el.selected = 1;
        }
        return el;
      });
      this.optionsKuasa1 = options;
    }
  
    if(this.updateSingle["hadiah_dikuasakan"] == "KUASA" || this.updateSingle["hadiah_dikuasakan"] == "TIDAK KUASA"){
      let options = this.optionsKuasa2.map(el => {
        if(el.value == this.updateSingle["hadiah_dikuasakan"]){
          el.selected = 1;
        }
        return el;
      });
      this.optionsKuasa2 = options;
    }
  
    if(this.updateSingle["hadiah_dikuasakan"] == "ya" || this.updateSingle["hadiah_dikuasakan"] == "tidak"){
      let options = this.optionsKuasa3.map(el => {
        if(el.value == this.updateSingle["hadiah_dikuasakan"]){
          el.selected = 1;
        }
        return el;
      });
      this.optionsKuasa3 = options;
    }

    if(this.updateSingle["hadiah_dikuasakan"] == "-"){
      console.warn("im here!")
      let options = this.optionsKuasa2.map(el => {
        if(el.value == this.updateSingle["hadiah_dikuasakan"]){
          el.selected = 1;
        }
        return el;
      });
      this.optionsKuasa2 = options;
    }

  }

  checkValueGroup(){
    if(this.updateSingle["description"] == "upfront" || this.updateSingle["description"] == "nonupfront"){
      let options = this.optionsGroup.map(el => {
        if(el.value == this.updateSingle["description"]){
          el.selected = 1;
        }
        return el;
      });
      this.optionsGroup = options;
    }
  }

  isArray(curVar : any){
    return Array.isArray(curVar) ? true : false;
  }

  assignArrayBoolean(curVar : any, varCont : any){
    if(this.isArray(curVar)){
      curVar.forEach(element => {
        varCont.push(false);
      });
    }
  }

  async firstLoad() {
    console.warn("detail",this.detail);

    const program = localStorage.getItem('programName');
    let _this = this;
    this.contentList.filter(function(element) {
        if(element.appLabel == program) {
            if(element.type == "reguler") {
              _this.programType = "reguler";
            } else if(element.type == "custom_kontraktual") {
              _this.programType = "custom_kontraktual";
              _this.sig_kontraktual = true;
            } else {
              _this.programType = "custom";
            }
        }
    });

    this.updateSingle = JSON.parse(JSON.stringify(this.detail.input_form_data));
    this.updateSingle.foto_bast = this.isArray(this.updateSingle.foto_bast) ? this.updateSingle.foto_bast: [];
    this.allBast = this.updateSingle.foto_bast;
    this.updateSingle.foto_surat_kuasa = this.isArray(this.updateSingle.foto_surat_kuasa) ? this.updateSingle.foto_surat_kuasa: [];
    this.allSuratKuasa = this.updateSingle.foto_surat_kuasa;
    this.assignArrayBoolean(this.allBast, this.loadingBAST);
    this.assignArrayBoolean(this.allSuratKuasa, this.loadingSuratKuasa);
    // console.log("loadingBAST", this.loadingBAST);
    // this.assignArrayBoolean(this.allSuratKuasa);
    // console.log("count", this.allBast, this.allSuratKuasa);
    this.checkValueKuasa();
    this.checkValueGroup();
    
    this.detail.previous_member_id = this.detail.member_id;
    this.activationStatus.forEach((element, index, products_ids) => {
        if (element.value == this.detail.activation_status) {
            this.activationStatus[index].selected = 1;
        }
    });

    this.detail.previous_member_id = this.detail.member_id;
    this.memberStatus.forEach((element, index, products_ids) => {
        if (element.value == this.detail.member_status) {
            this.memberStatus[index].selected = 1;
        }
    });

  }

  backToDetail() {
    // console.log("Edit member",this.back);
    this.back[0][this.back[1]]();
    // this.back[0]();
  }

  validURL(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
      '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
    return !!pattern.test(str);
  }

  openUpload(type:any, index:any){
    let upFile : any = document.getElementById(type+index);
    upFile.click();
  }


  async saveThis() {
    
    let new_form :any = {
      // key:"ps-form-1",
      id_pel: this.detail.input_form_data.id_pel,
    };

    if(this.updateSingle.foto_bast && this.updateSingle.foto_bast.length > 0){
      let currentBast = this.updateSingle.foto_bast.filter((element : any) => {
        if(element.url && this.validURL(element.url)){
          return element;
        }
      });
      this.updateSingle.foto_bast = currentBast;
    }
    if(this.updateSingle.foto_surat_kuasa && this.updateSingle.foto_surat_kuasa.length > 0){
      let currentSuratKuasa = this.updateSingle.foto_surat_kuasa.filter((element:any) => {
        if(element.url && this.validURL(element.url)){
          return element;
        }
      });
      this.updateSingle.foto_surat_kuasa = currentSuratKuasa;
    }
    
    for(let prop in this.updateSingle){
      if(this.updateSingle[prop] != this.detail[prop] && this.updateSingle[prop] != ""){
        if(prop == 'hadiah_dikuasakan') {
          let kuasa = ["Ya", "true", "ya"];
          let nonKuasa = ["Tidak", "false", "tidak"];
          if(kuasa.includes(this.updateSingle[prop])){
            new_form[prop] = "KUASA";
          } else if(nonKuasa.includes(this.updateSingle[prop])){
            new_form[prop] = "TIDAK KUASA";
          }else {
            new_form[prop] = this.updateSingle[prop];
          }
        } else {
          new_form[prop] = this.updateSingle[prop];
        }
      }
      if(prop == 'foto_bast' || prop == 'foto_surat_kuasa'){
        new_form[prop] = this.updateSingle[prop];
      }
    }

    //check if object from autocomplete component
    if(new_form) {
      if(new_form.provinsi_rumah && new_form.provinsi_rumah && typeof new_form.provinsi_rumah === 'object' && new_form.provinsi_rumah !== null && new_form.provinsi_rumah.province) {
        new_form.provinsi_rumah = new_form.provinsi_rumah.province;
      }

      if(new_form.kota_rumah && new_form.kota_rumah && typeof new_form.kota_rumah === 'object' && new_form.kota_rumah !== null && new_form.kota_rumah.city) {
        new_form.kota_rumah = new_form.kota_rumah.city;
      }

      if(new_form.kecamatan_rumah && new_form.kecamatan_rumah && typeof new_form.kecamatan_rumah === 'object' && new_form.kecamatan_rumah !== null && new_form.kecamatan_rumah.subdistrict) {
        new_form.kecamatan_rumah = new_form.kecamatan_rumah.subdistrict;
      }

      if(new_form.kelurahan_rumah && new_form.kelurahan_rumah && typeof new_form.kelurahan_rumah === 'object' && new_form.kelurahan_rumah !== null && new_form.kelurahan_rumah.village) {
        new_form.kelurahan_rumah = new_form.kelurahan_rumah.village;
      }
    }
      
    try {
      await this.memberService.updateSingleFormMember(new_form);
      this.detail = JSON.parse(JSON.stringify(this.updateSingle));
      //  Swal.fire('Success', 'Edit data pelanggan berhasil', 'success');
      Swal.fire({
        title: 'Success',
        text: 'Edit data pelanggan berhasil',
        icon: 'success',
        confirmButtonText: 'Ok',
      }).then((result) => {
        if(result.isConfirmed){
          this.backToDetail();
        }
      });
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
      // alert("Error");

      let message = this.errorLabel;
      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
      }
      Swal.fire({
        icon: 'error',
        title: message,
      });
    }
  }

  async onFileSelected2(event, img) {
		var reader = new FileReader()
    const programName = localStorage.getItem('programName');
    const fileExtens = ['jpg', 'jpeg', 'png', 'gif', 'pdf'];
		try {
			this.errorFile = false;
			let fileMaxSize = 2064000;
			Array.from(event.target.files).forEach((file: any) => {
				if (file.size > fileMaxSize) {
					this.errorFile = 'Maximum File Upload is 2MB';
				}
        const splitFileName = file.name.split('.');
        if(splitFileName.length < 1)
        { 
          this.errorFile = "file extention is required";
        } else {
          let extensFile = splitFileName[splitFileName.length - 1];
          const extens = extensFile.toLowerCase();
          if(!fileExtens.includes(extens)) this.errorFile = "extention ${extens} is not Allowed";
        }
			});
			this.selectedFile = event.target.files[0];

			reader.onload = (event: any) => {
				this.selFile = event.target.result;
			}
			if(event.target.files[0]){
        reader.readAsDataURL(event.target.files[0]);
      }
			if (this.selectedFile) {
        let valueImage = this.checkLoading(img, true);
				let splitFileName = this.selectedFile.name.split('.');
        const extensFile = splitFileName[splitFileName.length - 1];
        const extens = extensFile.toLowerCase();
        const typeFile = extens == 'pdf' ? 'document':'image';

        const newFileName =  programName + '-' + this.detail.username + '-' + img + '.' + extens;
				const logo = await this.productService.upload(this.selectedFile, this, typeFile, (result) => {
          if(typeof result == 'object' && result.base_url){
            this.updateSingle[valueImage] = result.base_url + result.pic_big_path;
          } else {
            this.updateSingle[valueImage] = result;
          }
          this.checkLoading(img, false);
					// console.log("updateSingle", this.updateSingle);
					// this.callImage(result, img);
				}, newFileName);
			}
			
		} catch (e) {
			this.errorLabel = (<Error>e).message; //conversion to Error type
		}
  }

  checkFileType(text : any){
    let splitFileName = text.split('.');
    const extensFile = splitFileName[splitFileName.length - 1];
    const extens = extensFile.toLowerCase();
    return extens == 'pdf' ? 'document':'image';
  }

  async onFileSelected(event, typeImg: any, idx : any) {
    if(typeImg == 'bast'){
      this.loadingBAST[idx] = true;
    }

    else if(typeImg == 'suratKuasa'){
      this.loadingSuratKuasa[idx] = true;
    }

		var reader = new FileReader();
		try {
			this.errorFile = false;
			let fileMaxSize = 2064000;
			Array.from(event.target.files).forEach((file: any) => {
				if (file.size > fileMaxSize) {
					this.errorFile = 'Maximum File Upload is 2MB';
				}
			});
			this.selectedFile = event.target.files[0];

			reader.onload = (event: any) => {
				this.selFile = event.target.result;
			}
			if(event.target.files[0]){
        reader.readAsDataURL(event.target.files[0]);
      }
			if (this.selectedFile) {
				await this.productService.upload(this.selectedFile, this, 'image', (result) => {
          const dataImage = {
            "url": result.base_url + result.pic_big_path,
            "updated_date": Date.now()
          }
          if(typeImg == 'bast'){
            this.updateSingle.foto_bast[idx] = {...this.updateSingle.foto_bast[idx], ...dataImage};
            this.loadingBAST[idx] = false;

          }

          else if(typeImg == 'suratKuasa'){
            this.updateSingle.foto_surat_kuasa[idx] = {...this.updateSingle.foto_surat_kuasa[idx], ...dataImage};
            this.loadingSuratKuasa[idx] = false;
          }

          
				});
			}
			
		} catch (e) {
			this.errorLabel = (<Error>e).message; //conversion to Error type
		}
  }

  addMoreUpload(typeImg : any)
  {
    const dummyImg = {
      "url" : "",
      "updated_date" : Date.now(),
      "desc": ""
    }
    if(typeImg == 'bast'){
      // if(!this.isArray(this.updateSingle.foto_bast)) this.updateSingle.foto_bast = [];
      this.updateSingle.foto_bast.push(dummyImg);
      this.loadingBAST.push(false);
    }else if(typeImg == 'suratKuasa'){
      // if(!this.isArray(this.updateSingle.foto_surat_kuasa)) this.updateSingle.foto_surat_kuasa = [];
      this.updateSingle.foto_surat_kuasa.push(dummyImg);
      this.loadingSuratKuasa.push(false);
    }
  }

  deleteImage(type : any, idx : any, url : any){
    Swal.fire({
      title: "Delete",
      text : "Are you sure want to delete this ?",
      imageUrl : url,
      imageHeight : 250,
      imageAlt : type,
      showCancelButton:true,
      focusConfirm:false,
    }).then(res =>{
      if(res.isConfirmed){
        if(type == "bast"){
          this.updateSingle.foto_bast.splice(idx,1);
          this.loadingBAST.splice(idx,1);
          // console.log("BAST DELETE", this.updateSingle.foto_bast,this.allBast);
        }else if(type="suratKuasa"){
          this.updateSingle.foto_surat_kuasa.splice(idx,1);
          this.loadingSuratKuasa.splice(idx,1);
          // console.log("SURAT KUASA DELETE", this.updateSingle.foto_surat_kuasa,this.allSuratKuasa);
        }
      }
    });
  }

  deletePhoto(type : any, url : any){
    Swal.fire({
      title: "Delete",
      text : "Are you sure want to delete this ?",
      imageUrl : url,
      imageHeight : 250,
      imageAlt : type,
      showCancelButton:true,
      focusConfirm:false,
    }).then(res =>{
      if(res.isConfirmed){
        if(type == "foto_ktp_pemilik"){
          this.updateSingle.foto_ktp_pemilik = '-';
          this.loadingKTPPemilik = false;
          // console.log("KTP Pemilik DELETE", this.updateSingle.foto_ktp_pemilik);
        }else if(type == "foto_npwp_pemilik"){
          this.updateSingle.foto_npwp_pemilik = '-';
          this.loadingNPWPPemilik = false;
          // console.log("NPWP Pemilik DELETE", this.updateSingle.foto_npwp_pemilik);
        }else if(type == "foto_ktp_penerima"){
          this.updateSingle.foto_ktp_penerima = '-';
          this.loadingKTPPenerima = false;
          // console.log("KTP Penerima DELETE", this.updateSingle.foto_ktp_penerima);
        }else if(type == "foto_npwp_penerima"){
          this.updateSingle.foto_npwp_penerima = '-';
          this.loadingNPWPPenerima = false;
          // console.log("NPWP Penerima DELETE", this.updateSingle.foto_npwp_penerima);
        }
      }
    });
  }

  convertDate(time){
    const date:any =  new Date(time);
    const dateString = date.toString();
    const retDate = dateString.split("GMT");
    
    return retDate[0];
  }

  checkLoading(fieldName, fieldValue){
    let valueImage:any;
    switch (fieldName) {
      case 'ktpPemilik':
        valueImage = 'foto_ktp_pemilik';
        this.loadingKTPPemilik = fieldValue;
        break;

      case 'npwpPemilik':
        valueImage = 'foto_npwp_pemilik';
        this.loadingNPWPPemilik = fieldValue;
        break;

      case 'ktpPenerima':
        valueImage = 'foto_ktp_penerima';
        this.loadingKTPPenerima = fieldValue;
        break;

      case 'npwpPenerima':
        valueImage = 'foto_npwp_penerima';
        this.loadingNPWPPenerima =fieldValue;
        break;
      case 'bast':
        valueImage = 'foto_bast';
        this.loadingBAST = fieldValue;
        break;
      case 'suratKuasa' :
        valueImage = 'foto_surat_kuasa';
        this.loadingSuratKuasa = fieldValue;
    }
    return valueImage;
  }

  selectEvent(item, region) {
    // do something with selected item
    if(region == "province") {
      this.updateSingle.provinsi_rumah = item.province;
      this.province_code = item.province_code;
    } else if(region == "city") {
      this.updateSingle.kota_rumah = item.city;
      this.city_code = item.city_code;
    } else if(region == "subdistrict") {
      this.updateSingle.kecamatan_rumah = item.subdistrict;
      this.subdistrict_code = item.subdistrict_code;
    } else if(region == "village") {
      this.updateSingle.kelurahan_rumah = item.village;
      this.village_code = item.village_code;
    }
  }

  async inputRegion(val: string, region) {
    //prevent calling API twice or more
    await clearTimeout(this.inputTimeOut);

    this.isInputComplete = false;

    //set loading indicator
    if(region == "province") {
      this.loadingProvince = true;

      if(this.programType != "reguler") {
        this.updateSingle.kota_rumah = "";
        this.updateSingle.kecamatan_rumah = "";
        this.updateSingle.kelurahan_rumah = "";

        delete this.province_code;
        delete this.city_code;
        delete this.subdistrict_code;
      }
    } else if(region == "city") {
      this.loadingCity = true;

      if(this.programType != "reguler") {
        this.updateSingle.kecamatan_rumah = "";
        this.updateSingle.kelurahan_rumah = "";

        delete this.city_code;
        delete this.subdistrict_code;
      }
    } else if(region == "subdistrict") {
      this.loadingSubDistrict = true;

      if(this.programType != "reguler") {

        delete this.subdistrict_code;
        this.updateSingle.kelurahan_rumah = "";
      }
    } else if(region == "village") {
      this.loadingVillage = true;
    }

    this.inputTimeOut = setTimeout(() => {
      this.isInputComplete = true;

      if(this.isInputComplete) {
        this.filterRegion(val, region);
      }
    } , 300);
  }

  public filterRegion(search, region): void {
      let payload:any = {
        "region":region,
        "search":search
      };

      if(region == "city") {
        payload.province_code = this.province_code;
      } else if(region == "subdistrict") {
        payload.city_code = this.city_code;
      } else if(region == "village") {
        payload.subdistrict_code = this.subdistrict_code;
      }

      // Set val to the value of the searchbar
      if(search.length > 0) {
        this.memberService.searchRegion(payload).then((result) => {
          this.loadingProvince = false;
          this.loadingCity = false;
          this.loadingSubDistrict = false;
          this.loadingVillage = false;

          if(result && result.length > 0) {
            if(region == "province") {
              this.dataProvince = result;
            } else if(region == "city") {
              this.dataCity = result;
            } else if(region == "subdistrict") {
              this.dataSubDistrict = result;
            } else if(region == "village") {
              this.dataVillage = result;
            }
          }
        }, (err) => {
          console.log(err);
          this.loadingProvince = false;
          this.loadingCity = false;
          this.loadingSubDistrict = false;
          this.loadingVillage = false;

          this.dataProvince = [];
          this.dataCity = [];
          this.dataSubDistrict = [];
          this.dataVillage = [];
        });
      } else {
        this.loadingProvince = false;
        this.loadingCity = false;
        this.loadingSubDistrict = false;
        this.loadingVillage = false;

        this.dataProvince = [];
        this.dataCity = [];
        this.dataSubDistrict = [];
        this.dataVillage = [];
      }
  }
}
