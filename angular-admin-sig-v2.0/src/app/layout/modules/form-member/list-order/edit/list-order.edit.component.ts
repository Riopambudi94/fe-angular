// import { AdditonalDocumentComponent } from '../list-order.component';
import { ProductService } from '../../../../../services/product/product.service';
import { Component, OnInit, Input, NgZone } from '@angular/core';
import { routerTransition } from '../../../../../router.animations';
import { MemberService } from '../../../../../services/member/member.service';
import Swal from 'sweetalert2';
import { OrderhistoryService } from '../../../../../services/orderhistory/orderhistory.service';
// import { basename } from 'path';
// import { ProductService } from '../../../../services/product/product.service';


@Component({
  selector: 'app-list-order-edit',
  templateUrl: './list-order.edit.component.html',
  styleUrls: ['./list-order.edit.component.scss'],
  animations: [routerTransition()]
})

export class EditListOrderComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;
  @Input() public detailOrder:any;
  errorLabel : any = false;
  member_status:any;
  activation_status:any;
  new_form:any = {};

  // private allBast : any = [];
  // private allSuratKuasa : any = [];
  private updateSingle: any;
  
  public loading        : boolean = false;

  errorFile: any = false;
  cancel: Boolean = false;
  selectedFile: any;
  selFile: any;
  // loadingKTPPemilik: boolean = false;
  // loadingNPWPPemilik: boolean = false;
  // loadingKTPPenerima: boolean = false;
  // loadingNPWPPenerima: boolean = false;
  loadingBAST: any = [];
  loadingSuratKuasa: any = [];
  loadingFotoBAST: Boolean = false;
  loadingFotoSuratKuasa: Boolean = false;
  loadingFotoOthers:Boolean = false;
  loadingFotoReceipt:Boolean = false;
  loadingOthers:any = [];
  loadingReceipt:any = [];
  order: any;
  Report: any;
  prodOnUpload: Boolean;
  memberDetail : any;

  constructor(
     public memberService: MemberService,
     public orderHistoryService : OrderhistoryService,
     public productService:ProductService,
     private zone: NgZone) {
  }

  ngOnInit() {
    this.memberDetail = this.detail;
    this.order = this.detailOrder;
    this.firstLoad();

  }

  isArray(curVar : any){
    return Array.isArray(curVar) ? true : false;
  }

  assignArrayBoolean(curVar : any, varCont : any){
    if(this.isArray(curVar)){
      curVar.forEach(element => {
        varCont.push(false);
      });
    }
  }

  async firstLoad() {
    this.loadingOthers = [];
    this.loadingReceipt = [];
    if(this.order.additional_info && this.order.additional_info.others && this.order.additional_info.others.length > 0){
      this.order.additional_info.others.forEach(item=>this.loadingOthers.push(false));
    }
    if(this.order.additional_info && this.order.additional_info.receipt && this.order.additional_info.receipt.length > 0){
      this.order.additional_info.receipt.forEach(item=>this.loadingReceipt.push(false));
    }
    // if(!this.detail.additional_info) this.detail.additional_info = {bast:[],surat_kuasa:[]};
    // if(!this.detail.additional_info.bast) this.detail.additional_info.bast = [];
    // if(!this.detail.additional_info.surat_kuasa) this.detail.additional_info.surat_kuasa = [];

    // this.updateSingle = JSON.parse(JSON.stringify(this.detail.additional_info));
    // this.updateSingle.bast = this.isArray(this.updateSingle.bast) ? this.updateSingle.bast: [];
    // this.allBast = this.updateSingle.bast;
    // this.updateSingle.surat_kuasa = this.isArray(this.updateSingle.surat_kuasa) ? this.updateSingle.surat_kuasa: [];
    // this.allSuratKuasa = this.updateSingle.surat_kuasa;
    // this.assignArrayBoolean(this.allBast, this.loadingBAST);
    // this.assignArrayBoolean(this.allSuratKuasa, this.loadingSuratKuasa);
    // console.log("loadingBAST", this.loadingBAST);
    // this.assignArrayBoolean(this.allSuratKuasa);
    // console.log("count", this.allBast, this.allSuratKuasa);

    // console.warn("update single", this.updateSingle);
    
    // this.detail.previous_member_id = this.detail.member_id;

    // this.detail.previous_member_id = this.detail.member_id;
  }

  backToDetail() {
    this.back[0][this.back[1]]();
  }

  validURL(str) {
    return /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(str);
  }

  openUpload(type:any, index:any){
    console.log("index upload", index, type);
    let upFile : any = index || index == 0 ? document.getElementById(type+index):document.getElementById(type);
    console.warn("upfile", upFile);
    upFile.click();
  }


  async saveThis(){

    var arrAddBAST = [];
    var arrAddSK = [];
    var arrDeleteBAST = [];
    var arrDeleteSK = [];

    //validate BAST
    for(let i = 0; i < this.detail.additional_info.bast.length; i++){
      let found = false;
      for(let j = 0; j < this.updateSingle.bast.length; j++){

        if(this.updateSingle.bast[j].id == this.detail.additional_info.bast[i].id){
          found = true;
          break;
        }
      }
      if(!found){
        arrDeleteBAST.push(this.detail.additional_info.bast[i].id);
      }
    }
    
    for(let i = 0; i < this.updateSingle.bast.length; i++){
      if(this.updateSingle.bast[i].id == null && this.updateSingle.bast[i].pic_file_name){
        arrAddBAST.push(this.updateSingle.bast[i].pic_file_name);
      }
    }
    
    // Validate Surat Kuasa
    for(let i = 0; i < this.detail.additional_info.surat_kuasa.length; i++){
      let found = false;
      for(let j = 0; j < this.updateSingle.surat_kuasa.length; j++){
        if(this.updateSingle.surat_kuasa[j].id == this.detail.additional_info.surat_kuasa[i].id){
          found = true;
          break;
        }
      }
      if(!found){
        arrDeleteSK.push(this.detail.additional_info.surat_kuasa[i].id);
      }
    }

    for(let i = 0; i < this.updateSingle.surat_kuasa.length; i++){
      if(this.updateSingle.surat_kuasa[i].id == null && this.updateSingle.surat_kuasa[i].pic_file_name){
        arrAddSK.push(this.updateSingle.surat_kuasa[i].pic_file_name);
      }
    }
    

    let payloadRemove = {
      username:this.detail.username,
      bast:arrDeleteBAST,
      surat_kuasa:arrDeleteSK
    }

    let payloadAdd = {
       username:this.detail.username,
       bast:arrAddBAST,
       surat_kuasa:arrAddSK
    }

    try {
      if(payloadAdd.bast.length > 0 || payloadAdd.surat_kuasa.length > 0) await this.memberService.addBastSk(payloadAdd);
      if(payloadRemove.bast.length > 0 || payloadRemove.surat_kuasa.length > 0) await this.memberService.deleteBastSk(payloadRemove);
      Swal.fire({
        title: 'Success',
        text: 'Edit data pelanggan berhasil',
        icon: 'success',
        confirmButtonText: 'Ok',
      }).then((result) => {
        if(result.isConfirmed){
          this.backToDetail();
        }
      });
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
      // alert("Error");
      let message = this.errorLabel;
      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
      }
      Swal.fire({
        icon: 'error',
        title: message,
      });
    }
  }

  checkFileType(text : any){
    let splitFileName = text.split('.');
    const extensFile = splitFileName[splitFileName.length - 1];
    const extens = extensFile.toLowerCase();
    return extens == 'pdf' ? 'document':'image';
  }


  // async oldsaveThis() {
  //   this.new_form = {
  //     username: this.detail.username
  //   }

  //   if(this.updateSingle.bast && this.updateSingle.bast.length > 0){
  //     let currentBast = this.updateSingle.bast.filter((element : any) => {
  //       if(element.url && this.validURL(element.url)){
  //         return element;
  //       }
  //     });
  //     this.updateSingle.bast = currentBast;
  //   }
  //   if(this.updateSingle.surat_kuasa && this.updateSingle.surat_kuasa.length > 0){
  //     let currentSuratKuasa = this.updateSingle.surat_kuasa.filter((element:any) => {
  //       if(element.url && this.validURL(element.url)){
  //         return element;
  //       }
  //     });
  //     this.updateSingle.surat_kuasa = currentSuratKuasa;
  //   }
    
  //   for(let prop in this.updateSingle){
  //     if(this.updateSingle[prop] != this.detail[prop] && this.updateSingle[prop] != ""){
  //         this.new_form[prop] = this.updateSingle[prop];
  //     }
  //     if(prop == 'bast' || prop == 'surat_kuasa'){
  //       this.new_form[prop] = this.updateSingle[prop];
  //     }
  //   }

  //   console.warn("new form", this.new_form);
  //   console.warn("updateSingle", this.updateSingle);
  //   console.warn("detail", this.detail);
      
  //   try {
  //     await this.memberService.updateBASTandSK(new_form);
  //     this.detail = JSON.parse(JSON.stringify(this.updateSingle));
  //     //  Swal.fire('Success', 'Edit data pelanggan berhasil', 'success');
  //     Swal.fire({
  //       title: 'Success',
  //       text: 'Edit data pelanggan berhasil',
  //       icon: 'success',
  //       confirmButtonText: 'Ok',
  //     }).then((result) => {
  //       if(result.isConfirmed){
  //         this.backToDetail();
  //       }
  //     });
  //   } catch (e) {
  //     this.errorLabel = ((<Error>e).message);//conversion to Error type
  //     alert("Error");
  //   }
  // }

  

  async onFileSelected(event, typeImg: any, idx : any) {
    let fileValue:any;
    let payload:any = null;
    let option:any = null;
    let id:any = null;
    let receipt_id:any = null;
    if(typeImg == 'bast'){
      this.loadingFotoBAST = true;
      fileValue = 'bast';
    }

    else if(typeImg == 'suratKuasa'){
      this.loadingFotoSuratKuasa = true;
      fileValue = 'surat_kuasa';
    }

    else if(typeImg == 'others'){
      if(idx || idx == 0){
        this.loadingOthers[idx] = true;
        id = this.order.additional_info.others[idx].id;
        option = 'edit';
      }else {
        this.loadingFotoOthers = true;
      }
      fileValue = 'others';
    }

    else if(typeImg == 'receipt'){
      if(idx || idx == 0){
        this.loadingReceipt[idx] = true;
        receipt_id = this.order.additional_info.receipt[idx].receipt_id;
        option = 'edit';
      }else {
        this.loadingFotoReceipt = true;
      }
      fileValue = 'receipt';
    }

		var reader = new FileReader();
		try {
			this.errorFile = false;
			let fileMaxSize = 2064000;
			Array.from(event.target.files).forEach((file: any) => {
				if (file.size > fileMaxSize) {
					this.errorFile = 'Maximum File Upload is 2MB';
				}
			});
			this.selectedFile = event.target.files[0];

			reader.onload = (event: any) => {
				this.selFile = event.target.result;
			}
			if(event.target.files[0]){
        reader.readAsDataURL(event.target.files[0]);
      }

      if(typeImg == 'receipt') {
        payload = {
          type : 'application/form-data',
          option,
          receipt_id
        }
      } else {
        payload = {
          type : 'application/form-data',
          option,
          id
        }
      }
      // let payload = {
      //   type : 'application/form-data',
      //   option,
      //   id
      // }

      console.warn("payload edit", payload);
      
			if (this.selectedFile) {
        const result: any = await this.orderHistoryService.uploadAdditionalImage(this.selectedFile,this,payload, this.order.order_id, fileValue);
        if(typeImg == 'bast'){
          this.loadingFotoBAST = false;
        }
    
        else if(typeImg == 'suratKuasa'){
          this.loadingFotoSuratKuasa = false;
        }

        else if(typeImg == 'others'){
          if(idx || idx == 0){
            this.loadingOthers[idx] = false;
          }else {
            this.loadingFotoOthers = false;
          }
        }

        else if(typeImg == 'receipt'){
          if(idx || idx == 0){
            this.loadingReceipt[idx] = false;
          }else {
            this.loadingFotoReceipt = false;
          }
        }

        if(result){
          await this.getCurrentOrderHistory();
        }else {

        }
				// await this.productService.upload(this.selectedFile, this, 'image', (result) => {

        //   let dateNow = Date.now();

        //   const dataImage = {
        //     pic_file_name: result.base_url + result.pic_big_path,
        //     updated_date: dateNow,
        //     created_date: dateNow,
        //     id: null
        //   }

        //   if(typeImg == 'bast'){
        //     this.updateSingle.bast[idx] = {...this.updateSingle.bast[idx], ...dataImage};
        //     this.loadingBAST[idx] = false;

        //   }

        //   else if(typeImg == 'suratKuasa'){
        //     this.updateSingle.surat_kuasa[idx] = {...this.updateSingle.surat_kuasa[idx], ...dataImage};
        //     this.loadingSuratKuasa[idx] = false;
        //   }

          
				// });
			}
			
		} catch (e) {
			this.errorLabel = (<Error>e).message; //conversion to Error type
      console.log
      let message = this.errorLabel;
      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
      }
      Swal.fire({
        icon: 'error',
        title: message,
      });
		}
  }

  async getCurrentOrderHistory(){
    let query = {
      search: {
        order_id : this.order.order_id
      },
      limit_per_page:1,
      current_page:1
    }
     try {
       const result = await this.orderHistoryService.searchAllReportSalesOrder(query);
      //  this.loadingOthers.forEach((element,idx) => {
      //    document.getElementById('others'+idx)['value'] ='';
      //  });
       this.order = result.values[0];
       document.getElementById("bast")['value'] = '';
       document.getElementById("suratKuasa")['value'] = '';
       document.getElementById("others")['value'] = '';
       document.getElementById("receipt")['value'] = '';

       this.firstLoad();
     } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
     }
  }

  addMoreUpload(typeImg : any)
  {
    const dummyImg = {
      created_by: null,
      created_date: null,
      id: null,
      pic_file_name: null,
      updated_by: null,
      updated_date: null
    }
    if(typeImg == 'bast'){
      this.updateSingle.bast.push(dummyImg);
      this.loadingBAST.push(false);
    } else if(typeImg == 'suratKuasa'){
      this.updateSingle.surat_kuasa.push(dummyImg);
      this.loadingSuratKuasa.push(false);
    }
  }

  deleteImage(type : any, id : any, url : any, idx:any){
    Swal.fire({
      title: "Delete",
      text : "Apakah anda yakin ingin menghapus gambar ini ?",
      imageUrl : url,
      imageHeight : 250,
      imageAlt : type,
      showCancelButton:true,
      focusConfirm:false,
    }).then(async (res) =>{
      if(res.isConfirmed){ 
        if(type == "others"){
          this.loadingOthers[idx] = true;
          let payload = {
            type : 'application/form-data',
            option : 'delete',
            id
          }
          const result: any = await this.orderHistoryService.uploadAdditionalImage(null,this,payload, this.order.order_id, type);
          if(result){
            // Swal.fire({
            //   title: 'Success',
            //   text: 'Hapus gambar berhasil',
            //   icon: 'success',
            //   confirmButtonText: 'Ok',
            // }).then(async (result) => {
            //   if(result.isConfirmed){
            //   }
           // });
            await this.getCurrentOrderHistory();
          }
        }
      }
    });
  }

  deleteReceipt(type : any, receipt_id : any, url : any, idx:any){
    Swal.fire({
      title: "Delete",
      text : "Apakah anda yakin ingin menghapus gambar ini ?",
      imageUrl : url,
      imageHeight : 250,
      imageAlt : type,
      showCancelButton:true,
      focusConfirm:false,
    }).then(async (res) =>{
      if(res.isConfirmed){ 
        if(type == "receipt"){
          this.loadingReceipt[idx] = true;
          console.warn("receipt id", receipt_id);
          let payload = {
            type : 'application/form-data',
            option : 'delete',
            receipt_id
          }
          const result: any = await this.orderHistoryService.uploadAdditionalImage(null,this,payload, this.order.order_id, type);
          if(result){
            await this.getCurrentOrderHistory();
          }
        }
      }
    });
  }

  convertDate(time){
    const date:any =  new Date(time);
    const dateString = date.toString();
    const retDate = dateString.split("GMT");
    
    return retDate[0];
  }

  checkLoading(fieldName, fieldValue){
    let valueImage:any;
    switch (fieldName) {      
      case 'bast':
        valueImage = 'bast';
        this.loadingBAST = fieldValue;
        break;
      case 'suratKuasa' :
        valueImage = 'surat_kuasa';
        this.loadingSuratKuasa = fieldValue;
    }
    return valueImage;
  }

}
