import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderhistoryfailedRoutingModule } from './orderhistoryfailed-routing.module';
import { OrderhistoryfailedComponent } from './orderhistoryfailed.component';
import { PageHeaderModule } from './../../../shared';
import { FormBuilderTableComponent }  from '../../../component-libs/form-builder-table/form-builder-table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { OrderhistoryfailedDetailComponent } from './detail/orderhistoryfailed.detail.component';
import { OrderhistoryfailedEditComponent } from './edit/orderhistoryfailed.edit.component';






@NgModule({
  imports: [
    CommonModule, 
    OrderhistoryfailedRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule],
  declarations: [OrderhistoryfailedComponent,OrderhistoryfailedDetailComponent,OrderhistoryfailedEditComponent]
})
export class OrderhistoryfailedModule { }
