import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagebaruComponent } from './pagebaru.component';
import { PagebaruRoutingModule } from './pagebaru.routing';
import { PageHeaderModule } from '../../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { Ng2CompleterModule } from "ng2-completer";
@NgModule({
  declarations: [PagebaruComponent],
  // imports: [CommonModule, FormRoutingModule, PageHeaderModule],
  imports: [
    CommonModule, 
    PagebaruRoutingModule, 
    PageHeaderModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule,
    Ng2CompleterModule
  ]
})
export class PagebaruModule { }
