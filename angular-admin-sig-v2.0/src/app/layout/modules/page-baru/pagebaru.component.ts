import { Component, OnInit } from '@angular/core';
import { PaymentGatewayService } from '../../../services/paymentgateway/paymentgateway.service';
import { CompleterService, CompleterData } from 'ng2-completer';
import { routePath } from '../../routing.path';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';

import { Router } from '@angular/router';
@Component({
  selector: 'app-pagebaru',
  templateUrl: './pagebaru.component.html',
  styleUrls: ['./pagebaru.component.scss']
})


export class PagebaruComponent implements OnInit {
  service;
  paymentGatewayData;

  dataSummary: any = [];
  protected dataService: CompleterData;
  protected searchData = [
    { color: 'red', value: '#f00' },
    { color: 'green', value: '#0f0' },
    { color: 'blue', value: '#00f' },
    { color: 'cyan', value: '#0ff' },
    { color: 'magenta', value: '#f0f' },
    { color: 'yellow', value: '#ff0' },
    { color: 'black', value: '#000' }
  ];

      tableFormat        : TableFormat = {
      title           : 'Summary of Order History',
      label_headers   : [
        {label: 'Nama', visible: true, type: 'string', data_row_name: 'name'},
        // {label: 'Alias', visible: false, type: 'string', dumata_row_name: 'alias'}
      ],
      row_primary_key : '_id',
      formOptions     : {
                        row_id: '_id',
                        this  : this,
                        result_var_name: 'dataSummary',
                        detail_function: [this, 'callDetail']
                      }
  };
    
  form_input    : any = {};
  errorLabel    : any = false;
  
  routePath;
  orderhistorysummaryDetail: any = false;
  dataSearch;

  constructor(
    public paymentGatewayService:PaymentGatewayService, 
    public completerService: CompleterService,
    public router: Router    
    ) {
    this.dataService = completerService.local(this.searchData, 'color', 'color');
    this.routePath = routePath
   }

  async ngOnInit() {
    

    try{
      let result:any;
      this.service    = this.paymentGatewayService;
      result          = await this.paymentGatewayService.getPaymentGatewayLint();
      this.paymentGatewayData   = result.result;
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  onSearch(){
    
  }

  onDataChanged(){
    console.log('changed', this.dataSearch)
    this.router.navigateByUrl(this.dataSearch);
  }

}
