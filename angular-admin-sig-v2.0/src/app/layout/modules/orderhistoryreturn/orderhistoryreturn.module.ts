import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderhistoryreturnRoutingModule } from './orderhistoryreturn-routing.module';
import { OrderhistoryreturnComponent } from './orderhistoryreturn.component';
import { PageHeaderModule } from '../../../shared';
import { FormBuilderTableComponent }  from '../../../component-libs/form-builder-table/form-builder-table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { OrderhistoryreturnDetailComponent } from './detail/orderhistoryreturn.detail.component';
import { OrderhistoryreturnEditComponent } from './edit/orderhistoryreturn.edit.component';
import { NgxBarcodeModule } from 'ngx-barcode';
import {MatButtonModule} from '@angular/material/button';
import {MatRadioModule} from '@angular/material/radio';


@NgModule({
  imports: [
    CommonModule, 
    OrderhistoryreturnRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule,
    NgxBarcodeModule,
    MatButtonModule,
    MatRadioModule
  ],
  declarations: [
    OrderhistoryreturnComponent,
    OrderhistoryreturnDetailComponent,
    OrderhistoryreturnEditComponent
  ],
  exports:[
    OrderhistoryreturnComponent,
    OrderhistoryreturnDetailComponent,
    OrderhistoryreturnEditComponent
  ]
})
export class OrderhistoryreturnModule { }
