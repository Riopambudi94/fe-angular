import { Component, OnInit } from '@angular/core';
import { EscrowTransactionService } from '../../../services/escrow-transaction/escrow-transaction.service';
import { TableFormat } from '../../../object-interface/common.object';
import { Router } from '@angular/router';
import { getHost } from '../../../../environments/environment';


@Component({
  selector: 'app-escrow-transaction',
  templateUrl: './escrow-transaction.component.html',
  styleUrls: ['./escrow-transaction.component.scss']
})
export class EscrowTransactionComponent implements OnInit {

  
  EscrowData: any = [];
  tableFormat        : TableFormat = {
                                  title           : 'Escrow Transactions',
                                  label_headers   : [
                                    {label: 'Owner ID', visible: true, type: 'string', data_row_name: 'email'},
                                    {label: 'Nominal', visible: true, type: 'number', data_row_name: 'how_much'},
                                    {label: 'Description', visible: true, type: 'string', data_row_name: 'description'},
                                    {label: 'Created Date', visible: true, type: 'date', data_row_name: 'created_date'},
                                    {label: 'No. Ref', visible: true, type: 'string', data_row_name: 'record_id'},
                                    {label: 'Debit', visible: true, type: 'escrow-d', data_row_name: 'how_much'},
                                    {label: 'Credit', visible: true, type: 'escrow-c', data_row_name: 'how_much'},
                                    {label: 'Type', visible: true, options: ['withdrawal', 'plus', 'minus','hold'], type: 'list-escrow', data_row_name: 'type'},
                                    {label: 'status', visible: true, options: ['PENDING', 'RELEASED','HOLD','SUCCEESS'], type: 'list-escrow-status', data_row_name: 'transaction_status'},
                                  ],
                                  row_primary_key : '_id',
                                  formOptions     : {
                                                    row_id: '_id',
                                                    this  : this,
                                                    result_var_name: 'EscrowData',
                                                    detail_function: [this, 'callDetail']
                                                  }
  };
  form_input    : any = {};
  errorLabel : any = false;
  totalPage  = 0;
  page = 1;
  pageSize = 50;
  pointstransactionDetail: any = false;
  service: any;
  pages: any;
  currentPage= 1;
  pageNumbering: any[];
  total_page: any;
  orderBy: any;
  searchCallback: any;
  onSearchActive: boolean;
  showLoading: boolean;
  options: any;
  table_data: any;
  pageLimits: any;
  constructor(
    public EscrowTransactionService:EscrowTransactionService,
    private router: Router) { }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad(){
    try{
      this.service    = this.EscrowTransactionService;
      let result: any = await this.EscrowTransactionService.getEscrowtransactionLint();
      console.log("result", result)
      this.totalPage  = result.result.total_page
      this.pages = result.result.total_page
      console.log(" pages",this.pages)
      this.EscrowData = result.result.values;
      // if(result.result.total_page){
      //   this.buildPagesNumbers(this.pages)
      // }
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async callDetail(pointstransaction_id){
    try{
      let result: any;
      this.service    = this.EscrowTransactionService;
      result          = await this.EscrowTransactionService.detailEscrowtransaction(pointstransaction_id);
      console.log(result);

      this.pointstransactionDetail = result.result[0];
      console.log('ini',this.pointstransactionDetail)
      
      this.router.navigate(['administrator/escrow-transaction/detail'],  {queryParams: {id: pointstransaction_id }})
      
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  onChangePage(pageNumber){
    if(pageNumber <= 0 ){
      pageNumber = 1
    }
    if(pageNumber >= this.total_page){
      pageNumber = this.total_page;
    }

    this.currentPage = pageNumber;
    console.log(pageNumber, this.currentPage, this.total_page)

    this.valuechange({}, false, false)
  }
  pageLimitChanges(event) {
    console.log('Test limiter', this.pageLimits);
    this.valuechange({}, false, false);

  }
  pageLimiter() {
    return [
      { id: '10', name: '10' },
      { id: '20', name: '20' },
      { id: '50', name: '50' },
      { id: '100', name: '100' }
    ];
  }
  
  isCurrentPage(tp){
    if(tp == this.currentPage){
      return 'current-page';
    }
    else return ''
  }

  pagination(c, m) {
    var current = c,
      last = m,
      delta = 2,
      left = current - delta,
      right = current + delta + 1,
      range = [],
      rangeWithDots = [],
      l;

    for (let i = 1; i <= last; i++) {
      if (i == 1 || i == last || i >= left && i < right) {
        range.push(i);
      }
    }

    for (let i of range) {
      if (l) {
        if (i - l === 2) {
          rangeWithDots.push(l + 1);
        } else if (i - l !== 1) {
          rangeWithDots.push('...');
        }
      }
      rangeWithDots.push(i);
      l = i;
    }
    
    return rangeWithDots;
    
  }

  buildPagesNumbers(totalPage){
    this.pageNumbering = [];
    
    let maxNumber = 8;
    this.total_page = totalPage;
    this.pageNumbering = this.pagination(this.currentPage, totalPage);
    console.log(" numbering page ", this.pageNumbering)
    
  }
  orderChange(event, orderBy) {
    if (this.orderBy[orderBy]) {
      this.orderBy[orderBy].asc = !this.orderBy[orderBy].asc;
    }
    else {
      this.orderBy = [];
      this.orderBy[orderBy] = { asc: false };
    }
    this.valuechange(event, orderBy);

  }

 async valuechange(event, input_name, download?:boolean) {
    
    const searchCallback = this.searchCallback;
    console.log(searchCallback)
    let filter = {
      search: '', 
      order_by: {},
      limit_per_page: this.pageSize,
      current_page: this.currentPage,
      download: false
     }
     let result = await this.EscrowTransactionService.searchEscrowtransactionLint(filter);
     this.EscrowData = result.result.values;

  }


  public async backToHere(obj){
    obj.pointstransactionDetail = false;
  }

}
