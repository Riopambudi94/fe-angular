import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PromotionRuleRoutingModule } from './promotion-rule-routing.module';
import { PromotionRuleComponent } from './promotion-rule.component';


@NgModule({
  declarations: [PromotionRuleComponent],
  imports: [
    CommonModule,
    PromotionRuleRoutingModule
  ]
})
export class PromotionRuleModule { }
