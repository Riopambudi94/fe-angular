import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BroadcastComponent } from './broadcast.component';
import { BroadcastAddComponent } from './add/broadcast.add.component';
import { FormInputComponent }  from '../../../component-libs/form-input/form-input.component';
import { PageHeaderModule } from './../../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { FormSelectComponent } from '../../../component-libs/form-select/form-select.component';
import { BroadcastDetailComponent } from './detail/broadcast.detail.component';
import { BroadcastRoutingModule } from './broadcast-routing.module';
import { BroadcastEditComponent } from './edit/broadcast.edit.component';
import { BsComponentModule } from '../bs-component/bs-component.module';
import {MatCheckboxModule} from '@angular/material/checkbox';

@NgModule({
  imports: [CommonModule,
    BroadcastRoutingModule, 
    PageHeaderModule, 
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule,
    MatCheckboxModule,
],
  
  declarations: [
    BroadcastComponent, BroadcastAddComponent, BroadcastDetailComponent, BroadcastEditComponent] 
})
export class BroadcastModule { }
