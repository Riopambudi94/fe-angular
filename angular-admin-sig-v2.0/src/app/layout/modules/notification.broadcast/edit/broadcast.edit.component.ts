import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { BroadcastService } from '../../../../services/notification.broadcast/broadcast.service';


@Component({
  selector: 'app-broadcast-edit',
  templateUrl: './broadcast.edit.component.html',
  styleUrls: ['./broadcast.edit.component.scss'],
  animations: [routerTransition()]
})

export class BroadcastEditComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  public loading        :boolean = false;

  public settingBroadcast  : any = [];
  public groupBroadcast  : any = [];
  public templateBroadcast  : any = [];
  public broadcastStatus  : any = [
    {label:"ACTIVE", value:"ACTIVE"}
   ,{label:"INACTIVE", value:"INACTIVE"}
  ];
  errorLabel    : any = false;
  setting       : any = false;
  group         : any = false;
  template      : any = false;
  status:any;
  

  constructor(public broadcastService:BroadcastService) {}

  ngOnInit() {
    this.getCollection();
    this.firstLoad();
  }

  async getCollection(){
    try{
      this.setting = await this.broadcastService.getSettingLint();
      this.setting.result.forEach((element) => {
        this.settingBroadcast.push({label:element.name, value:element.id});
      });
      this.group = await this.broadcastService.getGroupLint();
      this.group.result.forEach((element) => {
        this.groupBroadcast.push({label:element.name, value:element.id});
      });
      this.template = await this.broadcastService.getTemplateLint();
      this.template.result.forEach((element) => {
        this.templateBroadcast.push({label:element.title, value:element.id});
      });
    }
    catch (e){
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  firstLoad(){
    this.detail.previous_status = this.detail.status;
    this.broadcastStatus.forEach((element, index) => {
        if(element.value == this.detail.status){
            this.broadcastStatus[index].selected = 1;
        }
    });
    console.log('template : ',this.broadcastStatus);

    this.detail.previous_setting_id = this.detail.setting_id;
    this.settingBroadcast.forEach((element, index) => {
        if(element.value == this.detail.setting_id){
            this.settingBroadcast[index].selected = 1;
        }
    });
    this.detail.previous_group_id = this.detail.group_id;
    this.groupBroadcast.forEach((element, index) => {
        if(element.value == this.detail.group_id){
            this.groupBroadcast[index].selected = 1;
        }
    });
    this.detail.previous_template_id = this.detail.template_id;
    this.templateBroadcast.forEach((element, index) => {
        if(element.value == this.detail.template_id){
            this.templateBroadcast[index].selected = 1;
        }
    });
    
  }

  backToDetail(){
    //console.log(this.back);
    this.back[0][this.back[1]]();
  }

  async saveThis(){
    try 
    {
      console.log(this.detail);
      this.loading=!this.loading;
      await this.broadcastService.updateBroadcastID(this.detail);
      this.loading=!this.loading;
    } 
    catch (e) 
    {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }
  
}
