import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { BroadcastService } from '../../../../services/notification.broadcast/broadcast.service';

@Component({
  selector: 'app-broadcast-detail',
  templateUrl: './broadcast.detail.component.html',
  styleUrls: ['./broadcast.detail.component.scss'],
  animations: [routerTransition()]
})

export class BroadcastDetailComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;
  errorLabel    : any = false;
  edit:boolean = false;
  
  constructor(public broadcastService:BroadcastService) {
  }

  ngOnInit() {
    this.firstLoad();
    
  }

  async firstLoad(){
    
  }

  editThis(){
    //console.log(this.edit );
    this.edit = !this.edit;
    //console.log(this.edit );
  }
  backToTable(){
    console.log(this.back);
    this.back[1](this.back[0]);
  }

  async deleteThis(){
    try{
      let delResult: any = await this.broadcastService.deleteNotificationSetting(this.detail);
      //console.log(delResult);
      if(delResult.error==false){
          console.log(this.back[0]);
          this.back[0].broadcastDetail=false;
          this.back[0].firstLoad();
          // delete this.back[0].notificationDetail;
      } 
    } catch (e) {
        this.errorLabel = ((<Error>e).message);//conversion to Error type
    }      
    
  }
}
