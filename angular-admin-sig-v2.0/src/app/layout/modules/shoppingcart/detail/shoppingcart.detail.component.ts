import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { ShoppingcartService } from '../../../../services/shoppingcart/shoppingcart.service';

@Component({
  selector: 'app-shoppingcart-detail',
  templateUrl: './shoppingcart.detail.component.html',
  styleUrls: ['./shoppingcart.detail.component.scss'],
  animations: [routerTransition()]
})

export class ShoppingcartDetailComponent implements OnInit {
  @Input() public detail: any;
  @Input() public back;

  edit:boolean = false;
  
  constructor(public shoppingcartService:ShoppingcartService) {
  }

  ngOnInit() {
    this.firstLoad();
    
  }

  async firstLoad(){
    
    
  }

  editThis(){
    console.log(this.edit );
    this.edit = !this.edit;
    console.log(this.edit );
  }
  backToTable(){
    console.log(this.back);
    this.back[1](this.back[0]);
  }

}
