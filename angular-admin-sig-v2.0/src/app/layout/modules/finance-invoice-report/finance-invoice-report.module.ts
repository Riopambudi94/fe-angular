import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FinanceInvoiceReportRoutingModule } from './finance-invoice-report-routing.module';
import { FinanceInvoiceReportComponent } from './finance-invoice-report.component';
import { PageHeaderModule } from '../../../shared';
import { FormBuilderTableComponent }  from '../../../component-libs/form-builder-table/form-builder-table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilderTableModule } from '../../../component-libs/form-builder-table/form-builder-table.module';
import { NgbModule, } from '@ng-bootstrap/ng-bootstrap';
import { FinanceInvoiceReportDetailComponent } from './detail/finance-invoice-report.detail.component';
import { FinanceInvoiceReportEditComponent } from './edit/finance-invoice-report.edit.component';
import { UpdateInvoiceComponent } from './update-invoice/finance-invoice-report.update-invoice.component';
import { ListOrderComponent } from './list-order/list-order.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
// import { BastComponent } from './bast/bast.component';

import { BsComponentModule } from '../bs-component/bs-component.module';

@NgModule({
  
  imports: [
    CommonModule,
    FinanceInvoiceReportRoutingModule,
    PageHeaderModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    FormBuilderTableModule,
    BsComponentModule,
    PdfViewerModule
  ],
    declarations: [
      FinanceInvoiceReportComponent,
      FinanceInvoiceReportDetailComponent,
      FinanceInvoiceReportEditComponent,
      UpdateInvoiceComponent,
      ListOrderComponent
    ],
  
})
export class FinanceInvoiceReportModule { }
