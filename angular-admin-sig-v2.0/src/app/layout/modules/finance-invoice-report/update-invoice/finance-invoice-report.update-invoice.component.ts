import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { OrderhistoryService } from '../../../../services/orderhistory/orderhistory.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-update-invoice',
  templateUrl: './finance-invoice-report.update-invoice.component.html',
  styleUrls: ['./finance-invoice-report.update-invoice.component.scss'],
  animations: [routerTransition()]
})

export class UpdateInvoiceComponent implements OnInit {
  @ViewChild('updateBulk', {static: false}) updateBulk;
  
  public name: string = "";
  Members: any = [];
  service: any;
  full_name:any;
  email:any;
  password:any;
  cell_phone:any;
  card_number:any;
  dob:any;
  mcity:any;
  mstate:any;
  mcountry:any;
  maddress1:any;
  mpostcode:any;
  gender:any;
  marital_status:any;
  type:any;
  member_status:any;
  activation_status:any;
  public form: any = {
    id_pel: "",
    nama_toko: "",
    nama_distributor: "",
    alamat_toko:"",
    kelurahan_toko: "-",
    kecamatan_toko: "-",
    kota_toko: "-",
    provinsi_toko: "-",
    kode_pos_toko: "-",
    landmark_toko: "-",
    nama_pemilik: "",
    telp_pemilik: "",
    no_wa_pemilik: "",
    ktp_pemilik:"",
    foto_ktp_pemilik: "-",
    npwp_pemilik:"",
    foto_npwp_pemilik: "-",
    alamat_rumah:"",
    kelurahan_rumah:"",
    kecamatan_rumah:"",
    kota_rumah:"",
    provinsi_rumah:"",
    kode_pos_rumah:"",
    landmark_rumah: "-",
    description: "nonupfront",
    hadiah_dikuasakan: "TIDAK KUASA",

    nama_penerima_gopay: "-",
    ktp_penerima: "-",
    foto_ktp_penerima: "-",
    npwp_penerima: "-",
    foto_npwp_penerima: "-",
    telp_penerima_gopay: "-",
    no_wa_penerima: "-",
    nama_id_gopay: "-",
    jenis_akun: "-",
    saldo_terakhir: "-",
  }
  
  errorLabel : any = false;
  public member_status_type = [
                       {label:'Superuser', value:'superuser'}
                      ,{label:'Member', value:'member', selected:1}
                      ,{label:'Marketing', value:'marketing'}
                      ,{label:'Admin', value:'admin'}
                      ,{label:'Merchant', value:'merchant'}
                    ];
  
  public group_desc: any = [
    {label:'Nonupfront', value:'nonupfront'}
    ,{label:'Upfront', value:'upfront'}
  ];

  public kuasa_option: any = [
    {label:'TIDAK KUASA', value:'TIDAK KUASA'},
    {label:'KUASA', value:'KUASA'}
  ];

  isBulkUpdate : any = false;
  showUploadButton: any = false;
  selectedFile  = null;
  cancel = false;
  progressBar = 0;
  errorFile: any = false;
  prodOnUpload: any = false;
  startUploading: any = false;

  constructor(public OrderhistoryService:OrderhistoryService) {
    
   }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad(){
    this.selectedFile  = null;

    this.prodOnUpload = false;
  }

  onFileSelected(event){
    this.errorFile = false;
    let fileMaxSize = 3000000;// let say 3Mb
    // var reader = new FileReader();
    // reader.readAsDataURL(event.target.files[0]); //
    Array.from(event.target.files).forEach((file: any) => {
        if(file.size > fileMaxSize){
          this.errorFile="maximum file is 3Mb";
        }
    });
    
    this.selectedFile = event.target.files[0];
    this.updateBulk.nativeElement.value = '';
    
  } 
  
  cancelThis(){
    this.cancel = !this.cancel;
  }

  async updateDataBulk(){
    try
      {
        this.startUploading = true;
        this.cancel = false;
        let payload = {
          type : 'application/form-data',
        }
        if(this.selectedFile) {
          console.log('file', this.selectedFile,this)
          const result: any = await this.OrderhistoryService.updateBulkInvoice(this.selectedFile,this,payload,"invoice_no");
          if (result) {
            this.firstLoad();
          }
        }
        // this.firstLoad();
      } 
      catch (e) 
      {
        this.startUploading = false;
        this.errorLabel = ((<Error>e).message);//conversion to Error type

        let message = this.errorLabel;
        if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
          message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
        }
        Swal.fire({
          icon: 'error',
          title: message,
        });
      }
  }

  updateProgressBar(value){
    this.progressBar = value;
  }

  backTo(){
    window.history.back();
  }

}
