import { Component, OnInit } from '@angular/core';
import {routerTransition} from '../../../router.animations';
import { CodePageService } from '../../../services/code-page/code-page.service';
import {FormOptions, TableFormat} from '../../../object-interface/common.object';

@Component({
    selector: 'app-code-page',
    templateUrl: './code-page.component.html',
    styleUrls: ['./code-page.component.scss'],
     animations: [routerTransition()]
})

export class CodePageComponent implements OnInit {

    PromoCode: any = [];
  tableFormat: TableFormat = {
                                  title           : 'Promo Code Detail Page',
                                  label_headers   : [
                                    // {label: 'ID', visible: false, type: 'string', data_row_name: '_id'},
                                    {label: 'Name', visible: true, type: 'string', data_row_name: 'promo_name'},
                                    {label: 'Code', visible: true, type: 'string', data_row_name: 'promo_code'},
                                    // {label: 'Description', visible: false, type: 'string', data_row_name: 'promo_description'},
                                    {label: 'Value', visible: true, type: 'string', data_row_name: 'value'},
                                    // {label: 'Rule ID', visible: false, type: 'string', data_row_name: 'rule_id'},
                                    // {label: 'Type', visible: false, type: 'string', data_row_name: 'type'},
                                    {label: 'Category', visible: true, type: 'string', data_row_name: 'category'},
                                    // {label: 'Start Date', visible: false, type: 'date', data_row_name: 'start_date'},
                                    // {label: 'End Date', visible: false, type: 'date', data_row_name: 'end_date'},
                                    // {label: 'Active', visible: false, type: 'string', data_row_name: 'active'},
                                  ],
                                  row_primary_key : '_id',
                                  formOptions     : {
                                                    row_id: '_id',
                                                    this  : this,
                                                    result_var_name: 'PromoCode',
                                                    detail_function: [this, 'callDetail']
                                                  }
  };
  form_input: any = {};
  errorLabel: any = false;

  promocodeDetail: any = false;
  service: any;

  constructor(
      public codepageService: CodePageService
  ) { }

  ngOnInit() {
      this.firstLoad();
  }

  async firstLoad() {
    try {
      this.service    = this.codepageService;
      const result: any  = await this.codepageService.getPromoCodeLint();
      console.log(result);
      this.PromoCode = result.result;
    } catch (e) {
      this.errorLabel = ((<Error>e).message); // conversion to Error type
    }
  }

  public async callDetail(promotion_id) {
    try {
      let result: any;
      this.service    = this.codepageService;
      result          = await this.codepageService.detailPromoCode(promotion_id);
      console.log(result);

      this.promocodeDetail = result.result[0];

    } catch (e) {
      this.errorLabel = ((<Error>e).message); // conversion to Error type
    }
  }

  public async backToHere(obj) {
    obj.promocodeDetail = false;
  }

}

