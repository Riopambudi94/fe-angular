import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PermissionObserver } from '../../services/observerable/permission-observer';

@Component({
  selector: 'app-administrator',
  templateUrl: './admin-management.component.html',
  styleUrls: ['./admin-management.component.scss']
})
export class AdminManagementComponent implements OnInit {

  collapedSideBar: boolean;

  constructor(private route:Router, private castPermission: PermissionObserver) {
    this.castPermission.currentPermission.subscribe((permissionVal) => {
      if(permissionVal != 'admin'){
        this.route.navigateByUrl("/");
      }
    });
  }

  ngOnInit() {}

  receiveCollapsed($event) {
      this.collapedSideBar = $event;
  }

}
