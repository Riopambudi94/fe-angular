import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdministratorComponent } from './administrator.component';
import { administratorRoutePath } from './administrator-route-path';

const routes: Routes = [
  {path: '',  component: AdministratorComponent, children: administratorRoutePath },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministratorRoutingModule { }
