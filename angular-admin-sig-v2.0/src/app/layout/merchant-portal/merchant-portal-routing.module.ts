import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {MerchantPortalComponent} from './merchant-portal.component';
import {path} from './merchant-route-path';
import { PortalGuard } from '../../shared/guard/portal.guard';
import { EvoucherModule } from '../modules/product.evoucher/evoucher.module';

const routes: Routes = [
    {
        path: '',
        component: MerchantPortalComponent,
        children: [
            ...path
        ],
        // canActivate: [PortalGuard]

    }

];


@NgModule({
    imports: [
        RouterModule.forChild(routes),
        // EvoucherModule
    ],
    exports: [RouterModule]
})
export class MerchantPortalRoutingModule {
}
