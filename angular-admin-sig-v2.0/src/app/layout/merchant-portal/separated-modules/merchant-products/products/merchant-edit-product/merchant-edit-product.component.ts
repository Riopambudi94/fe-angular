// Dear Future Developer, please consider leaving notes on your code, good habits will reduce development time ~ H

import { Component, OnInit } from '@angular/core';
import { formatCurrency } from '@angular/common';
import { ProductService } from '../../../../../../services/product/product.service';
import { isArray } from 'ngx-bootstrap/chronos';
import Swal from 'sweetalert2';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { CategoryService } from '../../../../../../services/category/category.service';
import { Router, ActivatedRoute } from '@angular/router';
import { PermissionObserver } from '../../../../../../services/observerable/permission-observer';
import { EVoucherService } from '../../../../../../services/e-voucher/e-voucher.service';
import { MerchantService } from '../../../../../../services/merchant/merchant.service';
import { isString } from '../../../../../../object-interface/common.object';
import {Location} from '@angular/common';

@Component({
	selector: 'app-merchant-edit-product-product',
	templateUrl: './merchant-edit-product.component.html',
	styleUrls: ['./merchant-edit-product.component.scss']
})
export class MerchantEditProductComponent implements OnInit {
	// images = [1, 2, 3].map(() => `https://picsum.photos/900/500?random&t=${Math.random()}`);

	genVoucher = {
		expiry_date: null,
		qty: 1
	};
	toggleDelete = false;
	oldProductCodeName;
	old_id;
	_id;
	errorFile: any = false;
	progressBar;
	productID;
	selFile;
	cancel;
	selectedFile: any;
	generateVoucherToggler = 0;
	// edit_status = true

	listOfProductType = [{ name: 'Product', active: true }, { name: 'evoucher', active: false }];

	listOfRedeemType = [{ name: 'Voucher Link', active: false }, { name: 'Voucher Code', active: false }];

	listOfVendorType = [];
	listOfMerchant = [];

	Editor = ClassicEditor;
	config = {
		toolbar: ['heading', '|', 'bold', 'italic', 'bulletedList', 'numberedList', 'blockQuote'],
		heading: {
			options: [
				{ model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
				{ model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
				{ model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' }
			]
		}
	}
	autoSKU = false;
	hashtagsKey = '';
	form: any = {
		type: 'product',
		min_order: 1,
		product_code: '',
		product_name: '',
		product_price:0,
    	product_value:0,
		variation: {},
		category: 'public',
		status:'',

		price: 0,
		fixed_price: 0,
		qty: 0,
		description: '',
		tnc: '',
		location_for_redeem: '',
		dimensions: {
			width: 0,
			length: 0,
			height: 0
		},
    	product_sku:[],
		weight: 1,
		images_gallery: [],
		// weightInGram: 1000,
		pic_file_name: '',
		need_outlet_code: 0,
		applicable_for_ios: false,
	};
	uploads: any = {
		image1: '',
		image2: '',
		image3: '',
		image4: '',
		image5: '',
	};
	gallery_label: any = ['image3', 'image1', 'image2', 'image4', 'image5'];
	labels = {
		type: 'product type',
		min_order: 'minimum order',
		product_code: 'product code',
		product_name: 'product name',
		category: 'category',
		price: 'price',
		fixed_price: 'public price',
		qty: 'quantity',
		description: 'description',
		tnc: 'term and conditions',
		dimensions: 'dimensions',
		weight: 'weight'
	};
	editype =''
	errorLabel: any = false;
	errorMessage  : any = false;
	isFileUploaded = false;
	multipleCategories = [];
	currentPermission;
	temp_gallery: any = [];
	result_id: [];
	multipleCategoriesDeals: any;
	multipleCategoriesProduct: any;
	showLoading: boolean = false;
	showLoading2: boolean = false;
	showLoading3: boolean = false;
	showLoading4: boolean = false;
	showLoading5: boolean = false;

	public productStatus: any = [
		{label: 'ACTIVE', value: 'ACTIVE'},
		{label: 'INACTIVE', value: 'INACTIVE'}
	];

	varianHeader: any = [];

	dataVarian: any = [];

	dataVarianModel:any = [];

	headerValue:any = [
		'SKU Code',
		'SKU Price',
		'SKU Value',
		'Stok',
		'Gambar',
		'Status',
	];

	valueVariation:any = [];

	productSku:any = [];

	isLoading = [];

	title:any = '';

	photoAlert: boolean = false;
	productNameAlert: boolean = false;
	productCodeAlert: boolean = false;
	descriptionAlert: boolean = false;
	tncAlert: boolean = false;
	productPriceAlert: boolean = false;
	productValueAlert: boolean = false;

	weightAlert: boolean = false;
	widthAlert: boolean = false;
	lengthAlert: boolean = false;
	heightAlert: boolean = false;

	varianAlert: boolean = false;
	varianAlertMessage: string = '';

	categoryAlert: boolean = false;

	categoryList:any = [];
	categoryProduct:any;
	categoryVoucher:any;
	categoryEvoucher:any;
	categoryGold:any;
	categoryEwallet:any;


	varianX = {
		name : '',
		active : false
	  };
	
	  varianY = {
		name : '',
		active : false
	  };
	
	  varianTypeX : any = [];
	  varianTypeY : any = [];
	
	  fieldVarianTypeX : Array<any> = [];
	  fieldVarianTypeY : Array<any> = [];
	
	  multiVarian : Array<Array<any>> = [];
	
	  fieldVarianX : any = {
		alert : false,
		message : ""
	  }
	  fieldVarianY : any = {
		alert : false,
		message : ""
	  }

	constructor(
		private productService: ProductService,
		private categoryService: CategoryService,
		private router: Router,
		private route: ActivatedRoute,
		private permission: PermissionObserver,
		private eVoucherService: EVoucherService,
		private merchantService: MerchantService,
		private _location: Location
	) {
		this.permission.currentPermission.subscribe((val) => {
			this.currentPermission = val;
		});
	}

	async ngOnInit() {
		this.inLoadedProduct();
		console.log('multivarian', this.multiVarian, this.multiVarian.length);
	}

	setVendorType(object) {
		this.listOfVendorType.forEach((el) => {
			if(el.name == object){
				el.active = true
			}else {
				el.active = false;
			}
			
		});
		// object.active = true;
		this.form.voucher_group = object;
	}
	async setVoucherGroup(){
		// console.log('form type', this.editype)
		
			// let mGroup = await this.merchantService.getVoucherGroup();
			// if (mGroup.result) { //if there is a result

			// 	this.listOfVendorType = [];
			// 	mGroup.result.voucher_group.forEach((el, i) => { //for each of the voucher group , push it to listOfVendorType array
			// 		this.listOfVendorType.push({ name: el.name, active: false });
			// 	})
			// 	// this.listOfVendorType.unshift({ name: 'MANUAL', active: true });
			// }
			
			
			// this.setVendorType(this.form.voucher_group);
			// this.setRedeemType(this.listOfRedeemType[0]);
			// if (mGroup.result) {
			// 	this.listOfVendorType = [];
			// }

			// mGroup.result.voucher_type.forEach((el, i) => {
			// 	this.listOfVendorType.push({ name: el.name });
			// });
			// this.listOfVendorType.unshift({name: 'MANUAL', active:true})

		

	}

	setRedeemType(object) {
		this.listOfRedeemType.forEach((el) => {
			el.active = false;
		});
		object.active = true;
		this.form.redeem_type = object.name;
	}

	inLoadedProduct() {
		// console.log(this.permission.getCurrentPermission());


		try {
			
		this.route.queryParams.subscribe(async (params) => {
			this.productID = params.product_code;
			let result = await this.productService.detailProducts(this.productID);
			// if(result.toString().includes("Unauthorized") || result.toString().includes("unauthorized") || result.toString().message.includes("access not permitted")) {
			// 	this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
			// }

			let data = result[0];

			if (isString(data.hashtags)) {
				data.hashtags = [];
			}

			if (!data.condition) {
				data.condition = 'new';
			}
			if (!data.hashtags) {
				data.hashtags = [];
			}

			this.oldProductCodeName = data.product_code;
			this.form = {
				type: data.type,
				voucher_group: data.voucher_group ? data.voucher_group : null,
				merchant_username: data.merchant_username ? data.merchant_username : null,
				min_order: data.min_order,
				product_code: data.product_code,
				product_name: data.product_name,
				category: data.category,
				price: data.price,
				fixed_price: data.fixed_price,
				qty: data.qty,
				description: data.description,
				location_for_redeem: data.location_for_redeem,
				tnc: data.tnc,
				need_outlet_code: data.need_outlet_code,
				applicable_for_ios :  data.applicable_for_ios,
				dimensions: {
					width: data.dimensions ? data.dimensions.width : 0,
					length: data.dimensions ? data.dimensions.length : 0,
					height: data.dimensions ? data.dimensions.height : 0
				},
				condition: data.condition,
				weight: data.weight ? data.weight : 0,
				status: data.status,
				images_gallery: data.images_gallery,
				base_url: data.base_url,
				pic_big_path: data.pic_big_path,
				pic_small_path: data.pic_small_path,
				pic_medium_path: data.pic_medium_path,
				pic_file_name: data.pic_file_name,
				hashtags: data.hashtags,
				variation: data.variation,
			};

			if(data.type == 'e-voucher') {
				this.form.sku_code = data.product_sku[0].sku_code;
			}
			
			if (
                (data.variation == null ||
                    (data.variation &&
                        ((Object.keys(data.variation).length === 0 &&
                            data.variation.constructor === Object) ||
                            (Array.isArray(data.variation) &&
                                data.variation.length == 0)))) &&
                data.product_sku &&
                data.product_sku.length > 0
            ) {
                this.form.product_value = data.product_sku[0].sku_value;
                this.form.product_price = data.product_sku[0].sku_price;
                this.form.qty = data.product_sku[0].qty;
            } else if (data.product_sku && data.product_sku.length > 0) {
                this.productSku = data.product_sku;

            }

			if(this.form.type == 'product') {
				this.title = 'Product';
			  } else if (this.form.type == 'voucher') {
				this.title = 'Voucher Fisik';
			  } else if (this.form.type == 'e-voucher') {
				this.title = 'E-Voucher';
			  } else if (this.form.type == 'gold') {
					this.title = 'Gold';
				} else if (this.form.type == 'top-up') {
					this.title = 'E-Wallet';
				}

			this.productStatus.forEach((element, index) => {
				if (element.value == this.form.status) {
					this.productStatus[index].selected = 1;
				}
			});

			this.editype = data.type
			this.loadImageToForm();
			if (this.form.images_gallery) {
				this.temp_gallery = this.form.images_gallery;
			}


			if (this.currentPermission == 'admin') {
				this.form.merchant_username = data.merchant_username;
			}
			//by default weight is in Kg, need to convert it in gram
			// this.form.weightInGram = this.form.weight * 1000;
			this.formPriceReplacer(null, 'price');
			this.formPriceReplacer(null, 'fixed_price');
			this.formPriceReplacer(null, 'weight');
			this.setVoucherGroup();
			this.afterProductLoaded();

			if(data.variation &&  data.variation.length != 0) {
				
				Object.entries(data.variation).forEach(([key, value],index) => {

					if(index == 0 ){
						this.varianX = {
							name : key,
							active : true
						};
						this.varianTypeX  = value;
						this.varianTypeY = [''];
					}
					if(index == 1){
						this.varianY = {
							name : key,
							active :true
						}
						this.varianTypeY = value;
					}
				});

				if(this.varianY.active){
					for(let i = 0; i < this.varianTypeY.length; i++){
						this.multiVarian[i] = [];
						for(let j = 0; j < this.varianTypeX.length; j++){
							this.multiVarian[i][j] = this.generateProductSKU();
						}
					}
				} else {
					this.multiVarian.push([]);
					for(let j = 0; j < this.varianTypeX.length; j++){
						this.multiVarian[0].push(this.generateProductSKU());
	
					}
				}


				for(let i = 0; i < data.product_sku.length; i++){
						var x : any;
						var y : any;
						Object.entries(data.product_sku[i].variant).forEach(([key, value],index) => {
							if(this.varianX.name == key){
								x = this.varianTypeX.indexOf(value);
							}
							else if(this.varianY.name == key){
								y = this.varianTypeY.indexOf(value);
							}
						});
						
						if(typeof y == 'number'){
							this.multiVarian[y][x] = data.product_sku[i];
							this.multiVarian[y][x].isloading = false;
						} else {
							this.multiVarian[0][x] = data.product_sku[i];
							this.multiVarian[0][x].isloading = false;
						}
					
				}
			}

		});

		} catch (error) {
			this.errorLabel = ((<Error>error).message);//conversion to Error type

			if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
				this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
			}
		}
	}

	clickVarian() {
		this.addVariationType();
		// this.form.product_price = 0;
		// this.form.product_value = 0;
	}

	changeText() {
		try {
		  this.productSku = [];
		  this.isLoading = [];
	
		  for (let i = 0; i < this.dataVarianModel[0].length; i++) {
			if(this.dataVarianModel[1]) {
			  for (let j = 0; j < this.dataVarianModel[1].length; j++) {
				this.productSku.push(
				  {
					// "sku_code":"TEST1",
					// "sku_price":100000,
					// "sku_value":1000,
					"variant":{
					  [this.varianHeader[0]]:this.dataVarianModel[0][i],
					  [this.varianHeader[1]]:this.dataVarianModel[1][j],
					},
					// "qty":100,
					"image_gallery":[
						// {
						//     "base_url" : "https://res.cloudinary.com/dqcj36zfd/image/upload/",
						//     "pic_file_name" : "v1615863583/admin/image/montiss-1.60501f1dce424.jpg",
						//     "pic_small_path" : "s--g8KpJ7Mu--/c_limit,q_auto:good,w_150/v1/admin/image/montiss-1.60501f1dce424.jpg",
						//     "pic_medium_path" : "s--dMwcc-P3--/c_limit,q_auto:good,w_600/v1/admin/image/montiss-1.60501f1dce424.jpg",
						//     "pic_big_path" : "v1615863583/admin/image/montiss-1.60501f1dce424.jpg"
						// }
					],
					// "status":"ACTIVE"
				  },
				);
				this.isLoading.push(false);
			  }
			} else {
			  this.productSku.push(
				{
				  // "sku_code":"TEST1",
				  // "sku_price":100000,
				  // "sku_value":1000,
				  "variant":{
					[this.varianHeader[0]]:this.dataVarianModel[0][i],
				  },
				  // "qty":100,
				  "image_gallery":[
					  // {
					  //     "base_url" : "https://res.cloudinary.com/dqcj36zfd/image/upload/",
					  //     "pic_file_name" : "v1615863583/admin/image/montiss-1.60501f1dce424.jpg",
					  //     "pic_small_path" : "s--g8KpJ7Mu--/c_limit,q_auto:good,w_150/v1/admin/image/montiss-1.60501f1dce424.jpg",
					  //     "pic_medium_path" : "s--dMwcc-P3--/c_limit,q_auto:good,w_600/v1/admin/image/montiss-1.60501f1dce424.jpg",
					  //     "pic_big_path" : "v1615863583/admin/image/montiss-1.60501f1dce424.jpg"
					  // }
				  ],
				  // "status":"ACTIVE"
				},
			  );
			  this.isLoading.push(false);
			}
		  }
		} catch (error) {
		  console.log("error", error);
		}
	  }
	
	  addVariationType() {
		this.dataVarian.push([
		  ''
		]);
	
		this.dataVarianModel.push([
		  ''
		]);

		if(this.multiVarian.length < 1){
			this.varianX.active = true;
			this.addVarianTypeY('');
			this.addVarianTypeX('');
		  }
	  }


	  activatedVarianY(){
		this.varianY.active = true;
		this.validateMultiVarian(false,false);
	  }
	
	  addVarianTypeX(fieldX: any = ''){
		this.varianTypeX.push(fieldX);
		console.log("varian type y",this.varianTypeY);
		for(let i = 0; i < this.varianTypeY.length; i++){
		  this.multiVarian[i].push(this.generateProductSKU(this.varianTypeY[i],fieldX));
		  console.log("data Varian",this.multiVarian);
		}
		this.validateMultiVarian(false,false);
	  }
	
	  deleteVarianTypeX(index : number){
		for(let i = 0; i < this.varianTypeY.length; i++){
		  this.multiVarian[i].splice(index,1);
		}
		this.varianTypeX.splice(index,1);
		this.validateMultiVarian(false,false);
	  }
	
	  addVarianTypeY(fieldY: any = ''){
		this.varianTypeY.push(fieldY);
		let arrayY : any = [];
		for(let i = 0; i < this.varianTypeX.length; i++){
		  arrayY.push(this.generateProductSKU(fieldY,this.varianTypeX[i]));
		}
		this.multiVarian.push(arrayY);
		this.validateMultiVarian(false,false);
	  }
	
	  deleteVarianTypeY(index : number){
		this.multiVarian.splice(index,1);
		this.varianTypeY.splice(index,1);
		this.validateMultiVarian(false,false);
	  }
	
	  deleteAllTypeY(){
		if(this.varianX.active){
		  this.multiVarian.splice(1,this.multiVarian.length - 1);
		  this.varianTypeY = [''];
		  this.varianY.active = false;
		  this.varianY.name = '';
		  this.fieldVarianX = {
			alert : false,
			message : ''
		  };
		  this.fieldVarianY = {
			alert : false,
			message : ''
		  }
		} else {
			this.deleteAllVarian();
		}
		this.validateMultiVarian(false,false);
	  }
	
	  deleteAllTypeX(){
		if(this.varianY.active){
		  const tempX : Array<any> = []; 
		  for(let i = 0; i < this.multiVarian.length; i++){
			tempX.push(this.multiVarian[i][0]);
		  }
		  this.multiVarian = [tempX];
		  this.varianTypeX = [...this.varianTypeY];
		  this.varianX.name = this.varianY.name;
		  this.varianTypeY = [''];
		  this.varianY.active = false;
		  this.varianY.name = '';
		  this.fieldVarianX = {
			alert : false,
			message : ''
		  };
		  this.fieldVarianY = {
			alert : false,
			message : ''
		  }
		}else {
			this.deleteAllVarian();
		}
		this.validateMultiVarian(false,false);
	  }
	
	  deleteAllVarian(){
		this.varianX = {
		  name : '',
		  active : false
		};
	  
		this.varianY = {
		  name : '',
		  active : false
		};
	  
		this.varianTypeX = [];
		this.varianTypeY = [];
	  
		this.multiVarian = [];
		this.fieldVarianX = {
		  alert : false,
		  message : ''
		};
		this.fieldVarianY = {
		  alert : false,
		  message : ''
		}
	  }
	
	  generateProductSKU(valueY : string = '', valueX: string = ''){
		return {
		  sku_code : '',
		  sku_price : '',
		  sku_value : '',
		  qty : 0,
		  image_gallery : [],
		  status : 'ACTIVE',
		  isloading:false,
	
		}
	  }	

	  scrollView = (id : string, alert : string = '' , scroll : boolean = true, objectScroll : any = false) => {
		if(alert)this[alert] = true;
		const defObject = objectScroll ? objectScroll : {
		  behavior: "smooth",
		  block: "start",
		  inline: "nearest"
		};
		if(scroll)document.getElementById(id).scrollIntoView(defObject);
		return false;
	  }
	
	  eventValidation = (event : any) => {
		this.validateMultiVarian(false,false);
	  }
	
	  validateMultiVarian = (valReturn : boolean = false, scroll : boolean = true) => {
		this.varianAlertMessage = '*data varian harus lengkap';
		let isFormValid : boolean = true;
		this.fieldVarianTypeX = [];
		this.fieldVarianTypeY = [];
		this.varianX.name = this.varianX.name.toString().trim();
		this.varianY.name = this.varianY.name.toString().trim();
		this.fieldVarianX = !this.varianX.name ? {alert:true, message:'field ini tidak boleh kosong'} : {alert:false, message:''};
		this.fieldVarianY = this.varianY.active && !this.varianY.name ? {alert:true, message:'field ini tidak boleh kosong'} : {alert:false, message:''};
	
		if(!this.varianX.name || (this.varianY.active && !this.varianY.name)){
		  
		  isFormValid = this.scrollView('varian', 'varianAlert', scroll);
		} else {
		  this.varianAlert = false;
		}

		const reg = new RegExp('^[0-9]+$');

		if(this.varianX.active && reg.test(this.varianX.name)){
			this.fieldVarianX = this.fieldVarianX.alert ? this.fieldVarianX : {alert:true, message:'Tipe varian tidak boleh berupa angka'};
			
			isFormValid = this.scrollView('varian', 'varianAlert', scroll);
			} else {
			this.varianAlert = false;
		  }
	  
		  if(this.varianY.active && reg.test(this.varianY.name)){
			this.fieldVarianY = this.fieldVarianY.alert ? this.fieldVarianY : {alert:true, message:'Tipe varian tidak boleh berupa angka'};
			
			isFormValid = this.scrollView('varian', 'varianAlert', scroll);
			} else {
			this.varianAlert = false;
		  }
	
		if(this.varianY.active && (this.varianX.name.toLowerCase() == this.varianY.name.toLowerCase())){
		  this.fieldVarianX = this.fieldVarianX.alert ? this.fieldVarianX : {alert:true, message:'Tipe varian tidak boleh memiliki value yang sama'};
		  this.fieldVarianY = this.fieldVarianY.alert ? this.fieldVarianY : {alert:true, message:'Tipe varian tidak boleh memiliki value yang sama'};
		  
		  isFormValid = this.scrollView('varian', 'varianAlert', scroll);
		  } else {
		  this.varianAlert = false;
		}
	
		if(this.varianX.active){
		  this.varianTypeX.forEach((el ,idx) => {
			this.varianTypeX[idx] = el.toString().trim();
			if(this.varianTypeX[idx]){
			  this.varianAlert = false;
			} else {
			  this.fieldVarianTypeX[idx] = {
				alert : true,
				message : '*field ini tidak boleh kosong'
			  }
			  isFormValid = this.scrollView('varian', 'varianAlert', scroll);
			}
		  });
	
		  let foundEqual = false;
	
		  for(let i = 0; i < this.varianTypeX.length; i++){
			let find = false;
			for(let j = 0; j < this.varianTypeX.length; j++){
			  if(i != j){
				if(this.varianTypeX[i].toLowerCase() == this.varianTypeX[j].toLowerCase()){
				  find = true;
				  if(!this.fieldVarianTypeX[j] || this.fieldVarianTypeX[j].alert == false){
					this.fieldVarianTypeX[j] = 
					{
					  alert : true,
					  message : '*field tidak boleh memiliki nilai sama'
					}
				  // break;
				  }
				}
			  }
	
			  if(find){
				foundEqual = true;
			  // break;
			  }
	
			}
		  }
	
			if(foundEqual){
			  isFormValid = this.scrollView('varian', 'varianAlert', scroll);
			} else {
			  this.varianAlert = false;
			}
		}
	
		if(this.varianY.active){
		  this.varianTypeY.forEach((el,idx) => {
			this.varianTypeY[idx] = el.toString().trim();
			if(this.varianTypeY[idx]){
			  this.varianAlert = false;
			} else {
			  this.fieldVarianTypeY[idx] = {
				alert : true,
				message : '*field ini tidak boleh kosong'
			  }
			  isFormValid = this.scrollView('varian', 'varianAlert', scroll);
			}
		  })
	
		  let foundEqual = false;
	
		  for(let i = 0; i < this.varianTypeY.length; i++){
			let find = false;
			for(let j = 0; j < this.varianTypeY.length; j++){
			  if(i != j){
				if(this.varianTypeY[i].toLowerCase() == this.varianTypeY[j].toLowerCase()){
				  find = true;
				  if(!this.fieldVarianTypeY[j] || this.fieldVarianTypeY[j].alert == false){
					this.fieldVarianTypeY[j] = 
					{
					  alert : true,
					  message : '*field tidak boleh memiliki nilai sama'
					}
				  // break;
				  }
				}
			  }
			}
	
			if(find){
			  foundEqual = true;
			  // break;
			}
	
		  }
	
		  if(foundEqual){
			isFormValid = this.scrollView('varian', 'varianAlert', scroll);
		  } else {
			this.varianAlert = false;
		  }
	
		}
		
		const allSkuCode : Array<string> = [];
	
		for(let i = 0; i < this.multiVarian.length; i++){
		  for(let x = 0; x < this.multiVarian[i].length; x++){
			if(!this.multiVarian[i][x].sku_code) {
			  isFormValid = this.scrollView('varian', 'varianAlert', scroll);
			  
			} else if(!this.multiVarian[i][x].sku_price) {
			  isFormValid = this.scrollView('varian', 'varianAlert', scroll);
			} else if(!this.multiVarian[i][x].sku_value) {
			  isFormValid = this.scrollView('varian', 'varianAlert', scroll);
			} else if(!this.multiVarian[i][x].status) {
			  isFormValid = this.scrollView('varian', 'varianAlert', scroll);
			} else if(this.multiVarian[i][x].qty < 0) {
			  isFormValid = this.scrollView('varian', 'varianAlert', scroll);
			} else {
			  this.varianAlert = false;
			  allSkuCode.push(this.multiVarian[i][x].sku_code);
			}
		  }
		}
	
		if(isFormValid && allSkuCode.length > 0){
		  let foundEqual = false;
		  for(let i = 0; i < allSkuCode.length; i++){
			for(let j = 0; j < allSkuCode.length; j++){
			  if(i != j){
				if(allSkuCode[j].toLowerCase() == allSkuCode[i].toLowerCase()){
				  foundEqual = true;
				}
			  }
			  if(foundEqual) break;
			}
			if(foundEqual) break;
		  }
		  if(foundEqual) {
			isFormValid = this.scrollView('varian', 'varianAlert', scroll);
			this.varianAlertMessage = '*terdapat SKU Code yang sama'
		  }
		}
	
		if(valReturn) return isFormValid;
	
	  }

	
	  deleteVariationType(indexValue) {
		this.varianHeader.splice(indexValue,1);
		this.dataVarian.splice(indexValue, 1);
		this.dataVarianModel.splice(indexValue, 1);
	  }
	
	  addVariationValue(index) {
		this.dataVarian[index].push('');
	
		this.dataVarianModel[index].push('');
	  }
	
	  deleteVariationValue(index, indexValue) {
		this.dataVarian[index].splice(indexValue, 1);
	
		this.dataVarianModel[index].splice(indexValue, 1);
	  }



	genVoucherToggler(val) {
		this.generateVoucherToggler = val;
	}
	loadImageToForm() {
		this.uploads = {
			image1: '',
			image2: '',
			image3: '',
			image4: '',
			image5: '',
		};
		if (this.form.images_gallery) {
			this.form.images_gallery.forEach((element, i) => {

				let img = this.gallery_label[i]
				// console.log('element',)
				this.uploads[img] = element.base_url + element.pic_big_path;

			});
		}
		// if (this.form.images_gallery == undefined) {
		// 	this.uploads['image3'] = this.form.base_url + this.form.pic_big_path
		// 	let temp = {
		// 		base_url: this.form.base_url,
		// 		pic_big_path: this.form.pic_big_path,
		// 		pic_small_path: this.form.pic_small_path,
		// 		pic_medium_path: this.form.pic_medium_path,
		// 		pic_file_name: this.form.pic_file_name,
		// 	};
		// 	this.temp_gallery.push(temp)
		// }
	}
	async afterProductLoaded() {

		// let mCategories = await this.categoryService.getCategoryList();
		// if (mCategories.result) {
		// 	console.log("hasil", mCategories)
		// 	mCategories.result.deals.forEach((element: any) => {
		// 	  element.value = false
		// 	  // console.log(element);
		// 	});
		// 	this.multipleCategoriesDeals = mCategories.result.deals;

		// 	console.log("hasil", mCategories)
		// 	mCategories.result.product.forEach((element: any) => {
		// 	  element.value = false
		// 	  // console.log(element);
		// 	});
		// 	this.multipleCategoriesProduct = mCategories.result.product;

		// }
		// if (this.form.type == 'product' || this.form.type == 'voucher') {
		// 	let mCategories = await this.categoryService.getCategoryList();
		// 	if (mCategories.result) {
		// 		for (let mC = 0; mC < mCategories.result.product.length; mC++) {
		// 			let element = mCategories.result.product[mC];

		// 			if (this.form.category && isArray(this.form.category)) {

		// 				let found = 0;
		// 				for (let nC = 0; nC < this.form.category.length; nC++) {
		// 					let el = this.form.category[nC];

		// 					if (el == element.code) {
		// 						found = 1;
		// 						break;
		// 					}
		// 				}
		// 				if (found == 1) {
		// 					element.value = true;
		// 					continue;
		// 				}
		// 			}

		// 			element.value = false;
		// 		}

		// 		this.multipleCategories = mCategories.result.product;

		// 	}
		// }
		// if (this.form.type == 'evoucher') {
		// 	let mCategories = await this.categoryService.getCategoryList();
		// 	if (mCategories.result) {
		// 		for (let mC = 0; mC < mCategories.result.deals.length; mC++) {
		// 			let element = mCategories.result.deals[mC];

		// 			if (this.form.category && isArray(this.form.category)) {

		// 				let found = 0;
		// 				for (let nC = 0; nC < this.form.category.length; nC++) {
		// 					let el = this.form.category[nC];

		// 					if (el == element.code) {
		// 						found = 1;
		// 						break;
		// 					}
		// 				}
		// 				if (found == 1) {
		// 					element.value = true;

		// 					continue;
		// 				}
		// 			}

		// 			element.value = false;
		// 		}
		// 		this.multipleCategories = mCategories.result.deals;

		// 	}
		// }


		// let params = {
		// 	type: 'merchant'
		// };
		// let mUsername = await this.merchantService.getMerchant(params);

		// mUsername.result.values.forEach((el, i) => {
		// 	this.listOfMerchant.push({ name: el.merchant_username });
		// });

		try {
			this.formPriceReplacer(null, 'product_price');
			this.formPriceReplacer(null, 'product_value');
			this.formPriceReplacer(null, 'weight');
	  
			this.categoryList = await this.productService.getCategoryLint();
	  
			this.categoryList.forEach(element => {
			  if(element.type == "product") {
				this.categoryProduct = element.category_list;
			  }
			  if(element.type == "voucher") {
				this.categoryVoucher = element.category_list;
			  }
			  if(element.type == "e-voucher") {
				this.categoryEvoucher = element.category_list;
			  }
				if(element.type == "gold") {
					this.categoryGold = element.category_list;
				}
				if(element.type == "top-up") {
					this.categoryEwallet = element.category_list;
				}
			});
		} catch (error) {
		console.log("error", error);
		}
	}
	addHashtags(newWord) {
		this.form.hashtags.push(newWord);
	}
	addHashasdtags(newWord) {
		this.form.hashtags.push(newWord);
	}
	hashtagsKeyDown(event) {
		let n = event.target.textContent;

		let key = event.key.toLowerCase();
		// console.log("key", key)
		if (key == ' ' || key == 'enter') {
			if (n.trim() !== '') {
				this.addHashtags(n);
			}
			event.target.textContent = '';
			event.preventDefault();
		}

		if (key == 'backspace' && n == '' && this.form.hashtags.length > 0) {
			this.form.hashtags.pop();
		}
	}

	productNameInputed($event) {
		if (this.autoSKU == true) {
			let s = this.form.product_name.toLowerCase().replace(/[\s\?\*]/g, '-');
			this.form.product_code = s;
		}
	}

	async deleteThis() {
		let r = await this.productService.deleteProduct(this.form.product_code);
		if (r.status && this.currentPermission == 'admin') {
			alert('Your Product ' + this.form.product_name + ' Has been deleted');
			this.router.navigate(['/administrator/productadmin']);
			// window.location.reload();
		} else if (this.currentPermission == 'merchant') {
			alert('Your Product' + this.form.product_name + ' Has been deleted')
			this.router.navigate(['/merchant-portal/product-list']);
		}
	}

	toggleDeleteConfirm(l: boolean) {
		this.toggleDelete = l;
	}

	formPriceReplacer($event, label: string) {
		if (this.form[label]) {
			if (typeof this.form[label] == 'number') this.form[label] = this.form[label].toString();

			let s = this.form[label].toLowerCase().replace(/[^0-9]/g, '');
			let c = parseInt(s);
			if (isNaN(c)) {
				this.form[label] = 0;
			} else {
				this.form[label] = c.toLocaleString('en');
			}
		}
	}

	async onFileSelected2(event, img) {
		var reader = new FileReader()
		try {
			this.errorFile = false;
			let fileMaxSize = 3000000; // let say 3Mb
			Array.from(event.target.files).forEach((file: any) => {
				if (file.size > fileMaxSize) {
					this.errorFile = 'Maximum File Upload is 3MB';
				}
			});
			this.selectedFile = event.target.files[0];

			reader.onload = (event: any) => {
				this.selFile = event.target.result;
			}

			if (event.target.files[0]) {
				reader.readAsDataURL(event.target.files[0]);
			}
			if (this.selectedFile) {
				switch (img) {
					case 'image1':

						this.showLoading = true;
						break;
					case 'image2':

						this.showLoading2 = true;
						break;
					case 'image3':

						this.showLoading3 = true;
						break;
					case 'image4':

						this.showLoading4 = true;
						break;
					case 'image5':

						this.showLoading5 = true;
						break;

				}

				const logo = await this.productService.upload(this.selectedFile, this, 'image', (result) => {
					this.callImage(result, img);
				});
			}

		} catch (e) {
			this.errorLabel = (<Error>e).message; //conversion to Error type
		}
	}
	callImage(result, img) {

		if (img == 'image3') {
			let product = this.form
			this.form = { ...product, ...result }
		}
		// console.log('result upload logo ', result,img);
		this.uploads[img] = result.base_url + result.pic_big_path;
		// this.temp_gallery.push(result)
		let a = this.gallery_label.indexOf(img)
		this.temp_gallery[a] = result;
		// console.log('This detail', this.temp_gallery);
		this.allfalse();

	}

	deleteImage(img) {
		Swal.fire({
			title: 'Are you sure?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!'
		}).then((result) => {
			if (result.isConfirmed) {
				let a = this.gallery_label.indexOf(img)
				this.uploads[img] = ''
				this.temp_gallery.splice(a, 1)
				Swal.fire(
					'Deleted!',
					'Your file has been deleted.',
					'success'
				)
				this.loadImageToForm();
			}
		})

	}
	allfalse() {
		this.showLoading = this.showLoading2 = this.showLoading3
			= this.showLoading4 = this.showLoading5 = false
	}

	onImageSelected(event) {
		this.isFileUploaded = false;

		this.errorLabel = false;
		let fileMaxSize = 3000000; // let say 3Mb
		var reader = new FileReader();

		reader.onload = (event: any) => {
			this.selFile = event.target.result;
		};

		reader.readAsDataURL(event.target.files[0]);

		Array.from(event.target.files).forEach((file: any) => {
			if (file.size > fileMaxSize) {
				this.errorLabel = 'maximum file is 3Mb';
			}
		});
		// console.log(event.target.files, this.selFile);
		if (this.errorLabel == false) {
			this.selectedFile = event.target.files[0];

			setTimeout(() => {
				this.onUpload();
			}, 1500);
		}
	}

	onFileSelected(event) {
		this.isFileUploaded = false;

		this.errorLabel = false;
		let fileMaxSize = 3000000; // let say 3Mb
		var reader = new FileReader();

		reader.onload = (event: any) => {
			this.selFile = event.target.result;
		};

		reader.readAsDataURL(event.target.files[0]);

		Array.from(event.target.files).forEach((file: any) => {
			if (file.size > fileMaxSize) {
				this.errorLabel = 'maximum file is 3Mb';
			}
		});
		// console.log(event.target.files, this.selFile);
		if (this.errorLabel == false) {
			this.selectedFile = event.target.files[0];

			// setTimeout(() => {
			//   this.uploadFile();
			// }, 1500)
		}
	}

	// onFileSelected(event){
	//   this.errorFile = false;
	//   let fileMaxSize = 3000000;// let say 3Mb
	//   Array.from(event.target.files).forEach((file: any) => {
	//       if(file.size > fileMaxSize){
	//         this.errorFile="maximum file is 3Mb";
	//       }
	//   });
	//   this.selectedFile = event.target.files[0];

	// }

	async onUpload() {
		try {
			this.cancel = false;
			let result;
			if (this.selectedFile) {
				result = await this.productService.upload(this.selectedFile, this, 'image', (result) => {
					this.callAfterUpload(result);
				});
			}
		} catch (e) {
			this.errorLabel = (<Error>e).message; //conversion to Error type
		}
		//   this.onUpload.
		return false;
	}

	async generateVoucher() {
		try {
			if (this.genVoucher.expiry_date && this.genVoucher.expiry_date.year) {
				let params = {
					expiry_date:
						this.genVoucher.expiry_date.year +
						'/' +
						this.genVoucher.expiry_date.month +
						'/' +
						this.genVoucher.expiry_date.day,
					qty: this.genVoucher.qty,
					product_code: this.form.product_code,
					merchant_username: this.form.merchant_username
				};
				await this.eVoucherService.generateEVoucherLint(params).then((result) => {
					if (result.result && result.result.status == 'success') {
						if (this.currentPermission == 'admin') {
							alert("Voucher has been generated!");
							this.router.navigate(['administrator/evoucheradmin']);
						} else if (this.currentPermission == 'merchant') {
							this.router.navigate(['merchant-portal/product-list/generated-voucher/list'], {
								queryParams: { id: this.productID }
							});
						}
					}
				});
			}
		} catch (e) {
			this.errorLabel = (<Error>e).message; //conversion to Error type
			console.log('ERROR', this.errorLabel);
		}
	}

	generatedVoucher() {
		if (this.currentPermission == 'admin') {
			this.router.navigate(['administrator/evoucheradmin'], { queryParams: { id: this.form.product_code } });
		} else if (this.currentPermission == 'merchant') {
			this.router.navigate(['merchant-portal/product-list/generated-voucher/list'], {
				queryParams: { id: this.form.product_code }
			});
		}
	}

	callAfterUpload(result) {
		if (result) {
			let product = this.form;
			this.form = {
				...product,
				...result
			};
			let temp = []
			temp.push(result)
			this.temp_gallery = temp
			this.isFileUploaded = true;
		}
	}

	stringToNumber(str: any) {
		if (typeof str == 'number') {
			str = str + '';
		}

		let s = str.toLowerCase().replace(/[^0-9]/g, '');

		return parseInt(s);
	}

	parseStringToArray(str: any) {
		let splitR;
		if (!isString(str)) return [];
		else {
			splitR = str.split(/[\,\.]/);
		}
		return splitR;
	}
	
	async saveThis() {
		try {
			if (this.currentPermission == 'merchant' || this.currentPermission == 'admin') {
				// delete this.form.merchant_username;
				delete this.form.merchant_group;
				// this.form.tnc = "no tnc"
			}

			if (this.form.type == 'product' || this.form.type == 'voucher' || this.form.type == 'gold' || this.form.type == 'top-up') {
				delete this.form.location_for_redeem;
				delete this.form.voucher_group;
			}
			
			
			
			// this.validateForm(this.form);
			let data = this.form;
			
			let isFormValid:boolean = true;
			
			if(this.form.type != "e-voucher" && this.form.type != "top-up" && (this.form.dimensions.height <= 0 || this.form.dimensions.height == "" || this.form.dimensions.height == "0")) {
				isFormValid = this.scrollView('weight', 'heightAlert');
			} else {
				this.heightAlert = false;
			}				
			
			if(this.form.type != "e-voucher" && this.form.type != "top-up" && (this.form.dimensions.length <= 0 || this.form.dimensions.length == "" || this.form.dimensions.length == "0")) {
				isFormValid = this.scrollView('weight', 'lengthAlert');
			} else {
				this.lengthAlert = false;
			}
			
			if(this.form.type != "e-voucher" && this.form.type != "top-up" && (this.form.dimensions.width <= 0 || this.form.dimensions.width == "" || this.form.dimensions.width == "0")) {
				isFormValid = this.scrollView('weight', 'widthAlert');
			} else {
				this.widthAlert = false;
			}
			
			if(this.form.type != "e-voucher" && this.form.type != "top-up" && (this.form.weight <= 0 || this.form.weight == "" || this.form.weight == "0")) {
				isFormValid = this.scrollView('weight', 'weightAlert');
			} else {
				this.weightAlert = false;
			}
			
			if(this.multiVarian && this.multiVarian.length > 0) {
				isFormValid = this.validateMultiVarian(true);
			}
		
			if((this.multiVarian && this.multiVarian.length == 0) && (!this.form.product_price || this.form.product_price <= 0 || this.form.product_price == "" || this.form.product_price == "0")) {
				isFormValid = this.scrollView('price', 'productPriceAlert');
			} else {
				this.productPriceAlert = false;
			}
		
			if(this.form.type == "e-voucher" && this.form.tnc == "") {
				isFormValid = this.scrollView('tnc', 'tncAlert');
			} else {
				this.tncAlert = false;
			}
		
			if(this.form.description == "") {
				this.form.description = this.form.product_name;
				// isFormValid = this.scrollView('description', 'descriptionAlert');
			} else {
				this.descriptionAlert = false;
			}
		
			if(this.form.product_code == "") {
				isFormValid = this.scrollView('product-information', 'productCodeAlert');
			} else {
				this.productCodeAlert = false;
			}
		
			if(this.form.product_name == "") {
				isFormValid = this.scrollView('product-information', 'productNameAlert');
			} else {
			  this.productNameAlert = false;
			}
		
			if(this.temp_gallery.length <= 0) {
				isFormValid = this.scrollView('photo-alert', 'photoAlert');
			} else {
			  this.photoAlert = false;
			}



			if(isFormValid == true) {
				// this.productSku.forEach((el) => {
				// 	el.sku_price = this.stringToNumber(el.sku_price);
				// 	el.sku_value = this.stringToNumber(el.sku_value);
				// });

				data.voucher_group = this.form.voucher_group;
				// data.price = this.stringToNumber(data.price);
				// data.fixed_price = this.stringToNumber(data.fixed_price);
				// data.category = this.parseStringToArray(data.category);
				// data.min_order = parseInt(data.min_order);
				// data.pic_file_name = String(data.pic_file_name);
				if(data.product_price) data.product_price = this.stringToNumber(data.product_price);
				if(data.product_value) data.product_value = this.stringToNumber(data.product_value);
				data.weight = this.stringToNumber(data.weight);
				// data.weight = data.weightInGram / 1000;
				data.images_gallery = this.temp_gallery;
				data.dimensions = {
					length: data.type == "e-voucher" ? 0 : parseFloat(data.dimensions.length),
					width: data.type == "e-voucher" ? 0 : parseFloat(data.dimensions.width),
					height: data.type == "e-voucher" ? 0 : parseFloat(data.dimensions.height)
				};

				if (data.currentPermission == 'admin' && this.form.merchant_username) {
					data.merchant_username = this.form.merchant_username;
				}

				data.qty = data.type == "e-voucher" ? 0 : parseInt(data.qty);
				// data.category = [];
				data.weight = data.type == "e-voucher" ? 0 : data.weight;
				this.multipleCategories.forEach((el) => {
					if (el.value) data.category.push(el.code);
				});

				this.productSku.forEach((el) => {
					el.sku_price = this.stringToNumber(el.sku_price);
					el.sku_value = this.stringToNumber(el.sku_value);
				});


				let dataProduct:any;
				let varianList:any = {};

				for (let i = 0; i < this.varianHeader.length; i++) {
					varianList[this.varianHeader[i]] = this.dataVarianModel[i];
				}

				const allProductSKU :Array<any> = [];

				for(let i = 0; i < this.multiVarian.length; i++){
					for (let j = 0; j < this.multiVarian[i].length; j++){
						const tempProductSKU = this.multiVarian[i][j];
						delete tempProductSKU.isloading;
						tempProductSKU.variant = {
						[this.varianX.name]:this.varianTypeX[j] 
						};
						if(this.varianY.active) tempProductSKU.variant[this.varianY.name] = this.varianTypeY[i];
						allProductSKU.push(tempProductSKU);
					}
				}

				// if(data.type == 'product' || data.type == 'voucher') {
					dataProduct = {
						product_name:data.product_name,
						product_code:data.product_code,
						// product_price:data.product_price,
						// product_value:data.product_value,
						description:data.description,
						variation: varianList,
						type:data.type,
						category:data.category,
						status:data.status,
						weight:data.weight,
						dimensions:data.dimensions,
						images_gallery:data.images_gallery,
						qty:data.qty
						// product_sku: this.productSku,
					};
				// }

				if(this.varianX.active) {
					dataProduct.product_sku = allProductSKU;
					dataProduct.variation = {
					  [this.varianX.name] : this.varianTypeX
					};
					if(this.varianY.active) dataProduct.variation[this.varianY.name] = this.varianTypeY;
		  
				} else {
					dataProduct.variation = [];
					dataProduct.product_value = parseFloat(data.product_value);
					dataProduct.product_price = parseFloat(data.product_price);
					dataProduct.qty = data.qty;
				}				

				if(data.type == "e-voucher") {
					dataProduct.tnc = data.tnc;
				}


				// if(varianList && ((Object.keys(varianList).length === 0 && varianList.constructor === Object) || (Array.isArray(varianList) && varianList.length == 0))) {
				// 	dataProduct.product_value = data.product_value;
				// 	dataProduct.product_price = data.product_price;
				// 	dataProduct.qty = data.qty;
				// } else if(this.productSku && this.productSku.length > 0) {
				// 	dataProduct.product_sku = this.productSku;
				// }

				console.log("data product",dataProduct);

				await this.productService.updateProductData(dataProduct);
				this.errorLabel = false;
				Swal.fire("Product/Voucher has been updated!").then(() => {
					if (this.currentPermission == 'merchant') {
						this.router.navigate(['/merchant-portal/product-list/detail'], {
							queryParams: { id: this.productID }
						});
					}
					else if (this.currentPermission == 'admin' && data.type == "e-voucher") {
						this.router.navigate(['administrator/vouchers']);
					}
					else if (this.currentPermission == 'admin') {
						this.router.navigate(['administrator/productadmin']);
					}
				});
				// alert('Product/Voucher has been updated!');
				// if (this.currentPermission == 'merchant') {
				// 	this.router.navigate(['/merchant-portal/product-list/detail'], {
				// 		queryParams: { id: this.productID }
				// 	});
				// }
			}
		} catch (e) {
			this.errorLabel = (<Error>e).message; //conversion to Error type

			let message = this.errorLabel;
			if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
				message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
			}
			Swal.fire({
				icon: 'error',
				title: message,
			})
		}
	}

	validateForm(form) {
		for (var prop in form) {
			if (
				!form.hasOwnProperty(prop) ||
				prop == 'tnc' ||
				prop == 'category' ||
				prop == 'weight'
				|| prop == 'hashtags' || prop == 'images_gallery' || prop == 'description'
			)
				continue;

			if (prop == 'base_url') {
				if (!form[prop]) {
					throw new Error('Please upload new image for your product ');
				}
			}
			// console.log(prop, form[prop] == undefined, form[prop] == '')
			if (form[prop] == undefined) {
				throw new Error('you need to completing this ' + this.labels[prop]);
			}

			if (isString(form[prop]) && form[prop].trim() == '')
				throw new Error('you need to completing this input ' + this.labels[prop]);

			if (isArray(form[prop]) && form[prop].length == 0) {
				throw new Error('Hi, you need to completing this ' + this.labels[prop]);
			}
		}
	}

	if(listOfProductType: 'product') { }

	toggleCategories(index) {
		this.multipleCategories.forEach((el) => {
			el.value = false;
		});
		this.multipleCategories[index].value = true;
	}

	async uploadFile() {
		try {
			if (this.selectedFile) {
				//  let result =await this.productService.uploadFile(
				// 	this.selectedFile,
				// 	this,
				// 	'evoucher',
				// 	this.callAfterUpload
				// );
				const logo = await this.productService.uploadFile(this.selectedFile, this, 'evoucher', (result) => {
					this.callAfterUpload;
				});
				// console.log('resuuult',result)

			}
		} catch (e) {
			this.errorLabel = (<Error>e).message;
		}
		if (this.selectedFile) {
		}
	}

	// setEdit(){
	//   if(this.edit_status){
	//     this.edit_status = false
	//   }
	//   else{
	//     this.edit_status = true
	//   }
	// }

	async onFileVariant(event, image_gallery, index) {
			var reader = new FileReader()
			try {
				this.errorFile = false;
				let fileMaxSize = 3000000; // let say 3Mb
				Array.from(event.target.files).forEach((file: any) => {
					if (file.size > fileMaxSize) {
						this.errorFile = 'Maximum File Upload is 3MB';
					}
				});
				this.selectedFile = event.target.files[0];
	
				reader.onload = (event: any) => {
					this.selFile = event.target.result;
				}
	
				if(event.target.files[0]){
					reader.readAsDataURL(event.target.files[0]);
				  }
				if (this.selectedFile) {
					// switch (img) {
					// 	case 'image1':
							
					// 		this.showLoading = true;
					// 		console.log("ini1", img)
					// 		break;
					// 	case 'image2':
							
					// 		this.showLoading2 = true;
					// 		console.log("ini2", img)
					// 		break;
					// 	case 'image3':
							
					// 		this.showLoading3 = true;
					// 		console.log("ini3", img)
					// 		break;
					// 	case 'image4':
						
					// 		this.showLoading4 = true;
					// 		console.log("ini4", img)
					// 		break;
					// 	case 'image5':
							
					// 		this.showLoading5 = true;
					// 		console.log("ini5", img)
					// 		break;
	
					// }
	
			// let result = {
			//   "base_url" : "https://res.cloudinary.com/dqcj36zfd/image/upload/",
			//   "pic_file_name" : "v1615863583/admin/image/montiss-1.60501f1dce424.jpg",
			//   "pic_small_path" : "s--g8KpJ7Mu--/c_limit,q_auto:good,w_150/v1/admin/image/montiss-1.60501f1dce424.jpg",
			//   "pic_medium_path" : "s--dMwcc-P3--/c_limit,q_auto:good,w_600/v1/admin/image/montiss-1.60501f1dce424.jpg",
			//   "pic_big_path" : "v1615863583/admin/image/montiss-1.60501f1dce424.jpg"
			// }
	
			// image_gallery.push(result);
					this.isLoading[index] = true;
							
					const logo = await this.productService.upload(this.selectedFile, this, 'image', (result) => {
						image_gallery.push(result);
						this.isLoading[index] = false;
					});
				}
				
			} catch (e) {
		  console.log("error upload", e);
				this.errorLabel = (<Error>e).message; //conversion to Error type
			}
	  }
	
	  deleteVarianImage(image_gallery) {
			Swal.fire({
				title: 'Are you sure?',
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, delete it!'
			}).then((result) => {
				if (result.isConfirmed) {
			image_gallery.length = 0;
					Swal.fire(
						'Deleted!',
						'Your file has been deleted.',
						'success'
					)
					// this.loadImageToForm();
				}
			})
	
		}
	backClicked() {
		this._location.back();
	}
}
