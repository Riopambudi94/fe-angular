// Dear Future Developer, please consider leaving notes on your code, good habits will reduce development time ~ H

import { Component, Input, OnInit, TestabilityRegistry } from '@angular/core';
import { formatCurrency } from '@angular/common';
import { ProductService } from '../../../../../../services/product/product.service';
import { isArray } from 'ngx-bootstrap/chronos';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { CategoryService } from '../../../../../../services/category/category.service';
import { MerchantService } from '../../../../../../services/merchant/merchant.service';
import { Router } from '@angular/router';
import { PermissionObserver } from '../../../../../../services/observerable/permission-observer';
import { isString } from '../../../../../../object-interface/common.object';
import Swal from 'sweetalert2';
import {Location} from '@angular/common';
// import { throwMatDialogContentAlreadyAttachedError } from '@angular/material';
// import { SSL_OP_SSLEAY_080_CLIENT_DH_BUG } from 'constants';

@Component({
  selector: 'app-merchant-add-new-product',
  templateUrl: './merchant-add-new-product.component.html',
  styleUrls: ['./merchant-add-new-product.component.scss'],
})

export class MerchantAddNewProductComponent implements OnInit {
  
  @Input()
  public type: string;
  arrayVariationValues: any = [];

  currentX:number = 0;
  currentY:number = 0;
  
  Editor = ClassicEditor;
  config = {
    toolbar: [ 'heading', '|', 'bold', 'italic', 'bulletedList', 'numberedList', 'blockQuote' ],
    heading: {
        options: [
            { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
            { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
            { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' }
        ]
    }
}
  newData: any;
  autoSKU = false;
  hashtagsKey = "";
  listOfProductType = [
    { name: 'Product', value: 'product', active: true },
    { name: 'Gold', value: 'gold', active: false },
    { name: 'Voucher Fisik', value: 'voucher', active: false },
    { name: 'E-Voucher', value: 'e-voucher', active: false },
    { name: 'E-Wallet', value: 'top-up', active: false },
  ]
  listOfRedeemType = [
    { name: 'voucher_link', active: true },
    { name: 'voucher_code', active: false }
  ]
  gallery_label: any = ['image3', 'image1', 'image2', 'image4', 'image5'];
  listOfVendorType = [];
  listOfMerchant = [];
  
  errorFile;
  form: any = {
    min_order: 1,
    product_name: '',
    product_code: '',
    product_price:0,
    product_value:0,
    description: '',
    variation: {},
    type: 'product',
    category: '',
    status:'ACTIVE',
    weight: 1, // in Kg,
    merchant_username: '',
    voucher_group: '',
    redeem_type: '',
    price: 0,
    fixed_price: 0,
    qty: 1,
    dimensions: {
      width: 1,
      length: 1,
      height: 1
    },
    images_gallery: [],
    product_sku:[],
    
    tnc: '',
    condition: '',
    active: 0,
    need_outlet_code: 0,
    hashtags: [],
    applicable_for_ios: true,
    weightInGram: 1000,
  }
  title= 'Product';
  uploads: any = {
		image1: '',
		image2: '',
		image3: '',
		image4: '',
		image5: '',
	};
  labels = {
    type: 'product type',
    min_order: 'minimum order',
    product_code: 'product code',
    product_name: 'product name',
    merchant_username: 'merchant username',
    category: 'category',
    price: 'price',
    fixed_price: 'public price',
    qty: 'quantity',
    description: 'description',
    tnc: 'term and conditions',
    dimensions: 'dimensions',
    weight: 'weight'
  }
  temp_gallery : any = [];
  progressBar;
  errorLabel: any = false;
  errorMessage  : any = false;
  selFile;
  isFileUploaded = false;
  cancel;
  selectedFile: any;
  multipleCategories = [];
  result_id: [];
  data1:any ={}
  data2:any ={}
  data3:any ={}
  data4:any ={}
  data5:any ={}
  currentPermission;
  multipleCategoriesDeals: any;
  multipleCategoriesProduct: any;
  showLoading: boolean = false;
	showLoading2: boolean = false;
	showLoading3: boolean = false;
	showLoading4: boolean = false;
	showLoading5: boolean = false;

  varianHeader: any = [];

  dataVarian: any = [];

  dataVarianModel:any = [];

  isLoading : any = [];

  isloading : any = [];

  categoryList:any = [];
  categoryProduct:any;
  categoryVoucher:any;
  categoryEvoucher:any;
  categoryGold:any;
  categoryEwallet:any;

  headerValue:any = [
    'SKU Code',
    'SKU Price',
    'SKU Value',
    'Stok',
    'Gambar',
    'Status',
  ];

  valueVariation:any = [
    ''
  ];

  productSku:any = [];


  public productStatus: any = [
		{label: 'ACTIVE', value: 'ACTIVE', selected: 1},
		{label: 'INACTIVE', value: 'INACTIVE'}
	];

  categoryAlert : boolean = false;


  photoAlert: boolean = false;
  productNameAlert: boolean = false;
  productCodeAlert: boolean = false;
  descriptionAlert: boolean = false;
  tncAlert: boolean = false;
  productPriceAlert: boolean = false;
  productValueAlert: boolean = false;

  weightAlert: boolean = false;
  widthAlert: boolean = false;
  lengthAlert: boolean = false;
  heightAlert: boolean = false;

  varianAlert: boolean = false;
  varianAlertMessage: string = '';

  
  varianX = {
    name : '',
    active : false
  };

  varianY = {
    name : '',
    active : false
  };

  varianTypeX : Array<string> = [];
  varianTypeY : Array<string> = [];

  fieldVarianTypeX : Array<any> = [];
  fieldVarianTypeY : Array<any> = [];

  multiVarian : Array<Array<any>> = [];

  fieldVarianX : any = {
    alert : false,
    message : ""
  }
  fieldVarianY : any = {
    alert : false,
    message : ""
  }




  constructor(
    private productService: ProductService,
    private categoryService: CategoryService,
    private merchantService: MerchantService,
    private permission: PermissionObserver,
    private router: Router,
    private _location: Location
  ) {
    this.permission.currentPermission.subscribe((val) => {
      this.currentPermission = val;

      if (this.currentPermission == 'merchant') {
        this.listOfProductType = [];
        this.form.type = 'product';
      }

    })
  }

  clickVarian() {
    this.addVariationType();
    this.form.product_price = 0;
    this.form.product_value = 0;
  }

  changeText() {
    try {
      this.productSku = [];
      this.isLoading = [];

      for (let i = 0; i < this.dataVarianModel[0].length; i++) {
        if(this.dataVarianModel[1]) {
          for (let j = 0; j < this.dataVarianModel[1].length; j++) {
            this.productSku.push(
              {
                "variant":{
                  [this.varianHeader[0]]:this.dataVarianModel[0][i],
                  [this.varianHeader[1]]:this.dataVarianModel[1][j],
                },
                "image_gallery":[
                ],
              },
            );
            this.isLoading.push(false);
          }
        } else {
          this.productSku.push(
            {
              "variant":{
                [this.varianHeader[0]]:this.dataVarianModel[0][i],
              },
              "image_gallery":[
              ],
            },
          );
          this.isLoading.push(false);
        }
      }
    } catch (error) {
    }
  }

  addVariationType() {

    if(this.multiVarian.length < 1){
      this.varianX.active = true;
      this.addVarianTypeY('');
      this.addVarianTypeX('');
    }
  }

  activatedVarianY(){
    this.varianY.active = true;
    this.validateMultiVarian(false,false);
  }

  addVarianTypeX(fieldX: any = ''){
    this.varianTypeX.push(fieldX);
    for(let i = 0; i < this.varianTypeY.length; i++){
      this.multiVarian[i].push(this.generateProductSKU(this.varianTypeY[i],fieldX));
    }
    this.validateMultiVarian(false,false);
  }

  deleteVarianTypeX(index : number){
    for(let i = 0; i < this.varianTypeY.length; i++){
      this.multiVarian[i].splice(index,1);
    }
    this.varianTypeX.splice(index,1);
    this.validateMultiVarian(false,false);
  }

  addVarianTypeY(fieldY: any = ''){
    this.varianTypeY.push(fieldY);
    let arrayY : any = [];
    for(let i = 0; i < this.varianTypeX.length; i++){
      arrayY.push(this.generateProductSKU(fieldY,this.varianTypeX[i]));
    }
    this.multiVarian.push(arrayY);
    this.validateMultiVarian(false,false);
  }

  deleteVarianTypeY(index : number){
    this.multiVarian.splice(index,1);
    this.varianTypeY.splice(index,1);
    this.validateMultiVarian(false,false);
  }

  deleteAllTypeY(){
    if(this.varianX.active){
      this.multiVarian.splice(1,this.multiVarian.length - 1);
      this.varianTypeY = [''];
      this.varianY.active = false;
      this.varianY.name = '';
      this.fieldVarianX = {
        alert : false,
        message : ''
      };
      this.fieldVarianY = {
        alert : false,
        message : ''
      }
      this.validateMultiVarian(false,false);
    } else {
      this.deleteAllVarian();
    }
  }

  deleteAllTypeX(){
    if(this.varianY.active){
      const tempX : Array<any> = []; 
      for(let i = 0; i < this.multiVarian.length; i++){
        tempX.push(this.multiVarian[i][0]);
      }
      this.multiVarian = [tempX];
      this.varianTypeX = [...this.varianTypeY];
      this.varianX.name = this.varianY.name;
      this.varianTypeY = [''];
      this.varianY.active = false;
      this.varianY.name = '';
      this.fieldVarianX = {
        alert : false,
        message : ''
      };
      this.fieldVarianY = {
        alert : false,
        message : ''
      }
    }else {
      this.deleteAllVarian();
    }
    this.validateMultiVarian(false,false);
  }

  deleteAllVarian(){
    this.varianX = {
      name : '',
      active : false
    };
  
    this.varianY = {
      name : '',
      active : false
    };
  
    this.varianTypeX = [];
    this.varianTypeY = [];
  
    this.multiVarian = [];
    this.fieldVarianX = {
      alert : false,
      message : ''
    };
    this.fieldVarianY = {
      alert : false,
      message : ''
    }
  }

  generateProductSKU(valueY : string = '', valueX: string = ''){
    return {
      sku_code : '',
      sku_price : '',
      sku_value : '',
      qty : 0,
      image_gallery : [],
      status : 'ACTIVE',
      isloading:false,

    }
  }


  

  addArrayValue(type :any, count:number){

    for(let x = 0; x < this.currentX; x++){
      for(let y = 0; y < this.currentY; y++){
        
      }
    }
  }

  deleteVariationType(indexValue) {
    this.dataVarian.splice(indexValue, 1);
    this.dataVarianModel.splice(indexValue, 1);
  }

  addVariationValue(index) {
    this.dataVarian[index].push('');
    this.dataVarianModel[index].push('');
  }

  deleteVariationValue(index, indexValue) {
    this.dataVarian[index].splice(indexValue, 1);
    this.dataVarianModel[index].splice(indexValue, 1);
  }

  async ngOnInit() {
   
    try {
      if(this.type == "e-voucher") {
        this.listOfProductType[2].active = true;
        this.setProductType(this.listOfProductType[2]);
      }
      
      this.formPriceReplacer(null, 'product_price');
      this.formPriceReplacer(null, 'weightInGram');

      this.categoryList = await this.productService.getCategoryLint();

      this.categoryList.forEach(element => {
        console.log("element",element);
        if(element.type == "product") {
          this.categoryProduct = element.category_list;
        }
        if(element.type == "voucher") {
          this.categoryVoucher = element.category_list;
        }
        if(element.type == "e-voucher") {
          this.categoryEvoucher = element.category_list;
        }
        if(element.type == "gold") {
          this.categoryGold = element.category_list;
        }
        if(element.type == "top-up") {
          this.categoryEwallet = element.category_list;
        }
      });
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type

      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
      }
    }
  }

  copyInputMessage(inputElement){
    inputElement.select();
    document.execCommand('copy');
    inputElement.setSelectionRange(0, 0);
  }

  productNameInputed($event) {
    if (this.autoSKU == true) {
      let s = this.form.product_name.toLowerCase().replace(/[\s\?\*]/g, '-');
      this.form.product_code = s;
    }
  }

  addHashtags(newWord) {

    this.form.hashtags.push(newWord);
  }

  hashtagsKeyDown(event) {
    let n = event.target.textContent

    let key = event.key.toLowerCase();
    // console.log("key", key)
    if (key == ' ' || key == 'enter') {

      if (n.trim() !== '') {
        this.addHashtags(n);
      }
      event.target.textContent = ''
      event.preventDefault();
    }

    if (key == 'backspace' && n == '' && this.form.hashtags.length > 0) {
      this.form.hashtags.pop();
    }

  }

  formPriceReplacer($event, label: string) {
    if (this.form[label]) {
      if (typeof this.form[label] == 'number')
        this.form[label] = this.form[label].toString()

      let s = this.form[label].toLowerCase().replace(/[^0-9]/g, '');
      let c = parseInt(s);
      if (isNaN(c)) {
        this.form[label] = 0;
      }
      else {
        this.form[label] = c.toLocaleString('en');
      }
    }
  }
  async onFileSelected2(event, img) {
		var reader = new FileReader()
		try {
			this.errorFile = false;
			let fileMaxSize = 2064000; // let say 2Mb
			Array.from(event.target.files).forEach((file: any) => {
				if (file.size > fileMaxSize) {
					this.errorFile = 'Maximum File Upload is 2MB';
				}
			});
			this.selectedFile = event.target.files[0];

			reader.onload = (event: any) => {
				this.selFile = event.target.result;
			}

			if(event.target.files[0]){
				reader.readAsDataURL(event.target.files[0]);
			  }
			if (this.selectedFile) {
				switch (img) {
					case 'image1':
						
						this.showLoading = true;
						break;
					case 'image2':
						
						this.showLoading2 = true;
						break;
					case 'image3':
						
						this.showLoading3 = true;
						break;
					case 'image4':
					
						this.showLoading4 = true;
						break;
					case 'image5':
						
						this.showLoading5 = true;
						break;

				}
				
				const logo = await this.productService.upload(this.selectedFile, this, 'image', (result) => {
					this.callImage(result, img);
				});
			}
			
		} catch (e) {
			this.errorLabel = (<Error>e).message; //conversion to Error type
		}
  }
  deleteImage(img) {
		Swal.fire({
			title: 'Are you sure?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!'
		}).then((result) => {
			if (result.isConfirmed) {
				let a = this.gallery_label.indexOf(img)
				this.uploads[img] = ''
				this.temp_gallery.splice(a, 1)
				Swal.fire(
					'Deleted!',
					'Your file has been deleted.',
					'success'
				)
			}
		})

	}

  callImage(result, img) {
    if( img == 'image3'){
      let product = this.form
      this.form = { ...product, ...result }
    }
    this.uploads[img] = result.base_url + result.pic_big_path;
      switch (img) {
        case "image1":
          this.data1 = result
          break;
        case "image2":
          this.data2 = result
          break;
        case "image3":
          this.data3 = result
          break;
        case "image4":
          this.data4 = result
          break;
        case "image5":
          this.data5 = result
          break;
      }
  
		
    this.allfalse();
		
  }

  async onFileVariant(event, image_gallery, index) {
		var reader = new FileReader()
		try {
			this.errorFile = false;
			let fileMaxSize = 2064000; // let say 2Mb
			Array.from(event.target.files).forEach((file: any) => {
				if (file.size > fileMaxSize) {
					this.errorFile = 'Maximum File Upload is 2MB';
				}
			});
			this.selectedFile = event.target.files[0];

			reader.onload = (event: any) => {
				this.selFile = event.target.result;
			}

			if(event.target.files[0]){
				reader.readAsDataURL(event.target.files[0]);
			  }
			if (this.selectedFile) {
        
        this.multiVarian[index[0]][index[1]].isloading = true;
				
				const logo = await this.productService.upload(this.selectedFile, this, 'image', (result) => {
          image_gallery.push(result);
          this.multiVarian[index[0]][index[1]].isloading = false;
				});

			}
			
		} catch (e) {
			this.errorLabel = (<Error>e).message; //conversion to Error type
		}
  }

  deleteVarianImage(image_gallery) {
		Swal.fire({
			title: 'Are you sure?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!'
		}).then((result) => {
			if (result.isConfirmed) {
        image_gallery.length = 0;
				Swal.fire(
					'Deleted!',
					'Your file has been deleted.',
					'success'
				)
			}
		})

	}



  allfalse() {
		this.showLoading = this.showLoading2 = this.showLoading3
      = this.showLoading4 = this.showLoading5 =false
  }
  onFileSelected(event) {
    this.isFileUploaded = false;

    this.errorLabel = false;
    let fileMaxSize = 3000000;// let say 3Mb
    var reader = new FileReader();

    reader.onload = (event: any) => {
      this.selFile = event.target.result;
    }

    reader.readAsDataURL(event.target.files[0]);

    Array.from(event.target.files).forEach((file: any) => {
      if (file.size > fileMaxSize) {
        this.errorLabel = "maximum file is 3Mb";
      }
    });
    if (this.errorLabel == false) {
      this.selectedFile = event.target.files[0];

      setTimeout(() => {
        this.onUpload();
      }, 1500)
    }

  }

  async onUpload() {
    try {
      this.cancel = false;
      let result
      if (this.selectedFile) {
        result = await this.productService.upload(this.selectedFile, this, 'image', (result) => { this.callAfterUpload(result) });
      }
    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type

      let message = this.errorLabel;
      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
      }
      Swal.fire({
        icon: 'error',
        title: message,
      });
    }
    return false;
  }

  callAfterUpload(result) {
    if (result) {
      let product = this.form
      this.form = { ...product, ...result }

      this.isFileUploaded = true;
    }
  }

  stringToNumber(str: any) {
    if (typeof str == 'number') {
      str = str + '';
    }

    let s = str.toLowerCase().replace(/[^0-9]/g, '');

    return parseInt(s);
  }


  parseStringToArray(str: any) {
    let splitR;
    if (!isString(str)) return [];
    else {
      splitR = str.split(/[\,\.]/);
    }
    return splitR;
  }

  parseStringToArray2(str: any) {
    let array = []
    array.push(str)
    return array
  }


  scrollView = (id : string, alert : string = '' , scroll : boolean = true, objectScroll : any = false) => {
    if(alert)this[alert] = true;
    const defObject = objectScroll ? objectScroll : {
      behavior: "smooth",
      block: "start",
      inline: "nearest"
    };
    if(scroll)document.getElementById(id).scrollIntoView(defObject);
    return false;
  }

  eventValidation = (event : any) => {
    this.validateMultiVarian(false,false);
  }

  validateMultiVarian = (valReturn : boolean = false, scroll : boolean = true) => {
    this.varianAlertMessage = '*data varian harus lengkap';
    let isFormValid : boolean = true;
    this.fieldVarianTypeX = [];
    this.fieldVarianTypeY = [];
    this.varianX.name = this.varianX.name.toString().trim();
    this.varianY.name = this.varianY.name.toString().trim();
    this.fieldVarianX = !this.varianX.name ? {alert:true, message:'field ini tidak boleh kosong'} : {alert:false, message:''};
    this.fieldVarianY = this.varianY.active && !this.varianY.name ? {alert:true, message:'field ini tidak boleh kosong'} : {alert:false, message:''};

    if(!this.varianX.name || (this.varianY.active && !this.varianY.name)){
      
      isFormValid = this.scrollView('varian', 'varianAlert', scroll);
    } else {
      this.varianAlert = false;
    }

		const reg = new RegExp('^[0-9]+$');

    if(this.varianX.active && reg.test(this.varianX.name)){
      this.fieldVarianX = this.fieldVarianX.alert ? this.fieldVarianX : {alert:true, message:'Tipe varian tidak boleh berupa angka'};
      isFormValid = this.scrollView('varian', 'varianAlert', scroll);
      } else {
      this.varianAlert = false;
    }

    if(this.varianY.active && reg.test(this.varianY.name)){
      this.fieldVarianY = this.fieldVarianY.alert ? this.fieldVarianY : {alert:true, message:'Tipe varian tidak boleh berupa angka'};
      isFormValid = this.scrollView('varian', 'varianAlert', scroll);
      } else {
      this.varianAlert = false;
    }

    if(this.varianY.active && (this.varianX.name.toLowerCase() == this.varianY.name.toLowerCase())){
      this.fieldVarianX = this.fieldVarianX.alert ? this.fieldVarianX : {alert:true, message:'Tipe varian tidak boleh memiliki value yang sama'};
      this.fieldVarianY = this.fieldVarianY.alert ? this.fieldVarianY : {alert:true, message:'Tipe varian tidak boleh memiliki value yang sama'};
      
      isFormValid = this.scrollView('varian', 'varianAlert', scroll);
      } else {
      this.varianAlert = false;
    }

    if(this.varianX.active){
      this.varianTypeX.forEach((el,idx) => {
        this.varianTypeX[idx] = el.toString().trim();
        if(this.varianTypeX[idx]){
          this.varianAlert = false;
        } else {
          this.fieldVarianTypeX[idx] = {
            alert : true,
            message : '*field ini tidak boleh kosong'
          }
          isFormValid = this.scrollView('varian', 'varianAlert', scroll);
        }
      });

      let foundEqual = false;

      for(let i = 0; i < this.varianTypeX.length; i++){
        let find = false;
        for(let j = 0; j < this.varianTypeX.length; j++){
          if(i != j){
            if(this.varianTypeX[i].toLowerCase() == this.varianTypeX[j].toLowerCase()){
              find = true;
              if(!this.fieldVarianTypeX[j] || this.fieldVarianTypeX[j].alert == false){
                this.fieldVarianTypeX[j] = 
                {
                  alert : true,
                  message : '*field tidak boleh memiliki nilai sama'
                }
              // break;
              }
            }
          }

          if(find){
            foundEqual = true;
          // break;
          }

        }
      }

        if(foundEqual){
          isFormValid = this.scrollView('varian', 'varianAlert', scroll);
        } else {
          this.varianAlert = false;
        }
    }

    if(this.varianY.active){
      this.varianTypeY.forEach((el,idx) => {
        console.log("element Y", el);
        this.varianTypeY[idx] = el.toString().trim();
        if(this.varianTypeY[idx]){
          this.varianAlert = false;
        } else {
          this.fieldVarianTypeY[idx] = {
            alert : true,
            message : '*field ini tidak boleh kosong'
          }
          isFormValid = this.scrollView('varian', 'varianAlert', scroll);
        }
      })

      let foundEqual = false;

      for(let i = 0; i < this.varianTypeY.length; i++){
        let find = false;
        for(let j = 0; j < this.varianTypeY.length; j++){
          if(i != j){
            if(this.varianTypeY[i].toLowerCase() == this.varianTypeY[j].toLowerCase()){
              find = true;
              if(!this.fieldVarianTypeY[j] || this.fieldVarianTypeY[j].alert == false){
                this.fieldVarianTypeY[j] = 
                {
                  alert : true,
                  message : '*field tidak boleh memiliki nilai sama'
                }
              // break;
              }
            }
          }
        }

        if(find){
          foundEqual = true;
          // break;
        }

      }

      if(foundEqual){
        isFormValid = this.scrollView('varian', 'varianAlert', scroll);
      } else {
        this.varianAlert = false;
      }

    }
    
    const allSkuCode : Array<string> = [];

    for(let i = 0; i < this.multiVarian.length; i++){
      for(let x = 0; x < this.multiVarian[i].length; x++){
        if(!this.multiVarian[i][x].sku_code) {
          isFormValid = this.scrollView('varian', 'varianAlert', scroll);
          
        } else if(!this.multiVarian[i][x].sku_price) {
          isFormValid = this.scrollView('varian', 'varianAlert', scroll);
        } else if(!this.multiVarian[i][x].sku_value) {
          isFormValid = this.scrollView('varian', 'varianAlert', scroll);
        } else if(!this.multiVarian[i][x].status) {
          isFormValid = this.scrollView('varian', 'varianAlert', scroll);
        } else if(this.multiVarian[i][x].qty < 0) {
          isFormValid = this.scrollView('varian', 'varianAlert', scroll);
        } else {
          this.varianAlert = false;
          allSkuCode.push(this.multiVarian[i][x].sku_code);
        }
      }
    }

    if(isFormValid && allSkuCode.length > 0){
      let foundEqual = false;
      for(let i = 0; i < allSkuCode.length; i++){
        for(let j = 0; j < allSkuCode.length; j++){
          if(i != j){
            if(allSkuCode[j].toLowerCase() == allSkuCode[i].toLowerCase()){
              foundEqual = true;
            }
          }
          if(foundEqual) break;
        }
        if(foundEqual) break;
      }
      if(foundEqual) {
        isFormValid = this.scrollView('varian', 'varianAlert', scroll);
        this.varianAlertMessage = '*terdapat SKU Code yang sama'
      }
    }

    if(valReturn) return isFormValid;

  }

  async saveThis() {

    try {
      if (this.currentPermission == 'merchant') {
        delete this.form.merchant_username;
        delete this.form.merchant_group;
        delete this.form.redeem_type;
        delete this.form.oid;
      }
      
      let data = this.form;

      let isFormValid:boolean = true;

      this.temp_gallery = []
      Object.keys(this.data1).length != 0? this.temp_gallery.push(this.data1): null
      Object.keys(this.data2).length != 0? this.temp_gallery.push(this.data2): null
      Object.keys(this.data3).length != 0? this.temp_gallery.push(this.data3): null
      Object.keys(this.data4).length != 0? this.temp_gallery.push(this.data4): null
      Object.keys(this.data5).length != 0? this.temp_gallery.push(this.data5): null

      if(this.form.type != "e-voucher" && this.form.type != "top-up" && (this.form.dimensions.height <= 0 || this.form.dimensions.height == "" || this.form.dimensions.height == "0")) {
        isFormValid = this.scrollView('weight', 'heightAlert');
      } else {
        this.heightAlert = false;
      }

      if(this.form.type != "e-voucher" && this.form.type != "top-up" && (this.form.dimensions.length <= 0 || this.form.dimensions.length == "" || this.form.dimensions.length == "0")) {
        isFormValid = this.scrollView('weight', 'lengthAlert');
      } else {
        this.lengthAlert = false;
      }

      if(this.form.type != "e-voucher" && this.form.type != "top-up" && (this.form.dimensions.width <= 0 || this.form.dimensions.width == "" || this.form.dimensions.width == "0")) {
        isFormValid = this.scrollView('weight', 'widthAlert');
      } else {
        this.widthAlert = false;
      }

      if(this.form.type != "e-voucher" && this.form.type != "top-up" && (this.form.weightInGram <= 0 || this.form.weightInGram == "" || this.form.weightInGram == "0")) {
        isFormValid = this.scrollView('weight', 'weightAlert');
      } else {
        this.weightAlert = false;
      }

      if((this.multiVarian && this.multiVarian.length == 0) && (this.form.product_value <= 0 || this.form.product_value == "" || this.form.product_value == "0")) {
        isFormValid = this.scrollView('price', 'productValueAlert');
      } else {
        this.productValueAlert = false;
      }
    
      if(this.multiVarian && this.multiVarian.length > 0) {
        isFormValid = this.validateMultiVarian(true);
      }

      if((this.multiVarian && this.multiVarian.length == 0) && (this.form.product_price <= 0 || this.form.product_price == "" || this.form.product_price == "0")) {
        isFormValid = this.scrollView('price', 'productPriceAlert');
      } else {
        this.productPriceAlert = false;
      }

      if(this.form.type == "e-voucher" && this.form.tnc == "") {
        isFormValid = this.scrollView('tnc', 'tncAlert');
      } else {
        this.tncAlert = false;
      }

      if(this.form.description == "") {
        isFormValid = this.scrollView('description', 'descriptionAlert');
      } else {
        this.descriptionAlert = false;
      }

      if(this.form.product_code == "") {
        isFormValid = this.scrollView('product-information', 'productCodeAlert');
      } else {
        this.productCodeAlert = false;
      }

      if(this.form.product_name == "") {
        isFormValid = this.scrollView('product-information', 'productNameAlert');
      } else {
        this.productNameAlert = false;
      }

      if(this.temp_gallery.length <= 0) {
        isFormValid = this.scrollView('photo-alert', 'photoAlert');
      } else {
        this.photoAlert = false;
      }

      if(isFormValid == true) {
        data.images_gallery = this.temp_gallery;

        data.price = this.stringToNumber(data.price);
        data.fixed_price = this.stringToNumber(data.fixed_price);
        data.product_price = this.stringToNumber(data.product_price);
        data.product_value = this.stringToNumber(data.product_value);
        data.min_order = parseInt(data.min_order)
        data.weight = parseFloat(data.weight)
        data.dimensions = {
          length: parseFloat(data.type == "e-voucher" ? 0 : data.dimensions.length),
          width: parseFloat(data.type == "e-voucher" ? 0 : data.dimensions.width),
          height: parseFloat(data.type == "e-voucher" ? 0 : data.dimensions.height),
        }
        
        data.weightInGram = this.stringToNumber(data.weightInGram);
        data.weight = data.type == "e-voucher" ? 0 : data.weightInGram;
        data.qty = data.type == "e-voucher" ? 0 : parseInt(data.qty);

        let dataProduct:any;

        const allProductSKU :Array<any> = [];

        for(let i = 0; i < this.multiVarian.length; i++){
          for (let j = 0; j < this.multiVarian[i].length; j++){
            const tempProductSKU = this.multiVarian[i][j];
            delete tempProductSKU.isloading;
            tempProductSKU.variant = {
              [this.varianX.name]:this.varianTypeX[j] 
            };
            if(this.varianY.active) tempProductSKU.variant[this.varianY.name] = this.varianTypeY[i];
            allProductSKU.push(tempProductSKU);
          }
        }

        dataProduct = {
          product_name:data.product_name,
          product_code:data.product_code,
          product_price:data.product_price,
          product_value:data.product_value,
          qty:data.qty,
          description:data.description,
          type:data.type,
          category:data.category,
          status:data.status,
          weight:data.weight,
          dimensions:data.dimensions,
          images_gallery:data.images_gallery,
          // product_sku: allProductSKU,
        };

        if(this.varianX.active) {
          dataProduct.product_sku = allProductSKU;
          dataProduct.variation = {
            [this.varianX.name] : this.varianTypeX
          };
          if(this.varianY.active) dataProduct.variation[this.varianY.name] = this.varianTypeY;

        } else {
          dataProduct.variation = [];
          dataProduct.product_sku = [];
        }

        if(data.type == "e-voucher") {
          dataProduct.tnc = data.tnc;
        }

        console.log("data product",dataProduct);

        let result = await this.productService.addProducts(dataProduct);
        this.newData = 'new';

        Swal.fire("Product has been added!").then(() => {
          if (this.currentPermission == 'merchant') {
            this.router.navigate(['merchant-portal/product-list'], { queryParams: { id: result.result._id, newData: this.newData } })
          }
          else if (this.currentPermission == 'admin' && data.type == "e-voucher") {
            this.router.navigate(['administrator/vouchers']);
          }
          else if (this.currentPermission == 'admin') {
              this.router.navigate(['administrator/productadmin']);
          }
        });
      }
    } catch (e) {
      this.errorLabel = ((<Error>e).message);

      let message = this.errorLabel;
      if(this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
        message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
      }
      Swal.fire({
        icon: 'error',
        title: message,
      });
    }


  }

  validateForm(form) {
    for (var prop in form) {

      if (!form.hasOwnProperty(prop) || prop == 'tnc' || prop == 'category' || prop == 'weightInGram' || prop == 'hashtags' || prop == 'images_gallery' || prop == 'description')
        continue;

      if (prop == 'base_url') {
        if (!form[prop]) {
          throw new Error("Please upload new image for your product ")
        }
      }
      if (form[prop] == undefined)
        throw new Error("you need to completing this " + this.labels[prop])

      if (isString(form[prop]) && form[prop].trim() == '')
        throw new Error("you need to completing this input " + this.labels[prop])

      if (isArray(form[prop]) && form[prop].length == 0) {
        throw new Error("Hi, you need to completing this " + this.labels[prop])

      }

    }
  }

  setProductType(object) {
    try {
      this.listOfProductType.forEach((el) => {
        el.active = false;
      })
      object.active = true;
      this.form.type = object.value;

      if(this.form.type == 'product') {
        this.title = 'Product';
      } else if (this.form.type == 'voucher') {
        this.title = 'Voucher Fisik';
      } else if (this.form.type == 'e-voucher') {
        this.title = 'E-Voucher';
      } else if (this.form.type == 'gold') {
        this.title = 'Gold';
      } else if (this.form.type == 'top-up') {
        this.title = 'E-Wallet';
      }

      if (this.form.type == 'e-voucher') {
        this.form.merchant_username = "LOCARD";
      }
    } catch (error) {
      console.log("error", error);
    }
  }

  setVendorType(object) {
    this.listOfVendorType.forEach((el) => {
      el.active = false;
    })
    object.active = true;
    this.form.voucher_group = object.name
  }

  setRedeemType(object) {
    this.listOfRedeemType.forEach((el) => {
      el.active = false;
    })
    object.active = true;
    this.form.redeem_type = object.name
  }

  toggleCategories(index) {
    if(this.form.type == 'e-voucher'){
      this.multipleCategoriesDeals.forEach((el) => {
        el.value = false;
      })
      this.multipleCategoriesDeals[index].value = true;
    }
    if(this.form.type == 'product'){
      this.multipleCategoriesProduct.forEach((el) => {
        el.value = false;
      })
      this.multipleCategoriesProduct[index].value = true;
    }
  }

  backClicked() {
    this._location.back();
  }
}
