// Dear Future Developer, please consider leaving notes on your code, good habits will reduce development time ~ H

import { Component, OnInit } from '@angular/core';
import { TableFormat, productsTableFormat } from '../../../../object-interface/common.object';
import { ProductService } from '../../../../services/product/product.service';
import { Router } from '@angular/router';
import { query } from '@angular/animations';

@Component({
  selector: 'app-merchant-products',
  templateUrl: './merchant-products.component.html',
  styleUrls: ['./merchant-products.component.scss']
})
export class MerchantProductsComponent implements OnInit {

  Products: any = [];
  tableFormat: TableFormat;

  form_input: any = {};
  errorLabel: any = false;
  orderBy: string = "newest";
  filteredBy: string = "order_id"
  orderByResult: any = { order_date: -1 };
  allMemberDetail: any = false;
  service: any;
  swaper = true;
  loading = false;
  valueAll: any;
  previousPage: any;
  srcDownload: any;
  filterBy = [
    { name: 'Order ID', value: 'order_id' },
    { name: 'Buyer Name', value: 'buyer_name' },
    { name: 'Product Name', value: 'product_name' },
    { name: 'No. Resi', value: 'no_resi' }
  ]
  sortBy = [
    { name: 'Newest', value: 'newest' },
    { name: 'Highest Transactions', value: 'highest transactions' },
    { name: 'Lowest Transactions', value: 'lowest transactions' }
  ]
  page = 1;
  totalPage = 0;
  pageSize = 50;
  searchProduct: any = {};

  constructor(private productService: ProductService, private router: Router) {
    this.service = this.productService;
    this.tableFormat = productsTableFormat(this)

    console.log(this.tableFormat);

    this.tableFormat.label_headers.splice(0, 1) // Remove the Table Format Label for Merchant Username
    this.tableFormat.formOptions.addForm = false;
    delete this.tableFormat.formOptions.customButtons;

  }

  public async callDetail(product_id) {
    this.openDetail(product_id)
  }

  openDetail(product_id) {
    this.router.navigate(['merchant-portal/product-list/detail'], { queryParams: { id: product_id } })
  }

  swapClick(bool) {
    this.swaper = bool;
  }
  ngOnInit() {
    this.firstLoad();
  }
  addProduct() {
    // console.log('add product clicked');
    this.router.navigate(['merchant-portal/add']);

  }
  addProductBulk() {
    // console.log('add product clicked');
    this.router.navigate(['merchant-portal/report']);

  }
  async loadPage(page: number) {
    this.loading = true
    if (page !== this.previousPage) {
      console.log('prev page', this.previousPage, page)
      this.previousPage = page;
      var search = {
        search: {type: 'product'},
        order_by: null,
        limit_per_page: 50,
        current_page: page,
        download: false
      }
      // {"search":{"get_detail":true},"order_by":{},"limit_per_page":"50","current_page":3,"download":false}

      try {
        let result = await this.productService.searchProductsLint((search));
        if (result) {
          this.Products = result.result.values
          console.log('product berubah ', this.Products)
          this.loading = false
        }
      } catch (e) {
        console.log(e)
      }


      // this.loadData();
    }
  }

  async firstLoad() {
    try {
      let rQuery = {
        column_request: "product_code,product_name,price,fixed_price,qty,active,type,approved,pic_small_path,pic_medium_path,pic_big_path,base_url",
        // search: {'type':'product'}
      }

      let request = JSON.stringify(rQuery);

      let result: any;
      this.service = this.productService;
      result = await this.productService.getProductsReport();
      console.log("isi produk", result)
      this.valueAll = result.result.total_all_values
      this.Products = result.result.values;
      this.totalPage = result.result.total_page
      console.log("result", this.Products)

    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  async search($event) {
    let search = $event.target.value
    console.log("test", search)
    this.searchProduct = {
      current_page: 1,
      download: false,
      limit_per_page: 50,
      order_by: {},
      search: { product_name: search }
    }
    console.log("test")
    let result = await this.productService.searchProductsLint(this.searchProduct);
    this.Products = result.result.values;
    console.log("tst", this.searchProduct);
    // const url = await this.productService.getProductsReport();

  }
  async searchSortFilter(params) {
    console.log('awdawd', params)
    let order_by = params;
    this.searchProduct = {
      current_page: 1,
      download: false,
      limit_per_page: 50,
      order_by,
      search: {}
    }
    console.log("test")
    let result = await this.productService.searchProductsLint(this.searchProduct);
    this.Products = result.result.values;
    console.log("tst", this.searchProduct);
    // const url = await this.productService.getProductsReport();

  }
  async orderBySelected() {
    let params;
    if (this.orderBy == "highest transactions") {
      this.orderByResult = {
        total_price: -1
      }
      this.searchSortFilter(this.orderByResult)
      // this.valuechange({}, false, false)
    }
    else if (this.orderBy == "lowest transactions") {
      this.orderByResult = {
        total_price: 1
      }
      this.searchSortFilter(this.orderByResult)
      // this.valuechange({}, false, false)
    }
    else if (this.orderBy == "newest") {
      this.orderByResult = {
        order_date: -1
      }
      this.searchSortFilter(this.orderByResult)
      // this.valuechange({}, false, false)
    }
  }
}
