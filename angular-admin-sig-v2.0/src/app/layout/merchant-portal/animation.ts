import {trigger, transition, style, query, animateChild, group, animate, stagger, state} from '@angular/animations';

export const slideInAnimation =
    trigger('routeAnimations', [
        transition('* <=> *', [
            style({position: 'relative'}),
            // First State
            query(':enter', [
                style({position: 'absolute', 'z-index': '1', opacity: '0'})
            ], {optional: true}),
            // End State when it leave
            query(':leave', [style({position: 'absolute', 'z-index': '1', opacity: '1'})], {optional: true}),

            //animating is here
            group([
                query(':leave', [
                    animate('350ms ease-out', style({'z-index': '1', opacity: '0'}))
                ], {optional: true}),
                query(':enter', [
                    animate('350ms ease-out', style({'z-index': '1', opacity: '1'}))
                ], {optional: true})
            ]),
            query(':enter', animateChild(), {optional: true}),
        ]),

        // transition(':leave', [
        //     group([
        //       animate('1.5s ease', style({
        //         transform: 'translate(150px,25px)'
        //       })),
        //       animate('1.5s ease', style({
        //         opacity: 0
        //       }))
        //     ])
        //   ])


    ]);
