import { Component, OnInit } from '@angular/core';
import { EscrowTransactionService } from '../../../services/escrow-transaction/escrow-transaction.service';
import { TableFormat } from '../../../object-interface/common.object';
import { Router } from '@angular/router';
import { MemberService } from '../../../services/member/member.service';
import { MerchantService } from '../../../services/merchant/merchant.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-merchant-my-escrow',
  templateUrl: './merchant-my-escrow.component.html',
  styleUrls: ['./merchant-my-escrow.component.scss']
})
export class MerchantMyEscrowComponent implements OnInit {

  
  EscrowData: any = [];
  orderByResult: any;
  tableFormat        : TableFormat = {
                                  title           : 'My Escrow Transactions',
                                  label_headers   : [
                                    // {label: 'Owner ID', visible: true, type: 'string', data_row_name: 'owner_id'},
                                    {label: 'Created Date', visible: true, type: 'date', data_row_name: 'created_date'},
                                    {label: 'Description', visible: true, type: 'string', data_row_name: 'description'},
                                    {label: 'Nominal', visible: true, type: 'number', data_row_name: 'how_much'},
                                   
                                    {label: 'No. Ref', visible: true, type: 'string', data_row_name: 'record_id'},
                                    {label: 'Debit', visible: true, type: 'escrow-d', data_row_name: 'how_much'},
                                    {label: 'Credit', visible: true, type: 'escrow-c', data_row_name: 'how_much'},
                                    {label: 'Type', visible: true, options: ['withdrawal', 'plus', 'minus','hold'], type: 'list-escrow', data_row_name: 'type'},
                                    {label: 'status', visible: true, options: ['PENDING', 'RELEASED','HOLD','SUCCEESS'], type: 'list-escrow-status', data_row_name: 'transaction_status'},
                                  ],
                                  row_primary_key : '_id',
                                  formOptions     : {
                                                    row_id: '_id',
                                                    this  : this,
                                                    result_var_name: 'EscrowData',
                                                    detail_function: [this, 'callDetail']
                                                  }
  };
  hide;
  form_input    : any = {};
  errorLabel : any = false;
  total_page: 0;
  totalPage: any;
  pageSize = 50;
  pageNumbering: any = [];
  currentPage = 1;
  myBalance;
  setPin = false;
  number = [];
  member:any;
  search:any = {};
  memberDetail: any;
  pin = {
    password: '',
    new_pin: '',
    repeat_new_pin: ''
  }


  status = [
    {
      name: 'Pending', value :'pending'
    },
    {
      name: 'Success', value: 'success'
    },
    {
      name: 'Refunded', value: 'refunded'
    },
    {
      name: 'Hold', value : 'hold'
    },
    {
      name: 'Rejected', value: 'rejected'
    }
  ]

  types = [
    {name:'Debit', value : 'debit'},
    {name:'Credit', value : 'credit'},
    {name:'Withdrawal', value : 'withdrawal'},
    {name:'Hold', value : 'hold'},
  ]

  filter: string = "order_id";
  filterResult: any = {order_date: -1};

  pointstransactionDetail: any = false;
  service: any;
  constructor(public EscrowTransactionService:EscrowTransactionService, private router: Router, public merchantService:MerchantService, public memberService: MemberService) { }

  ngOnInit() {
    this.firstLoad();

  }

  async firstLoad(){
    try{

      let member;

      // let params = {
      //   search: {},
      //   current_page: this.currentPage,
      //   status: this.status,
      //   type: this.types,
      // }

      const resultDetail = await this.merchantService.getDetail();

      this.member = resultDetail.result

      this.service    = this.EscrowTransactionService;
      let result: any = await this.EscrowTransactionService.getEscrowtransactionLint();
      console.log("result", result)

      let resBalance = await this.EscrowTransactionService.getEscrowBalance();
      console.log("resbalance", resBalance)
      this.myBalance = resBalance.result.balance

      this.totalPage  = result.result.total_page
      console.log(this.totalPage)

      this.EscrowData = result.result.values;
      // this.pagesTotals(result.result.total_page)
      // this.pagesTotals(this.totalPage);

      this.EscrowData.forEach((element,index) => {
        this.number.push(index + 1);
        // console.log("values", this.EscrowData.values)
        // console.log("number", this.number)
      })

    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  pagesTotals(totalPage){
    this.pageNumbering = [];
    this.total_page = totalPage;
    this.pageNumbering = this.pagination(this.currentPage, totalPage);
  }

  public async callDetail(escrowID){
    try{
      let result: any;
      
      this.router.navigate(
        ['merchant-portal/my-money/detail'],  
        {queryParams: {id: escrowID }})

    } catch (e) {
      this.errorLabel = ((<Error>e).message);//conversion to Error type
    }
  }

  public async backToHere(obj){
    obj.pointstransactionDetail = false;
  }

  moneyDetail(){
    console.log(true)
  }

  async checkAccount(){
    try{
      let result = await this.memberService.getMemberDetail();
      this.memberDetail = result.result
    } catch(error){
      console.log(((<Error>error).message))
    }
   
    if(!this.member.withdrawal_bank_account){
      Swal.fire("Sorry", "You don't have bank account to access this page, fill <b>Payment Info</b> first ", "error")
      this.router.navigate(['merchant-portal/profile'])
      console.log(true)
    } 
    if (this.memberDetail.access_pin != "enabled"){ 
      Swal.fire("Sorry", "You need to set PIN to withdraw", "error")
      this.setPin = true;
    } 
    if (this.memberDetail.access_pin == "enabled") {this.router.navigate(['merchant-portal/my-money/withdrawal'])}
  }

  pagination(c, m) {
    var current = c,
      last = m,
      delta = 2,
      left = current - delta,
      right = current + delta + 1,
      range = [],
      rangeWithDots = [],
      l;

    for (let i = 1; i <= last; i++) {
      if (i == 1 || i == last || i >= left && i < right) {
        range.push(i);
      }
    }

    for (let i of range) {
      if (l) {
        if (i - l === 2) {
          rangeWithDots.push(l + 1);
        } else if (i - l !== 1) {
          rangeWithDots.push('...');
        }
      }
      rangeWithDots.push(i);
      l = i;
    }

    return rangeWithDots;
  }

  async onChangePage(pageNumber) {
    if (pageNumber <= 0) {
      pageNumber = 1;
    }
    if (pageNumber >= this.total_page) {
      pageNumber = this.total_page;
    }

    this.currentPage = pageNumber;
    console.log(pageNumber, this.currentPage, this.total_page);

    // this.valuechange({}, false, false);
  }

  checkPin(){
    console.log("test")
    
  }


  closeDialog(){
  this.setPin = false;
  }

  async verify(){
		const settingPin = this.pin;

		var password = settingPin.password.toString();
		var newPin = settingPin.new_pin.toString();
		var repeatNewPin = settingPin.repeat_new_pin.toString();

		// settingPin.password = settingPin.password.toString();
		
		
		// console.log(settingPin)

		console.log(password)
		console.log(newPin)
		console.log(repeatNewPin)

		
		try{




		let result = await this.memberService.setNewPin(settingPin) 

		if(result.result){
			Swal.fire("Congratulations", "Your PIN has been registered to LOCARD", "success");
			// this.retypePin= false;
			this.setPin = false;
			// this.router.navigate(['merchant-portal/profile']);
		}
	} catch(error){
		console.log(((<Error>error).message))
	}
		
		// if(result = true){
		// 	this.closeDialog = true;
		// }

	}

  
  
}






