import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EscrowTransactionService } from '../../../../services/escrow-transaction/escrow-transaction.service';

@Component({
  selector: 'app-merchant-escrow-detail',
  templateUrl: './merchant-escrow-detail.component.html',
  styleUrls: ['./merchant-escrow-detail.component.scss']
})
export class MerchantEscrowDetailComponent implements OnInit {

  service:any;
  data:any;
  constructor(private route: ActivatedRoute,
    private EscrowTransactionService: EscrowTransactionService
    ) { 
   

  }

  ngOnInit() {
    this.firstload()
  }

  async firstload(){
    this.route.queryParams.subscribe( async (params) =>{
      let escrowID    = params.id;
      this.service    = this.EscrowTransactionService;
      let result      = await this.EscrowTransactionService.detailEscrowtransaction(escrowID);
      this.data       = result.result;
    })
  }

  back(){
    window.history.back();
  }

}
