"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.MerchantMyEscrowComponent = void 0;
var core_1 = require("@angular/core");
var sweetalert2_1 = require("sweetalert2");
var MerchantMyEscrowComponent = /** @class */ (function () {
    function MerchantMyEscrowComponent(EscrowTransactionService, router, merchantService, memberService) {
        this.EscrowTransactionService = EscrowTransactionService;
        this.router = router;
        this.merchantService = merchantService;
        this.memberService = memberService;
        this.EscrowData = [];
        this.tableFormat = {
            title: 'My Escrow Transactions',
            label_headers: [
                // {label: 'Owner ID', visible: true, type: 'string', data_row_name: 'owner_id'},
                { label: 'Created Date', visible: true, type: 'date', data_row_name: 'created_date' },
                { label: 'Description', visible: true, type: 'string', data_row_name: 'description' },
                { label: 'Nominal', visible: true, type: 'number', data_row_name: 'how_much' },
                { label: 'No. Ref', visible: true, type: 'string', data_row_name: 'record_id' },
                { label: 'Debit', visible: true, type: 'escrow-d', data_row_name: 'how_much' },
                { label: 'Credit', visible: true, type: 'escrow-c', data_row_name: 'how_much' },
                { label: 'Type', visible: true, options: ['withdrawal', 'plus', 'minus', 'hold'], type: 'list-escrow', data_row_name: 'type' },
                { label: 'status', visible: true, options: ['PENDING', 'RELEASED', 'HOLD', 'SUCCEESS'], type: 'list-escrow-status', data_row_name: 'transaction_status' },
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: '_id',
                "this": this,
                result_var_name: 'EscrowData',
                detail_function: [this, 'callDetail']
            }
        };
        this.form_input = {};
        this.errorLabel = false;
        this.pageSize = 50;
        this.pageNumbering = [];
        this.currentPage = 1;
        this.setPin = false;
        this.number = [];
        this.search = {};
        this.pin = {
            password: '',
            new_pin: '',
            repeat_new_pin: ''
        };
        this.status = [
            {
                name: 'Pending', value: 'pending'
            },
            {
                name: 'Success', value: 'success'
            },
            {
                name: 'Refunded', value: 'refunded'
            },
            {
                name: 'Hold', value: 'hold'
            },
            {
                name: 'Rejected', value: 'rejected'
            }
        ];
        this.types = [
            { name: 'Debit', value: 'debit' },
            { name: 'Credit', value: 'credit' },
            { name: 'Withdrawal', value: 'withdrawal' },
            { name: 'Hold', value: 'hold' },
        ];
        this.filter = "order_id";
        this.filterResult = { order_date: -1 };
        this.pointstransactionDetail = false;
    }
    MerchantMyEscrowComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    MerchantMyEscrowComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var member, resultDetail, result, resBalance, e_1;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 4, , 5]);
                        member = void 0;
                        return [4 /*yield*/, this.merchantService.getDetail()];
                    case 1:
                        resultDetail = _a.sent();
                        this.member = resultDetail.result;
                        this.service = this.EscrowTransactionService;
                        return [4 /*yield*/, this.EscrowTransactionService.getEscrowtransactionLint()];
                    case 2:
                        result = _a.sent();
                        console.log("result", result);
                        return [4 /*yield*/, this.EscrowTransactionService.getEscrowBalance()];
                    case 3:
                        resBalance = _a.sent();
                        console.log("resbalance", resBalance);
                        this.myBalance = resBalance.result.balance;
                        this.totalPage = result.result.total_page;
                        console.log(this.totalPage);
                        this.EscrowData = result.result.values;
                        // this.pagesTotals(result.result.total_page)
                        // this.pagesTotals(this.totalPage);
                        this.EscrowData.forEach(function (element, index) {
                            _this.number.push(index + 1);
                            // console.log("values", this.EscrowData.values)
                            // console.log("number", this.number)
                        });
                        return [3 /*break*/, 5];
                    case 4:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    MerchantMyEscrowComponent.prototype.pagesTotals = function (totalPage) {
        this.pageNumbering = [];
        this.total_page = totalPage;
        this.pageNumbering = this.pagination(this.currentPage, totalPage);
    };
    MerchantMyEscrowComponent.prototype.callDetail = function (escrowID) {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                try {
                    result = void 0;
                    this.router.navigate(['merchant-portal/my-money/detail'], { queryParams: { id: escrowID } });
                }
                catch (e) {
                    this.errorLabel = (e.message); //conversion to Error type
                }
                return [2 /*return*/];
            });
        });
    };
    MerchantMyEscrowComponent.prototype.backToHere = function (obj) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                obj.pointstransactionDetail = false;
                return [2 /*return*/];
            });
        });
    };
    MerchantMyEscrowComponent.prototype.moneyDetail = function () {
        console.log(true);
    };
    MerchantMyEscrowComponent.prototype.checkAccount = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.memberService.getMemberDetail()];
                    case 1:
                        result = _a.sent();
                        this.memberDetail = result.result;
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        console.log((error_1.message));
                        return [3 /*break*/, 3];
                    case 3:
                        if (!this.member.withdrawal_bank_account) {
                            sweetalert2_1["default"].fire("Sorry", "You don't have bank account to access this page, fill <b>Payment Info</b> first ", "error");
                            this.router.navigate(['merchant-portal/profile']);
                            console.log(true);
                        }
                        if (this.memberDetail.access_pin != "enabled") {
                            sweetalert2_1["default"].fire("Sorry", "You need to set PIN to withdraw", "error");
                            this.setPin = true;
                        }
                        if (this.memberDetail.access_pin == "enabled") {
                            this.router.navigate(['merchant-portal/my-money/withdrawal']);
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    MerchantMyEscrowComponent.prototype.pagination = function (c, m) {
        var current = c, last = m, delta = 2, left = current - delta, right = current + delta + 1, range = [], rangeWithDots = [], l;
        for (var i = 1; i <= last; i++) {
            if (i == 1 || i == last || i >= left && i < right) {
                range.push(i);
            }
        }
        for (var _i = 0, range_1 = range; _i < range_1.length; _i++) {
            var i = range_1[_i];
            if (l) {
                if (i - l === 2) {
                    rangeWithDots.push(l + 1);
                }
                else if (i - l !== 1) {
                    rangeWithDots.push('...');
                }
            }
            rangeWithDots.push(i);
            l = i;
        }
        return rangeWithDots;
    };
    MerchantMyEscrowComponent.prototype.onChangePage = function (pageNumber) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                if (pageNumber <= 0) {
                    pageNumber = 1;
                }
                if (pageNumber >= this.total_page) {
                    pageNumber = this.total_page;
                }
                this.currentPage = pageNumber;
                console.log(pageNumber, this.currentPage, this.total_page);
                return [2 /*return*/];
            });
        });
    };
    MerchantMyEscrowComponent.prototype.checkPin = function () {
        console.log("test");
    };
    MerchantMyEscrowComponent.prototype.closeDialog = function () {
        this.setPin = false;
    };
    MerchantMyEscrowComponent.prototype.verify = function () {
        return __awaiter(this, void 0, void 0, function () {
            var settingPin, password, newPin, repeatNewPin, result, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        settingPin = this.pin;
                        password = settingPin.password.toString();
                        newPin = settingPin.new_pin.toString();
                        repeatNewPin = settingPin.repeat_new_pin.toString();
                        // settingPin.password = settingPin.password.toString();
                        // console.log(settingPin)
                        console.log(password);
                        console.log(newPin);
                        console.log(repeatNewPin);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.memberService.setNewPin(settingPin)];
                    case 2:
                        result = _a.sent();
                        if (result.result) {
                            sweetalert2_1["default"].fire("Congratulations", "Your PIN has been registered to LOCARD", "success");
                            // this.retypePin= false;
                            this.setPin = false;
                            // this.router.navigate(['merchant-portal/profile']);
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        error_2 = _a.sent();
                        console.log((error_2.message));
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    MerchantMyEscrowComponent = __decorate([
        core_1.Component({
            selector: 'app-merchant-my-escrow',
            templateUrl: './merchant-my-escrow.component.html',
            styleUrls: ['./merchant-my-escrow.component.scss']
        })
    ], MerchantMyEscrowComponent);
    return MerchantMyEscrowComponent;
}());
exports.MerchantMyEscrowComponent = MerchantMyEscrowComponent;
