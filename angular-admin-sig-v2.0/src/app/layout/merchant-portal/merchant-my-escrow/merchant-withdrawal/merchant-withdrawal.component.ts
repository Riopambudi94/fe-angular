import { Component, OnInit } from '@angular/core';
import { EscrowTransactionService } from '../../../../services/escrow-transaction/escrow-transaction.service';
import { timingSafeEqual } from 'crypto';
import { Router } from '@angular/router';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-merchant-withdrawal',
  templateUrl: './merchant-withdrawal.component.html',
  styleUrls: ['./merchant-withdrawal.component.scss']
})
export class MerchantWithdrawalComponent implements OnInit {

  constructor(private EscrowTransactionService: EscrowTransactionService,
    private router: Router,
    fb: FormBuilder
  ) { 
    this.options = fb.group({
      hideRequired: this.hideRequiredControl,
      floatLabel: this.floatLabelControl,
    });
  }
  options: FormGroup;
  hideRequiredControl = new FormControl(false);
  floatLabelControl = new FormControl('never');
  myBalance
  ngOtpInput: any;
  errorMessage
  loading= false;
  form: any = {};
  formEditable: any = {};
  totalPage: 0;
  filled: false;
  otp;
  othersForm = false;
  config = {
    allowNumbersOnly: true,
    length: 6,
    isPasswordInput: true,
    disableAutoFocus: false,
    placeholder:'',
    inputStyles: {
      'width': '50px',
      'height': '50px'
    }
  };
  ngOnInit() {
    this.firstload()
  }

  async firstload() {
    try {
      let resBalance = await this.EscrowTransactionService.getEscrowBalance();
      this.myBalance = resBalance.result.balance
      console.log("this.myBalance", this.myBalance)
    }
    catch (e) {
      console.log("E report", e)
    }

  }

  editableForm(how_much, how) {

  }

  stringToNumber(str: any) {
    if (typeof str == 'number') {
      str = str + '';
    }

    let s = str.toLowerCase().replace(/[^0-9]/g, '');

    return parseInt(s);
  }

  closeDialog(){
    this.othersForm = false;
  }
  forgotPin(){
    console.log('forgotpin')
  }
  async sendRequest() {
    this.errorMessage = null;

    let balance: number = this.myBalance

    if (this.form.how_much) {
      try {
        let curHowMuch = this.stringToNumber(this.form.how_much);
        this.form.how_much = curHowMuch
        if (curHowMuch > this.myBalance) {
          this.errorMessage = "Your balance is not enough";
        } else if (curHowMuch < balance && curHowMuch < 50000){
          this.errorMessage = "Your withdrawal amount in less than Rp. 50.000"
        } 
      } catch (e) {
        this.errorMessage = "please fill with the correct number";
      }

      if(!this.form.description){
        this.errorMessage = "Please fill the Description";
      }

      if (!this.errorMessage) {
        this.othersForm = true;
       }
    } else {
      this.errorMessage = "please fill with the correct number";
    }

  }
  onOtpChange(otp) {
    this.form.pin_code = otp;
    console.log(this.form.otp)
  }
  async confirm() {
    this.loading = true
    try {
      let r = await this.EscrowTransactionService.escrowWithdrawalRequest(this.form)
      if (!r.error) {
        alert("we will process your request soon")
        this.othersForm = false;
        this.router.navigate(['merchant-portal/my-money/'])
      }
      this.loading = false;
    }
    catch(e){
      this.errorMessage = (<Error>e).message
    }
    
  }
}
