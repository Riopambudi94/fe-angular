import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShippingLabelRoutingModule } from './shipping-label-routing.module';
import { ShippingLabelComponent } from './shipping-label.component';
import { NgxBarcodeModule } from 'ngx-barcode';


@NgModule({
  declarations: [],
  // declarations: [ShippingLabelComponent],
  imports: [
    NgxBarcodeModule,
    CommonModule,
    ShippingLabelRoutingModule
  ]
})
export class ShippingLabelModule { }
