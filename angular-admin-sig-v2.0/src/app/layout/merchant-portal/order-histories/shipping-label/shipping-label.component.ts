import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { OrderhistoryService } from '../../../../services/orderhistory/orderhistory.service';
import { async } from 'q';
import { MemberService } from '../../../../services/member/member.service';
@Component({
  selector: 'app-shipping-label',
  templateUrl: './shipping-label.component.html',
  styleUrls: ['./shipping-label.component.scss']
})
export class ShippingLabelComponent implements OnInit {

  service;
  getData = [];
  totalResult = [];
  totalWeight = [];
  perWeight = [];
  endTotalResult = [];
  loading = true;
  same = false;
  merchant_data ={};
  memberCity:any;
  courier_list: any;
  current_courier: any;
  courier_image:any;
  courier_image_array: any = [];
    
  errorMessage: string;
  constructor(private OrderhistoryService: OrderhistoryService, private router: Router, private route: ActivatedRoute,private memberService: MemberService,) {
    // this.getData = route.snapshot.params['invoiceIds']
    //   .split(',');
  }

  ngOnInit() {
    this.firstload();
    // this.print();
  }

  async firstload() {
    try {
      let member = await this.memberService.getMemberDetail();
      this.merchant_data = member.result
      console.log('data' , this.merchant_data)
    if (member.result){
      this.memberCity = member.result.mcity
      console.log("member",member.result)
    }
    }catch(e){
			this.errorMessage = ((<Error>e).message)
		}
    this.route.queryParams.subscribe(async (params) => {
      this.getData = params.data
      this.service = this.OrderhistoryService
      console.log(this.getData)
    })
      for (let element of this.getData) {
       
        // let result = await this.OrderhistoryService.detailOrderhistory(element);
        try {
          let result = await this.OrderhistoryService.getDetailOrderhistory(element);
        console.log(result)
        this.totalResult.push(result.result[0]) 
        
        }catch(e){
          this.errorMessage = ((<Error>e).message)
          this.loading = false
        }
        // let result = await this.OrderhistoryService.getDetailOrderhistory(element);
        // console.log(result)
        // this.totalResult.push(result.result[0])
        // console.log(result2)
      } 
      
      console.log(this.totalResult)
      this.totalResult.forEach((element,index) => {
        console.log("asd",element)
        this.current_courier = element.courier.courier
        this.courier_list = element.courier_list.supported
        this.courier_list.forEach((entry) => {
          // console.log(entry)
          console.log(entry.courier)
          if( entry.courier == this.current_courier){ 
            let format = {
              'image' : entry.image_url,'courier':this.current_courier, 'index': index, 'same': true
            }
            
            this.courier_image_array.push(format)
            this.courier_image = entry.image_url
            console.log("image", this.courier_image)
            this.same = true; 
          }
        })
        if(this.same == false){
          let format = {
            'image' : '','courier': this.current_courier, 'index': index, 'same': false
          }
          this.courier_image_array.push(format)
        }
        element.products.forEach((el,i) => {
          this.perWeight = el.weight;
          
        });
        this.same = false;
        console.log('courier image',this.courier_image_array)
        this.totalWeight[index] = this.perWeight
      });
  
    // window.print();
    this.loading = false;
  }

  public prints(){
    window.print()
  }
}
