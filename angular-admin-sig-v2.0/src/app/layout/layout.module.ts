import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NgbDropdownModule, NgbModule, } from '@ng-bootstrap/ng-bootstrap';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HeaderComponent } from './components/header/header.component';
import { FormBuilderTableModule } from '../component-libs/form-builder-table/form-builder-table.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsComponentModule } from './modules/bs-component/bs-component.module';
import { MemberDetailComponent } from './modules/member/detail/member.detail.component';
import { MemberEditComponent } from './modules/member/edit/member.edit.component';
import { ProductEditComponent } from './modules/product/edit/product.edit.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { ProductDetailComponent } from './modules/product/detail/product.detail.component';
import { OrderhistoryallhistoryModule } from './modules/orderhistoryallhistory/orderhistoryallhistory.module';
import { PortalGuard } from '../shared/guard/portal.guard';
import { RouterMasterComponent } from './router-master/router-master.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdminOrderHistoryComponent } from './modules/admin-order-history/admin-order-history.component';

@NgModule({
    imports: [
        CommonModule,
        LayoutRoutingModule,
        TranslateModule,
        NgbModule,
        NgbDropdownModule,
        FormBuilderTableModule,
        FormsModule,
        ReactiveFormsModule,
        FormBuilderTableModule,
        BsComponentModule,
        CKEditorModule,
        OrderhistoryallhistoryModule,
        
    ],
    declarations: [LayoutComponent,  RouterMasterComponent, DashboardComponent, AdminOrderHistoryComponent, 
        // EvoucherSalesOrderComponent,
        // MemberDetailComponent, 
        // MemberEditComponent, 
        // ProductEditComponent,
        // ProductDetailComponent

    ],
    providers:[
        PortalGuard
    ]
})
export class LayoutModule { }
