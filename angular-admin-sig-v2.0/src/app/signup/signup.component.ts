import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import { RegisterService } from '../services/register/register.service';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss'],
    animations: [routerTransition()]
})
export class SignupComponent implements OnInit {
    public name: string = "";
    Register: any = [];
    service: any;
    errorLabel : any = false;
    password: string ;
    repeatPassword: string;
    full_name: string;
    email: string;

    constructor(public registerService:RegisterService) {
        let form_add     : any = [
            { label:"Full Name",  type: "text",  value: "", data_binding: 'full_name'  },
            { label:"Email",  type: "text",  value: "", data_binding: 'email'  },
            { label:"Password",  type: "password",  value: "", data_binding: 'password'  },
            { label:"Repeat Password",  type: "password",  value: "", data_binding: 'password'  }
          ];
    }

    ngOnInit() {
        this.firstLoad();
    }

    async firstLoad()
    {

    }

    async formSubmitAddMember(form){
        //console.log(form);
        //console.log(JSON.stringify(form));
        try 
        {
            this.service    = this.registerService;
            let result: any  = await this.registerService.register(form);
            //console.log(form);
            this.Register = result.result;
        } 
        catch (e) 
        {
            this.errorLabel = ((<Error>e).message);//conversion to Error type
        }
      }
}
