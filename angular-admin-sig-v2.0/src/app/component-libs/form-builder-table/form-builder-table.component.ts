// Dear Future Developer, please consider leaving notes on your code, good habits will reduce development time ~ H

import { Component, OnInit, Input, OnChanges, SimpleChanges, SimpleChange, Output, EventEmitter } from '@angular/core';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date';
import { DomSanitizer } from '@angular/platform-browser';
import { NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { getHost } from '../../../environments/environment';
import {FormOptions, TableFormat} from '../../object-interface/common.object';
import { ActivatedRoute, Router } from '@angular/router';
import { DownloadService } from '../../services/download/download.service';
import * as json2csv  from 'json2csv';
import * as XLSX from 'xlsx';


// import { ConsoleReporter } from 'jasmine';

@Component({
  selector: 'app-form-builder-table',
  templateUrl: './form-builder-table.component.html',
  styleUrls: ['./form-builder-table.component.scss']
})
export class FormBuilderTableComponent implements OnInit, OnChanges {

  @Input() public table_data: any[];
  @Input() public total_page: any;
  @Input() public table_row;
  @Input() public table_header;
  @Input() public table_footer;
  @Input() public table_title;
  @Input() public searchCallback;
  @Input() public row_id;
  @Input() public options: FormOptions;
  @Input() public table_detail;
  @Input() public image_data_property;
  @Input() public tableFormat: TableFormat;
  @Input() public detail: any;
  @Input() public direct_download : any;
  @Output() onPageChange = new EventEmitter<any>();

  public orderBy: any = [];
  public form_input: any = {};
  public onSearchActive: boolean = false;
  public showLoading: boolean = false;
  public form_detail: any = [];
  public edit: boolean = false;
  public list_data: any = [];
  public columnCheck: boolean = false;
  public columnCB: any[];
  public salesRedemp : any = [];
  public completeMember : any = [];
  public table_head;
  public pagesLimiter = [];
  public pageLimits = 50;
  public pageNumbering = [];
  public setupSetPage = false;
  public currentPage = 1;
  public checkBox = [];
  public activeCheckbox=false;
  public asc = true;
  public download_temp: any = {};
  public showSearch : boolean = true;
  srcDownload;
  bast_mode = false;
  toggler:any = {};
  public order: any= {};

  constructor(calendar: NgbCalendar, public sanitizer:DomSanitizer, private router: Router,public downloadService: DownloadService) {
    this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl("");
    // this.fromDate = calendar.getToday();
    // this.toDate = calendar.getNext(calendar.getToday(), 'd', 10);
    // console.log('HadfI', this.table_row);
    // console.log('HsdI', this.table_row);
    this.pagesLimiter = this.pageLimiter();
  }

  // checkboxFunction(cButton:any){
  //   if(this.checkBox.length ==0) return false;

  //   let getData = [];
  //   this.checkBox.forEach((el,i)=>{
  //       getData.push(this.table_data[i]);
  //   })
  //   cButton.func(getData);
  // }
  
  ngOnInit() {
    if(this.direct_download) this.showSearch = false;
    // console.log("hasil tabel", this.table_data)
    if(this.tableFormat != undefined) {
      this.table_header = this.tableFormat.label_headers
      this.table_title  = this.tableFormat.title
      this.row_id       = this.tableFormat.row_primary_key
      this.options      = this.tableFormat.formOptions
      // console.log('callback',this.searchCallback)
      if(this.total_page){
        this.buildPagesNumbers(this.total_page)
      
      }
    }

    if (!this.options) {
      this.options = null
    }
    if (!this.options.type) {
      this.options.type = 'row-text';

    }
    if (!this.options.image_data_property) {
      this.options.image_data_property = 'url';

    }

    this.table_header.forEach((a, index) => {
  
      if(typeof this.table_header[index] == 'string') {
        this.table_header[index] = {
          label: this.table_header[index],
          type: 'string'
        }
      }
      else{

        if(this.table_header[index].type == 'list' || this.table_header[index].type == 'list-escrow' || this.table_header[index].type == 'list-escrow-status') {
            this.table_header[index].options.forEach((b, index_options) => {
              if(typeof this.table_header[index].options[index_options] == 'string') {
                this.table_header[index].options[index_options] = {
                  label: this.table_header[index].options[index_options],
                  value: this.table_header[index].options[index_options],
                  
                }
              }
          });

        
        }
        
      }


      // this.form_input[a] = '';

    })
    
    this.showTableHead();
  }

  showTableHead(){
    this.table_head = [];

    this.table_header.forEach((a, index) => {

      if(a.visible != false){
        if(a.type == 'number' && this.form_input[a.data_row_name] == undefined){
          let ob:any = {option:'equal'};
          this.form_input[a.data_row_name] = ob;
        }
        else{
          this.form_input[a.data_row_name] = '';
        }
        this.table_head.push(a);

      }
    });

    // console.warn("table head", this.table_head)
  }

  checkFileType(text : any){
    let splitFileName = text.split('.');
    const extensFile = splitFileName[splitFileName.length - 1];
    const extens = extensFile.toLowerCase();
    return extens == 'pdf' ? 'document':'image';
  }

  openLabel() {
    this.router.navigate(['administrator/orderhistoryallhistoryadmin/receipt'], { queryParams: { id: this.detail._id } })
  }

  getValueofCheckbox(this){
      // console.warn("active checkbox", this.activeCheckbox)
      let getData = [];
      this.checkBox.forEach((el,i)=>{
        var data = this.table_data[i];
        if(!this.activeCheckbox){
        
          if (this.options.salesRedemption || this.options.bastReport){
           
          } else {
            getData.push(data._id);
          }
          
          // console.log(this.salesRedemp, this.checkBox)
        }
        if(this.activeCheckbox){
          console.log("HERE")
        }
        // else{
        //   var index = getData.indexOf(data._id);
        //   if (index !== -1) {
        //     getData.splice(index, 1);
        //     this.salesRedemp.splice(index, 1)
        //   console.log(this.salesRedemp)

        //   }

        // }
      })
      // this.salesRedemp.push(data) 
  

  }

  checkedColumn() {
    setTimeout(() => {
      this.showTableHead();
    }, 500);
  }

  checkedAll(){
    setTimeout(() => {
      this.table_data.forEach((e,i)=>{
        if(this.activeCheckbox){
          this.checkBox[i] = this.activeCheckbox
         console.log('checkbox all', this.checkBox[i])
        }
        else{
          this.checkBox=[];
        }
      })
    }, 100);
    
  }

  /* Date Picker Part */

  datePickerOnDateSelection(date: NgbDate, formName){
    
    let _this:any = this.make_This(formName);

    if (!_this.fromDate && !_this.toDate) {
      _this.fromDate = date;
    }
    else if (_this.fromDate && !_this.toDate && this.datePickerDateAfter( date, _this.fromDate)) {
      _this.toDate = date;
    }
    else {
      _this.toDate = null;
      _this.fromDate = date;
    }

    _this.from = (_this.fromDate != undefined && _this.fromDate != null) ? this.convertToDateString(_this.fromDate) : ''
    _this.to   = (_this.toDate   != undefined && _this.toDate   != null) ? this.convertToDateString(_this.toDate) : ''
  }

  typeof(object){
    return typeof object;
  }

  make_This(formName){
    if(typeof formName == 'object'){
      return formName;
    }

    if(this.form_input[formName] == undefined || this.form_input[formName] == ''){
      this.form_input[formName] = {};
    }
    
    return this.form_input[formName];
  }

  dateToggler(formName){
    this.toggler[formName] = (typeof this.toggler == undefined)? true: !this.toggler[formName];

    if(this.toggler[formName] == false && this.form_input[formName] !== ''){
      this.valuechange({}, false, false);
    }

    // console.log(this.form_input);
  }
  
  dateClear(event, dataInput, deleteInput){
    this.form_input[dataInput] = '';
    this.valuechange(event, deleteInput);
  }
  
  datePickerOnDateIsInside(date: NgbDate, formName) {
    let _this:any = this.make_This(formName)

    return this.datePickerDateAfter(date, _this.fromDate) && this.datePickerDateBefore(date, _this.toDate);
  }

  datePickerOnDateIsRange(date: NgbDate, formName) {
    let _this:any = this.make_This(formName)

    return this.datePickerDateEquals(date, _this.fromDate) || this.datePickerDateEquals(date,_this.toDate) || this.datePickerOnDateIsInside(date, _this) || this.datePickerOnDateIsHovered(date, _this);
  }

  datePickerOnDateIsHovered(date: NgbDate, formName) {
    let _this:any = this.make_This(formName)
    return _this.fromDate && !_this.toDate && _this.hoveredDate && this.datePickerDateAfter(date,_this.fromDate) && this.datePickerDateBefore(date, _this.hoveredDate);
  }

  datePickerDateAfter(datePrev:NgbDate, dateAfter: NgbDate){
    if(datePrev == undefined || dateAfter == undefined) return false

    let iFromDate = new Date(datePrev.year+ '-'+datePrev.month+ '-'+datePrev.day)
    let iToDate    = new Date(dateAfter.year+ '-'+dateAfter.month+ '-'+dateAfter.day)
    if(iToDate.getTime() < iFromDate.getTime()){
      return true
    }
    return false
  }

  convertToDateString(datePrev: NgbDate){
    
    return datePrev.year+ '-'+datePrev.month+ '-'+datePrev.day
  }
  datePickerDateEquals(datePrev:NgbDate, dateAfter: NgbDate){
    if(datePrev == undefined || dateAfter == undefined) return false

    let iFromDate = new Date(datePrev.year+ '-'+datePrev.month+ '-'+datePrev.day)
    let iToDate    = new Date(dateAfter.year+ '-'+dateAfter.month+ '-'+dateAfter.day)
    if(iToDate.getTime() == iFromDate.getTime()){
      return true
    }
    return false
  }

  datePickerDateBefore(datePrev:NgbDate, dateAfter: NgbDate){
    if(datePrev == undefined || dateAfter == undefined) return false

    let iFromDate = new Date(datePrev.year+ '-'+datePrev.month+ '-'+datePrev.day)
    let iToDate    = new Date(dateAfter.year+ '-'+dateAfter.month+ '-'+dateAfter.day)
    if(iToDate.getTime() > iFromDate.getTime()){
      return true
    }
    return false
  }

  /* end of datepicker component */

  ngOnChanges(changes: SimpleChanges) {
    // console.log("table data", this.table_data)
    if (this.setupSetPage == false){
      if (this.total_page > 0){
        
        this.buildPagesNumbers(this.total_page)
        // console.log(this.total_page)
        this.setupSetPage = true;

      }
    }
    // console.log('HI', this.total_page);
    // console.log('changes', this.table_row);
  }

  toggleColoumnCheck() {
    this.columnCheck = !this.columnCheck;
  }

  onChangePage(pageNumber){
    if(pageNumber <= 0 ){
      pageNumber = 1
    }
    if(pageNumber >= this.total_page){
      pageNumber = this.total_page;
    }

    this.currentPage = pageNumber;
    // console.log(pageNumber, this.currentPage, this.total_page)

    this.onPageChange.emit(this.currentPage);

    this.valuechange({}, false, false)
  }

  pageLimitChanges(event) {
    // console.log('Test limiter', this.pageLimits);
    this.valuechange({}, false, false);

  }

  pageLimiter() {
    return [
      { id: 10, name: '10' },
      { id: 20, name: '20' },
      { id: 50, name: '50' },
      { id: 100, name: '100' }
    ];
  }
  
  isCurrentPage(tp){
    if(tp == this.currentPage){
      return 'current-page';
    }
    else return ''
  }

   pagination(c, m) {
        var current = c,
          last = m,
          delta = 2,
          left = current - delta,
          right = current + delta + 1,
          range = [],
          rangeWithDots = [],
          l;

        for (let i = 1; i <= last; i++) {
          if (i == 1 || i == last || i >= left && i < right) {
            range.push(i);
          }
        }

        for (let i of range) {
          if (l) {
            if (i - l === 2) {
              rangeWithDots.push(l + 1);
            } else if (i - l !== 1) {
              rangeWithDots.push('...');
            }
          }
          rangeWithDots.push(i);
          l = i;
        }

        return rangeWithDots;
      }

  buildPagesNumbers(totalPage){
    this.pageNumbering = [];
    
    // let maxNumber = 8;
    this.total_page = totalPage;
    this.pageNumbering = this.pagination(this.currentPage, totalPage);

    
  }
  orderChange(event, orderBy) {
    // var row_name = orderBy
    if (this.orderBy[orderBy] && this.asc == false) {
      this.orderBy[orderBy].asc = !this.orderBy[orderBy].asc;
      this.asc = true;
      this.order[orderBy]= 1;
    }
    else {
      this.asc = false
      this.order = [];
      this.orderBy = [];
      this.orderBy[orderBy] = { asc: false };
      this.order[orderBy]= -1;
    }
    this.valuechange(event, orderBy);

  }
  stringToNumber(str: any) {
    if (typeof str == 'number') {
      str = str + '';
    }

    let s = str.toLowerCase().replace(/[^0-9]/g, '');

    return parseInt(s);
  }
  valuechange(event, input_name, download?:boolean) {

    const searchCallback = this.searchCallback;
    // console.log(searchCallback)
    if (this.onSearchActive == false) {
        this.onSearchActive = true;
        this.showLoading = true;
        let myVar = setTimeout(async () => {
          
          let clearFormInput:any = JSON.stringify(this.form_input);
          console.warn("clearFormInput", clearFormInput);
          clearFormInput = JSON.parse(clearFormInput);
          console.warn("clearFormInput parsed", clearFormInput);
          if( clearFormInput.unique_amount){ 
            clearFormInput.unique_amount = this.stringToNumber(clearFormInput.unique_amount)
          }
          if( clearFormInput.total_price){
            clearFormInput.total_price = this.stringToNumber(clearFormInput.total_price)
          }
          if( clearFormInput.po_no){
            Object.defineProperty(clearFormInput, 'po_no.value',
              Object.getOwnPropertyDescriptor(clearFormInput, 'po_no'));
            delete clearFormInput['po_no'];
          }
          if( clearFormInput.member_detail){
            Object.defineProperty(clearFormInput, 'member_detail.nama_penerima_gopay',
              Object.getOwnPropertyDescriptor(clearFormInput, 'member_detail'));
            delete clearFormInput['member_detail'];
          }
          if (searchCallback) { 
            let filter = {
              search: clearFormInput, 
              order_by: this.order,
              limit_per_page: this.pageLimits,
              current_page: this.currentPage,
              download: false
             }
            //  console.log(filter)
            if(download != undefined && download == true) {
              // console.log('here1')
              filter.limit_per_page = 0;
              filter = {...filter, ...{download:false}}
            }

            for(var property in filter.search) {
              /** check for type string */
              if(typeof filter.search[property] == 'string' && filter.search[property].trim() == '') {
                delete filter.search[property]
              }

              /** check for number */
              if(typeof filter.search[property] == 'object' && filter.search[property].option){
                if(filter.search[property].value == undefined && filter.search[property].from == undefined) {
                  delete filter.search[property]
                }
                
                else if(filter.search[property].value != undefined ) {

                  if(filter.search[property].value.trim() == '')
                    {

                      delete filter.search[property]
                    }
                }

                else if(filter.search[property].from && filter.search[property].from.trim() == '') {
                  delete filter.search[property]
                }
              }
            }

            if(this.direct_download){
              if(download != undefined && download == true) {
                this.downloadFile(this.direct_download);
              }
              this.onSearchActive = false;
              this.showLoading = false;
              return;
            }

            let result = await searchCallback[0][searchCallback[1]](filter);
            if(download != undefined && download == true) {
              if (result && result.values) {
              this.download_temp = result.values;
              this.downloadFile(this.download_temp);
              } else if (!result.values) {
                this.download_temp = result;
                this.downloadFile(this.download_temp);
              }
              else{
                this.download_temp = result;
                this.downloadFile(this.download_temp);
              }
              // console.log('here',this.download_temp)
            }
            if (result && filter.download != true && download != true ) {
              if(result && result.total_page){
                this.buildPagesNumbers(result.total_page);
              } else {
                this.buildPagesNumbers(1);
              }
              if (searchCallback[2]) {
                // console.warn("result", result);
                if (result && result.values) {
                  searchCallback[2][this.options.result_var_name] = result.values;
                }
                else if (!result.values) {
                  searchCallback[2][this.options.result_var_name] = result;
                }
                else {
                  searchCallback[2][this.options.result_var_name] = result;
                }
                /* for(let data of this.table_data) {
                  if(this.list_data.includes(data.member_id)) {
                    data.selected = true;
                  }
                } */
              } else {
                
                if(result.values){
                    this.table_data = result.values;
                } else{
                  this.table_data = result;

                }
                /* for(let data of this.table_data) {
                  if(this.list_data.includes(data.member_id)) {
                    data.selected = true;
                  }
                } */
              }
            } else if(result && filter.download == true){
              if(typeof result.result == 'string'){
                let srcDownload=getHost()+'.'+'/'+result.result;
                window.open(srcDownload, "_blank");
                // this.srcDownload = '';
                // setTimeout(() => {
                //   this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl(srcDownload);
                // }, 200);
              }
              
            
            }
          }
          this.onSearchActive = false;
          this.showLoading = false;
      }, 2000);
    }

  }

  async submit() {
    //this.list_data = this.table_data.filter(list => list.selected == true);
    let result = await this.options.submit_function[0][this.options.submit_function[1]](this.list_data);
  }

  toggleCheck(event, member_id) {
    if (event.target.checked) {
      if (!this.list_data.includes(member_id)) {
        this.list_data.push(member_id);
      }
    }
    else if (!event.target.checked) {
      if (this.list_data.includes(member_id)) {
        this.list_data.splice(this.list_data.indexOf(member_id), 1);
      }
    }
  }

  removeFromList(member_id) {
    this.list_data.splice(this.list_data.indexOf(member_id), 1);
    let index = this.table_data.findIndex(obj => obj.member_id == member_id);
    if (index > -1) {
      this.table_data[index].selected = false;
    }
  }

  validURL(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
      '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
    return !!pattern.test(str);
  }

  async clickedRowTable(data, rowData) {
    // console.warn("clickrow", data, rowData);
    // console.log("data",rowData)
    // console.log("options", this.options)
    // console.log(await this.options.detail_function[0][this.options.detail_function[1]](data));
    if (!this.options.check_box)
     {
      let result = await this.options.detail_function[0][this.options.detail_function[1]](data, rowData);
    }
    // if (this.options.check_box){
    //   console.log("HRE CATCH", rowData)
    // }
  }

  isString(data) {
    return typeof data == 'string';
  }
  include(data){
    if(data != null){
      var x = data.toString()
    return x.includes('-')
    }else {
      var y = '0'
      return y.includes('-')
    }
  }

  stripHtml(value: string) {
    if (typeof value == 'string')
      return value.replace(/<.*?>/g, ''); // replace tags
    return value;
  }

  async onClickDownload($event) {
    // this.valuechange($event, false, true);
    // console.log('RESPONSE', this.download_temp);
   
    try {
      await this.valuechange($event, false, true);
      
    }catch (e){
      console.log(e)
    }
    // console.log(' download_temp' ,this.download_temp)
    
  }
  downloadFile(data, filename='data') {
    var label = []
    var key = []
    this.table_head.forEach((a, index) => {
      label.push(this.table_head[index].label)
      key.push(this.table_head[index].data_row_name)
    })
    
    let csvData = this.ConvertToCSV(data, label, key);
    const newCSV : String = csvData.replace(/"+/g,''); 
    const arrayCSV:  Array<String> = newCSV.split('\r\n');
    const aoa : Array<Array<String>> = [];
    for(let i = 0; i < arrayCSV.length; i++){
      aoa.push(arrayCSV[i].split(';'));
    }
    const ws: XLSX.WorkSheet =XLSX.utils.aoa_to_sheet(aoa);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    let fileName = this.table_title.replace(/\s+/g,'-');
    const date = Date().split(' ');
    const fileDate = '-' + date[2] + '-' + date[1] + '-' + date[3] + '-' + date[4];

    /* save to file */
    XLSX.writeFile(wb, fileName + fileDate + '.xlsx');

    // let blob = new Blob(['\ufeff' + csvData], { type: 'text/csv;charset=utf-8;' });
    // let dwldLink = document.createElement("a");
    // let url = URL.createObjectURL(blob);
    // let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
    // if (isSafariBrowser) {  //if Safari open in new window to save file with random filename.
    //     dwldLink.setAttribute("target", "_blank");
    // }
    // dwldLink.setAttribute("href", url);
    // dwldLink.setAttribute("download", filename + ".csv");
    // dwldLink.style.visibility = "hidden";
    // document.body.appendChild(dwldLink);
    // dwldLink.click();
    // document.body.removeChild(dwldLink);
}

ConvertToCSV(objArray, labelHeader ,headerList ) {
     let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
     let str = '';
     let row = 'No;';

     for (let index in labelHeader) {
         row += labelHeader[index] + ';';
     }
     row = row.slice(0, -1);
     str += row + '\r\n';
     // 
     for (let i = 0; i < array.length; i++) {
         let line = (i+1)+'';
         for (let index in headerList) {
            let head = headerList[index];
            // console.warn("HEAD", head)
            
            let headtype = this.table_head[index].type
            
            // console.warn('HEADTYPE',headtype, 'HEAD', head)
             if((array[i][head]) instanceof Array && (array[i][head]) != null && head == 'products'){
               let line2 = ''
               if(headtype == 'productcode'){
                if ((array[i][head])[0].product_code){
                  if ((array[i][head]).length == 1){
                    line2 += (array[i][head])[0].product_code +' '
                   }else {
                    for (let j = 0; j < (array[i][head]).length-1; j++) {              
                      line2 += (array[i][head])[j].product_code + ' , '
                    }
                   } 
                }
                if ((array[i][head])[0].item_code){
                  if ((array[i][head]).length == 1){
                    line2 += (array[i][head])[0].item_code +' '
                   }else {
                    for (let j = 0; j < (array[i][head]).length-1; j++) {              
                      line2 += (array[i][head])[j].item_code + ' , '
                    }
                   } 
                }       
                line += ';' + JSON.stringify(line2).replace(/;/g,' ') + ' '

               }else if(headtype == 'product_code'){
              
                  if ((array[i][head]).length == 1){
                    line2 += (array[i][head])[0].product_code + ' '
                  }else {
                    for (let j = 0; j < (array[i][head]).length; j++) {              
                      line2 += (array[i][head])[j].product_code + ', '
                    }
                  }  
                  line += ';' + JSON.stringify(line2).replace(/;/g,' ') + ' '
                  line2 = null
              
               }else if(headtype == 'productweight'){
            
                if ((array[i][head]).length == 1){
                  line2 += (array[i][head])[0].weight +'(' + (array[i][head])[0].weight * 1000 + 'gram )' +' '
                 }else {
                  for (let j = 0; j < (array[i][head]).length-1; j++) {              
                    line2 += (array[i][head])[j].weight +'(' + (array[i][head])[0].weight * 1000 + 'gram )' +' , '
                  }
                 }  
                line += ';' + JSON.stringify(line2).replace(/;/g,' ') + ' '
                line2 = null
               }else if(headtype == 'productdesc'){
            
                if ((array[i][head]).length == 1){
                  line2 += (array[i][head])[0].description +' '
                 }else {
                  for (let j = 0; j < (array[i][head]).length-1; j++) {              
                    line2 += (array[i][head])[j].description + ' , '
                  }
                 }  
                line += ';' + JSON.stringify(line2).replace(/;/g,' ') + ' '
                line2 = null
               }else if(headtype == 'productname'){
            
                if ((array[i][head]).length == 1){
                  line2 += (array[i][head])[0].product_name +' '
                 }else {
                  for (let j = 0; j < (array[i][head]).length-1; j++) {              
                    line2 += (array[i][head])[j].product_name + ' , '
                  }
                 }  
                line += ';' + JSON.stringify(line2).replace(/;/g,' ') + ' '
                line2 = null
              }else if(headtype == 'product_name'){
            
                if ((array[i][head]).length == 1){
                  line2 += (array[i][head])[0].product_name + '(' + (array[i][head])[0].quantity + ') ' + ' '
                 }else {
                  for (let j = 0; j < (array[i][head]).length; j++) {              
                    line2 += (array[i][head])[j].product_name + '(' + (array[i][head])[j].quantity + ') ' + ', '
                  }
                 }  
                line += ';' + JSON.stringify(line2).replace(/;/g,' ') + ' '
                line2 = null
               }else if(headtype == 'productcat'){
                if((array[i][head])[0].categories ){
                if ((array[i][head]).length == 1){
                  line2 += (array[i][head])[0].categories +' '
                 }else {
                  for (let j = 0; j < (array[i][head]).length-1; j++) {              
                    line2 += (array[i][head])[j].categories + ' , '
                  }
                 }  
                 }
                 if((array[i][head])[0].category ){
                  if ((array[i][head]).length == 1){
                    line2 += (array[i][head])[0].category +' '
                   }else {
                    for (let j = 0; j < (array[i][head]).length-1; j++) {              
                      line2 += (array[i][head])[j].category + ' , '
                    }
                   }  
                   }
                line += ';' + JSON.stringify(line2).replace(/;/g,' ') + ' '
                line2 = null
               }else if(headtype == 'productmerc'){
              
                if ((array[i][head]).length == 1){
                  line2 += (array[i][head])[0].merchant_username +' '
                 }else {
                  for (let j = 0; j < (array[i][head]).length-1; j++) {              
                    line2 += (array[i][head])[j].merchant_username + ' , '
                  }
                 }  
                line += ';' + JSON.stringify(line2).replace(/;/g,' ') + ' '
                line2 = null
               }else if(headtype == 'productqty') {
                let qty = 0
                if ((array[i][head]).length == 1){
                  qty += 1
                 }else {
                  for (let j = 0; j < (array[i][head]).length-1; j++) {   
                    qty += this.stringToNumber((array[i][head])[j].quantity)
                  }
                 }  
                line2 = qty.toString()
                line += ';' + JSON.stringify(line2).replace(/;/g,' ') + ' '
                line2 = null
              }else if(headtype == 'quantity') {
                let qty = 0
                if ((array[i][head]).length == 1){
                  qty += (array[i][head])[0].quantity
                 }else {
                  for (let j = 0; j < (array[i][head]).length; j++) {
                    qty += this.stringToNumber((array[i][head])[j].quantity)
                  }
                 }  
                line2 = qty.toString()
                line += ';' + JSON.stringify(line2).replace(/;/g,' ') + ' '
                line2 = null
               }else if(headtype == 'productdim'){
                  // for (let j = 0; j < (array[i][head]).length-1; j++) {      
                    line2 += 'height:' + (array[i][head])[0].dimensions.height + ' length :' + (array[i][head])[0].dimensions.length + ' width:' + (array[i][head])[0].dimensions.width +', '        
                    // line2 += (array[i][head])[j].product_name +' '+ '(' + (array[i][head])[j].fixed_price + ' ) ' +','
                  // }
                  
               
                line += ';'  + JSON.stringify(line2).replace(/;/g,' ') + ' '
                line2 = null
               }else if(headtype == 'productunit'){
            
                if ((array[i][head]).length == 1){
                  line2 += (array[i][head])[0].product_name +' '+ '('+(array[i][head])[0].fixed_price +' )'
                 }else {
                  for (let j = 0; j < (array[i][head]).length-1; j++) {              
                    line2 += (array[i][head])[j].product_name +' '+ '(' + (array[i][head])[j].fixed_price + ' ) ' +','
                  }
                 }  
                line += ';'  + JSON.stringify(line2).replace(/;/g,' ') + ' '
                line2 = null
               }else if(headtype == 'producttotal'){
              
                if ((array[i][head]).length == 1){
                  line2 += (array[i][head])[0].product_name +' '+ '('+ (array[i][head])[0].total_product_price +' )'
                 }else {
                  for (let j = 0; j < (array[i][head]).length-1; j++) {              
                    line2 += (array[i][head])[0].product_name +' '+ '('+(array[i][head])[j].total_product_price + ') '
                  }
                 }  
                line += ';' + JSON.stringify(line2).replace(/;/g,' ') + ' '
               
               }else if(headtype == 'product'){
                if ((array[i][head]).length == 1){
                  line2 += (array[i][head])[0].product_name +' '+ '(' + ' '+(array[i][head])[0].total_product_price +' '+ ')' + ' '+ (array[i][head])[0].quantity + ' '+ 'X'
                 }else {
                  for (let j = 0; j < (array[i][head]).length-1; j++) {              
                    line2 += (array[i][head])[j].product_name +' '+ '(' + ' '+(array[i][head])[j].total_product_price +' '+ ')' + ' '+ (array[i][head])[j].quantity + ' '+ 'X'
                  }
                 }   
                 line += ';' +' ' + JSON.stringify(line2).replace(/;/g,' ') + ' '
               }
           
             }

            //  Shipping Info
            else if((array[i][head]) instanceof Array && (array[i][head]) != null && head == 'shipping_info'){
              let line2 = ''
              if(headtype == 'on_process_date'){
               if ((array[i][head])[0].created_date){
                 if ((array[i][head]).length == 1){
                   line2 += (array[i][head])[0].created_date +' '
                  }else {
                   for (let j = 0; j < (array[i][head]).length-1; j++) {              
                     line2 += (array[i][head])[j].created_date + ' , '
                   }
                  } 
               }    
               line += ';' + JSON.stringify(line2).replace(/;/g,' ') + ' '

              }else if(headtype == 'on_delivery_date'){

                if ((array[i][head]).length == 1){
                  // line2 += (array[i][head])[0].product_code + ' '
                  line2 = "-";
                }else {
                  for (let j = 0; j < (array[i][head]).length; j++) {   
                    if ((array[i][head])[j].label == "on_delivery") {
                      line2 += (array[i][head])[j].created_date
                    }           
                  }
                }  
                line += ';' + JSON.stringify(line2).replace(/;/g,' ') + ' '
                console.warn("line", line)
                line2 = null
               }

              //  else if(headtype == 'input_awb_date'){

              //   if ((array[i][head]).length == 1){
              //     // line2 += (array[i][head])[0].product_code + ' '
              //     line2 = "-";
              //   }else {
              //     for (let j = 0; j < (array[i][head]).length; j++) {   
              //       if ((array[i][head])[j].label == "on_delivery" && (array[i][head])[j].updated_date) {
              //         line2 += (array[i][head])[j].updated_date
              //       } else {
              //         line2 = "-"
              //       }         
              //     }
              //   }  
              //   line += ';' + JSON.stringify(line2).replace(/;/g,' ') + ' '
              //   console.warn("line", line)
              //   line2 = null
              //  }
            }

            //  start array of member address
             else if((array[i][head]) instanceof Array && (array[i][head]) != null && head == 'member_address'){
              let line2 = ''
              if(headtype == 'penerima_hadiah'){
             
                 if ((array[i][head]).length == 1){
                   line2 += (array[i][head])[0].name + ' '
                 }else {
                   for (let j = 0; j < (array[i][head]).length; j++) {              
                     line2 += (array[i][head])[j].name + ', '
                   }
                 }  
                 line += ';' + JSON.stringify(line2).replace(/;/g,' ') + ' '
                 line2 = null
             
              } else if(headtype == 'telp_penerima'){
             
                if ((array[i][head]).length == 1){
                  line2 += (array[i][head])[0].cell_phone + ' '
                }else {
                  for (let j = 0; j < (array[i][head]).length; j++) {              
                    line2 += (array[i][head])[j].cell_phone + ', '
                  }
                }  
                line += ';' + JSON.stringify(line2).replace(/;/g,' ') + ' '
                line2 = null
             }
              
              else if(headtype == 'productweight'){
           
               if ((array[i][head]).length == 1){
                 line2 += (array[i][head])[0].weight +'(' + (array[i][head])[0].weight * 1000 + 'gram )' +' '
                }else {
                 for (let j = 0; j < (array[i][head]).length-1; j++) {              
                   line2 += (array[i][head])[j].weight +'(' + (array[i][head])[0].weight * 1000 + 'gram )' +' , '
                 }
                }  
               line += ';' + JSON.stringify(line2).replace(/;/g,' ') + ' '
               line2 = null
              }else if(headtype == 'productdesc'){
           
               if ((array[i][head]).length == 1){
                 line2 += (array[i][head])[0].description +' '
                }else {
                 for (let j = 0; j < (array[i][head]).length-1; j++) {              
                   line2 += (array[i][head])[j].description + ' , '
                 }
                }  
               line += ';' + JSON.stringify(line2).replace(/;/g,' ') + ' '
               line2 = null
              }else if(headtype == 'productname'){
           
               if ((array[i][head]).length == 1){
                 line2 += (array[i][head])[0].product_name +' '
                }else {
                 for (let j = 0; j < (array[i][head]).length-1; j++) {              
                   line2 += (array[i][head])[j].product_name + ' , '
                 }
                }  
               line += ';' + JSON.stringify(line2).replace(/;/g,' ') + ' '
               line2 = null
             }else if(headtype == 'product_name'){
           
               if ((array[i][head]).length == 1){
                 line2 += (array[i][head])[0].product_name + '(' + (array[i][head])[0].quantity + ') ' + ' '
                }else {
                 for (let j = 0; j < (array[i][head]).length; j++) {              
                   line2 += (array[i][head])[j].product_name + '(' + (array[i][head])[j].quantity + ') ' + ', '
                 }
                }  
               line += ';' + JSON.stringify(line2).replace(/;/g,' ') + ' '
               line2 = null
              }else if(headtype == 'productcat'){
               if((array[i][head])[0].categories ){
               if ((array[i][head]).length == 1){
                 line2 += (array[i][head])[0].categories +' '
                }else {
                 for (let j = 0; j < (array[i][head]).length-1; j++) {              
                   line2 += (array[i][head])[j].categories + ' , '
                 }
                }  
                }
                if((array[i][head])[0].category ){
                 if ((array[i][head]).length == 1){
                   line2 += (array[i][head])[0].category +' '
                  }else {
                   for (let j = 0; j < (array[i][head]).length-1; j++) {              
                     line2 += (array[i][head])[j].category + ' , '
                   }
                  }  
                  }
               line += ';' + JSON.stringify(line2).replace(/;/g,' ') + ' '
               line2 = null
              }else if(headtype == 'productmerc'){
             
               if ((array[i][head]).length == 1){
                 line2 += (array[i][head])[0].merchant_username +' '
                }else {
                 for (let j = 0; j < (array[i][head]).length-1; j++) {              
                   line2 += (array[i][head])[j].merchant_username + ' , '
                 }
                }  
               line += ';' + JSON.stringify(line2).replace(/;/g,' ') + ' '
               line2 = null
              }else if(headtype == 'productqty') {
               let qty = 0
               if ((array[i][head]).length == 1){
                 qty += 1
                }else {
                 for (let j = 0; j < (array[i][head]).length-1; j++) {   
                   qty += this.stringToNumber((array[i][head])[j].quantity)
                 }
                }  
               line2 = qty.toString()
               line += ';' + JSON.stringify(line2).replace(/;/g,' ') + ' '
               line2 = null
             }else if(headtype == 'quantity') {
               let qty = 0
               if ((array[i][head]).length == 1){
                 qty += (array[i][head])[0].quantity
                }else {
                 for (let j = 0; j < (array[i][head]).length; j++) {
                   qty += this.stringToNumber((array[i][head])[j].quantity)
                 }
                }  
               line2 = qty.toString()
               line += ';' + JSON.stringify(line2).replace(/;/g,' ') + ' '
               line2 = null
              }else if(headtype == 'productdim'){
                 // for (let j = 0; j < (array[i][head]).length-1; j++) {      
                   line2 += 'height:' + (array[i][head])[0].dimensions.height + ' length :' + (array[i][head])[0].dimensions.length + ' width:' + (array[i][head])[0].dimensions.width +', '        
                   // line2 += (array[i][head])[j].product_name +' '+ '(' + (array[i][head])[j].fixed_price + ' ) ' +','
                 // }
                 
              
               line += ';'  + JSON.stringify(line2).replace(/;/g,' ') + ' '
               line2 = null
              }else if(headtype == 'productunit'){
           
               if ((array[i][head]).length == 1){
                 line2 += (array[i][head])[0].product_name +' '+ '('+(array[i][head])[0].fixed_price +' )'
                }else {
                 for (let j = 0; j < (array[i][head]).length-1; j++) {              
                   line2 += (array[i][head])[j].product_name +' '+ '(' + (array[i][head])[j].fixed_price + ' ) ' +','
                 }
                }  
               line += ';'  + JSON.stringify(line2).replace(/;/g,' ') + ' '
               line2 = null
              }else if(headtype == 'producttotal'){
             
               if ((array[i][head]).length == 1){
                 line2 += (array[i][head])[0].product_name +' '+ '('+ (array[i][head])[0].total_product_price +' )'
                }else {
                 for (let j = 0; j < (array[i][head]).length-1; j++) {              
                   line2 += (array[i][head])[0].product_name +' '+ '('+(array[i][head])[j].total_product_price + ') '
                 }
                }  
               line += ';' + JSON.stringify(line2).replace(/;/g,' ') + ' '
              
              }else if(headtype == 'product'){
               if ((array[i][head]).length == 1){
                 line2 += (array[i][head])[0].product_name +' '+ '(' + ' '+(array[i][head])[0].total_product_price +' '+ ')' + ' '+ (array[i][head])[0].quantity + ' '+ 'X'
                }else {
                 for (let j = 0; j < (array[i][head]).length-1; j++) {              
                   line2 += (array[i][head])[j].product_name +' '+ '(' + ' '+(array[i][head])[j].total_product_price +' '+ ')' + ' '+ (array[i][head])[j].quantity + ' '+ 'X'
                 }
                }   
                line += ';' +' ' + JSON.stringify(line2).replace(/;/g,' ') + ' '
              }
          
            }
            // end of member address
             else if(head == 'description'){
         
              let line2
              if((array[i][head])){
                 line2 = JSON.stringify(array[i][head]).replace(/;/g,' ')
              }
              
              line += ';' + line2;

              //start of input form data
            } else if(head == 'input_form_data' || head == 'member_detail'){
               let line2
               if(headtype == 'form_nama_penerima'){
                
                if((array[i][head])){
                   line2 = (array[i][head]).nama_penerima_gopay
                  
                }         
               } else if(headtype == 'form_group'){
                
                if((array[i][head])){
                   line2 = (array[i][head]).description
                  
                }         
               } else if(headtype == 'form_cluster'){
                
                if((array[i][head])){
                   line2 = (array[i][head]).cluster
                  
                }         
               } else if(headtype == 'form_group_mci'){
                
                if((array[i][head])){
                   line2 = (array[i][head]).description_mci
                  
                }         
               }else if(headtype == 'form_nama_toko'){
                
                if((array[i][head])){
                   line2 = (array[i][head]).nama_toko
                  
                }         
               } else if(headtype == 'form_nama_distributor'){
                
                if((array[i][head])){
                   line2 = (array[i][head]).nama_distributor
                  
                }         
               } else if(headtype == 'form_alamat_toko'){
                
                if((array[i][head])){
                   line2 = (array[i][head]).alamat_toko
                  
                }         
               } else if(headtype == 'form_nama_pemilik'){
                
                if((array[i][head])){
                   line2 = (array[i][head]).nama_pemilik
                  
                }         
               } else if(headtype == 'form_business_id'){
                
                if((array[i][head])){
                   line2 = (array[i][head]).business_id
                  
                }         
               } else if(headtype == 'form_id_pel'){
                
                if((array[i][head])){
                   line2 = (array[i][head]).id_pel
                  
                }         
               }else if(headtype == 'form_ktp_pemilik'){
                
                if((array[i][head])){
                   line2 = (array[i][head]).ktp_pemilik
                  
                }         
               } else if(headtype == 'form_foto_ktp_pemilik'){
                
                if((array[i][head])){
                   line2 = (array[i][head]).foto_ktp_pemilik
                  
                }         
               } else if(headtype == 'form_npwp_pemilik'){
                
                if((array[i][head])){
                   line2 = (array[i][head]).npwp_pemilik
                  
                }         
               } else if(headtype == 'form_foto_npwp_pemilik'){
                
                if((array[i][head])){
                   line2 = (array[i][head]).foto_npwp_pemilik
                  
                }         
               } else if(headtype == 'form_telp_pemilik'){
                
                if((array[i][head])){
                   line2 = (array[i][head]).telp_pemilik
                  
                }         
               } else if(headtype == 'form_wa_pemilik'){
                
                if((array[i][head])){
                   line2 = (array[i][head]).no_wa_pemilik
                  
                }         
               } else if(headtype == 'form_email_pelanggan'){
                
                if((array[i][head])){
                   line2 = (array[i][head]).email
                  
                }         
               } else if(headtype == 'form_alamat_penerima'){
                
                if((array[i][head])){
                   line2 = (array[i][head]).alamat_rumah
                  
                }         
               } else if(headtype == 'form_hadiah_dikuasakan'){
                
                if((array[i][head])){
                   line2 = (array[i][head]).hadiah_dikuasakan
                  
                }         
               } else if(headtype == 'form_ktp_penerima'){
                
                if((array[i][head])){
                   line2 = (array[i][head]).ktp_penerima
                  
                }         
               } else if(headtype == 'form_foto_ktp_penerima'){
                
                if((array[i][head])){
                   line2 = (array[i][head]).foto_ktp_penerima
                  
                }         
               } else if(headtype == 'form_npwp_penerima'){
                
                if((array[i][head])){
                   line2 = (array[i][head]).npwp_penerima
                  
                }         
               } else if(headtype == 'form_foto_npwp_penerima'){
                
                if((array[i][head])){
                   line2 = (array[i][head]).foto_npwp_penerima
                  
                }         
               } else if(headtype == 'form_wa_penerima'){
                
                if((array[i][head])){
                   line2 = (array[i][head]).no_wa_penerima
                  
                }         
               }else if(headtype == 'form_gopay_penerima'){
                
                if((array[i][head])){
                   line2 = (array[i][head]).telp_penerima_gopay
                  
                }         
               }

               line2 = line2 === undefined ?  '-': line2
               line += ';' + JSON.stringify(line2).replace(/;/g,' ');
            } 

            
            else if(head == 'additional_info'){
              let line2
              if(headtype == 'bast'){
               
               if((array[i][head]) && (array[i][head]).bast && (array[i][head]).bast.pic_file_name){
                  line2 = (array[i][head]).bast.pic_file_name
               } else {
                 line2 = "-";
               }        
              } else if(headtype == 'surat_kuasa'){
               
               if((array[i][head]) && (array[i][head]).surat_kuasa && (array[i][head]).surat_kuasa.pic_file_name){
                  line2 = (array[i][head]).surat_kuasa.pic_file_name
               } else {
                line2 = "-";
               }  
              } else if(headtype == 'bast_date'){
               
                if((array[i][head]) && (array[i][head]).bast && (array[i][head]).bast.created_date){
                   line2 = (array[i][head]).bast.created_date
                } else {
                 line2 = "-";
                }  
              } else if(headtype == 'invoice_no'){
               
                if((array[i][head]) && (array[i][head]).invoice_no){
                   line2 = (array[i][head]).invoice_no
                } else {
                  line2 = "-";
                }  
               } else if(headtype == 'data_complete'){
                if((array[i][head])){
                   line2 = (array[i][head]).data_complete;
                }         
              }

              line2 = line2 === undefined ?  '-': line2
              line += ';' + JSON.stringify(line2).replace(/;/g,' ');
           }
          // delivery_detail
          else if(head == 'delivery_detail'){
          let line2
          if(headtype == 'awb_number'){
           
           if((array[i][head]) && (array[i][head]).awb_number){
              line2 = (array[i][head]).awb_number
           } else {
             line2 = "-";
           }        
          } else if(headtype == 'delivered_date'){
           
            if((array[i][head]) && (array[i][head]).delivered_date){
               line2 = (array[i][head]).delivered_date
            } else {
              line2 = "-";
            }        
          } else if(headtype == 'delivery_date'){
           
            if((array[i][head]) && (array[i][head]).delivery_date){
               line2 = (array[i][head]).delivery_date
            } else {
              line2 = "-";
            }        
          } else if(headtype == 'last_shipping_info'){
           
            if((array[i][head]) && (array[i][head]).last_shipping_info){
               line2 = (array[i][head]).last_shipping_info
            } else {
              line2 = "-";
            }        
          } else if(headtype == 'receiver_name_sap'){
           
            if((array[i][head]) && (array[i][head]).receiver_name){
               line2 = (array[i][head]).receiver_name
            } else {
              line2 = "-";
            }        
          } else if(headtype == 'relation_name'){
           
            if((array[i][head]) && (array[i][head]).relation_name){
               line2 = (array[i][head]).relation_name
            } else {
              line2 = "-";
            }        
          }

          line2 = line2 === undefined ?  '-': line2
          line += ';' + JSON.stringify(line2).replace(/;/g,' ');
       }
       //address detail
       else if(head == 'destination'){
        let line2
        if(headtype == 'receiver_name'){
         
         if((array[i][head]) && (array[i][head]).name){
            line2 = (array[i][head]).name
         } else {
           line2 = "-";
         }        
        }
        else if(headtype == 'shipping_address'){
         
          if((array[i][head]) && (array[i][head]).address){
             line2 = (array[i][head]).address
          } else {
            line2 = "-";
          }        
         }
         else if(headtype == 'phone_number'){
         
          if((array[i][head]) && (array[i][head]).cell_phone){
             line2 = (array[i][head]).cell_phone;
          } else {
            line2 = "-";
          }        
         } 

        line2 = line2 === undefined ?  '-': line2
        line += ';' + JSON.stringify(line2).replace(/;/g,' ');
     }
     

          //  Invoice Number
           else if(head == 'invoice_no'){
            let line2
            if(headtype == 'value_invoice'){
             
             if((array[i][head]) && (array[i][head]).value){
                line2 = (array[i][head]).value
             } else {
               line2 = "-";
             }        
            } else if(headtype == 'date_invoice'){
             
             if((array[i][head]) && (array[i][head]).created_date){
                line2 = (array[i][head]).created_date
             } else {
              line2 = "-";
             }          
            }

            line2 = line2 === undefined ?  '-': line2
            line += ';' + JSON.stringify(line2).replace(/;/g,' ');
         }

         else if(head == 'reference_no'){
          let line2
          if(headtype == 'reference_no_length'){   
            if((array[i][head])){
              line2 = (array[i][head]).constructor === Array ? (array[i][head]).length : 0
            } else {
              line2 = 0;
            }     
          } 

          line2 = line2 === undefined ?  '-': line2
          line += ';' + JSON.stringify(line2).replace(/;/g,' ');
       }

         else if(head == 'po_no'){
          let line2
          if(headtype == 'value_po'){
           
           if((array[i][head]) && (array[i][head]).value){
              line2 = (array[i][head]).value
           } else {
             line2 = "-";
           }        
          }

          line2 = line2 === undefined ?  '-': line2
          line += ';' + JSON.stringify(line2).replace(/;/g,' ');
       }
            
            else if(head == 'autoform'){
              
              let line2
              if(headtype == 'penerima'){
               
               if((array[i][head]) && (array[i][head]).values){
                  line2 = (array[i][head]).values.nama_penerima_gopay
                 
               }         
              }else if(headtype == 'pemilik'){
               
               if((array[i][head]) && (array[i][head]).values){
                  line2 = (array[i][head]).values.nama_pemilik
                 
               }         
              }
              else if(headtype == 'nama_toko'){
               
               if((array[i][head]) && (array[i][head]).values){
                
                 line2 = (array[i][head]).values.nama_toko
                
              }             
              }
              else if(headtype == 'alamat_kirim'){
      
               if((array[i][head]) && (array[i][head]).values){
               
                  line2 = (array[i][head]).values.alamat_rumah
           
               }         
              } else if(headtype == 'id_toko'){
     
               if((array[i][head]) && (array[i][head]).values){
               
                  line2 = (array[i][head]).values.id_pel
          
               }         
              } else if(headtype == 'dikuasakan'){
     
               if((array[i][head]) && (array[i][head]).values){
               
                  line2 = (array[i][head]).values.hadiah_dikuasakan
            
               }         
              } else if(headtype == 'no_wa'){
               
               if((array[i][head]) && (array[i][head]).values){
               
                  line2 = "'"+(array[i][head]).values.no_wa_penerima
            
               }         
              }
              else if(headtype == 'member_desc'){
            
               if((array[i][head]) && (array[i][head]).values){
                 
                  line2 = (array[i][head]).values.description
               
               }         
              }
              line2 = line2 === undefined ?  '-': line2
              line += ';' + JSON.stringify(line2).replace(/;/g,' ');
            }
            //  end of input form data
            
            //Points
            else if(head == 'points'){
              
              let line2
              if(headtype == 'point_balance'){
               
               if((array[i][head])){
                  line2 = (array[i][head]).point_balance
                 
               }         
              }else if(headtype == 'expire_date'){
               
               if((array[i][head])){
                  line2 = (array[i][head]).expire_date
                 
               }         
              }
              
              line2 = line2 === undefined ?  '-': line2
              line += ';' + JSON.stringify(line2).replace(/;/g,' ');
           }
            else if (head=="access_list") {
              let line2 = "";
              if(headtype == 'usr_mngt'){
                for (const key in (array[i][head])) {
                  line2 += key + " : " + (array[i][head])[key] + "  ";
                }
              //   if((array[i][head]) && (array[i][head]).usr_mngt){
              //      line2 += "User Management : " + (array[i][head]).usr_mngt + "  ";
              //   } 

              //   if((array[i][head]) && (array[i][head]).dynamix_sbi){
              //     line2 += "Dynamix SBI : " + (array[i][head]).dynamix_sbi;
              //  } 
              }
              line2 = line2 === undefined ?  '-': line2.replace(/\|/g, ', ');
              line += ';' + JSON.stringify(line2).replace(/;/g,' ');
            }

            else if (head=="variation") {
              let line2 = ``;
              if(headtype == 'variation'){
                if((array[i][head])){
                  for (const property in (array[i][head])) {
                    line2 += `${property}: ${(array[i][head])[property]} | `
                  }
                } 
              }
              line2 = line2 === undefined ?  '-': line2;
              line += ';' + JSON.stringify(line2).replace(/;/g,' ');
            }

            else if (head=="dimensions") {
              let line2 = ``;
              if(headtype == 'dimensions'){
                if((array[i][head])){
                  for (const property in (array[i][head])) {
                    line2 += `${property}: ${(array[i][head])[property]} | `
                  }
                } 
              }
              line2 = line2 === undefined ?  '-': line2
              line += ';' + JSON.stringify(line2).replace(/;/g,' ');
            }

            else if (head=="images_gallery") {
              let line2;
              if(headtype == 'images_gallery'){
                if((array[i][head])){
                  line2 = array[i][head][0].base_url + array[i][head][0].pic_file_name;
                } 
              }
              line2 = line2 === undefined ?  '-': line2
              line += ';' + JSON.stringify(line2).replace(/;/g,' ');
            }

            // IF ROW IS PRODUCTS
            else if(head == 'products' || head == 'product_list'){
              let line2
              if(headtype == 'quantity'){
               
               if((array[i][head]) && (array[i][head]).values){
                  line2 = (array[i][head]).values.quantity
                 
               }         
              } else if(headtype == 'product_quantity'){
                
                if((array[i][head])){
                   line2 = (array[i][head]).quantity
                  
                }         
              } else if(headtype == 'product_redeem'){
                
                if((array[i][head])){
                   line2 = (array[i][head]).product_name
                  
                }         
              } else if(headtype == 'product_code_redeem'){
                
                if((array[i][head])){
                   line2 = (array[i][head]).product_code
                  
                }         
              }else if(headtype == 'product_price_redeem'){
                
                if((array[i][head])){
                   line2 = (array[i][head]).total_product_price
                  
                }         
              }
              // else if(headtype == 'product_code'){
               
              //  if((array[i][head]) && (array[i][head]).values){
              //     line2 = (array[i][head]).values.product_code
                 
              //  }         
              // }
              line2 = line2 === undefined ?  '-': line2
              line += ';' + JSON.stringify(line2).replace(/;/g,' ');
           }
             else{
           
              line += ';' + JSON.stringify(array[i][head])
             }
         }
         str += line + '\r\n';
     }
     return str;
 }

//  start of input form data
form_id_pel(params){
  let form_id_pel = ''
  if(params)
    form_id_pel = params.id_pel
  
  return form_id_pel
}

form_business_id(params){
  let form_business_id = ''
  if(params)
    form_business_id = params.business_id
  
  return form_business_id
}

form_nama_penerima(params){
  let form_nama_penerima = ''
  if(params)
    form_nama_penerima = params.nama_penerima_gopay
  
  return form_nama_penerima
}

form_group(params){
  let form_group = ''
  if(params)
    form_group = params.description
  
  return form_group
}

form_cluster(params){
  let form_cluster = ''
  if(params)
    form_cluster = params.cluster
  
  return form_cluster
}

form_group_mci(params){
  let form_group_mci = ''
  if(params)
    form_group_mci = params.description_mci
  
  return form_group_mci
}

data_complete(params){
  let data_complete = ''
  if(params)
  data_complete = params.data_complete
  
  return data_complete
}

form_nama_toko(params){
  let form_nama_toko = ''
  if(params)
    form_nama_toko = params.nama_toko
  
  return form_nama_toko
}

reference_no_length(params){
  if(params && params.constructor === Array && params.length > 0) {
    let reference_no_length;
    reference_no_length = params.length
    
    return reference_no_length
  } else {
    return 0;
  }
}

form_alamat_toko(params){
  let form_alamat_toko = ''
  if(params)
    form_alamat_toko = params.alamat_toko
  
  return form_alamat_toko
}

form_nama_distributor(params){
  let form_nama_distributor = ''
  if(params)
    form_nama_distributor = params.nama_distributor
  
  return form_nama_distributor
}

form_nama_pemilik(params){
  let form_nama_pemilik = ''
  if(params)
    form_nama_pemilik = params.nama_pemilik
  
  return form_nama_pemilik
}

form_ktp_pemilik(params){
  let form_ktp_pemilik = ''
  if(params)
    form_ktp_pemilik = params.ktp_pemilik
  
  return form_ktp_pemilik
}

form_npwp_pemilik(params){
  let form_npwp_pemilik = ''
  if(params)
    form_npwp_pemilik = params.npwp_pemilik
  
  return form_npwp_pemilik
}

form_foto_ktp_pemilik(params){
  let form_foto_ktp_pemilik = ''
  if(params)
    form_foto_ktp_pemilik = params.foto_ktp_pemilik

  return form_foto_ktp_pemilik
}

form_foto_npwp_pemilik(params){
  let form_foto_npwp_pemilik = ''
  if(params)
    form_foto_npwp_pemilik = params.foto_npwp_pemilik

  return form_foto_npwp_pemilik
}

form_foto_npwp_penerima(params){
  let form_foto_npwp_penerima = ''
  if(params)
    form_foto_npwp_penerima = params.foto_npwp_penerima
  
  return form_foto_npwp_penerima
}

form_telp_pemilik(params){
  let form_telp_pemilik = ''
  if(params)
    form_telp_pemilik = params.telp_pemilik
  
  return form_telp_pemilik
}

form_wa_pemilik(params){
  let form_wa_pemilik = ''
  if(params)
    form_wa_pemilik = params.no_wa_pemilik
  
  return form_wa_pemilik
}

form_email_pelanggan(params){
  let form_email_pelanggan = ''
  if(params)
  form_email_pelanggan = params.email
  
  return form_email_pelanggan
}

form_alamat_penerima(params){
  let form_alamat_penerima = ''
  if(params)
    form_alamat_penerima = params.alamat_rumah
  
  return form_alamat_penerima
}

form_hadiah_dikuasakan(params){
  let form_hadiah_dikuasakan = ''
  if(params)
    form_hadiah_dikuasakan = params.hadiah_dikuasakan
  
  return form_hadiah_dikuasakan
}

form_ktp_penerima(params){
  let form_ktp_penerima = ''
  if(params)
    form_ktp_penerima = params.ktp_penerima
  
  return form_ktp_penerima
}

form_foto_ktp_penerima(params){
  let form_foto_ktp_penerima = ''
  if(params)
    form_foto_ktp_penerima = params.foto_ktp_penerima
  
  return form_foto_ktp_penerima
}

form_npwp_penerima(params){
  let form_npwp_penerima = ''
  if(params)
    form_npwp_penerima = params.npwp_penerima
  
  return form_npwp_penerima
}

form_wa_penerima(params){
  let form_wa_penerima = ''
  if(params)
    form_wa_penerima = params.no_wa_penerima
  
  return form_wa_penerima
}

form_gopay_penerima(params){
  let form_gopay_penerima = ''
  if(params)
    form_gopay_penerima = params.telp_penerima_gopay
  
  return form_gopay_penerima
}

// additional info
bast(params){
  let bast = ''
  if(params && params.bast && params.bast.pic_file_name) {
    bast = params.bast.pic_file_name
  } else {
    bast = '-';
  }
  
  return bast
}

surat_kuasa(params){
  let surat_kuasa = ''
  if(params && params.surat_kuasa && params.surat_kuasa.pic_file_name) {
    surat_kuasa = params.surat_kuasa.pic_file_name
  } else {
    surat_kuasa = '-';
  }
  
  return surat_kuasa
}

bast_date(params){
  let bast_date = ''
  if(params && params.bast && params.bast.created_date) {
    bast_date = params.bast.created_date
  } else {
    bast_date = '-';
  }
  
  return bast_date
}

invoice_no(params){
  let invoice_no = ''
  if(params && params.invoice_no) {
    invoice_no = params.invoice_no
  } else {
    invoice_no = '-';
  }
  
  return invoice_no
}

// delivery detail
awb_number(params){
  let awb_number = ''
  if(params && params.awb_number) {
    awb_number = params.awb_number
  } else {
    awb_number = '-';
  }
  
  return awb_number
}

delivered_date(params){
  let delivered_date = ''
  if(params && params.delivered_date) {
    delivered_date = params.delivered_date
  } else {
    delivered_date = '-';
  }
  
  return delivered_date
}

delivery_date(params){
  let delivery_date = ''
  if(params && params.delivery_date) {
    delivery_date = params.delivery_date
  } else {
    delivery_date = '-';
  }
  
  return delivery_date
}

last_shipping_info(params){
  let last_shipping_info = ''
  if(params && params.last_shipping_info) {
    last_shipping_info = params.last_shipping_info
  } else {
    last_shipping_info = '-';
  }
  
  return last_shipping_info
}

receiver_name_sap(params){
  let receiver_name_sap = ''
  if(params && params.receiver_name) {
    receiver_name_sap = params.receiver_name
  } else {
    receiver_name_sap = '-';
  }
  
  return receiver_name_sap
}

relation_name(params){
  let relation_name = ''
  if(params && params.relation_name) {
    relation_name = params.relation_name
  } else {
    relation_name = '-';
  }
  
  return relation_name
}

// address detail
receiver_name(params){
  let receiver_name = ''
  if(params && params.name) {
    receiver_name = params.name
  } else {
    receiver_name = '-';
  }
  
  return receiver_name;
}

shipping_address(params){
  let shipping_address = ''
  if(params && params.address) {
    shipping_address = params.address
  } else {
    shipping_address = '-';
  }
  
  return shipping_address;
}

phone_number(params){
  let phone_number = '';
  if(params && params.cell_phone) {
    phone_number = params.cell_phone
  } else {
    phone_number = '-';
  }
  
  return phone_number;
}

// Inovice Finance New
value_invoice(params){
  let value_invoice = ''
  if(params)
  value_invoice = params.value
  
  return value_invoice
}

value_po(params){
  let value_po = ''
  if(params)
  value_po = params.value
  
  return value_po
}

date_invoice(params){
  let date_invoice = ''
  if(params)
  date_invoice = params.created_date
  
  return date_invoice
}

//product sales order new 
product_quantity(params){
  let product_quantity = ''
  if(params)
    product_quantity = params.quantity
  
  return product_quantity
}

product_redeem(params){
  let product_redeem = ''
  if(params)
    product_redeem = params.product_name
  
  return product_redeem
}

product_code_redeem(params){
  let product_code_redeem = ''
  if(params)
    product_code_redeem = params.product_code
  
  return product_code_redeem
}

product_price_redeem(params){
  let product_price_redeem = ''
  if(params)
    product_price_redeem = params.total_product_price
  
  return product_price_redeem
}
 
 penerima(params){
  //  console.log( " HREEE")
  let penerima = ''
  if(params.values)
    penerima = params.values.nama_penerima_gopay
  

  return penerima
 }
 pemilik(params){
  //  console.log( " HREEE")
  let pemilik = ''
  if(params.values)
    pemilik = params.values.nama_pemilik
  

  return pemilik
 }


 nama_toko(params){
  // console.log( " HREEE")
 let penerima = ''
 if(params.values)
   penerima = params.values.nama_toko
  //  console.log(' pe', penerima)
 

 return penerima
}
member_desc(params){
  // console.log( " HREEE")
 let penerima = ''
 if(params.values)
   penerima = params.values.description
  //  console.log(' pe', penerima)
 

 return penerima
}
id_toko(params){
  // console.log( " HREEE")
 let penerima = ''
 if(params.values)
   penerima = params.values.id_pel
 return penerima
}
dikuasakan(params){

 let penerima = ''
 if(params.values)
   penerima = params.values.hadiah_dikuasakan
 

 return penerima
}
no_wa(params){
  // console.log( " HREEE")
 let penerima = ''
 if(params.values)
   penerima = params.values.no_wa_penerima
 

 return penerima
}
alamat_kirim(params){
  // console.log( " HREEE")
 let penerima = ''
 if(params.values)
   penerima = params.values.alamat_rumah
 

 return penerima
}
shipping_date(params){
  // console.log( " HREEE")
 let penerima = ''
 if(params.values)
   penerima = params.values.order_date
 

 return penerima
}

 product(params){
  let product = ''
  for (let i = 0; i < params.length; i++) {
    // console.log(' here',params[i]);
    
    product = product + params[i].product_name +' '+ '(' + ' '+params[i].total_product_price +' '+ ')' + ' '+ params[i].quantity + ' '+ 'X' + ','
  }

  return product
 }
 productcode(params){
  let product = ''
  for (let i = 0; i < params.length; i++) {
    // console.log(' here',params[i]);
    if (params[i].product_code){
      product += params[i].product_code + ',' 
    }
    if (params[i].item_code){
      product += params[i].item_code + ','
    }
    
  }

  return product
 }

 product_code(params){
  let product = ''
  for (let i = 0; i < params.length; i++) {
    // console.log(' here',params[i]);
    if (params[i].product_code){
      product += params[i].product_code + ',' 
    }
    if (params[i].item_code){
      product += params[i].item_code + ','
    }
    
  }

  return product
 }

 productweight(params){
  let product = ''
  for (let i = 0; i < params.length; i++) {
    // console.log(' here',params[i]);
    
    product += params[i].weight +'(' + params[i].weight * 1000 + 'gram )' +',' 
  }

  return product
 }
 productdesc(params){
  let product = ''
  for (let i = 0; i < params.length; i++) {
    // console.log(' here',params[i]);
    
    product += params[i].description +', '
  }

  return product
 }
 productname(params){
  let product = ''
  for (let i = 0; i < params.length; i++) {
    // console.log(' here',params[i]);
    
    product += params[i].product_name +', '
  }

  return product
 }

 product_name(params){
  let product = ''
  for (let i = 0; i < params.length; i++) {
    // console.log(' here',params[i]);
    
    product += params[i].product_name + '(' + params[i].quantity + ') ' +', '
  }

  return product
 }

 productdim(params){
  let product = ''
  for (let i = 0; i < params.length; i++) {
    // console.log(' here',params[i]);
    
    product += 'height:' + params[i].dimensions.height + ' length :' + params[i].dimensions.length + ' width:' + params[i].dimensions.width +', '
  }

  return product
 }
 productcat(params){
  let product = ''
  for (let i = 0; i < params.length; i++) {
    // console.log(' here',params[i]);
    if (params[i].categories){
      product += params[i].categories +', '
    }
    if (params[i].category){
      product += params[i].category +', '
    }
    
    
  }

  return product
 }
 productmerc(params){
  let product = ''
  for (let i = 0; i < params.length; i++) {
    // console.log(' here',params[i]);
    
    product += params[i].merchant_username + ','
  }

  return product
 }
 productqty(params){
  let product = 0
  for (let i = 0; i < params.length; i++) {
    // console.log(' here',params[i]);
    
    product += this.stringToNumber(params[i].quantity)
  }

  return product
 }

 quantity(params){
  let product = 0
  for (let i = 0; i < params.length; i++) {
    // console.log(' here',params[i]);
    
    product += this.stringToNumber(params[i].quantity)
  }

  return product
 }

 productunit(params){
  let product = ''
  for (let i = 0; i < params.length; i++) {
    // console.log(' here',params[i]);
    
    product += params[i].product_name + '(' + params[i].fixed_price + ')' + ','
  }

  return product
 }
 producttotal(params){
  let product = ''
  for (let i = 0; i < params.length; i++) {
    // console.log(' here',params[i]);
    
    product = product + params[i].product_name +' '+ '(' + ' '+params[i].total_product_price +' '+ ')'+ ','
  }

  return product
 }

//  points
point_balance(params){
  let point_balance = ''
  if(params)
    point_balance = params.point_balance

    return point_balance
}

expire_date(params){
  let expire_date = ''
  if(params)
    expire_date = params.expire_date

    return expire_date
}

// member address
penerima_hadiah(params){
  let penerima_hadiah = ''
  for (let i = 0; i < params.length; i++) {
    // console.log(' here',params[i]);
    if (params[i].name){
      penerima_hadiah += params[i].name
    }
  }
  return penerima_hadiah
 }

telp_penerima(params){
  let telp_penerima = ''
  for (let i = 0; i < params.length; i++) {
    // console.log(' here',params[i]);
    if (params[i].name){
      telp_penerima += params[i].cell_phone
    }
  }
  return telp_penerima
}

alamat_penerima(params){
  let alamat_penerima = ''
  for (let i = 0; i < params.length; i++) {
    // console.log(' here',params[i]);
    if (params[i].address){
      alamat_penerima += params[i].address
    }
  }
  return alamat_penerima
}

kelurahan_penerima(params){
  let keluarahan_penerima = ''
  for (let i = 0; i < params.length; i++) {
    // console.log(' here',params[i]);
    if (params[i].village_name){
      keluarahan_penerima += params[i].village_name
    }
  }
  return keluarahan_penerima
}

kecamatan_penerima(params){
  let kecamatan_penerima = ''
  for (let i = 0; i < params.length; i++) {
    // console.log(' here',params[i]);
    if (params[i].district_name){
      kecamatan_penerima += params[i].district_name
    }
  }
  return kecamatan_penerima
}

kota_penerima(params){
  let kota_penerima = ''
  for (let i = 0; i < params.length; i++) {
    // console.log(' here',params[i]);
    if (params[i].city_name){
      kota_penerima += params[i].city_name
    }
  }
  return kota_penerima
}

provinsi_penerima(params){
  let provinsi_penerima = ''
  for (let i = 0; i < params.length; i++) {
    // console.log(' here',params[i]);
    if (params[i].province_name){
      provinsi_penerima += params[i].province_name
    }
  }
  return provinsi_penerima
}

kode_pos_penerima(params){
  let kode_pos_penerima = ''
  for (let i = 0; i < params.length; i++) {
    // console.log(' here',params[i]);
    if (params[i].postal_code){
      kode_pos_penerima += params[i].postal_code
    }
  }
  return kode_pos_penerima
}

// end of member address

// shipping_info
// on_process_date(params){
//   let product = ''
//   for (let i = 0; i < params.length; i++) {
//     // console.log(' here',params[i]);
//     if (params[i].label == "on_processing"){
//       product += params[i].created_date
//     }    
//   }

//   return product
//  }

 on_delivery_date(params){
  let product = ''
  for (let i = 0; i < params.length; i++) {
    // console.log(' here',params[i]);
    if (params.length > 1) {
      if (params[i].label == "on_delivery"){
        product += params[i].created_date
      }
    } else if (params.length == 1) {
      product = "-"
    }
  }

  return product
 }

//  input_awb_date(params){
//   let product = ''
//   for (let i = 0; i < params.length; i++) {
//     // console.log(' here',params[i]);
//     if (params.length > 1) {
//       if (params[i].label == "on_delivery" && params[i].updated_date){
//         product += params[i].updated_date
//       }
//     } else if (params.length == 1) {
//       product = "-"
//     }
//   }

//   return product
//  }



 autoBast(){
  // console.log('print BAST',this.checkBox)
  if(this.checkBox.length == 0) return;
  this.checkBox.forEach((el,i)=>{
    if (el == true) {
      this.salesRedemp.push(this.table_data[i])
      console.log('el',this.salesRedemp)
    }
  })  
  this.bast_mode = true;

 }

 async setCompleteMember() { 
  this.checkBox.forEach((el,i)=>{
    if (el == true) {
      this.completeMember.push(this.table_data[i])
      console.log('el',this.completeMember)
    }
  });

  if(this.completeMember.length > 0){
    await this.options.detail_function[0]['completeSelectedMembers'](this.options.detail_function[0],this.completeMember);
    this.checkBox = [];
  }
 }

 async approveOrder(){
  let selectedOrder = [];
  this.checkBox.forEach((el,i)=>{
    if (el == true) {
      selectedOrder.push(this.table_data[i].order_id);
    }
  });
  if(selectedOrder.length > 0){
    await this.options.detail_function[0]['processSelectedOrders'](this.options.detail_function[0],selectedOrder);
    this.checkBox = [];
  }
 }

 async removeFromPackingList(){
  let selectedOrder = [];
  this.checkBox.forEach((el,i)=>{
    if (el == true) {
      selectedOrder.push(this.table_data[i].reference_no);
    }
  });
  if(selectedOrder.length > 0){
    await this.options.detail_function[0]['processSelectedOrders'](this.options.detail_function[0],selectedOrder);
    this.checkBox = [];
  }
 }

 async cancelSelectedProduct(){
  let selectedOrder = [];
  this.checkBox.forEach((el,i)=>{
    if (el == true) {
      selectedOrder.push(this.table_data[i].products.sku_code);
    }
  });
  if(selectedOrder.length > 0){
    await this.options.detail_function[0]['processSelectedProducts'](this.options.detail_function[0],selectedOrder);
    this.checkBox = [];
  }
 }

 usr_mngt(params){
  let usr_mngt = ''
  if(params)
      usr_mngt = params

  let tableHTML = usr_mngt.replace(/\|/g, ', ');
 

  return tableHTML
}
  clickToDownloadReport(){
    //reportTable ID
   
    let dataType = 'application/vnd.ms-excel';
    let tableSelect = document.getElementById('reportTable');
    let tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

    // Specify file name
    let filename = 'excel_data.xls';
    let downloadLink = document.createElement("a");
    document.body.appendChild(downloadLink);
    
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
    
        // Setting the file name
        downloadLink.download = filename;
        
        //triggering the function
        downloadLink.click();
    }
  }

  variation(params){
    let color = ``;

    if(params) {
      for (const property in params) {
        color += `${property}: ${params[property]}<br>`
      }
    }
    
    return (color);
  }

  dimensions(params){
    let color = ``;

    if(params) {
      for (const property in params) {
        color += `${property}: ${params[property]}<br>`
      }
    }
    
    return (color);
  }

  images_gallery(params){
    let image = ``;

    if(params && params[0] && params[0].base_url && params[0].pic_file_name) {
      image = params[0].base_url + params[0].pic_file_name
    }
    
    return image;
  }
}

