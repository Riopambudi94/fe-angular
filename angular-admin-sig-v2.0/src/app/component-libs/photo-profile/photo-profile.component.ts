import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-photo-profile-component',
  templateUrl: './photo-profile.component.html',
  styleUrls: ['./photo-profile.component.scss']
})
export class PhotoProfileComponent implements OnInit {

  @Input() bgUrl;
  constructor() { }

  ngOnInit() {
    if(this.bgUrl == undefined || this.bgUrl == null){
      this.bgUrl;
    }
  }

}
