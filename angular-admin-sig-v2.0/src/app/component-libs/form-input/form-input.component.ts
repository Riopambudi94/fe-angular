import { Component, OnInit, Input, Output } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'form-input',
  templateUrl: './form-input.component.html',
  styleUrls: ['./form-input.component.scss'],
  providers:[
    { provide: NG_VALUE_ACCESSOR, 
      useExisting: FormInputComponent, 
      multi:true}
  ]
})
export class FormInputComponent implements ControlValueAccessor {

  @Input() text_value : string;
  @Input() placeholder: string;
  @Input() type       : string;
  @Input() disabled   : boolean;
  inputType           : string;
  @Input() addClass   : string;
  @Input() alert      : any;
  @Input() onKeyup    : any;
  constructor(){
   
  }

  private onChange:(value: string)=>void;

  onKeyupMy(event : any){
    this.onChange(this.text_value);
    if(this.onKeyup)this.onKeyup(event);

  }
  writeValue(value: string){
    if(value) 
      this.text_value = value;
    
    if(this.type=='password'){
        this.inputType = 'password';
    }
    if(!this.disabled){
      this.disabled = false;
    }
   
  }

  registerOnChange(onChange: (value: string)=> void){
    this.onChange = onChange;
  }

  registerOnTouched(){}

  changePasswordType(){
    if(this.inputType=='password') this.inputType='text';
    else this.inputType= 'password';

  }
}
