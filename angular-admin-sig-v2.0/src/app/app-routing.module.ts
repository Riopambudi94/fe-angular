import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared';

const routes: Routes = [
    { path: '',         loadChildren: './layout/layout.module#LayoutModule', canActivate: [AuthGuard] },
    { path: 'login',    loadChildren: './login/login.module#LoginModule' },
    { path: 'app-login',    loadChildren: './app-login/app-login.module#AppLoginModule' },
    { path: 'reset-password',    loadChildren: './reset-password/reset-password.module#ResetPasswordModule' },
    { path: 'login-merchant', loadChildren: './login-merchant/login-merchant.module#LoginMerchantModule' },
    { path: 'register', loadChildren: './register/register.module#RegisterModule' },
    { path: 'signup',   loadChildren: './signup/signup.module#SignupModule' },
    { path: 'devmode',  loadChildren: './devmode/devmode.module#DevmodeModule' },
    { path: 'error',    loadChildren: './server-error/server-error.module#ServerErrorModule' },
    { path: 'access-denied', loadChildren: './access-denied/access-denied.module#AccessDeniedModule' },
    { path: 'not-found',loadChildren: './not-found/not-found.module#NotFoundModule' },
    // { path: 'merchant-portal',  loadChildren: './layout/merchant-portal/merchant-portal.module#MerchantPortalModule',  data: { preload: true }, canActivate: [AuthGuard] },
    { path: '**', redirectTo: 'not-found' },

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
