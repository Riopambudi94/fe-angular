import { Title } from "@angular/platform-browser";
import { type } from "os";

export interface TableFormat {
  title: string,
  label_headers: any[],
  row_primary_key: string,
  formOptions: FormOptions
  show_checkbox_options?
  filterForm?
  addRoute?

}

export interface FormOptions {
  row_id: string
  this: any
  result_var_name: string
  detail_function: any[]
  type?
  image_data_property?
  submit_function?
  check_box?
  addForm?
  addBulk?
  bulkUpdate?
  updateInvoice?
  approveButton?
  addFormGenerate?
  addMultipleLable?
  salesRedemption?
  bastReport?
  setCompleteMember?
  customButtons?: any[]
  ADcustomButtons?: any[]
  filterForm?
  pointMovementProduct?
  pointMovementOrder?
  removePackinglist?
  cancelProduct?
}

export interface TopBarMenuItem {
  label: string,
  routerLink: string,
  active?: boolean
}

export interface GeneralComponent {
  title: string
}

export const productsTableFormat = function (_this: any) {

  let t: TableFormat = {
    title: 'Products Data',
    label_headers: [
      {label: 'Created Date', visible: true, type: 'date', data_row_name: 'created_date'},

      {label: 'Merchant Name', visible: true, type: 'string', data_row_name: 'merchant_username'},
      {label: 'Product Name', visible: true, type: 'string', data_row_name: 'product_name'},
      {label: 'Product Code', visible: true, type: 'string', data_row_name: 'product_code'},
      {label: 'Category', visible: true, type: 'string', data_row_name: 'category'},
      {label: 'Price', visible: true, type: 'string', data_row_name: 'price'},
      {label: 'Fixed Price', visible: true, type: 'string', data_row_name: 'fixed_price'},
      {label: 'Discount', visible: true, type: 'string', data_row_name: 'discount_value'},
      {label: 'Quantity', visible: true, type: 'string', data_row_name: 'qty'},
      {label: 'Status',visible:true, type: 'string', data_row_name: 'status'},
      {label: 'Approved',visible:true, type: 'string', data_row_name: 'approved'},
    ],
    row_primary_key: '_id',
    formOptions: {
      row_id: '_id',
      addForm: true,
      this: _this,
      result_var_name: 'Products',
      detail_function: [_this, 'callDetail'],
      customButtons: [
        { label: 'Approve', func: (f) => { _this.approval(f) } },
      ]
    },
    show_checkbox_options: true
  }

  return t;
}

export const evoucherListDetail = function (_this:any){
  let t : TableFormat = {
  title: 'Evoucher List Detail',
  label_headers: [
    // {label: 'Voucher Code', visible: true, type: 'string', data_row_name: 'ev_voucher_code'},
    {label: 'Expiry Date', visible: true, type: 'date', data_row_name: 'expiry_date'},
    {label: 'Merchant Username', visible: true, type: 'string', data_row_name: 'merchant_username'},
    {label: 'approved',visible:true, type: 'string', data_row_name: 'active'},
  ],
  row_primary_key: '_id',
  formOptions: {
    row_id: '_id',
    addForm: true,
    this: _this,
    result_var_name: 'evoucherDetail',
    detail_function: [_this, 'callDetail'],
    customButtons: [
      { label: 'Activate', func: (f) => { _this.evoucherStatus(f) } },
      { label: 'Deactivate', func: (f) => { _this.evoucherStatus(f) } },
    ]
      },
  show_checkbox_options: true
}
return t;
}

export const evoucherListDetailAdmin = function (_this:any){
  let t : TableFormat = {
  title: 'Evoucher List Detail',
  label_headers: [
    // {label: 'Voucher Code', visible: true, type: 'string', data_row_name: 'ev_voucher_code'},
    {label: 'Expiry Date', visible: true, type: 'date', data_row_name: 'expiry_date'},
    {label: 'Merchant Username', visible: true, type: 'string', data_row_name: 'merchant_username'},
    {label: 'approved',visible:true, type: 'string', data_row_name: 'active'},
  ],
  row_primary_key: '_id',
  formOptions: {
    row_id: '_id',
    addForm: true,
    this: _this,
    result_var_name: 'evoucherDetail',
    detail_function: [_this, 'callDetail'],
    customButtons: [
      { label: 'Approve', func: (f) => { _this.approval(f) } },
    ]
      },
  show_checkbox_options: true
}
return t;
}

export const eVouchersTableFormat = function (_this: any) {

  let t: TableFormat = {
    title: 'E-Vouchers Data',
    label_headers: [
      // { label: 'Active Total', visible: true, type: 'string', data_row_name: 'active_total' },
      // // { label: 'E-Voucher Code', visible: true, type: 'string', data_row_name: 'ev_product_code' },
      // { label: 'Expired Date', visible: true, type: 'string', data_row_name: 'expiry_date' },
      // { label: 'Quantity In Total', visible: true, type: 'string', data_row_name: 'qty_in_total' },
      // { label: 'Quantity Out Total', visible: true, type: 'string', data_row_name: 'qty_out_total' },
      // { label: 'Total', visible: true, type: 'string', data_row_name: 'total' },
      {label: 'Expiry Date', visible: true, type: 'date', data_row_name: 'expiry_date'},
      {label: 'Available Stock', visible: true, type: 'string', data_row_name: 'qty_available'},
      {label: 'Bought by User', visible: true, type: 'string', data_row_name: 'qty_sold'},
      {label: 'Used by User', visible: true, type: 'string', data_row_name: 'qty_used'},
      {label: 'Inactive Stock', visible: true, type: 'string', data_row_name: 'qty_inactive'},
      {label: 'Created Date', visible: false, type: 'string', data_row_name: 'qty_inactive'},

    ],
    row_primary_key: '_id',
    formOptions: {
      row_id: '_id',
      addForm: true,
      this: _this,
      result_var_name: 'generatedVouchers',
      detail_function: [_this, 'callDetail'],
      customButtons: [
        { label: 'Approval', func: (f) => { _this.approval(f) } },
      ]
    },
    show_checkbox_options: true
  }

  return t;

  
}



export const eVouchersSalesReportTableFormat = function (_this: any) {

  let t: TableFormat = {
    title: 'E-Vouchers Sales Report',
    label_headers: [
      { label: 'Product Code', visible: true, type: 'string', data_row_name: 'ev_product_code' },
      { label: 'Product Name', visible: true, type: 'string', data_row_name: 'product_name' },
      { label: 'Merchant Username', visible: false, type: 'string', data_row_name: 'merchant_username' },
      { label: 'Stock', visible: true, type: 'string', data_row_name: 'stock' },
      { label: 'Bought', visible: true, type: 'string', data_row_name: 'bought' },
      { label: 'Used', visible: true, type: 'string', data_row_name: 'redeem' },
    ],
    row_primary_key: '_id',
    formOptions: {
      row_id: '_id',
      addForm: true,
      this: _this,
      result_var_name: 'evouchers',
      detail_function: [_this, 'callDetail'],
      customButtons: [
        { label: 'Approve', func: (f) => { _this.approval(f) } },
      ]
    },
    show_checkbox_options: true
  }

  return t;
}


export function orderHistoriesTableFormat(_this: any): TableFormat {
  let t: TableFormat = {
    title: 'Order All History Detail Page',
    label_headers: [
      { label: 'Order date', visible: true, type: 'date', data_row_name: 'created_date' },
      { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
      { label: 'User Name', visible: true, type: 'string', data_row_name: 'user_name' },
      { label: 'User Email', visible: true, type: 'string', data_row_name: 'email' },
      { label: 'Cell Phone', visible: true, type: 'string', data_row_name: 'cell_phone' },
      // { label: 'Product Code', visible: false, type: 'string', data_row_name: 'product_code' },
      // { label: 'Product Name', visible: false, type: 'string', data_row_name: 'product' },
      // { label: 'Quantity', visible: false, type: 'string', data_row_name: 'qty' },
      // { label: 'Price', visible: false, type: 'string', data_row_name: 'price' },
      // { label: 'Discounts LO Points', visible: false, type: 'string', data_row_name: 'discounts' },
      // { label: 'Delcost', visible: false, type: 'string', data_row_name: 'delcost' },
      // { label: 'Unique Code', visible: false, type: 'string', data_row_name: 'unique_amount' },
      { label: 'Total Price', visible: true, type: 'string', data_row_name: 'total_price' },
      { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
      // { label: 'User Id', visible: false, type: 'string', data_row_name: 'user_id' },
      // { label: 'Payment Method', visible: false, type: 'string', data_row_name: 'payment_method' },
      // { label: 'Courier', visible: false, type: 'string', data_row_name: 'shipping_info' },
      // { label: 'Bank Name', visible: false, type: 'string', data_row_name: 'bank_name' },
      { label: 'Unique Amount', visible: true, type: 'string', data_row_name: 'unique_amount' },
      // {
      //   label: 'Type', visible: false,
      //   options: ['product', 'voucher'],
      //   type: 'list', data_row_name: 'type'
      // },
     
    ],
    row_primary_key: '_id',
    formOptions: {
      row_id: '_id',
      this: _this,
      result_var_name: 'Orderhistoryallhistory',
      detail_function: [_this, 'callDetail'],
    },
    show_checkbox_options: true
  }

  return t;
}

export function salesOrderTableFormat(_this: any): TableFormat {
  let t: any = {
    title: 'Order All History Detail Page',
    label_headers:[
      { label: 'Product Name', visible: true, type: 'string', data_row_name: 'product_name' },
      { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
      { label: 'Quantity', visible: true, type: 'string', data_row_name: 'qty' },
      { label: '@Price', visible: true, type: 'string', data_row_name: 'price' },
      { label: 'Discount Price', visible: true, type: 'string', data_row_name: 'discount_price'},
      { label: 'Total Price', visible: true, type: 'string', data_row_name: 'total_price' },
      { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
      { label: 'User Email', visible: true, type: 'string', data_row_name: 'user_email' },
      { label: 'Order date', visible: true, type: 'date', data_row_name: 'order_date' },
      { label: 'User Name', visible: true, type: 'string', data_row_name: 'username' },
      { label: 'Payment Method', visible: true, type: 'string', data_row_name: 'payment_method' },
      { label: 'Bank Name', visible: true, type: 'string', data_row_name: 'bank' }
    ],
    row_primary_key: '_id',
    formOptions: {
      row_id: '_id',
      this: _this,
      result_var_name: 'Orderhistoryallhistory',
      detail_function: [_this, 'callDetail'],
    },
    show_checkbox_options: true
  }
  return t;
}

export function merchantSalesOrderTableFormat(_this: any): TableFormat {
  let t: any = {
    title: 'Merchant Sales Order',
    label_headers:[
      { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
      { label: 'Product Name', visible: true, type: 'string', data_row_name: 'product_name' },
      { label: 'Quantity', visible: true, type: 'string', data_row_name: 'quantity' },
      { label: 'Price', visible: true, type: 'string', data_row_name: 'price' },
      { label: 'Total Price', visible: true, type: 'string', data_row_name: 'total_price' },
      { label: 'Bank', visible: true, type: 'string', data_row_name: 'bank' }
    ],
    row_primary_key: '_id',
    formOptions: {
      row_id: '_id',
      this: _this,
      result_var_name: 'allData',
      detail_function: [_this, 'callDetail']
    },
    show_checkbox_options: true
  }
  return t;
}

export function isString(variable){
  return typeof(variable) == 'string'
}

export function isArray(variable){
  return variable instanceof Array;
}

export function isNumber(variable){
  return typeof(variable) == 'number'
}

export function isInt(variable){
  return typeof(variable) == 'number' && Number.isInteger(variable)
}