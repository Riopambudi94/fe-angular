// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

// API UNTUK DEVELOPMENT (41418C656335D6BA2B58C9F5B404CCF0B894070466F0D038D6B991F9F67B70B6)
// API UNTUK B2B (825DBDE1417DF646BC636FD53B3F6A1D3979D9FBD17496717DBFBD76F1CBAE17)
// API UNTUK MALAYSIA (FAABE444EF1AC28216096734AF78EFB08B99DE3A8629FED4FA849B7DF9716EF1)
// API SIG QUICKWIN(DEV POIN SAHABAT) A490D9FD3F1DB33C94C7E484BB03CBE93C4830EE9D4389F78070ED24C9AC1DBF
// API SIG POIN SAHABAT 87E12038912E1E8387F99677EA16387B6FB6DE93929055D519561DB4AD5149A6


export const environment = {
  production: false
};


export function api_url() {
  return getHost() + 'v1/';
  // return 'http://localhost/locard/f3/v1/';
}

export function getHost() {
    // return 'http://192.168.56.4/f3/';
    // return 'https://api2.dev.locard.co.id/f3/';
  return 'http://dev.be.reals.cls-indo.co.id:30001/';
}

export function getAPIKeyHeader() {
  //Locard API Key
  // let Api_Key = '87E12038912E1E8387F99677EA16387B6FB6DE93929055D519561DB4AD5149A6'
  let Api_Key = 'A490D9FD3F1DB33C94C7E484BB03CBE93C4830EE9D4389F78070ED24C9AC1DBF';
  let customHeaders
  
  customHeaders = { 'Api_key': Api_Key }
  return customHeaders
  customHeaders
}

export function getLink(){
  const program = localStorage.getItem('programName');
  return 'http://dev.redemption.reals.cls-indo.co.id';
  // const program = localStorage.getItem('programName');
  // if (program == 'dynamix') {

  // } else if (program == 'MCI') {
  //   return 'http://redemption.reals.cls-indo.co.id';
  //   // return 'http://dev.mci.cls-indo.co.id/';
  // }
}


export function getTokenHeader() {
  const myToken = localStorage.getItem('tokenlogin')
  const customHeaders = { 'Authorization': myToken }
  return customHeaders
}

export function getCustomTokenHeader(app_label) {
  const myToken = localStorage.getItem('tokenlogin')
  const customHeaders = { 'Authorization': myToken, 'app-label': app_label }
  return customHeaders
}

export function getTokenHeaderRetailer() {
  const myToken = localStorage.getItem('tokenlogin');
  const program = localStorage.getItem('programName');
  // console.warn("program", program);

  let appLabel: any;
  if (program == 'dynamix_sbi') {
    appLabel = "retailer_prg";
  } else {
    appLabel = program;
  }

  const customHeaders = { 'Authorization': myToken, 'app-label': appLabel }

  return customHeaders
}

export function errorConvertToMessage(errorMessage: string) {
  errorMessage = errorMessage.replace("Error:", "");
  let errorJSON: any = JSON.parse(errorMessage);
  return errorJSON.error;
}
