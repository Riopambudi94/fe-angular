export const environment = {
    production: true
  };
  
  export function api_url(){
    return getHost()+'v1/';
    // return 'http://localhost/locard/f3/v1/';
  }
  
  export function getHost(){
    if(localStorage.getItem('devmode')== 'active'){
        // return 'http://192.168.56.4/f3/';
        return 'https://api2.dev.locard.co.id/f3/';
        
    }
    else{
        return 'https://api2.locard.co.id/f3/';
    }
    //  return 'https://api2.locard.co.id/f3/';
    return 'http://192.168.56.4/f3/';
  }
  
  export function getAPIKeyHeader(){
    //Locard API Key
    let Api_Key = '825DBDE1417DF646BC636FD53B3F6A1D3979D9FBD17496717DBFBD76F1CBAE17'
    let customHeaders
    // return customHeaders
  
    if(localStorage.getItem('devmode') == 'active'){
         Api_Key = '825DBDE1417DF646BC636FD53B3F6A1D3979D9FBD17496717DBFBD76F1CBAE17'
         customHeaders = {'Api_key': Api_Key }
         return customHeaders
    } else {
         Api_Key = '825DBDE1417DF646BC636FD53B3F6A1D3979D9FBD17496717DBFBD76F1CBAE17'
        customHeaders = { 'Api_key': Api_Key } 
    } return customHeaders
  }
  
  
  export function getTokenHeader(){
    const myToken = localStorage.getItem('tokenlogin')
    const customHeaders = { 'Authorization': myToken }
    return customHeaders
  }
  
  export function errorConvertToMessage(errorMessage: string){
    errorMessage = errorMessage.replace("Error:", "");
    let errorJSON:any = JSON.parse(errorMessage);
    return errorJSON.error;
  }
  