(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-salesorder-salesorder-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/salesorder/salesorder.component.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/salesorder/salesorder.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n  <app-page-header [heading]=\"'Sales Order'\" [icon]=\"'fa-table'\"></app-page-header>\r\n  <div *ngIf=\"Salesorder\">\r\n        <app-form-builder-table \r\n        [table_data]=\"Salesorder\"\r\n        [searchCallback]=\"[service, 'searchSalesOrderReport',this]\" \r\n        [tableFormat]=\"tableFormat\"\r\n        [total_page]=\"totalPage\"\r\n        >\r\n        </app-form-builder-table>\r\n\r\n  </div>\r\n\r\n"

/***/ }),

/***/ "./src/app/layout/modules/salesorder/salesorder-routing.module.ts":
/*!************************************************************************!*\
  !*** ./src/app/layout/modules/salesorder/salesorder-routing.module.ts ***!
  \************************************************************************/
/*! exports provided: SalesorderRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SalesorderRoutingModule", function() { return SalesorderRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _salesorder_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./salesorder.component */ "./src/app/layout/modules/salesorder/salesorder.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



// import { OrderhistorysummaryDetailComponent } from './detail/orderhistorysummary.detail.component';
// import { OrderhistorysummaryEditComponent } from './edit/orderhistorysummary.edit.component';
var routes = [
    {
        path: '', component: _salesorder_component__WEBPACK_IMPORTED_MODULE_2__["SalesorderComponent"]
    }
];
var SalesorderRoutingModule = /** @class */ (function () {
    function SalesorderRoutingModule() {
    }
    SalesorderRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], SalesorderRoutingModule);
    return SalesorderRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/salesorder/salesorder.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/layout/modules/salesorder/salesorder.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL3NhbGVzb3JkZXIvc2FsZXNvcmRlci5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/layout/modules/salesorder/salesorder.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/layout/modules/salesorder/salesorder.component.ts ***!
  \*******************************************************************/
/*! exports provided: SalesorderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SalesorderComponent", function() { return SalesorderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var SalesorderComponent = /** @class */ (function () {
    function SalesorderComponent(orderhistoryService) {
        this.orderhistoryService = orderhistoryService;
        this.Salesorder = [];
        this.tableFormat = {
            title: 'Sales Order Page',
            label_headers: [
                { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
                { label: 'Product Name', visible: true, type: 'string', data_row_name: 'product_name' },
                { label: 'Quantity', visible: true, type: 'string', data_row_name: 'quantity' },
                { label: 'Price', visible: true, type: 'string', data_row_name: 'price' },
                // { label: 'Discount Price', visible: false, type: 'string', data_row_name: 'discount_price' },
                // { label: 'Total Price', visible: false, type: 'string', data_row_name: 'total_price' },
                { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
                // { label: 'Email', visible: false, type: 'string', data_row_name: 'user_email' },
                // { label: 'Order Date', visible: false, type: 'string', data_row_name: 'order_date' },
                // { label: 'Username', visible: false, type: 'string', data_row_name: 'username' },
                // { label: 'Payment Method', visible: false, type: 'string', data_row_name: 'payment_method' },
                { label: 'Total Price', visible: true, type: 'string', data_row_name: 'total_price' },
                { label: 'Bank', visible: true, type: 'string', data_row_name: 'bank' },
                // unfinished
                // unfinished
                // unfinished
                {
                    label: 'Type', visible: false,
                    options: ['product', 'voucher'],
                    type: 'list', data_row_name: 'type'
                },
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: '_id',
                this: this,
                result_var_name: 'Salesorder',
                detail_function: [this, 'callDetail'],
            },
            show_checkbox_options: true
        };
        this.form_input = {};
        this.errorLabel = false;
    }
    SalesorderComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    SalesorderComponent.prototype.hello = function (f) {
        console.log("HELLO", f);
    };
    SalesorderComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.service = this.orderhistoryService;
                        return [4 /*yield*/, this.orderhistoryService.getSalesOrdeReport()];
                    case 1:
                        result = _a.sent();
                        this.totalPage = result.result.total_page;
                        console.log(result);
                        this.Salesorder = result.result.values;
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); // conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    SalesorderComponent.ctorParameters = function () { return [
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"] }
    ]; };
    SalesorderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-salesorder',
            template: __webpack_require__(/*! raw-loader!./salesorder.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/salesorder/salesorder.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./salesorder.component.scss */ "./src/app/layout/modules/salesorder/salesorder.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"]])
    ], SalesorderComponent);
    return SalesorderComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/salesorder/salesorder.module.ts":
/*!****************************************************************!*\
  !*** ./src/app/layout/modules/salesorder/salesorder.module.ts ***!
  \****************************************************************/
/*! exports provided: SalesorderModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SalesorderModule", function() { return SalesorderModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _salesorder_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./salesorder-routing.module */ "./src/app/layout/modules/salesorder/salesorder-routing.module.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
/* harmony import */ var _salesorder_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./salesorder.component */ "./src/app/layout/modules/salesorder/salesorder.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var SalesorderModule = /** @class */ (function () {
    function SalesorderModule() {
    }
    SalesorderModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _salesorder_routing_module__WEBPACK_IMPORTED_MODULE_2__["SalesorderRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_3__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__["NgbModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_5__["FormBuilderTableModule"],
                _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_7__["BsComponentModule"]
            ],
            declarations: [_salesorder_component__WEBPACK_IMPORTED_MODULE_8__["SalesorderComponent"]]
        })
    ], SalesorderModule);
    return SalesorderModule;
}());



/***/ })

}]);
//# sourceMappingURL=modules-salesorder-salesorder-module.js.map