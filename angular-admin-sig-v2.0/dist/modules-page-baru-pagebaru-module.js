(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-page-baru-pagebaru-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/page-baru/pagebaru.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/page-baru/pagebaru.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div >\r\n  <!-- <ng2-completer [(ngModel)]=\"searchStr\" [datasource]=\"dataService\" [minSearchLength]=\"0\"></ng2-completer> -->\r\n  <input list=\"browsers\" (keyup)=\"onSearch()\" (change)=\"onDataChanged()\" [(ngModel)]=\"dataSearch\">\r\n  <datalist id=\"browsers\">\r\n    <option *ngFor=\"let dt of routePath\" [value]=\"dt.path\">{{dt.label}}</option>\r\n  </datalist>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/router-master/router-master.component.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/router-master/router-master.component.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/layout/modules/page-baru/pagebaru.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/layout/modules/page-baru/pagebaru.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL3BhZ2UtYmFydS9wYWdlYmFydS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/layout/modules/page-baru/pagebaru.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/layout/modules/page-baru/pagebaru.component.ts ***!
  \****************************************************************/
/*! exports provided: PagebaruComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagebaruComponent", function() { return PagebaruComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_paymentgateway_paymentgateway_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/paymentgateway/paymentgateway.service */ "./src/app/services/paymentgateway/paymentgateway.service.ts");
/* harmony import */ var ng2_completer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng2-completer */ "./node_modules/ng2-completer/esm5/ng2-completer.js");
/* harmony import */ var _routing_path__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../routing.path */ "./src/app/layout/routing.path.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var PagebaruComponent = /** @class */ (function () {
    function PagebaruComponent(paymentGatewayService, completerService, router) {
        this.paymentGatewayService = paymentGatewayService;
        this.completerService = completerService;
        this.router = router;
        this.dataSummary = [];
        this.searchData = [
            { color: 'red', value: '#f00' },
            { color: 'green', value: '#0f0' },
            { color: 'blue', value: '#00f' },
            { color: 'cyan', value: '#0ff' },
            { color: 'magenta', value: '#f0f' },
            { color: 'yellow', value: '#ff0' },
            { color: 'black', value: '#000' }
        ];
        this.tableFormat = {
            title: 'Summary of Order History',
            label_headers: [
                { label: 'Nama', visible: true, type: 'string', data_row_name: 'name' },
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: '_id',
                this: this,
                result_var_name: 'dataSummary',
                detail_function: [this, 'callDetail']
            }
        };
        this.form_input = {};
        this.errorLabel = false;
        this.orderhistorysummaryDetail = false;
        this.dataService = completerService.local(this.searchData, 'color', 'color');
        this.routePath = _routing_path__WEBPACK_IMPORTED_MODULE_3__["routePath"];
    }
    PagebaruComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        result = void 0;
                        this.service = this.paymentGatewayService;
                        return [4 /*yield*/, this.paymentGatewayService.getPaymentGatewayLint()];
                    case 1:
                        result = _a.sent();
                        this.paymentGatewayData = result.result;
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    PagebaruComponent.prototype.onSearch = function () {
    };
    PagebaruComponent.prototype.onDataChanged = function () {
        console.log('changed', this.dataSearch);
        this.router.navigateByUrl(this.dataSearch);
    };
    PagebaruComponent.ctorParameters = function () { return [
        { type: _services_paymentgateway_paymentgateway_service__WEBPACK_IMPORTED_MODULE_1__["PaymentGatewayService"] },
        { type: ng2_completer__WEBPACK_IMPORTED_MODULE_2__["CompleterService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
    ]; };
    PagebaruComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-pagebaru',
            template: __webpack_require__(/*! raw-loader!./pagebaru.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/page-baru/pagebaru.component.html"),
            styles: [__webpack_require__(/*! ./pagebaru.component.scss */ "./src/app/layout/modules/page-baru/pagebaru.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_paymentgateway_paymentgateway_service__WEBPACK_IMPORTED_MODULE_1__["PaymentGatewayService"],
            ng2_completer__WEBPACK_IMPORTED_MODULE_2__["CompleterService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], PagebaruComponent);
    return PagebaruComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/page-baru/pagebaru.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/layout/modules/page-baru/pagebaru.module.ts ***!
  \*************************************************************/
/*! exports provided: PagebaruModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagebaruModule", function() { return PagebaruModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _pagebaru_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./pagebaru.component */ "./src/app/layout/modules/page-baru/pagebaru.component.ts");
/* harmony import */ var _pagebaru_routing__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./pagebaru.routing */ "./src/app/layout/modules/page-baru/pagebaru.routing.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
/* harmony import */ var ng2_completer__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng2-completer */ "./node_modules/ng2-completer/esm5/ng2-completer.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var PagebaruModule = /** @class */ (function () {
    function PagebaruModule() {
    }
    PagebaruModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_pagebaru_component__WEBPACK_IMPORTED_MODULE_2__["PagebaruComponent"]],
            // imports: [CommonModule, FormRoutingModule, PageHeaderModule],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _pagebaru_routing__WEBPACK_IMPORTED_MODULE_3__["PagebaruRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_4__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__["NgbModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_7__["FormBuilderTableModule"],
                _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__["BsComponentModule"],
                ng2_completer__WEBPACK_IMPORTED_MODULE_9__["Ng2CompleterModule"]
            ]
        })
    ], PagebaruModule);
    return PagebaruModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/page-baru/pagebaru.routing.ts":
/*!**************************************************************!*\
  !*** ./src/app/layout/modules/page-baru/pagebaru.routing.ts ***!
  \**************************************************************/
/*! exports provided: PagebaruRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagebaruRoutingModule", function() { return PagebaruRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _pagebaru_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./pagebaru.component */ "./src/app/layout/modules/page-baru/pagebaru.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '', component: _pagebaru_component__WEBPACK_IMPORTED_MODULE_2__["PagebaruComponent"]
    }
];
var PagebaruRoutingModule = /** @class */ (function () {
    function PagebaruRoutingModule() {
    }
    PagebaruRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], PagebaruRoutingModule);
    return PagebaruRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/router-master/router-master.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/layout/router-master/router-master.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9yb3V0ZXItbWFzdGVyL3JvdXRlci1tYXN0ZXIuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/layout/router-master/router-master.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/layout/router-master/router-master.component.ts ***!
  \*****************************************************************/
/*! exports provided: RouterMasterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RouterMasterComponent", function() { return RouterMasterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_observerable_permission_observer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/observerable/permission-observer */ "./src/app/services/observerable/permission-observer.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RouterMasterComponent = /** @class */ (function () {
    function RouterMasterComponent(router, castPermission) {
        this.router = router;
        this.castPermission = castPermission;
    }
    RouterMasterComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.castPermission.currentPermission.subscribe(function (permissionVal) {
            // if(permissionVal == 'merchant'){
            //     this.router.navigate(['/merchant-portal/homepage']);
            //     return true;
            // }
            if (permissionVal == 'admin') {
                // this.router.navigate(['/app-login']);
                // this.router.navigate(['/administrator/order-history-summary']);
            }
            else {
                _this.router.navigate(['/login/out']);
            }
        });
        var isLoggedin = localStorage.getItem('isLoggedin');
        if (isLoggedin == 'true') {
            this.router.navigateByUrl('/app-login');
        }
    };
    RouterMasterComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] },
        { type: _services_observerable_permission_observer__WEBPACK_IMPORTED_MODULE_2__["PermissionObserver"] }
    ]; };
    RouterMasterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-router-master',
            template: __webpack_require__(/*! raw-loader!./router-master.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/router-master/router-master.component.html"),
            styles: [__webpack_require__(/*! ./router-master.component.scss */ "./src/app/layout/router-master/router-master.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _services_observerable_permission_observer__WEBPACK_IMPORTED_MODULE_2__["PermissionObserver"]])
    ], RouterMasterComponent);
    return RouterMasterComponent;
}());



/***/ }),

/***/ "./src/app/layout/routing.path.ts":
/*!****************************************!*\
  !*** ./src/app/layout/routing.path.ts ***!
  \****************************************/
/*! exports provided: routePath */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routePath", function() { return routePath; });
/* harmony import */ var _router_master_router_master_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./router-master/router-master.component */ "./src/app/layout/router-master/router-master.component.ts");

var routePath = [
    { path: '', component: _router_master_router_master_component__WEBPACK_IMPORTED_MODULE_0__["RouterMasterComponent"], label: "router master component" },
    { path: 'administrator', loadChildren: './administrator/administrator.module#AdministratorModule', label: 'Administrator' },
    { path: 'merchant-portal', loadChildren: './merchant-portal/merchant-portal.module#MerchantPortalModule', label: "Merchant portal" },
    { path: 'admin-management', loadChildren: './admin-management/admin-management.module#AdminManagementModule', label: 'Admin Management' },
];


/***/ }),

/***/ "./src/app/services/observerable/permission-observer.ts":
/*!**************************************************************!*\
  !*** ./src/app/services/observerable/permission-observer.ts ***!
  \**************************************************************/
/*! exports provided: PermissionObserver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PermissionObserver", function() { return PermissionObserver; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PermissionObserver = /** @class */ (function () {
    function PermissionObserver() {
        this.currentPermission = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"]({});
        this.castCurrentPermission = this.currentPermission.asObservable();
        this.api_url = '';
    }
    PermissionObserver.prototype.updateCurrentPermission = function (data) {
        this.currentPermission.next(data);
    };
    PermissionObserver = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root',
        }),
        __metadata("design:paramtypes", [])
    ], PermissionObserver);
    return PermissionObserver;
}());



/***/ })

}]);
//# sourceMappingURL=modules-page-baru-pagebaru-module.js.map