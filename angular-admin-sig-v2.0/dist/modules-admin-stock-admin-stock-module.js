(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-admin-stock-admin-stock-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin-stock/admin-stock.component.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/admin-stock/admin-stock.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div >\r\n    <app-page-header [heading]=\"'&nbsp;'\" [icon]=\"'fa-table'\"></app-page-header>\r\n      \r\n      <div class=\"container-swapper-option\" *ngIf=\"SalesRedemptionDetail==false&&!errorMessage\">\r\n            <span style=\"margin-right: 10px;\">View</span> \r\n            <div class=\"button-container\">\r\n            <button class=\"btn toolbar {{swaper}}\" (click)=\"swapClick(true)\">\r\n            <span class=\"fa fa-table\"> Processed</span>\r\n            </button>\r\n            <button class=\"btn toolbar {{!swaper}}\" (click)=\"swapClick(false)\">\r\n            <span class=\"fa fa-table\"> Canceled</span>\r\n            </button>\r\n            </div>\r\n      </div>\r\n    \r\n      <div *ngIf=\"SalesRedemption && !mci_project && programType!='custom_kontraktual'\">\r\n            <app-form-builder-table\r\n            [searchCallback]= \"[service, swaper == true ? 'searchRedemptionReportint' : 'searchRedemptionCanceledReportint' ,this]\"\r\n            [tableFormat]=\"tableFormat\"\r\n            [table_data]=\"SalesRedemption\"\r\n            [total_page]=\"totalPage\"\r\n            >\r\n  \r\n            </app-form-builder-table>\r\n      </div>\r\n      <div *ngIf=\"SalesRedemption && mci_project && programType!='custom_kontraktual'\">\r\n            <app-form-builder-table\r\n            [searchCallback]= \"[service, swaper == true ? 'searchRedemptionReportint' : 'searchRedemptionCanceledReportint' ,this]\"\r\n            [tableFormat]=\"tableFormat2\"\r\n            [table_data]=\"SalesRedemption\"\r\n            [total_page]=\"totalPage\"\r\n            >\r\n\r\n            </app-form-builder-table>\r\n      </div>\r\n\r\n      <div *ngIf=\"SalesRedemption && !mci_project && programType=='custom_kontraktual'\">\r\n            <app-form-builder-table\r\n            [searchCallback]= \"[service, swaper == true ? 'searchRedemptionReportint' : 'searchRedemptionCanceledReportint' ,this]\"\r\n            [tableFormat]=\"tableFormat3\"\r\n            [table_data]=\"SalesRedemption\"\r\n            [total_page]=\"totalPage\"\r\n            >\r\n\r\n            </app-form-builder-table>\r\n      </div>\r\n        <!-- <div *ngIf=\"SalesRedemptionDetail\"> -->\r\n              <!-- <app-bast [back]=\"[this,backToHere]\" [detail]=\"SalesRedemptionDetail\"></app-bast> -->\r\n        <!-- </div> -->\r\n  \r\n    </div>\r\n  "

/***/ }),

/***/ "./src/app/layout/modules/admin-stock/admin-stock-routing.module.ts":
/*!**************************************************************************!*\
  !*** ./src/app/layout/modules/admin-stock/admin-stock-routing.module.ts ***!
  \**************************************************************************/
/*! exports provided: AdminStockRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminStockRoutingModule", function() { return AdminStockRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _admin_stock_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./admin-stock.component */ "./src/app/layout/modules/admin-stock/admin-stock.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



// import { BastComponent} from './bast/bast.component'
var routes = [
    {
        path: '', component: _admin_stock_component__WEBPACK_IMPORTED_MODULE_2__["AdminStockComponent"]
    },
];
var AdminStockRoutingModule = /** @class */ (function () {
    function AdminStockRoutingModule() {
    }
    AdminStockRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AdminStockRoutingModule);
    return AdminStockRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin-stock/admin-stock.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/layout/modules/admin-stock/admin-stock.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container-swapper-option {\n  display: flex;\n  justify-content: center;\n  flex-direction: row;\n  align-items: center;\n}\n.container-swapper-option .button-container {\n  border: 2px solid #ddd;\n  border-radius: 8px;\n}\n.container-swapper-option .button-container button {\n  color: #ddd;\n  background: white;\n  font-size: 14px;\n  border-radius: 8px;\n}\n.container-swapper-option .button-container button.true {\n  background: #2480fb;\n  border-radius: 8px;\n  color: #ddd;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvYWRtaW4tc3RvY2svRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxtb2R1bGVzXFxhZG1pbi1zdG9ja1xcYWRtaW4tc3RvY2suY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2FkbWluLXN0b2NrL2FkbWluLXN0b2NrLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQ0NGO0FEQUU7RUFFRSxzQkFBQTtFQUNBLGtCQUFBO0FDQ0o7QURBSTtFQUNFLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtBQ0VOO0FEQUk7RUFFRSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtBQ0NOIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L21vZHVsZXMvYWRtaW4tc3RvY2svYWRtaW4tc3RvY2suY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY29udGFpbmVyLXN3YXBwZXItb3B0aW9uIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAuYnV0dG9uLWNvbnRhaW5lciB7XHJcblxyXG4gICAgYm9yZGVyOiAycHggc29saWQgI2RkZDtcclxuICAgIGJvcmRlci1yYWRpdXM6OHB4O1xyXG4gICAgYnV0dG9uIHtcclxuICAgICAgY29sb3I6ICNkZGQ7XHJcbiAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICAgIH1cclxuICAgIGJ1dHRvbi50cnVlIHtcclxuICAgICBcclxuICAgICAgYmFja2dyb3VuZDogIzI0ODBmYjtcclxuICAgICAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gICAgICBjb2xvcjogI2RkZDtcclxuICAgIH1cclxuICB9XHJcbn0iLCIuY29udGFpbmVyLXN3YXBwZXItb3B0aW9uIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4uY29udGFpbmVyLXN3YXBwZXItb3B0aW9uIC5idXR0b24tY29udGFpbmVyIHtcbiAgYm9yZGVyOiAycHggc29saWQgI2RkZDtcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xufVxuLmNvbnRhaW5lci1zd2FwcGVyLW9wdGlvbiAuYnV0dG9uLWNvbnRhaW5lciBidXR0b24ge1xuICBjb2xvcjogI2RkZDtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xufVxuLmNvbnRhaW5lci1zd2FwcGVyLW9wdGlvbiAuYnV0dG9uLWNvbnRhaW5lciBidXR0b24udHJ1ZSB7XG4gIGJhY2tncm91bmQ6ICMyNDgwZmI7XG4gIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgY29sb3I6ICNkZGQ7XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/modules/admin-stock/admin-stock.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/layout/modules/admin-stock/admin-stock.component.ts ***!
  \*********************************************************************/
/*! exports provided: AdminStockComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminStockComponent", function() { return AdminStockComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
/* harmony import */ var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../assets/json/content.json */ "./src/assets/json/content.json");
var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_2___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../../../assets/json/content.json */ "./src/assets/json/content.json", 1);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var AdminStockComponent = /** @class */ (function () {
    function AdminStockComponent(OrderhistoryService) {
        this.OrderhistoryService = OrderhistoryService;
        this.SalesRedemption = [];
        this.swaper = true;
        this.tableFormat = {
            title: 'Sales Redemption report',
            label_headers: [
                { label: 'Tanggal Order', visible: true, type: 'date', data_row_name: 'request_date' },
                { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
                { label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'id_toko' },
                // {label: 'Member', visible: true, type: 'string', data_row_name: 'member_name'},
                { label: 'Nama Pemilik', visible: true, type: 'form_nama_pemilik', data_row_name: 'member_detail' },
                { label: 'Nama Penerima', visible: true, type: 'form_nama_penerima', data_row_name: 'member_detail' },
                { label: 'Nama Toko', visible: true, type: 'form_nama_toko', data_row_name: 'member_detail' },
                { label: 'Group', visible: true, type: 'form_group', data_row_name: 'member_detail' },
                { label: 'Alamat Kirim', visible: true, type: 'form_alamat_penerima', data_row_name: 'member_detail' },
                { label: 'No WA Penerima', visible: true, type: 'form_wa_penerima', data_row_name: 'member_detail' },
                { label: 'Hadiah Dikuasakan', visible: true, type: 'form_hadiah_dikuasakan', data_row_name: 'member_detail' },
                { label: 'Quantity', visible: true, type: 'product_quantity', data_row_name: 'products' },
                // {label: 'Points', visible: true, type: 'string', data_row_name: 'points'},
                { label: 'Hadiah Redeem', visible: true, type: 'product_redeem', data_row_name: 'products' },
                { label: 'Product Code', visible: true, type: 'product_code_redeem', data_row_name: 'products' },
                { label: 'Points Redeem', visible: true, type: 'string', data_row_name: 'sum_total' },
                // {label: 'Updated Date', visible: true, type: 'date', data_row_name: 'updated_date'},
                { label: 'Status Order', visible: true, type: 'string', data_row_name: 'status' },
                { label: 'Status Pengiriman', visible: true, type: 'last_shipping_info', data_row_name: 'delivery_detail' },
                { label: 'Tanggal Kirim', visible: true, type: 'delivery_date', data_row_name: 'delivery_detail' },
                { label: 'Tanggal Terima', visible: true, type: 'delivered_date', data_row_name: 'delivery_detail' },
                { label: 'Nama Penerima Langsung', visible: true, type: 'receiver_name_sap', data_row_name: 'delivery_detail' },
                { label: 'Status Penerima', visible: true, type: 'relation_name', data_row_name: 'delivery_detail' },
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: '_id',
                this: this,
                result_var_name: 'SalesRedemption',
                detail_function: [this, 'callDetail'],
                salesRedemption: true,
            },
            show_checkbox_options: true
        };
        this.tableFormat2 = {
            title: 'Sales Redemption report',
            label_headers: [
                { label: 'Tanggal Order', visible: true, type: 'date', data_row_name: 'request_date' },
                { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
                { label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'id_toko' },
                { label: 'Nama Penerima', visible: true, type: 'form_nama_penerima', data_row_name: 'member_detail' },
                { label: 'No WA Penerima', visible: true, type: 'form_wa_penerima', data_row_name: 'member_detail' },
                { label: 'Alamat Kirim', visible: true, type: 'form_alamat_penerima', data_row_name: 'member_detail' },
                { label: 'Quantity', visible: true, type: 'product_quantity', data_row_name: 'products' },
                { label: 'Hadiah Redeem', visible: true, type: 'product_redeem', data_row_name: 'products' },
                { label: 'Product Code', visible: true, type: 'product_code_redeem', data_row_name: 'products' },
                { label: 'Points Redeem', visible: true, type: 'string', data_row_name: 'sum_total' },
                { label: 'Program', visible: true, type: 'form_group', data_row_name: 'member_detail' },
                { label: 'Status Order', visible: true, type: 'string', data_row_name: 'status' },
                { label: 'Status Pengiriman', visible: true, type: 'last_shipping_info', data_row_name: 'delivery_detail' },
                { label: 'Tanggal Kirim', visible: true, type: 'delivery_date', data_row_name: 'delivery_detail' },
                { label: 'Tanggal Terima', visible: true, type: 'delivered_date', data_row_name: 'delivery_detail' },
                { label: 'Nama Penerima Langsung', visible: true, type: 'receiver_name_sap', data_row_name: 'delivery_detail' },
                { label: 'Status Penerima', visible: true, type: 'relation_name', data_row_name: 'delivery_detail' },
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: '_id',
                this: this,
                result_var_name: 'SalesRedemption',
                detail_function: [this, 'callDetail'],
                salesRedemption: true,
            },
            show_checkbox_options: true
        };
        this.tableFormat3 = {
            title: 'Sales Redemption report',
            label_headers: [
                { label: 'Tanggal Order', visible: true, type: 'date', data_row_name: 'request_date' },
                { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
                { label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'id_toko' },
                { label: 'Nama Program', visible: true, type: 'form_cluster', data_row_name: 'member_detail' },
                // {label: 'Member', visible: true, type: 'string', data_row_name: 'member_name'},
                { label: 'Nama PIC', visible: true, type: 'form_nama_pemilik', data_row_name: 'member_detail' },
                { label: 'Nama Penerima', visible: true, type: 'form_nama_penerima', data_row_name: 'member_detail' },
                { label: 'Nama Entitas', visible: true, type: 'form_nama_toko', data_row_name: 'member_detail' },
                // {label: 'Group', visible: true, type: 'form_group', data_row_name: 'member_detail'},
                { label: 'Alamat Kirim', visible: true, type: 'form_alamat_penerima', data_row_name: 'member_detail' },
                { label: 'No WA Penerima', visible: true, type: 'form_wa_penerima', data_row_name: 'member_detail' },
                { label: 'Hadiah Dikuasakan', visible: true, type: 'form_hadiah_dikuasakan', data_row_name: 'member_detail' },
                { label: 'Quantity', visible: true, type: 'product_quantity', data_row_name: 'products' },
                // {label: 'Points', visible: true, type: 'string', data_row_name: 'points'},
                { label: 'Hadiah Redeem', visible: true, type: 'product_redeem', data_row_name: 'products' },
                { label: 'Product Code', visible: true, type: 'product_code_redeem', data_row_name: 'products' },
                { label: 'Points Redeem', visible: true, type: 'string', data_row_name: 'sum_total' },
                { label: 'Status Order', visible: true, type: 'string', data_row_name: 'status' },
                // {label: 'Updated Date', visible: true, type: 'date', data_row_name: 'updated_date'},
                { label: 'Status Pengiriman', visible: true, type: 'last_shipping_info', data_row_name: 'delivery_detail' },
                { label: 'Tanggal Kirim', visible: true, type: 'delivery_date', data_row_name: 'delivery_detail' },
                { label: 'Tanggal Terima', visible: true, type: 'delivered_date', data_row_name: 'delivery_detail' },
                { label: 'Nama Penerima Langsung', visible: true, type: 'receiver_name_sap', data_row_name: 'delivery_detail' },
                { label: 'Status Penerima', visible: true, type: 'relation_name', data_row_name: 'delivery_detail' },
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: '_id',
                this: this,
                result_var_name: 'SalesRedemption',
                detail_function: [this, 'callDetail'],
                salesRedemption: true,
            },
            show_checkbox_options: true
        };
        this.contentList = _assets_json_content_json__WEBPACK_IMPORTED_MODULE_2__;
        this.form_input = {};
        this.errorLabel = false;
        this.totalPage = 0;
        this.SalesRedemptionDetail = false;
        this.mci_project = false;
        this.programType = "";
    }
    AdminStockComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    AdminStockComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var params, program_1, _this_1, result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 5, , 6]);
                        params = {
                            search: {
                                type: {
                                    in: ["product", "voucher"]
                                }
                                // active : '1'
                            },
                            limit_per_page: 50,
                            current_page: 1,
                            order_by: { request_date: -1 }
                        };
                        program_1 = localStorage.getItem('programName');
                        _this_1 = this;
                        this.contentList.filter(function (element) {
                            if (element.appLabel == program_1) {
                                if (element.type == "reguler") {
                                    _this_1.mci_project = true;
                                    _this_1.programType = "reguler";
                                }
                                else if (element.type == "custom_kontraktual") {
                                    _this_1.mci_project = false;
                                    _this_1.programType = "custom_kontraktual";
                                }
                                else {
                                    _this_1.mci_project = false;
                                    _this_1.programType = "custom";
                                }
                            }
                        });
                        this.service = this.OrderhistoryService;
                        result = void 0;
                        if (!(this.swaper == true)) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.OrderhistoryService.getRedemptionReportint()];
                    case 1:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 2: return [4 /*yield*/, this.OrderhistoryService.getRedemptionCanceledReportint()];
                    case 3:
                        result = _a.sent();
                        _a.label = 4;
                    case 4:
                        this.totalPage = result.total_page;
                        this.SalesRedemption = result.values;
                        return [3 /*break*/, 6];
                    case 5:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    AdminStockComponent.prototype.callDetail = function (SalesRedemption_id) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                try {
                    // let result: any;
                    // this.service    = this.OrderhistoryService;
                    // result          = await this.OrderhistoryService.detailPointstransaction(SalesRedemption_id);
                    // console.log(result);
                    // this.SalesRedemptionDetail = result.result[0];
                }
                catch (e) {
                    this.errorLabel = (e.message); //conversion to Error type
                }
                return [2 /*return*/];
            });
        });
    };
    AdminStockComponent.prototype.backToHere = function (obj) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                obj.SalesRedemptionDetail = false;
                return [2 /*return*/];
            });
        });
    };
    AdminStockComponent.prototype.addMonths = function (date, months) {
        date.setMonth(date.getMonth() + months);
        return date;
    };
    AdminStockComponent.prototype.swapClick = function (bool) {
        this.swaper = bool;
        this.firstLoad();
    };
    AdminStockComponent.ctorParameters = function () { return [
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_1__["OrderhistoryService"] }
    ]; };
    AdminStockComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-admin-stock',
            template: __webpack_require__(/*! raw-loader!./admin-stock.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin-stock/admin-stock.component.html"),
            styles: [__webpack_require__(/*! ./admin-stock.component.scss */ "./src/app/layout/modules/admin-stock/admin-stock.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_1__["OrderhistoryService"]])
    ], AdminStockComponent);
    return AdminStockComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin-stock/admin-stock.module.ts":
/*!******************************************************************!*\
  !*** ./src/app/layout/modules/admin-stock/admin-stock.module.ts ***!
  \******************************************************************/
/*! exports provided: AdminStockModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminStockModule", function() { return AdminStockModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _admin_stock_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./admin-stock-routing.module */ "./src/app/layout/modules/admin-stock/admin-stock-routing.module.ts");
/* harmony import */ var _admin_stock_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./admin-stock.component */ "./src/app/layout/modules/admin-stock/admin-stock.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








// import { BastComponent } from './bast/bast.component';

var AdminStockModule = /** @class */ (function () {
    function AdminStockModule() {
    }
    AdminStockModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _admin_stock_routing_module__WEBPACK_IMPORTED_MODULE_2__["AdminStockRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_4__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["NgbModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__["FormBuilderTableModule"],
                _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__["BsComponentModule"]
            ],
            declarations: [_admin_stock_component__WEBPACK_IMPORTED_MODULE_3__["AdminStockComponent"]],
        })
    ], AdminStockModule);
    return AdminStockModule;
}());



/***/ })

}]);
//# sourceMappingURL=modules-admin-stock-admin-stock-module.js.map