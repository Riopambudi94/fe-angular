(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-admin-shipping-admin-shipping-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin-shipping/admin-shipping.component.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/admin-shipping/admin-shipping.component.html ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div >\r\n      \r\n    <app-page-header [heading]=\"'&nbsp;'\" [icon]=\"'fa-table'\"></app-page-header>\r\n    <div class=\"container-swapper-option\">\r\n      <span style=\"margin-right: 10px;\">View  </span> \r\n      <div class=\"button-container\">\r\n    <button class=\"btn toolbar {{swaper}}\" (click)=\"swapClick(true)\">\r\n      <span class=\"fa fa-grip-horizontal\">Based On Product</span>\r\n    </button>\r\n    <button class=\"btn toolbar {{!swaper}}\" (click)=\"swapClick(false)\">\r\n      <span class=\"fa fa-table\">Based On Order_id</span>\r\n    </button>\r\n  </div>\r\n  </div>\r\n    <div *ngIf=\"Pointstransaction && swaper\">\r\n          <app-form-builder-table\r\n          [searchCallback]= \"[service, 'searchShippingProductReportint',this]\"\r\n          [tableFormat]=\"tableFormat\"\r\n          [table_data]=\"Pointstransaction\"\r\n          [total_page]=\"totalPage\"\r\n        >\r\n  \r\n          </app-form-builder-table>\r\n    </div><div *ngIf=\"Pointstransaction && !swaper\">\r\n      <app-form-builder-table\r\n      [searchCallback]= \"[service, 'searchShippingidReportint',this]\"\r\n      [tableFormat]=\"tableFormat\"\r\n      [table_data]=\"Pointstransaction\"\r\n      [total_page]=\"totalPage\"\r\n    >\r\n\r\n      </app-form-builder-table>\r\n</div>\r\n        <div *ngIf=\"pointstransactionDetail\">\r\n              <!-- <app-pointstransaction-detail [back]=\"[this,backToHere]\" [detail]=\"pointstransactionDetail\"></app-pointstransaction-detail> -->\r\n        </div>\r\n  \r\n    </div>\r\n  "

/***/ }),

/***/ "./src/app/layout/modules/admin-shipping/admin-shipping-routing.module.ts":
/*!********************************************************************************!*\
  !*** ./src/app/layout/modules/admin-shipping/admin-shipping-routing.module.ts ***!
  \********************************************************************************/
/*! exports provided: AdminShippingRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminShippingRoutingModule", function() { return AdminShippingRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _admin_shipping_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./admin-shipping.component */ "./src/app/layout/modules/admin-shipping/admin-shipping.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '', component: _admin_shipping_component__WEBPACK_IMPORTED_MODULE_2__["AdminShippingComponent"]
    }
];
var AdminShippingRoutingModule = /** @class */ (function () {
    function AdminShippingRoutingModule() {
    }
    AdminShippingRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AdminShippingRoutingModule);
    return AdminShippingRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin-shipping/admin-shipping.component.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/layout/modules/admin-shipping/admin-shipping.component.scss ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container-swapper-option {\n  display: flex;\n  justify-content: center;\n  flex-direction: row;\n  align-items: center;\n}\n.container-swapper-option .button-container {\n  border: 2px solid #ddd;\n  border-radius: 8px;\n}\n.container-swapper-option .button-container button {\n  color: #ddd;\n  background: white;\n  font-size: 14px;\n  border-radius: 8px;\n}\n.container-swapper-option .button-container button.true {\n  background: #2480fb;\n  border-radius: 8px;\n  color: #ddd;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvYWRtaW4tc2hpcHBpbmcvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxtb2R1bGVzXFxhZG1pbi1zaGlwcGluZ1xcYWRtaW4tc2hpcHBpbmcuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2FkbWluLXNoaXBwaW5nL2FkbWluLXNoaXBwaW5nLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQ0NKO0FEQUk7RUFFRSxzQkFBQTtFQUNBLGtCQUFBO0FDQ047QURBTTtFQUNFLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtBQ0VSO0FEQU07RUFFRSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtBQ0NSIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L21vZHVsZXMvYWRtaW4tc2hpcHBpbmcvYWRtaW4tc2hpcHBpbmcuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY29udGFpbmVyLXN3YXBwZXItb3B0aW9uIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgLmJ1dHRvbi1jb250YWluZXIge1xyXG5cclxuICAgICAgYm9yZGVyOiAycHggc29saWQgI2RkZDtcclxuICAgICAgYm9yZGVyLXJhZGl1czo4cHg7XHJcbiAgICAgIGJ1dHRvbiB7XHJcbiAgICAgICAgY29sb3I6ICNkZGQ7XHJcbiAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICAgICAgfVxyXG4gICAgICBidXR0b24udHJ1ZSB7XHJcbiAgICAgICBcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjMjQ4MGZiO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICAgICAgICBjb2xvcjogI2RkZDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH0iLCIuY29udGFpbmVyLXN3YXBwZXItb3B0aW9uIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4uY29udGFpbmVyLXN3YXBwZXItb3B0aW9uIC5idXR0b24tY29udGFpbmVyIHtcbiAgYm9yZGVyOiAycHggc29saWQgI2RkZDtcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xufVxuLmNvbnRhaW5lci1zd2FwcGVyLW9wdGlvbiAuYnV0dG9uLWNvbnRhaW5lciBidXR0b24ge1xuICBjb2xvcjogI2RkZDtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xufVxuLmNvbnRhaW5lci1zd2FwcGVyLW9wdGlvbiAuYnV0dG9uLWNvbnRhaW5lciBidXR0b24udHJ1ZSB7XG4gIGJhY2tncm91bmQ6ICMyNDgwZmI7XG4gIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgY29sb3I6ICNkZGQ7XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/modules/admin-shipping/admin-shipping.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/layout/modules/admin-shipping/admin-shipping.component.ts ***!
  \***************************************************************************/
/*! exports provided: AdminShippingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminShippingComponent", function() { return AdminShippingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};


var AdminShippingComponent = /** @class */ (function () {
    function AdminShippingComponent(OrderhistoryService) {
        this.OrderhistoryService = OrderhistoryService;
        this.Pointstransaction = [];
        this.tableFormat = {
            title: 'Shipping Report Page',
            label_headers: [
                { label: 'Tgl. Order', visible: true, type: 'date', data_row_name: 'created_date' },
                { label: 'No. Order', visible: true, type: 'string', data_row_name: 'invoice_id' },
                { label: 'No. Resi', visible: true, type: 'string', data_row_name: 'awb_number' },
                { label: 'Destination', visible: true, type: 'string', data_row_name: 'destination' },
                { label: 'Member ID', visible: true, type: 'string', data_row_name: 'member_email' },
                { label: 'Name', visible: true, type: 'string', data_row_name: 'member_name' },
                { label: 'Item Code', visible: true, type: 'productcode', data_row_name: 'products' },
                // {label: 'Item Description', visible: true, type: 'productdesc', data_row_name: 'products'},
                { label: 'Category', visible: true, type: 'productcat', data_row_name: 'products' },
                { label: 'Weight', visible: true, type: 'productweight', data_row_name: 'products' },
                { label: 'Dimensi', visible: true, type: 'productdim', data_row_name: 'products' },
                { label: 'Qty', visible: true, type: 'productqty', data_row_name: 'products' },
                { label: 'Unit Price', visible: true, type: 'productunit', data_row_name: 'products' },
                { label: 'Total Order', visible: true, type: 'producttotal', data_row_name: 'products' },
                // {label: 'Invoice ID', visible: true, type: 'string', data_row_name: 'invoice_id'},
                { label: 'Shipping Via', visible: true, type: 'string', data_row_name: 'courier' },
                { label: 'Shipping Cost', visible: true, type: 'string', data_row_name: 'shipping_fee' },
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: '_id',
                this: this,
                result_var_name: 'Pointstransaction',
                detail_function: [this, 'callDetail']
            }
        };
        this.form_input = {};
        this.errorLabel = false;
        this.totalPage = 0;
        this.swaper = true;
        this.pointstransactionDetail = false;
    }
    AdminShippingComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    AdminShippingComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, params, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 5, , 6]);
                        result = void 0;
                        params = {
                            search: { start_date: this.addMonths(new Date(), -6), end_date: new Date() },
                            current_page: 1,
                            limit_per_page: 50
                        };
                        this.service = this.OrderhistoryService;
                        if (!(this.swaper == true)) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.OrderhistoryService.getShippingReportint('product')];
                    case 1:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 2: return [4 /*yield*/, this.OrderhistoryService.getShippingReportint('order_id')];
                    case 3:
                        result = _a.sent();
                        _a.label = 4;
                    case 4:
                        // result = await this.OrderhistoryService.getShippingReportint(params);
                        this.totalPage = result.result.total_page;
                        this.Pointstransaction = result.result.values;
                        return [3 /*break*/, 6];
                    case 5:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    AdminShippingComponent.prototype.swapClick = function (bool) {
        this.swaper = bool;
        this.firstLoad();
    };
    // public async callDetail(pointstransaction_id){
    //   try{
    //     let result: any;
    //     this.service    = this.OrderhistoryService;
    //     result          = await this.OrderhistoryService.detailPointstransaction(pointstransaction_id);
    //     console.log(result);
    //     this.pointstransactionDetail = result.result[0];
    //   } catch (e) {
    //     this.errorLabel = ((<Error>e).message);//conversion to Error type
    //   }
    // }
    AdminShippingComponent.prototype.backToHere = function (obj) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                obj.pointstransactionDetail = false;
                return [2 /*return*/];
            });
        });
    };
    AdminShippingComponent.prototype.addMonths = function (date, months) {
        date.setMonth(date.getMonth() + months);
        return date;
    };
    AdminShippingComponent.ctorParameters = function () { return [
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_1__["OrderhistoryService"] }
    ]; };
    AdminShippingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-admin-shipping',
            template: __webpack_require__(/*! raw-loader!./admin-shipping.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin-shipping/admin-shipping.component.html"),
            styles: [__webpack_require__(/*! ./admin-shipping.component.scss */ "./src/app/layout/modules/admin-shipping/admin-shipping.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_1__["OrderhistoryService"]])
    ], AdminShippingComponent);
    return AdminShippingComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin-shipping/admin-shipping.module.ts":
/*!************************************************************************!*\
  !*** ./src/app/layout/modules/admin-shipping/admin-shipping.module.ts ***!
  \************************************************************************/
/*! exports provided: AdminShippingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminShippingModule", function() { return AdminShippingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _admin_shipping_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./admin-shipping-routing.module */ "./src/app/layout/modules/admin-shipping/admin-shipping-routing.module.ts");
/* harmony import */ var _admin_shipping_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./admin-shipping.component */ "./src/app/layout/modules/admin-shipping/admin-shipping.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var AdminShippingModule = /** @class */ (function () {
    function AdminShippingModule() {
    }
    AdminShippingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _admin_shipping_routing_module__WEBPACK_IMPORTED_MODULE_2__["AdminShippingRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_4__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["NgbModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__["FormBuilderTableModule"],
                _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__["BsComponentModule"]
            ],
            declarations: [_admin_shipping_component__WEBPACK_IMPORTED_MODULE_3__["AdminShippingComponent"]],
        })
    ], AdminShippingModule);
    return AdminShippingModule;
}());



/***/ })

}]);
//# sourceMappingURL=modules-admin-shipping-admin-shipping-module.js.map