(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~merchant-portal-merchant-portal-module~modules-evouchersalesreport-evoucher-sales-report-evo~7231c304"],{

/***/ "./src/app/services/e-voucher/e-voucher.service.ts":
/*!*********************************************************!*\
  !*** ./src/app/services/e-voucher/e-voucher.service.ts ***!
  \*********************************************************/
/*! exports provided: EVoucherService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EVoucherService", function() { return EVoucherService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _master_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../master.service */ "./src/app/services/master.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var EVoucherService = /** @class */ (function () {
    function EVoucherService(http, myService) {
        this.http = http;
        this.myService = myService;
        this.api_url = '';
        this.httpReq = false;
        this.api_url = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["api_url"])();
    }
    EVoucherService.prototype.detailEVoucher = function (evoucher_id) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'evoucher/detail/' + evoucher_id;
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_1 = _a.sent();
                        throw new TypeError(error_1);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EVoucherService.prototype.getEVoucherLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'evouchers/report';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_2 = _a.sent();
                        throw new TypeError(error_2);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EVoucherService.prototype.searchEvouchersLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log('params', params);
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'evouchers/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log('evouchers', result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_3 = _a.sent();
                        throw new TypeError(error_3);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EVoucherService.prototype.getEvoucherList = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'evoucher/generated-vouchers/report/' + params.id;
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_4 = _a.sent();
                        throw new TypeError(error_4);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EVoucherService.prototype.getEvoucherStockSummary = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        // let params = {'id': 'voucher-1'}
                        if (!params.search) {
                            params.search = {
                                ev_product_code: params.id
                            };
                        }
                        console.log("params_id", params);
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'evouchers/stock-summary';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_5 = _a.sent();
                        throw new TypeError(error_5);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EVoucherService.prototype.searchEvoucherStockSummary = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'evouchers/stock-summary';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_6 = _a.sent();
                        throw new TypeError(error_6);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EVoucherService.prototype.getEvoucherSummaryReport = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_7;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        ;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'evoucher/summary/report';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_7 = _a.sent();
                        throw new TypeError(error_7);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EVoucherService.prototype.searchEvoucherSummaryReport = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_8;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        ;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'evoucher/summary/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_8 = _a.sent();
                        throw new TypeError(error_8);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EVoucherService.prototype.getEvoucherStockReport = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, urlRequested, urlApproved, error_9;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = '';
                        urlRequested = 'evoucher/stock/report?request={"search":{"status": "REQUESTED"},"limit_per_page":50,"current_page":1}';
                        urlApproved = 'evoucher/stock/report?request={"search":{"status": "APPROVED"},"limit_per_page":50,"current_page":1}';
                        if (params == 'REQUESTED') {
                            url = urlRequested;
                        }
                        else {
                            url = urlApproved;
                        }
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_9 = _a.sent();
                        throw new TypeError(error_9);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EVoucherService.prototype.searchEvoucherStockReport = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_10;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params.search.status = "REQUESTED";
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        ;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'evoucher/stock/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_10 = _a.sent();
                        throw new TypeError(error_10);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EVoucherService.prototype.searchEvoucherStockReport2 = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_11;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params.search.status = "APPROVED";
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        ;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'evoucher/stock/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_11 = _a.sent();
                        throw new TypeError(error_11);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EVoucherService.prototype.detailEvoucherStock = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_12;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        ;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = "evoucher/stock/detail/" + params;
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_12 = _a.sent();
                        throw new TypeError(error_12);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EVoucherService.prototype.processEvoucherCode = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_13;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        ;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = "evoucher/stock/process";
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_13 = _a.sent();
                        throw new TypeError(error_13);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EVoucherService.prototype.uploadEvoucherCodeBulk = function (file, obj, payload) {
        return __awaiter(this, void 0, void 0, function () {
            var myStatus, myToken, fd, url, result;
            var _this = this;
            return __generator(this, function (_a) {
                myStatus = localStorage.getItem('isLoggedin');
                if (myStatus) {
                    myToken = localStorage.getItem('tokenlogin');
                }
                fd = new FormData();
                //fd.append('image', file, file.name)
                fd.append('type', payload.type);
                fd.append('document', file);
                url = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["api_url"])() + 'evoucher/stock/upload';
                console.log('now', fd);
                this.httpReq = this.http.post(url, fd, {
                    headers: Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])(),
                    // headers : new HttpHeaders({
                    //   'Authorization': myToken,
                    //   'app-label': 'retailer_prg'
                    // }),
                    reportProgress: true,
                    observe: 'events'
                }).subscribe(function (event) { return __awaiter(_this, void 0, void 0, function () {
                    var c, prgval;
                    return __generator(this, function (_a) {
                        console.log("event", event);
                        console.log(event);
                        c = JSON.parse(JSON.stringify(event));
                        if (c.body) {
                            console.log('this is body');
                            //obj.firstLoad();
                            console.warn("c body", c.body);
                            obj.Report = c.body._id;
                            // obj.prodOnUpload = true;
                            if (c.body && c.body.status == "success" && obj.Report) {
                                obj.selectedFile = null;
                                obj.startUploading = false;
                                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Success!', 'Your file has been uploaded.', 'success');
                            }
                            //untuk mengecek data pertama kali dan resultnya data uploadan terbaru
                            // obj.process_id = c.body;
                            // console.log("process_id",obj.Report);
                            // obj.prodOnUpload = true;
                            // console.log(c.body.failed);
                            // obj.failed = c.body.failed.length;
                        }
                        if (event.type == _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpEventType"].UploadProgress) {
                            prgval = Math.round(event.loaded / event.total * 100);
                            obj.updateProgressBar(prgval);
                            //console.log(obj.cancel);
                            if (prgval == 100) {
                                console.log(event);
                            }
                            if (obj.cancel == true) {
                                this.httpReq.unsubscribe();
                            }
                            // if(prgval == 100){
                            //   if(obj.onUploadSuccess) 
                            //     obj.onUploadSuccess();
                            // }
                            console.log('upload Progress: ' + prgval + "%");
                        }
                        return [2 /*return*/];
                    });
                }); }, function (result) {
                    // console.warn("RESULT", result);
                    if (result.error) {
                        obj.errorFile = result.error.error;
                        obj.cancel = true;
                        obj.selectedFile = null;
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Failed!', result.error.error, 'warning');
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    // 
    // 
    // 
    EVoucherService.prototype.updateEvoucher = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_14;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'evouchers/report' + params._id;
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        // console.log(params);
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_14 = _a.sent();
                        throw new TypeError(error_14);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EVoucherService.prototype.getEvoucherSalesOrderReport = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, params, customHeaders, url, error_15;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params = {
                            search: {
                                type: 'evoucher'
                            }
                        };
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'sales-order/find';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log("resultnya om", result.result.values);
                        return [3 /*break*/, 4];
                    case 3:
                        error_15 = _a.sent();
                        throw new TypeError(error_15);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EVoucherService.prototype.searchEvoucherSalesOrderLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_16;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log('params', params);
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'sales-order/find';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log('test', result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_16 = _a.sent();
                        throw new TypeError(error_16);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EVoucherService.prototype.upload = function (file, obj) {
        return __awaiter(this, void 0, void 0, function () {
            var fd, url, myToken;
            var _this = this;
            return __generator(this, function (_a) {
                fd = new FormData();
                fd.append('image', file, file.name);
                fd.append('type', 'evoucher');
                url = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["api_url"])() + 'evoucher/upload';
                console.log(url);
                myToken = localStorage.getItem('tokenlogin');
                this.httpReq = this.http.post(url, fd, {
                    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                        'Authorization': myToken,
                    }),
                    reportProgress: true,
                    observe: 'events'
                }).subscribe(function (event) {
                    console.log(event);
                    if (event.type == _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpEventType"].UploadProgress) {
                        var prgval = Math.round(event.loaded / event.total * 100);
                        obj.updateProgressBar(prgval);
                        console.log(obj.cancel);
                        if (obj.cancel == true) {
                            _this.httpReq.unsubscribe();
                        }
                        console.log('Upload Progress: ' + prgval + "%");
                    }
                }, function (result) {
                    console.log(result);
                    if (result.error) {
                        obj.errorFile = result.error.error;
                        obj.cancel = true;
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    EVoucherService.prototype.getDropdown = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_17;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'products/evoucher';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_17 = _a.sent();
                        throw new TypeError(error_17);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EVoucherService.prototype.generateEVoucherLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_18;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'evoucher/generate';
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_18 = _a.sent();
                        throw new TypeError(error_18);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EVoucherService.prototype.getCategoryLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_19;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'category/all';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_19 = _a.sent();
                        throw new TypeError(error_19);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EVoucherService.prototype.approvedEvoucher = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_20;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'evoucher/set_active';
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_20 = _a.sent();
                        throw new TypeError(error_20);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EVoucherService.prototype.config = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_21;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'configs/keyname/merchant-group-configuration';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_21 = _a.sent();
                        throw new TypeError(error_21);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    EVoucherService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] },
        { type: _master_service__WEBPACK_IMPORTED_MODULE_3__["MasterService"] }
    ]; };
    EVoucherService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root',
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _master_service__WEBPACK_IMPORTED_MODULE_3__["MasterService"]])
    ], EVoucherService);
    return EVoucherService;
}());



/***/ })

}]);
//# sourceMappingURL=default~merchant-portal-merchant-portal-module~modules-evouchersalesreport-evoucher-sales-report-evo~7231c304.js.map