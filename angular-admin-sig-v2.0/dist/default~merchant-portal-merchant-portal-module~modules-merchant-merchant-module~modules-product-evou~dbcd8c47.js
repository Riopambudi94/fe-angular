(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~merchant-portal-merchant-portal-module~modules-merchant-merchant-module~modules-product-evou~dbcd8c47"],{

/***/ "./src/app/services/merchant/merchant.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/services/merchant/merchant.service.ts ***!
  \*******************************************************/
/*! exports provided: MerchantService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MerchantService", function() { return MerchantService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _master_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../master.service */ "./src/app/services/master.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var MerchantService = /** @class */ (function () {
    function MerchantService(http, myService) {
        this.http = http;
        this.myService = myService;
        this.api_url = '';
        this.httpReq = false;
        this.api_url = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["api_url"])();
    }
    MerchantService.prototype.getDetail = function (params) {
        if (params === void 0) { params = {}; }
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'merchant/detail';
                        return [4 /*yield*/, this.myService.get(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_1 = _a.sent();
                        throw new TypeError(error_1);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MerchantService.prototype.detailMerchant = function (merchant_username, params) {
        if (params === void 0) { params = {}; }
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'merchant/detail' + merchant_username;
                        return [4 /*yield*/, this.myService.get(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_2 = _a.sent();
                        throw new TypeError(error_2);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MerchantService.prototype.getMerchant = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("params", params);
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'merchant/find';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_3 = _a.sent();
                        throw new TypeError(error_3);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MerchantService.prototype.searchMerchant = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'merchant/all';
                        params.type = 'merchant';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_4 = _a.sent();
                        throw new TypeError(error_4);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MerchantService.prototype.showMerchant = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'merchant/report';
                        return [4 /*yield*/, this.myService.get(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_5 = _a.sent();
                        throw new TypeError(error_5);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MerchantService.prototype.addMerchant = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'merchant/add';
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_6 = _a.sent();
                        throw new TypeError(error_6);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MerchantService.prototype.setMDR = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_7;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'merchant/set_mdr';
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_7 = _a.sent();
                        throw new TypeError(error_7);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MerchantService.prototype.merchantInfo = function (merchant_username, params) {
        if (params === void 0) { params = {}; }
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_8;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'merchant/info/' + encodeURIComponent(merchant_username);
                        return [4 /*yield*/, this.myService.get(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_8 = _a.sent();
                        throw new TypeError(error_8);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MerchantService.prototype.generateProductsLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_9;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'generate';
                        return [4 /*yield*/, this.myService.add(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_9 = _a.sent();
                        throw new TypeError(error_9);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MerchantService.prototype.updateMerchant = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_10;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'merchant/update/' + params.merchant_username;
                        console.log(params);
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_10 = _a.sent();
                        throw new TypeError(error_10);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MerchantService.prototype.deleteMerchant = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_11;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'merchant/' + params.merchant_username;
                        return [4 /*yield*/, this.myService.delete(params, url, customHeaders)];
                    case 2:
                        // console.log(params);
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_11 = _a.sent();
                        throw new TypeError(error_11);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MerchantService.prototype.configuration = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_12;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'configs/keyname/merchant-group-configuration';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        // console.log(params);
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_12 = _a.sent();
                        throw new TypeError(error_12);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MerchantService.prototype.getVoucherGroup = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_13;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'configs/keyname/products-configuration';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        // console.log(params);
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_13 = _a.sent();
                        throw new TypeError(error_13);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MerchantService.prototype.getCourier = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_14;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'courier/list';
                        console.log("hasil", url);
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_14 = _a.sent();
                        throw new TypeError(error_14);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    // public async getCourier() {
    //   let result;
    //   const customHeaders = getTokenHeader();
    //   try {
    //     let url = 'courier/list';
    //     // console.log(params);
    //     result = await this.myService.get(null, url, customHeaders);
    //   } catch (error) {
    //     throw new TypeError(error);
    //   }
    //   return result;
    // }
    // public async getMerchantGroup(){
    //   let result;
    //   const customHeaders = getTokenHeader();
    //   try{
    //     let url = 'merchant/merchant-group';
    //     // console.log(params);
    //     result = await this.myService.get(null, url, customHeaders);
    //   } catch (error) {
    //     throw new TypeError(error);
    //   }
    //   return  result;
    // }
    MerchantService.prototype.statusEdit = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_15;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'products/set_status';
                        return [4 /*yield*/, this.myService.post(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_15 = _a.sent();
                        throw new TypeError(error_15);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MerchantService.prototype.getRegion = function (match, usage) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_16;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'shippingregion/searchregion';
                        return [4 /*yield*/, this.myService.get({ match: match, usage: usage }, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_16 = _a.sent();
                        throw new TypeError(error_16);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MerchantService.prototype.getOutlet = function (outlet_id) {
        if (outlet_id === void 0) { outlet_id = {}; }
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_17;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'merchant/find_outlet/';
                        return [4 /*yield*/, this.myService.get(outlet_id, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_17 = _a.sent();
                        throw new TypeError(error_17);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    MerchantService.prototype.upload = function (file, obj, fileType, funcOnFinish) {
        return __awaiter(this, void 0, void 0, function () {
            var fd, url, myToken;
            var _this = this;
            return __generator(this, function (_a) {
                fd = new FormData();
                fd.append('image', file, file.name);
                if (fileType == 'product') {
                    fd.append('type', 'product');
                }
                else if (fileType == 'image') {
                    fd.append('type', 'image');
                }
                url = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["api_url"])() + 'media/upload';
                myToken = localStorage.getItem('tokenlogin');
                this.httpReq = this.http.post(url, fd, {
                    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                        'Authorization': myToken,
                    }),
                    reportProgress: true,
                    observe: 'events'
                }).subscribe(function (event) {
                    // console.log(event);
                    if (event.type == _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpEventType"].UploadProgress) {
                        var prgval = Math.round(event.loaded / event.total * 100);
                        // obj.updateProgressBar(prgval);
                        // console.log(obj.cancel);
                        if (obj.cancel == true) {
                            _this.httpReq.unsubscribe();
                        }
                        console.log('Upload Progress: ' + prgval + "%", event);
                    }
                    if (event["body"] != undefined) {
                        console.log("EVENT STATUS body", event['body']);
                        funcOnFinish(event['body'].result);
                    }
                }, function (result) {
                    console.log("ERROR result", result);
                    if (result.error) {
                        obj.errorFile = result.error.error;
                        obj.cancel = true;
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    MerchantService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] },
        { type: _master_service__WEBPACK_IMPORTED_MODULE_3__["MasterService"] }
    ]; };
    MerchantService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root',
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _master_service__WEBPACK_IMPORTED_MODULE_3__["MasterService"]])
    ], MerchantService);
    return MerchantService;
}());



/***/ })

}]);
//# sourceMappingURL=default~merchant-portal-merchant-portal-module~modules-merchant-merchant-module~modules-product-evou~dbcd8c47.js.map