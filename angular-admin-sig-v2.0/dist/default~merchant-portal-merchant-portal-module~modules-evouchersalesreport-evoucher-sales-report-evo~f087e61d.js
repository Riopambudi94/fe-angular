(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~merchant-portal-merchant-portal-module~modules-evouchersalesreport-evoucher-sales-report-evo~f087e61d"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/merchant-evoucher-sales-report/merchant-evoucher-sales-report.component.html":
/*!***********************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/merchant-portal/merchant-evoucher-sales-report/merchant-evoucher-sales-report.component.html ***!
  \***********************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <app-form-builder-table \r\n      [table_data]=\"generatedVouchers\" \r\n      [searchCallback]=\"[service,'searchProductsLint',this]\"\r\n      [tableFormat]=\"tableFormat\">\r\n</app-form-builder-table> -->\r\n<app-form-builder-table \r\n    [tableFormat]=\"tableFormat\"\r\n    [table_data]=\"evouchers\"\r\n    [searchCallback]=\"[service, 'searchEvoucherSalesOrderLint', this]\"\r\n    >\r\n\r\n</app-form-builder-table>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/evouchersalesreport/evoucher-sales-report/evoucher-sales-report.component.html":
/*!*****************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/evouchersalesreport/evoucher-sales-report/evoucher-sales-report.component.html ***!
  \*****************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"form\">\r\n        <app-merchant-evoucher-sales-report></app-merchant-evoucher-sales-report>\r\n</div>"

/***/ }),

/***/ "./src/app/layout/merchant-portal/merchant-evoucher-sales-report/merchant-evoucher-sales-report.component.scss":
/*!*********************************************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/merchant-evoucher-sales-report/merchant-evoucher-sales-report.component.scss ***!
  \*********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tZXJjaGFudC1wb3J0YWwvbWVyY2hhbnQtZXZvdWNoZXItc2FsZXMtcmVwb3J0L21lcmNoYW50LWV2b3VjaGVyLXNhbGVzLXJlcG9ydC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/layout/merchant-portal/merchant-evoucher-sales-report/merchant-evoucher-sales-report.component.ts":
/*!*******************************************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/merchant-evoucher-sales-report/merchant-evoucher-sales-report.component.ts ***!
  \*******************************************************************************************************************/
/*! exports provided: MerchantEvoucherSalesReportComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MerchantEvoucherSalesReportComponent", function() { return MerchantEvoucherSalesReportComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _object_interface_common_object__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../object-interface/common.object */ "./src/app/object-interface/common.object.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/e-voucher/e-voucher.service */ "./src/app/services/e-voucher/e-voucher.service.ts");
/* harmony import */ var _services_observerable_permission_observer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/observerable/permission-observer */ "./src/app/services/observerable/permission-observer.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var MerchantEvoucherSalesReportComponent = /** @class */ (function () {
    function MerchantEvoucherSalesReportComponent(route, permission, evoucherService, router) {
        var _this = this;
        this.route = route;
        this.permission = permission;
        this.evoucherService = evoucherService;
        this.router = router;
        this.evouchers = [];
        this.errorLabel = false;
        this.service = this.evoucherService;
        this.tableFormat = Object(_object_interface_common_object__WEBPACK_IMPORTED_MODULE_1__["eVouchersSalesReportTableFormat"])(this);
        this.tableFormat.label_headers.splice(0, 1);
        this.tableFormat.formOptions.addForm = false;
        delete this.tableFormat.formOptions.customButtons;
        this.permission.currentPermission.subscribe(function (val) {
            _this.currentPermission = val;
        });
    }
    MerchantEvoucherSalesReportComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    MerchantEvoucherSalesReportComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.evoucherService.getEvoucherSalesOrderReport()];
                    case 1:
                        result = _a.sent();
                        console.log("test", result);
                        this.evouchers = result.result.values;
                        return [2 /*return*/];
                }
            });
        });
    };
    MerchantEvoucherSalesReportComponent.prototype.callDetail = function (evoucher_id) {
        if (this.currentPermission == 'admin') {
            this.router.navigate(['administrator/productadmin/edit'], { queryParams: { id: evoucher_id } });
        }
        else if (this.currentPermission == 'merchant') {
            this.router.navigate(['merchant-portal/product-list/edit'], { queryParams: { id: evoucher_id } });
        }
    };
    MerchantEvoucherSalesReportComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: _services_observerable_permission_observer__WEBPACK_IMPORTED_MODULE_4__["PermissionObserver"] },
        { type: _services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_3__["EVoucherService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    MerchantEvoucherSalesReportComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-merchant-evoucher-sales-report',
            template: __webpack_require__(/*! raw-loader!./merchant-evoucher-sales-report.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/merchant-evoucher-sales-report/merchant-evoucher-sales-report.component.html"),
            styles: [__webpack_require__(/*! ./merchant-evoucher-sales-report.component.scss */ "./src/app/layout/merchant-portal/merchant-evoucher-sales-report/merchant-evoucher-sales-report.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _services_observerable_permission_observer__WEBPACK_IMPORTED_MODULE_4__["PermissionObserver"],
            _services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_3__["EVoucherService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], MerchantEvoucherSalesReportComponent);
    return MerchantEvoucherSalesReportComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/evouchersalesreport/evoucher-sales-report/evoucher-sales-report-routing.module.ts":
/*!******************************************************************************************************************!*\
  !*** ./src/app/layout/modules/evouchersalesreport/evoucher-sales-report/evoucher-sales-report-routing.module.ts ***!
  \******************************************************************************************************************/
/*! exports provided: EvoucherSalesReportRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EvoucherSalesReportRoutingModule", function() { return EvoucherSalesReportRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _merchant_portal_merchant_evoucher_sales_report_merchant_evoucher_sales_report_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../merchant-portal/merchant-evoucher-sales-report/merchant-evoucher-sales-report.component */ "./src/app/layout/merchant-portal/merchant-evoucher-sales-report/merchant-evoucher-sales-report.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    { path: '', component: _merchant_portal_merchant_evoucher_sales_report_merchant_evoucher_sales_report_component__WEBPACK_IMPORTED_MODULE_2__["MerchantEvoucherSalesReportComponent"] }
];
var EvoucherSalesReportRoutingModule = /** @class */ (function () {
    function EvoucherSalesReportRoutingModule() {
    }
    EvoucherSalesReportRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], EvoucherSalesReportRoutingModule);
    return EvoucherSalesReportRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/evouchersalesreport/evoucher-sales-report/evoucher-sales-report.component.scss":
/*!***************************************************************************************************************!*\
  !*** ./src/app/layout/modules/evouchersalesreport/evoucher-sales-report/evoucher-sales-report.component.scss ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".form {\n  margin-top: 60px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZXZvdWNoZXJzYWxlc3JlcG9ydC9ldm91Y2hlci1zYWxlcy1yZXBvcnQvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxtb2R1bGVzXFxldm91Y2hlcnNhbGVzcmVwb3J0XFxldm91Y2hlci1zYWxlcy1yZXBvcnRcXGV2b3VjaGVyLXNhbGVzLXJlcG9ydC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZXZvdWNoZXJzYWxlc3JlcG9ydC9ldm91Y2hlci1zYWxlcy1yZXBvcnQvZXZvdWNoZXItc2FsZXMtcmVwb3J0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksZ0JBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2V2b3VjaGVyc2FsZXNyZXBvcnQvZXZvdWNoZXItc2FsZXMtcmVwb3J0L2V2b3VjaGVyLXNhbGVzLXJlcG9ydC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5mb3Jte1xyXG4gICAgbWFyZ2luLXRvcCA6IDYwcHg7XHJcbn0iLCIuZm9ybSB7XG4gIG1hcmdpbi10b3A6IDYwcHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/modules/evouchersalesreport/evoucher-sales-report/evoucher-sales-report.component.ts":
/*!*************************************************************************************************************!*\
  !*** ./src/app/layout/modules/evouchersalesreport/evoucher-sales-report/evoucher-sales-report.component.ts ***!
  \*************************************************************************************************************/
/*! exports provided: EvoucherSalesReportComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EvoucherSalesReportComponent", function() { return EvoucherSalesReportComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var EvoucherSalesReportComponent = /** @class */ (function () {
    function EvoucherSalesReportComponent() {
    }
    // service: any;
    // tableFormat : TableFormat;
    // errorLabel : any = false
    // constructor(private route:ActivatedRoute, 
    //             private evoucherService: EVoucherService,
    //             private router:Router) {
    //               console.log("HI");
    //               this.service      = this.evoucherService;
    //               this.tableFormat  = eVouchersSalesReportTableFormat(this)
    //               this.tableFormat.label_headers.splice(0,1)
    //               this.tableFormat.formOptions.addForm = false;
    //               delete this.tableFormat.formOptions.customButtons;
    //              }
    EvoucherSalesReportComponent.prototype.ngOnInit = function () {
        // console.log("Test");
    };
    EvoucherSalesReportComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-evoucher-sales-report',
            template: __webpack_require__(/*! raw-loader!./evoucher-sales-report.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/evouchersalesreport/evoucher-sales-report/evoucher-sales-report.component.html"),
            styles: [__webpack_require__(/*! ./evoucher-sales-report.component.scss */ "./src/app/layout/modules/evouchersalesreport/evoucher-sales-report/evoucher-sales-report.component.scss")]
        })
    ], EvoucherSalesReportComponent);
    return EvoucherSalesReportComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/evouchersalesreport/evoucher-sales-report/evoucher-sales-report.module.ts":
/*!**********************************************************************************************************!*\
  !*** ./src/app/layout/modules/evouchersalesreport/evoucher-sales-report/evoucher-sales-report.module.ts ***!
  \**********************************************************************************************************/
/*! exports provided: EvoucherSalesReportModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EvoucherSalesReportModule", function() { return EvoucherSalesReportModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _evoucher_sales_report_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./evoucher-sales-report-routing.module */ "./src/app/layout/modules/evouchersalesreport/evoucher-sales-report/evoucher-sales-report-routing.module.ts");
/* harmony import */ var _evoucher_sales_report_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./evoucher-sales-report.component */ "./src/app/layout/modules/evouchersalesreport/evoucher-sales-report/evoucher-sales-report.component.ts");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _merchant_portal_merchant_evoucher_sales_report_merchant_evoucher_sales_report_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../merchant-portal/merchant-evoucher-sales-report/merchant-evoucher-sales-report.component */ "./src/app/layout/merchant-portal/merchant-evoucher-sales-report/merchant-evoucher-sales-report.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var EvoucherSalesReportModule = /** @class */ (function () {
    function EvoucherSalesReportModule() {
    }
    EvoucherSalesReportModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _evoucher_sales_report_routing_module__WEBPACK_IMPORTED_MODULE_2__["EvoucherSalesReportRoutingModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_4__["FormBuilderTableModule"]
            ],
            declarations: [
                _evoucher_sales_report_component__WEBPACK_IMPORTED_MODULE_3__["EvoucherSalesReportComponent"],
                _merchant_portal_merchant_evoucher_sales_report_merchant_evoucher_sales_report_component__WEBPACK_IMPORTED_MODULE_5__["MerchantEvoucherSalesReportComponent"]
            ],
            exports: [
                _evoucher_sales_report_component__WEBPACK_IMPORTED_MODULE_3__["EvoucherSalesReportComponent"],
                _merchant_portal_merchant_evoucher_sales_report_merchant_evoucher_sales_report_component__WEBPACK_IMPORTED_MODULE_5__["MerchantEvoucherSalesReportComponent"]
            ]
        })
    ], EvoucherSalesReportModule);
    return EvoucherSalesReportModule;
}());



/***/ })

}]);
//# sourceMappingURL=default~merchant-portal-merchant-portal-module~modules-evouchersalesreport-evoucher-sales-report-evo~f087e61d.js.map