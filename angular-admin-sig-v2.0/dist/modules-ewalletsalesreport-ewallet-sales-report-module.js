(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-ewalletsalesreport-ewallet-sales-report-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/ewalletsalesreport/detail/ewallet-sales-report.detail.component.html":
/*!*******************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/ewalletsalesreport/detail/ewallet-sales-report.detail.component.html ***!
  \*******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"salesRedemptionDetail\" class=\"card card-detail\">\r\n    <div class=\"card-header\">\r\n        <button class=\"btn back_button\" (click)=\"backToHere()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button>\r\n    </div>\r\n\r\n    <ng-template #empty_image><span>\r\n        empty\r\n    </span></ng-template>\r\n  \r\n    <div class=\"card-content\">\r\n        <div class=\"member-detail\">\r\n    \r\n            <div class=\"row\">\r\n                <div class=\"col-md-4\">\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Data Order</h2></div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">\r\n                                <label>Tanggal Proses</label>\r\n                                <div name=\"order_id\">{{salesRedemptionDetail.approve_date}}</div>\r\n\r\n                                <label>Order ID</label>\r\n                                <div name=\"order_id\">{{salesRedemptionDetail.order_id}}</div>\r\n\r\n                                <label>Total Quantity</label>\r\n                                <div name=\"member_id\">{{salesRedemptionDetail.total_quantity}}</div>\r\n\r\n                                <label>Total Points Redeem</label>\r\n                                <div name=\"member_id\">{{salesRedemptionDetail.sum_total}}</div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\"col-md-4\">\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Data Pelanggan</h2></div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">             \r\n                                <label>ID Pelanggan</label>\r\n                                <div name=\"order_id\">{{salesRedemptionDetail.id_toko}}</div>\r\n\r\n                                <label>Nama Entitas</label>\r\n                                <div name=\"member_id\">{{salesRedemptionDetail.nama_toko}}</div>\r\n\r\n                                <label>Nama PIC</label>\r\n                                <div name=\"member_id\">{{salesRedemptionDetail.nama_pemilik}}</div>\r\n\r\n                                <label>Alamat Kirim</label>\r\n                                <div name=\"member_id\">{{salesRedemptionDetail.address_detail.address}}</div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\"col-md-4\">\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Status Order</h2></div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">             \r\n                                <label>Status</label>\r\n                                <div name=\"member_id\">{{salesRedemptionDetail.status}}</div>\r\n                                \r\n                                <!-- <div class=\"need-approval-text\" *ngIf=\"salesRedemptionDetail.status == 'REQUESTED' && !processNow && !declineNow\">\r\n                                    <span>This request needs approval</span>\r\n                                </div> -->\r\n\r\n                                <div class=\"need-approval-text\" *ngIf=\"salesRedemptionDetail.status == 'PROCESSED' && processNow && !declineNow\">\r\n                                    <span>Are you sure to complete this order?</span>\r\n                                </div>\r\n\r\n                                <!-- <div class=\"need-approval-text\" *ngIf=\"salesRedemptionDetail.status == 'REQUESTED' && !processNow && declineNow\">\r\n                                    <span>Are you sure to decline this request?</span>\r\n                                </div> -->\r\n\r\n                                <div style=\"display: flex;\">\r\n                                    <div class=\"btn-action-stock\" *ngIf=\"salesRedemptionDetail.status == 'PROCESSED' && !processNow && !declineNow\">\r\n                                        <button class=\"btn btn-approve\" (click)=\"isProcessNow()\">Complete this Order</button>\r\n                                    </div>\r\n    \r\n                                    <!-- <div class=\"btn-action-stock\" *ngIf=\"salesRedemptionDetail.status == 'REQUESTED' && !processNow && !declineNow\">\r\n                                        <button class=\"btn btn-decline\" (click)=\"isDeclineNow()\">Decline</button>\r\n                                    </div> -->\r\n                                </div>\r\n\r\n                                <div class=\"btn-action-stock\" *ngIf=\"salesRedemptionDetail.status == 'PROCESSED' && processNow && !declineNow\">\r\n                                    <button class=\"btn btn-approve\" (click)=\"completeOrder(salesRedemptionDetail.order_id)\">Complete Now</button>\r\n                                    <button class=\"btn btn-cancel\" (click)=\"isProcessNow()\">Cancel</button>\r\n                                </div>\r\n\r\n                                <!-- <div class=\"btn-action-stock\" *ngIf=\"salesRedemptionDetail.status == 'REQUESTED' && !processNow && declineNow\">\r\n                                    <button class=\"btn btn-decline\" (click)=\"completeOrder(salesRedemptionDetail.order_id, 'cancel')\">Decline Now</button>\r\n                                    <button class=\"btn btn-cancel\" (click)=\"isDeclineNow()\">Cancel</button>\r\n                                </div> -->\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n\r\n                </div>\r\n               \r\n            </div>\r\n         </div>\r\n    </div>\r\n    \r\n</div>\r\n\r\n<div *ngIf=\"salesRedemption&&redemptionDetail==false &&!errorMessage\">\r\n    <app-form-builder-table \r\n    [tableFormat] = \"tableFormat\" \r\n    [table_data]  = \"salesRedemption\" \r\n    [searchCallback]= \"[service, 'searchRedemptionReportint',this]\" \r\n    [total_page]=\"totalPage\"\r\n    >\r\n    </app-form-builder-table>\r\n</div>\r\n\r\n<div *ngIf=\"showLoadingSpinner\" id=\"cover-spin\">\r\n    <div class=\"spin-content\">\r\n        <div class=\"spin-text\">processing ...</div>\r\n        <progressbar [max]=\"processTotal\" [value]=\"processCount\" [striped]=\"true\" [animate]=\"true\"><i>{{processCount}} / {{processTotal}}</i></progressbar>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/ewalletsalesreport/ewallet-sales-report.component.html":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/ewalletsalesreport/ewallet-sales-report.component.html ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div >\r\n    <app-page-header [heading]=\"'&nbsp;'\" [icon]=\"'fa-table'\"></app-page-header>\r\n      \r\n      <div class=\"container-swapper-option\" *ngIf=\"SalesRedemptionDetail==false&&!errorMessage\">\r\n            <span style=\"margin-right: 10px;\">View</span> \r\n            <div class=\"button-container\">\r\n            <button class=\"btn toolbar {{swaper}}\" (click)=\"swapClick(true)\">\r\n            <span class=\"fa fa-table\"> All</span>\r\n            </button>\r\n            <button class=\"btn toolbar {{!swaper}}\" (click)=\"swapClick(false)\">\r\n            <span class=\"fa fa-table\"> With Receipt</span>\r\n            </button>\r\n            </div>\r\n      </div>\r\n    \r\n      <div *ngIf=\"SalesRedemption && SalesRedemptionDetail==false && !mci_project && programType!='custom_kontraktual'\">\r\n            <app-form-builder-table\r\n            [searchCallback]= \"[service, swaper == true ?'searchEwalletSalesReportint':'searchEwalletReceiptReportint',this]\"\r\n            [tableFormat]=\"tableFormat\"\r\n            [table_data]=\"SalesRedemption\"\r\n            [total_page]=\"totalPage\"\r\n            >\r\n  \r\n            </app-form-builder-table>\r\n      </div>\r\n      <div *ngIf=\"SalesRedemption && SalesRedemptionDetail==false && mci_project && programType!='custom_kontraktual'\">\r\n            <app-form-builder-table\r\n            [searchCallback]= \"[service, swaper == true ?'searchEwalletSalesReportint':'searchEwalletReceiptReportint',this]\"\r\n            [tableFormat]=\"tableFormat2\"\r\n            [table_data]=\"SalesRedemption\"\r\n            [total_page]=\"totalPage\"\r\n            >\r\n\r\n            </app-form-builder-table>\r\n      </div>\r\n\r\n      <div *ngIf=\"SalesRedemption && SalesRedemptionDetail==false && !mci_project && programType=='custom_kontraktual'\">\r\n            <app-form-builder-table\r\n            [searchCallback]= \"[service, swaper == true ?'searchEwalletSalesReportint':'searchEwalletReceiptReportint',this]\"\r\n            [tableFormat]=\"tableFormat3\"\r\n            [table_data]=\"SalesRedemption\"\r\n            [total_page]=\"totalPage\"\r\n            >\r\n\r\n            </app-form-builder-table>\r\n      </div>\r\n        <!-- <div *ngIf=\"SalesRedemptionDetail\"> -->\r\n              <!-- <app-bast [back]=\"[this,backToHere]\" [detail]=\"SalesRedemptionDetail\"></app-bast> -->\r\n        <!-- </div> -->\r\n      <div *ngIf=\"SalesRedemptionDetail\">\r\n            <app-ewallet-sales-report-detail [back]=\"[this,backToHere]\" [detail]=\"SalesRedemptionDetail\"></app-ewallet-sales-report-detail>\r\n      </div>\r\n  \r\n    </div>\r\n  "

/***/ }),

/***/ "./src/app/layout/modules/ewalletsalesreport/detail/ewallet-sales-report.detail.component.scss":
/*!*****************************************************************************************************!*\
  !*** ./src/app/layout/modules/ewalletsalesreport/detail/ewallet-sales-report.detail.component.scss ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bg-delete {\n  background-color: #ffe3f3;\n  padding: 10px;\n}\n\n.card-detail > .card-header {\n  position: relative;\n}\n\n.card-detail > .card-header .back_button {\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n\n.card-detail > .card-header .edit_button {\n  position: absolute;\n  top: 7px;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 10px;\n}\n\n.card-detail > .card-header .edit_button i {\n  font-weight: bold;\n}\n\n.card-detail > .card-header .btn-right {\n  background-color: #555;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  margin-left: 10px;\n}\n\n.card-detail .card-content {\n  padding: 15px;\n}\n\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n\n.card-detail .member-detail form-select[name=status] .form-group {\n  margin-bottom: 0px;\n}\n\n.card-detail .member-detail label {\n  margin-top: 10px;\n}\n\n.card-detail .member-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n\n.card-detail .member-detail .card-header {\n  background-color: #555;\n}\n\n.card-detail .member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n\n.need-approval-text {\n  color: red;\n  font-size: 14px;\n  margin-bottom: 15px;\n}\n\n.btn-action-stock {\n  display: flex;\n}\n\n.btn-action-stock .btn-approve {\n  background-color: #28a745;\n  color: white;\n  margin-right: 10px;\n  font-size: 14px;\n}\n\n.btn-action-stock .btn-decline {\n  font-size: 14px;\n  margin-right: 10px;\n  background-color: red;\n  color: white;\n}\n\n.btn-action-stock .btn-cancel {\n  font-size: 14px;\n}\n\n#cover-spin {\n  position: fixed;\n  width: 100%;\n  left: 0;\n  right: 0;\n  top: 0;\n  bottom: 0;\n  background-color: rgba(255, 255, 255, 0.7);\n  z-index: 9999;\n}\n\n#cover-spin .spin-content {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  flex-direction: column;\n  height: 100%;\n}\n\n#cover-spin .spin-content .spin-text {\n  margin-bottom: 12px;\n  font-weight: bold;\n  color: #095eed;\n  font-size: 17px;\n}\n\n#cover-spin .spin-content .progress {\n  width: 300px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZXdhbGxldHNhbGVzcmVwb3J0L2RldGFpbC9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1vZHVsZXNcXGV3YWxsZXRzYWxlc3JlcG9ydFxcZGV0YWlsXFxld2FsbGV0LXNhbGVzLXJlcG9ydC5kZXRhaWwuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2V3YWxsZXRzYWxlc3JlcG9ydC9kZXRhaWwvZXdhbGxldC1zYWxlcy1yZXBvcnQuZGV0YWlsLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kseUJBQUE7RUFDQSxhQUFBO0FDQ0o7O0FER0k7RUFDSSxrQkFBQTtBQ0FSOztBRENRO0VBRUksZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLDZCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFFQSx5QkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7QUNEWjs7QURFWTtFQUNJLGlCQUFBO0FDQWhCOztBREdRO0VBQ0ksa0JBQUE7RUFDQSxRQUFBO0VBRUEsV0FBQTtFQUNBLFlBQUE7RUFDQSwwQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBSUEsV0FBQTtBQ0xaOztBREVZO0VBQ0ksaUJBQUE7QUNBaEI7O0FESVE7RUFDSSxzQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsMEJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0FDRlo7O0FES0k7RUFDSSxhQUFBO0FDSFI7O0FESVE7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0FDRlo7O0FEaUJJO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUNmUjs7QURrQlE7RUFDSSxrQkFBQTtBQ2hCWjs7QURrQlE7RUFDSSxnQkFBQTtBQ2hCWjs7QURrQlE7RUFDSSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FDaEJaOztBRG1CUTtFQUNJLHNCQUFBO0FDakJaOztBRGtCWTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLDBCQUFBO0FDaEJoQjs7QUR1QkE7RUFDSSxVQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0FDcEJKOztBRHVCQTtFQUNJLGFBQUE7QUNwQko7O0FEcUJJO0VBQ0kseUJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FDbkJSOztBRHFCSTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0EsWUFBQTtBQ25CUjs7QURxQkk7RUFDSSxlQUFBO0FDbkJSOztBRHVCQTtFQUNJLGVBQUE7RUFDQSxXQUFBO0VBQ0EsT0FBQTtFQUFPLFFBQUE7RUFBUSxNQUFBO0VBQU0sU0FBQTtFQUNyQiwwQ0FBQTtFQUNBLGFBQUE7QUNqQko7O0FEbUJJO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxzQkFBQTtFQUNBLFlBQUE7QUNqQlI7O0FEbUJRO0VBQ0ksbUJBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0FDakJaOztBRG9CUTtFQUNJLFlBQUE7QUNsQloiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9ld2FsbGV0c2FsZXNyZXBvcnQvZGV0YWlsL2V3YWxsZXQtc2FsZXMtcmVwb3J0LmRldGFpbC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5iZy1kZWxldGV7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlM2YzO1xyXG4gICAgcGFkZGluZzogMTBweDtcclxufVxyXG5cclxuLmNhcmQtZGV0YWlse1xyXG4gICAgPi5jYXJkLWhlYWRlcntcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgLmJhY2tfYnV0dG9ue1xyXG4gICAgICAgICAgICAvLyBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IC00cHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAtMTBweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5lZGl0X2J1dHRvbntcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICB0b3A6IDdweDtcclxuICAgICAgICAgICAgLy8gYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICBpe1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmlnaHQ6IDEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5idG4tcmlnaHQge1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC5jYXJkLWNvbnRlbnR7XHJcbiAgICAgICAgcGFkZGluZzogMTVweDtcclxuICAgICAgICA+LmNvbC1tZC0xMntcclxuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDBweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAvLyAuaW1hZ2V7XHJcbiAgICAvLyAgICAgaDN7XHJcbiAgICAvLyAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIC8vICAgICB9XHJcbiAgICAvLyAgICAgLmNhcmQtY29udGVudHtcclxuICAgIC8vICAgICAgICAgaW1ne1xyXG4gICAgLy8gICAgICAgICAgICAgbWF4LXdpZHRoOiAxMDAlO1xyXG4gICAgLy8gICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcblxyXG4gICAgLy8gICAgICAgICB9XHJcbiAgICAvLyAgICAgfVxyXG4gICAgLy8gfVxyXG4gICAgaDF7XHJcbiAgICAgICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICB9XHJcbiAgICAubWVtYmVyLWRldGFpbHtcclxuICAgICAgICBmb3JtLXNlbGVjdFtuYW1lPXN0YXR1c10gLmZvcm0tZ3JvdXAge1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxhYmVse1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBsYWJlbCtkaXZ7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICNmMGYwZjA7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0Om5vcm1hO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG4gICAgICAgICAgICBtaW4taGVpZ2h0OiAyN3B4O1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNjY2O1xyXG4gICAgICAgIH1cclxuICAgICAgICBcclxuICAgICAgICAuY2FyZC1oZWFkZXJ7IFxyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBoMntcclxuICAgICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgICAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiB1bnNldDtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogdW5zZXQ7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW46IDBweDtcclxuICAgICAgICAgICAgICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gICAgICAgIFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG4ubmVlZC1hcHByb3ZhbC10ZXh0IHtcclxuICAgIGNvbG9yOiByZWQ7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG59XHJcblxyXG4uYnRuLWFjdGlvbi1zdG9jayB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgLmJ0bi1hcHByb3ZlIHtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjhhNzQ1O1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgfVxyXG4gICAgLmJ0bi1kZWNsaW5lIHtcclxuICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IHJlZDtcclxuICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICB9XHJcbiAgICAuYnRuLWNhbmNlbCB7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgfVxyXG59XHJcblxyXG4jY292ZXItc3BpbiB7XHJcbiAgICBwb3NpdGlvbjpmaXhlZDtcclxuICAgIHdpZHRoOjEwMCU7XHJcbiAgICBsZWZ0OjA7cmlnaHQ6MDt0b3A6MDtib3R0b206MDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMjU1LDI1NSwyNTUsMC43KTtcclxuICAgIHotaW5kZXg6OTk5OTtcclxuXHJcbiAgICAuc3Bpbi1jb250ZW50IHtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcblxyXG4gICAgICAgIC5zcGluLXRleHQge1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxMnB4O1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgY29sb3I6ICMwOTVlZWQ7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTdweDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5wcm9ncmVzcyB7XHJcbiAgICAgICAgICAgIHdpZHRoOiAzMDBweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0iLCIuYmctZGVsZXRlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZTNmMztcbiAgcGFkZGluZzogMTBweDtcbn1cblxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5iYWNrX2J1dHRvbiB7XG4gIG1hcmdpbi10b3A6IC00cHg7XG4gIG1hcmdpbi1sZWZ0OiAtMTBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIGNvbG9yOiAjNTU1O1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5iYWNrX2J1dHRvbiBpIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmVkaXRfYnV0dG9uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDdweDtcbiAgY29sb3I6ICNmZmY7XG4gIGhlaWdodDogMzBweDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICByaWdodDogMTBweDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuZWRpdF9idXR0b24gaSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5idG4tcmlnaHQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xuICBjb2xvcjogI2ZmZjtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQge1xuICBwYWRkaW5nOiAxNXB4O1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQgPiAuY29sLW1kLTEyIHtcbiAgcGFkZGluZy1sZWZ0OiAwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDBweDtcbn1cbi5jYXJkLWRldGFpbCBoMSB7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjNTU1O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCBmb3JtLXNlbGVjdFtuYW1lPXN0YXR1c10gLmZvcm0tZ3JvdXAge1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgbGFiZWwge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIGxhYmVsICsgZGl2IHtcbiAgYmFja2dyb3VuZDogI2YwZjBmMDtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hO1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xuICBtaW4taGVpZ2h0OiAyN3B4O1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiAjNjY2O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5jYXJkLWhlYWRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmNhcmQtaGVhZGVyIGgyIHtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE1cHg7XG4gIGxpbmUtaGVpZ2h0OiB1bnNldDtcbiAgaGVpZ2h0OiB1bnNldDtcbiAgbWFyZ2luOiAwcHg7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xufVxuXG4ubmVlZC1hcHByb3ZhbC10ZXh0IHtcbiAgY29sb3I6IHJlZDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xufVxuXG4uYnRuLWFjdGlvbi1zdG9jayB7XG4gIGRpc3BsYXk6IGZsZXg7XG59XG4uYnRuLWFjdGlvbi1zdG9jayAuYnRuLWFwcHJvdmUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjhhNzQ1O1xuICBjb2xvcjogd2hpdGU7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuLmJ0bi1hY3Rpb24tc3RvY2sgLmJ0bi1kZWNsaW5lIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHJlZDtcbiAgY29sb3I6IHdoaXRlO1xufVxuLmJ0bi1hY3Rpb24tc3RvY2sgLmJ0bi1jYW5jZWwge1xuICBmb250LXNpemU6IDE0cHg7XG59XG5cbiNjb3Zlci1zcGluIHtcbiAgcG9zaXRpb246IGZpeGVkO1xuICB3aWR0aDogMTAwJTtcbiAgbGVmdDogMDtcbiAgcmlnaHQ6IDA7XG4gIHRvcDogMDtcbiAgYm90dG9tOiAwO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNyk7XG4gIHotaW5kZXg6IDk5OTk7XG59XG4jY292ZXItc3BpbiAuc3Bpbi1jb250ZW50IHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGhlaWdodDogMTAwJTtcbn1cbiNjb3Zlci1zcGluIC5zcGluLWNvbnRlbnQgLnNwaW4tdGV4dCB7XG4gIG1hcmdpbi1ib3R0b206IDEycHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogIzA5NWVlZDtcbiAgZm9udC1zaXplOiAxN3B4O1xufVxuI2NvdmVyLXNwaW4gLnNwaW4tY29udGVudCAucHJvZ3Jlc3Mge1xuICB3aWR0aDogMzAwcHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/modules/ewalletsalesreport/detail/ewallet-sales-report.detail.component.ts":
/*!***************************************************************************************************!*\
  !*** ./src/app/layout/modules/ewalletsalesreport/detail/ewallet-sales-report.detail.component.ts ***!
  \***************************************************************************************************/
/*! exports provided: EwalletSalesReportDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EwalletSalesReportDetailComponent", function() { return EwalletSalesReportDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var EwalletSalesReportDetailComponent = /** @class */ (function () {
    function EwalletSalesReportDetailComponent(route, router, OrderhistoryService) {
        this.route = route;
        this.router = router;
        this.OrderhistoryService = OrderhistoryService;
        this.processNow = false;
        this.cancelNow = false;
        this.declineNow = false;
        this.declineCancel = false;
        this.cancelRemarks = "";
        this.edit = false;
        this.errorLabel = false;
        this.salesRedemption = [];
        this.row_id = "order_id";
        this.errorMessage = false;
        this.showLoadingSpinner = false;
        this.processCount = 0;
        this.processTotal = 0;
        this.redemptionDetail = false;
        this.tableFormat = {
            title: 'All Redemptions in the Order',
            label_headers: [
                // {label: 'Tanggal Order', visible: true, type: 'date', data_row_name: 'request_date'},
                // {label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id'},
                // {label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'id_toko'},
                // {label: 'Nama Program', visible: true, type: 'form_cluster', data_row_name: 'member_detail' },
                // {label: 'Nama PIC', visible: true, type: 'form_nama_pemilik', data_row_name: 'member_detail'},
                { label: 'Nama Penerima', visible: true, type: 'form_nama_penerima', data_row_name: 'member_detail' },
                // {label: 'Nama Entitas', visible: true, type: 'form_nama_toko', data_row_name: 'member_detail'},
                // {label: 'Alamat Kirim', visible: true, type: 'form_alamat_penerima', data_row_name: 'member_detail'},
                // {label: 'No WA Penerima', visible: true, type: 'form_wa_penerima', data_row_name: 'member_detail'},
                { label: 'Hadiah Dikuasakan', visible: true, type: 'form_hadiah_dikuasakan', data_row_name: 'member_detail' },
                { label: 'Quantity', visible: true, type: 'product_quantity', data_row_name: 'products' },
                { label: 'Hadiah Redeem', visible: true, type: 'product_redeem', data_row_name: 'products' },
                { label: 'Product Code', visible: true, type: 'product_code_redeem', data_row_name: 'products' },
                { label: 'Points Redeem', visible: true, type: 'string', data_row_name: 'sum_total' },
                { label: 'Status Order', visible: true, type: 'string', data_row_name: 'status' },
                { label: 'Status Pengiriman', visible: true, type: 'last_shipping_info', data_row_name: 'delivery_detail' },
                { label: 'Tanggal Kirim', visible: true, type: 'delivery_date', data_row_name: 'delivery_detail' },
                { label: 'Tanggal Terima', visible: true, type: 'delivered_date', data_row_name: 'delivery_detail' },
                { label: 'Nama Penerima Langsung', visible: true, type: 'receiver_name_sap', data_row_name: 'delivery_detail' },
                { label: 'Status Penerima', visible: true, type: 'relation_name', data_row_name: 'delivery_detail' },
                { label: 'Tanggal Cancel', visible: true, type: 'date', data_row_name: 'cancel_date' },
                { label: 'Cancel Remarks', visible: true, type: 'string', data_row_name: 'cancel_remarks' },
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: this.row_id,
                this: this,
                result_var_name: 'salesRedemption',
                detail_function: [this, 'callDetail'],
                cancelProduct: true
            },
            show_checkbox_options: true
        };
    }
    EwalletSalesReportDetailComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    EwalletSalesReportDetailComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, i, cancel_detail, j, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.salesRedemptionDetail = this.detail;
                        console.warn("detail", this.salesRedemptionDetail);
                        this.orderID = this.detail.order_id;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        this.service = this.OrderhistoryService;
                        return [4 /*yield*/, this.OrderhistoryService.searchAllRedemptionPerOrder(this.orderID)];
                    case 2:
                        result = _a.sent();
                        this.totalPage = result.total_page;
                        this.salesRedemption = result.values;
                        // console.warn("result before??", this.salesRedemption);
                        for (i = 0; i < this.salesRedemption.length; i++) {
                            cancel_detail = [];
                            if (this.salesRedemption[i].products.cancel_detail) {
                                cancel_detail.push(this.salesRedemption[i].products.cancel_detail);
                                for (j = 0; j < cancel_detail.length; j++) {
                                    this.salesRedemption[i].cancel_remarks = cancel_detail[j].remarks;
                                    this.salesRedemption[i].cancel_date = cancel_detail[j].cancel_date;
                                    // console.warn("result after??", this.salesRedemption);
                                }
                            }
                            // console.warn("cancel detail nih", cancel_detail);
                            // let cancel_detail = this.salesRedemption[i].products.cancel_detail;
                            // this.salesRedemption[i].cancel_remarks = cancel_detail.remarks;
                            // this.salesRedemption[i].cancel_date = cancel_detail.cancel_date;
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        console.warn("ERROR");
                        this.errorLabel = (e_1.message); //conversion to Error type
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
                        }
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    EwalletSalesReportDetailComponent.prototype.backToHere = function () {
        // this.evoucherStockDetail = false;
        this.back[1](this.back[0]);
        // this.firstLoad();
    };
    EwalletSalesReportDetailComponent.prototype.isProcessNow = function () {
        this.processNow = !this.processNow;
    };
    EwalletSalesReportDetailComponent.prototype.isCancelNow = function () {
        this.cancelNow = !this.cancelNow;
    };
    EwalletSalesReportDetailComponent.prototype.isDeclineCancel = function () {
        this.declineCancel = !this.declineCancel;
    };
    EwalletSalesReportDetailComponent.prototype.isDeclineNow = function () {
        this.declineNow = !this.declineNow;
    };
    EwalletSalesReportDetailComponent.prototype.completeOrder = function (orderID) {
        return __awaiter(this, void 0, void 0, function () {
            var payloadData, result, e_2, message;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        payloadData = {
                            "order_id": [
                                orderID
                            ]
                        };
                        return [4 /*yield*/, this.OrderhistoryService.updateStatusOrderCompleted(payloadData, this)];
                    case 1:
                        result = _a.sent();
                        console.warn("result process", result);
                        if (result) {
                            sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                                title: 'Success',
                                text: 'Order is Completed',
                                icon: 'success',
                                confirmButtonText: 'Ok',
                            }).then(function (result) {
                                if (result.isConfirmed) {
                                    _this.backToHere();
                                }
                            });
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        e_2 = _a.sent();
                        console.warn("error appove", e_2);
                        this.errorLabel = (e_2.message);
                        message = this.errorLabel;
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
                        }
                        sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                            icon: 'error',
                            title: message,
                        });
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    EwalletSalesReportDetailComponent.prototype.processSelectedProducts = function (obj, products) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                if (products.length > 0) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                        title: 'Konfirmasi',
                        text: 'Alasan Pembatalan Produk : ',
                        // icon: 'success',
                        confirmButtonText: 'Ok',
                        cancelButtonText: "Kembali",
                        showCancelButton: true,
                        input: 'text',
                    }).then(function (result) { return __awaiter(_this, void 0, void 0, function () {
                        var payload, e_3, message;
                        var _this = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    if (!result.isConfirmed) return [3 /*break*/, 5];
                                    this.showLoadingSpinner = true;
                                    this.processTotal = products.length;
                                    this.processCount = 0;
                                    payload = {
                                        "order_id": this.orderID,
                                        "product_sku": products,
                                        "remarks": result.value
                                    };
                                    _a.label = 1;
                                case 1:
                                    _a.trys.push([1, 3, , 4]);
                                    return [4 /*yield*/, obj.OrderhistoryService.cancelDigitalProductOrder(payload).then(function () {
                                            _this.processCount++;
                                            if (_this.processCount >= products.length) {
                                                _this.showLoadingSpinner = false;
                                                sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                                                    title: 'Success',
                                                    text: 'Produk terpilih telah dibatalkan',
                                                    icon: 'success',
                                                    confirmButtonText: 'Ok',
                                                    showCancelButton: false
                                                }).then(function (result) { return __awaiter(_this, void 0, void 0, function () {
                                                    return __generator(this, function (_a) {
                                                        if (result.isConfirmed) {
                                                            console.log("confirmed");
                                                            obj.firstLoad();
                                                        }
                                                        return [2 /*return*/];
                                                    });
                                                }); });
                                            }
                                        })];
                                case 2:
                                    _a.sent();
                                    return [3 /*break*/, 4];
                                case 3:
                                    e_3 = _a.sent();
                                    this.errorLabel = (e_3.message); //conversion to Error type
                                    this.showLoadingSpinner = false;
                                    message = this.errorLabel;
                                    if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                                        message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
                                    }
                                    sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                                        icon: 'error',
                                        title: message,
                                    });
                                    return [3 /*break*/, 4];
                                case 4:
                                    ;
                                    _a.label = 5;
                                case 5: return [2 /*return*/];
                            }
                        });
                    }); });
                }
                return [2 /*return*/];
            });
        });
    };
    EwalletSalesReportDetailComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] },
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_4__["OrderhistoryService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], EwalletSalesReportDetailComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], EwalletSalesReportDetailComponent.prototype, "back", void 0);
    EwalletSalesReportDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-ewallet-sales-report-detail',
            template: __webpack_require__(/*! raw-loader!./ewallet-sales-report.detail.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/ewalletsalesreport/detail/ewallet-sales-report.detail.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_3__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./ewallet-sales-report.detail.component.scss */ "./src/app/layout/modules/ewalletsalesreport/detail/ewallet-sales-report.detail.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_4__["OrderhistoryService"]])
    ], EwalletSalesReportDetailComponent);
    return EwalletSalesReportDetailComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/ewalletsalesreport/ewallet-sales-report-routing.module.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/layout/modules/ewalletsalesreport/ewallet-sales-report-routing.module.ts ***!
  \******************************************************************************************/
/*! exports provided: EwalletSalesReportRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EwalletSalesReportRoutingModule", function() { return EwalletSalesReportRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ewallet_sales_report_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ewallet-sales-report.component */ "./src/app/layout/modules/ewalletsalesreport/ewallet-sales-report.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



// import { BastComponent} from './bast/bast.component'
var routes = [
    {
        path: '', component: _ewallet_sales_report_component__WEBPACK_IMPORTED_MODULE_2__["EwalletSalesReportComponent"]
    },
];
var EwalletSalesReportRoutingModule = /** @class */ (function () {
    function EwalletSalesReportRoutingModule() {
    }
    EwalletSalesReportRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], EwalletSalesReportRoutingModule);
    return EwalletSalesReportRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/ewalletsalesreport/ewallet-sales-report.component.scss":
/*!***************************************************************************************!*\
  !*** ./src/app/layout/modules/ewalletsalesreport/ewallet-sales-report.component.scss ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container-swapper-option {\n  display: flex;\n  justify-content: center;\n  flex-direction: row;\n  align-items: center;\n}\n.container-swapper-option .button-container {\n  border: 2px solid #ddd;\n  border-radius: 8px;\n}\n.container-swapper-option .button-container button {\n  color: #ddd;\n  background: white;\n  font-size: 14px;\n  border-radius: 8px;\n}\n.container-swapper-option .button-container button.true {\n  background: #2480fb;\n  border-radius: 8px;\n  color: #ddd;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZXdhbGxldHNhbGVzcmVwb3J0L0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbW9kdWxlc1xcZXdhbGxldHNhbGVzcmVwb3J0XFxld2FsbGV0LXNhbGVzLXJlcG9ydC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZXdhbGxldHNhbGVzcmVwb3J0L2V3YWxsZXQtc2FsZXMtcmVwb3J0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQ0NGO0FEQUU7RUFFRSxzQkFBQTtFQUNBLGtCQUFBO0FDQ0o7QURBSTtFQUNFLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtBQ0VOO0FEQUk7RUFFRSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtBQ0NOIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZXdhbGxldHNhbGVzcmVwb3J0L2V3YWxsZXQtc2FsZXMtcmVwb3J0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRhaW5lci1zd2FwcGVyLW9wdGlvbiB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgLmJ1dHRvbi1jb250YWluZXIge1xyXG5cclxuICAgIGJvcmRlcjogMnB4IHNvbGlkICNkZGQ7XHJcbiAgICBib3JkZXItcmFkaXVzOjhweDtcclxuICAgIGJ1dHRvbiB7XHJcbiAgICAgIGNvbG9yOiAjZGRkO1xyXG4gICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICBib3JkZXItcmFkaXVzOiA4cHg7XHJcbiAgICB9XHJcbiAgICBidXR0b24udHJ1ZSB7XHJcbiAgICAgXHJcbiAgICAgIGJhY2tncm91bmQ6ICMyNDgwZmI7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICAgICAgY29sb3I6ICNkZGQ7XHJcbiAgICB9XHJcbiAgfVxyXG59IiwiLmNvbnRhaW5lci1zd2FwcGVyLW9wdGlvbiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLmNvbnRhaW5lci1zd2FwcGVyLW9wdGlvbiAuYnV0dG9uLWNvbnRhaW5lciB7XG4gIGJvcmRlcjogMnB4IHNvbGlkICNkZGQ7XG4gIGJvcmRlci1yYWRpdXM6IDhweDtcbn1cbi5jb250YWluZXItc3dhcHBlci1vcHRpb24gLmJ1dHRvbi1jb250YWluZXIgYnV0dG9uIHtcbiAgY29sb3I6ICNkZGQ7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBmb250LXNpemU6IDE0cHg7XG4gIGJvcmRlci1yYWRpdXM6IDhweDtcbn1cbi5jb250YWluZXItc3dhcHBlci1vcHRpb24gLmJ1dHRvbi1jb250YWluZXIgYnV0dG9uLnRydWUge1xuICBiYWNrZ3JvdW5kOiAjMjQ4MGZiO1xuICBib3JkZXItcmFkaXVzOiA4cHg7XG4gIGNvbG9yOiAjZGRkO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/modules/ewalletsalesreport/ewallet-sales-report.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/layout/modules/ewalletsalesreport/ewallet-sales-report.component.ts ***!
  \*************************************************************************************/
/*! exports provided: EwalletSalesReportComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EwalletSalesReportComponent", function() { return EwalletSalesReportComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
/* harmony import */ var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../assets/json/content.json */ "./src/assets/json/content.json");
var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_4___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../../../assets/json/content.json */ "./src/assets/json/content.json", 1);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var EwalletSalesReportComponent = /** @class */ (function () {
    function EwalletSalesReportComponent(OrderhistoryService, router, activatedRoute) {
        this.OrderhistoryService = OrderhistoryService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.SalesRedemption = [];
        this.swaper = true;
        this.tableFormat = {
            title: 'E-wallet Sales report',
            label_headers: [
                { label: 'Tanggal Order', visible: true, type: 'date', data_row_name: 'request_date' },
                { label: 'Status Order', visible: true, type: 'string', data_row_name: 'status' },
                { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
                { label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'id_toko' },
                // {label: 'Member', visible: true, type: 'string', data_row_name: 'member_name'},
                { label: 'Nama Pemilik', visible: true, type: 'form_nama_pemilik', data_row_name: 'member_detail' },
                { label: 'Nama Penerima', visible: true, type: 'form_nama_penerima', data_row_name: 'member_detail' },
                { label: 'Nama Toko', visible: true, type: 'form_nama_toko', data_row_name: 'member_detail' },
                { label: 'Group', visible: true, type: 'form_group', data_row_name: 'member_detail' },
                { label: 'Alamat Kirim', visible: true, type: 'form_alamat_penerima', data_row_name: 'member_detail' },
                { label: 'No WA Penerima', visible: true, type: 'form_wa_penerima', data_row_name: 'member_detail' },
                { label: 'Hadiah Dikuasakan', visible: true, type: 'form_hadiah_dikuasakan', data_row_name: 'member_detail' },
                { label: 'Quantity', visible: true, type: 'product_quantity', data_row_name: 'products' },
                // {label: 'Points', visible: true, type: 'string', data_row_name: 'points'},
                { label: 'Hadiah Redeem', visible: true, type: 'product_redeem', data_row_name: 'products' },
                { label: 'Product Code', visible: true, type: 'product_code_redeem', data_row_name: 'products' },
                { label: 'Points Redeem', visible: true, type: 'string', data_row_name: 'sum_total' },
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: '_id',
                this: this,
                result_var_name: 'SalesRedemption',
                detail_function: [this, 'callDetail'],
                salesRedemption: true,
            },
            show_checkbox_options: true
        };
        this.tableFormat2 = {
            title: 'E-wallet Sales report',
            label_headers: [
                { label: 'Tanggal Order', visible: true, type: 'date', data_row_name: 'request_date' },
                { label: 'Status Order', visible: true, type: 'string', data_row_name: 'status' },
                { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
                { label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'id_toko' },
                { label: 'Nama Penerima', visible: true, type: 'form_nama_penerima', data_row_name: 'member_detail' },
                { label: 'No WA Penerima', visible: true, type: 'form_wa_penerima', data_row_name: 'member_detail' },
                { label: 'Alamat Kirim', visible: true, type: 'form_alamat_penerima', data_row_name: 'member_detail' },
                { label: 'Quantity', visible: true, type: 'product_quantity', data_row_name: 'products' },
                { label: 'Hadiah Redeem', visible: true, type: 'product_redeem', data_row_name: 'products' },
                { label: 'Product Code', visible: true, type: 'product_code_redeem', data_row_name: 'products' },
                { label: 'Points Redeem', visible: true, type: 'string', data_row_name: 'sum_total' },
                { label: 'Program', visible: true, type: 'form_group', data_row_name: 'member_detail' },
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: '_id',
                this: this,
                result_var_name: 'SalesRedemption',
                detail_function: [this, 'callDetail'],
                salesRedemption: true,
            },
            show_checkbox_options: true
        };
        this.tableFormat3 = {
            title: 'E-wallet Sales report',
            label_headers: [
                { label: 'Tanggal Order', visible: true, type: 'date', data_row_name: 'request_date' },
                { label: 'Status Order', visible: true, type: 'string', data_row_name: 'status' },
                { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
                { label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'id_toko' },
                { label: 'Nama Program', visible: true, type: 'form_cluster', data_row_name: 'member_detail' },
                // {label: 'Member', visible: true, type: 'string', data_row_name: 'member_name'},
                { label: 'Nama PIC', visible: true, type: 'form_nama_pemilik', data_row_name: 'member_detail' },
                { label: 'Nama Penerima', visible: true, type: 'form_nama_penerima', data_row_name: 'member_detail' },
                { label: 'Nama Entitas', visible: true, type: 'form_nama_toko', data_row_name: 'member_detail' },
                // {label: 'Group', visible: true, type: 'form_group', data_row_name: 'member_detail'},
                { label: 'Alamat Kirim', visible: true, type: 'form_alamat_penerima', data_row_name: 'member_detail' },
                { label: 'No WA Penerima', visible: true, type: 'form_wa_penerima', data_row_name: 'member_detail' },
                { label: 'Hadiah Dikuasakan', visible: true, type: 'form_hadiah_dikuasakan', data_row_name: 'member_detail' },
                { label: 'Quantity', visible: true, type: 'product_quantity', data_row_name: 'products' },
                // {label: 'Points', visible: true, type: 'string', data_row_name: 'points'},
                { label: 'Hadiah Redeem', visible: true, type: 'product_redeem', data_row_name: 'products' },
                { label: 'Product Code', visible: true, type: 'product_code_redeem', data_row_name: 'products' },
                { label: 'Points Redeem', visible: true, type: 'string', data_row_name: 'sum_total' },
            ],
            row_primary_key: '_id',
            formOptions: {
                row_id: '_id',
                this: this,
                result_var_name: 'SalesRedemption',
                detail_function: [this, 'callDetail'],
                salesRedemption: true,
            },
            show_checkbox_options: true
        };
        this.contentList = _assets_json_content_json__WEBPACK_IMPORTED_MODULE_4__;
        this.form_input = {};
        this.errorLabel = false;
        this.totalPage = 0;
        this.SalesRedemptionDetail = false;
        this.mci_project = false;
        this.programType = "";
    }
    EwalletSalesReportComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    EwalletSalesReportComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var params, program_1, _this_1, result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 5, , 6]);
                        params = {
                            search: {
                                type: {
                                    in: ["product", "voucher"]
                                }
                                // active : '1'
                            },
                            limit_per_page: 50,
                            current_page: 1,
                            order_by: { request_date: -1 }
                        };
                        program_1 = localStorage.getItem('programName');
                        _this_1 = this;
                        this.contentList.filter(function (element) {
                            if (element.appLabel == program_1) {
                                if (element.type == "reguler") {
                                    _this_1.mci_project = true;
                                    _this_1.programType = "reguler";
                                }
                                else if (element.type == "custom_kontraktual") {
                                    _this_1.mci_project = false;
                                    _this_1.programType = "custom_kontraktual";
                                }
                                else {
                                    _this_1.mci_project = false;
                                    _this_1.programType = "custom";
                                }
                            }
                        });
                        this.service = this.OrderhistoryService;
                        result = void 0;
                        if (!(this.swaper == true)) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.OrderhistoryService.getEwalletSalesReportint()];
                    case 1:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 2: return [4 /*yield*/, this.OrderhistoryService.getEwalletReceiptReportint()];
                    case 3:
                        result = _a.sent();
                        _a.label = 4;
                    case 4:
                        this.totalPage = result.total_page;
                        this.SalesRedemption = result.values;
                        return [3 /*break*/, 6];
                    case 5:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    EwalletSalesReportComponent.prototype.callDetail = function (data, rowData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                try {
                    this.SalesRedemptionDetail = this.SalesRedemption.find(function (redemption) { return redemption.order_id == rowData.order_id; });
                    // let result: any;
                    // this.service    = this.OrderhistoryService;
                    // result          = await this.OrderhistoryService.detailPointstransaction(SalesRedemption_id);
                    // console.log(result);
                }
                catch (e) {
                    this.errorLabel = (e.message); //conversion to Error type
                }
                return [2 /*return*/];
            });
        });
    };
    EwalletSalesReportComponent.prototype.backToHere = function (obj) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                console.warn("masuk back awal");
                console.warn("obj", obj);
                obj.SalesRedemptionDetail = false;
                obj.router.navigate([]).then(function () {
                    obj.firstLoad();
                });
                return [2 /*return*/];
            });
        });
    };
    EwalletSalesReportComponent.prototype.swapClick = function (bool) {
        this.swaper = bool;
        this.firstLoad();
    };
    EwalletSalesReportComponent.prototype.addMonths = function (date, months) {
        date.setMonth(date.getMonth() + months);
        return date;
    };
    EwalletSalesReportComponent.ctorParameters = function () { return [
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_3__["OrderhistoryService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }
    ]; };
    EwalletSalesReportComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-ewallet-sales-report',
            template: __webpack_require__(/*! raw-loader!./ewallet-sales-report.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/ewalletsalesreport/ewallet-sales-report.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./ewallet-sales-report.component.scss */ "./src/app/layout/modules/ewalletsalesreport/ewallet-sales-report.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_3__["OrderhistoryService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], EwalletSalesReportComponent);
    return EwalletSalesReportComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/ewalletsalesreport/ewallet-sales-report.module.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/layout/modules/ewalletsalesreport/ewallet-sales-report.module.ts ***!
  \**********************************************************************************/
/*! exports provided: EwalletSalesReportModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EwalletSalesReportModule", function() { return EwalletSalesReportModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ewallet_sales_report_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ewallet-sales-report-routing.module */ "./src/app/layout/modules/ewalletsalesreport/ewallet-sales-report-routing.module.ts");
/* harmony import */ var _ewallet_sales_report_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ewallet-sales-report.component */ "./src/app/layout/modules/ewalletsalesreport/ewallet-sales-report.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _detail_ewallet_sales_report_detail_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./detail/ewallet-sales-report.detail.component */ "./src/app/layout/modules/ewalletsalesreport/detail/ewallet-sales-report.detail.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var ngx_bootstrap_progressbar__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-bootstrap/progressbar */ "./node_modules/ngx-bootstrap/progressbar/fesm5/ngx-bootstrap-progressbar.js");
/* harmony import */ var _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










// import { BastComponent } from './bast/bast.component';

var EwalletSalesReportModule = /** @class */ (function () {
    function EwalletSalesReportModule() {
    }
    EwalletSalesReportModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _ewallet_sales_report_routing_module__WEBPACK_IMPORTED_MODULE_2__["EwalletSalesReportRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_4__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_8__["NgbModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_7__["FormBuilderTableModule"],
                _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_10__["BsComponentModule"],
                ngx_bootstrap_progressbar__WEBPACK_IMPORTED_MODULE_9__["ProgressbarModule"].forRoot(),
            ],
            declarations: [
                _ewallet_sales_report_component__WEBPACK_IMPORTED_MODULE_3__["EwalletSalesReportComponent"],
                _detail_ewallet_sales_report_detail_component__WEBPACK_IMPORTED_MODULE_5__["EwalletSalesReportDetailComponent"]
            ],
        })
    ], EwalletSalesReportModule);
    return EwalletSalesReportModule;
}());



/***/ })

}]);
//# sourceMappingURL=modules-ewalletsalesreport-ewallet-sales-report-module.js.map