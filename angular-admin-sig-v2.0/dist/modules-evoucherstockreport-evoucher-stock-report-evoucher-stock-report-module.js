(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-evoucherstockreport-evoucher-stock-report-evoucher-stock-report-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/evoucherstockreport/evoucher-stock-report/add/evoucher-stock-report.add.component.html":
/*!*************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/evoucherstockreport/evoucher-stock-report/add/evoucher-stock-report.add.component.html ***!
  \*************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n\r\n    <app-page-header [heading]=\"''\" [icon]=\"'fa-table'\"></app-page-header>\r\n\r\n    <div *ngIf=\"!showPasswordPage\">\r\n        <div class=\"card card-detail\">\r\n            <div class=\"card-header\">\r\n                <button class=\"btn back_button\" (click)=\"backTo()\"><i class=\"fa fa-fw fa-angle-left\"></i>Back</button>\r\n            </div>\r\n\r\n            <div class=\"card-content\">\r\n                <div class=\"member-detail\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-md-6\">\r\n                            <div class=\"card mb-3\">\r\n                                <input #addBulk type=\"file\" style=\"display:none\" (change)=\"onFileSelected($event)\"\r\n                                />\r\n\r\n                                <div class=\"card-header\">\r\n                                    <h2>Upload Voucher Code</h2>\r\n                                </div>\r\n                                <div class=\"card-content\">\r\n                                    <div class=\"col-md-12\">\r\n                                        <label>Upload Your File Here</label>\r\n                                        <div class=\"member-bulk-update-container\">\r\n                                            <span *ngIf=\"selectedFile && prodOnUpload == false\">{{selectedFile.name}}</span>\r\n                                        </div>\r\n\r\n                                        <div class=\"form-group uploaded\" *ngIf=\"prodOnUpload == false && startUploading == true && cancel == false\">\r\n                                            <div class=\"loading\">\r\n                                              <div class=\"sk-fading-circle\">\r\n                                                <div class=\"sk-circle1 sk-circle\"></div>\r\n                                                <div class=\"sk-circle2 sk-circle\"></div>\r\n                                                <div class=\"sk-circle3 sk-circle\"></div>\r\n                                                <div class=\"sk-circle4 sk-circle\"></div>\r\n                                                <div class=\"sk-circle5 sk-circle\"></div>\r\n                                                <div class=\"sk-circle6 sk-circle\"></div>\r\n                                                <div class=\"sk-circle7 sk-circle\"></div>\r\n                                                <div class=\"sk-circle8 sk-circle\"></div>\r\n                                                <div class=\"sk-circle9 sk-circle\"></div>\r\n                                                <div class=\"sk-circle10 sk-circle\"></div>\r\n                                                <div class=\"sk-circle11 sk-circle\"></div>\r\n                                                <div class=\"sk-circle12 sk-circle\"></div>\r\n                                              </div>\r\n                                            </div>\r\n                                        </div>\r\n\r\n                                        <div class=\"container-two-button\">\r\n                                            <div class=\"img-upload\" *ngIf=\"!selectedFile; else upload_image\">\r\n                                                <button class=\"btn btn-upload\" (click)=\"addBulk.click()\">\r\n                                                    <i class=\"fa fa-fw fa-upload\"></i>\r\n                                                    <span>upload</span>\r\n                                                </button>\r\n                                            </div>\r\n    \r\n                                            <div class=\"img-upload\" *ngIf=\"selectedFile\">\r\n                                                <button class=\"btn btn-submit\" (click)=\"updateDataBulk()\">\r\n                                                    <i class=\"fa fa-fw fa-save\"></i>\r\n                                                    <span>submit</span>\r\n                                                </button>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n\r\n                                    <ng-template #upload_image>\r\n                                        <button class=\"btn btn-upload\" (click)=\"addBulk.click()\">\r\n                                            <i class=\"fa fa-fw fa-upload\"></i>\r\n                                            <span>re-upload</span>\r\n                                        </button>\r\n                                    </ng-template>\r\n\r\n                                    <div class=\"col-md-12 col-sm-12 error-msg-upload\">\r\n                                        <div *ngIf=\"errorFile\"><mark>Error: {{errorFile}}</mark></div>                       \r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/evoucherstockreport/evoucher-stock-report/detail/evoucher-stock-report.detail.component.html":
/*!*******************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/evoucherstockreport/evoucher-stock-report/detail/evoucher-stock-report.detail.component.html ***!
  \*******************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"evoucherStockDetail\" class=\"card card-detail\">\r\n    <div class=\"card-header\">\r\n        <button class=\"btn back_button\" (click)=\"backToHere()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button>\r\n    </div>\r\n\r\n    <ng-template #empty_image><span>\r\n        empty\r\n    </span></ng-template>\r\n  \r\n    <div class=\"card-content\">\r\n        <div class=\"member-detail\">\r\n    \r\n            <div class=\"row\">\r\n                <div class=\"col-md-4\">\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Stock Detail</h2></div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">             \r\n                                <label>Process Number</label>\r\n                                <div name=\"member_id\">{{evoucherStockDetail.process_number}}</div>\r\n\r\n                                <label>Process Type</label>\r\n                                <div name=\"member_id\">{{evoucherStockDetail.process_type}}</div>\r\n\r\n                                <label>Total Quantity</label>\r\n                                <div name=\"member_id\">{{evoucherStockDetail.total_qty}}</div>\r\n\r\n                                <label>Total SKU</label>\r\n                                <div name=\"member_id\">{{evoucherStockDetail.total_sku}}</div>\r\n\r\n                                <label>SKU List</label>\r\n                                <div name=\"member_id\" *ngFor=\"let skuList of evoucherStockDetail.sku_list\">\r\n                                    <li>{{skuList.sku_code}}, qty: {{skuList.qty}}</li>\r\n                                </div>\r\n\r\n                                <label>Total Value</label>\r\n                                <div name=\"member_id\">{{evoucherStockDetail.total_value}}</div>\r\n\r\n                                <label>Total Price</label>\r\n                                <div name=\"member_id\">{{evoucherStockDetail.total_price}}</div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\"col-md-4\">\r\n                    <div class=\"card mb-3\">\r\n                        <div class=\"card-header\"><h2>Approval Status</h2></div>\r\n                        <div class=\"card-content\">\r\n                            <div class=\"col-md-12\">             \r\n                                <label>Status</label>\r\n                                <div name=\"member_id\">{{evoucherStockDetail.status}}</div>\r\n                                \r\n                                <div class=\"need-approval-text\" *ngIf=\"evoucherStockDetail.status == 'REQUESTED' && !processNow\">\r\n                                    <span>This request needs approval</span>\r\n                                </div>\r\n\r\n                                <div class=\"need-approval-text\" *ngIf=\"evoucherStockDetail.status == 'REQUESTED' && processNow\">\r\n                                    <span>Are you sure to process this request?</span>\r\n                                </div>\r\n\r\n                                <div class=\"btn-action-stock\" *ngIf=\"evoucherStockDetail.status == 'REQUESTED' && !processNow\">\r\n                                    <button class=\"btn btn-approve\" (click)=\"isProcessNow()\">Approve</button>\r\n                                    <!-- <button class=\"btn btn-decline\">Decline</button> -->\r\n                                </div>\r\n\r\n                                <div class=\"btn-action-stock\" *ngIf=\"evoucherStockDetail.status == 'REQUESTED' && processNow\">\r\n                                    <button class=\"btn btn-approve\" (click)=\"processEvoucher(evoucherStockDetail.process_number)\">Process Now</button>\r\n                                    <button class=\"btn btn-decline\" (click)=\"isProcessNow()\">Cancel</button>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n               \r\n            </div>\r\n         </div>\r\n    </div>\r\n    \r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/evoucherstockreport/evoucher-stock-report/evoucher-stock-report.component.html":
/*!*****************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/evoucherstockreport/evoucher-stock-report/evoucher-stock-report.component.html ***!
  \*****************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n       \r\n        <app-page-header [heading]=\"'&nbsp;'\" [icon]=\"'fa-table'\"></app-page-header>\r\n        <div class=\"container-swapper-option\" *ngIf=\"evoucherStockDetail==false&&!errorMessage\">\r\n            <span style=\"margin-right: 10px;\">View</span> \r\n            <div class=\"button-container\">\r\n            <button class=\"btn toolbar {{swaper}}\" (click)=\"swapClick(true)\">\r\n                <span class=\"fa fa-table\"> Requested</span>\r\n            </button>\r\n            <button class=\"btn toolbar {{!swaper}}\" (click)=\"swapClick(false)\">\r\n                <span class=\"fa fa-table\"> Approved</span>\r\n            </button>\r\n            </div>\r\n        </div>\r\n\r\n        <div *ngIf=\"Evouchers && evoucherStockDetail==false && swaper &&!errorMessage\">\r\n              <app-form-builder-table \r\n              [tableFormat] = \"tableFormat\" \r\n              [table_data]  = \"Evouchers\" \r\n              [searchCallback]= \"[service, 'searchEvoucherStockReport',this]\" \r\n              [total_page]=\"totalPage\"\r\n              >\r\n                  \r\n              </app-form-builder-table>\r\n        </div>\r\n\r\n        <div *ngIf=\"Evouchers && evoucherStockDetail==false && !swaper &&!errorMessage\">\r\n            <app-form-builder-table \r\n            [tableFormat] = \"tableFormat\" \r\n            [table_data]  = \"Evouchers\" \r\n            [searchCallback]= \"[service, 'searchEvoucherStockReport2',this]\" \r\n            [total_page]=\"totalPage\"\r\n            >\r\n                \r\n            </app-form-builder-table>\r\n        </div>\r\n\r\n        <div *ngIf=\"errorMessage\" class=\"error-message\">\r\n            <div class=\"text-message\">\r\n                <i class=\"fas fa-ban\"></i>\r\n                {{errorMessage}}\r\n            </div>\r\n        </div>\r\n      \r\n        <div *ngIf=\"evoucherStockDetail\">\r\n            <app-evoucher-stock-report-detail [back]=\"[this,backToHere]\" [detail]=\"evoucherStockDetail\"></app-evoucher-stock-report-detail>\r\n        </div>\r\n\r\n       \r\n      </div>\r\n      \r\n       "

/***/ }),

/***/ "./src/app/layout/modules/evoucherstockreport/evoucher-stock-report/add/evoucher-stock-report.add.component.scss":
/*!***********************************************************************************************************************!*\
  !*** ./src/app/layout/modules/evoucherstockreport/evoucher-stock-report/add/evoucher-stock-report.add.component.scss ***!
  \***********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bg-delete {\n  background-color: #ffe3f3;\n  padding: 10px;\n}\n\n.ng-pristine {\n  color: white !important;\n}\n\n.ng-pristine {\n  color: black !important;\n  width: 100%;\n  border-radius: 5px;\n  padding: 6px 0px;\n}\n\n.member-bulk-update-container {\n  padding: 10px !important;\n}\n\n.container-two-button {\n  display: flex;\n}\n\n.error-msg-upload {\n  margin-top: 10px;\n  padding-left: 0px !important;\n  padding-right: 0px !important;\n}\n\n.file-selected {\n  color: red;\n}\n\n.btn-add-bulk {\n  margin-bottom: 20px;\n}\n\n.btn-upload, .btn-submit {\n  margin-right: 10px;\n}\n\n.btn-upload {\n  border-color: black;\n}\n\n.btn-submit {\n  background-color: #3498db;\n  color: white;\n}\n\n.star-required {\n  color: red;\n}\n\n.text-required {\n  color: red;\n  font-size: 11px;\n  margin-top: -15px;\n}\n\nlabel {\n  color: black;\n}\n\n.card-detail > .card-header {\n  position: relative;\n  min-height: 48px;\n}\n\n.card-detail > .card-header .back_button {\n  position: absolute;\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n\n.card-detail > .card-header .save_button {\n  position: absolute;\n  top: 10px;\n  background-color: #3498db;\n  color: white;\n  height: 30px;\n  align-items: center;\n  display: flex;\n  right: 10px;\n  padding-top: 0px;\n  padding-right: 10px;\n  padding-bottom: 0;\n  padding-left: 10px;\n  font-weight: bold;\n}\n\n.card-detail .card-content {\n  padding: 15px;\n}\n\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n\n.card-detail .member-detail form-select[name=status] .form-group {\n  margin-bottom: 0px;\n}\n\n.card-detail .member-detail label {\n  margin-top: 10px;\n}\n\n.card-detail .member-detail label + div {\n  background: #f0f0f0;\n  font-weight: normal;\n  padding: 0px;\n  border: 1px solid #f8f9fa;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n\n.card-detail .member-detail .card-header {\n  background-color: #555;\n}\n\n.card-detail .member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n\n.rounded-btn {\n  margin-top: 20px;\n  border-radius: 70px;\n  color: white;\n  font-size: 18px;\n  line-height: 40px;\n  padding: 0 40px;\n  background: #56a4ff;\n}\n\n.rounded-btn-danger {\n  margin-top: 20px;\n  border-radius: 70px;\n  color: white;\n  font-size: 18px;\n  line-height: 40px;\n  padding: 0 40px;\n  color: #555;\n  margin-left: 10px;\n}\n\n.rounded-btn:hover {\n  background: #0a7bff;\n  outline: none;\n}\n\n.save-button {\n  background-color: #3498db;\n  color: white;\n  margin-top: 20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZXZvdWNoZXJzdG9ja3JlcG9ydC9ldm91Y2hlci1zdG9jay1yZXBvcnQvYWRkL0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbW9kdWxlc1xcZXZvdWNoZXJzdG9ja3JlcG9ydFxcZXZvdWNoZXItc3RvY2stcmVwb3J0XFxhZGRcXGV2b3VjaGVyLXN0b2NrLXJlcG9ydC5hZGQuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2V2b3VjaGVyc3RvY2tyZXBvcnQvZXZvdWNoZXItc3RvY2stcmVwb3J0L2FkZC9ldm91Y2hlci1zdG9jay1yZXBvcnQuYWRkLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kseUJBQUE7RUFDQSxhQUFBO0FDQ0o7O0FERUE7RUFDSSx1QkFBQTtBQ0NKOztBREVBO0VBQ0ksdUJBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQ0NKOztBREVBO0VBQ0ksd0JBQUE7QUNDSjs7QURFQTtFQUNJLGFBQUE7QUNDSjs7QURFQTtFQUNJLGdCQUFBO0VBQ0EsNEJBQUE7RUFDQSw2QkFBQTtBQ0NKOztBREVBO0VBQ0ksVUFBQTtBQ0NKOztBREVBO0VBQ0ksbUJBQUE7QUNDSjs7QURFQTtFQUNJLGtCQUFBO0FDQ0o7O0FERUE7RUFDSSxtQkFBQTtBQ0NKOztBREVBO0VBQ0kseUJBQUE7RUFDQSxZQUFBO0FDQ0o7O0FERUE7RUFDSSxVQUFBO0FDQ0o7O0FERUE7RUFDSSxVQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FDQ0o7O0FERUE7RUFDSSxZQUFBO0FDQ0o7O0FERUk7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0FDQ1I7O0FEQVE7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSw2QkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBRUEseUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0FDQ1o7O0FEQVk7RUFDSSxpQkFBQTtBQ0VoQjs7QURDUTtFQUNJLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxXQUFBO0VBRUEsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtBQ0FaOztBREdJO0VBQ0ksYUFBQTtBQ0RSOztBREVRO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtBQ0FaOztBRElJO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUNGUjs7QURLUTtFQUNJLGtCQUFBO0FDSFo7O0FES1E7RUFDSSxnQkFBQTtBQ0haOztBREtRO0VBQ0ksbUJBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtBQ0haOztBRE9RO0VBQ0ksc0JBQUE7QUNMWjs7QURNWTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLDBCQUFBO0FDSmhCOztBRFVBO0VBQ0ksZ0JBQUE7RUFFQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7QUNQSjs7QURZQTtFQUNJLGdCQUFBO0VBRUEsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtBQ1RKOztBRFdBO0VBQ1EsbUJBQUE7RUFFQSxhQUFBO0FDVFI7O0FEb0JBO0VBQ0kseUJBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QUNqQkoiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9ldm91Y2hlcnN0b2NrcmVwb3J0L2V2b3VjaGVyLXN0b2NrLXJlcG9ydC9hZGQvZXZvdWNoZXItc3RvY2stcmVwb3J0LmFkZC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5iZy1kZWxldGV7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlM2YzO1xyXG4gICAgcGFkZGluZzogMTBweDtcclxufVxyXG5cclxuLm5nLXByaXN0aW5le1xyXG4gICAgY29sb3IgOiB3aGl0ZSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4ubmctcHJpc3RpbmV7XHJcbiAgICBjb2xvcjogYmxhY2sgIWltcG9ydGFudDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgcGFkZGluZzogNnB4IDBweDtcclxufVxyXG5cclxuLm1lbWJlci1idWxrLXVwZGF0ZS1jb250YWluZXIge1xyXG4gICAgcGFkZGluZzogMTBweCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uY29udGFpbmVyLXR3by1idXR0b24ge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxufVxyXG5cclxuLmVycm9yLW1zZy11cGxvYWQge1xyXG4gICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgIHBhZGRpbmctbGVmdDogMHB4ICFpbXBvcnRhbnQ7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAwcHggIWltcG9ydGFudDtcclxufVxyXG5cclxuLmZpbGUtc2VsZWN0ZWQge1xyXG4gICAgY29sb3I6IHJlZDtcclxufVxyXG5cclxuLmJ0bi1hZGQtYnVsayB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG59XHJcblxyXG4uYnRuLXVwbG9hZCwgLmJ0bi1zdWJtaXQge1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG59XHJcblxyXG4uYnRuLXVwbG9hZCB7XHJcbiAgICBib3JkZXItY29sb3I6IGJsYWNrO1xyXG59XHJcblxyXG4uYnRuLXN1Ym1pdCB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG4uc3Rhci1yZXF1aXJlZCB7XHJcbiAgICBjb2xvcjogcmVkO1xyXG59XHJcblxyXG4udGV4dC1yZXF1aXJlZCB7XHJcbiAgICBjb2xvcjogcmVkO1xyXG4gICAgZm9udC1zaXplOiAxMXB4O1xyXG4gICAgbWFyZ2luLXRvcDogLTE1cHg7XHJcbn1cclxuXHJcbmxhYmVse1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG59XHJcbi5jYXJkLWRldGFpbHtcclxuICAgID4uY2FyZC1oZWFkZXJ7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIG1pbi1oZWlnaHQ6IDQ4cHg7XHJcbiAgICAgICAgLmJhY2tfYnV0dG9ue1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IC00cHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAtMTBweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zYXZlX2J1dHRvbntcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICB0b3A6IDEwcHg7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XHJcbiAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICByaWdodDogMTBweDtcclxuICAgICAgICAgICAgLy8gbWFyZ2luLXRvcDogNXB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogMDtcclxuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAxMHB4O1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAuY2FyZC1jb250ZW50e1xyXG4gICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICAgICAgPi5jb2wtbWQtMTJ7XHJcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGgxe1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjogIzU1NTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgfVxyXG4gICAgLm1lbWJlci1kZXRhaWx7XHJcbiAgICAgICAgZm9ybS1zZWxlY3RbbmFtZT1zdGF0dXNdIC5mb3JtLWdyb3VwIHtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBsYWJlbHtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGFiZWwrZGl2e1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDpub3JtYWw7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweDtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2Y4ZjlmYTtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICAgICAgICAgICAgbWluLWhlaWdodDogMjdweDtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgICAgICBjb2xvcjogIzY2NjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIFxyXG4gICAgICAgIC5jYXJkLWhlYWRlcnsgXHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XHJcbiAgICAgICAgICAgIGgye1xyXG4gICAgICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IHVuc2V0O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiB1bnNldDtcclxuICAgICAgICAgICAgICAgIG1hcmdpbjogMHB4O1xyXG4gICAgICAgICAgICAgICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiAgICAgICAgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuLnJvdW5kZWQtYnRuIHtcclxuICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDcwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA3MHB4O1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDQwcHg7XHJcbiAgICBwYWRkaW5nOiAwIDQwcHg7XHJcbiAgICBiYWNrZ3JvdW5kOiAjNTZhNGZmO1xyXG4gICAgLy8gbWFyZ2luLWxlZnQ6IDQwJTtcclxuICAgIC8vIG1hcmdpbi1yaWdodDogNTAlO1xyXG4gICAgfVxyXG5cclxuLnJvdW5kZWQtYnRuLWRhbmdlciB7XHJcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gICAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiA3MHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogNzBweDtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgIGxpbmUtaGVpZ2h0OiA0MHB4O1xyXG4gICAgcGFkZGluZzogMCA0MHB4O1xyXG4gICAgY29sb3I6ICM1NTU7XHJcbiAgICBtYXJnaW4tbGVmdDogMTBweDtcclxuICAgIH1cclxuLnJvdW5kZWQtYnRuOmhvdmVyIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiBkYXJrZW4oIzU2QTRGRiwgMTUlKTtcclxuICAgICAgICAvLyBib3JkZXI6IDJweCBzb2xpZCBibHVlO1xyXG4gICAgICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB9XHJcbi8vIC5kYXRlcGlja2VyLWlucHV0e1xyXG4vLyAgICAgcG9zaXRpb246IGFic29sdXRlOyAgICBcclxuLy8gICAgIH1cclxuLy8gLmZvcm0tY29udHJvbHtcclxuLy8gICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuLy8gICAgIHRvcDogMTBweDtcclxuLy8gICAgIGxlZnQ6IDBweDtcclxuLy8gfVxyXG5cclxuLnNhdmUtYnV0dG9uIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG59XHJcbiIsIi5iZy1kZWxldGUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlM2YzO1xuICBwYWRkaW5nOiAxMHB4O1xufVxuXG4ubmctcHJpc3RpbmUge1xuICBjb2xvcjogd2hpdGUgIWltcG9ydGFudDtcbn1cblxuLm5nLXByaXN0aW5lIHtcbiAgY29sb3I6IGJsYWNrICFpbXBvcnRhbnQ7XG4gIHdpZHRoOiAxMDAlO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIHBhZGRpbmc6IDZweCAwcHg7XG59XG5cbi5tZW1iZXItYnVsay11cGRhdGUtY29udGFpbmVyIHtcbiAgcGFkZGluZzogMTBweCAhaW1wb3J0YW50O1xufVxuXG4uY29udGFpbmVyLXR3by1idXR0b24ge1xuICBkaXNwbGF5OiBmbGV4O1xufVxuXG4uZXJyb3ItbXNnLXVwbG9hZCB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIHBhZGRpbmctbGVmdDogMHB4ICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmctcmlnaHQ6IDBweCAhaW1wb3J0YW50O1xufVxuXG4uZmlsZS1zZWxlY3RlZCB7XG4gIGNvbG9yOiByZWQ7XG59XG5cbi5idG4tYWRkLWJ1bGsge1xuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xufVxuXG4uYnRuLXVwbG9hZCwgLmJ0bi1zdWJtaXQge1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG5cbi5idG4tdXBsb2FkIHtcbiAgYm9yZGVyLWNvbG9yOiBibGFjaztcbn1cblxuLmJ0bi1zdWJtaXQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi5zdGFyLXJlcXVpcmVkIHtcbiAgY29sb3I6IHJlZDtcbn1cblxuLnRleHQtcmVxdWlyZWQge1xuICBjb2xvcjogcmVkO1xuICBmb250LXNpemU6IDExcHg7XG4gIG1hcmdpbi10b3A6IC0xNXB4O1xufVxuXG5sYWJlbCB7XG4gIGNvbG9yOiBibGFjaztcbn1cblxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtaW4taGVpZ2h0OiA0OHB4O1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5iYWNrX2J1dHRvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbWFyZ2luLXRvcDogLTRweDtcbiAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgY29sb3I6ICM1NTU7XG4gIGhlaWdodDogMzBweDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmJhY2tfYnV0dG9uIGkge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuc2F2ZV9idXR0b24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMTBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcbiAgY29sb3I6IHdoaXRlO1xuICBoZWlnaHQ6IDMwcHg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHJpZ2h0OiAxMHB4O1xuICBwYWRkaW5nLXRvcDogMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMDtcbiAgcGFkZGluZy1sZWZ0OiAxMHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50IHtcbiAgcGFkZGluZzogMTVweDtcbn1cbi5jYXJkLWRldGFpbCAuY2FyZC1jb250ZW50ID4gLmNvbC1tZC0xMiB7XG4gIHBhZGRpbmctbGVmdDogMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgaDEge1xuICBmb250LXNpemU6IDE4cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogIzU1NTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgZm9ybS1zZWxlY3RbbmFtZT1zdGF0dXNdIC5mb3JtLWdyb3VwIHtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIGxhYmVsIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCBsYWJlbCArIGRpdiB7XG4gIGJhY2tncm91bmQ6ICNmMGYwZjA7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gIHBhZGRpbmc6IDBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2Y4ZjlmYTtcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbiAgbWluLWhlaWdodDogMjdweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogIzY2Njtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCAuY2FyZC1oZWFkZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5jYXJkLWhlYWRlciBoMiB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBsaW5lLWhlaWdodDogdW5zZXQ7XG4gIGhlaWdodDogdW5zZXQ7XG4gIG1hcmdpbjogMHB4O1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbn1cblxuLnJvdW5kZWQtYnRuIHtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiA3MHB4O1xuICBib3JkZXItcmFkaXVzOiA3MHB4O1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgbGluZS1oZWlnaHQ6IDQwcHg7XG4gIHBhZGRpbmc6IDAgNDBweDtcbiAgYmFja2dyb3VuZDogIzU2YTRmZjtcbn1cblxuLnJvdW5kZWQtYnRuLWRhbmdlciB7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG4gIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNzBweDtcbiAgYm9yZGVyLXJhZGl1czogNzBweDtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE4cHg7XG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xuICBwYWRkaW5nOiAwIDQwcHg7XG4gIGNvbG9yOiAjNTU1O1xuICBtYXJnaW4tbGVmdDogMTBweDtcbn1cblxuLnJvdW5kZWQtYnRuOmhvdmVyIHtcbiAgYmFja2dyb3VuZDogIzBhN2JmZjtcbiAgb3V0bGluZTogbm9uZTtcbn1cblxuLnNhdmUtYnV0dG9uIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcbiAgY29sb3I6IHdoaXRlO1xuICBtYXJnaW4tdG9wOiAyMHB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/modules/evoucherstockreport/evoucher-stock-report/add/evoucher-stock-report.add.component.ts":
/*!*********************************************************************************************************************!*\
  !*** ./src/app/layout/modules/evoucherstockreport/evoucher-stock-report/add/evoucher-stock-report.add.component.ts ***!
  \*********************************************************************************************************************/
/*! exports provided: EvoucherStockAddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EvoucherStockAddComponent", function() { return EvoucherStockAddComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../services/e-voucher/e-voucher.service */ "./src/app/services/e-voucher/e-voucher.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var EvoucherStockAddComponent = /** @class */ (function () {
    function EvoucherStockAddComponent(evoucherService) {
        this.evoucherService = evoucherService;
        this.name = "";
        this.Evouchers = [];
        this.errorLabel = false;
        this.isBulkUpdate = false;
        this.selectedFile = null;
        this.cancel = false;
        this.progressBar = 0;
        this.errorFile = false;
        this.prodOnUpload = false;
        this.startUploading = false;
        this.showUploadButton = false;
        this.successUpload = false;
        this.urlDownload = "";
        this.warnIDPelanggan = false;
        this.warnNamaPemilik = false;
        this.warnTelpPemilik = false;
        this.warnWAPemilik = false;
        this.showPasswordPage = false;
    }
    EvoucherStockAddComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    EvoucherStockAddComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.selectedFile = null;
                this.startUploading = false;
                return [2 /*return*/];
            });
        });
    };
    EvoucherStockAddComponent.prototype.updateDataBulk = function () {
        return __awaiter(this, void 0, void 0, function () {
            var payload, result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        this.startUploading = true;
                        this.cancel = false;
                        payload = {
                            type: 'application/form-data',
                        };
                        if (!this.selectedFile) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.evoucherService.uploadEvoucherCodeBulk(this.selectedFile, this, payload)];
                    case 1:
                        result = _a.sent();
                        if (result) {
                            this.firstLoad();
                        }
                        _a.label = 2;
                    case 2: return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        this.startUploading = false;
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    // async formSubmitAddMember(){
    //     let form_add = this.form;
    //     try {
    //       this.service    = this.evoucherService;
    //       const result: any  = await this.evoucherService.uploadEvoucherCodeBulk(form_add);
    //       console.warn("result add member", result)
    //       if (result && result.data) {
    //         this.getPassword = result.data.password;
    //       }
    //       this.data.username = this.form.id_pel;
    //       this.data.password = this.getPassword;
    //       Swal.fire({
    //         title: 'Success',
    //         text: 'Pelanggan berhasil ditambahkan',
    //         icon: 'success',
    //         confirmButtonText: 'Ok',
    //       }).then((result) => {
    //         if(result.isConfirmed){
    //           this.showThisPassword();
    //         }
    //       });
    //     } catch (e) {
    //       console.warn("error", e.message)
    //       Swal.fire({
    //         title: 'Failed',
    //         text: e.message,
    //         icon: 'warning',
    //         confirmButtonText: 'Ok',
    //       }).then((result) => {
    //         if(result.isConfirmed){
    //           this.firstLoad();
    //         }
    //       });
    //       this.errorLabel = ((<Error>e).message);//conversion to Error type
    //     }
    // }
    EvoucherStockAddComponent.prototype.showThisPassword = function () {
        this.showPasswordPage = !this.showPasswordPage;
        if (this.showPasswordPage == false) {
            this.firstLoad();
        }
        // console.log(this.edit );
    };
    EvoucherStockAddComponent.prototype.onFileSelected = function (event) {
        var _this = this;
        this.errorFile = false;
        var fileMaxSize = 3000000; // let say 3Mb
        // var reader = new FileReader();
        // reader.readAsDataURL(event.target.files[0]); //
        Array.from(event.target.files).forEach(function (file) {
            if (file.size > fileMaxSize) {
                _this.errorFile = "maximum file is 3Mb";
            }
        });
        this.selectedFile = event.target.files[0];
        this.addBulk.nativeElement.value = '';
    };
    EvoucherStockAddComponent.prototype.cancelThis = function () {
        this.cancel = !this.cancel;
    };
    EvoucherStockAddComponent.prototype.actionShowUploadButton = function () {
        this.showUploadButton = !this.showUploadButton;
    };
    EvoucherStockAddComponent.prototype.updateProgressBar = function (value) {
        this.progressBar = value;
    };
    EvoucherStockAddComponent.prototype.backTo = function () {
        window.history.back();
    };
    EvoucherStockAddComponent.ctorParameters = function () { return [
        { type: _services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_2__["EVoucherService"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], EvoucherStockAddComponent.prototype, "back", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], EvoucherStockAddComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('addBulk', { static: false }),
        __metadata("design:type", Object)
    ], EvoucherStockAddComponent.prototype, "addBulk", void 0);
    EvoucherStockAddComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-evoucher-stock-report-add',
            template: __webpack_require__(/*! raw-loader!./evoucher-stock-report.add.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/evoucherstockreport/evoucher-stock-report/add/evoucher-stock-report.add.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./evoucher-stock-report.add.component.scss */ "./src/app/layout/modules/evoucherstockreport/evoucher-stock-report/add/evoucher-stock-report.add.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_2__["EVoucherService"]])
    ], EvoucherStockAddComponent);
    return EvoucherStockAddComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/evoucherstockreport/evoucher-stock-report/detail/evoucher-stock-report.detail.component.scss":
/*!*****************************************************************************************************************************!*\
  !*** ./src/app/layout/modules/evoucherstockreport/evoucher-stock-report/detail/evoucher-stock-report.detail.component.scss ***!
  \*****************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bg-delete {\n  background-color: #ffe3f3;\n  padding: 10px;\n}\n\n.card-detail > .card-header {\n  position: relative;\n}\n\n.card-detail > .card-header .back_button {\n  margin-top: -4px;\n  margin-left: -10px;\n  background-color: transparent;\n  color: #555;\n  height: 30px;\n  padding: 0px 10px 0px 0px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #ccc;\n}\n\n.card-detail > .card-header .back_button i {\n  font-weight: bold;\n}\n\n.card-detail > .card-header .edit_button {\n  position: absolute;\n  top: 7px;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 10px;\n}\n\n.card-detail > .card-header .edit_button i {\n  font-weight: bold;\n}\n\n.card-detail > .card-header .btn-right {\n  background-color: #555;\n  color: #fff;\n  height: 30px;\n  padding: 0px 10px 0px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  margin-left: 10px;\n}\n\n.card-detail .card-content {\n  padding: 15px;\n}\n\n.card-detail .card-content > .col-md-12 {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n\n.card-detail h1 {\n  font-size: 18px;\n  font-weight: bold;\n  color: #555;\n  text-align: center;\n  margin-bottom: 0px;\n}\n\n.card-detail .member-detail form-select[name=status] .form-group {\n  margin-bottom: 0px;\n}\n\n.card-detail .member-detail label {\n  margin-top: 10px;\n}\n\n.card-detail .member-detail label + div {\n  background: #f0f0f0;\n  font-weight: norma;\n  padding: 10px;\n  border: 1px solid #ccc;\n  margin-bottom: 15px;\n  min-height: 27px;\n  font-size: 12px;\n  color: #666;\n}\n\n.card-detail .member-detail .card-header {\n  background-color: #555;\n}\n\n.card-detail .member-detail .card-header h2 {\n  color: white;\n  font-size: 15px;\n  line-height: unset;\n  height: unset;\n  margin: 0px;\n  text-transform: capitalize;\n}\n\n.need-approval-text {\n  color: red;\n  font-size: 14px;\n  margin-bottom: 15px;\n}\n\n.btn-action-stock {\n  display: flex;\n}\n\n.btn-action-stock .btn-approve {\n  background-color: #3498db;\n  color: white;\n  margin-right: 10px;\n  font-size: 14px;\n}\n\n.btn-action-stock .btn-decline {\n  font-size: 14px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZXZvdWNoZXJzdG9ja3JlcG9ydC9ldm91Y2hlci1zdG9jay1yZXBvcnQvZGV0YWlsL0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbW9kdWxlc1xcZXZvdWNoZXJzdG9ja3JlcG9ydFxcZXZvdWNoZXItc3RvY2stcmVwb3J0XFxkZXRhaWxcXGV2b3VjaGVyLXN0b2NrLXJlcG9ydC5kZXRhaWwuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2V2b3VjaGVyc3RvY2tyZXBvcnQvZXZvdWNoZXItc3RvY2stcmVwb3J0L2RldGFpbC9ldm91Y2hlci1zdG9jay1yZXBvcnQuZGV0YWlsLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kseUJBQUE7RUFDQSxhQUFBO0FDQ0o7O0FER0k7RUFDSSxrQkFBQTtBQ0FSOztBRENRO0VBRUksZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLDZCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFFQSx5QkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7QUNEWjs7QURFWTtFQUNJLGlCQUFBO0FDQWhCOztBREdRO0VBQ0ksa0JBQUE7RUFDQSxRQUFBO0VBRUEsV0FBQTtFQUNBLFlBQUE7RUFDQSwwQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBSUEsV0FBQTtBQ0xaOztBREVZO0VBQ0ksaUJBQUE7QUNBaEI7O0FESVE7RUFDSSxzQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsMEJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0FDRlo7O0FES0k7RUFDSSxhQUFBO0FDSFI7O0FESVE7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0FDRlo7O0FEaUJJO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUNmUjs7QURrQlE7RUFDSSxrQkFBQTtBQ2hCWjs7QURrQlE7RUFDSSxnQkFBQTtBQ2hCWjs7QURrQlE7RUFDSSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FDaEJaOztBRG1CUTtFQUNJLHNCQUFBO0FDakJaOztBRGtCWTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLDBCQUFBO0FDaEJoQjs7QUR1QkE7RUFDSSxVQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0FDcEJKOztBRHVCQTtFQUNJLGFBQUE7QUNwQko7O0FEcUJJO0VBQ0kseUJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FDbkJSOztBRHFCSTtFQUNJLGVBQUE7QUNuQlIiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9ldm91Y2hlcnN0b2NrcmVwb3J0L2V2b3VjaGVyLXN0b2NrLXJlcG9ydC9kZXRhaWwvZXZvdWNoZXItc3RvY2stcmVwb3J0LmRldGFpbC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5iZy1kZWxldGV7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlM2YzO1xyXG4gICAgcGFkZGluZzogMTBweDtcclxufVxyXG5cclxuLmNhcmQtZGV0YWlse1xyXG4gICAgPi5jYXJkLWhlYWRlcntcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgLmJhY2tfYnV0dG9ue1xyXG4gICAgICAgICAgICAvLyBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IC00cHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAtMTBweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgaXtcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5lZGl0X2J1dHRvbntcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICB0b3A6IDdweDtcclxuICAgICAgICAgICAgLy8gYmFja2dyb3VuZC1jb2xvcjogIzU1NTtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICBpe1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmlnaHQ6IDEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5idG4tcmlnaHQge1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC5jYXJkLWNvbnRlbnR7XHJcbiAgICAgICAgcGFkZGluZzogMTVweDtcclxuICAgICAgICA+LmNvbC1tZC0xMntcclxuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDBweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAvLyAuaW1hZ2V7XHJcbiAgICAvLyAgICAgaDN7XHJcbiAgICAvLyAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIC8vICAgICB9XHJcbiAgICAvLyAgICAgLmNhcmQtY29udGVudHtcclxuICAgIC8vICAgICAgICAgaW1ne1xyXG4gICAgLy8gICAgICAgICAgICAgbWF4LXdpZHRoOiAxMDAlO1xyXG4gICAgLy8gICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcblxyXG4gICAgLy8gICAgICAgICB9XHJcbiAgICAvLyAgICAgfVxyXG4gICAgLy8gfVxyXG4gICAgaDF7XHJcbiAgICAgICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIGNvbG9yOiAjNTU1O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICB9XHJcbiAgICAubWVtYmVyLWRldGFpbHtcclxuICAgICAgICBmb3JtLXNlbGVjdFtuYW1lPXN0YXR1c10gLmZvcm0tZ3JvdXAge1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxhYmVse1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBsYWJlbCtkaXZ7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICNmMGYwZjA7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0Om5vcm1hO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG4gICAgICAgICAgICBtaW4taGVpZ2h0OiAyN3B4O1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNjY2O1xyXG4gICAgICAgIH1cclxuICAgICAgICBcclxuICAgICAgICAuY2FyZC1oZWFkZXJ7IFxyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xyXG4gICAgICAgICAgICBoMntcclxuICAgICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgICAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiB1bnNldDtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogdW5zZXQ7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW46IDBweDtcclxuICAgICAgICAgICAgICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gICAgICAgIFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG4ubmVlZC1hcHByb3ZhbC10ZXh0IHtcclxuICAgIGNvbG9yOiByZWQ7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG59XHJcblxyXG4uYnRuLWFjdGlvbi1zdG9jayB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgLmJ0bi1hcHByb3ZlIHtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgfVxyXG4gICAgLmJ0bi1kZWNsaW5lIHtcclxuICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICB9XHJcbn0iLCIuYmctZGVsZXRlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZTNmMztcbiAgcGFkZGluZzogMTBweDtcbn1cblxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5iYWNrX2J1dHRvbiB7XG4gIG1hcmdpbi10b3A6IC00cHg7XG4gIG1hcmdpbi1sZWZ0OiAtMTBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIGNvbG9yOiAjNTU1O1xuICBoZWlnaHQ6IDMwcHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCAwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5iYWNrX2J1dHRvbiBpIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uY2FyZC1kZXRhaWwgPiAuY2FyZC1oZWFkZXIgLmVkaXRfYnV0dG9uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDdweDtcbiAgY29sb3I6ICNmZmY7XG4gIGhlaWdodDogMzBweDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDEwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICByaWdodDogMTBweDtcbn1cbi5jYXJkLWRldGFpbCA+IC5jYXJkLWhlYWRlciAuZWRpdF9idXR0b24gaSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNhcmQtZGV0YWlsID4gLmNhcmQtaGVhZGVyIC5idG4tcmlnaHQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xuICBjb2xvcjogI2ZmZjtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwYWRkaW5nOiAwcHggMTBweCAwcHggMTBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQge1xuICBwYWRkaW5nOiAxNXB4O1xufVxuLmNhcmQtZGV0YWlsIC5jYXJkLWNvbnRlbnQgPiAuY29sLW1kLTEyIHtcbiAgcGFkZGluZy1sZWZ0OiAwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDBweDtcbn1cbi5jYXJkLWRldGFpbCBoMSB7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjNTU1O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn1cbi5jYXJkLWRldGFpbCAubWVtYmVyLWRldGFpbCBmb3JtLXNlbGVjdFtuYW1lPXN0YXR1c10gLmZvcm0tZ3JvdXAge1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgbGFiZWwge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIGxhYmVsICsgZGl2IHtcbiAgYmFja2dyb3VuZDogI2YwZjBmMDtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hO1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xuICBtaW4taGVpZ2h0OiAyN3B4O1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiAjNjY2O1xufVxuLmNhcmQtZGV0YWlsIC5tZW1iZXItZGV0YWlsIC5jYXJkLWhlYWRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM1NTU7XG59XG4uY2FyZC1kZXRhaWwgLm1lbWJlci1kZXRhaWwgLmNhcmQtaGVhZGVyIGgyIHtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE1cHg7XG4gIGxpbmUtaGVpZ2h0OiB1bnNldDtcbiAgaGVpZ2h0OiB1bnNldDtcbiAgbWFyZ2luOiAwcHg7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xufVxuXG4ubmVlZC1hcHByb3ZhbC10ZXh0IHtcbiAgY29sb3I6IHJlZDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xufVxuXG4uYnRuLWFjdGlvbi1zdG9jayB7XG4gIGRpc3BsYXk6IGZsZXg7XG59XG4uYnRuLWFjdGlvbi1zdG9jayAuYnRuLWFwcHJvdmUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xuICBjb2xvcjogd2hpdGU7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuLmJ0bi1hY3Rpb24tc3RvY2sgLmJ0bi1kZWNsaW5lIHtcbiAgZm9udC1zaXplOiAxNHB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/modules/evoucherstockreport/evoucher-stock-report/detail/evoucher-stock-report.detail.component.ts":
/*!***************************************************************************************************************************!*\
  !*** ./src/app/layout/modules/evoucherstockreport/evoucher-stock-report/detail/evoucher-stock-report.detail.component.ts ***!
  \***************************************************************************************************************************/
/*! exports provided: EVStockDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EVStockDetailComponent", function() { return EVStockDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../services/e-voucher/e-voucher.service */ "./src/app/services/e-voucher/e-voucher.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var EVStockDetailComponent = /** @class */ (function () {
    function EVStockDetailComponent(evoucherService, route, router) {
        this.evoucherService = evoucherService;
        this.route = route;
        this.router = router;
        this.processNow = false;
        this.edit = false;
        this.errorLabel = false;
    }
    EVStockDetailComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    EVStockDetailComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.evoucherStockDetail = this.detail;
                        this.processNumber = this.detail.process_number;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.evoucherService.detailEvoucherStock(this.processNumber)];
                    case 2:
                        result = _a.sent();
                        console.warn("result detail stock", result);
                        if (result.length > 0) {
                            if (result[0].process_number == this.detail.process_number) {
                                this.evoucherStockDetail = result[0];
                            }
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    EVStockDetailComponent.prototype.backToHere = function () {
        // this.evoucherStockDetail = false;
        this.back[1](this.back[0]);
        // this.firstLoad();
    };
    EVStockDetailComponent.prototype.isProcessNow = function () {
        this.processNow = !this.processNow;
    };
    EVStockDetailComponent.prototype.processEvoucher = function (process_number) {
        return __awaiter(this, void 0, void 0, function () {
            var dataProcess, result, e_2;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        dataProcess = {
                            "process_number": [process_number],
                            "status": "approve"
                        };
                        return [4 /*yield*/, this.evoucherService.processEvoucherCode(dataProcess)];
                    case 1:
                        result = _a.sent();
                        console.warn("result process", result);
                        if (result) {
                            sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
                                title: 'Success',
                                text: 'Voucher Code is Approved',
                                icon: 'success',
                                confirmButtonText: 'Ok',
                            }).then(function (result) {
                                if (result.isConfirmed) {
                                    _this.backToHere();
                                }
                            });
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        e_2 = _a.sent();
                        console.warn("error appove", e_2);
                        alert("Error");
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    EVStockDetailComponent.prototype.loadDetail = function (historyID) {
        return __awaiter(this, void 0, void 0, function () {
            var result, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.service = this.evoucherService;
                        return [4 /*yield*/, this.evoucherService.detailEvoucherStock(historyID)];
                    case 1:
                        result = _a.sent();
                        this.evoucherStockDetail = result.result[0];
                        console.log(this.evoucherStockDetail);
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    EVStockDetailComponent.ctorParameters = function () { return [
        { type: _services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_2__["EVoucherService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], EVStockDetailComponent.prototype, "detail", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], EVStockDetailComponent.prototype, "back", void 0);
    EVStockDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-evoucher-stock-report-detail',
            template: __webpack_require__(/*! raw-loader!./evoucher-stock-report.detail.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/evoucherstockreport/evoucher-stock-report/detail/evoucher-stock-report.detail.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./evoucher-stock-report.detail.component.scss */ "./src/app/layout/modules/evoucherstockreport/evoucher-stock-report/detail/evoucher-stock-report.detail.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_2__["EVoucherService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], EVStockDetailComponent);
    return EVStockDetailComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/evoucherstockreport/evoucher-stock-report/evoucher-stock-report-routing.module.ts":
/*!******************************************************************************************************************!*\
  !*** ./src/app/layout/modules/evoucherstockreport/evoucher-stock-report/evoucher-stock-report-routing.module.ts ***!
  \******************************************************************************************************************/
/*! exports provided: EvoucherStockReportRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EvoucherStockReportRoutingModule", function() { return EvoucherStockReportRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _evoucher_stock_report_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./evoucher-stock-report.component */ "./src/app/layout/modules/evoucherstockreport/evoucher-stock-report/evoucher-stock-report.component.ts");
/* harmony import */ var _add_evoucher_stock_report_add_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add/evoucher-stock-report.add.component */ "./src/app/layout/modules/evoucherstockreport/evoucher-stock-report/add/evoucher-stock-report.add.component.ts");
/* harmony import */ var _detail_evoucher_stock_report_detail_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./detail/evoucher-stock-report.detail.component */ "./src/app/layout/modules/evoucherstockreport/evoucher-stock-report/detail/evoucher-stock-report.detail.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var routes = [
    {
        path: '', component: _evoucher_stock_report_component__WEBPACK_IMPORTED_MODULE_2__["EvoucherStockReportComponent"]
    },
    {
        path: 'add', component: _add_evoucher_stock_report_add_component__WEBPACK_IMPORTED_MODULE_3__["EvoucherStockAddComponent"]
    },
    {
        path: 'detail', component: _detail_evoucher_stock_report_detail_component__WEBPACK_IMPORTED_MODULE_4__["EVStockDetailComponent"]
    },
];
var EvoucherStockReportRoutingModule = /** @class */ (function () {
    function EvoucherStockReportRoutingModule() {
    }
    EvoucherStockReportRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], EvoucherStockReportRoutingModule);
    return EvoucherStockReportRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/evoucherstockreport/evoucher-stock-report/evoucher-stock-report.component.scss":
/*!***************************************************************************************************************!*\
  !*** ./src/app/layout/modules/evoucherstockreport/evoucher-stock-report/evoucher-stock-report.component.scss ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".form {\n  margin-top: 60px;\n}\n\n.container-swapper-option {\n  display: flex;\n  justify-content: center;\n  flex-direction: row;\n  align-items: center;\n}\n\n.container-swapper-option .button-container {\n  border: 2px solid #ddd;\n  border-radius: 8px;\n}\n\n.container-swapper-option .button-container button {\n  color: #ddd;\n  background: white;\n  font-size: 14px;\n  border-radius: 8px;\n}\n\n.container-swapper-option .button-container button.true {\n  background: #2480fb;\n  border-radius: 8px;\n  color: #ddd;\n}\n\n.error-message {\n  background: white;\n  height: 100vh;\n  text-align: center;\n  font-size: 26px;\n  padding-top: 10px;\n}\n\n.error-message .text-message {\n  background: #E74C3C;\n  padding: 20px;\n  margin: 0 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZXZvdWNoZXJzdG9ja3JlcG9ydC9ldm91Y2hlci1zdG9jay1yZXBvcnQvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxtb2R1bGVzXFxldm91Y2hlcnN0b2NrcmVwb3J0XFxldm91Y2hlci1zdG9jay1yZXBvcnRcXGV2b3VjaGVyLXN0b2NrLXJlcG9ydC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvZXZvdWNoZXJzdG9ja3JlcG9ydC9ldm91Y2hlci1zdG9jay1yZXBvcnQvZXZvdWNoZXItc3RvY2stcmVwb3J0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksZ0JBQUE7QUNDSjs7QURFQTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7QUNDSjs7QURBSTtFQUVFLHNCQUFBO0VBQ0Esa0JBQUE7QUNDTjs7QURBTTtFQUNFLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtBQ0VSOztBREFNO0VBRUUsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7QUNDUjs7QURJQTtFQUNFLGlCQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FDREY7O0FER0U7RUFDSSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxjQUFBO0FDRE4iLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9ldm91Y2hlcnN0b2NrcmVwb3J0L2V2b3VjaGVyLXN0b2NrLXJlcG9ydC9ldm91Y2hlci1zdG9jay1yZXBvcnQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZm9ybXtcclxuICAgIG1hcmdpbi10b3AgOiA2MHB4O1xyXG59XHJcblxyXG4uY29udGFpbmVyLXN3YXBwZXItb3B0aW9uIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgLmJ1dHRvbi1jb250YWluZXIge1xyXG5cclxuICAgICAgYm9yZGVyOiAycHggc29saWQgI2RkZDtcclxuICAgICAgYm9yZGVyLXJhZGl1czo4cHg7XHJcbiAgICAgIGJ1dHRvbiB7XHJcbiAgICAgICAgY29sb3I6ICNkZGQ7XHJcbiAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICAgICAgfVxyXG4gICAgICBidXR0b24udHJ1ZSB7XHJcbiAgICAgICBcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjMjQ4MGZiO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICAgICAgICBjb2xvcjogI2RkZDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbi5lcnJvci1tZXNzYWdlIHtcclxuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICBoZWlnaHQ6MTAwdmg7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGZvbnQtc2l6ZToyNnB4O1xyXG4gIHBhZGRpbmctdG9wOiAxMHB4O1xyXG5cclxuICAudGV4dC1tZXNzYWdlIHtcclxuICAgICAgYmFja2dyb3VuZDogI0U3NEMzQztcclxuICAgICAgcGFkZGluZzogMjBweDtcclxuICAgICAgbWFyZ2luOiAwIDEwcHg7XHJcbiAgfVxyXG59IiwiLmZvcm0ge1xuICBtYXJnaW4tdG9wOiA2MHB4O1xufVxuXG4uY29udGFpbmVyLXN3YXBwZXItb3B0aW9uIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4uY29udGFpbmVyLXN3YXBwZXItb3B0aW9uIC5idXR0b24tY29udGFpbmVyIHtcbiAgYm9yZGVyOiAycHggc29saWQgI2RkZDtcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xufVxuLmNvbnRhaW5lci1zd2FwcGVyLW9wdGlvbiAuYnV0dG9uLWNvbnRhaW5lciBidXR0b24ge1xuICBjb2xvcjogI2RkZDtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xufVxuLmNvbnRhaW5lci1zd2FwcGVyLW9wdGlvbiAuYnV0dG9uLWNvbnRhaW5lciBidXR0b24udHJ1ZSB7XG4gIGJhY2tncm91bmQ6ICMyNDgwZmI7XG4gIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgY29sb3I6ICNkZGQ7XG59XG5cbi5lcnJvci1tZXNzYWdlIHtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGhlaWdodDogMTAwdmg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAyNnB4O1xuICBwYWRkaW5nLXRvcDogMTBweDtcbn1cbi5lcnJvci1tZXNzYWdlIC50ZXh0LW1lc3NhZ2Uge1xuICBiYWNrZ3JvdW5kOiAjRTc0QzNDO1xuICBwYWRkaW5nOiAyMHB4O1xuICBtYXJnaW46IDAgMTBweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/modules/evoucherstockreport/evoucher-stock-report/evoucher-stock-report.component.ts":
/*!*************************************************************************************************************!*\
  !*** ./src/app/layout/modules/evoucherstockreport/evoucher-stock-report/evoucher-stock-report.component.ts ***!
  \*************************************************************************************************************/
/*! exports provided: EvoucherStockReportComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EvoucherStockReportComponent", function() { return EvoucherStockReportComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/e-voucher/e-voucher.service */ "./src/app/services/e-voucher/e-voucher.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var EvoucherStockReportComponent = /** @class */ (function () {
    // private options = new RequestOptions(
    //   { headers: new Headers({ 'Content-Type': 'application/json' }) });
    function EvoucherStockReportComponent(route, router, evoucherService, sanitizer) {
        this.route = route;
        this.router = router;
        this.evoucherService = evoucherService;
        this.sanitizer = sanitizer;
        this.swaper = true;
        this.Evouchers = [];
        this.row_id = "process_number";
        this.tableFormat = {
            title: 'E-Voucher Stock Report',
            label_headers: [
                { label: 'Process Number', visible: true, type: 'string', data_row_name: 'process_number' },
                { label: 'Process Type', visible: true, type: 'string', data_row_name: 'process_type' },
                { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
                { label: 'Total Quantity', visible: true, type: 'string', data_row_name: 'total_qty' },
                { label: 'Total SKU', visible: true, type: 'string', data_row_name: 'total_sku' },
                { label: 'Total Value', visible: true, type: 'string', data_row_name: 'total_value' },
                { label: 'Total Price', visible: true, type: 'string', data_row_name: 'total_price' },
                { label: 'Requested Date', visible: true, type: 'date', data_row_name: 'request_date' },
            ],
            row_primary_key: 'process_number',
            formOptions: {
                addForm: true,
                row_id: this.row_id,
                this: this,
                result_var_name: 'Evouchers',
                detail_function: [this, 'callDetail']
            }
        };
        this.tableFormat2 = {
            title: 'E-Voucher Stock Report',
            label_headers: [
                { label: 'Process Number', visible: true, type: 'string', data_row_name: 'process_number' },
                { label: 'Process Type', visible: true, type: 'string', data_row_name: 'process_type' },
                { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
                { label: 'Total Quantity', visible: true, type: 'string', data_row_name: 'total_qty' },
                { label: 'Total SKU', visible: true, type: 'string', data_row_name: 'total_sku' },
                { label: 'Total Value', visible: true, type: 'string', data_row_name: 'total_value' },
                { label: 'Total Price', visible: true, type: 'string', data_row_name: 'total_price' },
                { label: 'Requested Date', visible: true, type: 'date', data_row_name: 'request_date' },
            ],
            row_primary_key: 'process_number',
            formOptions: {
                addForm: true,
                row_id: this.row_id,
                this: this,
                result_var_name: 'Evouchers',
                detail_function: [this, 'callDetail']
            }
        };
        this.form_input = {};
        this.errorLabel = false;
        this.errorMessage = false;
        this.evoucherStockDetail = false;
        this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl("");
    }
    EvoucherStockReportComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    EvoucherStockReportComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 5, , 6]);
                        this.service = this.evoucherService;
                        result = {};
                        if (!(this.swaper == true)) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.evoucherService.getEvoucherStockReport('REQUESTED')];
                    case 1:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 2: return [4 /*yield*/, this.evoucherService.getEvoucherStockReport('APPROVED')];
                    case 3:
                        result = _a.sent();
                        _a.label = 4;
                    case 4:
                        // const result: any = await this.evoucherService.getEvoucherStockReport();
                        this.totalPage = result.total_page;
                        this.Evouchers = result.values;
                        return [3 /*break*/, 6];
                    case 5:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message);
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
                        }
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    EvoucherStockReportComponent.prototype.callDetail = function (data, rowData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                console.warn("data", data);
                console.warn("rowData", rowData);
                try {
                    this.evoucherStockDetail = this.Evouchers.find(function (evoucher) { return evoucher.process_number == rowData.process_number; });
                    // console.warn("Member detail",this.memberDetail);
                }
                catch (e) {
                    this.errorLabel = (e.message); //conversion to Error type
                    if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                        this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
                    }
                }
                return [2 /*return*/];
            });
        });
    };
    EvoucherStockReportComponent.prototype.swapClick = function (bool) {
        this.swaper = bool;
        this.firstLoad();
    };
    EvoucherStockReportComponent.prototype.backToHere = function (obj) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                obj.evoucherStockDetail = false;
                obj.firstLoad();
                return [2 /*return*/];
            });
        });
    };
    EvoucherStockReportComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_4__["EVoucherService"] },
        { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["DomSanitizer"] }
    ]; };
    EvoucherStockReportComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-evoucher-stock-report',
            template: __webpack_require__(/*! raw-loader!./evoucher-stock-report.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/evoucherstockreport/evoucher-stock-report/evoucher-stock-report.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_2__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./evoucher-stock-report.component.scss */ "./src/app/layout/modules/evoucherstockreport/evoucher-stock-report/evoucher-stock-report.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_4__["EVoucherService"],
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["DomSanitizer"]])
    ], EvoucherStockReportComponent);
    return EvoucherStockReportComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/evoucherstockreport/evoucher-stock-report/evoucher-stock-report.module.ts":
/*!**********************************************************************************************************!*\
  !*** ./src/app/layout/modules/evoucherstockreport/evoucher-stock-report/evoucher-stock-report.module.ts ***!
  \**********************************************************************************************************/
/*! exports provided: EvoucherStockReportModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EvoucherStockReportModule", function() { return EvoucherStockReportModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _evoucher_stock_report_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./evoucher-stock-report-routing.module */ "./src/app/layout/modules/evoucherstockreport/evoucher-stock-report/evoucher-stock-report-routing.module.ts");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _evoucher_stock_report_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./evoucher-stock-report.component */ "./src/app/layout/modules/evoucherstockreport/evoucher-stock-report/evoucher-stock-report.component.ts");
/* harmony import */ var _detail_evoucher_stock_report_detail_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./detail/evoucher-stock-report.detail.component */ "./src/app/layout/modules/evoucherstockreport/evoucher-stock-report/detail/evoucher-stock-report.detail.component.ts");
/* harmony import */ var _add_evoucher_stock_report_add_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./add/evoucher-stock-report.add.component */ "./src/app/layout/modules/evoucherstockreport/evoucher-stock-report/add/evoucher-stock-report.add.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var EvoucherStockReportModule = /** @class */ (function () {
    function EvoucherStockReportModule() {
    }
    EvoucherStockReportModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _evoucher_stock_report_routing_module__WEBPACK_IMPORTED_MODULE_3__["EvoucherStockReportRoutingModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_4__["FormBuilderTableModule"],
                _shared__WEBPACK_IMPORTED_MODULE_5__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
            ],
            declarations: [
                _evoucher_stock_report_component__WEBPACK_IMPORTED_MODULE_6__["EvoucherStockReportComponent"],
                _detail_evoucher_stock_report_detail_component__WEBPACK_IMPORTED_MODULE_7__["EVStockDetailComponent"],
                _add_evoucher_stock_report_add_component__WEBPACK_IMPORTED_MODULE_8__["EvoucherStockAddComponent"],
            ],
            exports: [
                _evoucher_stock_report_component__WEBPACK_IMPORTED_MODULE_6__["EvoucherStockReportComponent"],
                _detail_evoucher_stock_report_detail_component__WEBPACK_IMPORTED_MODULE_7__["EVStockDetailComponent"],
            ]
        })
    ], EvoucherStockReportModule);
    return EvoucherStockReportModule;
}());



/***/ })

}]);
//# sourceMappingURL=modules-evoucherstockreport-evoucher-stock-report-evoucher-stock-report-module.js.map