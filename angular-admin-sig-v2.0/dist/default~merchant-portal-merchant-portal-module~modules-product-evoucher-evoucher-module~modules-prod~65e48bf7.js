(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~merchant-portal-merchant-portal-module~modules-product-evoucher-evoucher-module~modules-prod~65e48bf7"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/separated-modules/merchant-products/products/evoucher-list/evoucher-list.component.html":
/*!**********************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/merchant-portal/separated-modules/merchant-products/products/evoucher-list/evoucher-list.component.html ***!
  \**********************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"_container\">\r\n      <div class=\"grid-view\">\r\n            <div class=\"modal\" *ngFor=\"let c of generatedVouchers\" (click)=\"open(content)\">\r\n            </div>\r\n      </div>\r\n</div>\r\n\r\n\r\n\r\n<app-form-builder-table \r\n    [table_data]=\"generatedVouchers\" \r\n    [searchCallback]=\"[service,'getEvoucherStockSummary',this]\"\r\n    [tableFormat]=\"tableFormat\" \r\n    [total_page]=\"totalPage\">\r\n</app-form-builder-table>\r\n\r\n<div id=\"myModal\" class=\"modal {{popUpForm}}\">\r\n    <!-- Modal content -->\r\n    <div class=\"modal-content\">\r\n        <div class=\"modal-header\">\r\n            <span class=\"close\" (click)=\"popUpClose()\">&times;</span>\r\n        </div>\r\n        <div class=\"modal-body\">\r\n            <p>How many vouchers will be disabled?</p>\r\n            <p>Inactive Stock : {{data.qty_inactive}} | Available Stock : {{data.qty_available}}</p>\r\n            <input id=\"inputan\" type=\"number\" [(ngModel)]=\"inputedVoucher\">\r\n            <br />\r\n            <button class=\"btn btn-primary\" (click)=\"approval(data)\">Active Stock</button>\r\n            <button class=\"btn btn-secondary\" (click)=\"disabled(data)\" style=\"margin-left: 20px;\">Inactive\r\n                Stock</button>\r\n\r\n        </div>\r\n        <br />\r\n        <div class=\"modal-body\">\r\n            <h3> How To Use?</h3>\r\n            <p> Jika ingin mengcancel stock voucher, silahkan input angka dan tekan button \"<strong>Inactive\r\n                    Stock \"</strong></p>\r\n            <p>Jika ingin mengaktifkan stock voucher yang tersisa, silahkan input angka tekan button\r\n                \"<strong>Active Stock\"</strong></p>\r\n        </div>\r\n        <div class=\"modal-body\">\r\n            <button class=\"btn btn-color\" (click)=\"approveAllClicked(data)\">Activate All Vouchers</button>\r\n        </div>\r\n        <div class=\"modal-footer\">\r\n\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/separated-modules/merchant-products/products/merchant-add-new-product/merchant-add-new-product.component.html":
/*!********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/merchant-portal/separated-modules/merchant-products/products/merchant-add-new-product/merchant-add-new-product.component.html ***!
  \********************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"form\">\r\n  <div class=\"row\">\r\n    <button class=\"btn back_button\" (click)=\"backClicked()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button>\r\n  </div>\r\n  <input #inputFile4 type=\"file\" style=\"display:none\" (change)=\"onFileSelected2($event, 'image2')\"\r\n    accept=\"image/x-png,image/gif,image/jpeg\">\r\n  <input #inputFile3 type=\"file\" style=\"display:none\" (change)=\"onFileSelected2($event, 'image1')\"\r\n    accept=\"image/x-png,image/gif,image/jpeg\">\r\n  <input #inputFile1 type=\"file\" style=\"display:none\" (change)=\"onFileSelected2($event, 'image3')\"\r\n    accept=\"image/x-png,image/gif,image/jpeg\">\r\n  <input #inputFile2 type=\"file\" style=\"display:none\" (change)=\"onFileSelected2($event, 'image4')\"\r\n    accept=\"image/x-png,image/gif,image/jpeg\">\r\n  <input #inputFile5 type=\"file\" style=\"display:none\" (change)=\"onFileSelected2($event, 'image5')\"\r\n    accept=\"image/x-png,image/gif,image/jpeg\">\r\n\r\n  <div class=\"row header-first\" *ngIf=\"!errorMessage\">\r\n    <div class=\"content-title-container\">\r\n      <h3 class=\"content-title\">Product List<span><i class=\"fa fa-angle-right custom-icon\"></i><span class=\"span-2\">Add\r\n            New Product</span></span></h3>\r\n    </div>\r\n    <div class=\"col-md-12\" *ngIf=\"listOfProductType.length > 0\">\r\n      <div id=\"buttonoption\" class=\"form-group\">\r\n        <div class=\" product-type-list\">\r\n          <div *ngFor=\"let pt of listOfProductType\" class=\"product-type {{pt.active}}\" (click)=\"setProductType(pt)\">\r\n            {{pt.name}}\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <!-- <div class=\"col-md-12\" *ngIf=\"form.type=='evoucher'\">\r\n      <div id=\"buttonoption\" class=\"form-group\">\r\n        <label>Choose Vendor Type: </label>\r\n        <div>\r\n          <div class=\" product-type-list\">\r\n            <div *ngFor=\"let vt of listOfVendorType\" class=\"product-type {{vt.active}}\" (click)=\"setVendorType(vt)\">\r\n              {{vt.name}}\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div> -->\r\n    <!-- Redeem Type -->\r\n    <!-- <div class=\"col-md-12\" *ngIf=\"form.type=='evoucher'\">\r\n      <div class=\"form-group\">\r\n        <label>Choose Redeem Type: </label>\r\n        <div>\r\n          <div class=\" product-type-list\">\r\n            <div *ngFor=\"let vt of listOfRedeemType\" class=\"product-type {{vt.active}}\" (click)=\"setRedeemType(vt)\">\r\n              {{vt.name}}\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div> -->\r\n    <!-- Begining Of Multiple Image Upload -->\r\n    <div class=\"upload-container col-md-12\" [ngClass]=\"photoAlert? 'red-alert' : ''\" id=\"photo-alert\">\r\n      <div class=\"content-title-container\">\r\n        \r\n        <h3 class=\"content-title\">Upload {{title}}</h3>\r\n        <label>File size max. 5 MB and file format must be .JPG/.JPEG/.PNG</label>\r\n      </div>\r\n      <!-- <input type=\"text\" [ngModel]=\"uploads.image3\" #userinput> -->\r\n      <!-- <button (click)=\"copyInputMessage(userinput)\" value=\"click to copy\" >Copy from Textbox</button> -->\r\n      <div class=\"section-upload row left-section-upload\">\r\n        <div class=\"col-md-2dot4 col-half-offset\">\r\n          <div class=\"card cl-list\">\r\n            <div class=\"card-content\" *ngIf=\"!uploads.image3 && !showLoading3\">\r\n              <div class=\"image\">\r\n                <div class=\"card-content custom\">\r\n                  <div class=\"image-canvas\">\r\n                    <img *ngIf=\"!uploads.image3\" class=\"logo-upload\" src=\"/assets/images/image-solid.svg\">\r\n                  </div>\r\n                  <!-- <p class=\"upload-desc\">Upload your image here <br> or</p> -->\r\n                  <div class=\"browse-files\">\r\n                    <div class=\"pb-framer\">\r\n                      <div class=\"progressbar {{cancel?'terminated':''}}\" [ngStyle]=\"{'width.%': progressBar}\">\r\n\r\n                      </div>\r\n                      <span *ngIf=\"progressBar<58&&progressBar>0\">\r\n                        {{cancel?'terminated':progressBar+'%'}} </span>\r\n                      <span class=\"clr-white\" *ngIf=\"progressBar>58\">\r\n                        {{cancel?'terminated':progressBar+'%'}} </span>\r\n                    </div>\r\n                    <a class=\"common-button blue-upload fileupload\" (click)=\"inputFile1.click()\">+ Add Image</a>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div *ngIf=\"errorLabel!==false\">\r\n                {{errorLabel}}\r\n              </div>\r\n            </div>\r\n            <div class=\"form-group uploaded\" *ngIf=\"showLoading3\">\r\n              <div class=\"loading\">\r\n                <div class=\"sk-fading-circle\">\r\n                  <div class=\"sk-circle1 sk-circle\"></div>\r\n                  <div class=\"sk-circle2 sk-circle\"></div>\r\n                  <div class=\"sk-circle3 sk-circle\"></div>\r\n                  <div class=\"sk-circle4 sk-circle\"></div>\r\n                  <div class=\"sk-circle5 sk-circle\"></div>\r\n                  <div class=\"sk-circle6 sk-circle\"></div>\r\n                  <div class=\"sk-circle7 sk-circle\"></div>\r\n                  <div class=\"sk-circle8 sk-circle\"></div>\r\n                  <div class=\"sk-circle9 sk-circle\"></div>\r\n                  <div class=\"sk-circle10 sk-circle\"></div>\r\n                  <div class=\"sk-circle11 sk-circle\"></div>\r\n                  <div class=\"sk-circle12 sk-circle\"></div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"form-group uploaded\" *ngIf=\"uploads.image3 && !showLoading3\">\r\n              <!-- <i class=\"fa fa-times-circle remove-icon\" (click)=\"deleteImage('image3')\"></i> -->\r\n              <div class=\"uploaded-image2\">\r\n                <div class=\"img\">\r\n                  <img class=\"resize\" src=\"{{uploads.image3}}\">\r\n                  <a class=\"common-button blue-upload fileupload\" (click)=\"inputFile1.click()\">Re-Upload</a>\r\n                </div>\r\n\r\n              </div>\r\n\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-2dot4\" *ngIf=\"form.type=='product'|| form.type=='voucher' || form.type=='gold'\">\r\n\r\n          <div class=\"card cl-list\">\r\n            <div class=\"card-content\" *ngIf=\"!uploads.image1 && !showLoading\">\r\n              <div class=\"image\">\r\n                <div class=\"card-content custom\">\r\n                  <div class=\"image-canvas\">\r\n\r\n                    <img *ngIf=\"!uploads.image1\" class=\"logo-upload\" src=\"/assets/images/image-solid.svg\">\r\n                  </div>\r\n                  <!-- <p class=\"upload-desc\">Upload your image here <br> or</p> -->\r\n                  <div class=\"browse-files\">\r\n                    <div class=\"pb-framer\">\r\n                      <div class=\"progressbar {{cancel?'terminated':''}}\" [ngStyle]=\"{'width.%': progressBar}\">\r\n\r\n                      </div>\r\n                      <span *ngIf=\"progressBar<58&&progressBar>0\">\r\n                        {{cancel?'terminated':progressBar+'%'}} </span>\r\n                      <span class=\"clr-white\" *ngIf=\"progressBar>58\">\r\n                        {{cancel?'terminated':progressBar+'%'}} </span>\r\n\r\n                    </div>\r\n\r\n                    <a class=\"common-button blue-upload fileupload\" (click)=\"inputFile3.click()\">+ Add Image</a>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div *ngIf=\"errorLabel!==false\">\r\n                {{errorLabel}}\r\n              </div>\r\n            </div>\r\n            <div class=\"form-group uploaded\" *ngIf=\"showLoading\">\r\n              <div class=\"loading\">\r\n                <div class=\"sk-fading-circle\">\r\n                  <div class=\"sk-circle1 sk-circle\"></div>\r\n                  <div class=\"sk-circle2 sk-circle\"></div>\r\n                  <div class=\"sk-circle3 sk-circle\"></div>\r\n                  <div class=\"sk-circle4 sk-circle\"></div>\r\n                  <div class=\"sk-circle5 sk-circle\"></div>\r\n                  <div class=\"sk-circle6 sk-circle\"></div>\r\n                  <div class=\"sk-circle7 sk-circle\"></div>\r\n                  <div class=\"sk-circle8 sk-circle\"></div>\r\n                  <div class=\"sk-circle9 sk-circle\"></div>\r\n                  <div class=\"sk-circle10 sk-circle\"></div>\r\n                  <div class=\"sk-circle11 sk-circle\"></div>\r\n                  <div class=\"sk-circle12 sk-circle\"></div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"form-group uploaded\" *ngIf=\"uploads.image1 && !showLoading\">\r\n              <i class=\"fa fa-times-circle remove-icon\" (click)=\"deleteImage('image1')\"></i>\r\n              <div class=\"uploaded-image2\">\r\n                <div class=\"img\">\r\n                  <img class=\"resize\" src=\"{{uploads.image1}}\">\r\n                  <a class=\"common-button blue-upload fileupload\" (click)=\"inputFile3.click()\">Re-Upload</a>\r\n                </div>\r\n\r\n              </div>\r\n\r\n            </div>\r\n          </div>\r\n\r\n        </div>\r\n        <div class=\"col-md-2dot4 col-half-offset\" *ngIf=\"form.type=='product'|| form.type=='voucher' || form.type=='gold'\">\r\n\r\n          <div class=\"card cl-list\">\r\n            <div class=\"card-content\" *ngIf=\"!uploads.image2 && !showLoading2\">\r\n              <div class=\"image\">\r\n                <div class=\"card-content custom\">\r\n                  <div class=\"image-canvas\">\r\n\r\n                    <img *ngIf=\"!uploads.image2\" class=\"logo-upload\" src=\"/assets/images/image-solid.svg\">\r\n                  </div>\r\n                  <!-- <p class=\"upload-desc\">Upload your image here <br> or</p> -->\r\n                  <div class=\"browse-files\">\r\n                    <div class=\"pb-framer\">\r\n                      <div class=\"progressbar {{cancel?'terminated':''}}\" [ngStyle]=\"{'width.%': progressBar}\">\r\n\r\n                      </div>\r\n                      <span *ngIf=\"progressBar<58&&progressBar>0\">\r\n                        {{cancel?'terminated':progressBar+'%'}} </span>\r\n                      <span class=\"clr-white\" *ngIf=\"progressBar>58\">\r\n                        {{cancel?'terminated':progressBar+'%'}} </span>\r\n\r\n                    </div>\r\n\r\n                    <a class=\"common-button blue-upload fileupload\" (click)=\"inputFile4.click()\">+ Add Image</a>\r\n\r\n\r\n                  </div>\r\n\r\n                </div>\r\n\r\n              </div>\r\n              <div *ngIf=\"errorLabel!==false\">\r\n                {{errorLabel}}\r\n              </div>\r\n            </div>\r\n            <div class=\"form-group uploaded\" *ngIf=\"showLoading2\">\r\n              <div class=\"loading\">\r\n                <div class=\"sk-fading-circle\">\r\n                  <div class=\"sk-circle1 sk-circle\"></div>\r\n                  <div class=\"sk-circle2 sk-circle\"></div>\r\n                  <div class=\"sk-circle3 sk-circle\"></div>\r\n                  <div class=\"sk-circle4 sk-circle\"></div>\r\n                  <div class=\"sk-circle5 sk-circle\"></div>\r\n                  <div class=\"sk-circle6 sk-circle\"></div>\r\n                  <div class=\"sk-circle7 sk-circle\"></div>\r\n                  <div class=\"sk-circle8 sk-circle\"></div>\r\n                  <div class=\"sk-circle9 sk-circle\"></div>\r\n                  <div class=\"sk-circle10 sk-circle\"></div>\r\n                  <div class=\"sk-circle11 sk-circle\"></div>\r\n                  <div class=\"sk-circle12 sk-circle\"></div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"form-group uploaded\" *ngIf=\"uploads.image2 && !showLoading2\">\r\n              <i class=\"fa fa-times-circle remove-icon\" (click)=\"deleteImage('image2')\"></i>\r\n              <div class=\"uploaded-image2\">\r\n                <div class=\"img\">\r\n                  <img class=\"resize\" src=\"{{uploads.image2}}\">\r\n                  <a class=\"common-button blue-upload fileupload\" (click)=\"inputFile4.click()\">Re-Upload</a>\r\n                </div>\r\n\r\n              </div>\r\n\r\n            </div>\r\n          </div>\r\n        </div>\r\n\r\n        <div class=\"col-md-2dot4 col-half-offset\"   *ngIf=\"form.type=='product' || form.type=='voucher' || form.type=='gold'\">\r\n\r\n          <div class=\"card cl-list\">\r\n            <div class=\"card-content\" *ngIf=\"!uploads.image4 && !showLoading4\">\r\n              <div class=\"image\">\r\n                <div class=\"card-content custom\">\r\n                  <div class=\"image-canvas\">\r\n                    <img *ngIf=\"!uploads.image4\" class=\"logo-upload\" src=\"/assets/images/image-solid.svg\">\r\n                  </div>\r\n                  <!-- <p class=\"upload-desc\">Upload your image here <br> or</p> -->\r\n                  <div class=\"browse-files\">\r\n                    <div class=\"pb-framer\">\r\n                      <div class=\"progressbar {{cancel?'terminated':''}}\" [ngStyle]=\"{'width.%': progressBar}\">\r\n\r\n                      </div>\r\n                      <span *ngIf=\"progressBar<58&&progressBar>0\">\r\n                        {{cancel?'terminated':progressBar+'%'}} </span>\r\n                      <span class=\"clr-white\" *ngIf=\"progressBar>58\">\r\n                        {{cancel?'terminated':progressBar+'%'}} </span>\r\n                    </div>\r\n                    <a class=\"common-button blue-upload fileupload\" (click)=\"inputFile2.click()\">+ Add Image</a>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div *ngIf=\"errorLabel!==false\">\r\n                {{errorLabel}}\r\n              </div>\r\n            </div>\r\n            <div class=\"form-group uploaded\" *ngIf=\"showLoading4\">\r\n              <div class=\"loading\">\r\n                <div class=\"sk-fading-circle\">\r\n                  <div class=\"sk-circle1 sk-circle\"></div>\r\n                  <div class=\"sk-circle2 sk-circle\"></div>\r\n                  <div class=\"sk-circle3 sk-circle\"></div>\r\n                  <div class=\"sk-circle4 sk-circle\"></div>\r\n                  <div class=\"sk-circle5 sk-circle\"></div>\r\n                  <div class=\"sk-circle6 sk-circle\"></div>\r\n                  <div class=\"sk-circle7 sk-circle\"></div>\r\n                  <div class=\"sk-circle8 sk-circle\"></div>\r\n                  <div class=\"sk-circle9 sk-circle\"></div>\r\n                  <div class=\"sk-circle10 sk-circle\"></div>\r\n                  <div class=\"sk-circle11 sk-circle\"></div>\r\n                  <div class=\"sk-circle12 sk-circle\"></div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"form-group uploaded\" *ngIf=\"uploads.image4 && !showLoading4\">\r\n              <i class=\"fa fa-times-circle remove-icon\" (click)=\"deleteImage('image4')\"></i>\r\n              <div class=\"uploaded-image2\">\r\n                <div class=\"img\">\r\n                  <img class=\"resize\" src=\"{{uploads.image4}}\">\r\n                  <a class=\"common-button blue-upload fileupload\" (click)=\"inputFile2.click()\">Re-Upload</a>\r\n                </div>\r\n\r\n              </div>\r\n\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-2dot4 col-half-offset\"   *ngIf=\"form.type=='product'|| form.type=='voucher' || form.type=='gold'\">\r\n\r\n          <div class=\"card cl-list\">\r\n            <div class=\"card-content\" *ngIf=\"!uploads.image5 && !showLoading5\">\r\n              <div class=\"image\">\r\n                <div class=\"card-content custom\">\r\n                  <div class=\"image-canvas\">\r\n                    <img *ngIf=\"!uploads.image5\" class=\"logo-upload\" src=\"/assets/images/image-solid.svg\">\r\n                  </div>\r\n                  <!-- <p class=\"upload-desc\">Upload your image here <br> or</p> -->\r\n                  <div class=\"browse-files\">\r\n                    <div class=\"pb-framer\">\r\n                      <div class=\"progressbar {{cancel?'terminated':''}}\" [ngStyle]=\"{'width.%': progressBar}\">\r\n\r\n                      </div>\r\n                      <span *ngIf=\"progressBar<58&&progressBar>0\">\r\n                        {{cancel?'terminated':progressBar+'%'}} </span>\r\n                      <span class=\"clr-white\" *ngIf=\"progressBar>58\">\r\n                        {{cancel?'terminated':progressBar+'%'}} </span>\r\n                    </div>\r\n                    <a class=\"common-button blue-upload fileupload\" (click)=\"inputFile5.click()\">+ Add Image</a>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div *ngIf=\"errorLabel!==false\">\r\n                {{errorLabel}}\r\n              </div>\r\n            </div>\r\n            <div class=\"form-group uploaded\" *ngIf=\"showLoading5\">\r\n              <div class=\"loading\">\r\n                <div class=\"sk-fading-circle\">\r\n                  <div class=\"sk-circle1 sk-circle\"></div>\r\n                  <div class=\"sk-circle2 sk-circle\"></div>\r\n                  <div class=\"sk-circle3 sk-circle\"></div>\r\n                  <div class=\"sk-circle4 sk-circle\"></div>\r\n                  <div class=\"sk-circle5 sk-circle\"></div>\r\n                  <div class=\"sk-circle6 sk-circle\"></div>\r\n                  <div class=\"sk-circle7 sk-circle\"></div>\r\n                  <div class=\"sk-circle8 sk-circle\"></div>\r\n                  <div class=\"sk-circle9 sk-circle\"></div>\r\n                  <div class=\"sk-circle10 sk-circle\"></div>\r\n                  <div class=\"sk-circle11 sk-circle\"></div>\r\n                  <div class=\"sk-circle12 sk-circle\"></div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"form-group uploaded\" *ngIf=\"uploads.image5 && !showLoading5\">\r\n              <i class=\"fa fa-times-circle remove-icon\" (click)=\"deleteImage('image5')\"></i>\r\n              <div class=\"uploaded-image2\">\r\n                <div class=\"img\">\r\n                  <img class=\"resize\" src=\"{{uploads.image5}}\">\r\n                  <a class=\"common-button blue-upload fileupload\" (click)=\"inputFile5.click()\">Re-Upload</a>\r\n                </div>\r\n              </div>\r\n\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div *ngIf=\"photoAlert\" class=\"red-error-alert\">*{{title}} harus memiliki minimum 1 gambar</div>\r\n    <!-- End Of Multiple Image Upload -->\r\n    <!-- Product Information -->\r\n    <div class=\"upload-container col-md-12\" id=\"product-information\">\r\n      <div class=\"content-title-container\">\r\n        <h3 class=\"content-title\">{{title}} Information</h3>\r\n      </div>\r\n\r\n      <div class=\"row\">\r\n        <!-- <div class=\"col-md-6\" *ngIf=\"currentPermission=='admin'\">\r\n          <div class=\"form-group\">\r\n            <div class=\"label\">\r\n              <label>Choose Merchant</label>\r\n            </div>\r\n            <select class=\"form-control\" [(ngModel)]=\"form.merchant_username\">\r\n              <option *ngFor=\"let mt of listOfMerchant\" [ngValue]='mt.name'>{{mt.name}}</option>\r\n            </select>\r\n          </div>\r\n        </div> -->\r\n        <!-- <div class=\"form-group\" *ngIf=\"form.type == 'evoucher'\">\r\n    <label>Merchant Group</label>\r\n    <select class=\"form-control\" [(ngModel)]=\"form.merchant_group\">\r\n      <option *ngFor=\"let mg of listOfVendorType\" [ngValue]=\"mg.name\">{{mg.name}}</option>\r\n    </select>\r\n  </div>\r\n  \r\n  <div class=\"form-group\" *ngIf=\"form.type == 'evoucher'\">\r\n    <label>Redeem Type</label>\r\n    <select class=\"form-control\" *ngIf=\"form.type == 'evoucher'\">\r\n      <option *ngFor = \"let rt of listOfRedeemType\" [ngValue]=\"rt.name\">{{rt.name}}</option>\r\n  </select>\r\n  </div> -->\r\n\r\n        <!-- PRoduct Name -->\r\n        <div class=\"col-md-6\">\r\n          <div class=\"form-group\">\r\n            <div class=\"label\">\r\n              <label for=\"exampleInputEmail1\"><span  *ngIf=\"form.type\">{{title}}</span> Name</label>\r\n            </div>\r\n            <div class=\"col-md-8\">\r\n              <input (input)=\"productNameInputed($event)\" class=\"product_name form-control\"\r\n              [(ngModel)]=\"form.product_name\" placeholder=\"Example : Asus ROG Zephyrus S15 GX502 \" [ngClass]=\"productNameAlert? 'red-alert' : ''\"/>\r\n              <div *ngIf=\"productNameAlert\" class=\"red-error-alert\">*product name tidak boleh kosong</div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n\r\n        <!-- PRoduct Code -->\r\n        <div class=\"col-md-6\">\r\n          <div class=\"form-group\">\r\n            <div class=\"label\">\r\n              <label for=\"exampleInputEmail1\">SKU /Item Code </label>\r\n              <i class=\"fas fa-info-circle\" id=\"tooltip\"> <span id=\"tooltiptext\"> Maximum Character For {{title}} Code is\r\n                  20\r\n                  Characters</span></i>\r\n              <span class=\"autoSKU\">\r\n                <!-- <mat-checkbox [(ngModel)]=\"autoSKU\" value=\"autoSKU\">\r\n        auto-type SKU\r\n      </mat-checkbox> -->\r\n              </span>\r\n            </div>\r\n            <div class=\"col-md-8\">\r\n              <input class=\"product_code form-control\" [(ngModel)]=\"form.product_code\" [ngClass]=\"productCodeAlert? 'red-alert' : ''\"/>\r\n              <div *ngIf=\"productCodeAlert\" class=\"red-error-alert\">*product code tidak boleh kosong</div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-6\">\r\n          <div class=\"form-group\" *ngIf=\"form.type=='e-voucher'\">\r\n            <div class=\"label\">\r\n              <label>Category Voucher</label>\r\n            </div>\r\n            <div class=\"col-md-8\">\r\n              <select class=\"form-control\" [(ngModel)]=\"form.category\" [ngClass]=\"categoryAlert? 'red-alert' : ''\">\r\n                <option *ngFor=\"let mt of categoryEvoucher\" [ngValue]='mt.code'>{{mt.name}}</option>\r\n              </select>\r\n              <div *ngIf=\"categoryAlert\" class=\"red-error-alert\">*category tidak boleh kosong</div>\r\n            </div>\r\n          </div>\r\n          <div class=\"form-group\" *ngIf=\"form.type=='product'\">\r\n            <div class=\"label\">\r\n              <label>Categories {{title}}</label>\r\n            </div>\r\n            <div class=\"col-md-8\">\r\n              <select class=\"form-control\" [(ngModel)]=\"form.category\" [ngClass]=\"categoryAlert? 'red-alert' : ''\">\r\n                <option *ngFor=\"let mt of categoryProduct\" [ngValue]='mt.code'>{{mt.name}}</option>\r\n              </select>\r\n              <div *ngIf=\"categoryAlert\" class=\"red-error-alert\">*category tidak boleh kosong</div>\r\n            </div>\r\n          </div>\r\n          <div class=\"form-group\" *ngIf=\"form.type=='voucher'\">\r\n            <div class=\"label\">\r\n              <label>Categories {{title}}</label>\r\n            </div>\r\n            <div class=\"col-md-8\">\r\n              <select class=\"form-control\" [(ngModel)]=\"form.category\" [ngClass]=\"categoryAlert? 'red-alert' : ''\">\r\n                <option *ngFor=\"let mt of categoryVoucher\" [ngValue]='mt.code'>{{mt.name}}</option>\r\n              </select>\r\n              <div *ngIf=\"categoryAlert\" class=\"red-error-alert\">*category tidak boleh kosong</div>\r\n            </div>\r\n          </div>\r\n          <div class=\"form-group\" *ngIf=\"form.type=='gold'\">\r\n            <div class=\"label\">\r\n              <label>Categories {{title}}</label>\r\n            </div>\r\n            <div class=\"col-md-8\">\r\n              <select class=\"form-control\" [(ngModel)]=\"form.category\" [ngClass]=\"categoryAlert? 'red-alert' : ''\">\r\n                <option *ngFor=\"let mt of categoryGold\" [ngValue]='mt.code'>{{mt.name}}</option>\r\n              </select>\r\n              <div *ngIf=\"categoryAlert\" class=\"red-error-alert\">*category tidak boleh kosong</div>\r\n            </div>\r\n          </div>\r\n          <div class=\"form-group\" *ngIf=\"form.type=='top-up'\">\r\n            <div class=\"label\">\r\n              <label>Categories {{title}}</label>\r\n            </div>\r\n            <div class=\"col-md-8\">\r\n              <select class=\"form-control\" [(ngModel)]=\"form.category\" [ngClass]=\"categoryAlert? 'red-alert' : ''\">\r\n                <option *ngFor=\"let mt of categoryEwallet\" [ngValue]='mt.code'>{{mt.name}}</option>\r\n              </select>\r\n              <div *ngIf=\"categoryAlert\" class=\"red-error-alert\">*category tidak boleh kosong</div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <!-- <div class=\"col-md-6\">\r\n        <div class=\"navbar-menu\">\r\n          <div class=\"list-group category-list\">\r\n            <div *ngIf=\"form.type=='evoucher'\">\r\n              <div class=\"list-group-item\" *ngFor=\"let n of multipleCategoriesDeals; let i = index;\"\r\n                (click)=\"toggleCategories(i)\">\r\n                <span class=\"checked-icon {{n.value}}\">&#10003; </span> {{n.name}}\r\n              </div>\r\n            </div>\r\n            <div *ngIf=\"form.type=='product'\">\r\n              <div class=\"list-group-item\" *ngFor=\"let n of multipleCategoriesProduct; let i = index;\"\r\n                (click)=\"toggleCategories(i)\">\r\n                <span class=\"checked-icon {{n.value}}\">&#10003; </span> {{n.name}}\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div> -->\r\n        <!-- <div class=\"col-md-6\">\r\n          <div class=\"form-group\">\r\n            <div class=\"label\">\r\n              <label for=\"exampleInputEmail1\">Hashtags</label>\r\n            </div>\r\n            <div class=\"form-control hashhash\">\r\n              <span class=\"hashtag-item\" *ngFor=\"let ht of form.hashtags\"> {{ht}}</span>\r\n              <div class=\"hashtagskey-input\" contenteditable=\"true\" [textContent]=\"hashtagsKey\"\r\n                (keyup)=\"hashtagsKeyDown($event)\"></div>\r\n\r\n            </div>\r\n          </div>\r\n        </div> -->\r\n      </div>\r\n\r\n    </div>\r\n    <!-- End Of Product Information -->\r\n\r\n    <!-- Product Description-->\r\n    <div class=\"upload-container col-md-12\" id=\"description\">\r\n      <div class=\"content-title-container\">\r\n        <h3 class=\"content-title\">{{title}} Description</h3>\r\n      </div>\r\n      <div class=\"row\">\r\n        <!-- <div class=\"col-md-6\">\r\n          <div class=\"form-group\" *ngIf=\"form.type =='product'\">\r\n            <div class=\"label\">\r\n              <label>Condition</label>\r\n            </div>\r\n            <select class=\"form-control\" [(ngModel)]=\"form.condition\">\r\n              <option [ngValue]=\"'new'\">New</option>\r\n              <option [ngValue]=\"'second'\">Second</option>\r\n            </select>\r\n          </div>\r\n          \r\n        </div> -->\r\n        <div class=\"col-md-8\" *ngIf=\"form.type=='e-voucher'\">\r\n          <div class=\"form-group\">\r\n            <div class=\"label\">\r\n              <label for=\"exampleInputEmail1\">Terms &amp; Conditions</label>\r\n            </div>\r\n            <ckeditor style=\"width: 100%;\" [config]=\"config\" class=\"tnc \" [editor]=\"Editor\" [(ngModel)]=\"form.tnc\" [ngClass]=\"tncAlert? 'red-alert' : ''\" id=\"tnc\">\r\n            </ckeditor>\r\n          </div>\r\n          <div *ngIf=\"tncAlert\" class=\"red-error-alert\">*tnc tidak boleh kosong</div>\r\n        </div>\r\n        <div class=\"col-md-8\">\r\n          <div class=\"form-group\">\r\n            <div class=\"label\">\r\n              <label for=\"exampleInputEmail1\">Description</label>\r\n            </div>\r\n            <!-- <textarea class=\"form-control\" style=\"min-height: 200px;\" [(ngModel)]=\"form.description\" placeholder=\"Write your product description here...\"></textarea> -->\r\n            <ckeditor style=\"width: 100%;\" class=\"description \" [config]=\"config\" [editor]=\"Editor\"\r\n              [(ngModel)]=\"form.description\" [ngClass]=\"descriptionAlert? 'red-alert' : ''\" id=\"description\"></ckeditor>\r\n            <!-- <input class=\"description form-control\" [(ngModel)]=\"form.description\" /> -->\r\n          </div>\r\n          <div *ngIf=\"descriptionAlert\" class=\"red-error-alert\">*description tidak boleh kosong</div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <!--End Of Product Description-->\r\n\r\n    <!-- Varian -->\r\n    <div class=\"upload-container col-md-12\" *ngIf=\"multiVarian.length > 0\" id=\"varian\" [ngClass]=\"varianAlert? 'red-alert' : ''\">\r\n      <div class=\"content-title-container\">\r\n        <h3 class=\"content-title\">Varian</h3>\r\n      </div>\r\n\r\n      <div class=\"label\">\r\n        <label for=\"exampleInputEmail1\">Tipe Varian</label>\r\n      </div>\r\n      <!-- <div class=\"col-md-12\" style=\"display:flex\" *ngFor=\"let _dataVarian of dataVarian; index as x\">\r\n        <div class=\"col-md-6\">\r\n          <div class=\"col-md-12\" class=\"varian-parent\">\r\n            <form-input  [type]=\"'text'\" [placeholder]=\"'Masukkan nama variasi, contoh : warna, dll'\" autofocus required (keyup)=\"changeText()\" [(ngModel)] = varianHeader[x] class=\"varian-type-input\"></form-input>\r\n            <i class=\"fas fa-trash\" id=\"tooltip\" (click) = deleteVariationType(x)></i>\r\n          </div>\r\n        </div>\r\n        \r\n        <div class=\"col-md-6\">\r\n          <div class=\"col-md-12\" *ngFor=\"let _valueVariation of _dataVarian; index as i\" class=\"varian-parent\">\r\n            <form-input [type]=\"'text'\" [placeholder]=\"'Masukkan pilihan variasi, contoh : merah, dll'\" autofocus required [(ngModel)]=\"dataVarianModel[x][i]\" (keyup)=\"changeText()\" class=\"varian-value-input\"></form-input>\r\n            <i *ngIf=\"_dataVarian.length > 1\" class=\"fas fa-trash\" id=\"tooltip\" (click) = deleteVariationValue(x,i)></i>\r\n          </div>\r\n\r\n\r\n          <button type=\"button\" (click)=\"addVariationValue(x)\" class=\"btn add-value\">Add Value</button>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"col-md-12\" *ngIf=\"dataVarian.length < 2\">\r\n        <button type=\"button\" (click)=\"addVariationType()\" class=\"btn\">Add Varian</button>\r\n      </div>\r\n      <div class=\"col-md-12 varian-table\">\r\n        <div class=\"form-group\">\r\n          <table class=\"table table-striped\">\r\n            <thead>\r\n              <tr>\r\n                <th *ngFor=\"let _varianHeader of varianHeader\">{{_varianHeader}}</th>\r\n                <th *ngFor=\"let _headerValue of headerValue\">{{_headerValue}}</th>\r\n              </tr>\r\n            </thead>\r\n            <tbody>\r\n              <tr *ngFor=\"let _productSku of productSku; index as i\">\r\n                <td *ngFor=\"let _varianHeader of varianHeader\">\r\n                  {{_productSku.variant[_varianHeader]}}\r\n                </td>\r\n                <td>\r\n                  <input class=\"price form-control\" [(ngModel)]=\"_productSku.sku_code\" />\r\n                </td>\r\n                <td>\r\n                  <input class=\"price form-control\" [(ngModel)]=\"_productSku.sku_price\" />\r\n                </td>\r\n                <td>\r\n                  <input class=\"price form-control\" [(ngModel)]=\"_productSku.sku_value\" />\r\n                </td>\r\n                <td>\r\n                  <input class=\"price form-control\" [(ngModel)]=\"_productSku.qty\" />\r\n                </td>\r\n                <td *ngIf=\"_productSku.image_gallery.length == 0 && !isLoading[i]\">\r\n                  <input #inputFileVarian type=\"file\" style=\"display:none\" (change)=\"onFileVariant($event, _productSku.image_gallery, i)\" accept=\"image/x-png,image/gif,image/jpeg\">\r\n                  <a class=\"common-button blue-upload fileupload\" (click)=\"inputFileVarian.click()\">+ Add Image</a>\r\n                </td>\r\n                <td *ngIf=\"isLoading[i]\">\r\n                  <div class=\"form-group uploaded\">\r\n                    <div class=\"loading\">\r\n                      <div class=\"sk-fading-circle\">\r\n                        <div class=\"sk-circle1 sk-circle\"></div>\r\n                        <div class=\"sk-circle2 sk-circle\"></div>\r\n                        <div class=\"sk-circle3 sk-circle\"></div>\r\n                        <div class=\"sk-circle4 sk-circle\"></div>\r\n                        <div class=\"sk-circle5 sk-circle\"></div>\r\n                        <div class=\"sk-circle6 sk-circle\"></div>\r\n                        <div class=\"sk-circle7 sk-circle\"></div>\r\n                        <div class=\"sk-circle8 sk-circle\"></div>\r\n                        <div class=\"sk-circle9 sk-circle\"></div>\r\n                        <div class=\"sk-circle10 sk-circle\"></div>\r\n                        <div class=\"sk-circle11 sk-circle\"></div>\r\n                        <div class=\"sk-circle12 sk-circle\"></div>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </td>\r\n                \r\n                <td *ngIf=\"_productSku.image_gallery.length > 0 && !isLoading[i]\" class=\"varian-image-parent\">\r\n                  <img src=\"{{_productSku.image_gallery[0].base_url + _productSku.image_gallery[0].pic_file_name}}\" style=\"width: 200px\" />\r\n                  <i class=\"fas fa-times-circle delete-varian-image\" (click) = \"deleteVarianImage(_productSku.image_gallery)\"></i>\r\n                </td>\r\n                <td>\r\n                  <select class=\"form-control status-form\" [(ngModel)]=\"_productSku.status\">\r\n                    <option value=\"ACTIVE\">Active</option>\r\n                    <option value=\"INACTIVE\">Inactive</option>\r\n                  </select>\r\n                </td>\r\n              </tr>\r\n            </tbody>\r\n          </table>\r\n        </div>\r\n      </div> -->\r\n\r\n      \r\n      <div class=\"col-md-12\" style=\"display:flex\" *ngIf=\"varianX.active\">\r\n        <div class=\"col-md-6\">\r\n          <div class=\"col-md-12\" class=\"varian-parent\">\r\n            <form-input  type=\"text\" placeholder=\"Masukkan nama variasi, contoh : warna, dll\" [(ngModel)]=\"varianX.name\" [alert]=\"fieldVarianX\" [onKeyup]=\"eventValidation\"></form-input>\r\n            <i class=\"fas fa-trash\" id=\"tooltip\" (click) = deleteAllTypeX()></i>\r\n          </div>\r\n        </div>\r\n        \r\n        <!-- Using ng-option and for loop-->\r\n        <div class=\"col-md-6\">\r\n          <div class=\"col-md-12\" *ngFor=\"let item of [].constructor(varianTypeX.length); index as iox\" class=\"varian-parent\">\r\n            <form-input type=\"text\" placeholder=\"Masukkan pilihan variasi, contoh : merah, dll\" [(ngModel)]=\"varianTypeX[iox]\" [alert]=\"fieldVarianTypeX[iox]\" [onKeyup]=\"eventValidation\"></form-input>\r\n            <i *ngIf=\"varianTypeX.length > 1\" class=\"fas fa-trash\" id=\"tooltip\" (click) = deleteVarianTypeX(iox)></i>\r\n          </div>\r\n\r\n\r\n          <button type=\"button\" (click)=\"addVarianTypeX()\" class=\"btn add-value\">Add Value</button>\r\n        </div>\r\n      </div>\r\n\r\n      <ng-container>\r\n        <div class=\"col-md-12\" *ngIf=\"!varianY.active\">\r\n          <button type=\"button\" (click)=\"activatedVarianY()\" class=\"btn\">Add Varian</button>\r\n        </div>\r\n      </ng-container>\r\n\r\n      <div class=\"col-md-12\" style=\"display:flex\" *ngIf=\"varianY.active\">\r\n        <div class=\"col-md-6\">\r\n          <div class=\"col-md-12\" class=\"varian-parent\">\r\n            <form-input  type=\"text\" placeholder=\"Masukkan nama variasi, contoh : warna, dll\" [(ngModel)]=\"varianY.name\" [alert]=\"fieldVarianY\" [onKeyup]=\"eventValidation\"></form-input>\r\n            <i class=\"fas fa-trash\" id=\"tooltip\" (click) = deleteAllTypeY()></i>\r\n          </div>\r\n        </div>\r\n        \r\n        <!-- Using ng-option and for loop-->\r\n        <div class=\"col-md-6\">\r\n          <div class=\"col-md-12\" *ngFor=\"let item of [].constructor(varianTypeY.length); index as ioy\" class=\"varian-parent\">\r\n            <form-input type=\"text\" placeholder=\"Masukkan pilihan variasi, contoh : merah, dll\" [(ngModel)]=\"varianTypeY[ioy]\" [alert]=\"fieldVarianTypeY[ioy]\" [onKeyup]=\"eventValidation\"></form-input>\r\n            <i *ngIf=\"varianTypeY.length > 1\" class=\"fas fa-trash\" id=\"tooltip\" (click) = deleteVarianTypeY(ioy)></i>\r\n          </div>\r\n\r\n\r\n          <button type=\"button\" (click)=\"addVarianTypeY()\" class=\"btn add-value\">Add Value</button>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 varian-table\">\r\n        <div class=\"form-group\">\r\n          <table class=\"table table-striped\">\r\n            <thead>\r\n              <tr>\r\n                <th>\r\n                  {{varianY.name}}\r\n                </th>\r\n                <th>\r\n                  {{varianX.name}}\r\n                </th>\r\n                <th *ngFor=\"let _headerValue of headerValue\">{{_headerValue}}</th>\r\n              </tr>\r\n            </thead>\r\n            <tbody>\r\n              <ng-container *ngFor=\"let varianY of multiVarian; index as ioy\">\r\n              <tr *ngFor=\"let varianYX of varianY; index as iox\">\r\n                <td>\r\n                  {{varianTypeY[ioy]}}\r\n                </td>\r\n                <td>\r\n                  {{varianTypeX[iox]}}\r\n                </td>\r\n                <td>\r\n                  <input class=\"price form-control\" [(ngModel)]=\"multiVarian[ioy][iox].sku_code\" />\r\n                </td>\r\n                <td>\r\n                  <input type=\"number\" class=\"price form-control\" [(ngModel)]=\"multiVarian[ioy][iox].sku_price\" />\r\n                </td>\r\n                <td>\r\n                  <input type=\"number\" class=\"price form-control\" [(ngModel)]=\"multiVarian[ioy][iox].sku_value\" />\r\n                </td>\r\n                <td>\r\n                  <input type=\"number\" class=\"price form-control\" [(ngModel)]=\"multiVarian[ioy][iox].qty\" />\r\n                </td>\r\n                <td *ngIf=\"multiVarian[ioy][iox].image_gallery.length == 0 && !multiVarian[ioy][iox].isloading\">\r\n                  <input #inputFileVarian type=\"file\" style=\"display:none\" (change)=\"onFileVariant($event, multiVarian[ioy][iox].image_gallery, [ioy,iox])\" accept=\"image/x-png,image/gif,image/jpeg\">\r\n                  <a class=\"common-button blue-upload fileupload\" (click)=\"inputFileVarian.click()\">+ Add Image</a>\r\n                </td>\r\n                <td *ngIf=\"multiVarian[ioy][iox].isloading\">\r\n                  <div class=\"form-group uploaded\">\r\n                    <div class=\"loading\">\r\n                      <div class=\"sk-fading-circle\">\r\n                        <div class=\"sk-circle1 sk-circle\"></div>\r\n                        <div class=\"sk-circle2 sk-circle\"></div>\r\n                        <div class=\"sk-circle3 sk-circle\"></div>\r\n                        <div class=\"sk-circle4 sk-circle\"></div>\r\n                        <div class=\"sk-circle5 sk-circle\"></div>\r\n                        <div class=\"sk-circle6 sk-circle\"></div>\r\n                        <div class=\"sk-circle7 sk-circle\"></div>\r\n                        <div class=\"sk-circle8 sk-circle\"></div>\r\n                        <div class=\"sk-circle9 sk-circle\"></div>\r\n                        <div class=\"sk-circle10 sk-circle\"></div>\r\n                        <div class=\"sk-circle11 sk-circle\"></div>\r\n                        <div class=\"sk-circle12 sk-circle\"></div>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </td>\r\n                \r\n                <td *ngIf=\"multiVarian[ioy][iox].image_gallery.length > 0 && !multiVarian[ioy][iox].isloading\" class=\"varian-image-parent\">\r\n                  <img src=\"{{multiVarian[ioy][iox].image_gallery[0].base_url + multiVarian[ioy][iox].image_gallery[0].pic_file_name}}\" style=\"width: 200px\" />\r\n                  <i class=\"fas fa-times-circle delete-varian-image\" (click) = \"deleteVarianImage(multiVarian[ioy][iox].image_gallery)\"></i>\r\n                </td>\r\n                <td>\r\n                  <select class=\"form-control status-form\" [(ngModel)]=\"multiVarian[ioy][iox].status\">\r\n                    <option value=\"ACTIVE\">Active</option>\r\n                    <option value=\"INACTIVE\">Inactive</option>\r\n                  </select>\r\n                </td>\r\n              </tr>\r\n            </ng-container>\r\n            </tbody>\r\n          </table>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div *ngIf=\"varianAlert\" class=\"red-error-alert\">{{varianAlertMessage}}</div>\r\n    <!--End Of Varian-->\r\n\r\n    <div class=\"upload-container col-md-12 varian-button\" *ngIf=\"form.type=='product' && multiVarian.length == 0\" (click)=\"clickVarian()\">\r\n      <i class=\"fas fa-plus-circle\"></i> Aktifkan Varian\r\n    </div>\r\n\r\n    <!-- Product Price-->\r\n    <div class=\"upload-container col-md-12\" *ngIf=\"multiVarian.length == 0\" id=\"price\">\r\n      <div class=\"content-title-container\">\r\n        <h3 class=\"content-title\">Price</h3>\r\n      </div>\r\n      <div class=\"row\">\r\n        <div class=\"col-md-6\">\r\n          <div class=\"form-group\">\r\n            <div class=\"label\">\r\n              <label for=\"exampleInputEmail1\">Product Price</label>\r\n            </div>\r\n            <div class=\"price-column\">\r\n              <p>Rp</p>\r\n              <div>\r\n                <input class=\"price form-control\" (keyup)=\"formPriceReplacer($event, 'product_price')\" [(ngModel)]=\"form.product_price\" [ngClass]=\"productPriceAlert? 'red-alert' : ''\"/>\r\n                <div *ngIf=\"productPriceAlert\" class=\"red-error-alert\">*product price tidak boleh nol</div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-6\">\r\n          <div class=\"form-group\">\r\n            <div class=\"label\">\r\n              <label for=\"exampleInputEmail1\">Product Value</label>\r\n            </div>\r\n            <div class=\"price-column\">\r\n              <div>\r\n                <input class=\"price form-control\" (keyup)=\"formPriceReplacer($event, 'product_value')\" [(ngModel)]=\"form.product_value\" [ngClass]=\"productValueAlert? 'red-alert' : ''\"/>\r\n                <div *ngIf=\"productValueAlert\" class=\"red-error-alert\">*product value tidak boleh nol</div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-6\" *ngIf=\"form.type=='product' || form.type=='voucher' || form.type=='gold' || form.type == 'top-up'\">\r\n          <div class=\"label\">\r\n            <label for=\"exampleInputEmail1\">Stock</label>\r\n          </div>\r\n          <input class=\"qty form-control\" type=\"number\" [(ngModel)]=\"form.qty\" />\r\n        </div>\r\n        <!-- <div class=\"col-md-6\">\r\n          <div class=\"form-group\">\r\n            <div class=\"label\">\r\n              <label for=\"fixed_price\">Fixed price (after discount)</label>\r\n            </div>\r\n            <div class=\"price-column\">\r\n              <p>Rp</p>\r\n              <input class=\"fixed_price form-control\" name=\"fixed_price\"\r\n                (keyup)=\"formPriceReplacer($event, 'fixed_price')\" [(ngModel)]=\"form.fixed_price\" />\r\n            </div>\r\n          </div>\r\n        </div> -->\r\n        <!-- <div class=\"col-md-6\">\r\n          <div class=\"form-group\">\r\n            <div class=\"label\">\r\n              <label for=\"exampleInputEmail1\">Minimum Order</label>\r\n            </div>\r\n            <input class=\"min_order form-control\" type=\"number\" [(ngModel)]=\"form.min_order\" />\r\n          </div>\r\n        </div> -->\r\n      </div>\r\n    </div>\r\n\r\n    <!--End Of Product Price-->\r\n\r\n    <!--Product Management -->\r\n    <div class=\"upload-container col-md-12\">\r\n\r\n      <div class=\"row\">\r\n        <div class=\"col-md-6 \">\r\n          <div class=\"content-title-container\">\r\n            <h3 class=\"content-title\">{{title}} Management</h3>\r\n          </div>\r\n          <div class=\"form-group\">\r\n            <div class=\"label\">\r\n              <label>Status</label>\r\n              <form-select [(ngModel)]=\"form.status\" [data]=\"productStatus\"></form-select>\r\n              <label style=\"color:#8f8f8f;font-size: 12px;\">If the status is <span style=\"color:blue\">publish</span>, your {{title}} can be\r\n                purchased</label>\r\n            </div>\r\n            <!-- <select class=\"form-control status-form\" [(ngModel)]=\"form.active\">\r\n              <option [ngValue]=\"INACTIVE\" class=\"false\">Don't publish yet</option>\r\n              <option [ngValue]=\"ACTIVE\">Publish</option>\r\n            </select> -->\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-6 custom-price-column\" *ngIf=\"form.type == 'product' || form.type == 'voucher' || form.type == 'gold'\" id=\"weight\">\r\n          <div class=\"content-title-container\">\r\n            <h3 class=\"content-title\">Weight & Dimension</h3>\r\n          </div>\r\n          <div>\r\n            <div class=\"form-group\">\r\n                <div class=\"label\">\r\n                  <label for=\"exampleInputEmail1\">Weight</label>\r\n                </div>\r\n                <div class=\"col-md-8\">\r\n                  <div class=\"input-group\">\r\n                    <input class=\"form-control dimension\" type=\"text\" (keyup)=\"formPriceReplacer($event, 'weightInGram')\"\r\n                      [(ngModel)]=\"form.weightInGram\" aria-label=\"weightInGram\" aria-describedby=\"basic-addon1\" [ngClass]=\"weightAlert? 'red-alert' : ''\">\r\n                    <p style=\"margin: 5px 10px 5px 0px;\">gram</p>\r\n                  </div>  \r\n                  <div *ngIf=\"weightAlert\" class=\"red-error-alert\">*weight tidak boleh nol</div> \r\n                </div> \r\n            </div>\r\n          </div>\r\n          <div>\r\n            <div class=\"form-group\">\r\n                <div class=\"label\">\r\n                  <label for=\"exampleInputEmail1\">Width</label>\r\n                </div>\r\n                <div class=\"col-md-8\">\r\n                  <div class=\"input-group\">\r\n                    <input class=\"form-control dimension\" type=\"number\" [(ngModel)]=\"form.dimensions.width\"\r\n                    aria-label=\"Username\" aria-describedby=\"basic-addon1\" [ngClass]=\"widthAlert? 'red-alert' : ''\">\r\n                    <p style=\"margin: 5px 10px 5px 0px;\">cm</p>\r\n                  </div>\r\n                  <div *ngIf=\"widthAlert\" class=\"red-error-alert\">*width value tidak boleh nol</div> \r\n                </div>\r\n            </div>\r\n          </div>\r\n          <div>\r\n            <div class=\"form-group\">\r\n                <div class=\"label\">\r\n                  <label for=\"exampleInputEmail1\">Length</label>\r\n                </div>\r\n                <div class=\"col-md-8\">\r\n                  <div class=\"input-group\">\r\n                    <input class=\"form-control dimension\" type=\"number\" [(ngModel)]=\"form.dimensions.length\"\r\n                    aria-label=\"Username\" aria-describedby=\"basic-addon1\" [ngClass]=\"lengthAlert? 'red-alert' : ''\">\r\n                    <p style=\"margin: 5px 10px 5px 0px;\">cm</p>\r\n                  </div>\r\n                  <div *ngIf=\"lengthAlert\" class=\"red-error-alert\">*length value tidak boleh nol</div> \r\n                </div>\r\n            </div>\r\n          </div>\r\n          <div>\r\n            <div class=\"form-group\">\r\n                <div class=\"label\">\r\n                  <label for=\"exampleInputEmail1\">Height</label>\r\n                </div>\r\n                <div class=\"col-md-8\">\r\n                  <div class=\"input-group\">\r\n                    <input class=\"form-control dimension\" type=\"number\" [(ngModel)]=\"form.dimensions.height\"\r\n                    aria-label=\"Username\" aria-describedby=\"basic-addon1\" [ngClass]=\"heightAlert? 'red-alert' : ''\">\r\n                    <p style=\"margin: 5px 10px 5px 0px;\">cm</p>\r\n                  </div>\r\n                  <div *ngIf=\"heightAlert\" class=\"red-error-alert\">*height value tidak boleh nol</div> \r\n                </div>    \r\n            </div>\r\n          </div>\r\n          <!-- <div class=\"input-group mb-3\">\r\n            \r\n              <div class=\"col-md-4 custom-non-border\">\r\n                 Width\r\n                <div class=\"price-column\">\r\n                  <p>Width</p>\r\n                  <input class=\"form-control dimension\" type=\"number\" [(ngModel)]=\"form.dimensions.width\"\r\n                    aria-label=\"Username\" aria-describedby=\"basic-addon1\">\r\n                  <p>cm</p>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-md-4 custom-border\">\r\n                 Length\r\n                <div class=\"price-column\">\r\n                  <p>Length</p>\r\n                  <input class=\"form-control dimension\" type=\"number\" [(ngModel)]=\"form.dimensions.length\"\r\n                    aria-label=\"Username\" aria-describedby=\"basic-addon1\">\r\n                  <p>cm</p>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-md-4 custom-non-border\">\r\n                Height \r\n                <div class=\"price-column\">\r\n                  <p>Height</p>\r\n                  <input class=\"form-control dimension\" type=\"number\" [(ngModel)]=\"form.dimensions.height\"\r\n                    aria-label=\"Username\" aria-describedby=\"basic-addon1\">\r\n                  <p >cm</p>\r\n                </div>\r\n              </div>\r\n            \r\n          </div> -->\r\n        </div>\r\n\r\n      </div>\r\n\r\n\r\n    </div>\r\n    <!-- End Of Product Management -->\r\n    <!-- Some Additional Stuff -->\r\n    <!-- <div class=\"col-md-12\">\r\n      <div class=\"form-group\" *ngIf=\"form.type=='evoucher'\">\r\n        <label>Need Outlet Code?</label>\r\n        <div>\r\n          <select class=\"form-control status-form\" [(ngModel)]=\"form.need_outlet_code\">\r\n            <option [ngValue]=\"0\" class=\"false\">No</option>\r\n            <option [ngValue]=\"1\">Yes</option>\r\n          </select>\r\n        </div>\r\n      </div>\r\n    </div> -->\r\n    <!-- <div class=\"col-md-12\">\r\n      <div class=\"form-group\" *ngIf=\"form.type=='evoucher'\">\r\n        <label>Applicable For IOS?</label>\r\n        <div>\r\n          <select class=\"form-control status-form\" [(ngModel)]=\"form.applicable_for_ios\">\r\n            <option [ngValue]=\"false\" class=\"false\">No</option>\r\n            <option [ngValue]=\"true\">Yes</option>\r\n          </select>\r\n        </div>\r\n      </div>\r\n    </div> -->\r\n\r\n    <!-- Some Additional Stuff Ends Here -->\r\n    <input #fileinput type=\"file\" (change)=\"onFileSelected($event)\" [(ngModel)]=\"selFile\"\r\n      accept=\"image/png, image/jpeg\">\r\n    <br>\r\n  </div>\r\n  <div class=\"row end-footer\" *ngIf=\"!errorMessage\">\r\n    <div class=\"footer-save-cancel\">\r\n      <button class=\"btn cancel\">Cancel</button>\r\n      <button class=\"btn save btn-primary\" (click)=\"saveThis()\">Save</button>\r\n    </div>\r\n  </div>\r\n\r\n  <div *ngIf=\"errorMessage\" class=\"error-message\">\r\n      <div class=\"text-message\">\r\n          <i class=\"fas fa-ban\"></i>\r\n          {{errorMessage}}\r\n      </div>\r\n  </div>\r\n</div>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<!-- <div class=\"form\">\r\n  <div class=\"error\" *ngIf=\"errorLabel\">{{errorLabel}}</div>\r\n\r\n  <div class=\"col-md-6\">\r\n    <div class=\"form-group\">\r\n      <label for=\"member_id\">Member ID</label>\r\n      <input (input)=\"'text'\" class=\"_id form-control\" [(ngModel)]=\"detail._id\" disabled>\r\n    </div>\r\n\r\n    <div class=\"form-group\">\r\n      <label>Full Name</label>\r\n      <input type=\"'text'\" class=\"full_name form-control\" [(ngModel)]=\"full_name\">\r\n    </div>\r\n\r\n    <div class=\"form-group\">\r\n      <label>Member Status</label>\r\n      <input class=\"form-control\" [(ngModel)]=\"member_status\">\r\n    </div>\r\n\r\n    <div class=\"form-group\">\r\n      <label>Address</label>\r\n      <input class=\"address form-control\" [(ngModel)]=\"maddress\">\r\n    </div>\r\n\r\n    <div class=\"form-group\">\r\n      <label>City</label>\r\n      <input class=\"form-control\" [(ngModel)]=\"mcity\">\r\n    </div>\r\n\r\n    <div class=\"form-group\">\r\n      <label>Postcode</label>\r\n      <input class=\"form-control\" [(ngModel)]=\"mpostcode\">\r\n    </div>\r\n\r\n    <div class=\"form-group\">\r\n      <label>State</label>\r\n      <input class=\"form-control\" [(ngModel)]=\"mstate\">\r\n    </div>\r\n\r\n    <div class=\"form-group\">\r\n      <label>Country</label>\r\n      <input class=\"form-control\" [(ngModel)]=\"mcountry\">\r\n    </div>\r\n\r\n    <div class=\"form-group\">\r\n\r\n  </div>\r\n</div> -->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/separated-modules/merchant-products/products/merchant-detail-product/merchant-detail-product.component.html":
/*!******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/merchant-portal/separated-modules/merchant-products/products/merchant-detail-product/merchant-detail-product.component.html ***!
  \******************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<input #fileinput type=\"file\" (change)=\"onFileSelected($event)\" [(ngModel)]=\"selFile\" accept=\"image/png, image/jpeg\"\r\n    style=\"display: none;\">\r\n<div class=\"form\">\r\n    <div class=\"error\" *ngIf=\"errorLabel\">{{errorLabel}}</div>\r\n    <!-- <div class=\"new\" *ngIf=\"newData\">New products!</div> -->\r\n\r\n    \r\n    <div class=\"centered\"></div>\r\n    <div class=\"col-md-12\">\r\n        <div class=\"back\">\r\n            <!-- <button class=\"back-button\" style=\"background-color: transparent;\" (click)=\"back()\">Back</button> -->\r\n            <i class=\"fas fa-chevron-left\">&nbsp;&nbsp;<a (click)=\"back()\" style=\"cursor: pointer; font-size:20px;\">Back</a></i>\r\n        </div><br/>\r\n\r\n        <div class=\"form-group\" *ngIf=\"currentPermission=='admin' && form.merchant_username\">\r\n            <label for=\"exampleInputEmail1\">Merchant Owner : <strong>{{form.merchant_username}}</strong></label>\r\n        </div>\r\n        <div class=\"card\">\r\n            <div class=\"card-header\">\r\n                <label>Product Detail</label>\r\n                <button class=\"btn btn-primary edit float-right\" (click)=\"editThis()\"\r\n                    style=\"margin-right: 10px; margin-top: 15px;border-radius:5px;\"><i class=\"fas fa-edit\"></i> Edit\r\n                </button>\r\n            </div>\r\n            <div class=\"row\">\r\n            <div class=\"col-md-3\">\r\n                <div class=\"product-image\">\r\n                    <div class=\"form-group uploaded\" *ngIf=\"form.base_url\">\r\n                        <div class=\"uploaded-image\">\r\n                            <div class=\"img\"\r\n                                [ngStyle]=\"{'background-image':'url('+(form.base_url+form.pic_medium_path)+')'}\">\r\n                                <div class=\"editor-tool\">\r\n\r\n                                    <!-- <i class=\"fa fa-pen\"></i> -->\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                </div>\r\n            <div class=\"col-md-9\">\r\n            <div class=\"detail\">\r\n                <div class=\"form-group\">\r\n                    <label for=\"exampleInputEmail1\">Product Name : </label><br>\r\n                    <label><strong>{{form.product_name}}</strong></label>\r\n\r\n\r\n                </div>\r\n                <div class=\"form-group\">\r\n                    <label for=\"exampleInputEmail1\">SKU / Product Code : </label> <br>\r\n\r\n                    <label><strong>{{form.product_code}}</strong></label>\r\n\r\n                </div>\r\n\r\n                <div class=\"form-group\" *ngIf=\"form.category != undefined\">\r\n                    <label for=\"exampleInputEmail1\">Category </label> <br>\r\n\r\n                    <label><strong>{{form.category}}</strong></label>\r\n\r\n                </div>\r\n\r\n                <div *ngIf=\"form.type == 'evoucher'\">\r\n                    <div class=\"form-group\">\r\n                        <label>Redeem Type</label><br />\r\n                        <label><strong>{{form.redeem_type}}</strong></label>\r\n\r\n                    </div>\r\n                </div>\r\n\r\n                <table>\r\n                    <tr style=\"font-weight: 500;color:#757474\">\r\n                        <th>Price</th>\r\n                        <th>Public Price (fixed price)</th>\r\n                        <th>Quantity</th>\r\n                    </tr>\r\n                    <tr style=\"font-weight: bold;color:#757474\">\r\n                        <td>{{form.price}}</td>\r\n                        <td>{{form.fixed_price}}</td>\r\n                        <td>{{form.qty}}</td>\r\n                    </tr>\r\n                </table>\r\n                <br/>\r\n\r\n                <!-- <div class=\"form-group\">\r\n                    <label for=\"exampleInputEmail1\">Price : </label> <br />\r\n                    <label><strong>{{form.price}}</strong></label>\r\n\r\n                </div>\r\n\r\n                <div class=\"form-group\">\r\n                    <label for=\"fixed_price\">Public price (fixed price) : </label> <br />\r\n                    <label><strong>{{form.fixed_price}}</strong></label>\r\n                </div> -->\r\n\r\n                <!-- <div class=\"form-group\" *ngIf=\"form.type == 'product'\">\r\n                    <label for=\"exampleInputEmail1\">Quantity : </label> <br />\r\n                    <label><strong>{{form.qty}}</strong></label> -->\r\n                    <!-- <input class=\"qty form-control\" type=\"number\" [(ngModel)]=\"form.qty\" /> -->\r\n                <!-- </div> -->\r\n\r\n                <div class=\"form-group\">\r\n                    <label for=\"exampleInputEmail1\">Description</label><br />\r\n                    <label><strong>{{form.description}}</strong></label>\r\n\r\n                </div>\r\n\r\n                <div class=\"form-group\" *ngIf=\"form.type == 'evoucher'\">\r\n                    <label for=\"exampleInputEmail1\">Terms &amp; Conditions</label><br />\r\n                    <label><strong>{{form.tnc}}</strong></label>\r\n\r\n                </div>\r\n\r\n\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n</div>\r\n</div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/separated-modules/merchant-products/products/merchant-edit-product/merchant-edit-product.component.html":
/*!**************************************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/merchant-portal/separated-modules/merchant-products/products/merchant-edit-product/merchant-edit-product.component.html ***!
  \**************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<input #fileinput type=\"file\" (change)=\"onImageSelected($event)\" [(ngModel)]=\"selFile\"\r\naccept=\"image/png, image/jpeg\" style=\"display: none;\">\r\n<div class=\"row\">\r\n  <button class=\"btn back_button\" (click)=\"backClicked()\"><i class=\"fa fa-fw fa-angle-left\"></i> back</button>\r\n</div>\r\n<div class=\"form\" *ngIf=\"!errorMessage\">\r\n  <div class=\"error\" *ngIf=\"errorLabel\">{{errorLabel}}</div>\r\n  <input #inputFile4 type=\"file\" style=\"display:none\" (change)=\"onFileSelected2($event, 'image2')\"\r\n    accept=\"image/x-png,image/gif,image/jpeg\">\r\n  <input #inputFile3 type=\"file\" style=\"display:none\" (change)=\"onFileSelected2($event, 'image1')\"\r\n    accept=\"image/x-png,image/gif,image/jpeg\">\r\n  <input #inputFile1 type=\"file\" style=\"display:none\" (change)=\"onFileSelected2($event, 'image3')\"\r\n    accept=\"image/x-png,image/gif,image/jpeg\">\r\n  <input #inputFile2 type=\"file\" style=\"display:none\" (change)=\"onFileSelected2($event, 'image4')\"\r\n    accept=\"image/x-png,image/gif,image/jpeg\">\r\n  <input #inputFile5 type=\"file\" style=\"display:none\" (change)=\"onFileSelected2($event, 'image5')\"\r\n    accept=\"image/x-png,image/gif,image/jpeg\">\r\n\r\n  <!-- <div class=\"new\" *ngIf=\"newData\">New products!</div> -->\r\n  <div class=\"multiple-image-upload\" [ngClass]=\"photoAlert? 'red-alert' : ''\" id=\"photo-alert\">\r\n    <div class=\"upload-container\">\r\n      <div class=\"content-title-container\">\r\n        <h3 class=\"content-title\">Upload {{title}}</h3>\r\n        <label>File size max. 5 MB and file format must be .JPG/.JPEG/.PNG</label>\r\n      </div>\r\n      <div class=\"section-upload row left-section-upload\">\r\n        <div class=\"col-md-2dot4 col-half-offset\">\r\n          <div class=\"card cl-list\">\r\n            <div class=\"card-content\" *ngIf=\"!uploads.image3 && !showLoading3\">\r\n              <div class=\"image\">\r\n                <div class=\"card-content custom\">\r\n                  <div class=\"image-canvas\">\r\n                    <img *ngIf=\"!uploads.image3\" class=\"logo-upload\" src=\"/assets/images/image-solid.svg\">\r\n                  </div>\r\n                  <!-- <p class=\"upload-desc\">Upload your image here <br> or</p> -->\r\n                  <div class=\"browse-files\">\r\n                    <div class=\"pb-framer\">\r\n                      <div class=\"progressbar {{cancel?'terminated':''}}\" [ngStyle]=\"{'width.%': progressBar}\">\r\n  \r\n                      </div>\r\n                      <span *ngIf=\"progressBar<58&&progressBar>0\">\r\n                        {{cancel?'terminated':progressBar+'%'}} </span>\r\n                      <span class=\"clr-white\" *ngIf=\"progressBar>58\">\r\n                        {{cancel?'terminated':progressBar+'%'}} </span>\r\n                    </div>\r\n                    <a class=\"common-button blue-upload fileupload\" (click)=\"inputFile1.click()\">+ Add Image</a>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div *ngIf=\"errorLabel!==false\">\r\n                {{errorLabel}}\r\n              </div>\r\n            </div>\r\n            <div class=\"form-group uploaded\" *ngIf=\"showLoading3\">\r\n              <div class=\"loading\">\r\n                <div class=\"sk-fading-circle\">\r\n                  <div class=\"sk-circle1 sk-circle\"></div>\r\n                  <div class=\"sk-circle2 sk-circle\"></div>\r\n                  <div class=\"sk-circle3 sk-circle\"></div>\r\n                  <div class=\"sk-circle4 sk-circle\"></div>\r\n                  <div class=\"sk-circle5 sk-circle\"></div>\r\n                  <div class=\"sk-circle6 sk-circle\"></div>\r\n                  <div class=\"sk-circle7 sk-circle\"></div>\r\n                  <div class=\"sk-circle8 sk-circle\"></div>\r\n                  <div class=\"sk-circle9 sk-circle\"></div>\r\n                  <div class=\"sk-circle10 sk-circle\"></div>\r\n                  <div class=\"sk-circle11 sk-circle\"></div>\r\n                  <div class=\"sk-circle12 sk-circle\"></div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"form-group uploaded\" *ngIf=\"uploads.image3 && !showLoading3\">\r\n              <!-- <i class=\"fa fa-times-circle remove-icon\" (click)=\"deleteImage('image3')\"></i> -->\r\n              <div class=\"uploaded-image2\">\r\n                <div class=\"img\">\r\n                  <img class=\"resize\" src=\"{{uploads.image3}}\">\r\n                  <a class=\"common-button blue-upload fileupload\" (click)=\"inputFile1.click()\">Re-Upload</a>\r\n                </div>\r\n  \r\n              </div>\r\n  \r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-2dot4\" *ngIf=\"form.type=='product'|| form.type=='voucher' || form.type=='gold'\">\r\n  \r\n          <div class=\"card cl-list\">\r\n            <div class=\"card-content\" *ngIf=\"!uploads.image1 && !showLoading\">\r\n              <div class=\"image\">\r\n                <div class=\"card-content custom\">\r\n                  <div class=\"image-canvas\">\r\n  \r\n                    <img *ngIf=\"!uploads.image1\" class=\"logo-upload\" src=\"/assets/images/image-solid.svg\">\r\n                  </div>\r\n                  <!-- <p class=\"upload-desc\">Upload your image here <br> or</p> -->\r\n                  <div class=\"browse-files\">\r\n                    <div class=\"pb-framer\">\r\n                      <div class=\"progressbar {{cancel?'terminated':''}}\" [ngStyle]=\"{'width.%': progressBar}\">\r\n  \r\n                      </div>\r\n                      <span *ngIf=\"progressBar<58&&progressBar>0\">\r\n                        {{cancel?'terminated':progressBar+'%'}} </span>\r\n                      <span class=\"clr-white\" *ngIf=\"progressBar>58\">\r\n                        {{cancel?'terminated':progressBar+'%'}} </span>\r\n  \r\n                    </div>\r\n  \r\n                    <a class=\"common-button blue-upload fileupload\" (click)=\"inputFile3.click()\">+ Add Image</a>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div *ngIf=\"errorLabel!==false\">\r\n                {{errorLabel}}\r\n              </div>\r\n            </div>\r\n            <div class=\"form-group uploaded\" *ngIf=\"showLoading\">\r\n              <div class=\"loading\">\r\n                <div class=\"sk-fading-circle\">\r\n                  <div class=\"sk-circle1 sk-circle\"></div>\r\n                  <div class=\"sk-circle2 sk-circle\"></div>\r\n                  <div class=\"sk-circle3 sk-circle\"></div>\r\n                  <div class=\"sk-circle4 sk-circle\"></div>\r\n                  <div class=\"sk-circle5 sk-circle\"></div>\r\n                  <div class=\"sk-circle6 sk-circle\"></div>\r\n                  <div class=\"sk-circle7 sk-circle\"></div>\r\n                  <div class=\"sk-circle8 sk-circle\"></div>\r\n                  <div class=\"sk-circle9 sk-circle\"></div>\r\n                  <div class=\"sk-circle10 sk-circle\"></div>\r\n                  <div class=\"sk-circle11 sk-circle\"></div>\r\n                  <div class=\"sk-circle12 sk-circle\"></div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"form-group uploaded\" *ngIf=\"uploads.image1 && !showLoading\">\r\n              <i class=\"fa fa-times-circle remove-icon\"  (click)=\"deleteImage('image1')\"></i>\r\n              <div class=\"uploaded-image2\">\r\n                <div class=\"img\">\r\n                  <img class=\"resize\" src=\"{{uploads.image1}}\">\r\n                  <a class=\"common-button blue-upload fileupload\" (click)=\"inputFile3.click()\">Re-Upload</a>\r\n                </div>\r\n  \r\n              </div>\r\n  \r\n            </div>\r\n          </div>\r\n  \r\n        </div>\r\n        <div class=\"col-md-2dot4 col-half-offset\" *ngIf=\"form.type=='product'|| form.type=='voucher' || form.type=='gold'\">\r\n  \r\n          <div class=\"card cl-list\">\r\n            <div class=\"card-content\" *ngIf=\"!uploads.image2 && !showLoading2\">\r\n              <div class=\"image\">\r\n                <div class=\"card-content custom\">\r\n                  <div class=\"image-canvas\">\r\n  \r\n                    <img *ngIf=\"!uploads.image2\" class=\"logo-upload\" src=\"/assets/images/image-solid.svg\">\r\n                  </div>\r\n                  <!-- <p class=\"upload-desc\">Upload your image here <br> or</p> -->\r\n                  <div class=\"browse-files\">\r\n                    <div class=\"pb-framer\">\r\n                      <div class=\"progressbar {{cancel?'terminated':''}}\" [ngStyle]=\"{'width.%': progressBar}\">\r\n  \r\n                      </div>\r\n                      <span *ngIf=\"progressBar<58&&progressBar>0\">\r\n                        {{cancel?'terminated':progressBar+'%'}} </span>\r\n                      <span class=\"clr-white\" *ngIf=\"progressBar>58\">\r\n                        {{cancel?'terminated':progressBar+'%'}} </span>\r\n  \r\n                    </div>\r\n  \r\n                    <a class=\"common-button blue-upload fileupload\" (click)=\"inputFile4.click()\">+ Add Image</a>\r\n  \r\n  \r\n                  </div>\r\n  \r\n                </div>\r\n  \r\n              </div>\r\n              <div *ngIf=\"errorLabel!==false\">\r\n                {{errorLabel}}\r\n              </div>\r\n            </div>\r\n            <div class=\"form-group uploaded\" *ngIf=\"showLoading2\">\r\n              <div class=\"loading\">\r\n                <div class=\"sk-fading-circle\">\r\n                  <div class=\"sk-circle1 sk-circle\"></div>\r\n                  <div class=\"sk-circle2 sk-circle\"></div>\r\n                  <div class=\"sk-circle3 sk-circle\"></div>\r\n                  <div class=\"sk-circle4 sk-circle\"></div>\r\n                  <div class=\"sk-circle5 sk-circle\"></div>\r\n                  <div class=\"sk-circle6 sk-circle\"></div>\r\n                  <div class=\"sk-circle7 sk-circle\"></div>\r\n                  <div class=\"sk-circle8 sk-circle\"></div>\r\n                  <div class=\"sk-circle9 sk-circle\"></div>\r\n                  <div class=\"sk-circle10 sk-circle\"></div>\r\n                  <div class=\"sk-circle11 sk-circle\"></div>\r\n                  <div class=\"sk-circle12 sk-circle\"></div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"form-group uploaded\" *ngIf=\"uploads.image2 && !showLoading2\">\r\n              <i class=\"fa fa-times-circle remove-icon\"  (click)=\"deleteImage('image2')\"></i>\r\n              <div class=\"uploaded-image2\">\r\n                <div class=\"img\">\r\n                  <img class=\"resize\" src=\"{{uploads.image2}}\">\r\n                  <a class=\"common-button blue-upload fileupload\" (click)=\"inputFile4.click()\">Re-Upload</a>\r\n                </div>\r\n  \r\n              </div>\r\n  \r\n            </div>\r\n          </div>\r\n        </div>\r\n  \r\n        <div class=\"col-md-2dot4 col-half-offset\" *ngIf=\"form.type=='product'|| form.type=='voucher' || form.type=='gold'\">\r\n  \r\n          <div class=\"card cl-list\">\r\n            <div class=\"card-content\" *ngIf=\"!uploads.image4 && !showLoading4\">\r\n              <div class=\"image\">\r\n                <div class=\"card-content custom\">\r\n                  <div class=\"image-canvas\">\r\n                    <img *ngIf=\"!uploads.image4\" class=\"logo-upload\" src=\"/assets/images/image-solid.svg\">\r\n                  </div>\r\n                  <!-- <p class=\"upload-desc\">Upload your image here <br> or</p> -->\r\n                  <div class=\"browse-files\">\r\n                    <div class=\"pb-framer\">\r\n                      <div class=\"progressbar {{cancel?'terminated':''}}\" [ngStyle]=\"{'width.%': progressBar}\">\r\n  \r\n                      </div>\r\n                      <span *ngIf=\"progressBar<58&&progressBar>0\">\r\n                        {{cancel?'terminated':progressBar+'%'}} </span>\r\n                      <span class=\"clr-white\" *ngIf=\"progressBar>58\">\r\n                        {{cancel?'terminated':progressBar+'%'}} </span>\r\n                    </div>\r\n                    <a class=\"common-button blue-upload fileupload\" (click)=\"inputFile2.click()\">+ Add Image</a>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div *ngIf=\"errorLabel!==false\">\r\n                {{errorLabel}}\r\n              </div>\r\n            </div>\r\n            <div class=\"form-group uploaded\" *ngIf=\"showLoading4\">\r\n              <div class=\"loading\">\r\n                <div class=\"sk-fading-circle\">\r\n                  <div class=\"sk-circle1 sk-circle\"></div>\r\n                  <div class=\"sk-circle2 sk-circle\"></div>\r\n                  <div class=\"sk-circle3 sk-circle\"></div>\r\n                  <div class=\"sk-circle4 sk-circle\"></div>\r\n                  <div class=\"sk-circle5 sk-circle\"></div>\r\n                  <div class=\"sk-circle6 sk-circle\"></div>\r\n                  <div class=\"sk-circle7 sk-circle\"></div>\r\n                  <div class=\"sk-circle8 sk-circle\"></div>\r\n                  <div class=\"sk-circle9 sk-circle\"></div>\r\n                  <div class=\"sk-circle10 sk-circle\"></div>\r\n                  <div class=\"sk-circle11 sk-circle\"></div>\r\n                  <div class=\"sk-circle12 sk-circle\"></div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"form-group uploaded\" *ngIf=\"uploads.image4 && !showLoading4\">\r\n              <i class=\"fa fa-times-circle remove-icon\"  (click)=\"deleteImage('image4')\"></i>\r\n              <div class=\"uploaded-image2\">\r\n                <div class=\"img\">\r\n                  <img class=\"resize\" src=\"{{uploads.image4}}\">\r\n                  <a class=\"common-button blue-upload fileupload\" (click)=\"inputFile2.click()\">Re-Upload</a>\r\n                </div>\r\n  \r\n              </div>\r\n  \r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-2dot4 col-half-offset\" *ngIf=\"form.type=='product'|| form.type=='voucher' || form.type=='gold'\">\r\n  \r\n          <div class=\"card cl-list\">\r\n            <div class=\"card-content\" *ngIf=\"!uploads.image5 && !showLoading5\">\r\n              <div class=\"image\">\r\n                <div class=\"card-content custom\">\r\n                  <div class=\"image-canvas\">\r\n                    <img *ngIf=\"!uploads.image5\" class=\"logo-upload\" src=\"/assets/images/image-solid.svg\">\r\n                  </div>\r\n                  <!-- <p class=\"upload-desc\">Upload your image here <br> or</p> -->\r\n                  <div class=\"browse-files\">\r\n                    <div class=\"pb-framer\">\r\n                      <div class=\"progressbar {{cancel?'terminated':''}}\" [ngStyle]=\"{'width.%': progressBar}\">\r\n  \r\n                      </div>\r\n                      <span *ngIf=\"progressBar<58&&progressBar>0\">\r\n                        {{cancel?'terminated':progressBar+'%'}} </span>\r\n                      <span class=\"clr-white\" *ngIf=\"progressBar>58\">\r\n                        {{cancel?'terminated':progressBar+'%'}} </span>\r\n                    </div>\r\n                    <a class=\"common-button blue-upload fileupload\" (click)=\"inputFile5.click()\">+ Add Image</a>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div *ngIf=\"errorLabel!==false\">\r\n                {{errorLabel}}\r\n              </div>\r\n            </div>\r\n            <div class=\"form-group uploaded\" *ngIf=\"showLoading5\">\r\n              <div class=\"loading\">\r\n                <div class=\"sk-fading-circle\">\r\n                  <div class=\"sk-circle1 sk-circle\"></div>\r\n                  <div class=\"sk-circle2 sk-circle\"></div>\r\n                  <div class=\"sk-circle3 sk-circle\"></div>\r\n                  <div class=\"sk-circle4 sk-circle\"></div>\r\n                  <div class=\"sk-circle5 sk-circle\"></div>\r\n                  <div class=\"sk-circle6 sk-circle\"></div>\r\n                  <div class=\"sk-circle7 sk-circle\"></div>\r\n                  <div class=\"sk-circle8 sk-circle\"></div>\r\n                  <div class=\"sk-circle9 sk-circle\"></div>\r\n                  <div class=\"sk-circle10 sk-circle\"></div>\r\n                  <div class=\"sk-circle11 sk-circle\"></div>\r\n                  <div class=\"sk-circle12 sk-circle\"></div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"form-group uploaded\" *ngIf=\"uploads.image5 && !showLoading5\">\r\n              <i class=\"fa fa-times-circle remove-icon\"  (click)=\"deleteImage('image3')\"></i>\r\n              <div class=\"uploaded-image2\">\r\n                <div class=\"img\">\r\n                  <img class=\"resize\" src=\"{{uploads.image5}}\">\r\n                  <a class=\"common-button blue-upload fileupload\" (click)=\"inputFile5.click()\">Re-Upload</a>\r\n                </div>\r\n  \r\n              </div>\r\n  \r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <!-- End Of Multiple Image Upload -->\r\n  </div>\r\n  <div *ngIf=\"photoAlert\" class=\"red-error-alert\">*{{title}} harus memiliki minimum 1 gambar</div>\r\n  <div class=\"row\">\r\n    <div class=\"col-md-12\">\r\n      <div class=\"form-group\" *ngIf=\"form.type=='evoucher'\">\r\n        <label>Choose Vendor Type: </label>\r\n        <div>\r\n          <div class=\" product-type-list\">\r\n            <div *ngFor=\"let vt of listOfVendorType\" class=\"product-type {{vt.active}}\" (click)=\"setVendorType(vt.name)\">\r\n              {{vt.name}}\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div> \r\n      <!-- Redeem Type -->\r\n       <!-- <div class=\"form-group\" *ngIf=\"form.type=='evoucher'\">\r\n        <label>Choose Redeem Type: </label>\r\n        <div>\r\n          <div class=\" product-type-list\">\r\n            <div *ngFor=\"let vt of listOfRedeemType\" class=\"product-type {{vt.active}}\" (click)=\"setRedeemType(vt)\" [(ngModel)]=\"form.redeem_type\" >\r\n              {{vt.name}}\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>  -->\r\n      <!-- Begining Of Multiple Image Upload -->\r\n      \r\n      <div class=\"form-group\" *ngIf=\"currentPermission=='admin' && form.merchant_username && form.type !='evoucher'\">\r\n        <label>Choose Merchant</label>\r\n        <select class=\"form-control\" [(ngModel)]=\"form.merchant_username\">\r\n          <option *ngFor=\"let mt of listOfMerchant\" [ngValue]='mt.name'>{{mt.name}}</option>\r\n        </select>\r\n      </div>\r\n      <div class=\"form-group\" *ngIf=\"currentPermission=='admin' && form.merchant_username\">\r\n        <!-- <label for=\"exampleInputEmail1\">Merchant Owner : <strong>{{form.merchant_username}}</strong></label> -->\r\n      </div>\r\n      <!-- backup for old UI -->\r\n      <!-- <div class=\"form-group\">\r\n        <label for=\"exampleInputEmail1\">Product Name</label>\r\n        <input (input)=\"productNameInputed($event)\" class=\"product_name form-control\" [(ngModel)]=\"form.product_name\" />\r\n      </div>\r\n      <div class=\"form-group\">\r\n        <label for=\"exampleInputEmail1\">SKU / Product Code</label>\r\n        <span class=\"autoSKU\"> <input type=\"checkbox\" [(ngModel)]=\"autoSKU\" /> auto-type SKU </span>\r\n        <input class=\"product_code form-control\" [(ngModel)]=\"form.product_code\" />\r\n      </div> -->\r\n      <!-- <div class=\"form-group\">\r\n        <label for=\"type_product\">Product Type</label>\r\n        <input class=\"form-control\" [(ngModel)]=\"form.type\">\r\n      </div> -->\r\n\r\n      <!-- <div class=\"form-group\">\r\n        <label>Merchant Group</label>\r\n        <input class=\"form-control\" [(ngModel)]=\"form.merchant_group\" aria-disabled=\"true;\">\r\n      </div> -->\r\n      <!-- <div *ngIf=\"form.type == 'evoucher'\">\r\n        <div class=\"form-group\">\r\n          <label>Redeem Type</label>\r\n          <input class=\"form-control\" [(ngModel)]=\"form.redeem_type\" aria-disabled=\"true\">\r\n        </div>\r\n      </div> -->\r\n      <!-- <div class=\"form-group\">\r\n        <label for=\"exampleInputEmail1\">Hashtags</label>\r\n\r\n        <div class=\"form-control hashhash\">\r\n          <span class=\"hashtag-item\" *ngFor=\"let ht of form.hashtags\"> {{ht}}</span>\r\n          <div class=\"hashtagskey-input\" contenteditable=\"true\" [textContent]=\"hashtagsKey\"\r\n            (keydown)=\"hashtagsKeyDown($event)\"></div>\r\n        </div>\r\n      </div> -->\r\n\r\n      <!-- <div class=\"form-group\" *ngIf=\"form.type == 'product'\">\r\n        <label for=\"exampleInputEmail1\">Quantity</label>\r\n        <input class=\"qty form-control\" type=\"number\" [(ngModel)]=\"form.qty\" />\r\n      </div> -->\r\n\r\n\r\n\r\n<!-- Product Information -->\r\n<div class=\"upload-container col-md-12\" id=\"product-information\">\r\n  <div class=\"content-title-container\">\r\n    <h3 class=\"content-title\">{{title}} Information</h3>\r\n  </div>\r\n\r\n  <div class=\"row\">\r\n    <!-- PRoduct Name -->\r\n    <div class=\"col-md-6\">\r\n      <div class=\"form-group\">\r\n        <div class=\"label\">\r\n          <label for=\"exampleInputEmail1\"><span  *ngIf=\"form.type\">{{title}}</span> Name</label>\r\n        </div>\r\n        <div class=\"col-md-12\">\r\n          <input (input)=\"productNameInputed($event)\" class=\"product_name form-control\"\r\n          [(ngModel)]=\"form.product_name\" placeholder=\"Example : Asus ROG Zephyrus S15 GX502 \" [ngClass]=\"productNameAlert? 'red-alert' : ''\"/>\r\n          <div *ngIf=\"productNameAlert\" class=\"red-error-alert\">*product  name tidak boleh kosong</div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n    <!-- PRoduct Code -->\r\n    <div class=\"col-md-6\">\r\n      <div class=\"form-group\">\r\n        <div class=\"label\">\r\n          <label for=\"exampleInputEmail1\">Product Code </label>\r\n          <!-- <i class=\"fas fa-info-circle\" id=\"tooltip\"> <span id=\"tooltiptext\"> Maximum Character For {{title}} Code is\r\n              20\r\n              Characters</span></i> -->\r\n          <span class=\"autoSKU\">\r\n            <!-- <mat-checkbox [(ngModel)]=\"autoSKU\" value=\"autoSKU\">\r\n    auto-type SKU\r\n  </mat-checkbox> -->\r\n          </span>\r\n        </div>\r\n        <div class=\"col-md-12\">\r\n          <input class=\"product_code form-control\" [(ngModel)]=\"form.product_code\" [ngClass]=\"productCodeAlert? 'red-alert' : ''\" disabled=true/>\r\n          <div *ngIf=\"productCodeAlert\" class=\"red-error-alert\">*product code tidak boleh kosong</div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-6\">\r\n      <div class=\"form-group\" *ngIf=\"form.type=='e-voucher'\">\r\n        <div class=\"label\">\r\n          <label>Category Voucher</label>\r\n        </div>\r\n        <div class=\"col-md-12\">\r\n          <select class=\"form-control\" [(ngModel)]=\"form.category\" [ngClass]=\"categoryAlert? 'red-alert' : ''\">\r\n            <option *ngFor=\"let mt of categoryEvoucher\" [ngValue]='mt.code'>{{mt.name}}</option>\r\n          </select>\r\n          <div *ngIf=\"categoryAlert\" class=\"red-error-alert\">*category tidak boleh kosong</div>\r\n        </div>\r\n      </div>\r\n      <div class=\"form-group\" *ngIf=\"form.type=='product'\">\r\n        <div class=\"label\">\r\n          <label>Categories {{title}}</label>\r\n        </div>\r\n        <div class=\"col-md-12\">\r\n          <select class=\"form-control\" [(ngModel)]=\"form.category\" [ngClass]=\"categoryAlert? 'red-alert' : ''\">\r\n            <option *ngFor=\"let mt of categoryProduct\" [ngValue]='mt.code'>{{mt.name}}</option>\r\n          </select>\r\n          <div *ngIf=\"categoryAlert\" class=\"red-error-alert\">*category tidak boleh kosong</div>\r\n        </div>\r\n      </div>\r\n      <div class=\"form-group\" *ngIf=\"form.type=='voucher'\">\r\n        <div class=\"label\">\r\n          <label>Categories {{title}}</label>\r\n        </div>\r\n        <div class=\"col-md-12\">\r\n          <select class=\"form-control\" [(ngModel)]=\"form.category\" [ngClass]=\"categoryAlert? 'red-alert' : ''\">\r\n            <option *ngFor=\"let mt of categoryVoucher\" [ngValue]='mt.code'>{{mt.name}}</option>\r\n          </select>\r\n          <div *ngIf=\"categoryAlert\" class=\"red-error-alert\">*category tidak boleh kosong</div>\r\n        </div>\r\n      </div>\r\n      <div class=\"form-group\" *ngIf=\"form.type=='gold'\">\r\n        <div class=\"label\">\r\n          <label>Categories {{title}}</label>\r\n        </div>\r\n        <div class=\"col-md-12\">\r\n          <select class=\"form-control\" [(ngModel)]=\"form.category\" [ngClass]=\"categoryAlert? 'red-alert' : ''\">\r\n            <option *ngFor=\"let mt of categoryGold\" [ngValue]='mt.code'>{{mt.name}}</option>\r\n          </select>\r\n          <div *ngIf=\"categoryAlert\" class=\"red-error-alert\">*category tidak boleh kosong</div>\r\n        </div>\r\n      </div>\r\n      <div class=\"form-group\" *ngIf=\"form.type=='top-up'\">\r\n        <div class=\"label\">\r\n          <label>Categories {{title}}</label>\r\n        </div>\r\n        <div class=\"col-md-8\">\r\n          <select class=\"form-control\" [(ngModel)]=\"form.category\" [ngClass]=\"categoryAlert? 'red-alert' : ''\">\r\n            <option *ngFor=\"let mt of categoryEwallet\" [ngValue]='mt.code'>{{mt.name}}</option>\r\n          </select>\r\n          <div *ngIf=\"categoryAlert\" class=\"red-error-alert\">*category tidak boleh kosong</div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <!-- PRoduct Code -->\r\n    <div class=\"col-md-6\" *ngIf=\"form.type=='e-voucher'\">\r\n      <div class=\"form-group\">\r\n        <div class=\"label\">\r\n          <label for=\"exampleInputEmail1\">SKU Code </label>\r\n          <!-- <i class=\"fas fa-info-circle\" id=\"tooltip\"> <span id=\"tooltiptext\"> Maximum Character For {{title}} Code is\r\n              20\r\n              Characters</span></i> -->\r\n          <span class=\"autoSKU\">\r\n            <!-- <mat-checkbox [(ngModel)]=\"autoSKU\" value=\"autoSKU\">\r\n    auto-type SKU\r\n  </mat-checkbox> -->\r\n          </span>\r\n        </div>\r\n        <div class=\"col-md-12\">\r\n          <input class=\"product_code form-control\" [(ngModel)]=\"form.sku_code\" [ngClass]=\"productCodeAlert? 'red-alert' : ''\" disabled=true/>\r\n          <!-- <div *ngIf=\"productCodeAlert\" class=\"red-error-alert\">*product code tidak boleh kosong</div> -->\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n</div>\r\n<!-- End Of Product Information -->\r\n\r\n\r\n\r\n\r\n\r\n\r\n    <div class=\"upload-container col-md-12\">\r\n      <div class=\"form-group\">\r\n        <label for=\"exampleInputEmail1\">Description</label>\r\n        <!-- <textarea class=\"form-control\" style=\"min-height: 200px;\" [(ngModel)]=\"form.description\"></textarea> -->\r\n        <ckeditor [config]=\"config\" class=\"description \" [editor]=\"Editor\" [(ngModel)]=\"form.description\" [ngClass]=\"descriptionAlert? 'red-alert' : ''\" id=\"description\"></ckeditor>\r\n        <!-- <input class=\"description form-control\" [(ngModel)]=\"form.description\" /> -->\r\n      </div>\r\n      <div *ngIf=\"descriptionAlert\" class=\"red-error-alert\">*description tidak boleh kosong</div>\r\n\r\n      <div class=\"form-group\" *ngIf=\"form.type == 'e-voucher'\">\r\n        <label for=\"exampleInputEmail1\">Terms &amp; Conditions</label>\r\n        <!-- <textarea class=\"form-control\" style=\"min-height: 200px;\" [(ngModel)]=\"form.tnc\"></textarea> -->\r\n        <ckeditor [config]=\"config\" class=\"tnc \" [editor]=\"Editor\" [(ngModel)]=\"form.tnc\" [ngClass]=\"tncAlert? 'red-alert' : ''\" id=\"tnc\"></ckeditor>\r\n      </div>\r\n      <div *ngIf=\"tncAlert\" class=\"red-error-alert\">*tnc tidak boleh kosong</div>\r\n\r\n      <div class=\"form-group\" *ngIf=\"form.type == 'evoucher'\"> \r\n        <label>Location For Redeem</label>\r\n        <ckeditor [config]=\"config\" class=\"tnc \" [editor]=\"Editor\" [(ngModel)]=\"form.location_for_redeem\"></ckeditor>\r\n        <!-- <textarea class=\"form-control\" style=\"min-height: 200px;\" [(ngModel)]=\"form.location_for_redeem\"></textarea> -->\r\n      </div>\r\n      <!-- <div class=\"form-group\">\r\n        <label>Condition</label>\r\n        <select class=\"form-control\" [(ngModel)]=\"form.condition\">\r\n          <option [ngValue]=\"'new'\">new</option>\r\n          <option [ngValue]=\"'second'\">second</option>\r\n        </select>\r\n      </div> -->\r\n      </div>\r\n\r\n\r\n\r\n      <!-- Varian -->\r\n    <div class=\"upload-container col-md-12\" *ngIf=\"multiVarian.length > 0\" id=\"varian\" [ngClass]=\"varianAlert? 'red-alert' : ''\">\r\n      <div class=\"content-title-container\">\r\n        <h3 class=\"content-title\">Varian</h3>\r\n      </div>\r\n\r\n      <div class=\"label\">\r\n        <label for=\"exampleInputEmail1\">Tipe Varian</label>\r\n      </div>\r\n\r\n      <!-- <div class=\"col-md-12 form-varian\" style=\"display:flex\" *ngFor=\"let _dataVarian of dataVarian; index as x\">\r\n        <div class=\"col-md-6\">\r\n          <div class=\"col-md-12\" class=\"varian-parent\">\r\n            <form-input  [type]=\"'text'\" [placeholder]=\"'Masukkan nama variasi, contoh : warna, dll'\" autofocus required (keyup)=\"changeText()\" [(ngModel)] = varianHeader[x] class=\"varian-type-input\"></form-input>\r\n            <i class=\"fas fa-trash\" id=\"tooltip\" (click) = deleteVariationType(x)></i>\r\n          </div>\r\n        </div>\r\n        \r\n      \r\n        <div class=\"col-md-6\">\r\n          <div class=\"col-md-12\" *ngFor=\"let _valueVariation of _dataVarian; index as i\" class=\"varian-parent\">\r\n            <form-input [type]=\"'text'\" [placeholder]=\"'Masukkan pilihan variasi, contoh : merah, dll'\" autofocus required [(ngModel)]=\"dataVarianModel[x][i]\" (keyup)=\"changeText()\" class=\"varian-value-input\"></form-input>\r\n            <i *ngIf=\"_dataVarian.length > 1\" class=\"fas fa-trash\" id=\"tooltip\" (click) = deleteVariationValue(x,i)></i>\r\n          </div>\r\n\r\n\r\n          <button type=\"button\" (click)=\"addVariationValue(x)\" class=\"add-value btn\">Add Value</button>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 form-varian\" *ngIf=\"dataVarian.length < 2\">\r\n        <button type=\"button\" (click)=\"addVariationType()\" class=\"btn\">Add Varian</button>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 varian-table form-varian\">\r\n        <div class=\"form-group\">\r\n          <table class=\"table table-striped\">\r\n            <thead>\r\n              <tr>\r\n                <th *ngFor=\"let _varianHeader of varianHeader\">{{_varianHeader}}</th>\r\n                <th *ngFor=\"let _headerValue of headerValue\">{{_headerValue}}</th>\r\n              </tr>\r\n            </thead>\r\n            <tbody>\r\n              <tr *ngFor=\"let _productSku of productSku; index as i\">\r\n                <td *ngFor=\"let _varianHeader of varianHeader\">\r\n                  {{_productSku.variant[_varianHeader]}}\r\n                </td>\r\n                <td>\r\n                  <input class=\"price form-control\" [(ngModel)]=\"_productSku.sku_code\" />\r\n                </td>\r\n                <td>\r\n                  <input class=\"price form-control\" [(ngModel)]=\"_productSku.sku_price\" />\r\n                </td>\r\n                <td>\r\n                  <input class=\"price form-control\" [(ngModel)]=\"_productSku.sku_value\" />\r\n                </td>\r\n                <td>\r\n                  <input class=\"price form-control\" [(ngModel)]=\"_productSku.qty\" />\r\n                </td>\r\n                <td *ngIf=\"_productSku.image_gallery.length == 0 && !isLoading[i]\">\r\n                  <input #inputFileVarian type=\"file\" style=\"display:none\" (change)=\"onFileVariant($event, _productSku.image_gallery, i)\" accept=\"image/x-png,image/gif,image/jpeg\">\r\n                  <a class=\"common-button blue-upload fileupload\" (click)=\"inputFileVarian.click()\">+ Add Image</a>\r\n                </td>\r\n                <td *ngIf=\"isLoading[i]\">\r\n                  <div class=\"form-group uploaded\">\r\n                    <div class=\"loading\">\r\n                      <div class=\"sk-fading-circle\">\r\n                        <div class=\"sk-circle1 sk-circle\"></div>\r\n                        <div class=\"sk-circle2 sk-circle\"></div>\r\n                        <div class=\"sk-circle3 sk-circle\"></div>\r\n                        <div class=\"sk-circle4 sk-circle\"></div>\r\n                        <div class=\"sk-circle5 sk-circle\"></div>\r\n                        <div class=\"sk-circle6 sk-circle\"></div>\r\n                        <div class=\"sk-circle7 sk-circle\"></div>\r\n                        <div class=\"sk-circle8 sk-circle\"></div>\r\n                        <div class=\"sk-circle9 sk-circle\"></div>\r\n                        <div class=\"sk-circle10 sk-circle\"></div>\r\n                        <div class=\"sk-circle11 sk-circle\"></div>\r\n                        <div class=\"sk-circle12 sk-circle\"></div>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </td>\r\n                <td *ngIf=\"_productSku.image_gallery.length > 0 && !isLoading[i]\" class=\"varian-image-parent\">\r\n                  <img src=\"{{_productSku.image_gallery[0].base_url + _productSku.image_gallery[0].pic_file_name}}\" style=\"width: 200px\" />\r\n                  <i class=\"fas fa-times-circle delete-varian-image\" (click) = \"deleteVarianImage(_productSku.image_gallery)\"></i>\r\n                </td>\r\n                <td>\r\n                  <select class=\"form-control status-form\" [(ngModel)]=\"_productSku.status\">\r\n                    <option value=\"ACTIVE\">Active</option>\r\n                    <option value=\"INACTIVE\">Inactive</option>\r\n                  </select>\r\n                </td>\r\n              </tr>\r\n            </tbody>\r\n          </table>\r\n        </div>\r\n      </div> -->\r\n\r\n\r\n      <div class=\"col-md-12\" style=\"display:flex\" *ngIf=\"varianX.active\">\r\n        <div class=\"col-md-6\">\r\n          <div class=\"col-md-12\" class=\"varian-parent\">\r\n            <form-input  type=\"text\" placeholder=\"Masukkan nama variasi, contoh : warna, dll\" [(ngModel)]=\"varianX.name\" [alert]=\"fieldVarianX\" [onKeyup]=\"eventValidation\"></form-input>\r\n            <i class=\"fas fa-trash\" id=\"tooltip\" (click) = deleteAllTypeX()></i>\r\n          </div>\r\n        </div>\r\n        \r\n        <!-- Using ng-option and for loop-->\r\n        <div class=\"col-md-6\">\r\n          <div class=\"col-md-12\" *ngFor=\"let item of [].constructor(varianTypeX.length); index as iox\" class=\"varian-parent\">\r\n            <form-input type=\"text\" placeholder=\"Masukkan pilihan variasi, contoh : merah, dll\" [(ngModel)]=\"varianTypeX[iox]\" [alert]=\"fieldVarianTypeX[iox]\" [onKeyup]=\"eventValidation\"></form-input>\r\n            <i *ngIf=\"varianTypeX.length > 1\" class=\"fas fa-trash\" id=\"tooltip\" (click) = deleteVarianTypeX(iox)></i>\r\n          </div>\r\n\r\n\r\n          <button type=\"button\" (click)=\"addVarianTypeX()\" class=\"btn add-value\">Add Value</button>\r\n        </div>\r\n      </div>\r\n\r\n      <ng-container>\r\n        <div class=\"col-md-12\" *ngIf=\"!varianY.active\">\r\n          <button type=\"button\" (click)=\"activatedVarianY()\" class=\"btn\">Add Varian</button>\r\n        </div>\r\n      </ng-container>\r\n\r\n      <div class=\"col-md-12\" style=\"display:flex\" *ngIf=\"varianY.active\">\r\n        <div class=\"col-md-6\">\r\n          <div class=\"col-md-12\" class=\"varian-parent\">\r\n            <form-input  type=\"text\" placeholder=\"Masukkan nama variasi, contoh : warna, dll\" [(ngModel)]=\"varianY.name\" [alert]=\"fieldVarianY\" [onKeyup]=\"eventValidation\"></form-input>\r\n            <i class=\"fas fa-trash\" id=\"tooltip\" (click) = deleteAllTypeY()></i>\r\n          </div>\r\n        </div>\r\n        \r\n        <!-- Using ng-option and for loop-->\r\n        <div class=\"col-md-6\">\r\n          <div class=\"col-md-12\" *ngFor=\"let item of [].constructor(varianTypeY.length); index as ioy\" class=\"varian-parent\">\r\n            <form-input type=\"text\" placeholder=\"Masukkan pilihan variasi, contoh : merah, dll\" [(ngModel)]=\"varianTypeY[ioy]\" [alert]=\"fieldVarianTypeY[ioy]\" [onKeyup]=\"eventValidation\"></form-input>\r\n            <i *ngIf=\"varianTypeY.length > 1\" class=\"fas fa-trash\" id=\"tooltip\" (click) = deleteVarianTypeY(ioy)></i>\r\n          </div>\r\n\r\n\r\n          <button type=\"button\" (click)=\"addVarianTypeY()\" class=\"btn add-value\">Add Value</button>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"col-md-12 varian-table\">\r\n        <div class=\"form-group\">\r\n          <table class=\"table table-striped\">\r\n            <thead>\r\n              <tr>\r\n                <th>\r\n                  {{varianY.name}}\r\n                </th>\r\n                <th>\r\n                  {{varianX.name}}\r\n                </th>\r\n                <th *ngFor=\"let _headerValue of headerValue\">{{_headerValue}}</th>\r\n              </tr>\r\n            </thead>\r\n            <tbody>\r\n              <ng-container *ngFor=\"let varianY of multiVarian; index as ioy\">\r\n              <tr *ngFor=\"let varianYX of varianY; index as iox\">\r\n                <td>\r\n                  {{varianTypeY[ioy]}}\r\n                </td>\r\n                <td>\r\n                  {{varianTypeX[iox]}}\r\n                </td>\r\n                <td>\r\n                  <input class=\"price form-control\" [(ngModel)]=\"multiVarian[ioy][iox].sku_code\" />\r\n                </td>\r\n                <td>\r\n                  <input type=\"number\" class=\"price form-control\" [(ngModel)]=\"multiVarian[ioy][iox].sku_price\" />\r\n                </td>\r\n                <td>\r\n                  <input type=\"number\" class=\"price form-control\" [(ngModel)]=\"multiVarian[ioy][iox].sku_value\" />\r\n                </td>\r\n                <td>\r\n                  <input type=\"number\" class=\"price form-control\" [(ngModel)]=\"multiVarian[ioy][iox].qty\" />\r\n                </td>\r\n                <td *ngIf=\"multiVarian[ioy][iox].image_gallery.length == 0 && !multiVarian[ioy][iox].isloading\">\r\n                  <input #inputFileVarian type=\"file\" style=\"display:none\" (change)=\"onFileVariant($event, multiVarian[ioy][iox].image_gallery, [ioy,iox])\" accept=\"image/x-png,image/gif,image/jpeg\">\r\n                  <a class=\"common-button blue-upload fileupload\" (click)=\"inputFileVarian.click()\">+ Add Image</a>\r\n                </td>\r\n                <td *ngIf=\"multiVarian[ioy][iox].isloading\">\r\n                  <div class=\"form-group uploaded\">\r\n                    <div class=\"loading\">\r\n                      <div class=\"sk-fading-circle\">\r\n                        <div class=\"sk-circle1 sk-circle\"></div>\r\n                        <div class=\"sk-circle2 sk-circle\"></div>\r\n                        <div class=\"sk-circle3 sk-circle\"></div>\r\n                        <div class=\"sk-circle4 sk-circle\"></div>\r\n                        <div class=\"sk-circle5 sk-circle\"></div>\r\n                        <div class=\"sk-circle6 sk-circle\"></div>\r\n                        <div class=\"sk-circle7 sk-circle\"></div>\r\n                        <div class=\"sk-circle8 sk-circle\"></div>\r\n                        <div class=\"sk-circle9 sk-circle\"></div>\r\n                        <div class=\"sk-circle10 sk-circle\"></div>\r\n                        <div class=\"sk-circle11 sk-circle\"></div>\r\n                        <div class=\"sk-circle12 sk-circle\"></div>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </td>\r\n                \r\n                <td *ngIf=\"multiVarian[ioy][iox].image_gallery.length > 0 && !multiVarian[ioy][iox].isloading\" class=\"varian-image-parent\">\r\n                  <img src=\"{{multiVarian[ioy][iox].image_gallery[0].base_url + multiVarian[ioy][iox].image_gallery[0].pic_file_name}}\" style=\"width: 200px\" />\r\n                  <i class=\"fas fa-times-circle delete-varian-image\" (click) = \"deleteVarianImage(multiVarian[ioy][iox].image_gallery)\"></i>\r\n                </td>\r\n                <td>\r\n                  <select class=\"form-control status-form\" [(ngModel)]=\"multiVarian[ioy][iox].status\">\r\n                    <option value=\"ACTIVE\">Active</option>\r\n                    <option value=\"INACTIVE\">Inactive</option>\r\n                  </select>\r\n                </td>\r\n              </tr>\r\n            </ng-container>\r\n            </tbody>\r\n          </table>\r\n        </div>\r\n      </div>\r\n    </div>      \r\n\r\n    <div *ngIf=\"varianAlert\" class=\"red-error-alert\">{{varianAlertMessage}}</div>\r\n\r\n\r\n    <div class=\"upload-container col-md-12 varian-button\" *ngIf=\"form.type=='product' && multiVarian.length == 0\" (click)=\"clickVarian()\">\r\n      <i class=\"fas fa-plus-circle\"></i> Aktifkan Varian\r\n    </div>\r\n\r\n    <div class=\"form-group upload-container\" *ngIf=\"multiVarian.length == 0\" id=\"price\">\r\n      <div class=\"row\">\r\n        <div class=\"col-md-6\">\r\n          <div class=\"form-group\">\r\n            <label for=\"exampleInputEmail1\">{{title}} Price</label>\r\n            <div>\r\n              <input class=\"price form-control\" (keyup)=\"formPriceReplacer($event, 'product_price')\" [(ngModel)]=\"form.product_price\" [ngClass]=\"productPriceAlert? 'red-alert' : ''\"/>\r\n              <div *ngIf=\"productPriceAlert\" class=\"red-error-alert\">*product price tidak boleh nol</div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-6\">\r\n          <label for=\"fixed_price\">{{title}} Value</label>\r\n          <div>\r\n            <input class=\"fixed_price form-control\" name=\"fixed_price\"\r\n            (keyup)=\"formPriceReplacer($event, 'product_value')\" [(ngModel)]=\"form.product_value\" [ngClass]=\"productValueAlert? 'red-alert' : ''\" />\r\n            <div *ngIf=\"productValueAlert\" class=\"red-error-alert\">*product value tidak boleh nol</div>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-6\" *ngIf=\"form.type=='product'|| form.type=='voucher' || form.type == 'gold' || form.type == 'top-up'\">\r\n          <label for=\"fixed_price\">Qty</label>\r\n          <input class=\"fixed_price form-control\" name=\"qty\" [(ngModel)]=\"form.qty\" />\r\n        </div>\r\n      </div>\r\n    </div>\r\n    \r\n\r\n\r\n\r\n\r\n\r\n      <div class=\"form-group upload-container\" *ngIf=\"form.type == 'product' || form.type=='voucher' || form.type == 'gold'\" id=\"weight\" [ngClass]=\"widthAlert || lengthAlert || heightAlert? 'red-alert' : ''\">\r\n        <label for=\"exampleInputEmail1\">Dimensions: (<em>Width x Length x Height</em>)</label>\r\n        <div class=\"input-group mb-3\">\r\n          <!-- Width -->\r\n          <div class=\"input-group-prepend\">\r\n            <span class=\"input-group-text\" id=\"basic-addon1\">Width</span>\r\n          </div>\r\n          <div class=\"form-control des\">\r\n            <input type=\"text\" type=\"number\" [(ngModel)]=\"form.dimensions.width\" aria-label=\"Username\"\r\n              aria-describedby=\"basic-addon1\">\r\n            cm\r\n          </div>\r\n          <!-- Length -->\r\n          <div class=\"input-group-prepend\">\r\n            <span class=\"input-group-text\" id=\"basic-addon1\">Length</span>\r\n          </div>\r\n          <div class=\"form-control des\">\r\n            <input type=\"text\" class=\"\" type=\"number\" [(ngModel)]=\"form.dimensions.length\" aria-label=\"Username\"\r\n              aria-describedby=\"basic-addon1\">\r\n            cm\r\n          </div>\r\n          <!-- Height -->\r\n          <div class=\"input-group-prepend\">\r\n            <span class=\"input-group-text\" id=\"basic-addon1\">Height</span>\r\n          </div>\r\n          <div class=\"form-control des\">\r\n            <input type=\"text\" class=\"\" type=\"number\" [(ngModel)]=\"form.dimensions.height\" aria-label=\"Username\"\r\n              aria-describedby=\"basic-addon1\">\r\n            cm\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div *ngIf=\"widthAlert\" class=\"red-error-alert\">*width value tidak boleh nol</div> \r\n      <div *ngIf=\"lengthAlert\" class=\"red-error-alert\">*length value tidak boleh nol</div> \r\n      <div *ngIf=\"heightAlert\" class=\"red-error-alert\">*height value tidak boleh nol</div> \r\n      <div class=\"form-group upload-container\" *ngIf=\"form.type == 'product' || form.type=='voucher' || form.type=='gold'\" id=\"weight\">\r\n        <label for=\"exampleInputEmail1\">Weight</label>\r\n        <div class=\"input-group mb-3\">\r\n          <!-- Width -->\r\n          <!-- <div class=\"input-group-prepend\">\r\n                <span class=\"input-group-text\" id=\"basic-addon1\">Width</span>\r\n              </div> -->\r\n          <div class=\"form-control des\">\r\n            <input type=\"text\" type=\"text\" (keyup)=\"formPriceReplacer($event, 'weight')\"\r\n              [(ngModel)]=\"form.weight\" aria-label=\"weight\" aria-describedby=\"basic-addon1\" [ngClass]=\"weightAlert? 'red-alert' : ''\">\r\n            gram\r\n          </div>\r\n\r\n        </div>\r\n        <div *ngIf=\"weightAlert\" class=\"red-error-alert\">*weight tidak boleh nol</div> \r\n      </div>\r\n      <div class=\"form-group upload-container\">\r\n        <label>Status</label>\r\n        <form-select [(ngModel)]=\"form.status\" [data]=\"productStatus\"></form-select>\r\n        <!-- <select class=\"form-control\" [(ngModel)]=\"form.status\" [data]=\"productStatus\"> -->\r\n          <!-- <option [ngValue]=\"INACTIVE\">Inactive</option>\r\n          <option [ngValue]=\"ACTIVE\">Active</option> -->\r\n        <!-- </select> -->\r\n      </div>\r\n      <!-- <div class=\"form-group\">\r\n        <label for=\"exampleInputEmail1\">Minimum Order</label>\r\n        <input class=\"min_order form-control\" type=\"number\" [(ngModel)]=\"form.min_order\" />\r\n      </div> -->\r\n      <!-- <div class=\"image-upload new-upload\" *ngIf=\"!form.base_url\">\r\n        <button class=\"fileupload\" (click)=\"uploadFile()\">file upload</button>\r\n        <div [ngClass]=\"'uploading-file '+isFileUploaded\">\r\n          <img *ngIf=\"selFile\" [src]=\"selFile\" height=\"200\" />\r\n          <span *ngIf=\"isFileUploaded\"> UPLOADED </span>\r\n        </div>\r\n        <br />\r\n        <div class=\"pb-framer\">\r\n          <div class=\"progressbar {{cancel?'terminated':''}}\" [ngStyle]=\"{'width.%': progressBar}\">\r\n\r\n          </div>\r\n          <span *ngIf=\"progressBar<58&&progressBar>0\">\r\n            {{cancel?'terminated':progressBar+'%'}} </span>\r\n          <span class=\"clr-white\" *ngIf=\"progressBar>58\">\r\n            {{cancel?'terminated':progressBar+'%'}} </span>\r\n\r\n        </div>\r\n\r\n        <div class=\"error\" *ngIf=\"errorLabel\">{{errorLabel}}</div>\r\n\r\n\r\n      </div> -->\r\n\r\n\r\n    </div>\r\n    <div class=\"col-md-4\">\r\n      <div class=\"card\" *ngIf=\"form.type=='evoucher'\">\r\n        <div class=\"card-header\">\r\n          <label class=\"uploading\">Upload New Image</label>\r\n        </div>\r\n        <div class=\"form-group\" *ngIf=\"!form.base_url\">\r\n          <div class=\"image-upload\">\r\n            <i><img src=\"assets/images/upload.svg\" class=\"logo-upload\"></i>\r\n            <p class=\"upload-desc\">Upload your image here <br> or</p>\r\n            <button class=\"btn btn-primary fileupload\" (click)=\"fileinput.click()\">Browse File</button>\r\n            <div [ngClass]=\"'uploading-file '+isFileUploaded\">\r\n              <img *ngIf=\"selFile\" [src]=\"selFile\" height=\"200\" />\r\n              <span *ngIf=\"isFileUploaded\"> UPLOADED </span>\r\n            </div>\r\n            <div class=\"pb-framer\">\r\n              <div class=\"progressbar {{cancel?'terminated':''}}\" [ngStyle]=\"{'width.%': progressBar}\">\r\n\r\n              </div>\r\n              <span *ngIf=\"progressBar<58&&progressBar>0\">\r\n                {{cancel?'terminated':progressBar+'%'}} </span>\r\n              <span class=\"clr-white\" *ngIf=\"progressBar>58\">\r\n                {{cancel?'terminated':progressBar+'%'}} </span>\r\n\r\n            </div>\r\n\r\n            <div class=\"error\" *ngIf=\"errorLabel\">{{errorLabel}}</div>\r\n\r\n          </div>\r\n        </div>\r\n        <div class=\"form-group uploaded\" *ngIf=\"form.base_url\">\r\n          <!-- <label> Your Product Image</label> -->\r\n          <div class=\"uploaded-image\">\r\n            <div class=\"img\" [ngStyle]=\"{'background-image':'url('+(form.base_url+form.pic_medium_path)+')'}\">\r\n              <div class=\"editor-tool\">\r\n                <!-- <button >TEST</button> -->\r\n                <i class=\"fa fa-pen\"></i>\r\n              </div>\r\n            </div>\r\n            <a class=\"text-fileupload\" (click)=\"fileinput.click()\">re-upload it</a>\r\n\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <!-- <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <label>Categories</label>\r\n        </div>\r\n        <div class=\"navbar-menu\">\r\n          <div class=\"list-group category-list\">\r\n            <div class=\"list-group-item\" *ngFor=\"let n of multipleCategories; let i = index;\"\r\n              (click)=\"toggleCategories(i)\">\r\n              <span class=\"checked-icon {{n.value}}\">&#10003; </span> {{n.name}}\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div> -->\r\n\r\n      <div class=\"condition-wrapper\" *ngIf=\"form.type == 'evoucher'\">\r\n        <div class=\"form-group\">\r\n          <button class=\"btn btn-default\" style=\"width: 100%;\" *ngIf=\"generateVoucherToggler == 0\"\r\n            (click)=\"genVoucherToggler(1)\">Generate voucher</button>\r\n          <div class=\"row\">\r\n            <div class=\"col-md-12\" *ngIf=\"generateVoucherToggler == 1\">\r\n              <label>How do you want to generate voucher?</label>\r\n              <button class=\"atc btn btn-default\" (click)=\"genVoucherToggler(3)\">upload it</button>\r\n              <button class=\"atc btn btn-default\" (click)=\"genVoucherToggler(2)\">automatic</button>\r\n            </div>\r\n            <div class=\"col-md-12\" *ngIf=\"generateVoucherToggler == 2\">\r\n              <label>Please set expired Date</label>\r\n\r\n              <div class=\"date-picker-overlay\">\r\n                <input class=\"form-control\" type=\"text\" ngbDatepicker #d=\"ngbDatepicker\" (focus)=\"d.open()\"\r\n                  [(ngModel)]=\"genVoucher.expiry_date\" />\r\n              </div>\r\n              <label>How many?</label>\r\n              <input class=\"form-control\" type=\"number\" placeholder=\"quantity\" [(ngModel)]=\"genVoucher.qty\" />\r\n              <br /><br />\r\n              <button class=\"btn btn-default\" style=\"width: 100%\" (click)=\"generateVoucher()\"> Generate now</button>\r\n              <!-- <div class=\"dpicker\">\r\n                  <ngb-datepicker #d (select)=\"datePickerOnDateSelection($event, generateExpirationDate)\" [displayMonths]=\"1\"\r\n                    [dayTemplate]=\"t\" outsideDays=\"hidden\"></ngb-datepicker>\r\n                </div> -->\r\n              <!-- <ng-template #t let-date=\"date\" let-focused=\"focused\">\r\n                <span class=\"custom-day\" [class.focused]=\"focused\">\r\n                  {{ date.day }}\r\n                </span>\r\n              </ng-template> -->\r\n            </div>\r\n            <div class=\"col-md-12\" *ngIf=\"generateVoucherToggler == 3\">\r\n              <!-- <label>Choose File</label> -->\r\n              <input class=\"custom-content\" type=\"file\" style=\"color:black\" title=\"Choose File\"\r\n                (change)=\"onFileSelected($event)\" accept=\".csv, .xlsx, .xls\" />\r\n              <button class=\"btn btn-default\" (click)=\"uploadFile()\">Upload</button>\r\n\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n          <button class=\"btn btn-default\" style=\"width: 100%;\" (click)=\"generatedVoucher()\">Generated Vouchers\r\n            List</button>\r\n        </div>\r\n        <div class=\"col-md-12\">\r\n          <div class=\"form-group\" *ngIf=\"form.type=='evoucher'\">\r\n            <label>Need Outlet Code?</label>\r\n            <div>\r\n              <select class=\"form-control status-form\" [(ngModel)]=\"form.need_outlet_code\">\r\n                <option [ngValue]=\"0\" class=\"false\">No</option>\r\n                <option [ngValue]=\"1\">Yes</option>\r\n              </select>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-12\">\r\n          <div class=\"form-group\" *ngIf=\"form.type=='evoucher'\">\r\n            <label>Applicable For IOS?</label>\r\n            <div>\r\n              <select class=\"form-control status-form\" [(ngModel)]=\"form.need_outlet_code\">\r\n                <option [ngValue]=\"false\" class=\"false\">No</option>\r\n                <option [ngValue]=\"true\">Yes</option>\r\n              </select>\r\n            </div>\r\n          </div>\r\n        </div>\r\n       \r\n      </div>\r\n    </div>\r\n\r\n    <br>\r\n  </div>\r\n  <div class=\"row end-footer\">\r\n    <div class=\"col-md-12\">\r\n      <div class=\"form-group\">\r\n        <button class=\"btn save btn-primary\" (click)=\"saveThis()\"><i class=\"fa fa-save\"></i> Save this</button>\r\n        <button *ngIf=\"!toggleDelete\" class=\"btn delete btn-primary\" (click)=\"toggleDeleteConfirm(true)\">\r\n          <i class=\"fa fa-trash\"></i> Delete\r\n        </button>\r\n        <div *ngIf=\"toggleDelete\" class=\"delete-modal\">\r\n          Are you sure to delete this? &nbsp;&nbsp;\r\n          <div class=\"btn-group\">\r\n            <button class=\"btn no btn-sm btn-default\" (click)=\"toggleDeleteConfirm(false)\"><i class=\"fa fa-save\"></i>\r\n              No</button>\r\n            <button class=\"btn yes btn-sm btn-primary\" (click)=\"deleteThis()\"><i class=\"fa fa-save\"></i> Yes</button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div *ngIf=\"errorMessage\" class=\"error-message\">\r\n  <div class=\"text-message\">\r\n      <i class=\"fas fa-ban\"></i>\r\n      {{errorMessage}}\r\n  </div>\r\n</div>\r\n\r\n<!-- <button class=\"btn button-edit\" (click)=\"setEdit()\">\r\n  <i class=\"fa fa-fw fa-pencil-alt\"></i> Edit your product\r\n</button> -->\r\n"

/***/ }),

/***/ "./src/app/layout/merchant-portal/separated-modules/merchant-products/products.module.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/separated-modules/merchant-products/products.module.ts ***!
  \***********************************************************************************************/
/*! exports provided: ProductsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsModule", function() { return ProductsModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _products_merchant_edit_product_merchant_edit_product_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./products/merchant-edit-product/merchant-edit-product.component */ "./src/app/layout/merchant-portal/separated-modules/merchant-products/products/merchant-edit-product/merchant-edit-product.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ckeditor_ckeditor5_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ckeditor/ckeditor5-angular */ "./node_modules/@ckeditor/ckeditor5-angular/fesm5/ckeditor-ckeditor5-angular.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _products_merchant_add_new_product_merchant_add_new_product_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./products/merchant-add-new-product/merchant-add-new-product.component */ "./src/app/layout/merchant-portal/separated-modules/merchant-products/products/merchant-add-new-product/merchant-add-new-product.component.ts");
/* harmony import */ var _products_evoucher_list_evoucher_list_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./products/evoucher-list/evoucher-list.component */ "./src/app/layout/merchant-portal/separated-modules/merchant-products/products/evoucher-list/evoucher-list.component.ts");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/esm5/checkbox.es5.js");
/* harmony import */ var _products_merchant_detail_product_merchant_detail_product_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./products/merchant-detail-product/merchant-detail-product.component */ "./src/app/layout/merchant-portal/separated-modules/merchant-products/products/merchant-detail-product/merchant-detail-product.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







// import { EvoucherDetailComponent } from '../../../modules/product.evoucher/detail/evoucher.detail.component';




var ProductsModule = /** @class */ (function () {
    function ProductsModule() {
    }
    ProductsModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [
                _products_merchant_detail_product_merchant_detail_product_component__WEBPACK_IMPORTED_MODULE_10__["MerchantDetailProductComponent"],
                _products_merchant_edit_product_merchant_edit_product_component__WEBPACK_IMPORTED_MODULE_2__["MerchantEditProductComponent"],
                _products_merchant_add_new_product_merchant_add_new_product_component__WEBPACK_IMPORTED_MODULE_6__["MerchantAddNewProductComponent"],
                // EvoucherDetailComponent,
                _products_evoucher_list_evoucher_list_component__WEBPACK_IMPORTED_MODULE_7__["EvoucherListComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ckeditor_ckeditor5_angular__WEBPACK_IMPORTED_MODULE_4__["CKEditorModule"],
                // NgbModule,
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbDatepickerModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_8__["FormBuilderTableModule"],
                _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_9__["MatCheckboxModule"],
            ],
            exports: [
                _products_merchant_detail_product_merchant_detail_product_component__WEBPACK_IMPORTED_MODULE_10__["MerchantDetailProductComponent"],
                _products_merchant_edit_product_merchant_edit_product_component__WEBPACK_IMPORTED_MODULE_2__["MerchantEditProductComponent"],
                _products_merchant_add_new_product_merchant_add_new_product_component__WEBPACK_IMPORTED_MODULE_6__["MerchantAddNewProductComponent"],
                // EvoucherDetailComponent,
                _products_evoucher_list_evoucher_list_component__WEBPACK_IMPORTED_MODULE_7__["EvoucherListComponent"]
            ],
            bootstrap: [
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbPagination"],
            ]
        })
    ], ProductsModule);
    return ProductsModule;
}());



/***/ }),

/***/ "./src/app/layout/merchant-portal/separated-modules/merchant-products/products/evoucher-list/evoucher-list.component.scss":
/*!********************************************************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/separated-modules/merchant-products/products/evoucher-list/evoucher-list.component.scss ***!
  \********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".modal.false {\n  display: none;\n  /* Hidden by default */\n  position: fixed;\n  /* Stay in place */\n  z-index: 1;\n  /* Sit on top */\n  padding-top: 100px;\n  /* Location of the box */\n  left: 0;\n  top: 0;\n  width: 100%;\n  /* Full width */\n  height: 100%;\n  /* Full height */\n  overflow: auto;\n  /* Enable scroll if needed */\n  background-color: black;\n  /* Fallback color */\n  background-color: rgba(0, 0, 0, 0.4);\n  /* Black w/ opacity */\n}\n\n.modal.true {\n  display: block;\n  /* Hidden by default */\n  position: fixed;\n  /* Stay in place */\n  z-index: 1;\n  /* Sit on top */\n  padding-top: 100px;\n  /* Location of the box */\n  left: 100px;\n  top: 0;\n  width: 100%;\n  /* Full width */\n  height: 100%;\n  /* Full height */\n  overflow: auto;\n  /* Enable scroll if needed */\n  background-color: black;\n  /* Fallback color */\n  background-color: rgba(0, 0, 0, 0.4);\n  /* Black w/ opacity */\n}\n\n/* Modal Header */\n\n.modal-header {\n  padding: 2px 16px;\n  background-color: #5cb85c;\n  color: white;\n}\n\n/* Modal Body */\n\n.modal-body {\n  padding: 2px 16px;\n}\n\n/* Modal Footer */\n\n.modal-footer {\n  padding: 2px 16px;\n  background-color: #5cb85c;\n  color: white;\n}\n\n/* Modal Content */\n\n.modal-content {\n  margin-left: 256px;\n  position: relative;\n  background-color: #fefefe;\n  margin: auto;\n  padding: 0;\n  border: 1px solid #888;\n  width: 50%;\n  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\n  -webkit-animation-name: animatetop;\n          animation-name: animatetop;\n  -webkit-animation-duration: 0.4s;\n          animation-duration: 0.4s;\n  text-align: center;\n}\n\n/* The Close Button */\n\n.close {\n  color: white;\n  float: right;\n  font-size: 28px;\n  font-weight: bold;\n}\n\n.close:hover,\n.close:focus {\n  color: #000;\n  text-decoration: none;\n  cursor: pointer;\n}\n\n/* Add Animation */\n\n@-webkit-keyframes animatetop {\n  from {\n    top: -300px;\n    opacity: 0;\n  }\n  to {\n    top: 0;\n    opacity: 1;\n  }\n}\n\n@keyframes animatetop {\n  from {\n    top: -300px;\n    opacity: 0;\n  }\n  to {\n    top: 0;\n    opacity: 1;\n  }\n}\n\n.btn-color {\n  color: white;\n  width: auto;\n  padding: 5px 12px;\n  height: 50px;\n  border-radius: 10px;\n  background: limegreen;\n}\n\n#inputan {\n  margin-bottom: 10px;\n  width: 80%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21lcmNoYW50LXBvcnRhbC9zZXBhcmF0ZWQtbW9kdWxlcy9tZXJjaGFudC1wcm9kdWN0cy9wcm9kdWN0cy9ldm91Y2hlci1saXN0L0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbWVyY2hhbnQtcG9ydGFsXFxzZXBhcmF0ZWQtbW9kdWxlc1xcbWVyY2hhbnQtcHJvZHVjdHNcXHByb2R1Y3RzXFxldm91Y2hlci1saXN0XFxldm91Y2hlci1saXN0LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbWVyY2hhbnQtcG9ydGFsL3NlcGFyYXRlZC1tb2R1bGVzL21lcmNoYW50LXByb2R1Y3RzL3Byb2R1Y3RzL2V2b3VjaGVyLWxpc3QvZXZvdWNoZXItbGlzdC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGFBQUE7RUFBZSxzQkFBQTtFQUNmLGVBQUE7RUFBaUIsa0JBQUE7RUFDakIsVUFBQTtFQUFZLGVBQUE7RUFDWixrQkFBQTtFQUFvQix3QkFBQTtFQUNwQixPQUFBO0VBQ0EsTUFBQTtFQUNBLFdBQUE7RUFBYSxlQUFBO0VBQ2IsWUFBQTtFQUFjLGdCQUFBO0VBQ2QsY0FBQTtFQUFnQiw0QkFBQTtFQUNoQix1QkFBQTtFQUE4QixtQkFBQTtFQUM5QixvQ0FBQTtFQUFtQyxxQkFBQTtBQ1V2Qzs7QURQRTtFQUNFLGNBQUE7RUFBZ0Isc0JBQUE7RUFDaEIsZUFBQTtFQUFpQixrQkFBQTtFQUNqQixVQUFBO0VBQVksZUFBQTtFQUNaLGtCQUFBO0VBQW9CLHdCQUFBO0VBQ3BCLFdBQUE7RUFDQSxNQUFBO0VBQ0EsV0FBQTtFQUFhLGVBQUE7RUFDYixZQUFBO0VBQWMsZ0JBQUE7RUFDZCxjQUFBO0VBQWdCLDRCQUFBO0VBQ2hCLHVCQUFBO0VBQThCLG1CQUFBO0VBQzlCLG9DQUFBO0VBQW1DLHFCQUFBO0FDbUJ2Qzs7QURmRSxpQkFBQTs7QUFDQTtFQUNFLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0FDa0JKOztBRGZFLGVBQUE7O0FBQ0E7RUFBYSxpQkFBQTtBQ21CZjs7QURqQkUsaUJBQUE7O0FBQ0E7RUFDRSxpQkFBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtBQ29CSjs7QURqQkUsa0JBQUE7O0FBQ0E7RUFDRSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLHNCQUFBO0VBQ0EsVUFBQTtFQUNBLDRFQUFBO0VBQ0Esa0NBQUE7VUFBQSwwQkFBQTtFQUNBLGdDQUFBO1VBQUEsd0JBQUE7RUFDQSxrQkFBQTtBQ29CSjs7QURqQkUscUJBQUE7O0FBQ0E7RUFDRSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQ29CSjs7QURqQkU7O0VBRUUsV0FBQTtFQUNBLHFCQUFBO0VBQ0EsZUFBQTtBQ29CSjs7QURoQkUsa0JBQUE7O0FBQ0E7RUFDRTtJQUFNLFdBQUE7SUFBYSxVQUFBO0VDcUJyQjtFRHBCRTtJQUFJLE1BQUE7SUFBUSxVQUFBO0VDd0JkO0FBQ0Y7O0FEM0JFO0VBQ0U7SUFBTSxXQUFBO0lBQWEsVUFBQTtFQ3FCckI7RURwQkU7SUFBSSxNQUFBO0lBQVEsVUFBQTtFQ3dCZDtBQUNGOztBRHRCRTtFQUNFLFlBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxxQkFBQTtBQ3dCSjs7QURyQkU7RUFDSSxtQkFBQTtFQUNBLFVBQUE7QUN3Qk4iLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbWVyY2hhbnQtcG9ydGFsL3NlcGFyYXRlZC1tb2R1bGVzL21lcmNoYW50LXByb2R1Y3RzL3Byb2R1Y3RzL2V2b3VjaGVyLWxpc3QvZXZvdWNoZXItbGlzdC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tb2RhbC5mYWxzZSB7XHJcbiAgICBkaXNwbGF5OiBub25lOyAvKiBIaWRkZW4gYnkgZGVmYXVsdCAqL1xyXG4gICAgcG9zaXRpb246IGZpeGVkOyAvKiBTdGF5IGluIHBsYWNlICovXHJcbiAgICB6LWluZGV4OiAxOyAvKiBTaXQgb24gdG9wICovXHJcbiAgICBwYWRkaW5nLXRvcDogMTAwcHg7IC8qIExvY2F0aW9uIG9mIHRoZSBib3ggKi9cclxuICAgIGxlZnQ6IDA7XHJcbiAgICB0b3A6IDA7XHJcbiAgICB3aWR0aDogMTAwJTsgLyogRnVsbCB3aWR0aCAqL1xyXG4gICAgaGVpZ2h0OiAxMDAlOyAvKiBGdWxsIGhlaWdodCAqL1xyXG4gICAgb3ZlcmZsb3c6IGF1dG87IC8qIEVuYWJsZSBzY3JvbGwgaWYgbmVlZGVkICovXHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMCwwLDApOyAvKiBGYWxsYmFjayBjb2xvciAqL1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLDAsMCwwLjQpOyAvKiBCbGFjayB3LyBvcGFjaXR5ICovXHJcbiAgfVxyXG4gIFxyXG4gIC5tb2RhbC50cnVle1xyXG4gICAgZGlzcGxheTogYmxvY2s7IC8qIEhpZGRlbiBieSBkZWZhdWx0ICovXHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7IC8qIFN0YXkgaW4gcGxhY2UgKi9cclxuICAgIHotaW5kZXg6IDE7IC8qIFNpdCBvbiB0b3AgKi9cclxuICAgIHBhZGRpbmctdG9wOiAxMDBweDsgLyogTG9jYXRpb24gb2YgdGhlIGJveCAqL1xyXG4gICAgbGVmdDogMTAwcHg7XHJcbiAgICB0b3A6IDA7XHJcbiAgICB3aWR0aDogMTAwJTsgLyogRnVsbCB3aWR0aCAqL1xyXG4gICAgaGVpZ2h0OiAxMDAlOyAvKiBGdWxsIGhlaWdodCAqL1xyXG4gICAgb3ZlcmZsb3c6IGF1dG87IC8qIEVuYWJsZSBzY3JvbGwgaWYgbmVlZGVkICovXHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMCwwLDApOyAvKiBGYWxsYmFjayBjb2xvciAqL1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLDAsMCwwLjQpOyAvKiBCbGFjayB3LyBvcGFjaXR5ICovXHJcbiAgfVxyXG4gIFxyXG4gIFxyXG4gIC8qIE1vZGFsIEhlYWRlciAqL1xyXG4gIC5tb2RhbC1oZWFkZXIge1xyXG4gICAgcGFkZGluZzogMnB4IDE2cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNWNiODVjO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gIH1cclxuICBcclxuICAvKiBNb2RhbCBCb2R5ICovXHJcbiAgLm1vZGFsLWJvZHkge3BhZGRpbmc6IDJweCAxNnB4O31cclxuICBcclxuICAvKiBNb2RhbCBGb290ZXIgKi9cclxuICAubW9kYWwtZm9vdGVyIHtcclxuICAgIHBhZGRpbmc6IDJweCAxNnB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzVjYjg1YztcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICB9XHJcbiAgXHJcbiAgLyogTW9kYWwgQ29udGVudCAqL1xyXG4gIC5tb2RhbC1jb250ZW50IHtcclxuICAgIG1hcmdpbi1sZWZ0OiAyNTZweDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZWZlZmU7XHJcbiAgICBtYXJnaW46IGF1dG87XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgIzg4ODtcclxuICAgIHdpZHRoOiA1MCU7XHJcbiAgICBib3gtc2hhZG93OiAwIDRweCA4cHggMCByZ2JhKDAsMCwwLDAuMiksMCA2cHggMjBweCAwIHJnYmEoMCwwLDAsMC4xOSk7XHJcbiAgICBhbmltYXRpb24tbmFtZTogYW5pbWF0ZXRvcDtcclxuICAgIGFuaW1hdGlvbi1kdXJhdGlvbjogMC40cztcclxuICAgIHRleHQtYWxpZ246Y2VudGVyO1xyXG4gIH1cclxuICBcclxuICAvKiBUaGUgQ2xvc2UgQnV0dG9uICovXHJcbiAgLmNsb3NlIHtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGZsb2F0OiByaWdodDtcclxuICAgIGZvbnQtc2l6ZTogMjhweDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIH1cclxuICBcclxuICAuY2xvc2U6aG92ZXIsXHJcbiAgLmNsb3NlOmZvY3VzIHtcclxuICAgIGNvbG9yOiAjMDAwO1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gIH1cclxuICBcclxuICBcclxuICAvKiBBZGQgQW5pbWF0aW9uICovXHJcbiAgQGtleWZyYW1lcyBhbmltYXRldG9wIHtcclxuICAgIGZyb20ge3RvcDogLTMwMHB4OyBvcGFjaXR5OiAwfVxyXG4gICAgdG8ge3RvcDogMDsgb3BhY2l0eTogMX1cclxuICB9XHJcblxyXG4gIC5idG4tY29sb3J7XHJcbiAgICBjb2xvcjp3aGl0ZTtcclxuICAgIHdpZHRoOiBhdXRvO1xyXG4gICAgcGFkZGluZzogNXB4IDEycHg7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gICAgYmFja2dyb3VuZDogbGltZWdyZWVuO1xyXG4gIH1cclxuXHJcbiAgI2lucHV0YW4ge1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gICAgICB3aWR0aDogODAlO1xyXG4gIH0iLCIubW9kYWwuZmFsc2Uge1xuICBkaXNwbGF5OiBub25lO1xuICAvKiBIaWRkZW4gYnkgZGVmYXVsdCAqL1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIC8qIFN0YXkgaW4gcGxhY2UgKi9cbiAgei1pbmRleDogMTtcbiAgLyogU2l0IG9uIHRvcCAqL1xuICBwYWRkaW5nLXRvcDogMTAwcHg7XG4gIC8qIExvY2F0aW9uIG9mIHRoZSBib3ggKi9cbiAgbGVmdDogMDtcbiAgdG9wOiAwO1xuICB3aWR0aDogMTAwJTtcbiAgLyogRnVsbCB3aWR0aCAqL1xuICBoZWlnaHQ6IDEwMCU7XG4gIC8qIEZ1bGwgaGVpZ2h0ICovXG4gIG92ZXJmbG93OiBhdXRvO1xuICAvKiBFbmFibGUgc2Nyb2xsIGlmIG5lZWRlZCAqL1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcbiAgLyogRmFsbGJhY2sgY29sb3IgKi9cbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjQpO1xuICAvKiBCbGFjayB3LyBvcGFjaXR5ICovXG59XG5cbi5tb2RhbC50cnVlIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIC8qIEhpZGRlbiBieSBkZWZhdWx0ICovXG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgLyogU3RheSBpbiBwbGFjZSAqL1xuICB6LWluZGV4OiAxO1xuICAvKiBTaXQgb24gdG9wICovXG4gIHBhZGRpbmctdG9wOiAxMDBweDtcbiAgLyogTG9jYXRpb24gb2YgdGhlIGJveCAqL1xuICBsZWZ0OiAxMDBweDtcbiAgdG9wOiAwO1xuICB3aWR0aDogMTAwJTtcbiAgLyogRnVsbCB3aWR0aCAqL1xuICBoZWlnaHQ6IDEwMCU7XG4gIC8qIEZ1bGwgaGVpZ2h0ICovXG4gIG92ZXJmbG93OiBhdXRvO1xuICAvKiBFbmFibGUgc2Nyb2xsIGlmIG5lZWRlZCAqL1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcbiAgLyogRmFsbGJhY2sgY29sb3IgKi9cbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjQpO1xuICAvKiBCbGFjayB3LyBvcGFjaXR5ICovXG59XG5cbi8qIE1vZGFsIEhlYWRlciAqL1xuLm1vZGFsLWhlYWRlciB7XG4gIHBhZGRpbmc6IDJweCAxNnB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNWNiODVjO1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi8qIE1vZGFsIEJvZHkgKi9cbi5tb2RhbC1ib2R5IHtcbiAgcGFkZGluZzogMnB4IDE2cHg7XG59XG5cbi8qIE1vZGFsIEZvb3RlciAqL1xuLm1vZGFsLWZvb3RlciB7XG4gIHBhZGRpbmc6IDJweCAxNnB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNWNiODVjO1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi8qIE1vZGFsIENvbnRlbnQgKi9cbi5tb2RhbC1jb250ZW50IHtcbiAgbWFyZ2luLWxlZnQ6IDI1NnB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZWZlZmU7XG4gIG1hcmdpbjogYXV0bztcbiAgcGFkZGluZzogMDtcbiAgYm9yZGVyOiAxcHggc29saWQgIzg4ODtcbiAgd2lkdGg6IDUwJTtcbiAgYm94LXNoYWRvdzogMCA0cHggOHB4IDAgcmdiYSgwLCAwLCAwLCAwLjIpLCAwIDZweCAyMHB4IDAgcmdiYSgwLCAwLCAwLCAwLjE5KTtcbiAgYW5pbWF0aW9uLW5hbWU6IGFuaW1hdGV0b3A7XG4gIGFuaW1hdGlvbi1kdXJhdGlvbjogMC40cztcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4vKiBUaGUgQ2xvc2UgQnV0dG9uICovXG4uY2xvc2Uge1xuICBjb2xvcjogd2hpdGU7XG4gIGZsb2F0OiByaWdodDtcbiAgZm9udC1zaXplOiAyOHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuLmNsb3NlOmhvdmVyLFxuLmNsb3NlOmZvY3VzIHtcbiAgY29sb3I6ICMwMDA7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG4vKiBBZGQgQW5pbWF0aW9uICovXG5Aa2V5ZnJhbWVzIGFuaW1hdGV0b3Age1xuICBmcm9tIHtcbiAgICB0b3A6IC0zMDBweDtcbiAgICBvcGFjaXR5OiAwO1xuICB9XG4gIHRvIHtcbiAgICB0b3A6IDA7XG4gICAgb3BhY2l0eTogMTtcbiAgfVxufVxuLmJ0bi1jb2xvciB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgd2lkdGg6IGF1dG87XG4gIHBhZGRpbmc6IDVweCAxMnB4O1xuICBoZWlnaHQ6IDUwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIGJhY2tncm91bmQ6IGxpbWVncmVlbjtcbn1cblxuI2lucHV0YW4ge1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICB3aWR0aDogODAlO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/merchant-portal/separated-modules/merchant-products/products/evoucher-list/evoucher-list.component.ts":
/*!******************************************************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/separated-modules/merchant-products/products/evoucher-list/evoucher-list.component.ts ***!
  \******************************************************************************************************************************/
/*! exports provided: EvoucherListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EvoucherListComponent", function() { return EvoucherListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../services/e-voucher/e-voucher.service */ "./src/app/services/e-voucher/e-voucher.service.ts");
/* harmony import */ var _object_interface_common_object__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../object-interface/common.object */ "./src/app/object-interface/common.object.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var EvoucherListComponent = /** @class */ (function () {
    function EvoucherListComponent(route, router, evoucherService, modalService) {
        this.route = route;
        this.router = router;
        this.evoucherService = evoucherService;
        this.modalService = modalService;
        this.generatedVouchers = [];
        this.errorLabel = false;
        this.popUpForm = false;
        this.data = [];
        this.inputedVoucher = 0;
        this.approveAll = false;
        this.service = this.evoucherService;
        this.tableFormat = Object(_object_interface_common_object__WEBPACK_IMPORTED_MODULE_3__["eVouchersTableFormat"])(this);
        // this.tableFormat.label_headers.splice(0,1)
        this.tableFormat.formOptions.addForm = false;
        this.tableFormat.formOptions.customButtons;
    }
    EvoucherListComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.inLoadedProduct();
                return [2 /*return*/];
            });
        });
    };
    EvoucherListComponent.prototype.inLoadedProduct = function () {
        var _this = this;
        // console.log(this.permission.getCurrentPermission());
        this.route.queryParams.subscribe(function (params) { return __awaiter(_this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.productID = params.id;
                        return [4 /*yield*/, this.evoucherService.getEvoucherStockSummary({ id: params.id })];
                    case 1:
                        result = _a.sent();
                        console.log(result);
                        this.totalPage = result.result.total_page;
                        this.generatedVouchers = result.result.values;
                        console.log(this.generatedVouchers);
                        this.evoucherDetail = result.result.values._id;
                        return [2 /*return*/];
                }
            });
        }); });
        // try{
        //   let rQuery = { 
        //         column_request :"",
        //         // search: {'type':'product'}
        //       }
        //   let request = JSON.stringify(rQuery);
        //   let queryParams = {
        //         request: request            
        //   }
        //   let result: any;
        //   this.service    = this.evoucherService;
        //   // result          = await this.evoucherService.getProductsReport(queryParams);
        // } catch (e) {
        //   this.errorLabel = ((<Error>e).message);//conversion to Error type
        // }
    };
    EvoucherListComponent.prototype.approveAllClicked = function (data) {
        this.approveAll = true;
        this.approval(data);
    };
    EvoucherListComponent.prototype.approval = function (evoucherData) {
        return __awaiter(this, void 0, void 0, function () {
            var totalVoucher, expiry_date, month, day, stringDate, payloadData, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(evoucherData);
                        if (evoucherData == undefined) {
                            return [2 /*return*/, false];
                        }
                        expiry_date = new Date(evoucherData.expiry_date);
                        month = (expiry_date.getMonth() + 1).toString().padStart(2, "0");
                        day = (expiry_date.getDate() + 0).toString().padStart(2, "0");
                        stringDate = expiry_date.getFullYear() + '-' + month + '-' + day;
                        if (this.inputedVoucher != 0 && this.approveAll == false) {
                            totalVoucher = this.inputedVoucher;
                        }
                        else if (this.approveAll == true) {
                            this.approveAll = false;
                            totalVoucher = evoucherData.qty_inactive;
                        }
                        payloadData = {
                            product_code: evoucherData.ev_product_code,
                            expiry_date: stringDate,
                            qty: totalVoucher,
                            method: 2
                        };
                        this.service = this.evoucherService;
                        return [4 /*yield*/, this.evoucherService.approvedEvoucher(payloadData)];
                    case 1:
                        result = _a.sent();
                        if (result.result.status == 'success') {
                            alert("the Current Data Below has been approved");
                            this.inLoadedProduct();
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    EvoucherListComponent.prototype.disabled = function (evoucherData) {
        return __awaiter(this, void 0, void 0, function () {
            var totalVoucher, expiry_date, month, day, stringDate, payloadData, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(evoucherData);
                        if (evoucherData == undefined) {
                            return [2 /*return*/, false];
                        }
                        expiry_date = new Date(evoucherData.expiry_date);
                        month = (expiry_date.getMonth() + 1).toString().padStart(2, "0");
                        day = (expiry_date.getDate() + 0).toString().padStart(2, "0");
                        stringDate = expiry_date.getFullYear() + '-' + month + '-' + day;
                        if (this.inputedVoucher != 0) {
                            totalVoucher = this.inputedVoucher;
                        }
                        else if (this.inputedVoucher == 0) {
                            totalVoucher = evoucherData.qty_inactive;
                        }
                        console.log(this.inputedVoucher);
                        payloadData = {
                            product_code: evoucherData.ev_product_code,
                            expiry_date: stringDate,
                            qty: totalVoucher,
                            method: 1
                        };
                        console.log("payload", payloadData);
                        this.service = this.evoucherService;
                        return [4 /*yield*/, this.evoucherService.approvedEvoucher(payloadData)];
                    case 1:
                        result = _a.sent();
                        if (result.result.status == 'success') {
                            alert("The stock has been inactive");
                            this.inLoadedProduct();
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    EvoucherListComponent.prototype.openDetail = function (evoucher) {
        this.data = evoucher;
        console.log("result", evoucher);
        this.popUpForm = true;
    };
    EvoucherListComponent.prototype.popUpClose = function () {
        this.popUpForm = false;
    };
    EvoucherListComponent.prototype.callDetail = function (_id, evoucherData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.openDetail(evoucherData);
                return [2 /*return*/];
            });
        });
    };
    EvoucherListComponent.prototype.evoucherStatus = function (listedData) {
        return __awaiter(this, void 0, void 0, function () {
            var listOfData;
            return __generator(this, function (_a) {
                if (listedData == undefined || listedData.length == 0) {
                    return [2 /*return*/, false];
                }
                listOfData = [];
                listedData.forEach(function (element, index) {
                    listOfData.push(element._id);
                });
                listedData = [];
                if (listOfData.length == 0)
                    return [2 /*return*/, false];
                console.log("listdata", listOfData);
                return [2 /*return*/];
            });
        });
    };
    EvoucherListComponent.prototype.open = function (content) {
        var _this = this;
        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    EvoucherListComponent.prototype.getDismissReason = function (reason) {
        if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["ModalDismissReasons"].ESC) {
            return 'by pressing ESC';
        }
        else if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["ModalDismissReasons"].BACKDROP_CLICK) {
            return 'by clicking backdrop';
        }
        else {
            return '';
        }
    };
    EvoucherListComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] },
        { type: _services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_2__["EVoucherService"] },
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModal"] }
    ]; };
    EvoucherListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-evoucher-list',
            template: __webpack_require__(/*! raw-loader!./evoucher-list.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/separated-modules/merchant-products/products/evoucher-list/evoucher-list.component.html"),
            styles: [__webpack_require__(/*! ./evoucher-list.component.scss */ "./src/app/layout/merchant-portal/separated-modules/merchant-products/products/evoucher-list/evoucher-list.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_2__["EVoucherService"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModal"]])
    ], EvoucherListComponent);
    return EvoucherListComponent;
}());



/***/ }),

/***/ "./src/app/layout/merchant-portal/separated-modules/merchant-products/products/merchant-add-new-product/merchant-add-new-product.component.scss":
/*!******************************************************************************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/separated-modules/merchant-products/products/merchant-add-new-product/merchant-add-new-product.component.scss ***!
  \******************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host ::ng-deep .ck-editor__editable_inline {\n  min-height: 500px;\n}\n\n#buttonoption {\n  margin-left: -10px;\n}\n\n#tooltip #tooltiptext {\n  visibility: hidden;\n  width: 120px;\n  background-color: black;\n  color: #fff;\n  text-align: center;\n  border-radius: 6px;\n  padding: 5px 0;\n  /* Position the tooltip */\n  position: absolute;\n  z-index: 1;\n}\n\n#tooltip:hover #tooltiptext {\n  visibility: visible;\n}\n\n.back_button {\n  margin-top: 12px;\n  margin-left: 40px;\n}\n\n.form {\n  padding: 25px;\n  position: relative;\n}\n\n.form label {\n  color: #757474;\n}\n\n.form .content-title-container {\n  margin-bottom: 30px;\n}\n\n.form .content-title-container .content-title {\n  font-size: 20px;\n  font-weight: bold;\n  letter-spacing: 0.8px;\n}\n\n.form .content-title-container .content-title .custom-icon {\n  margin: 0px 10px;\n}\n\n.form .content-title-container .content-title .span-2 {\n  color: #2480fb;\n  font-weight: bold;\n}\n\n.form input[type=file] {\n  display: none;\n}\n\n.form .hashhash .hashtag-item {\n  display: inline-block;\n  margin-right: 5px;\n  background: #eee;\n  padding: 2px 5px;\n  margin-bottom: 5px;\n  border-radius: 5px;\n}\n\n.form .hashhash .hashtag-item::before {\n  content: \"#\";\n  display: inline;\n}\n\n.form .hashhash .hashtagskey-input {\n  display: inline-block;\n  min-width: 75px;\n  margin-bottom: 5px;\n  font-size: 1rem;\n  background: white;\n  border-radius: 5px;\n  outline: none;\n}\n\n.form .hashhash .hashtagskey-input::before {\n  content: \"#\";\n  display: inline;\n}\n\n.form .uploading-file {\n  opacity: 0.5;\n  -webkit-filter: grayscale(1);\n          filter: grayscale(1);\n}\n\n.form .uploading-file.true {\n  opacity: 1;\n  -webkit-filter: grayscale(0);\n          filter: grayscale(0);\n}\n\n.form .autoSKU {\n  font-size: 12px;\n  margin-left: 15px;\n  display: inline-block;\n}\n\n.form .image-upload {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  flex-direction: column;\n  border: 3px solid #ebebeb;\n  border-radius: 10px;\n  background-color: #f5f5f5;\n  min-height: 125px;\n  margin: 20px;\n  padding: 50px;\n}\n\n.form .image-upload .logo-upload {\n  width: 150px;\n}\n\n.form .image-upload .fileupload {\n  background-color: #2480fb;\n  color: white;\n  border-radius: 8px;\n  font-size: 14px;\n  padding: 12px 22px;\n}\n\n.form .image-upload .upload-desc {\n  color: #d6d6d6 !important;\n  text-align: center;\n  font-size: 18px;\n}\n\n.form .uploaded-image {\n  margin: 0px auto;\n  width: auto;\n  padding: 50px;\n  height: auto;\n  cursor: pointer;\n}\n\n.form .uploaded-image .text-fileupload {\n  display: block;\n  text-align: center;\n  cursor: pointer;\n  color: #2480fb;\n  margin-top: 10px;\n}\n\n.form .uploaded-image .img {\n  width: 100%;\n  min-height: 348px;\n  background-size: cover;\n  position: relative;\n  background-position: center center;\n  background-repeat: no-repeat;\n}\n\n.form .uploaded-image .img .editor-tool {\n  background: rgba(0, 0, 0, 0.2);\n  width: 100%;\n  height: 100%;\n  position: relative;\n  opacity: 0;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n.form .uploaded-image .img:hover .editor-tool {\n  opacity: 0.9;\n}\n\n.form .status-form.false {\n  background-color: #F2F2F2;\n  color: #bcbcbc;\n}\n\n.form .custom-form-right {\n  align-items: flex-end;\n  display: flex;\n  justify-content: flex-end;\n}\n\n.form .custom-price-column {\n  border-left: 1px solid #e2e2e2;\n}\n\n.form .price-column {\n  display: flex;\n  flex-direction: row;\n}\n\n.form .price-column p {\n  margin: 5px 10px 5px 0px;\n}\n\n.form .custom-border {\n  border-left: 1px solid #e2e2e2;\n  border-right: 1px solid #e2e2e2;\n}\n\n.form .dimension {\n  border-radius: 0.25rem;\n  margin-right: 10px;\n}\n\n.form .product-type-list {\n  display: inline-block;\n  align-items: center;\n}\n\n.form .product-type-list .product-type {\n  display: inline-flex;\n  padding: 11px 25px;\n  justify-content: center;\n  align-items: center;\n  height: 100%;\n  border-radius: 50px;\n  cursor: pointer;\n  border: 1px solid #9f9f9f;\n  margin-right: 10px;\n  margin-left: 10px;\n  margin-bottom: 5px;\n  transition: ease 0.2s all 0s;\n  color: #9f9f9f;\n  font-size: 14px;\n}\n\n.form .product-type-list .product-type.true {\n  color: white;\n  background-color: #2480fb;\n  border-color: #2980b9;\n}\n\n.form .form-control.des {\n  padding-top: 4px;\n  display: inline-flex;\n  padding-bottom: 4px;\n  justify-content: center;\n  align-items: center;\n}\n\n.form .form-control.des input {\n  border: 0px solid transparent;\n  width: calc(100% - 15px);\n}\n\n.form .form-control.des input:focus {\n  outline: none;\n}\n\n.form div.error {\n  background: #e74c3c;\n  color: white;\n  padding: 25px;\n  margin-bottom: 25px;\n}\n\n.form .navbar-menu {\n  overflow-y: scroll;\n  max-height: 250px;\n  margin: 15px 5px;\n}\n\n.form .navbar-menu .category-list .list-group-item {\n  border: none;\n  cursor: pointer;\n}\n\n.form .navbar-menu .category-list .list-group-item .checked-icon {\n  display: inline-flex;\n  justify-content: center;\n  align-items: center;\n  width: 20px;\n  height: 20px;\n  border-radius: 15px;\n  border: 1px solid #ccc;\n  color: #3498db;\n  font-weight: bold;\n  font-size: 14px;\n  margin-right: 10px;\n}\n\n.form .navbar-menu .category-list .list-group-item .checked-icon.false {\n  color: transparent;\n}\n\n.form .header-first {\n  margin: 30px;\n  border-radius: 18px;\n  background-color: #ffffff;\n}\n\n.form .left-section-upload {\n  justify-content: center !important;\n}\n\n.form .upload-container {\n  padding: 20px;\n  border-radius: 18px;\n  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.06);\n  background-color: #ffffff;\n  margin-bottom: 10px;\n}\n\n.form .upload-container .cl-list .img {\n  overflow: hidden;\n}\n\n.form .col-md-8 .form-group {\n  display: flex;\n  justify-content: space-between;\n  align-items: flex-start;\n}\n\n.form .col-md-8 .form-group .label {\n  width: 38%;\n}\n\n.form .col-md-6 .form-group {\n  display: flex;\n  justify-content: space-between;\n  align-items: flex-start;\n}\n\n.form .col-md-6 .form-group .label {\n  width: 60%;\n}\n\n.form .col-md-6 .form-group select.form-control:not([size]):not([multiple]) {\n  height: unset;\n}\n\n.form .section-upload {\n  width: 100%;\n  align-items: center;\n}\n\n.form .section-upload .col-2dot4,\n.form .section-upload .col-sm-2dot4,\n.form .section-upload .col-md-2dot4,\n.form .section-upload .col-lg-2dot4,\n.form .section-upload .col-xl-2dot4 {\n  position: relative;\n  width: 100%;\n  min-height: 1px;\n  padding-right: 15px;\n  padding-left: 15px;\n}\n\n.form .section-upload .col-2dot4 {\n  flex: 0 0 20%;\n  max-width: 20%;\n}\n\n@media (min-width: 540px) {\n  .form .section-upload .col-sm-2dot4 {\n    flex: 0 0 20%;\n    max-width: 20%;\n  }\n}\n\n@media (min-width: 720px) {\n  .form .section-upload .col-md-2dot4 {\n    flex: 0 0 20%;\n    max-width: 20%;\n  }\n}\n\n@media (min-width: 960px) {\n  .form .section-upload .col-lg-2dot4 {\n    flex: 0 0 20%;\n    max-width: 20%;\n  }\n}\n\n@media (min-width: 1140px) {\n  .form .section-upload .col-xl-2dot4 {\n    flex: 0 0 20%;\n    max-width: 20%;\n  }\n}\n\n.form .section-upload .col-md-2dot4 {\n  text-align: center;\n  font-size: 14px;\n}\n\n.form .section-upload .col-md-2dot4 .blue-upload {\n  color: #56a4ff;\n  font-size: 16px;\n  font-weight: bold;\n}\n\n.form .section-upload .col-md-2dot4 .card {\n  margin-top: 3vh;\n  height: 190px;\n  border: none;\n}\n\n.form .section-upload .col-md-2dot4 .card .card-content {\n  height: 100%;\n  position: relative;\n}\n\n.form .section-upload .col-md-2dot4 .card .card-content .upload-desc {\n  color: #bbb !important;\n  text-align: center;\n  font-size: 14px;\n}\n\n.form .section-upload .col-md-2dot4 .card .card-content .logo-upload {\n  width: 100%;\n  background: white;\n}\n\n.form .section-upload .col-md-2dot4 .card .card-content .image {\n  display: block;\n  float: left;\n  height: auto;\n  width: 100%;\n  height: 100%;\n}\n\n.form .section-upload .col-md-2dot4 .card .card-content .image .custom {\n  height: 100%;\n  border-radius: 10px;\n  justify-content: center;\n  align-items: center;\n  display: flex;\n  flex-direction: column;\n}\n\n.form .section-upload .col-md-2dot4 .card .card-content .image .custom .drag-drop {\n  font-size: 10px;\n  text-align: center;\n}\n\n.form .section-upload .col-md-2dot4 .card .card-content .image .custom .set-opacity {\n  opacity: 0.1;\n  height: 60px;\n  width: auto;\n}\n\n.form .section-upload .col-md-2dot4 .card .form-group.uploaded {\n  position: relative;\n  margin-bottom: 0px;\n  height: 100%;\n}\n\n.form .section-upload .col-md-2dot4 .card .form-group.uploaded .loading {\n  height: 100%;\n}\n\n.form .section-upload .col-md-2dot4 .card .form-group.uploaded .loading .sk-fading-circle {\n  top: 50%;\n}\n\n.form .section-upload .col-md-2dot4 .card .form-group.uploaded .uploaded-image2 {\n  height: 100%;\n}\n\n.form .section-upload .col-md-2dot4 .card .form-group.uploaded .uploaded-image2 .img {\n  padding: 15px;\n  height: 100%;\n  width: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  flex-direction: column;\n}\n\n.form .section-upload .col-md-2dot4 .card .form-group.uploaded .uploaded-image2 .img > img {\n  height: 100%;\n  max-height: 320px;\n}\n\n.form .section-upload .col-md-2dot4 .card .form-group.uploaded .uploaded-image2 .editor-tool {\n  height: 100%;\n}\n\n.form .section-upload .col-md-2dot4 {\n  text-align: center;\n}\n\n.form .section-upload .col-md-2dot4 .card {\n  height: 250px;\n  border: none;\n  border-radius: 8px;\n  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.05);\n  background-color: #ffffff;\n}\n\n.form .section-upload .col-md-2dot4 .card .card-content {\n  height: 100%;\n  position: relative;\n}\n\n.form .section-upload .col-md-2dot4 .card .card-content .image-canvas {\n  width: 80%;\n}\n\n.form .section-upload .col-md-2dot4 .card .card-content .upload-desc {\n  color: #bbb !important;\n  text-align: center;\n  font-size: 14px;\n}\n\n.form .section-upload .col-md-2dot4 .card .card-content .logo-upload {\n  width: 100%;\n  background: white;\n}\n\n.form .section-upload .col-md-2dot4 .card .card-content .image {\n  display: block;\n  float: left;\n  height: auto;\n  width: 100%;\n  height: 100%;\n}\n\n.form .section-upload .col-md-2dot4 .card .card-content .image .custom {\n  height: 100%;\n  border-radius: 10px;\n  justify-content: center;\n  align-items: center;\n  display: flex;\n  flex-direction: column;\n}\n\n.form .section-upload .col-md-2dot4 .card .card-content .image .custom .drag-drop {\n  font-size: 10px;\n  text-align: center;\n}\n\n.form .section-upload .col-md-2dot4 .card .card-content .image .custom .set-opacity {\n  opacity: 0.1;\n  height: 60px;\n  width: auto;\n}\n\n.form .section-upload .col-md-2dot4 .card .form-group.uploaded {\n  position: relative;\n  margin-bottom: 0px;\n  height: 100%;\n}\n\n.form .section-upload .col-md-2dot4 .card .form-group.uploaded .remove-icon {\n  position: absolute;\n  right: 0;\n  top: 0;\n  color: red;\n  font-size: 22px;\n}\n\n.form .section-upload .col-md-2dot4 .card .form-group.uploaded .loading {\n  height: 100%;\n}\n\n.form .section-upload .col-md-2dot4 .card .form-group.uploaded .loading .sk-fading-circle {\n  top: 50%;\n}\n\n.form .section-upload .col-md-2dot4 .card .form-group.uploaded .uploaded-image2 {\n  height: 100%;\n}\n\n.form .section-upload .col-md-2dot4 .card .form-group.uploaded .uploaded-image2 .img {\n  height: 100%;\n  width: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n.form .section-upload .col-md-2dot4 .card .form-group.uploaded .uploaded-image2 .img > img {\n  height: 100%;\n  max-height: 320px;\n}\n\n.form .section-upload .col-md-2dot4 .card .form-group.uploaded .uploaded-image2 .editor-tool {\n  height: 100%;\n}\n\n.form .card {\n  box-shadow: 0 1px 11px 0 rgba(0, 0, 0, 0.2), 0 2px 3px 0 rgba(0, 0, 0, 0.19);\n  margin-bottom: 20px;\n}\n\n.form .card .card-header {\n  background-color: white;\n  margin: 0px;\n  padding: 0px;\n  border-left: 3px solid #2480fb;\n}\n\n.form .card .card-header label {\n  margin: 20px 10px;\n  font-weight: bold;\n  font-size: 20px;\n}\n\n.form .card .uploaded {\n  border-top: 1px solid #ccc;\n}\n\n.form .end-footer {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n}\n\n.form .end-footer .footer-save-cancel {\n  margin: 50px;\n}\n\n.form .end-footer .footer-save-cancel .cancel {\n  border-color: #2480fb;\n  margin-right: 20px;\n  color: #2480fb;\n  background-color: white;\n  font-size: 20px;\n  padding: 15px;\n  width: 25rem;\n  border-radius: 10px;\n}\n\n.form .end-footer .footer-save-cancel .save {\n  font-size: 20px;\n  padding: 15px;\n  width: 25rem;\n  border-radius: 10px;\n  background-color: #2480fb;\n}\n\n.form .end-footer .delete {\n  position: relative;\n  float: right;\n  background-color: #e74c3c;\n  opacity: 0.5;\n  border-color: #c0392b;\n}\n\n.form .end-footer .delete .delete-modal {\n  position: absolute;\n  display: none;\n  background: white;\n  border: 1px solid #ccc;\n  border-radius: 10px;\n  padding: 4px 10px;\n  top: 0px;\n  right: 0px;\n  color: #e74c3c;\n  -webkit-transform: translateY(-45px);\n          transform: translateY(-45px);\n}\n\n.form .end-footer .delete .delete-modal .btn-group {\n  border: 1px solid #e74c3c;\n  border-radius: 5px;\n  overflow: hidden;\n}\n\n.form .end-footer .delete .delete-modal .btn-group .no {\n  opacity: 0.5;\n  border-width: 0px;\n}\n\n.form .end-footer .delete .delete-modal .btn-group .yes {\n  background: #e74c3c;\n  border-width: 0px;\n}\n\n.form .end-footer .delete .delete-modal .btn-group .no:hover {\n  opacity: 1;\n}\n\n.form .end-footer .delete:hover,\n.form .end-footer .delete:focus {\n  opacity: 1;\n}\n\n.form .end-footer .delete:focus .delete-modal {\n  display: block;\n}\n\n.form .varian-button {\n  cursor: pointer;\n}\n\n.form .varian-image-parent {\n  position: relative;\n}\n\n.form .varian-table {\n  margin-top: 40px;\n}\n\n.form .delete-varian-image {\n  position: absolute;\n  right: 22px;\n  top: 22px;\n  color: white;\n  font-size: 24px;\n  cursor: pointer;\n}\n\n.form .varian-parent {\n  position: relative;\n}\n\n.form .varian-parent ::ng-deep .input-group {\n  width: 91%;\n}\n\n.form .varian-parent i {\n  position: absolute;\n  right: 20px;\n  top: 10px;\n}\n\n.form .add-value {\n  margin-bottom: 20px;\n  margin-top: 5px;\n}\n\n.form .red-alert {\n  border: solid 1px red;\n}\n\n.form .red-error-alert {\n  color: red;\n  font-size: 12px;\n}\n\n.error-message {\n  background: white;\n  height: 100vh;\n  text-align: center;\n  font-size: 26px;\n  padding-top: 40px;\n}\n\n.error-message .text-message {\n  background: #E74C3C;\n  padding: 20px;\n  margin: 0 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21lcmNoYW50LXBvcnRhbC9zZXBhcmF0ZWQtbW9kdWxlcy9tZXJjaGFudC1wcm9kdWN0cy9wcm9kdWN0cy9tZXJjaGFudC1hZGQtbmV3LXByb2R1Y3QvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxtZXJjaGFudC1wb3J0YWxcXHNlcGFyYXRlZC1tb2R1bGVzXFxtZXJjaGFudC1wcm9kdWN0c1xccHJvZHVjdHNcXG1lcmNoYW50LWFkZC1uZXctcHJvZHVjdFxcbWVyY2hhbnQtYWRkLW5ldy1wcm9kdWN0LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbWVyY2hhbnQtcG9ydGFsL3NlcGFyYXRlZC1tb2R1bGVzL21lcmNoYW50LXByb2R1Y3RzL3Byb2R1Y3RzL21lcmNoYW50LWFkZC1uZXctcHJvZHVjdC9tZXJjaGFudC1hZGQtbmV3LXByb2R1Y3QuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxpQkFBQTtBQ0NKOztBREVBO0VBQ0ksa0JBQUE7QUNDSjs7QURHQTtFQUNJLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLHVCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0VBRUEseUJBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7QUNESjs7QURJQTtFQUNJLG1CQUFBO0FDREo7O0FESUE7RUFDSSxnQkFBQTtFQUNBLGlCQUFBO0FDREo7O0FES0E7RUFDSSxhQUFBO0VBQ0Esa0JBQUE7QUNGSjs7QURLSTtFQUNJLGNBQUE7QUNIUjs7QURLSTtFQUNJLG1CQUFBO0FDSFI7O0FESVE7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxxQkFBQTtBQ0ZaOztBREtZO0VBQ0ksZ0JBQUE7QUNIaEI7O0FETVk7RUFDSSxjQUFBO0VBQ0EsaUJBQUE7QUNKaEI7O0FEVUk7RUFDSSxhQUFBO0FDUlI7O0FEWVE7RUFDSSxxQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUNWWjs7QURhUTtFQUNJLFlBQUE7RUFDQSxlQUFBO0FDWFo7O0FEY1E7RUFDSSxxQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtBQ1paOztBRGVRO0VBQ0ksWUFBQTtFQUNBLGVBQUE7QUNiWjs7QURpQkk7RUFDSSxZQUFBO0VBQ0EsNEJBQUE7VUFBQSxvQkFBQTtBQ2ZSOztBRGtCSTtFQUNJLFVBQUE7RUFDQSw0QkFBQTtVQUFBLG9CQUFBO0FDaEJSOztBRG1CSTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLHFCQUFBO0FDakJSOztBRG9CSTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSxpQkFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0FDbEJSOztBRG9CUTtFQUNJLFlBQUE7QUNsQlo7O0FEc0JRO0VBQ0kseUJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QUNwQlo7O0FEdUJRO0VBQ0kseUJBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7QUNyQlo7O0FEeUJJO0VBQ0ksZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0FDdkJSOztBRHlCUTtFQUNJLGNBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7QUN2Qlo7O0FEMkJRO0VBQ0ksV0FBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtFQUNBLGtDQUFBO0VBQ0EsNEJBQUE7QUN6Qlo7O0FEMkJZO0VBQ0ksOEJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FDekJoQjs7QUQ2QlE7RUFDSSxZQUFBO0FDM0JaOztBRCtCSTtFQUNJLHlCQUFBO0VBQ0EsY0FBQTtBQzdCUjs7QURnQ0k7RUFDSSxxQkFBQTtFQUNBLGFBQUE7RUFDQSx5QkFBQTtBQzlCUjs7QURpQ0k7RUFDSSw4QkFBQTtBQy9CUjs7QURrQ0k7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7QUNoQ1I7O0FEa0NRO0VBQ0ksd0JBQUE7QUNoQ1o7O0FEb0NJO0VBQ0ksOEJBQUE7RUFDQSwrQkFBQTtBQ2xDUjs7QURxQ0k7RUFDSSxzQkFBQTtFQUNBLGtCQUFBO0FDbkNSOztBRHNDSTtFQUNJLHFCQUFBO0VBQ0EsbUJBQUE7QUNwQ1I7O0FEc0NRO0VBQ0ksb0JBQUE7RUFDQSxrQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSw0QkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0FDcENaOztBRHVDUTtFQUNJLFlBQUE7RUFDQSx5QkFBQTtFQUNBLHFCQUFBO0FDckNaOztBRHlDSTtFQUNJLGdCQUFBO0VBQ0Esb0JBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUN2Q1I7O0FEeUNRO0VBQ0ksNkJBQUE7RUFDQSx3QkFBQTtBQ3ZDWjs7QUQwQ1E7RUFDSSxhQUFBO0FDeENaOztBRDRDSTtFQUNJLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtBQzFDUjs7QUQ2Q0k7RUFDSSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7QUMzQ1I7O0FEOENZO0VBQ0ksWUFBQTtFQUNBLGVBQUE7QUM1Q2hCOztBRDhDZ0I7RUFDSSxvQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QUM1Q3BCOztBRCtDZ0I7RUFDSSxrQkFBQTtBQzdDcEI7O0FEbURJO0VBQ0ksWUFBQTtFQUVBLG1CQUFBO0VBQ0EseUJBQUE7QUNsRFI7O0FEcURJO0VBQ0ksa0NBQUE7QUNuRFI7O0FEc0RJO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsMkNBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0FDcERSOztBRHVEWTtFQUNJLGdCQUFBO0FDckRoQjs7QUQyRFE7RUFDSSxhQUFBO0VBQ0EsOEJBQUE7RUFDQSx1QkFBQTtBQ3pEWjs7QUQwRFk7RUFDSSxVQUFBO0FDeERoQjs7QUQ2RFE7RUFDSSxhQUFBO0VBQ0EsOEJBQUE7RUFDQSx1QkFBQTtBQzNEWjs7QUQ0RFk7RUFDSSxVQUFBO0FDMURoQjs7QUQ0RFk7RUFDSSxhQUFBO0FDMURoQjs7QUQrREk7RUFFSSxXQUFBO0VBRUEsbUJBQUE7QUMvRFI7O0FEaUVROzs7OztFQUtJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FDL0RaOztBRGtFUTtFQUdJLGFBQUE7RUFDQSxjQUFBO0FDaEVaOztBRG1FUTtFQUNJO0lBR0ksYUFBQTtJQUNBLGNBQUE7RUNqRWQ7QUFDRjs7QURvRVE7RUFDSTtJQUdJLGFBQUE7SUFDQSxjQUFBO0VDbEVkO0FBQ0Y7O0FEcUVRO0VBQ0k7SUFHSSxhQUFBO0lBQ0EsY0FBQTtFQ25FZDtBQUNGOztBRHNFUTtFQUNJO0lBR0ksYUFBQTtJQUNBLGNBQUE7RUNwRWQ7QUFDRjs7QURzRVE7RUFDSSxrQkFBQTtFQUNBLGVBQUE7QUNwRVo7O0FEdUVZO0VBQ0ksY0FBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQ3JFaEI7O0FEd0VZO0VBQ0ksZUFBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0FDdEVoQjs7QUR3RWdCO0VBQ0ksWUFBQTtFQUNBLGtCQUFBO0FDdEVwQjs7QUR3RW9CO0VBQ0ksc0JBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7QUN0RXhCOztBRHlFb0I7RUFDSSxXQUFBO0VBQ0EsaUJBQUE7QUN2RXhCOztBRDBFb0I7RUFDSSxjQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQ3hFeEI7O0FEMEV3QjtFQUNJLFlBQUE7RUFFQSxtQkFBQTtFQUVBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7QUMxRTVCOztBRDZFNEI7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7QUMzRWhDOztBRDhFNEI7RUFDSSxZQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7QUM1RWhDOztBRGtGZ0I7RUFDSSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtBQ2hGcEI7O0FEa0ZvQjtFQUNJLFlBQUE7QUNoRnhCOztBRGtGd0I7RUFDSSxRQUFBO0FDaEY1Qjs7QURvRm9CO0VBQ0ksWUFBQTtBQ2xGeEI7O0FEb0Z3QjtFQUNJLGFBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7QUNsRjVCOztBRHFGd0I7RUFDSSxZQUFBO0VBQ0EsaUJBQUE7QUNuRjVCOztBRHNGd0I7RUFDSSxZQUFBO0FDcEY1Qjs7QUQ0RlE7RUFDSSxrQkFBQTtBQzFGWjs7QUQ0Rlk7RUFFSSxhQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsMkNBQUE7RUFDQSx5QkFBQTtBQzNGaEI7O0FEOEZnQjtFQUNJLFlBQUE7RUFDQSxrQkFBQTtBQzVGcEI7O0FEOEZvQjtFQUNJLFVBQUE7QUM1RnhCOztBRCtGb0I7RUFDSSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQzdGeEI7O0FEZ0dvQjtFQUNJLFdBQUE7RUFDQSxpQkFBQTtBQzlGeEI7O0FEaUdvQjtFQUNJLGNBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FDL0Z4Qjs7QURpR3dCO0VBQ0ksWUFBQTtFQUVBLG1CQUFBO0VBQ0EsdUJBQUE7RUFFQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtBQ2pHNUI7O0FEb0c0QjtFQUNJLGVBQUE7RUFDQSxrQkFBQTtBQ2xHaEM7O0FEcUc0QjtFQUNJLFlBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQ25HaEM7O0FEeUdnQjtFQUNJLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0FDdkdwQjs7QUR3R29CO0VBQ0ksa0JBQUE7RUFDQSxRQUFBO0VBQ0EsTUFBQTtFQUNBLFVBQUE7RUFDQSxlQUFBO0FDdEd4Qjs7QUR3R29CO0VBQ0ksWUFBQTtBQ3RHeEI7O0FEd0d3QjtFQUNJLFFBQUE7QUN0RzVCOztBRDBHb0I7RUFDSSxZQUFBO0FDeEd4Qjs7QUQwR3dCO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQ3hHNUI7O0FEMkd3QjtFQUNJLFlBQUE7RUFDQSxpQkFBQTtBQ3pHNUI7O0FENEd3QjtFQUNJLFlBQUE7QUMxRzVCOztBRGtISTtFQUNJLDRFQUFBO0VBQ0EsbUJBQUE7QUNoSFI7O0FEa0hRO0VBQ0ksdUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLDhCQUFBO0FDaEhaOztBRGtIWTtFQUNJLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0FDaEhoQjs7QURvSFE7RUFDSSwwQkFBQTtBQ2xIWjs7QURzSEk7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FDcEhSOztBRHNIUTtFQUNJLFlBQUE7QUNwSFo7O0FEc0hZO0VBQ0kscUJBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSx1QkFBQTtFQUNBLGVBQUE7RUFDQSxhQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0FDcEhoQjs7QUR1SFk7RUFDSSxlQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0FDckhoQjs7QUR5SFE7RUFDSSxrQkFBQTtFQUNBLFlBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxxQkFBQTtBQ3ZIWjs7QUR5SFk7RUFDSSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxpQkFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLFFBQUE7RUFDQSxVQUFBO0VBQ0EsY0FBQTtFQUNBLG9DQUFBO1VBQUEsNEJBQUE7QUN2SGhCOztBRHlIZ0I7RUFDSSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUN2SHBCOztBRHlIb0I7RUFDSSxZQUFBO0VBQ0EsaUJBQUE7QUN2SHhCOztBRDBIb0I7RUFDSSxtQkFBQTtFQUNBLGlCQUFBO0FDeEh4Qjs7QUQySG9CO0VBQ0ksVUFBQTtBQ3pIeEI7O0FEK0hROztFQUVJLFVBQUE7QUM3SFo7O0FEaUlZO0VBQ0ksY0FBQTtBQy9IaEI7O0FEb0lJO0VBQ0ksZUFBQTtBQ2xJUjs7QURxSUk7RUFDSSxrQkFBQTtBQ25JUjs7QURzSUk7RUFDSSxnQkFBQTtBQ3BJUjs7QUR1SUk7RUFDSSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxlQUFBO0FDcklSOztBRHdJSTtFQUNJLGtCQUFBO0FDdElSOztBRHlJWTtFQUNJLFVBQUE7QUN2SWhCOztBRDJJUTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFNBQUE7QUN6SVo7O0FENklJO0VBQ0ksbUJBQUE7RUFDQSxlQUFBO0FDM0lSOztBRDhJSTtFQUNJLHFCQUFBO0FDNUlSOztBRDhJSTtFQUNJLFVBQUE7RUFDQSxlQUFBO0FDNUlSOztBRGlKQTtFQUNJLGlCQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FDOUlKOztBRGdKSTtFQUNJLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7QUM5SVIiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbWVyY2hhbnQtcG9ydGFsL3NlcGFyYXRlZC1tb2R1bGVzL21lcmNoYW50LXByb2R1Y3RzL3Byb2R1Y3RzL21lcmNoYW50LWFkZC1uZXctcHJvZHVjdC9tZXJjaGFudC1hZGQtbmV3LXByb2R1Y3QuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCA6Om5nLWRlZXAgLmNrLWVkaXRvcl9fZWRpdGFibGVfaW5saW5lIHtcclxuICAgIG1pbi1oZWlnaHQ6IDUwMHB4O1xyXG59XHJcblxyXG4jYnV0dG9ub3B0aW9uIHtcclxuICAgIG1hcmdpbi1sZWZ0OiAtMTBweDtcclxufVxyXG5cclxuXHJcbiN0b29sdGlwICN0b29sdGlwdGV4dCB7XHJcbiAgICB2aXNpYmlsaXR5OiBoaWRkZW47XHJcbiAgICB3aWR0aDogMTIwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNnB4O1xyXG4gICAgcGFkZGluZzogNXB4IDA7XHJcblxyXG4gICAgLyogUG9zaXRpb24gdGhlIHRvb2x0aXAgKi9cclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHotaW5kZXg6IDE7XHJcbn1cclxuXHJcbiN0b29sdGlwOmhvdmVyICN0b29sdGlwdGV4dCB7XHJcbiAgICB2aXNpYmlsaXR5OiB2aXNpYmxlO1xyXG59XHJcblxyXG4uYmFja19idXR0b24ge1xyXG4gICAgbWFyZ2luLXRvcDogMTJweDtcclxuICAgIG1hcmdpbi1sZWZ0OiA0MHB4O1xyXG59XHJcblxyXG5cclxuLmZvcm0ge1xyXG4gICAgcGFkZGluZzogMjVweDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIC8vIHRvcDogNXZ3O1xyXG5cclxuICAgIGxhYmVsIHtcclxuICAgICAgICBjb2xvcjogIzc1NzQ3NDtcclxuICAgIH1cclxuICAgIC5jb250ZW50LXRpdGxlLWNvbnRhaW5lciB7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMzBweDtcclxuICAgICAgICAuY29udGVudC10aXRsZSB7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIGxldHRlci1zcGFjaW5nOiAwLjhweDtcclxuICAgICAgICAgICAgLy8gbWFyZ2luLWJvdHRvbTogMWNtO1xyXG4gICAgXHJcbiAgICAgICAgICAgIC5jdXN0b20taWNvbiB7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW46IDBweCAxMHB4O1xyXG4gICAgICAgICAgICB9XHJcbiAgICBcclxuICAgICAgICAgICAgLnNwYW4tMiB7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogIzI0ODBmYjtcclxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gIFxyXG5cclxuICAgIGlucHV0W3R5cGU9XCJmaWxlXCJdIHtcclxuICAgICAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgfVxyXG5cclxuICAgIC5oYXNoaGFzaCB7XHJcbiAgICAgICAgLmhhc2h0YWctaXRlbSB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiA1cHg7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICNlZWU7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDJweCA1cHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDVweDtcclxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLmhhc2h0YWctaXRlbTo6YmVmb3JlIHtcclxuICAgICAgICAgICAgY29udGVudDogXCIjXCI7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5oYXNodGFnc2tleS1pbnB1dCB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICAgICAgbWluLXdpZHRoOiA3NXB4O1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMXJlbTtcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgICAgICAgICAgb3V0bGluZTogbm9uZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5oYXNodGFnc2tleS1pbnB1dDo6YmVmb3JlIHtcclxuICAgICAgICAgICAgY29udGVudDogXCIjXCI7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLnVwbG9hZGluZy1maWxlIHtcclxuICAgICAgICBvcGFjaXR5OiAwLjU7XHJcbiAgICAgICAgZmlsdGVyOiBncmF5c2NhbGUoMSk7XHJcbiAgICB9XHJcblxyXG4gICAgLnVwbG9hZGluZy1maWxlLnRydWUge1xyXG4gICAgICAgIG9wYWNpdHk6IDE7XHJcbiAgICAgICAgZmlsdGVyOiBncmF5c2NhbGUoMCk7XHJcbiAgICB9XHJcblxyXG4gICAgLmF1dG9TS1Uge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICBtYXJnaW4tbGVmdDogMTVweDtcclxuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICB9XHJcblxyXG4gICAgLmltYWdlLXVwbG9hZCB7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAgYm9yZGVyOiAzcHggc29saWQgI2ViZWJlYjtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmNWY1ZjU7XHJcbiAgICAgICAgbWluLWhlaWdodDogMTI1cHg7XHJcbiAgICAgICAgbWFyZ2luOiAyMHB4O1xyXG4gICAgICAgIHBhZGRpbmc6IDUwcHg7XHJcblxyXG4gICAgICAgIC5sb2dvLXVwbG9hZCB7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxNTBweDtcclxuICAgICAgICB9XHJcblxyXG5cclxuICAgICAgICAuZmlsZXVwbG9hZCB7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICMyNDgwZmI7XHJcbiAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDEycHggMjJweDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC51cGxvYWQtZGVzYyB7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZDZkNmQ2ICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAudXBsb2FkZWQtaW1hZ2Uge1xyXG4gICAgICAgIG1hcmdpbjogMHB4IGF1dG87XHJcbiAgICAgICAgd2lkdGg6IGF1dG87XHJcbiAgICAgICAgcGFkZGluZzogNTBweDtcclxuICAgICAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG5cclxuICAgICAgICAudGV4dC1maWxldXBsb2FkIHtcclxuICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICAgICAgICBjb2xvcjogIzI0ODBmYjtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAuaW1nIHtcclxuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgIG1pbi1oZWlnaHQ6IDM0OHB4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciBjZW50ZXI7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcblxyXG4gICAgICAgICAgICAuZWRpdG9yLXRvb2wge1xyXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwLjIpO1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgICAgICAgICBvcGFjaXR5OiAwO1xyXG4gICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLmltZzpob3ZlciAuZWRpdG9yLXRvb2wge1xyXG4gICAgICAgICAgICBvcGFjaXR5OiAwLjk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5zdGF0dXMtZm9ybS5mYWxzZSB7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI0YyRjJGMjtcclxuICAgICAgICBjb2xvcjogI2JjYmNiYztcclxuICAgIH1cclxuXHJcbiAgICAuY3VzdG9tLWZvcm0tcmlnaHQge1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XHJcbiAgICB9XHJcblxyXG4gICAgLmN1c3RvbS1wcmljZS1jb2x1bW4ge1xyXG4gICAgICAgIGJvcmRlci1sZWZ0OiAxcHggc29saWQgI2UyZTJlMjtcclxuICAgIH1cclxuXHJcbiAgICAucHJpY2UtY29sdW1uIHtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcblxyXG4gICAgICAgIHAge1xyXG4gICAgICAgICAgICBtYXJnaW46IDVweCAxMHB4IDVweCAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5jdXN0b20tYm9yZGVyIHtcclxuICAgICAgICBib3JkZXItbGVmdDogMXB4IHNvbGlkICNlMmUyZTI7XHJcbiAgICAgICAgYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgI2UyZTJlMjtcclxuICAgIH1cclxuXHJcbiAgICAuZGltZW5zaW9uIHtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAwLjI1cmVtO1xyXG4gICAgICAgIG1hcmdpbi1yaWdodDogMTBweDtcclxuICAgIH1cclxuXHJcbiAgICAucHJvZHVjdC10eXBlLWxpc3Qge1xyXG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG5cclxuICAgICAgICAucHJvZHVjdC10eXBlIHtcclxuICAgICAgICAgICAgZGlzcGxheTogaW5saW5lLWZsZXg7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDExcHggMjVweDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTBweDtcclxuICAgICAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjOWY5ZjlmO1xyXG4gICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbiAgICAgICAgICAgIHRyYW5zaXRpb246IGVhc2UgMC4ycyBhbGwgMHM7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjOWY5ZjlmO1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAucHJvZHVjdC10eXBlLnRydWUge1xyXG4gICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICMyNDgwZmI7XHJcbiAgICAgICAgICAgIGJvcmRlci1jb2xvcjogIzI5ODBiOTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLmZvcm0tY29udHJvbC5kZXMge1xyXG4gICAgICAgIHBhZGRpbmctdG9wOiA0cHg7XHJcbiAgICAgICAgZGlzcGxheTogaW5saW5lLWZsZXg7XHJcbiAgICAgICAgcGFkZGluZy1ib3R0b206IDRweDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG5cclxuICAgICAgICBpbnB1dCB7XHJcbiAgICAgICAgICAgIGJvcmRlcjogMHB4IHNvbGlkIHRyYW5zcGFyZW50O1xyXG4gICAgICAgICAgICB3aWR0aDogY2FsYygxMDAlIC0gMTVweCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpbnB1dDpmb2N1cyB7XHJcbiAgICAgICAgICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGRpdi5lcnJvciB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI2U3NGMzYztcclxuICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgcGFkZGluZzogMjVweDtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAyNXB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5uYXZiYXItbWVudSB7XHJcbiAgICAgICAgb3ZlcmZsb3cteTogc2Nyb2xsO1xyXG4gICAgICAgIG1heC1oZWlnaHQ6IDI1MHB4O1xyXG4gICAgICAgIG1hcmdpbjogMTVweCA1cHg7XHJcblxyXG4gICAgICAgIC5jYXRlZ29yeS1saXN0IHtcclxuICAgICAgICAgICAgLmxpc3QtZ3JvdXAtaXRlbSB7XHJcbiAgICAgICAgICAgICAgICBib3JkZXI6IG5vbmU7XHJcbiAgICAgICAgICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcblxyXG4gICAgICAgICAgICAgICAgLmNoZWNrZWQtaWNvbiB7XHJcbiAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogaW5saW5lLWZsZXg7XHJcbiAgICAgICAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgICAgICAgICB3aWR0aDogMjBweDtcclxuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDIwcHg7XHJcbiAgICAgICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMTVweDtcclxuICAgICAgICAgICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOiAjMzQ5OGRiO1xyXG4gICAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgICAgICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgLmNoZWNrZWQtaWNvbi5mYWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5oZWFkZXItZmlyc3Qge1xyXG4gICAgICAgIG1hcmdpbjogMzBweDtcclxuICAgICAgICAvLyBwYWRkaW5nOiAzMHB4O1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDE4cHg7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcclxuICAgIH1cclxuXHJcbiAgICAubGVmdC1zZWN0aW9uLXVwbG9hZCB7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXIgIWltcG9ydGFudDtcclxuICAgIH1cclxuXHJcbiAgICAudXBsb2FkLWNvbnRhaW5lciB7XHJcbiAgICAgICAgcGFkZGluZzogMjBweDtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxOHB4O1xyXG4gICAgICAgIGJveC1zaGFkb3c6IDAgM3B4IDZweCAwIHJnYmEoMCwgMCwgMCwgMC4wNik7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG5cclxuICAgICAgICAuY2wtbGlzdCB7XHJcbiAgICAgICAgICAgIC5pbWcge1xyXG4gICAgICAgICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICB9XHJcbiAgICAuY29sLW1kLTh7XHJcbiAgICAgICAgLmZvcm0tZ3JvdXB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XHJcbiAgICAgICAgICAgIC5sYWJlbCB7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMzglO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmNvbC1tZC02IHtcclxuICAgICAgICAuZm9ybS1ncm91cHtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcclxuICAgICAgICAgICAgLmxhYmVsIHtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiA2MCU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgc2VsZWN0LmZvcm0tY29udHJvbDpub3QoW3NpemVdKTpub3QoW211bHRpcGxlXSkge1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0IDogdW5zZXQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICBcclxuICAgIH1cclxuICAgIC5zZWN0aW9uLXVwbG9hZCB7XHJcbiAgICAgICAgLy8gbWFyZ2luLXRvcDogNXZoO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG5cclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG5cclxuICAgICAgICAuY29sLTJkb3Q0LFxyXG4gICAgICAgIC5jb2wtc20tMmRvdDQsXHJcbiAgICAgICAgLmNvbC1tZC0yZG90NCxcclxuICAgICAgICAuY29sLWxnLTJkb3Q0LFxyXG4gICAgICAgIC5jb2wteGwtMmRvdDQge1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICBtaW4taGVpZ2h0OiAxcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDE1cHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMTVweDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5jb2wtMmRvdDQge1xyXG4gICAgICAgICAgICAtd2Via2l0LWJveC1mbGV4OiAwO1xyXG4gICAgICAgICAgICAtbXMtZmxleDogMCAwIDIwJTtcclxuICAgICAgICAgICAgZmxleDogMCAwIDIwJTtcclxuICAgICAgICAgICAgbWF4LXdpZHRoOiAyMCU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBAbWVkaWEgKG1pbi13aWR0aDogNTQwcHgpIHtcclxuICAgICAgICAgICAgLmNvbC1zbS0yZG90NCB7XHJcbiAgICAgICAgICAgICAgICAtd2Via2l0LWJveC1mbGV4OiAwO1xyXG4gICAgICAgICAgICAgICAgLW1zLWZsZXg6IDAgMCAyMCU7XHJcbiAgICAgICAgICAgICAgICBmbGV4OiAwIDAgMjAlO1xyXG4gICAgICAgICAgICAgICAgbWF4LXdpZHRoOiAyMCU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIEBtZWRpYSAobWluLXdpZHRoOiA3MjBweCkge1xyXG4gICAgICAgICAgICAuY29sLW1kLTJkb3Q0IHtcclxuICAgICAgICAgICAgICAgIC13ZWJraXQtYm94LWZsZXg6IDA7XHJcbiAgICAgICAgICAgICAgICAtbXMtZmxleDogMCAwIDIwJTtcclxuICAgICAgICAgICAgICAgIGZsZXg6IDAgMCAyMCU7XHJcbiAgICAgICAgICAgICAgICBtYXgtd2lkdGg6IDIwJTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgQG1lZGlhIChtaW4td2lkdGg6IDk2MHB4KSB7XHJcbiAgICAgICAgICAgIC5jb2wtbGctMmRvdDQge1xyXG4gICAgICAgICAgICAgICAgLXdlYmtpdC1ib3gtZmxleDogMDtcclxuICAgICAgICAgICAgICAgIC1tcy1mbGV4OiAwIDAgMjAlO1xyXG4gICAgICAgICAgICAgICAgZmxleDogMCAwIDIwJTtcclxuICAgICAgICAgICAgICAgIG1heC13aWR0aDogMjAlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBAbWVkaWEgKG1pbi13aWR0aDogMTE0MHB4KSB7XHJcbiAgICAgICAgICAgIC5jb2wteGwtMmRvdDQge1xyXG4gICAgICAgICAgICAgICAgLXdlYmtpdC1ib3gtZmxleDogMDtcclxuICAgICAgICAgICAgICAgIC1tcy1mbGV4OiAwIDAgMjAlO1xyXG4gICAgICAgICAgICAgICAgZmxleDogMCAwIDIwJTtcclxuICAgICAgICAgICAgICAgIG1heC13aWR0aDogMjAlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5jb2wtbWQtMmRvdDQge1xyXG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuXHJcblxyXG4gICAgICAgICAgICAuYmx1ZS11cGxvYWQge1xyXG4gICAgICAgICAgICAgICAgY29sb3I6ICM1NmE0ZmY7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE2cHg7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLmNhcmQge1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogM3ZoO1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAxOTBweDtcclxuICAgICAgICAgICAgICAgIGJvcmRlcjogbm9uZTtcclxuXHJcbiAgICAgICAgICAgICAgICAuY2FyZC1jb250ZW50IHtcclxuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAudXBsb2FkLWRlc2Mge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb2xvcjogI2JiYiAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC5sb2dvLXVwbG9hZCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLmltYWdlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuY3VzdG9tIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGJhY2tncm91bmQtY29sb3I6I2VlZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBib3JkZXI6IDNweCBzb2xpZCAjZWJlYmViO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gcGFkZGluZzogNDBweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5kcmFnLWRyb3Age1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTBweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLnNldC1vcGFjaXR5IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvcGFjaXR5OiAwLjE7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiA2MHB4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiBhdXRvO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIC5mb3JtLWdyb3VwLnVwbG9hZGVkIHtcclxuICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLmxvYWRpbmcge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuc2stZmFkaW5nLWNpcmNsZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0b3A6IDUwJTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLnVwbG9hZGVkLWltYWdlMiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5pbWcge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogMTVweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5pbWc+aW1nIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1heC1oZWlnaHQ6IDMyMHB4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuZWRpdG9yLXRvb2wge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5jb2wtbWQtMmRvdDQge1xyXG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblxyXG4gICAgICAgICAgICAuY2FyZCB7XHJcblxyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAyNTBweDtcclxuICAgICAgICAgICAgICAgIGJvcmRlcjogbm9uZTtcclxuICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICAgICAgICAgICAgICAgIGJveC1zaGFkb3c6IDAgM3B4IDZweCAwIHJnYmEoMCwgMCwgMCwgMC4wNSk7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xyXG5cclxuXHJcbiAgICAgICAgICAgICAgICAuY2FyZC1jb250ZW50IHtcclxuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAuaW1hZ2UtY2FudmFzIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDgwJTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC51cGxvYWQtZGVzYyB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiAjYmJiICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLmxvZ28tdXBsb2FkIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLmltYWdlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuY3VzdG9tIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGJhY2tncm91bmQtY29sb3I6ICNlZWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBib3JkZXI6IDNweCBzb2xpZCAjZWJlYmViO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIHBhZGRpbmc6IDQwcHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAuZHJhZy1kcm9wIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDEwcHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zZXQtb3BhY2l0eSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb3BhY2l0eTogMC4xO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogNjBweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogYXV0bztcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAuZm9ybS1ncm91cC51cGxvYWRlZCB7XHJcbiAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgLnJlbW92ZS1pY29uIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByaWdodDogMDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdG9wOiAwO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb2xvcjogcmVkO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDIycHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgLmxvYWRpbmcge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuc2stZmFkaW5nLWNpcmNsZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0b3A6IDUwJTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLnVwbG9hZGVkLWltYWdlMiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5pbWcge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuaW1nPmltZyB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXgtaGVpZ2h0OiAzMjBweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgLmVkaXRvci10b29sIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAuY2FyZCB7XHJcbiAgICAgICAgYm94LXNoYWRvdzogMCAxcHggMTFweCAwIHJnYmEoMCwgMCwgMCwgMC4yKSwgMCAycHggM3B4IDAgcmdiYSgwLCAwLCAwLCAwLjE5KTtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG5cclxuICAgICAgICAuY2FyZC1oZWFkZXIge1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgbWFyZ2luOiAwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweDtcclxuICAgICAgICAgICAgYm9yZGVyLWxlZnQ6IDNweCBzb2xpZCAjMjQ4MGZiO1xyXG5cclxuICAgICAgICAgICAgbGFiZWwge1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luOiAyMHB4IDEwcHg7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLnVwbG9hZGVkIHtcclxuICAgICAgICAgICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5lbmQtZm9vdGVyIHtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHJcbiAgICAgICAgLmZvb3Rlci1zYXZlLWNhbmNlbCB7XHJcbiAgICAgICAgICAgIG1hcmdpbjogNTBweDtcclxuXHJcbiAgICAgICAgICAgIC5jYW5jZWwge1xyXG4gICAgICAgICAgICAgICAgYm9yZGVyLWNvbG9yOiAjMjQ4MGZiO1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xyXG4gICAgICAgICAgICAgICAgY29sb3I6ICMyNDgwZmI7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgICAgICAgICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMjVyZW07XHJcbiAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAuc2F2ZSB7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiAxNXB4O1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDI1cmVtO1xyXG4gICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICMyNDgwZmI7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5kZWxldGUge1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgICAgIGZsb2F0OiByaWdodDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2U3NGMzYztcclxuICAgICAgICAgICAgb3BhY2l0eTogMC41O1xyXG4gICAgICAgICAgICBib3JkZXItY29sb3I6ICNjMDM5MmI7XHJcblxyXG4gICAgICAgICAgICAuZGVsZXRlLW1vZGFsIHtcclxuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgICAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gICAgICAgICAgICAgICAgcGFkZGluZzogNHB4IDEwcHg7XHJcbiAgICAgICAgICAgICAgICB0b3A6IDBweDtcclxuICAgICAgICAgICAgICAgIHJpZ2h0OiAwcHg7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogI2U3NGMzYztcclxuICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNDVweCk7XHJcblxyXG4gICAgICAgICAgICAgICAgLmJ0bi1ncm91cCB7XHJcbiAgICAgICAgICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2U3NGMzYztcclxuICAgICAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICAgICAgICAgICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLm5vIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3BhY2l0eTogMC41O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBib3JkZXItd2lkdGg6IDBweDtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC55ZXMge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZTc0YzNjO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBib3JkZXItd2lkdGg6IDBweDtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC5ubzpob3ZlciB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wYWNpdHk6IDE7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAuZGVsZXRlOmhvdmVyLFxyXG4gICAgICAgIC5kZWxldGU6Zm9jdXMge1xyXG4gICAgICAgICAgICBvcGFjaXR5OiAxO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLmRlbGV0ZTpmb2N1cyB7XHJcbiAgICAgICAgICAgIC5kZWxldGUtbW9kYWwge1xyXG4gICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLnZhcmlhbi1idXR0b24ge1xyXG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIH1cclxuXHJcbiAgICAudmFyaWFuLWltYWdlLXBhcmVudCB7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgfVxyXG5cclxuICAgIC52YXJpYW4tdGFibGUge1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDQwcHg7XHJcbiAgICB9XHJcblxyXG4gICAgLmRlbGV0ZS12YXJpYW4taW1hZ2Uge1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICByaWdodDogMjJweDtcclxuICAgICAgICB0b3A6IDIycHg7XHJcbiAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjRweDtcclxuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICB9XHJcblxyXG4gICAgLnZhcmlhbi1wYXJlbnR7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG5cclxuICAgICAgICA6Om5nLWRlZXAgeyBcclxuICAgICAgICAgICAgLmlucHV0LWdyb3VwIHtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiA5MSU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGkge1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIHJpZ2h0OiAyMHB4O1xyXG4gICAgICAgICAgICB0b3A6IDEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5hZGQtdmFsdWUge1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogNXB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5yZWQtYWxlcnQge1xyXG4gICAgICAgIGJvcmRlcjogc29saWQgMXB4IHJlZDtcclxuICAgIH1cclxuICAgIC5yZWQtZXJyb3ItYWxlcnQge1xyXG4gICAgICAgIGNvbG9yOiByZWQ7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgfSBcclxufVxyXG5cclxuXHJcbi5lcnJvci1tZXNzYWdlIHtcclxuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgaGVpZ2h0OjEwMHZoO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOjI2cHg7XHJcbiAgICBwYWRkaW5nLXRvcDogNDBweDtcclxuICBcclxuICAgIC50ZXh0LW1lc3NhZ2Uge1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICNFNzRDM0M7XHJcbiAgICAgICAgcGFkZGluZzogMjBweDtcclxuICAgICAgICBtYXJnaW46IDAgMTBweDtcclxuICAgIH1cclxufSIsIjpob3N0IDo6bmctZGVlcCAuY2stZWRpdG9yX19lZGl0YWJsZV9pbmxpbmUge1xuICBtaW4taGVpZ2h0OiA1MDBweDtcbn1cblxuI2J1dHRvbm9wdGlvbiB7XG4gIG1hcmdpbi1sZWZ0OiAtMTBweDtcbn1cblxuI3Rvb2x0aXAgI3Rvb2x0aXB0ZXh0IHtcbiAgdmlzaWJpbGl0eTogaGlkZGVuO1xuICB3aWR0aDogMTIwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xuICBjb2xvcjogI2ZmZjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBib3JkZXItcmFkaXVzOiA2cHg7XG4gIHBhZGRpbmc6IDVweCAwO1xuICAvKiBQb3NpdGlvbiB0aGUgdG9vbHRpcCAqL1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHotaW5kZXg6IDE7XG59XG5cbiN0b29sdGlwOmhvdmVyICN0b29sdGlwdGV4dCB7XG4gIHZpc2liaWxpdHk6IHZpc2libGU7XG59XG5cbi5iYWNrX2J1dHRvbiB7XG4gIG1hcmdpbi10b3A6IDEycHg7XG4gIG1hcmdpbi1sZWZ0OiA0MHB4O1xufVxuXG4uZm9ybSB7XG4gIHBhZGRpbmc6IDI1cHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5mb3JtIGxhYmVsIHtcbiAgY29sb3I6ICM3NTc0NzQ7XG59XG4uZm9ybSAuY29udGVudC10aXRsZS1jb250YWluZXIge1xuICBtYXJnaW4tYm90dG9tOiAzMHB4O1xufVxuLmZvcm0gLmNvbnRlbnQtdGl0bGUtY29udGFpbmVyIC5jb250ZW50LXRpdGxlIHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgbGV0dGVyLXNwYWNpbmc6IDAuOHB4O1xufVxuLmZvcm0gLmNvbnRlbnQtdGl0bGUtY29udGFpbmVyIC5jb250ZW50LXRpdGxlIC5jdXN0b20taWNvbiB7XG4gIG1hcmdpbjogMHB4IDEwcHg7XG59XG4uZm9ybSAuY29udGVudC10aXRsZS1jb250YWluZXIgLmNvbnRlbnQtdGl0bGUgLnNwYW4tMiB7XG4gIGNvbG9yOiAjMjQ4MGZiO1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5mb3JtIGlucHV0W3R5cGU9ZmlsZV0ge1xuICBkaXNwbGF5OiBub25lO1xufVxuLmZvcm0gLmhhc2hoYXNoIC5oYXNodGFnLWl0ZW0ge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xuICBiYWNrZ3JvdW5kOiAjZWVlO1xuICBwYWRkaW5nOiAycHggNXB4O1xuICBtYXJnaW4tYm90dG9tOiA1cHg7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbn1cbi5mb3JtIC5oYXNoaGFzaCAuaGFzaHRhZy1pdGVtOjpiZWZvcmUge1xuICBjb250ZW50OiBcIiNcIjtcbiAgZGlzcGxheTogaW5saW5lO1xufVxuLmZvcm0gLmhhc2hoYXNoIC5oYXNodGFnc2tleS1pbnB1dCB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbWluLXdpZHRoOiA3NXB4O1xuICBtYXJnaW4tYm90dG9tOiA1cHg7XG4gIGZvbnQtc2l6ZTogMXJlbTtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgb3V0bGluZTogbm9uZTtcbn1cbi5mb3JtIC5oYXNoaGFzaCAuaGFzaHRhZ3NrZXktaW5wdXQ6OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiI1wiO1xuICBkaXNwbGF5OiBpbmxpbmU7XG59XG4uZm9ybSAudXBsb2FkaW5nLWZpbGUge1xuICBvcGFjaXR5OiAwLjU7XG4gIGZpbHRlcjogZ3JheXNjYWxlKDEpO1xufVxuLmZvcm0gLnVwbG9hZGluZy1maWxlLnRydWUge1xuICBvcGFjaXR5OiAxO1xuICBmaWx0ZXI6IGdyYXlzY2FsZSgwKTtcbn1cbi5mb3JtIC5hdXRvU0tVIHtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBtYXJnaW4tbGVmdDogMTVweDtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xufVxuLmZvcm0gLmltYWdlLXVwbG9hZCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBib3JkZXI6IDNweCBzb2xpZCAjZWJlYmViO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjVmNWY1O1xuICBtaW4taGVpZ2h0OiAxMjVweDtcbiAgbWFyZ2luOiAyMHB4O1xuICBwYWRkaW5nOiA1MHB4O1xufVxuLmZvcm0gLmltYWdlLXVwbG9hZCAubG9nby11cGxvYWQge1xuICB3aWR0aDogMTUwcHg7XG59XG4uZm9ybSAuaW1hZ2UtdXBsb2FkIC5maWxldXBsb2FkIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzI0ODBmYjtcbiAgY29sb3I6IHdoaXRlO1xuICBib3JkZXItcmFkaXVzOiA4cHg7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgcGFkZGluZzogMTJweCAyMnB4O1xufVxuLmZvcm0gLmltYWdlLXVwbG9hZCAudXBsb2FkLWRlc2Mge1xuICBjb2xvcjogI2Q2ZDZkNiAhaW1wb3J0YW50O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMThweDtcbn1cbi5mb3JtIC51cGxvYWRlZC1pbWFnZSB7XG4gIG1hcmdpbjogMHB4IGF1dG87XG4gIHdpZHRoOiBhdXRvO1xuICBwYWRkaW5nOiA1MHB4O1xuICBoZWlnaHQ6IGF1dG87XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbi5mb3JtIC51cGxvYWRlZC1pbWFnZSAudGV4dC1maWxldXBsb2FkIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBjb2xvcjogIzI0ODBmYjtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cbi5mb3JtIC51cGxvYWRlZC1pbWFnZSAuaW1nIHtcbiAgd2lkdGg6IDEwMCU7XG4gIG1pbi1oZWlnaHQ6IDM0OHB4O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciBjZW50ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG59XG4uZm9ybSAudXBsb2FkZWQtaW1hZ2UgLmltZyAuZWRpdG9yLXRvb2wge1xuICBiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuMik7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgb3BhY2l0eTogMDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4uZm9ybSAudXBsb2FkZWQtaW1hZ2UgLmltZzpob3ZlciAuZWRpdG9yLXRvb2wge1xuICBvcGFjaXR5OiAwLjk7XG59XG4uZm9ybSAuc3RhdHVzLWZvcm0uZmFsc2Uge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjJGMkYyO1xuICBjb2xvcjogI2JjYmNiYztcbn1cbi5mb3JtIC5jdXN0b20tZm9ybS1yaWdodCB7XG4gIGFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbn1cbi5mb3JtIC5jdXN0b20tcHJpY2UtY29sdW1uIHtcbiAgYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCAjZTJlMmUyO1xufVxuLmZvcm0gLnByaWNlLWNvbHVtbiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG59XG4uZm9ybSAucHJpY2UtY29sdW1uIHAge1xuICBtYXJnaW46IDVweCAxMHB4IDVweCAwcHg7XG59XG4uZm9ybSAuY3VzdG9tLWJvcmRlciB7XG4gIGJvcmRlci1sZWZ0OiAxcHggc29saWQgI2UyZTJlMjtcbiAgYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgI2UyZTJlMjtcbn1cbi5mb3JtIC5kaW1lbnNpb24ge1xuICBib3JkZXItcmFkaXVzOiAwLjI1cmVtO1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG4uZm9ybSAucHJvZHVjdC10eXBlLWxpc3Qge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4uZm9ybSAucHJvZHVjdC10eXBlLWxpc3QgLnByb2R1Y3QtdHlwZSB7XG4gIGRpc3BsYXk6IGlubGluZS1mbGV4O1xuICBwYWRkaW5nOiAxMXB4IDI1cHg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGJvcmRlci1yYWRpdXM6IDUwcHg7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgYm9yZGVyOiAxcHggc29saWQgIzlmOWY5ZjtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xuICBtYXJnaW4tbGVmdDogMTBweDtcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xuICB0cmFuc2l0aW9uOiBlYXNlIDAuMnMgYWxsIDBzO1xuICBjb2xvcjogIzlmOWY5ZjtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuLmZvcm0gLnByb2R1Y3QtdHlwZS1saXN0IC5wcm9kdWN0LXR5cGUudHJ1ZSB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzI0ODBmYjtcbiAgYm9yZGVyLWNvbG9yOiAjMjk4MGI5O1xufVxuLmZvcm0gLmZvcm0tY29udHJvbC5kZXMge1xuICBwYWRkaW5nLXRvcDogNHB4O1xuICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcbiAgcGFkZGluZy1ib3R0b206IDRweDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4uZm9ybSAuZm9ybS1jb250cm9sLmRlcyBpbnB1dCB7XG4gIGJvcmRlcjogMHB4IHNvbGlkIHRyYW5zcGFyZW50O1xuICB3aWR0aDogY2FsYygxMDAlIC0gMTVweCk7XG59XG4uZm9ybSAuZm9ybS1jb250cm9sLmRlcyBpbnB1dDpmb2N1cyB7XG4gIG91dGxpbmU6IG5vbmU7XG59XG4uZm9ybSBkaXYuZXJyb3Ige1xuICBiYWNrZ3JvdW5kOiAjZTc0YzNjO1xuICBjb2xvcjogd2hpdGU7XG4gIHBhZGRpbmc6IDI1cHg7XG4gIG1hcmdpbi1ib3R0b206IDI1cHg7XG59XG4uZm9ybSAubmF2YmFyLW1lbnUge1xuICBvdmVyZmxvdy15OiBzY3JvbGw7XG4gIG1heC1oZWlnaHQ6IDI1MHB4O1xuICBtYXJnaW46IDE1cHggNXB4O1xufVxuLmZvcm0gLm5hdmJhci1tZW51IC5jYXRlZ29yeS1saXN0IC5saXN0LWdyb3VwLWl0ZW0ge1xuICBib3JkZXI6IG5vbmU7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbi5mb3JtIC5uYXZiYXItbWVudSAuY2F0ZWdvcnktbGlzdCAubGlzdC1ncm91cC1pdGVtIC5jaGVja2VkLWljb24ge1xuICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHdpZHRoOiAyMHB4O1xuICBoZWlnaHQ6IDIwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIGNvbG9yOiAjMzQ5OGRiO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG4uZm9ybSAubmF2YmFyLW1lbnUgLmNhdGVnb3J5LWxpc3QgLmxpc3QtZ3JvdXAtaXRlbSAuY2hlY2tlZC1pY29uLmZhbHNlIHtcbiAgY29sb3I6IHRyYW5zcGFyZW50O1xufVxuLmZvcm0gLmhlYWRlci1maXJzdCB7XG4gIG1hcmdpbjogMzBweDtcbiAgYm9yZGVyLXJhZGl1czogMThweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbn1cbi5mb3JtIC5sZWZ0LXNlY3Rpb24tdXBsb2FkIHtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXIgIWltcG9ydGFudDtcbn1cbi5mb3JtIC51cGxvYWQtY29udGFpbmVyIHtcbiAgcGFkZGluZzogMjBweDtcbiAgYm9yZGVyLXJhZGl1czogMThweDtcbiAgYm94LXNoYWRvdzogMCAzcHggNnB4IDAgcmdiYSgwLCAwLCAwLCAwLjA2KTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbn1cbi5mb3JtIC51cGxvYWQtY29udGFpbmVyIC5jbC1saXN0IC5pbWcge1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuLmZvcm0gLmNvbC1tZC04IC5mb3JtLWdyb3VwIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcbn1cbi5mb3JtIC5jb2wtbWQtOCAuZm9ybS1ncm91cCAubGFiZWwge1xuICB3aWR0aDogMzglO1xufVxuLmZvcm0gLmNvbC1tZC02IC5mb3JtLWdyb3VwIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcbn1cbi5mb3JtIC5jb2wtbWQtNiAuZm9ybS1ncm91cCAubGFiZWwge1xuICB3aWR0aDogNjAlO1xufVxuLmZvcm0gLmNvbC1tZC02IC5mb3JtLWdyb3VwIHNlbGVjdC5mb3JtLWNvbnRyb2w6bm90KFtzaXplXSk6bm90KFttdWx0aXBsZV0pIHtcbiAgaGVpZ2h0OiB1bnNldDtcbn1cbi5mb3JtIC5zZWN0aW9uLXVwbG9hZCB7XG4gIHdpZHRoOiAxMDAlO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLmZvcm0gLnNlY3Rpb24tdXBsb2FkIC5jb2wtMmRvdDQsXG4uZm9ybSAuc2VjdGlvbi11cGxvYWQgLmNvbC1zbS0yZG90NCxcbi5mb3JtIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTJkb3Q0LFxuLmZvcm0gLnNlY3Rpb24tdXBsb2FkIC5jb2wtbGctMmRvdDQsXG4uZm9ybSAuc2VjdGlvbi11cGxvYWQgLmNvbC14bC0yZG90NCB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgd2lkdGg6IDEwMCU7XG4gIG1pbi1oZWlnaHQ6IDFweDtcbiAgcGFkZGluZy1yaWdodDogMTVweDtcbiAgcGFkZGluZy1sZWZ0OiAxNXB4O1xufVxuLmZvcm0gLnNlY3Rpb24tdXBsb2FkIC5jb2wtMmRvdDQge1xuICAtd2Via2l0LWJveC1mbGV4OiAwO1xuICAtbXMtZmxleDogMCAwIDIwJTtcbiAgZmxleDogMCAwIDIwJTtcbiAgbWF4LXdpZHRoOiAyMCU7XG59XG5AbWVkaWEgKG1pbi13aWR0aDogNTQwcHgpIHtcbiAgLmZvcm0gLnNlY3Rpb24tdXBsb2FkIC5jb2wtc20tMmRvdDQge1xuICAgIC13ZWJraXQtYm94LWZsZXg6IDA7XG4gICAgLW1zLWZsZXg6IDAgMCAyMCU7XG4gICAgZmxleDogMCAwIDIwJTtcbiAgICBtYXgtd2lkdGg6IDIwJTtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDcyMHB4KSB7XG4gIC5mb3JtIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTJkb3Q0IHtcbiAgICAtd2Via2l0LWJveC1mbGV4OiAwO1xuICAgIC1tcy1mbGV4OiAwIDAgMjAlO1xuICAgIGZsZXg6IDAgMCAyMCU7XG4gICAgbWF4LXdpZHRoOiAyMCU7XG4gIH1cbn1cbkBtZWRpYSAobWluLXdpZHRoOiA5NjBweCkge1xuICAuZm9ybSAuc2VjdGlvbi11cGxvYWQgLmNvbC1sZy0yZG90NCB7XG4gICAgLXdlYmtpdC1ib3gtZmxleDogMDtcbiAgICAtbXMtZmxleDogMCAwIDIwJTtcbiAgICBmbGV4OiAwIDAgMjAlO1xuICAgIG1heC13aWR0aDogMjAlO1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogMTE0MHB4KSB7XG4gIC5mb3JtIC5zZWN0aW9uLXVwbG9hZCAuY29sLXhsLTJkb3Q0IHtcbiAgICAtd2Via2l0LWJveC1mbGV4OiAwO1xuICAgIC1tcy1mbGV4OiAwIDAgMjAlO1xuICAgIGZsZXg6IDAgMCAyMCU7XG4gICAgbWF4LXdpZHRoOiAyMCU7XG4gIH1cbn1cbi5mb3JtIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTJkb3Q0IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDE0cHg7XG59XG4uZm9ybSAuc2VjdGlvbi11cGxvYWQgLmNvbC1tZC0yZG90NCAuYmx1ZS11cGxvYWQge1xuICBjb2xvcjogIzU2YTRmZjtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5mb3JtIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTJkb3Q0IC5jYXJkIHtcbiAgbWFyZ2luLXRvcDogM3ZoO1xuICBoZWlnaHQ6IDE5MHB4O1xuICBib3JkZXI6IG5vbmU7XG59XG4uZm9ybSAuc2VjdGlvbi11cGxvYWQgLmNvbC1tZC0yZG90NCAuY2FyZCAuY2FyZC1jb250ZW50IHtcbiAgaGVpZ2h0OiAxMDAlO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4uZm9ybSAuc2VjdGlvbi11cGxvYWQgLmNvbC1tZC0yZG90NCAuY2FyZCAuY2FyZC1jb250ZW50IC51cGxvYWQtZGVzYyB7XG4gIGNvbG9yOiAjYmJiICFpbXBvcnRhbnQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuLmZvcm0gLnNlY3Rpb24tdXBsb2FkIC5jb2wtbWQtMmRvdDQgLmNhcmQgLmNhcmQtY29udGVudCAubG9nby11cGxvYWQge1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG59XG4uZm9ybSAuc2VjdGlvbi11cGxvYWQgLmNvbC1tZC0yZG90NCAuY2FyZCAuY2FyZC1jb250ZW50IC5pbWFnZSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBmbG9hdDogbGVmdDtcbiAgaGVpZ2h0OiBhdXRvO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuLmZvcm0gLnNlY3Rpb24tdXBsb2FkIC5jb2wtbWQtMmRvdDQgLmNhcmQgLmNhcmQtY29udGVudCAuaW1hZ2UgLmN1c3RvbSB7XG4gIGhlaWdodDogMTAwJTtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG59XG4uZm9ybSAuc2VjdGlvbi11cGxvYWQgLmNvbC1tZC0yZG90NCAuY2FyZCAuY2FyZC1jb250ZW50IC5pbWFnZSAuY3VzdG9tIC5kcmFnLWRyb3Age1xuICBmb250LXNpemU6IDEwcHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5mb3JtIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTJkb3Q0IC5jYXJkIC5jYXJkLWNvbnRlbnQgLmltYWdlIC5jdXN0b20gLnNldC1vcGFjaXR5IHtcbiAgb3BhY2l0eTogMC4xO1xuICBoZWlnaHQ6IDYwcHg7XG4gIHdpZHRoOiBhdXRvO1xufVxuLmZvcm0gLnNlY3Rpb24tdXBsb2FkIC5jb2wtbWQtMmRvdDQgLmNhcmQgLmZvcm0tZ3JvdXAudXBsb2FkZWQge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuLmZvcm0gLnNlY3Rpb24tdXBsb2FkIC5jb2wtbWQtMmRvdDQgLmNhcmQgLmZvcm0tZ3JvdXAudXBsb2FkZWQgLmxvYWRpbmcge1xuICBoZWlnaHQ6IDEwMCU7XG59XG4uZm9ybSAuc2VjdGlvbi11cGxvYWQgLmNvbC1tZC0yZG90NCAuY2FyZCAuZm9ybS1ncm91cC51cGxvYWRlZCAubG9hZGluZyAuc2stZmFkaW5nLWNpcmNsZSB7XG4gIHRvcDogNTAlO1xufVxuLmZvcm0gLnNlY3Rpb24tdXBsb2FkIC5jb2wtbWQtMmRvdDQgLmNhcmQgLmZvcm0tZ3JvdXAudXBsb2FkZWQgLnVwbG9hZGVkLWltYWdlMiB7XG4gIGhlaWdodDogMTAwJTtcbn1cbi5mb3JtIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTJkb3Q0IC5jYXJkIC5mb3JtLWdyb3VwLnVwbG9hZGVkIC51cGxvYWRlZC1pbWFnZTIgLmltZyB7XG4gIHBhZGRpbmc6IDE1cHg7XG4gIGhlaWdodDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xufVxuLmZvcm0gLnNlY3Rpb24tdXBsb2FkIC5jb2wtbWQtMmRvdDQgLmNhcmQgLmZvcm0tZ3JvdXAudXBsb2FkZWQgLnVwbG9hZGVkLWltYWdlMiAuaW1nID4gaW1nIHtcbiAgaGVpZ2h0OiAxMDAlO1xuICBtYXgtaGVpZ2h0OiAzMjBweDtcbn1cbi5mb3JtIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTJkb3Q0IC5jYXJkIC5mb3JtLWdyb3VwLnVwbG9hZGVkIC51cGxvYWRlZC1pbWFnZTIgLmVkaXRvci10b29sIHtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuLmZvcm0gLnNlY3Rpb24tdXBsb2FkIC5jb2wtbWQtMmRvdDQge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uZm9ybSAuc2VjdGlvbi11cGxvYWQgLmNvbC1tZC0yZG90NCAuY2FyZCB7XG4gIGhlaWdodDogMjUwcHg7XG4gIGJvcmRlcjogbm9uZTtcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xuICBib3gtc2hhZG93OiAwIDNweCA2cHggMCByZ2JhKDAsIDAsIDAsIDAuMDUpO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xufVxuLmZvcm0gLnNlY3Rpb24tdXBsb2FkIC5jb2wtbWQtMmRvdDQgLmNhcmQgLmNhcmQtY29udGVudCB7XG4gIGhlaWdodDogMTAwJTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLmZvcm0gLnNlY3Rpb24tdXBsb2FkIC5jb2wtbWQtMmRvdDQgLmNhcmQgLmNhcmQtY29udGVudCAuaW1hZ2UtY2FudmFzIHtcbiAgd2lkdGg6IDgwJTtcbn1cbi5mb3JtIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTJkb3Q0IC5jYXJkIC5jYXJkLWNvbnRlbnQgLnVwbG9hZC1kZXNjIHtcbiAgY29sb3I6ICNiYmIgIWltcG9ydGFudDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDE0cHg7XG59XG4uZm9ybSAuc2VjdGlvbi11cGxvYWQgLmNvbC1tZC0yZG90NCAuY2FyZCAuY2FyZC1jb250ZW50IC5sb2dvLXVwbG9hZCB7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbn1cbi5mb3JtIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTJkb3Q0IC5jYXJkIC5jYXJkLWNvbnRlbnQgLmltYWdlIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZsb2F0OiBsZWZ0O1xuICBoZWlnaHQ6IGF1dG87XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG59XG4uZm9ybSAuc2VjdGlvbi11cGxvYWQgLmNvbC1tZC0yZG90NCAuY2FyZCAuY2FyZC1jb250ZW50IC5pbWFnZSAuY3VzdG9tIHtcbiAgaGVpZ2h0OiAxMDAlO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbn1cbi5mb3JtIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTJkb3Q0IC5jYXJkIC5jYXJkLWNvbnRlbnQgLmltYWdlIC5jdXN0b20gLmRyYWctZHJvcCB7XG4gIGZvbnQtc2l6ZTogMTBweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmZvcm0gLnNlY3Rpb24tdXBsb2FkIC5jb2wtbWQtMmRvdDQgLmNhcmQgLmNhcmQtY29udGVudCAuaW1hZ2UgLmN1c3RvbSAuc2V0LW9wYWNpdHkge1xuICBvcGFjaXR5OiAwLjE7XG4gIGhlaWdodDogNjBweDtcbiAgd2lkdGg6IGF1dG87XG59XG4uZm9ybSAuc2VjdGlvbi11cGxvYWQgLmNvbC1tZC0yZG90NCAuY2FyZCAuZm9ybS1ncm91cC51cGxvYWRlZCB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICBoZWlnaHQ6IDEwMCU7XG59XG4uZm9ybSAuc2VjdGlvbi11cGxvYWQgLmNvbC1tZC0yZG90NCAuY2FyZCAuZm9ybS1ncm91cC51cGxvYWRlZCAucmVtb3ZlLWljb24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAwO1xuICB0b3A6IDA7XG4gIGNvbG9yOiByZWQ7XG4gIGZvbnQtc2l6ZTogMjJweDtcbn1cbi5mb3JtIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTJkb3Q0IC5jYXJkIC5mb3JtLWdyb3VwLnVwbG9hZGVkIC5sb2FkaW5nIHtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuLmZvcm0gLnNlY3Rpb24tdXBsb2FkIC5jb2wtbWQtMmRvdDQgLmNhcmQgLmZvcm0tZ3JvdXAudXBsb2FkZWQgLmxvYWRpbmcgLnNrLWZhZGluZy1jaXJjbGUge1xuICB0b3A6IDUwJTtcbn1cbi5mb3JtIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTJkb3Q0IC5jYXJkIC5mb3JtLWdyb3VwLnVwbG9hZGVkIC51cGxvYWRlZC1pbWFnZTIge1xuICBoZWlnaHQ6IDEwMCU7XG59XG4uZm9ybSAuc2VjdGlvbi11cGxvYWQgLmNvbC1tZC0yZG90NCAuY2FyZCAuZm9ybS1ncm91cC51cGxvYWRlZCAudXBsb2FkZWQtaW1hZ2UyIC5pbWcge1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5mb3JtIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTJkb3Q0IC5jYXJkIC5mb3JtLWdyb3VwLnVwbG9hZGVkIC51cGxvYWRlZC1pbWFnZTIgLmltZyA+IGltZyB7XG4gIGhlaWdodDogMTAwJTtcbiAgbWF4LWhlaWdodDogMzIwcHg7XG59XG4uZm9ybSAuc2VjdGlvbi11cGxvYWQgLmNvbC1tZC0yZG90NCAuY2FyZCAuZm9ybS1ncm91cC51cGxvYWRlZCAudXBsb2FkZWQtaW1hZ2UyIC5lZGl0b3ItdG9vbCB7XG4gIGhlaWdodDogMTAwJTtcbn1cbi5mb3JtIC5jYXJkIHtcbiAgYm94LXNoYWRvdzogMCAxcHggMTFweCAwIHJnYmEoMCwgMCwgMCwgMC4yKSwgMCAycHggM3B4IDAgcmdiYSgwLCAwLCAwLCAwLjE5KTtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbn1cbi5mb3JtIC5jYXJkIC5jYXJkLWhlYWRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICBtYXJnaW46IDBweDtcbiAgcGFkZGluZzogMHB4O1xuICBib3JkZXItbGVmdDogM3B4IHNvbGlkICMyNDgwZmI7XG59XG4uZm9ybSAuY2FyZCAuY2FyZC1oZWFkZXIgbGFiZWwge1xuICBtYXJnaW46IDIwcHggMTBweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZvbnQtc2l6ZTogMjBweDtcbn1cbi5mb3JtIC5jYXJkIC51cGxvYWRlZCB7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAjY2NjO1xufVxuLmZvcm0gLmVuZC1mb290ZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5mb3JtIC5lbmQtZm9vdGVyIC5mb290ZXItc2F2ZS1jYW5jZWwge1xuICBtYXJnaW46IDUwcHg7XG59XG4uZm9ybSAuZW5kLWZvb3RlciAuZm9vdGVyLXNhdmUtY2FuY2VsIC5jYW5jZWwge1xuICBib3JkZXItY29sb3I6ICMyNDgwZmI7XG4gIG1hcmdpbi1yaWdodDogMjBweDtcbiAgY29sb3I6ICMyNDgwZmI7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDIwcHg7XG4gIHBhZGRpbmc6IDE1cHg7XG4gIHdpZHRoOiAyNXJlbTtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbn1cbi5mb3JtIC5lbmQtZm9vdGVyIC5mb290ZXItc2F2ZS1jYW5jZWwgLnNhdmUge1xuICBmb250LXNpemU6IDIwcHg7XG4gIHBhZGRpbmc6IDE1cHg7XG4gIHdpZHRoOiAyNXJlbTtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzI0ODBmYjtcbn1cbi5mb3JtIC5lbmQtZm9vdGVyIC5kZWxldGUge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGZsb2F0OiByaWdodDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U3NGMzYztcbiAgb3BhY2l0eTogMC41O1xuICBib3JkZXItY29sb3I6ICNjMDM5MmI7XG59XG4uZm9ybSAuZW5kLWZvb3RlciAuZGVsZXRlIC5kZWxldGUtbW9kYWwge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGRpc3BsYXk6IG5vbmU7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBwYWRkaW5nOiA0cHggMTBweDtcbiAgdG9wOiAwcHg7XG4gIHJpZ2h0OiAwcHg7XG4gIGNvbG9yOiAjZTc0YzNjO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTQ1cHgpO1xufVxuLmZvcm0gLmVuZC1mb290ZXIgLmRlbGV0ZSAuZGVsZXRlLW1vZGFsIC5idG4tZ3JvdXAge1xuICBib3JkZXI6IDFweCBzb2xpZCAjZTc0YzNjO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG4uZm9ybSAuZW5kLWZvb3RlciAuZGVsZXRlIC5kZWxldGUtbW9kYWwgLmJ0bi1ncm91cCAubm8ge1xuICBvcGFjaXR5OiAwLjU7XG4gIGJvcmRlci13aWR0aDogMHB4O1xufVxuLmZvcm0gLmVuZC1mb290ZXIgLmRlbGV0ZSAuZGVsZXRlLW1vZGFsIC5idG4tZ3JvdXAgLnllcyB7XG4gIGJhY2tncm91bmQ6ICNlNzRjM2M7XG4gIGJvcmRlci13aWR0aDogMHB4O1xufVxuLmZvcm0gLmVuZC1mb290ZXIgLmRlbGV0ZSAuZGVsZXRlLW1vZGFsIC5idG4tZ3JvdXAgLm5vOmhvdmVyIHtcbiAgb3BhY2l0eTogMTtcbn1cbi5mb3JtIC5lbmQtZm9vdGVyIC5kZWxldGU6aG92ZXIsXG4uZm9ybSAuZW5kLWZvb3RlciAuZGVsZXRlOmZvY3VzIHtcbiAgb3BhY2l0eTogMTtcbn1cbi5mb3JtIC5lbmQtZm9vdGVyIC5kZWxldGU6Zm9jdXMgLmRlbGV0ZS1tb2RhbCB7XG4gIGRpc3BsYXk6IGJsb2NrO1xufVxuLmZvcm0gLnZhcmlhbi1idXR0b24ge1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG4uZm9ybSAudmFyaWFuLWltYWdlLXBhcmVudCB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5mb3JtIC52YXJpYW4tdGFibGUge1xuICBtYXJnaW4tdG9wOiA0MHB4O1xufVxuLmZvcm0gLmRlbGV0ZS12YXJpYW4taW1hZ2Uge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAyMnB4O1xuICB0b3A6IDIycHg7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAyNHB4O1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG4uZm9ybSAudmFyaWFuLXBhcmVudCB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5mb3JtIC52YXJpYW4tcGFyZW50IDo6bmctZGVlcCAuaW5wdXQtZ3JvdXAge1xuICB3aWR0aDogOTElO1xufVxuLmZvcm0gLnZhcmlhbi1wYXJlbnQgaSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgcmlnaHQ6IDIwcHg7XG4gIHRvcDogMTBweDtcbn1cbi5mb3JtIC5hZGQtdmFsdWUge1xuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xuICBtYXJnaW4tdG9wOiA1cHg7XG59XG4uZm9ybSAucmVkLWFsZXJ0IHtcbiAgYm9yZGVyOiBzb2xpZCAxcHggcmVkO1xufVxuLmZvcm0gLnJlZC1lcnJvci1hbGVydCB7XG4gIGNvbG9yOiByZWQ7XG4gIGZvbnQtc2l6ZTogMTJweDtcbn1cblxuLmVycm9yLW1lc3NhZ2Uge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgaGVpZ2h0OiAxMDB2aDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDI2cHg7XG4gIHBhZGRpbmctdG9wOiA0MHB4O1xufVxuLmVycm9yLW1lc3NhZ2UgLnRleHQtbWVzc2FnZSB7XG4gIGJhY2tncm91bmQ6ICNFNzRDM0M7XG4gIHBhZGRpbmc6IDIwcHg7XG4gIG1hcmdpbjogMCAxMHB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/merchant-portal/separated-modules/merchant-products/products/merchant-add-new-product/merchant-add-new-product.component.ts":
/*!****************************************************************************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/separated-modules/merchant-products/products/merchant-add-new-product/merchant-add-new-product.component.ts ***!
  \****************************************************************************************************************************************************/
/*! exports provided: MerchantAddNewProductComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MerchantAddNewProductComponent", function() { return MerchantAddNewProductComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_product_product_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../../services/product/product.service */ "./src/app/services/product/product.service.ts");
/* harmony import */ var ngx_bootstrap_chronos__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-bootstrap/chronos */ "./node_modules/ngx-bootstrap/chronos/fesm5/ngx-bootstrap-chronos.js");
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ckeditor/ckeditor5-build-classic */ "./node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js");
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _services_category_category_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../../services/category/category.service */ "./src/app/services/category/category.service.ts");
/* harmony import */ var _services_merchant_merchant_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../../services/merchant/merchant.service */ "./src/app/services/merchant/merchant.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_observerable_permission_observer__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../../services/observerable/permission-observer */ "./src/app/services/observerable/permission-observer.ts");
/* harmony import */ var _object_interface_common_object__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../../../object-interface/common.object */ "./src/app/object-interface/common.object.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
// Dear Future Developer, please consider leaving notes on your code, good habits will reduce development time ~ H
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};











// import { throwMatDialogContentAlreadyAttachedError } from '@angular/material';
// import { SSL_OP_SSLEAY_080_CLIENT_DH_BUG } from 'constants';
var MerchantAddNewProductComponent = /** @class */ (function () {
    function MerchantAddNewProductComponent(productService, categoryService, merchantService, permission, router, _location) {
        var _this = this;
        this.productService = productService;
        this.categoryService = categoryService;
        this.merchantService = merchantService;
        this.permission = permission;
        this.router = router;
        this._location = _location;
        this.arrayVariationValues = [];
        this.currentX = 0;
        this.currentY = 0;
        this.Editor = _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_3___default.a;
        this.config = {
            toolbar: ['heading', '|', 'bold', 'italic', 'bulletedList', 'numberedList', 'blockQuote'],
            heading: {
                options: [
                    { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
                    { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
                    { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' }
                ]
            }
        };
        this.autoSKU = false;
        this.hashtagsKey = "";
        this.listOfProductType = [
            { name: 'Product', value: 'product', active: true },
            { name: 'Gold', value: 'gold', active: false },
            { name: 'Voucher Fisik', value: 'voucher', active: false },
            { name: 'E-Voucher', value: 'e-voucher', active: false },
            { name: 'E-Wallet', value: 'top-up', active: false },
        ];
        this.listOfRedeemType = [
            { name: 'voucher_link', active: true },
            { name: 'voucher_code', active: false }
        ];
        this.gallery_label = ['image3', 'image1', 'image2', 'image4', 'image5'];
        this.listOfVendorType = [];
        this.listOfMerchant = [];
        this.form = {
            min_order: 1,
            product_name: '',
            product_code: '',
            product_price: 0,
            product_value: 0,
            description: '',
            variation: {},
            type: 'product',
            category: '',
            status: 'ACTIVE',
            weight: 1,
            merchant_username: '',
            voucher_group: '',
            redeem_type: '',
            price: 0,
            fixed_price: 0,
            qty: 1,
            dimensions: {
                width: 1,
                length: 1,
                height: 1
            },
            images_gallery: [],
            product_sku: [],
            tnc: '',
            condition: '',
            active: 0,
            need_outlet_code: 0,
            hashtags: [],
            applicable_for_ios: true,
            weightInGram: 1000,
        };
        this.title = 'Product';
        this.uploads = {
            image1: '',
            image2: '',
            image3: '',
            image4: '',
            image5: '',
        };
        this.labels = {
            type: 'product type',
            min_order: 'minimum order',
            product_code: 'product code',
            product_name: 'product name',
            merchant_username: 'merchant username',
            category: 'category',
            price: 'price',
            fixed_price: 'public price',
            qty: 'quantity',
            description: 'description',
            tnc: 'term and conditions',
            dimensions: 'dimensions',
            weight: 'weight'
        };
        this.temp_gallery = [];
        this.errorLabel = false;
        this.errorMessage = false;
        this.isFileUploaded = false;
        this.multipleCategories = [];
        this.data1 = {};
        this.data2 = {};
        this.data3 = {};
        this.data4 = {};
        this.data5 = {};
        this.showLoading = false;
        this.showLoading2 = false;
        this.showLoading3 = false;
        this.showLoading4 = false;
        this.showLoading5 = false;
        this.varianHeader = [];
        this.dataVarian = [];
        this.dataVarianModel = [];
        this.isLoading = [];
        this.isloading = [];
        this.categoryList = [];
        this.headerValue = [
            'SKU Code',
            'SKU Price',
            'SKU Value',
            'Stok',
            'Gambar',
            'Status',
        ];
        this.valueVariation = [
            ''
        ];
        this.productSku = [];
        this.productStatus = [
            { label: 'ACTIVE', value: 'ACTIVE', selected: 1 },
            { label: 'INACTIVE', value: 'INACTIVE' }
        ];
        this.categoryAlert = false;
        this.photoAlert = false;
        this.productNameAlert = false;
        this.productCodeAlert = false;
        this.descriptionAlert = false;
        this.tncAlert = false;
        this.productPriceAlert = false;
        this.productValueAlert = false;
        this.weightAlert = false;
        this.widthAlert = false;
        this.lengthAlert = false;
        this.heightAlert = false;
        this.varianAlert = false;
        this.varianAlertMessage = '';
        this.varianX = {
            name: '',
            active: false
        };
        this.varianY = {
            name: '',
            active: false
        };
        this.varianTypeX = [];
        this.varianTypeY = [];
        this.fieldVarianTypeX = [];
        this.fieldVarianTypeY = [];
        this.multiVarian = [];
        this.fieldVarianX = {
            alert: false,
            message: ""
        };
        this.fieldVarianY = {
            alert: false,
            message: ""
        };
        this.scrollView = function (id, alert, scroll, objectScroll) {
            if (alert === void 0) { alert = ''; }
            if (scroll === void 0) { scroll = true; }
            if (objectScroll === void 0) { objectScroll = false; }
            if (alert)
                _this[alert] = true;
            var defObject = objectScroll ? objectScroll : {
                behavior: "smooth",
                block: "start",
                inline: "nearest"
            };
            if (scroll)
                document.getElementById(id).scrollIntoView(defObject);
            return false;
        };
        this.eventValidation = function (event) {
            _this.validateMultiVarian(false, false);
        };
        this.validateMultiVarian = function (valReturn, scroll) {
            if (valReturn === void 0) { valReturn = false; }
            if (scroll === void 0) { scroll = true; }
            _this.varianAlertMessage = '*data varian harus lengkap';
            var isFormValid = true;
            _this.fieldVarianTypeX = [];
            _this.fieldVarianTypeY = [];
            _this.varianX.name = _this.varianX.name.toString().trim();
            _this.varianY.name = _this.varianY.name.toString().trim();
            _this.fieldVarianX = !_this.varianX.name ? { alert: true, message: 'field ini tidak boleh kosong' } : { alert: false, message: '' };
            _this.fieldVarianY = _this.varianY.active && !_this.varianY.name ? { alert: true, message: 'field ini tidak boleh kosong' } : { alert: false, message: '' };
            if (!_this.varianX.name || (_this.varianY.active && !_this.varianY.name)) {
                isFormValid = _this.scrollView('varian', 'varianAlert', scroll);
            }
            else {
                _this.varianAlert = false;
            }
            var reg = new RegExp('^[0-9]+$');
            if (_this.varianX.active && reg.test(_this.varianX.name)) {
                _this.fieldVarianX = _this.fieldVarianX.alert ? _this.fieldVarianX : { alert: true, message: 'Tipe varian tidak boleh berupa angka' };
                isFormValid = _this.scrollView('varian', 'varianAlert', scroll);
            }
            else {
                _this.varianAlert = false;
            }
            if (_this.varianY.active && reg.test(_this.varianY.name)) {
                _this.fieldVarianY = _this.fieldVarianY.alert ? _this.fieldVarianY : { alert: true, message: 'Tipe varian tidak boleh berupa angka' };
                isFormValid = _this.scrollView('varian', 'varianAlert', scroll);
            }
            else {
                _this.varianAlert = false;
            }
            if (_this.varianY.active && (_this.varianX.name.toLowerCase() == _this.varianY.name.toLowerCase())) {
                _this.fieldVarianX = _this.fieldVarianX.alert ? _this.fieldVarianX : { alert: true, message: 'Tipe varian tidak boleh memiliki value yang sama' };
                _this.fieldVarianY = _this.fieldVarianY.alert ? _this.fieldVarianY : { alert: true, message: 'Tipe varian tidak boleh memiliki value yang sama' };
                isFormValid = _this.scrollView('varian', 'varianAlert', scroll);
            }
            else {
                _this.varianAlert = false;
            }
            if (_this.varianX.active) {
                _this.varianTypeX.forEach(function (el, idx) {
                    _this.varianTypeX[idx] = el.toString().trim();
                    if (_this.varianTypeX[idx]) {
                        _this.varianAlert = false;
                    }
                    else {
                        _this.fieldVarianTypeX[idx] = {
                            alert: true,
                            message: '*field ini tidak boleh kosong'
                        };
                        isFormValid = _this.scrollView('varian', 'varianAlert', scroll);
                    }
                });
                var foundEqual = false;
                for (var i = 0; i < _this.varianTypeX.length; i++) {
                    var find = false;
                    for (var j = 0; j < _this.varianTypeX.length; j++) {
                        if (i != j) {
                            if (_this.varianTypeX[i].toLowerCase() == _this.varianTypeX[j].toLowerCase()) {
                                find = true;
                                if (!_this.fieldVarianTypeX[j] || _this.fieldVarianTypeX[j].alert == false) {
                                    _this.fieldVarianTypeX[j] =
                                        {
                                            alert: true,
                                            message: '*field tidak boleh memiliki nilai sama'
                                        };
                                    // break;
                                }
                            }
                        }
                        if (find) {
                            foundEqual = true;
                            // break;
                        }
                    }
                }
                if (foundEqual) {
                    isFormValid = _this.scrollView('varian', 'varianAlert', scroll);
                }
                else {
                    _this.varianAlert = false;
                }
            }
            if (_this.varianY.active) {
                _this.varianTypeY.forEach(function (el, idx) {
                    console.log("element Y", el);
                    _this.varianTypeY[idx] = el.toString().trim();
                    if (_this.varianTypeY[idx]) {
                        _this.varianAlert = false;
                    }
                    else {
                        _this.fieldVarianTypeY[idx] = {
                            alert: true,
                            message: '*field ini tidak boleh kosong'
                        };
                        isFormValid = _this.scrollView('varian', 'varianAlert', scroll);
                    }
                });
                var foundEqual = false;
                for (var i = 0; i < _this.varianTypeY.length; i++) {
                    var find = false;
                    for (var j = 0; j < _this.varianTypeY.length; j++) {
                        if (i != j) {
                            if (_this.varianTypeY[i].toLowerCase() == _this.varianTypeY[j].toLowerCase()) {
                                find = true;
                                if (!_this.fieldVarianTypeY[j] || _this.fieldVarianTypeY[j].alert == false) {
                                    _this.fieldVarianTypeY[j] =
                                        {
                                            alert: true,
                                            message: '*field tidak boleh memiliki nilai sama'
                                        };
                                    // break;
                                }
                            }
                        }
                    }
                    if (find) {
                        foundEqual = true;
                        // break;
                    }
                }
                if (foundEqual) {
                    isFormValid = _this.scrollView('varian', 'varianAlert', scroll);
                }
                else {
                    _this.varianAlert = false;
                }
            }
            var allSkuCode = [];
            for (var i = 0; i < _this.multiVarian.length; i++) {
                for (var x = 0; x < _this.multiVarian[i].length; x++) {
                    if (!_this.multiVarian[i][x].sku_code) {
                        isFormValid = _this.scrollView('varian', 'varianAlert', scroll);
                    }
                    else if (!_this.multiVarian[i][x].sku_price) {
                        isFormValid = _this.scrollView('varian', 'varianAlert', scroll);
                    }
                    else if (!_this.multiVarian[i][x].sku_value) {
                        isFormValid = _this.scrollView('varian', 'varianAlert', scroll);
                    }
                    else if (!_this.multiVarian[i][x].status) {
                        isFormValid = _this.scrollView('varian', 'varianAlert', scroll);
                    }
                    else if (_this.multiVarian[i][x].qty < 0) {
                        isFormValid = _this.scrollView('varian', 'varianAlert', scroll);
                    }
                    else {
                        _this.varianAlert = false;
                        allSkuCode.push(_this.multiVarian[i][x].sku_code);
                    }
                }
            }
            if (isFormValid && allSkuCode.length > 0) {
                var foundEqual = false;
                for (var i = 0; i < allSkuCode.length; i++) {
                    for (var j = 0; j < allSkuCode.length; j++) {
                        if (i != j) {
                            if (allSkuCode[j].toLowerCase() == allSkuCode[i].toLowerCase()) {
                                foundEqual = true;
                            }
                        }
                        if (foundEqual)
                            break;
                    }
                    if (foundEqual)
                        break;
                }
                if (foundEqual) {
                    isFormValid = _this.scrollView('varian', 'varianAlert', scroll);
                    _this.varianAlertMessage = '*terdapat SKU Code yang sama';
                }
            }
            if (valReturn)
                return isFormValid;
        };
        this.permission.currentPermission.subscribe(function (val) {
            _this.currentPermission = val;
            if (_this.currentPermission == 'merchant') {
                _this.listOfProductType = [];
                _this.form.type = 'product';
            }
        });
    }
    MerchantAddNewProductComponent.prototype.clickVarian = function () {
        this.addVariationType();
        this.form.product_price = 0;
        this.form.product_value = 0;
    };
    MerchantAddNewProductComponent.prototype.changeText = function () {
        var _a, _b;
        try {
            this.productSku = [];
            this.isLoading = [];
            for (var i = 0; i < this.dataVarianModel[0].length; i++) {
                if (this.dataVarianModel[1]) {
                    for (var j = 0; j < this.dataVarianModel[1].length; j++) {
                        this.productSku.push({
                            "variant": (_a = {},
                                _a[this.varianHeader[0]] = this.dataVarianModel[0][i],
                                _a[this.varianHeader[1]] = this.dataVarianModel[1][j],
                                _a),
                            "image_gallery": [],
                        });
                        this.isLoading.push(false);
                    }
                }
                else {
                    this.productSku.push({
                        "variant": (_b = {},
                            _b[this.varianHeader[0]] = this.dataVarianModel[0][i],
                            _b),
                        "image_gallery": [],
                    });
                    this.isLoading.push(false);
                }
            }
        }
        catch (error) {
        }
    };
    MerchantAddNewProductComponent.prototype.addVariationType = function () {
        if (this.multiVarian.length < 1) {
            this.varianX.active = true;
            this.addVarianTypeY('');
            this.addVarianTypeX('');
        }
    };
    MerchantAddNewProductComponent.prototype.activatedVarianY = function () {
        this.varianY.active = true;
        this.validateMultiVarian(false, false);
    };
    MerchantAddNewProductComponent.prototype.addVarianTypeX = function (fieldX) {
        if (fieldX === void 0) { fieldX = ''; }
        this.varianTypeX.push(fieldX);
        for (var i = 0; i < this.varianTypeY.length; i++) {
            this.multiVarian[i].push(this.generateProductSKU(this.varianTypeY[i], fieldX));
        }
        this.validateMultiVarian(false, false);
    };
    MerchantAddNewProductComponent.prototype.deleteVarianTypeX = function (index) {
        for (var i = 0; i < this.varianTypeY.length; i++) {
            this.multiVarian[i].splice(index, 1);
        }
        this.varianTypeX.splice(index, 1);
        this.validateMultiVarian(false, false);
    };
    MerchantAddNewProductComponent.prototype.addVarianTypeY = function (fieldY) {
        if (fieldY === void 0) { fieldY = ''; }
        this.varianTypeY.push(fieldY);
        var arrayY = [];
        for (var i = 0; i < this.varianTypeX.length; i++) {
            arrayY.push(this.generateProductSKU(fieldY, this.varianTypeX[i]));
        }
        this.multiVarian.push(arrayY);
        this.validateMultiVarian(false, false);
    };
    MerchantAddNewProductComponent.prototype.deleteVarianTypeY = function (index) {
        this.multiVarian.splice(index, 1);
        this.varianTypeY.splice(index, 1);
        this.validateMultiVarian(false, false);
    };
    MerchantAddNewProductComponent.prototype.deleteAllTypeY = function () {
        if (this.varianX.active) {
            this.multiVarian.splice(1, this.multiVarian.length - 1);
            this.varianTypeY = [''];
            this.varianY.active = false;
            this.varianY.name = '';
            this.fieldVarianX = {
                alert: false,
                message: ''
            };
            this.fieldVarianY = {
                alert: false,
                message: ''
            };
            this.validateMultiVarian(false, false);
        }
        else {
            this.deleteAllVarian();
        }
    };
    MerchantAddNewProductComponent.prototype.deleteAllTypeX = function () {
        if (this.varianY.active) {
            var tempX = [];
            for (var i = 0; i < this.multiVarian.length; i++) {
                tempX.push(this.multiVarian[i][0]);
            }
            this.multiVarian = [tempX];
            this.varianTypeX = this.varianTypeY.slice();
            this.varianX.name = this.varianY.name;
            this.varianTypeY = [''];
            this.varianY.active = false;
            this.varianY.name = '';
            this.fieldVarianX = {
                alert: false,
                message: ''
            };
            this.fieldVarianY = {
                alert: false,
                message: ''
            };
        }
        else {
            this.deleteAllVarian();
        }
        this.validateMultiVarian(false, false);
    };
    MerchantAddNewProductComponent.prototype.deleteAllVarian = function () {
        this.varianX = {
            name: '',
            active: false
        };
        this.varianY = {
            name: '',
            active: false
        };
        this.varianTypeX = [];
        this.varianTypeY = [];
        this.multiVarian = [];
        this.fieldVarianX = {
            alert: false,
            message: ''
        };
        this.fieldVarianY = {
            alert: false,
            message: ''
        };
    };
    MerchantAddNewProductComponent.prototype.generateProductSKU = function (valueY, valueX) {
        if (valueY === void 0) { valueY = ''; }
        if (valueX === void 0) { valueX = ''; }
        return {
            sku_code: '',
            sku_price: '',
            sku_value: '',
            qty: 0,
            image_gallery: [],
            status: 'ACTIVE',
            isloading: false,
        };
    };
    MerchantAddNewProductComponent.prototype.addArrayValue = function (type, count) {
        for (var x = 0; x < this.currentX; x++) {
            for (var y = 0; y < this.currentY; y++) {
            }
        }
    };
    MerchantAddNewProductComponent.prototype.deleteVariationType = function (indexValue) {
        this.dataVarian.splice(indexValue, 1);
        this.dataVarianModel.splice(indexValue, 1);
    };
    MerchantAddNewProductComponent.prototype.addVariationValue = function (index) {
        this.dataVarian[index].push('');
        this.dataVarianModel[index].push('');
    };
    MerchantAddNewProductComponent.prototype.deleteVariationValue = function (index, indexValue) {
        this.dataVarian[index].splice(indexValue, 1);
        this.dataVarianModel[index].splice(indexValue, 1);
    };
    MerchantAddNewProductComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, e_1;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        if (this.type == "e-voucher") {
                            this.listOfProductType[2].active = true;
                            this.setProductType(this.listOfProductType[2]);
                        }
                        this.formPriceReplacer(null, 'product_price');
                        this.formPriceReplacer(null, 'weightInGram');
                        _a = this;
                        return [4 /*yield*/, this.productService.getCategoryLint()];
                    case 1:
                        _a.categoryList = _b.sent();
                        this.categoryList.forEach(function (element) {
                            console.log("element", element);
                            if (element.type == "product") {
                                _this.categoryProduct = element.category_list;
                            }
                            if (element.type == "voucher") {
                                _this.categoryVoucher = element.category_list;
                            }
                            if (element.type == "e-voucher") {
                                _this.categoryEvoucher = element.category_list;
                            }
                            if (element.type == "gold") {
                                _this.categoryGold = element.category_list;
                            }
                            if (element.type == "top-up") {
                                _this.categoryEwallet = element.category_list;
                            }
                        });
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _b.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
                        }
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MerchantAddNewProductComponent.prototype.copyInputMessage = function (inputElement) {
        inputElement.select();
        document.execCommand('copy');
        inputElement.setSelectionRange(0, 0);
    };
    MerchantAddNewProductComponent.prototype.productNameInputed = function ($event) {
        if (this.autoSKU == true) {
            var s = this.form.product_name.toLowerCase().replace(/[\s\?\*]/g, '-');
            this.form.product_code = s;
        }
    };
    MerchantAddNewProductComponent.prototype.addHashtags = function (newWord) {
        this.form.hashtags.push(newWord);
    };
    MerchantAddNewProductComponent.prototype.hashtagsKeyDown = function (event) {
        var n = event.target.textContent;
        var key = event.key.toLowerCase();
        // console.log("key", key)
        if (key == ' ' || key == 'enter') {
            if (n.trim() !== '') {
                this.addHashtags(n);
            }
            event.target.textContent = '';
            event.preventDefault();
        }
        if (key == 'backspace' && n == '' && this.form.hashtags.length > 0) {
            this.form.hashtags.pop();
        }
    };
    MerchantAddNewProductComponent.prototype.formPriceReplacer = function ($event, label) {
        if (this.form[label]) {
            if (typeof this.form[label] == 'number')
                this.form[label] = this.form[label].toString();
            var s = this.form[label].toLowerCase().replace(/[^0-9]/g, '');
            var c = parseInt(s);
            if (isNaN(c)) {
                this.form[label] = 0;
            }
            else {
                this.form[label] = c.toLocaleString('en');
            }
        }
    };
    MerchantAddNewProductComponent.prototype.onFileSelected2 = function (event, img) {
        return __awaiter(this, void 0, void 0, function () {
            var reader, fileMaxSize_1, logo, e_2;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        reader = new FileReader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, , 5]);
                        this.errorFile = false;
                        fileMaxSize_1 = 2064000;
                        Array.from(event.target.files).forEach(function (file) {
                            if (file.size > fileMaxSize_1) {
                                _this.errorFile = 'Maximum File Upload is 2MB';
                            }
                        });
                        this.selectedFile = event.target.files[0];
                        reader.onload = function (event) {
                            _this.selFile = event.target.result;
                        };
                        if (event.target.files[0]) {
                            reader.readAsDataURL(event.target.files[0]);
                        }
                        if (!this.selectedFile) return [3 /*break*/, 3];
                        switch (img) {
                            case 'image1':
                                this.showLoading = true;
                                break;
                            case 'image2':
                                this.showLoading2 = true;
                                break;
                            case 'image3':
                                this.showLoading3 = true;
                                break;
                            case 'image4':
                                this.showLoading4 = true;
                                break;
                            case 'image5':
                                this.showLoading5 = true;
                                break;
                        }
                        return [4 /*yield*/, this.productService.upload(this.selectedFile, this, 'image', function (result) {
                                _this.callImage(result, img);
                            })];
                    case 2:
                        logo = _a.sent();
                        _a.label = 3;
                    case 3: return [3 /*break*/, 5];
                    case 4:
                        e_2 = _a.sent();
                        this.errorLabel = e_2.message; //conversion to Error type
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    MerchantAddNewProductComponent.prototype.deleteImage = function (img) {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_9___default.a.fire({
            title: 'Are you sure?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then(function (result) {
            if (result.isConfirmed) {
                var a = _this.gallery_label.indexOf(img);
                _this.uploads[img] = '';
                _this.temp_gallery.splice(a, 1);
                sweetalert2__WEBPACK_IMPORTED_MODULE_9___default.a.fire('Deleted!', 'Your file has been deleted.', 'success');
            }
        });
    };
    MerchantAddNewProductComponent.prototype.callImage = function (result, img) {
        if (img == 'image3') {
            var product = this.form;
            this.form = __assign({}, product, result);
        }
        this.uploads[img] = result.base_url + result.pic_big_path;
        switch (img) {
            case "image1":
                this.data1 = result;
                break;
            case "image2":
                this.data2 = result;
                break;
            case "image3":
                this.data3 = result;
                break;
            case "image4":
                this.data4 = result;
                break;
            case "image5":
                this.data5 = result;
                break;
        }
        this.allfalse();
    };
    MerchantAddNewProductComponent.prototype.onFileVariant = function (event, image_gallery, index) {
        return __awaiter(this, void 0, void 0, function () {
            var reader, fileMaxSize_2, logo, e_3;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        reader = new FileReader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, , 5]);
                        this.errorFile = false;
                        fileMaxSize_2 = 2064000;
                        Array.from(event.target.files).forEach(function (file) {
                            if (file.size > fileMaxSize_2) {
                                _this.errorFile = 'Maximum File Upload is 2MB';
                            }
                        });
                        this.selectedFile = event.target.files[0];
                        reader.onload = function (event) {
                            _this.selFile = event.target.result;
                        };
                        if (event.target.files[0]) {
                            reader.readAsDataURL(event.target.files[0]);
                        }
                        if (!this.selectedFile) return [3 /*break*/, 3];
                        this.multiVarian[index[0]][index[1]].isloading = true;
                        return [4 /*yield*/, this.productService.upload(this.selectedFile, this, 'image', function (result) {
                                image_gallery.push(result);
                                _this.multiVarian[index[0]][index[1]].isloading = false;
                            })];
                    case 2:
                        logo = _a.sent();
                        _a.label = 3;
                    case 3: return [3 /*break*/, 5];
                    case 4:
                        e_3 = _a.sent();
                        this.errorLabel = e_3.message; //conversion to Error type
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    MerchantAddNewProductComponent.prototype.deleteVarianImage = function (image_gallery) {
        sweetalert2__WEBPACK_IMPORTED_MODULE_9___default.a.fire({
            title: 'Are you sure?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then(function (result) {
            if (result.isConfirmed) {
                image_gallery.length = 0;
                sweetalert2__WEBPACK_IMPORTED_MODULE_9___default.a.fire('Deleted!', 'Your file has been deleted.', 'success');
            }
        });
    };
    MerchantAddNewProductComponent.prototype.allfalse = function () {
        this.showLoading = this.showLoading2 = this.showLoading3
            = this.showLoading4 = this.showLoading5 = false;
    };
    MerchantAddNewProductComponent.prototype.onFileSelected = function (event) {
        var _this = this;
        this.isFileUploaded = false;
        this.errorLabel = false;
        var fileMaxSize = 3000000; // let say 3Mb
        var reader = new FileReader();
        reader.onload = function (event) {
            _this.selFile = event.target.result;
        };
        reader.readAsDataURL(event.target.files[0]);
        Array.from(event.target.files).forEach(function (file) {
            if (file.size > fileMaxSize) {
                _this.errorLabel = "maximum file is 3Mb";
            }
        });
        if (this.errorLabel == false) {
            this.selectedFile = event.target.files[0];
            setTimeout(function () {
                _this.onUpload();
            }, 1500);
        }
    };
    MerchantAddNewProductComponent.prototype.onUpload = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_4, message;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        this.cancel = false;
                        result = void 0;
                        if (!this.selectedFile) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.productService.upload(this.selectedFile, this, 'image', function (result) { _this.callAfterUpload(result); })];
                    case 1:
                        result = _a.sent();
                        _a.label = 2;
                    case 2: return [3 /*break*/, 4];
                    case 3:
                        e_4 = _a.sent();
                        this.errorLabel = (e_4.message); //conversion to Error type
                        message = this.errorLabel;
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
                        }
                        sweetalert2__WEBPACK_IMPORTED_MODULE_9___default.a.fire({
                            icon: 'error',
                            title: message,
                        });
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/, false];
                }
            });
        });
    };
    MerchantAddNewProductComponent.prototype.callAfterUpload = function (result) {
        if (result) {
            var product = this.form;
            this.form = __assign({}, product, result);
            this.isFileUploaded = true;
        }
    };
    MerchantAddNewProductComponent.prototype.stringToNumber = function (str) {
        if (typeof str == 'number') {
            str = str + '';
        }
        var s = str.toLowerCase().replace(/[^0-9]/g, '');
        return parseInt(s);
    };
    MerchantAddNewProductComponent.prototype.parseStringToArray = function (str) {
        var splitR;
        if (!Object(_object_interface_common_object__WEBPACK_IMPORTED_MODULE_8__["isString"])(str))
            return [];
        else {
            splitR = str.split(/[\,\.]/);
        }
        return splitR;
    };
    MerchantAddNewProductComponent.prototype.parseStringToArray2 = function (str) {
        var array = [];
        array.push(str);
        return array;
    };
    MerchantAddNewProductComponent.prototype.saveThis = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b, data_1, isFormValid, dataProduct, allProductSKU, i, j, tempProductSKU, result_1, e_5, message;
            var _this = this;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _c.trys.push([0, 3, , 4]);
                        if (this.currentPermission == 'merchant') {
                            delete this.form.merchant_username;
                            delete this.form.merchant_group;
                            delete this.form.redeem_type;
                            delete this.form.oid;
                        }
                        data_1 = this.form;
                        isFormValid = true;
                        this.temp_gallery = [];
                        Object.keys(this.data1).length != 0 ? this.temp_gallery.push(this.data1) : null;
                        Object.keys(this.data2).length != 0 ? this.temp_gallery.push(this.data2) : null;
                        Object.keys(this.data3).length != 0 ? this.temp_gallery.push(this.data3) : null;
                        Object.keys(this.data4).length != 0 ? this.temp_gallery.push(this.data4) : null;
                        Object.keys(this.data5).length != 0 ? this.temp_gallery.push(this.data5) : null;
                        if (this.form.type != "e-voucher" && this.form.type != "top-up" && (this.form.dimensions.height <= 0 || this.form.dimensions.height == "" || this.form.dimensions.height == "0")) {
                            isFormValid = this.scrollView('weight', 'heightAlert');
                        }
                        else {
                            this.heightAlert = false;
                        }
                        if (this.form.type != "e-voucher" && this.form.type != "top-up" && (this.form.dimensions.length <= 0 || this.form.dimensions.length == "" || this.form.dimensions.length == "0")) {
                            isFormValid = this.scrollView('weight', 'lengthAlert');
                        }
                        else {
                            this.lengthAlert = false;
                        }
                        if (this.form.type != "e-voucher" && this.form.type != "top-up" && (this.form.dimensions.width <= 0 || this.form.dimensions.width == "" || this.form.dimensions.width == "0")) {
                            isFormValid = this.scrollView('weight', 'widthAlert');
                        }
                        else {
                            this.widthAlert = false;
                        }
                        if (this.form.type != "e-voucher" && this.form.type != "top-up" && (this.form.weightInGram <= 0 || this.form.weightInGram == "" || this.form.weightInGram == "0")) {
                            isFormValid = this.scrollView('weight', 'weightAlert');
                        }
                        else {
                            this.weightAlert = false;
                        }
                        if ((this.multiVarian && this.multiVarian.length == 0) && (this.form.product_value <= 0 || this.form.product_value == "" || this.form.product_value == "0")) {
                            isFormValid = this.scrollView('price', 'productValueAlert');
                        }
                        else {
                            this.productValueAlert = false;
                        }
                        if (this.multiVarian && this.multiVarian.length > 0) {
                            isFormValid = this.validateMultiVarian(true);
                        }
                        if ((this.multiVarian && this.multiVarian.length == 0) && (this.form.product_price <= 0 || this.form.product_price == "" || this.form.product_price == "0")) {
                            isFormValid = this.scrollView('price', 'productPriceAlert');
                        }
                        else {
                            this.productPriceAlert = false;
                        }
                        if (this.form.type == "e-voucher" && this.form.tnc == "") {
                            isFormValid = this.scrollView('tnc', 'tncAlert');
                        }
                        else {
                            this.tncAlert = false;
                        }
                        if (this.form.description == "") {
                            isFormValid = this.scrollView('description', 'descriptionAlert');
                        }
                        else {
                            this.descriptionAlert = false;
                        }
                        if (this.form.product_code == "") {
                            isFormValid = this.scrollView('product-information', 'productCodeAlert');
                        }
                        else {
                            this.productCodeAlert = false;
                        }
                        if (this.form.product_name == "") {
                            isFormValid = this.scrollView('product-information', 'productNameAlert');
                        }
                        else {
                            this.productNameAlert = false;
                        }
                        if (this.temp_gallery.length <= 0) {
                            isFormValid = this.scrollView('photo-alert', 'photoAlert');
                        }
                        else {
                            this.photoAlert = false;
                        }
                        if (!(isFormValid == true)) return [3 /*break*/, 2];
                        data_1.images_gallery = this.temp_gallery;
                        data_1.price = this.stringToNumber(data_1.price);
                        data_1.fixed_price = this.stringToNumber(data_1.fixed_price);
                        data_1.product_price = this.stringToNumber(data_1.product_price);
                        data_1.product_value = this.stringToNumber(data_1.product_value);
                        data_1.min_order = parseInt(data_1.min_order);
                        data_1.weight = parseFloat(data_1.weight);
                        data_1.dimensions = {
                            length: parseFloat(data_1.type == "e-voucher" ? 0 : data_1.dimensions.length),
                            width: parseFloat(data_1.type == "e-voucher" ? 0 : data_1.dimensions.width),
                            height: parseFloat(data_1.type == "e-voucher" ? 0 : data_1.dimensions.height),
                        };
                        data_1.weightInGram = this.stringToNumber(data_1.weightInGram);
                        data_1.weight = data_1.type == "e-voucher" ? 0 : data_1.weightInGram;
                        data_1.qty = data_1.type == "e-voucher" ? 0 : parseInt(data_1.qty);
                        dataProduct = void 0;
                        allProductSKU = [];
                        for (i = 0; i < this.multiVarian.length; i++) {
                            for (j = 0; j < this.multiVarian[i].length; j++) {
                                tempProductSKU = this.multiVarian[i][j];
                                delete tempProductSKU.isloading;
                                tempProductSKU.variant = (_a = {},
                                    _a[this.varianX.name] = this.varianTypeX[j],
                                    _a);
                                if (this.varianY.active)
                                    tempProductSKU.variant[this.varianY.name] = this.varianTypeY[i];
                                allProductSKU.push(tempProductSKU);
                            }
                        }
                        dataProduct = {
                            product_name: data_1.product_name,
                            product_code: data_1.product_code,
                            product_price: data_1.product_price,
                            product_value: data_1.product_value,
                            qty: data_1.qty,
                            description: data_1.description,
                            type: data_1.type,
                            category: data_1.category,
                            status: data_1.status,
                            weight: data_1.weight,
                            dimensions: data_1.dimensions,
                            images_gallery: data_1.images_gallery,
                        };
                        if (this.varianX.active) {
                            dataProduct.product_sku = allProductSKU;
                            dataProduct.variation = (_b = {},
                                _b[this.varianX.name] = this.varianTypeX,
                                _b);
                            if (this.varianY.active)
                                dataProduct.variation[this.varianY.name] = this.varianTypeY;
                        }
                        else {
                            dataProduct.variation = [];
                            dataProduct.product_sku = [];
                        }
                        if (data_1.type == "e-voucher") {
                            dataProduct.tnc = data_1.tnc;
                        }
                        console.log("data product", dataProduct);
                        return [4 /*yield*/, this.productService.addProducts(dataProduct)];
                    case 1:
                        result_1 = _c.sent();
                        this.newData = 'new';
                        sweetalert2__WEBPACK_IMPORTED_MODULE_9___default.a.fire("Product has been added!").then(function () {
                            if (_this.currentPermission == 'merchant') {
                                _this.router.navigate(['merchant-portal/product-list'], { queryParams: { id: result_1.result._id, newData: _this.newData } });
                            }
                            else if (_this.currentPermission == 'admin' && data_1.type == "e-voucher") {
                                _this.router.navigate(['administrator/vouchers']);
                            }
                            else if (_this.currentPermission == 'admin') {
                                _this.router.navigate(['administrator/productadmin']);
                            }
                        });
                        _c.label = 2;
                    case 2: return [3 /*break*/, 4];
                    case 3:
                        e_5 = _c.sent();
                        this.errorLabel = (e_5.message);
                        message = this.errorLabel;
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
                        }
                        sweetalert2__WEBPACK_IMPORTED_MODULE_9___default.a.fire({
                            icon: 'error',
                            title: message,
                        });
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    MerchantAddNewProductComponent.prototype.validateForm = function (form) {
        for (var prop in form) {
            if (!form.hasOwnProperty(prop) || prop == 'tnc' || prop == 'category' || prop == 'weightInGram' || prop == 'hashtags' || prop == 'images_gallery' || prop == 'description')
                continue;
            if (prop == 'base_url') {
                if (!form[prop]) {
                    throw new Error("Please upload new image for your product ");
                }
            }
            if (form[prop] == undefined)
                throw new Error("you need to completing this " + this.labels[prop]);
            if (Object(_object_interface_common_object__WEBPACK_IMPORTED_MODULE_8__["isString"])(form[prop]) && form[prop].trim() == '')
                throw new Error("you need to completing this input " + this.labels[prop]);
            if (Object(ngx_bootstrap_chronos__WEBPACK_IMPORTED_MODULE_2__["isArray"])(form[prop]) && form[prop].length == 0) {
                throw new Error("Hi, you need to completing this " + this.labels[prop]);
            }
        }
    };
    MerchantAddNewProductComponent.prototype.setProductType = function (object) {
        try {
            this.listOfProductType.forEach(function (el) {
                el.active = false;
            });
            object.active = true;
            this.form.type = object.value;
            if (this.form.type == 'product') {
                this.title = 'Product';
            }
            else if (this.form.type == 'voucher') {
                this.title = 'Voucher Fisik';
            }
            else if (this.form.type == 'e-voucher') {
                this.title = 'E-Voucher';
            }
            else if (this.form.type == 'gold') {
                this.title = 'Gold';
            }
            else if (this.form.type == 'top-up') {
                this.title = 'E-Wallet';
            }
            if (this.form.type == 'e-voucher') {
                this.form.merchant_username = "LOCARD";
            }
        }
        catch (error) {
            console.log("error", error);
        }
    };
    MerchantAddNewProductComponent.prototype.setVendorType = function (object) {
        this.listOfVendorType.forEach(function (el) {
            el.active = false;
        });
        object.active = true;
        this.form.voucher_group = object.name;
    };
    MerchantAddNewProductComponent.prototype.setRedeemType = function (object) {
        this.listOfRedeemType.forEach(function (el) {
            el.active = false;
        });
        object.active = true;
        this.form.redeem_type = object.name;
    };
    MerchantAddNewProductComponent.prototype.toggleCategories = function (index) {
        if (this.form.type == 'e-voucher') {
            this.multipleCategoriesDeals.forEach(function (el) {
                el.value = false;
            });
            this.multipleCategoriesDeals[index].value = true;
        }
        if (this.form.type == 'product') {
            this.multipleCategoriesProduct.forEach(function (el) {
                el.value = false;
            });
            this.multipleCategoriesProduct[index].value = true;
        }
    };
    MerchantAddNewProductComponent.prototype.backClicked = function () {
        this._location.back();
    };
    MerchantAddNewProductComponent.ctorParameters = function () { return [
        { type: _services_product_product_service__WEBPACK_IMPORTED_MODULE_1__["ProductService"] },
        { type: _services_category_category_service__WEBPACK_IMPORTED_MODULE_4__["CategoryService"] },
        { type: _services_merchant_merchant_service__WEBPACK_IMPORTED_MODULE_5__["MerchantService"] },
        { type: _services_observerable_permission_observer__WEBPACK_IMPORTED_MODULE_7__["PermissionObserver"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
        { type: _angular_common__WEBPACK_IMPORTED_MODULE_10__["Location"] }
    ]; };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], MerchantAddNewProductComponent.prototype, "type", void 0);
    MerchantAddNewProductComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-merchant-add-new-product',
            template: __webpack_require__(/*! raw-loader!./merchant-add-new-product.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/separated-modules/merchant-products/products/merchant-add-new-product/merchant-add-new-product.component.html"),
            styles: [__webpack_require__(/*! ./merchant-add-new-product.component.scss */ "./src/app/layout/merchant-portal/separated-modules/merchant-products/products/merchant-add-new-product/merchant-add-new-product.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_product_product_service__WEBPACK_IMPORTED_MODULE_1__["ProductService"],
            _services_category_category_service__WEBPACK_IMPORTED_MODULE_4__["CategoryService"],
            _services_merchant_merchant_service__WEBPACK_IMPORTED_MODULE_5__["MerchantService"],
            _services_observerable_permission_observer__WEBPACK_IMPORTED_MODULE_7__["PermissionObserver"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"],
            _angular_common__WEBPACK_IMPORTED_MODULE_10__["Location"]])
    ], MerchantAddNewProductComponent);
    return MerchantAddNewProductComponent;
}());



/***/ }),

/***/ "./src/app/layout/merchant-portal/separated-modules/merchant-products/products/merchant-detail-product/merchant-detail-product.component.scss":
/*!****************************************************************************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/separated-modules/merchant-products/products/merchant-detail-product/merchant-detail-product.component.scss ***!
  \****************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host ::ng-deep .ck-editor__editable_inline {\n  min-height: 500px;\n}\n\n.btn.btn-default {\n  background: white;\n  border: 1px solid #333;\n}\n\n.form {\n  padding: 25px;\n  position: relative;\n}\n\n.form label {\n  color: #757474;\n}\n\n.form .custom-content::-webkit-file-upload-button {\n  visibility: hidden;\n}\n\n.form .custom-content::before {\n  content: \"Choose File\";\n  margin-right: 5px;\n  display: inline-block;\n  border: 1px solid #999;\n  border-radius: 5px;\n  padding: 8px;\n  outline: none;\n  white-space: nowrap;\n  -webkit-user-select: none;\n  cursor: pointer;\n  font-weight: 700;\n  font-size: 10pt;\n  color: black;\n  letter-spacing: 0.5px;\n}\n\n.form .imagepict::before {\n  content: \"Choose Image\";\n}\n\n.form .hashhash .hashtag-item {\n  display: inline-block;\n  margin-right: 5px;\n  background: #eee;\n  padding: 2px 5px;\n  margin-bottom: 5px;\n  border-radius: 5px;\n}\n\n.form .hashhash .hashtag-item::before {\n  content: \"#\";\n  display: inline;\n}\n\n.form .hashhash .hashtagskey-input {\n  display: inline-block;\n  min-width: 75px;\n  margin-bottom: 5px;\n  font-size: 1rem;\n  background: white;\n  border-radius: 5px;\n  outline: none;\n}\n\n.form .hashhash .hashtagskey-input::before {\n  content: \"#\";\n  display: inline;\n}\n\n.form .card {\n  box-shadow: 0 1px 11px 0 rgba(0, 0, 0, 0.2), 0 2px 3px 0 rgba(0, 0, 0, 0.19);\n  margin-bottom: 20px;\n}\n\n.form .card .card-header {\n  background-color: white;\n  margin: 0px;\n  padding: 0px;\n  border-left: 3px solid #2480fb;\n}\n\n.form .card .card-header label {\n  margin: 20px 10px;\n  font-weight: bold;\n  font-size: 20px;\n}\n\n.form .card .image-upload {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  flex-direction: column;\n  border: 3px solid #ebebeb;\n  border-radius: 10px;\n  background-color: #f5f5f5;\n  min-height: 125px;\n  margin: 20px;\n  padding: 50px;\n}\n\n.form .card .image-upload .logo-upload {\n  width: 150px;\n}\n\n.form .card .image-upload .fileupload {\n  background-color: #2480fb;\n  color: white;\n  border-radius: 8px;\n  font-size: 14px;\n  padding: 12px 22px;\n}\n\n.form .card .image-upload .upload-desc {\n  color: #d6d6d6 !important;\n  text-align: center;\n  font-size: 18px;\n}\n\n.form .navbar-menu {\n  overflow-y: scroll;\n  max-height: 250px;\n  margin: 15px 5px;\n}\n\n.form .navbar-menu .category-list .list-group-item {\n  border: none;\n  cursor: pointer;\n}\n\n.form .navbar-menu .category-list .list-group-item .checked-icon {\n  display: inline-flex;\n  justify-content: center;\n  align-items: center;\n  width: 20px;\n  height: 20px;\n  border-radius: 15px;\n  border: 1px solid #ccc;\n  color: #3498db;\n  font-weight: bold;\n  font-size: 14px;\n  margin-right: 10px;\n}\n\n.form .navbar-menu .category-list .list-group-item .checked-icon.false {\n  color: transparent;\n}\n\n.form .uploading-file {\n  opacity: 0.5;\n  -webkit-filter: grayscale(1);\n          filter: grayscale(1);\n}\n\n.form .uploading-file.true {\n  opacity: 1;\n  -webkit-filter: grayscale(0);\n          filter: grayscale(0);\n}\n\n.form .autoSKU {\n  font-size: 12px;\n  margin-left: 15px;\n  display: inline-block;\n}\n\n.form .image-upload {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  flex-direction: column;\n  border: 1px dashed #ccc;\n  min-height: 125px;\n}\n\n.form table {\n  border-collapse: collapse;\n  width: 45%;\n}\n\n.form .uploaded-image {\n  margin: 0px auto;\n  width: auto;\n  height: auto;\n}\n\n.form .uploaded-image .text-fileupload {\n  display: block;\n  text-align: center;\n  cursor: pointer;\n  color: #2480fb;\n  margin-top: 10px;\n}\n\n.form .uploaded-image .img {\n  width: 100%;\n  min-height: 348px;\n  background-size: contain;\n  position: relative;\n  background-position: center center;\n  background-repeat: no-repeat;\n}\n\n.form .uploaded-image .img .editor-tool {\n  background: rgba(0, 0, 0, 0.2);\n  width: 100%;\n  height: 100%;\n  position: relative;\n  opacity: 0;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n.form .uploaded-image .img:hover .editor-tool {\n  opacity: 0.9;\n}\n\n.form .carousel-caption {\n  background: rgba(0, 0, 0, 0.2);\n}\n\n.form .carousel-indicators li {\n  background-color: black !important;\n}\n\n.form .product-type-list {\n  display: inline-block;\n}\n\n.form .product-type-list .product-type {\n  display: inline-flex;\n  padding: 15px;\n  justify-content: center;\n  align-items: center;\n  height: 35px;\n  border-radius: 7px;\n  cursor: pointer;\n  border: 1px solid #ccc;\n  margin-right: 5px;\n  margin-bottom: 5px;\n  transition: ease 0.2s all 0s;\n}\n\n.form .product-type-list .product-type.true {\n  color: white;\n  background-color: #3498db;\n  border-color: #2980b9;\n}\n\n.form .form-control.des {\n  padding-top: 4px;\n  display: inline-flex;\n  padding-bottom: 4px;\n  justify-content: center;\n  align-items: center;\n}\n\n.form .form-control.des input {\n  border: 0px solid transparent;\n  width: calc(100% - 15px);\n}\n\n.form .form-control.des input:focus {\n  outline: none;\n}\n\n.form div.error {\n  background: #e74c3c;\n  color: white;\n  padding: 25px;\n  margin-bottom: 25px;\n}\n\n.form div.new {\n  background: limegreen;\n  color: white;\n  padding: 25px;\n  margin-bottom: 25px;\n}\n\n.form .category-list .list-group-item {\n  cursor: pointer;\n}\n\n.form .category-list .list-group-item .checked-icon {\n  display: inline-flex;\n  justify-content: center;\n  align-items: center;\n  width: 20px;\n  height: 20px;\n  border-radius: 15px;\n  border: 1px solid #ccc;\n  color: #3498db;\n  font-weight: bold;\n  font-size: 14px;\n}\n\n.form .category-list .list-group-item .checked-icon.false {\n  color: transparent;\n}\n\n.form .end-footer .delete {\n  position: relative;\n  float: right;\n  background-color: #e74c3c;\n  opacity: 0.5;\n  border-color: #c0392b;\n}\n\n.form .end-footer .delete .delete-modal {\n  position: absolute;\n  display: none;\n  background: white;\n  border: 1px solid #ccc;\n  border-radius: 10px;\n  padding: 4px 10px;\n  top: 0px;\n  right: 0px;\n  color: #e74c3c;\n  -webkit-transform: translateY(-45px);\n          transform: translateY(-45px);\n}\n\n.form .end-footer .delete .delete-modal .btn-group {\n  border: 1px solid #e74c3c;\n  border-radius: 5px;\n  overflow: hidden;\n}\n\n.form .end-footer .delete .delete-modal .btn-group .no {\n  opacity: 0.5;\n  border-width: 0px;\n}\n\n.form .end-footer .delete .delete-modal .btn-group .yes {\n  background: #e74c3c;\n  border-width: 0px;\n}\n\n.form .end-footer .delete .delete-modal .btn-group .no:hover {\n  opacity: 1;\n}\n\n.form .end-footer .delete:hover,\n.form .end-footer .delete:focus {\n  opacity: 1;\n}\n\n.form .end-footer .delete:focus .delete-modal {\n  display: block;\n}\n\n.form.true {\n  pointer-events: none;\n  /* for \"disabled\" effect */\n  opacity: 0.5;\n  background: #ecf0f1;\n}\n\n.detail {\n  padding: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21lcmNoYW50LXBvcnRhbC9zZXBhcmF0ZWQtbW9kdWxlcy9tZXJjaGFudC1wcm9kdWN0cy9wcm9kdWN0cy9tZXJjaGFudC1kZXRhaWwtcHJvZHVjdC9EOlxcYWlzeWFoXFxjbHNcXGFuZ3VsYXItYWRtaW4tc2lnLXYuMi4wL3NyY1xcYXBwXFxsYXlvdXRcXG1lcmNoYW50LXBvcnRhbFxcc2VwYXJhdGVkLW1vZHVsZXNcXG1lcmNoYW50LXByb2R1Y3RzXFxwcm9kdWN0c1xcbWVyY2hhbnQtZGV0YWlsLXByb2R1Y3RcXG1lcmNoYW50LWRldGFpbC1wcm9kdWN0LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbWVyY2hhbnQtcG9ydGFsL3NlcGFyYXRlZC1tb2R1bGVzL21lcmNoYW50LXByb2R1Y3RzL3Byb2R1Y3RzL21lcmNoYW50LWRldGFpbC1wcm9kdWN0L21lcmNoYW50LWRldGFpbC1wcm9kdWN0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsaUJBQUE7QUNDRjs7QURFQTtFQUNFLGlCQUFBO0VBQ0Esc0JBQUE7QUNDRjs7QURFQTtFQUNFLGFBQUE7RUFDQSxrQkFBQTtBQ0NGOztBRE1FO0VBQ0UsY0FBQTtBQ0pKOztBRE9FO0VBQ0Usa0JBQUE7QUNMSjs7QURRRTtFQUNFLHNCQUFBO0VBQ0EsaUJBQUE7RUFDQSxxQkFBQTtFQUNBLHNCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtFQUNBLHFCQUFBO0FDTko7O0FEU0U7RUFDRSx1QkFBQTtBQ1BKOztBRFdJO0VBQ0UscUJBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FDVE47O0FEV0k7RUFDRSxZQUFBO0VBQ0EsZUFBQTtBQ1ROOztBRFdJO0VBQ0UscUJBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7QUNUTjs7QURXSTtFQUNFLFlBQUE7RUFDQSxlQUFBO0FDVE47O0FEYUU7RUFDRSw0RUFBQTtFQUNBLG1CQUFBO0FDWEo7O0FEWUk7RUFDRSx1QkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsOEJBQUE7QUNWTjs7QURXTTtFQUNFLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0FDVFI7O0FEYUk7RUFDRSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0VBQ0EseUJBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtBQ1hOOztBRGFNO0VBQ0UsWUFBQTtBQ1hSOztBRGNNO0VBQ0UseUJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QUNaUjs7QURlTTtFQUNFLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FDYlI7O0FEc0JFO0VBQ0Usa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0FDcEJKOztBRHNCTTtFQUNFLFlBQUE7RUFDQSxlQUFBO0FDcEJSOztBRHFCUTtFQUNFLG9CQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtBQ25CVjs7QURxQlE7RUFDRSxrQkFBQTtBQ25CVjs7QUR5QkU7RUFDRSxZQUFBO0VBQ0EsNEJBQUE7VUFBQSxvQkFBQTtBQ3ZCSjs7QUQwQkU7RUFDRSxVQUFBO0VBQ0EsNEJBQUE7VUFBQSxvQkFBQTtBQ3hCSjs7QUQwQkU7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxxQkFBQTtBQ3hCSjs7QUQyQkU7RUFDRSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSxpQkFBQTtBQ3pCSjs7QUQ0QkU7RUFDRSx5QkFBQTtFQUNBLFVBQUE7QUMxQko7O0FEK0JFO0VBQ0UsZ0JBQUE7RUFDQSxXQUFBO0VBRUEsWUFBQTtBQzlCSjs7QURnQ0k7RUFDRSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0FDOUJOOztBRGlDSTtFQUNFLFdBQUE7RUFDQSxpQkFBQTtFQUNBLHdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQ0FBQTtFQUNBLDRCQUFBO0FDL0JOOztBRGlDTTtFQUNFLDhCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQy9CUjs7QURrQ0k7RUFDRSxZQUFBO0FDaENOOztBRG9DRTtFQUNFLDhCQUFBO0FDbENKOztBRHFDRTtFQUNFLGtDQUFBO0FDbkNKOztBRHNDRTtFQUNFLHFCQUFBO0FDcENKOztBRHFDSTtFQUNFLG9CQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0Esc0JBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsNEJBQUE7QUNuQ047O0FEcUNJO0VBQ0UsWUFBQTtFQUNBLHlCQUFBO0VBQ0EscUJBQUE7QUNuQ047O0FEc0NFO0VBQ0UsZ0JBQUE7RUFDQSxvQkFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQ3BDSjs7QURxQ0k7RUFDRSw2QkFBQTtFQUNBLHdCQUFBO0FDbkNOOztBRHFDSTtFQUNFLGFBQUE7QUNuQ047O0FEc0NFO0VBQ0UsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0FDcENKOztBRHNDRTtFQUNFLHFCQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtBQ3BDSjs7QUR1Q0k7RUFDRSxlQUFBO0FDckNOOztBRHNDTTtFQUNFLG9CQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7QUNwQ1I7O0FEc0NNO0VBQ0Usa0JBQUE7QUNwQ1I7O0FEeUNJO0VBQ0Usa0JBQUE7RUFDQSxZQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0EscUJBQUE7QUN2Q047O0FEd0NNO0VBQ0Usa0JBQUE7RUFDQSxhQUFBO0VBQ0EsaUJBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxRQUFBO0VBQ0EsVUFBQTtFQUNBLGNBQUE7RUFDQSxvQ0FBQTtVQUFBLDRCQUFBO0FDdENSOztBRHVDUTtFQUNFLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQ3JDVjs7QURzQ1U7RUFDRSxZQUFBO0VBQ0EsaUJBQUE7QUNwQ1o7O0FEc0NVO0VBQ0UsbUJBQUE7RUFDQSxpQkFBQTtBQ3BDWjs7QURzQ1U7RUFDRSxVQUFBO0FDcENaOztBRHlDSTs7RUFFRSxVQUFBO0FDdkNOOztBRDBDTTtFQUNFLGNBQUE7QUN4Q1I7O0FEOENBO0VBQ0Usb0JBQUE7RUFFQSwwQkFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtBQzVDRjs7QUQrQ0E7RUFDRSxhQUFBO0FDNUNGIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L21lcmNoYW50LXBvcnRhbC9zZXBhcmF0ZWQtbW9kdWxlcy9tZXJjaGFudC1wcm9kdWN0cy9wcm9kdWN0cy9tZXJjaGFudC1kZXRhaWwtcHJvZHVjdC9tZXJjaGFudC1kZXRhaWwtcHJvZHVjdC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IDo6bmctZGVlcCAuY2stZWRpdG9yX19lZGl0YWJsZV9pbmxpbmUge1xyXG4gIG1pbi1oZWlnaHQ6IDUwMHB4O1xyXG59XHJcblxyXG4uYnRuLmJ0bi1kZWZhdWx0e1xyXG4gIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICMzMzM7XHJcbn1cclxuXHJcbi5mb3JtIHtcclxuICBwYWRkaW5nOiAyNXB4O1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAvLyB0b3A6IDV2dztcclxuXHJcbiAgaW5wdXRbdHlwZT1cImZpbGVcIl0ge1xyXG4gICAgLy8gZGlzcGxheTogbm9uZTtcclxuICB9XHJcblxyXG4gIGxhYmVsIHtcclxuICAgIGNvbG9yOiAjNzU3NDc0O1xyXG4gIH1cclxuXHJcbiAgLmN1c3RvbS1jb250ZW50Ojotd2Via2l0LWZpbGUtdXBsb2FkLWJ1dHRvbiB7XHJcbiAgICB2aXNpYmlsaXR5OiBoaWRkZW47XHJcbiAgfVxyXG5cclxuICAuY3VzdG9tLWNvbnRlbnQ6OmJlZm9yZSB7XHJcbiAgICBjb250ZW50OiBcIkNob29zZSBGaWxlXCI7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDVweDtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICM5OTk7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBwYWRkaW5nOiA4cHg7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAgIC13ZWJraXQtdXNlci1zZWxlY3Q6IG5vbmU7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICBmb250LXdlaWdodDogNzAwO1xyXG4gICAgZm9udC1zaXplOiAxMHB0O1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuNXB4O1xyXG4gIH1cclxuXHJcbiAgLmltYWdlcGljdDo6YmVmb3JlIHtcclxuICAgIGNvbnRlbnQ6IFwiQ2hvb3NlIEltYWdlXCI7XHJcbiAgfVxyXG5cclxuICAuaGFzaGhhc2gge1xyXG4gICAgLmhhc2h0YWctaXRlbSB7XHJcbiAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgbWFyZ2luLXJpZ2h0OiA1cHg7XHJcbiAgICAgIGJhY2tncm91bmQ6ICNlZWU7XHJcbiAgICAgIHBhZGRpbmc6IDJweCA1cHg7XHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDVweDtcclxuICAgICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgfVxyXG4gICAgLmhhc2h0YWctaXRlbTo6YmVmb3JlIHtcclxuICAgICAgY29udGVudDogXCIjXCI7XHJcbiAgICAgIGRpc3BsYXk6IGlubGluZTtcclxuICAgIH1cclxuICAgIC5oYXNodGFnc2tleS1pbnB1dCB7XHJcbiAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgbWluLXdpZHRoOiA3NXB4O1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbiAgICAgIGZvbnQtc2l6ZTogMXJlbTtcclxuICAgICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgICAgb3V0bGluZTogbm9uZTtcclxuICAgIH1cclxuICAgIC5oYXNodGFnc2tleS1pbnB1dDo6YmVmb3JlIHtcclxuICAgICAgY29udGVudDogXCIjXCI7XHJcbiAgICAgIGRpc3BsYXk6IGlubGluZTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC5jYXJkIHtcclxuICAgIGJveC1zaGFkb3c6IDAgMXB4IDExcHggMCByZ2JhKDAsIDAsIDAsIDAuMiksIDAgMnB4IDNweCAwIHJnYmEoMCwgMCwgMCwgMC4xOSk7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG4gICAgLmNhcmQtaGVhZGVyIHtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgICAgIG1hcmdpbjogMHB4O1xyXG4gICAgICBwYWRkaW5nOiAwcHg7XHJcbiAgICAgIGJvcmRlci1sZWZ0OiAzcHggc29saWQgIzI0ODBmYjtcclxuICAgICAgbGFiZWwge1xyXG4gICAgICAgIG1hcmdpbjogMjBweCAxMHB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5pbWFnZS11cGxvYWQge1xyXG4gICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgYm9yZGVyOiAzcHggc29saWQgI2ViZWJlYjtcclxuICAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2Y1ZjVmNTtcclxuICAgICAgbWluLWhlaWdodDogMTI1cHg7XHJcbiAgICAgIG1hcmdpbjogMjBweDtcclxuICAgICAgcGFkZGluZzogNTBweDtcclxuICBcclxuICAgICAgLmxvZ28tdXBsb2FkIHtcclxuICAgICAgICB3aWR0aDogMTUwcHg7XHJcbiAgICAgIH1cclxuICBcclxuICAgICAgLmZpbGV1cGxvYWQge1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICMyNDgwZmI7XHJcbiAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgcGFkZGluZzogMTJweCAyMnB4O1xyXG4gICAgICB9XHJcbiAgXHJcbiAgICAgIC51cGxvYWQtZGVzYyB7XHJcbiAgICAgICAgY29sb3I6ICNkNmQ2ZDYgIWltcG9ydGFudDtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLy8gLnVwbG9hZGVkIHtcclxuICAgIC8vICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAvLyB9XHJcbiAgfVxyXG5cclxuICAubmF2YmFyLW1lbnUge1xyXG4gICAgb3ZlcmZsb3cteTogc2Nyb2xsO1xyXG4gICAgbWF4LWhlaWdodDogMjUwcHg7XHJcbiAgICBtYXJnaW46IDE1cHggNXB4O1xyXG4gICAgLmNhdGVnb3J5LWxpc3Qge1xyXG4gICAgICAubGlzdC1ncm91cC1pdGVtIHtcclxuICAgICAgICBib3JkZXI6IG5vbmU7XHJcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICAgIC5jaGVja2VkLWljb24ge1xyXG4gICAgICAgICAgZGlzcGxheTogaW5saW5lLWZsZXg7XHJcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICB3aWR0aDogMjBweDtcclxuICAgICAgICAgIGhlaWdodDogMjBweDtcclxuICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XHJcbiAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgICAgY29sb3I6ICMzNDk4ZGI7XHJcbiAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgICAgIG1hcmdpbi1yaWdodDogMTBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgLmNoZWNrZWQtaWNvbi5mYWxzZSB7XHJcbiAgICAgICAgICBjb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAudXBsb2FkaW5nLWZpbGUge1xyXG4gICAgb3BhY2l0eTogMC41O1xyXG4gICAgZmlsdGVyOiBncmF5c2NhbGUoMSk7XHJcbiAgfVxyXG5cclxuICAudXBsb2FkaW5nLWZpbGUudHJ1ZSB7XHJcbiAgICBvcGFjaXR5OiAxO1xyXG4gICAgZmlsdGVyOiBncmF5c2NhbGUoMCk7XHJcbiAgfVxyXG4gIC5hdXRvU0tVIHtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIG1hcmdpbi1sZWZ0OiAxNXB4O1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIH1cclxuXHJcbiAgLmltYWdlLXVwbG9hZCB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGJvcmRlcjogMXB4IGRhc2hlZCAjY2NjO1xyXG4gICAgbWluLWhlaWdodDogMTI1cHg7XHJcbiAgfVxyXG5cclxuICB0YWJsZXtcclxuICAgIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XHJcbiAgICB3aWR0aDogNDUlO1xyXG4gICAgXHJcbiAgfVxyXG5cclxuXHJcbiAgLnVwbG9hZGVkLWltYWdlIHtcclxuICAgIG1hcmdpbjogMHB4IGF1dG87XHJcbiAgICB3aWR0aDogYXV0bztcclxuICAgIC8vIHBhZGRpbmc6IDUwcHggO1xyXG4gICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgLy8gY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgLnRleHQtZmlsZXVwbG9hZHtcclxuICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICBjb2xvcjogIzI0ODBmYjtcclxuICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgIFxyXG4gICAgfVxyXG4gICAgLmltZyB7XHJcbiAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICBtaW4taGVpZ2h0OiAzNDhweDtcclxuICAgICAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciBjZW50ZXI7XHJcbiAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcblxyXG4gICAgICAuZWRpdG9yLXRvb2wge1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC4yKTtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIG9wYWNpdHk6IDA7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAuaW1nOmhvdmVyIC5lZGl0b3ItdG9vbCB7XHJcbiAgICAgIG9wYWNpdHk6IDAuOTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC5jYXJvdXNlbC1jYXB0aW9uIHtcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC4yKTtcclxuICB9XHJcblxyXG4gIC5jYXJvdXNlbC1pbmRpY2F0b3JzIGxpIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IGJsYWNrICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG5cclxuICAucHJvZHVjdC10eXBlLWxpc3Qge1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgLnByb2R1Y3QtdHlwZSB7XHJcbiAgICAgIGRpc3BsYXk6IGlubGluZS1mbGV4O1xyXG4gICAgICBwYWRkaW5nOiAxNXB4O1xyXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgaGVpZ2h0OiAzNXB4O1xyXG4gICAgICBib3JkZXItcmFkaXVzOiA3cHg7XHJcbiAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgbWFyZ2luLXJpZ2h0OiA1cHg7XHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDVweDtcclxuICAgICAgdHJhbnNpdGlvbjogZWFzZSAwLjJzIGFsbCAwcztcclxuICAgIH1cclxuICAgIC5wcm9kdWN0LXR5cGUudHJ1ZSB7XHJcbiAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcclxuICAgICAgYm9yZGVyLWNvbG9yOiAjMjk4MGI5O1xyXG4gICAgfVxyXG4gIH1cclxuICAuZm9ybS1jb250cm9sLmRlcyB7XHJcbiAgICBwYWRkaW5nLXRvcDogNHB4O1xyXG4gICAgZGlzcGxheTogaW5saW5lLWZsZXg7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogNHB4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgaW5wdXQge1xyXG4gICAgICBib3JkZXI6IDBweCBzb2xpZCB0cmFuc3BhcmVudDtcclxuICAgICAgd2lkdGg6IGNhbGMoMTAwJSAtIDE1cHgpO1xyXG4gICAgfVxyXG4gICAgaW5wdXQ6Zm9jdXMge1xyXG4gICAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgfVxyXG4gIH1cclxuICBkaXYuZXJyb3Ige1xyXG4gICAgYmFja2dyb3VuZDogI2U3NGMzYztcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIHBhZGRpbmc6IDI1cHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAyNXB4O1xyXG4gIH1cclxuICBkaXYubmV3IHtcclxuICAgIGJhY2tncm91bmQ6IGxpbWVncmVlbjtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIHBhZGRpbmc6IDI1cHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAyNXB4O1xyXG4gIH1cclxuICAuY2F0ZWdvcnktbGlzdCB7XHJcbiAgICAubGlzdC1ncm91cC1pdGVtIHtcclxuICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICAuY2hlY2tlZC1pY29uIHtcclxuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIHdpZHRoOiAyMHB4O1xyXG4gICAgICAgIGhlaWdodDogMjBweDtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxNXB4O1xyXG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgY29sb3I6ICMzNDk4ZGI7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICB9XHJcbiAgICAgIC5jaGVja2VkLWljb24uZmFsc2Uge1xyXG4gICAgICAgIGNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuICAuZW5kLWZvb3RlciB7XHJcbiAgICAuZGVsZXRlIHtcclxuICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICBmbG9hdDogcmlnaHQ7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNlNzRjM2M7XHJcbiAgICAgIG9wYWNpdHk6IDAuNTtcclxuICAgICAgYm9yZGVyLWNvbG9yOiAjYzAzOTJiO1xyXG4gICAgICAuZGVsZXRlLW1vZGFsIHtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgZGlzcGxheTogbm9uZTtcclxuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICAgICAgcGFkZGluZzogNHB4IDEwcHg7XHJcbiAgICAgICAgdG9wOiAwcHg7XHJcbiAgICAgICAgcmlnaHQ6IDBweDtcclxuICAgICAgICBjb2xvcjogI2U3NGMzYztcclxuICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTQ1cHgpO1xyXG4gICAgICAgIC5idG4tZ3JvdXAge1xyXG4gICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2U3NGMzYztcclxuICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgICAubm8ge1xyXG4gICAgICAgICAgICBvcGFjaXR5OiAwLjU7XHJcbiAgICAgICAgICAgIGJvcmRlci13aWR0aDogMHB4O1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgLnllcyB7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICNlNzRjM2M7XHJcbiAgICAgICAgICAgIGJvcmRlci13aWR0aDogMHB4O1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgLm5vOmhvdmVyIHtcclxuICAgICAgICAgICAgb3BhY2l0eTogMTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIC5kZWxldGU6aG92ZXIsXHJcbiAgICAuZGVsZXRlOmZvY3VzIHtcclxuICAgICAgb3BhY2l0eTogMTtcclxuICAgIH1cclxuICAgIC5kZWxldGU6Zm9jdXMge1xyXG4gICAgICAuZGVsZXRlLW1vZGFsIHtcclxuICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuLmZvcm0udHJ1ZXtcclxuICBwb2ludGVyLWV2ZW50czogbm9uZTtcclxuXHJcbiAgLyogZm9yIFwiZGlzYWJsZWRcIiBlZmZlY3QgKi9cclxuICBvcGFjaXR5OiAwLjU7XHJcbiAgYmFja2dyb3VuZDogI2VjZjBmMTtcclxufVxyXG5cclxuLmRldGFpbHtcclxuICBwYWRkaW5nOiAxMHB4O1xyXG59XHJcblxyXG4vLyAuYnV0dG9uLWVkaXR7XHJcbi8vICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4vLyAgIG1hcmdpbi1sZWZ0OiBjYWxjKDUwJSAtIDEwMHB4KTtcclxuLy8gICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xyXG4vLyAgIGNvbG9yOiB3aGl0ZTtcclxuLy8gICB0b3A6IDEwcHg7XHJcbi8vIH1cclxuIiwiOmhvc3QgOjpuZy1kZWVwIC5jay1lZGl0b3JfX2VkaXRhYmxlX2lubGluZSB7XG4gIG1pbi1oZWlnaHQ6IDUwMHB4O1xufVxuXG4uYnRuLmJ0bi1kZWZhdWx0IHtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGJvcmRlcjogMXB4IHNvbGlkICMzMzM7XG59XG5cbi5mb3JtIHtcbiAgcGFkZGluZzogMjVweDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLmZvcm0gbGFiZWwge1xuICBjb2xvcjogIzc1NzQ3NDtcbn1cbi5mb3JtIC5jdXN0b20tY29udGVudDo6LXdlYmtpdC1maWxlLXVwbG9hZC1idXR0b24ge1xuICB2aXNpYmlsaXR5OiBoaWRkZW47XG59XG4uZm9ybSAuY3VzdG9tLWNvbnRlbnQ6OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiQ2hvb3NlIEZpbGVcIjtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgYm9yZGVyOiAxcHggc29saWQgIzk5OTtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBwYWRkaW5nOiA4cHg7XG4gIG91dGxpbmU6IG5vbmU7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIC13ZWJraXQtdXNlci1zZWxlY3Q6IG5vbmU7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgZm9udC1zaXplOiAxMHB0O1xuICBjb2xvcjogYmxhY2s7XG4gIGxldHRlci1zcGFjaW5nOiAwLjVweDtcbn1cbi5mb3JtIC5pbWFnZXBpY3Q6OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiQ2hvb3NlIEltYWdlXCI7XG59XG4uZm9ybSAuaGFzaGhhc2ggLmhhc2h0YWctaXRlbSB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG4gIGJhY2tncm91bmQ6ICNlZWU7XG4gIHBhZGRpbmc6IDJweCA1cHg7XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xufVxuLmZvcm0gLmhhc2hoYXNoIC5oYXNodGFnLWl0ZW06OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiI1wiO1xuICBkaXNwbGF5OiBpbmxpbmU7XG59XG4uZm9ybSAuaGFzaGhhc2ggLmhhc2h0YWdza2V5LWlucHV0IHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBtaW4td2lkdGg6IDc1cHg7XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbiAgZm9udC1zaXplOiAxcmVtO1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBvdXRsaW5lOiBub25lO1xufVxuLmZvcm0gLmhhc2hoYXNoIC5oYXNodGFnc2tleS1pbnB1dDo6YmVmb3JlIHtcbiAgY29udGVudDogXCIjXCI7XG4gIGRpc3BsYXk6IGlubGluZTtcbn1cbi5mb3JtIC5jYXJkIHtcbiAgYm94LXNoYWRvdzogMCAxcHggMTFweCAwIHJnYmEoMCwgMCwgMCwgMC4yKSwgMCAycHggM3B4IDAgcmdiYSgwLCAwLCAwLCAwLjE5KTtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbn1cbi5mb3JtIC5jYXJkIC5jYXJkLWhlYWRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICBtYXJnaW46IDBweDtcbiAgcGFkZGluZzogMHB4O1xuICBib3JkZXItbGVmdDogM3B4IHNvbGlkICMyNDgwZmI7XG59XG4uZm9ybSAuY2FyZCAuY2FyZC1oZWFkZXIgbGFiZWwge1xuICBtYXJnaW46IDIwcHggMTBweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZvbnQtc2l6ZTogMjBweDtcbn1cbi5mb3JtIC5jYXJkIC5pbWFnZS11cGxvYWQge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgYm9yZGVyOiAzcHggc29saWQgI2ViZWJlYjtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y1ZjVmNTtcbiAgbWluLWhlaWdodDogMTI1cHg7XG4gIG1hcmdpbjogMjBweDtcbiAgcGFkZGluZzogNTBweDtcbn1cbi5mb3JtIC5jYXJkIC5pbWFnZS11cGxvYWQgLmxvZ28tdXBsb2FkIHtcbiAgd2lkdGg6IDE1MHB4O1xufVxuLmZvcm0gLmNhcmQgLmltYWdlLXVwbG9hZCAuZmlsZXVwbG9hZCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMyNDgwZmI7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xuICBmb250LXNpemU6IDE0cHg7XG4gIHBhZGRpbmc6IDEycHggMjJweDtcbn1cbi5mb3JtIC5jYXJkIC5pbWFnZS11cGxvYWQgLnVwbG9hZC1kZXNjIHtcbiAgY29sb3I6ICNkNmQ2ZDYgIWltcG9ydGFudDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDE4cHg7XG59XG4uZm9ybSAubmF2YmFyLW1lbnUge1xuICBvdmVyZmxvdy15OiBzY3JvbGw7XG4gIG1heC1oZWlnaHQ6IDI1MHB4O1xuICBtYXJnaW46IDE1cHggNXB4O1xufVxuLmZvcm0gLm5hdmJhci1tZW51IC5jYXRlZ29yeS1saXN0IC5saXN0LWdyb3VwLWl0ZW0ge1xuICBib3JkZXI6IG5vbmU7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbi5mb3JtIC5uYXZiYXItbWVudSAuY2F0ZWdvcnktbGlzdCAubGlzdC1ncm91cC1pdGVtIC5jaGVja2VkLWljb24ge1xuICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHdpZHRoOiAyMHB4O1xuICBoZWlnaHQ6IDIwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIGNvbG9yOiAjMzQ5OGRiO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG4uZm9ybSAubmF2YmFyLW1lbnUgLmNhdGVnb3J5LWxpc3QgLmxpc3QtZ3JvdXAtaXRlbSAuY2hlY2tlZC1pY29uLmZhbHNlIHtcbiAgY29sb3I6IHRyYW5zcGFyZW50O1xufVxuLmZvcm0gLnVwbG9hZGluZy1maWxlIHtcbiAgb3BhY2l0eTogMC41O1xuICBmaWx0ZXI6IGdyYXlzY2FsZSgxKTtcbn1cbi5mb3JtIC51cGxvYWRpbmctZmlsZS50cnVlIHtcbiAgb3BhY2l0eTogMTtcbiAgZmlsdGVyOiBncmF5c2NhbGUoMCk7XG59XG4uZm9ybSAuYXV0b1NLVSB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgbWFyZ2luLWxlZnQ6IDE1cHg7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbn1cbi5mb3JtIC5pbWFnZS11cGxvYWQge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgYm9yZGVyOiAxcHggZGFzaGVkICNjY2M7XG4gIG1pbi1oZWlnaHQ6IDEyNXB4O1xufVxuLmZvcm0gdGFibGUge1xuICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xuICB3aWR0aDogNDUlO1xufVxuLmZvcm0gLnVwbG9hZGVkLWltYWdlIHtcbiAgbWFyZ2luOiAwcHggYXV0bztcbiAgd2lkdGg6IGF1dG87XG4gIGhlaWdodDogYXV0bztcbn1cbi5mb3JtIC51cGxvYWRlZC1pbWFnZSAudGV4dC1maWxldXBsb2FkIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBjb2xvcjogIzI0ODBmYjtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cbi5mb3JtIC51cGxvYWRlZC1pbWFnZSAuaW1nIHtcbiAgd2lkdGg6IDEwMCU7XG4gIG1pbi1oZWlnaHQ6IDM0OHB4O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyIGNlbnRlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbn1cbi5mb3JtIC51cGxvYWRlZC1pbWFnZSAuaW1nIC5lZGl0b3ItdG9vbCB7XG4gIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC4yKTtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBvcGFjaXR5OiAwO1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5mb3JtIC51cGxvYWRlZC1pbWFnZSAuaW1nOmhvdmVyIC5lZGl0b3ItdG9vbCB7XG4gIG9wYWNpdHk6IDAuOTtcbn1cbi5mb3JtIC5jYXJvdXNlbC1jYXB0aW9uIHtcbiAgYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwLjIpO1xufVxuLmZvcm0gLmNhcm91c2VsLWluZGljYXRvcnMgbGkge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjayAhaW1wb3J0YW50O1xufVxuLmZvcm0gLnByb2R1Y3QtdHlwZS1saXN0IHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xufVxuLmZvcm0gLnByb2R1Y3QtdHlwZS1saXN0IC5wcm9kdWN0LXR5cGUge1xuICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcbiAgcGFkZGluZzogMTVweDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGhlaWdodDogMzVweDtcbiAgYm9yZGVyLXJhZGl1czogN3B4O1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xuICBtYXJnaW4tYm90dG9tOiA1cHg7XG4gIHRyYW5zaXRpb246IGVhc2UgMC4ycyBhbGwgMHM7XG59XG4uZm9ybSAucHJvZHVjdC10eXBlLWxpc3QgLnByb2R1Y3QtdHlwZS50cnVlIHtcbiAgY29sb3I6IHdoaXRlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xuICBib3JkZXItY29sb3I6ICMyOTgwYjk7XG59XG4uZm9ybSAuZm9ybS1jb250cm9sLmRlcyB7XG4gIHBhZGRpbmctdG9wOiA0cHg7XG4gIGRpc3BsYXk6IGlubGluZS1mbGV4O1xuICBwYWRkaW5nLWJvdHRvbTogNHB4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5mb3JtIC5mb3JtLWNvbnRyb2wuZGVzIGlucHV0IHtcbiAgYm9yZGVyOiAwcHggc29saWQgdHJhbnNwYXJlbnQ7XG4gIHdpZHRoOiBjYWxjKDEwMCUgLSAxNXB4KTtcbn1cbi5mb3JtIC5mb3JtLWNvbnRyb2wuZGVzIGlucHV0OmZvY3VzIHtcbiAgb3V0bGluZTogbm9uZTtcbn1cbi5mb3JtIGRpdi5lcnJvciB7XG4gIGJhY2tncm91bmQ6ICNlNzRjM2M7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgcGFkZGluZzogMjVweDtcbiAgbWFyZ2luLWJvdHRvbTogMjVweDtcbn1cbi5mb3JtIGRpdi5uZXcge1xuICBiYWNrZ3JvdW5kOiBsaW1lZ3JlZW47XG4gIGNvbG9yOiB3aGl0ZTtcbiAgcGFkZGluZzogMjVweDtcbiAgbWFyZ2luLWJvdHRvbTogMjVweDtcbn1cbi5mb3JtIC5jYXRlZ29yeS1saXN0IC5saXN0LWdyb3VwLWl0ZW0ge1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG4uZm9ybSAuY2F0ZWdvcnktbGlzdCAubGlzdC1ncm91cC1pdGVtIC5jaGVja2VkLWljb24ge1xuICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHdpZHRoOiAyMHB4O1xuICBoZWlnaHQ6IDIwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIGNvbG9yOiAjMzQ5OGRiO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuLmZvcm0gLmNhdGVnb3J5LWxpc3QgLmxpc3QtZ3JvdXAtaXRlbSAuY2hlY2tlZC1pY29uLmZhbHNlIHtcbiAgY29sb3I6IHRyYW5zcGFyZW50O1xufVxuLmZvcm0gLmVuZC1mb290ZXIgLmRlbGV0ZSB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTc0YzNjO1xuICBvcGFjaXR5OiAwLjU7XG4gIGJvcmRlci1jb2xvcjogI2MwMzkyYjtcbn1cbi5mb3JtIC5lbmQtZm9vdGVyIC5kZWxldGUgLmRlbGV0ZS1tb2RhbCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgZGlzcGxheTogbm9uZTtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIHBhZGRpbmc6IDRweCAxMHB4O1xuICB0b3A6IDBweDtcbiAgcmlnaHQ6IDBweDtcbiAgY29sb3I6ICNlNzRjM2M7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNDVweCk7XG59XG4uZm9ybSAuZW5kLWZvb3RlciAuZGVsZXRlIC5kZWxldGUtbW9kYWwgLmJ0bi1ncm91cCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNlNzRjM2M7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cbi5mb3JtIC5lbmQtZm9vdGVyIC5kZWxldGUgLmRlbGV0ZS1tb2RhbCAuYnRuLWdyb3VwIC5ubyB7XG4gIG9wYWNpdHk6IDAuNTtcbiAgYm9yZGVyLXdpZHRoOiAwcHg7XG59XG4uZm9ybSAuZW5kLWZvb3RlciAuZGVsZXRlIC5kZWxldGUtbW9kYWwgLmJ0bi1ncm91cCAueWVzIHtcbiAgYmFja2dyb3VuZDogI2U3NGMzYztcbiAgYm9yZGVyLXdpZHRoOiAwcHg7XG59XG4uZm9ybSAuZW5kLWZvb3RlciAuZGVsZXRlIC5kZWxldGUtbW9kYWwgLmJ0bi1ncm91cCAubm86aG92ZXIge1xuICBvcGFjaXR5OiAxO1xufVxuLmZvcm0gLmVuZC1mb290ZXIgLmRlbGV0ZTpob3Zlcixcbi5mb3JtIC5lbmQtZm9vdGVyIC5kZWxldGU6Zm9jdXMge1xuICBvcGFjaXR5OiAxO1xufVxuLmZvcm0gLmVuZC1mb290ZXIgLmRlbGV0ZTpmb2N1cyAuZGVsZXRlLW1vZGFsIHtcbiAgZGlzcGxheTogYmxvY2s7XG59XG5cbi5mb3JtLnRydWUge1xuICBwb2ludGVyLWV2ZW50czogbm9uZTtcbiAgLyogZm9yIFwiZGlzYWJsZWRcIiBlZmZlY3QgKi9cbiAgb3BhY2l0eTogMC41O1xuICBiYWNrZ3JvdW5kOiAjZWNmMGYxO1xufVxuXG4uZGV0YWlsIHtcbiAgcGFkZGluZzogMTBweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/merchant-portal/separated-modules/merchant-products/products/merchant-detail-product/merchant-detail-product.component.ts":
/*!**************************************************************************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/separated-modules/merchant-products/products/merchant-detail-product/merchant-detail-product.component.ts ***!
  \**************************************************************************************************************************************************/
/*! exports provided: MerchantDetailProductComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MerchantDetailProductComponent", function() { return MerchantDetailProductComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_product_product_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../../services/product/product.service */ "./src/app/services/product/product.service.ts");
/* harmony import */ var ngx_bootstrap_chronos__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-bootstrap/chronos */ "./node_modules/ngx-bootstrap/chronos/fesm5/ngx-bootstrap-chronos.js");
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ckeditor/ckeditor5-build-classic */ "./node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js");
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _services_category_category_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../../services/category/category.service */ "./src/app/services/category/category.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_observerable_permission_observer__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../../services/observerable/permission-observer */ "./src/app/services/observerable/permission-observer.ts");
/* harmony import */ var _services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../../services/e-voucher/e-voucher.service */ "./src/app/services/e-voucher/e-voucher.service.ts");
/* harmony import */ var _services_merchant_merchant_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../../../services/merchant/merchant.service */ "./src/app/services/merchant/merchant.service.ts");
/* harmony import */ var _object_interface_common_object__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../../../object-interface/common.object */ "./src/app/object-interface/common.object.ts");
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};










var MerchantDetailProductComponent = /** @class */ (function () {
    function MerchantDetailProductComponent(productService, categoryService, router, route, permission, eVoucherService, merchantService) {
        var _this = this;
        this.productService = productService;
        this.categoryService = categoryService;
        this.router = router;
        this.route = route;
        this.permission = permission;
        this.eVoucherService = eVoucherService;
        this.merchantService = merchantService;
        // images = [1, 2, 3].map(() => `https://picsum.photos/900/500?random&t=${Math.random()}`);
        this.genVoucher = {
            expiry_date: null,
            qty: 1
        };
        this.toggleDelete = false;
        this.errorFile = false;
        this.generateVoucherToggler = 0;
        // edit_status = true
        this.edit = false;
        this.listOfProductType = [
            { name: 'Product', active: true },
            { name: 'evoucher', active: false }
        ];
        this.listOfRedeemType = [
            { name: 'Voucher Link', active: false },
            { name: 'Voucher Code', active: false }
        ];
        this.listOfVendorType = [];
        this.listOfMerchant = [];
        this.Editor = _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_3___default.a;
        this.autoSKU = false;
        this.hashtagsKey = "";
        this.form = {
            type: 'product',
            min_order: 1,
            product_code: '',
            product_name: '',
            category: 'public',
            price: 0,
            fixed_price: 0,
            qty: 0,
            description: '',
            tnc: '',
            dimensions: {
                width: 0,
                length: 0,
                height: 0
            },
            weight: 1,
            weightInGram: 1000,
            pic_file_name: '',
        };
        this.labels = {
            type: 'product type',
            min_order: 'minimum order',
            product_code: 'product code',
            product_name: 'product name',
            category: 'category',
            price: 'price',
            fixed_price: 'public price',
            qty: 'quantity',
            description: 'description',
            tnc: 'term and conditions',
            dimensions: 'dimensions',
            weight: 'weight',
        };
        this.errorLabel = false;
        this.isFileUploaded = false;
        this.multipleCategories = [];
        this.permission.currentPermission.subscribe(function (val) {
            _this.currentPermission = val;
        });
    }
    MerchantDetailProductComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var mGroup, params, mUsername;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.inLoadedProduct();
                        return [4 /*yield*/, this.merchantService.getVoucherGroup()];
                    case 1:
                        mGroup = _a.sent();
                        if (mGroup.result) {
                            this.listOfVendorType = [];
                        }
                        mGroup.result.forEach(function (el, i) {
                            _this.listOfVendorType.push({ name: el.name });
                        });
                        params = {
                            'type': 'merchant'
                        };
                        return [4 /*yield*/, this.merchantService.getMerchant(params)];
                    case 2:
                        mUsername = _a.sent();
                        mUsername.result.values.forEach(function (el, i) {
                            _this.listOfMerchant.push({ name: el.merchant_username });
                        });
                        this.setVendorType(this.listOfVendorType[0]);
                        this.setRedeemType(this.listOfRedeemType[0]);
                        return [2 /*return*/];
                }
            });
        });
    };
    MerchantDetailProductComponent.prototype.editThis = function () {
        this.router.navigate(['/merchant-portal/product-list/edit'], { queryParams: { id: this.productID } });
        console.log("param", this.productID);
    };
    MerchantDetailProductComponent.prototype.setVendorType = function (object) {
        this.listOfVendorType.forEach(function (el) {
            el.active = false;
        });
        console.log(this.listOfMerchant);
        object.active = true;
        this.form.merchant_group = object.name;
    };
    MerchantDetailProductComponent.prototype.setRedeemType = function (object) {
        this.listOfRedeemType.forEach(function (el) {
            el.active = false;
        });
        object.active = true;
        this.form.redeem_type = object.name;
    };
    MerchantDetailProductComponent.prototype.inLoadedProduct = function () {
        var _this = this;
        // console.log(this.permission.getCurrentPermission());
        this.route.queryParams.subscribe(function (params) { return __awaiter(_this, void 0, void 0, function () {
            var result, data, html, div;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.productID = params.id;
                        return [4 /*yield*/, this.productService.detailProducts(this.productID)];
                    case 1:
                        result = _a.sent();
                        data = result.result[0];
                        console.log("data", data);
                        html = data.description;
                        div = document.createElement('div');
                        div.innerHTML = html;
                        console.log("result text", div.innerText);
                        data.description = div.innerText;
                        console.log("test", data.description);
                        if (Object(_object_interface_common_object__WEBPACK_IMPORTED_MODULE_9__["isString"])(data.hashtags)) {
                            data.hashtags = [];
                        }
                        if (!data.condition) {
                            data.condition = 'new';
                        }
                        if (!data.hashtags) {
                            data.hashtags = [];
                        }
                        // console.log(params.newData);
                        // console.log("HASHTAGS", data)
                        this.oldProductCodeName = data.product_code;
                        this.form = {
                            type: data.type,
                            merchant_group: data.merchant_group ? data.merchant_group : null,
                            merchant_username: data.merchant_username ? data.merchant_username : null,
                            min_order: data.min_order,
                            product_code: data.product_code,
                            product_name: data.product_name,
                            category: data.category,
                            price: data.price,
                            fixed_price: data.fixed_price,
                            qty: data.qty,
                            description: data.description,
                            tnc: data.tnc,
                            dimensions: {
                                width: data.dimensions ? data.dimensions.width : 0,
                                length: data.dimensions ? data.dimensions.length : 0,
                                height: data.dimensions ? data.dimensions.height : 0
                            },
                            condition: data.condition,
                            weight: data.weight ? data.weight : 0,
                            active: data.active,
                            base_url: data.base_url,
                            pic_big_path: data.pic_big_path,
                            pic_small_path: data.pic_small_path,
                            pic_medium_path: data.pic_medium_path,
                            pic_file_name: data.pic_file_name,
                            hashtags: data.hashtags
                        };
                        console.log(this.form);
                        if (this.currentPermission == 'admin') {
                            this.form.merchant_username = data.merchant_username;
                        }
                        //by default weight is in Kg, need to convert it in gram
                        this.form.weightInGram = this.form.weight * 1000;
                        this.formPriceReplacer(null, 'price');
                        this.formPriceReplacer(null, 'fixed_price');
                        this.formPriceReplacer(null, 'weightInGram');
                        this.afterProductLoaded();
                        return [2 /*return*/];
                }
            });
        }); });
    };
    MerchantDetailProductComponent.prototype.genVoucherToggler = function (val) {
        this.generateVoucherToggler = val;
    };
    MerchantDetailProductComponent.prototype.afterProductLoaded = function () {
        return __awaiter(this, void 0, void 0, function () {
            var mCategories;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.categoryService.getCategoryList()];
                    case 1:
                        mCategories = _a.sent();
                        console.log(mCategories);
                        mCategories.result.category_list.forEach(function (element, index) {
                            if (_this.form.category == element.code) {
                                _this.form.category = element.name;
                                console.log("test array", _this.form.category[0]);
                            }
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    MerchantDetailProductComponent.prototype.addHashtags = function (newWord) {
        this.form.hashtags.push(newWord);
    };
    MerchantDetailProductComponent.prototype.hashtagsKeyDown = function (event) {
        var n = event.target.textContent;
        var key = event.key.toLowerCase();
        // console.log("key", key)
        if (key == " " || key == "enter") {
            if (n.trim() !== '') {
                this.addHashtags(n);
            }
            event.target.textContent = '';
            event.preventDefault();
        }
        if (key == 'backspace' && n == '' && this.form.hashtags.length > 0) {
            this.form.hashtags.pop();
        }
    };
    MerchantDetailProductComponent.prototype.productNameInputed = function ($event) {
        if (this.autoSKU == true) {
            var s = this.form.product_name.toLowerCase().replace(/[\s\?\*]/g, '-');
            this.form.product_code = s;
        }
    };
    MerchantDetailProductComponent.prototype.deleteThis = function () {
        return __awaiter(this, void 0, void 0, function () {
            var r;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.productService.deleteProduct({ _id: this.productID })];
                    case 1:
                        r = _a.sent();
                        if (r.result) {
                            alert("Your Product " + this.form.product_name + " Has been deleted");
                            this.router.navigate(['/merchant-portal/product-list']);
                            // window.location.reload();
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    MerchantDetailProductComponent.prototype.toggleDeleteConfirm = function (l) {
        this.toggleDelete = l;
    };
    MerchantDetailProductComponent.prototype.formPriceReplacer = function ($event, label) {
        if (this.form[label]) {
            if (typeof this.form[label] == 'number')
                this.form[label] = this.form[label].toString();
            var s = this.form[label].toLowerCase().replace(/[^0-9]/g, '');
            var c = parseInt(s);
            if (isNaN(c)) {
                this.form[label] = 0;
            }
            else {
                this.form[label] = c.toLocaleString('en');
            }
        }
    };
    MerchantDetailProductComponent.prototype.onFileSelected = function (event) {
        var _this = this;
        this.isFileUploaded = false;
        this.errorLabel = false;
        var fileMaxSize = 3000000; // let say 3Mb
        var reader = new FileReader();
        reader.onload = function (event) {
            _this.selFile = event.target.result;
        };
        reader.readAsDataURL(event.target.files[0]);
        Array.from(event.target.files).forEach(function (file) {
            if (file.size > fileMaxSize) {
                _this.errorLabel = "maximum file is 3Mb";
            }
        });
        // console.log(event.target.files, this.selFile);
        if (this.errorLabel == false) {
            this.selectedFile = event.target.files[0];
            setTimeout(function () {
                _this.onUpload();
            }, 1500);
        }
    };
    // onFileSelected(event){
    //   this.errorFile = false;
    //   let fileMaxSize = 3000000;// let say 3Mb
    //   Array.from(event.target.files).forEach((file: any) => {
    //       if(file.size > fileMaxSize){
    //         this.errorFile="maximum file is 3Mb";
    //       }
    //   });
    //   this.selectedFile = event.target.files[0];
    // } 
    MerchantDetailProductComponent.prototype.onUpload = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        this.cancel = false;
                        result = void 0;
                        console.log(this.selectedFile);
                        if (!this.selectedFile) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.productService.upload(this.selectedFile, this, 'image', function (result) { _this.callAfterUpload(result); })];
                    case 1:
                        result = _a.sent();
                        console.log(result);
                        _a.label = 2;
                    case 2: return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 4];
                    case 4: 
                    //   this.onUpload.
                    return [2 /*return*/, false];
                }
            });
        });
    };
    MerchantDetailProductComponent.prototype.generateVoucher = function () {
        return __awaiter(this, void 0, void 0, function () {
            var params;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(this.genVoucher.expiry_date && this.genVoucher.expiry_date.year)) return [3 /*break*/, 2];
                        params = {
                            expiry_date: this.genVoucher.expiry_date.year + "/" + this.genVoucher.expiry_date.month + "/" + this.genVoucher.expiry_date.day,
                            qty: this.genVoucher.qty,
                            product_code: this.form.product_code
                        };
                        return [4 /*yield*/, this.eVoucherService.generateEVoucherLint(params).then(function (result) {
                                if (result.result && result.result.status == 'success') {
                                    if (_this.currentPermission == 'admin') {
                                        _this.router.navigate(['administrator/evoucheradmin']);
                                    }
                                    else if (_this.currentPermission == 'merchant') {
                                        _this.router.navigate(['merchant-portal/product-list/generated-voucher/list']);
                                    }
                                }
                            })];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2: return [2 /*return*/];
                }
            });
        });
    };
    MerchantDetailProductComponent.prototype.generatedVoucher = function () {
        console.log("Test");
        if (this.currentPermission == 'admin') {
            this.router.navigate(['administrator/evoucheradmin'], { queryParams: { id: this.form.product_code } });
            console.log("Admin");
        }
        else if (this.currentPermission == 'merchant') {
            this.router.navigate(['merchant-portal/product-list/generated-voucher/list'], { queryParams: { id: this.form.product_code } });
            console.log("Merchant");
        }
    };
    MerchantDetailProductComponent.prototype.callAfterUpload = function (result) {
        console.log("the result is here", result);
        if (result) {
            var product = this.form;
            this.form = __assign({}, product, result);
            this.isFileUploaded = true;
        }
    };
    MerchantDetailProductComponent.prototype.stringToNumber = function (str) {
        if (typeof str == 'number') {
            str = str + '';
        }
        var s = str.toLowerCase().replace(/[^0-9]/g, '');
        return parseInt(s);
    };
    MerchantDetailProductComponent.prototype.parseStringToArray = function (str) {
        var splitR;
        if (!Object(_object_interface_common_object__WEBPACK_IMPORTED_MODULE_9__["isString"])(str))
            return [];
        else {
            splitR = str.split(/[\,\.]/);
        }
        return splitR;
    };
    MerchantDetailProductComponent.prototype.saveThis = function () {
        return __awaiter(this, void 0, void 0, function () {
            var data_1, e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.currentPermission == 'merchant') {
                            delete this.form.merchant_username;
                            // this.form.tnc = "no tnc"
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        this.validateForm(this.form);
                        data_1 = this.form;
                        data_1.price = this.stringToNumber(data_1.price);
                        data_1.fixed_price = this.stringToNumber(data_1.fixed_price);
                        data_1.category = this.parseStringToArray(data_1.category);
                        data_1.min_order = parseInt(data_1.min_order);
                        data_1.weightInGram = this.stringToNumber(data_1.weightInGram);
                        data_1.weight = data_1.weightInGram / 1000;
                        data_1.dimensions = {
                            length: parseFloat(data_1.dimensions.length),
                            width: parseFloat(data_1.dimensions.width),
                            height: parseFloat(data_1.dimensions.height),
                        };
                        if (data_1.currentPermission == 'admin' && this.form.merchant_username) {
                            data_1.merchant_username = this.form.merchant_username;
                        }
                        data_1.qty = parseInt(data_1.qty);
                        data_1.category = [];
                        data_1.weight = data_1.weightInGram / 1000;
                        this.multipleCategories.forEach(function (el) {
                            if (el.value)
                                data_1.category.push(el.code);
                        });
                        console.log("Data", data_1);
                        return [4 /*yield*/, this.productService.updateProduct(data_1, this.productID)];
                    case 2:
                        _a.sent();
                        this.errorLabel = false;
                        console.log(this.old_id);
                        console.log(data_1);
                        alert("Product/Voucher has been updated!");
                        this.router.navigate(['/merchant-portal/product-list/']);
                        return [3 /*break*/, 4];
                    case 3:
                        e_2 = _a.sent();
                        this.errorLabel = (e_2.message); //conversion to Error type
                        console.log("ERROR", this.errorLabel);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    MerchantDetailProductComponent.prototype.validateForm = function (form) {
        for (var prop in form) {
            if (!form.hasOwnProperty(prop) || prop == 'tnc' || prop == 'category' || prop == 'weightInGram' || prop == 'hashtags')
                continue;
            if (prop == 'base_url') {
                if (!form[prop]) {
                    throw new Error("Please upload new image for your product ");
                }
            }
            // console.log(prop, form[prop] == undefined, form[prop] == '')
            if (form[prop] == undefined) {
                console.log("PROP ERROR", prop, form);
                throw new Error("you need to completing this " + this.labels[prop]);
            }
            if (Object(_object_interface_common_object__WEBPACK_IMPORTED_MODULE_9__["isString"])(form[prop]) && form[prop].trim() == '')
                throw new Error("you need to completing this input " + this.labels[prop]);
            if (Object(ngx_bootstrap_chronos__WEBPACK_IMPORTED_MODULE_2__["isArray"])(form[prop]) && form[prop].length == 0) {
                console.log("PROP ERROR 2:", prop, form);
                throw new Error("Hi, you need to completing this " + this.labels[prop]);
            }
        }
    };
    MerchantDetailProductComponent.prototype.if = function (listOfProductType) {
    };
    MerchantDetailProductComponent.prototype.toggleCategories = function (index) {
        this.multipleCategories.forEach(function (el) {
            el.value = false;
        });
        this.multipleCategories[index].value = true;
    };
    MerchantDetailProductComponent.prototype.uploadFile = function () {
        return __awaiter(this, void 0, void 0, function () {
            var res, e_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        if (!this.selectedFile) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.productService.uploadFile(this.selectedFile, this, 'evoucher', this.callAfterUpload)];
                    case 1:
                        res = _a.sent();
                        console.log(res);
                        _a.label = 2;
                    case 2: return [3 /*break*/, 4];
                    case 3:
                        e_3 = _a.sent();
                        this.errorLabel = (e_3.message);
                        return [3 /*break*/, 4];
                    case 4:
                        if (this.selectedFile) { }
                        return [2 /*return*/];
                }
            });
        });
    };
    // editThis(){
    //   this.router.navigate(['/merchant-portal/product-list']);
    // }
    // setEdit(){
    //   if(this.edit_status){
    //     this.edit_status = false
    //   }
    //   else{
    //     this.edit_status = true
    //   }
    // }
    MerchantDetailProductComponent.prototype.back = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                console.log("trure");
                this.router.navigate(['merchant-portal/product-list']);
                return [2 /*return*/];
            });
        });
    };
    MerchantDetailProductComponent.ctorParameters = function () { return [
        { type: _services_product_product_service__WEBPACK_IMPORTED_MODULE_1__["ProductService"] },
        { type: _services_category_category_service__WEBPACK_IMPORTED_MODULE_4__["CategoryService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
        { type: _services_observerable_permission_observer__WEBPACK_IMPORTED_MODULE_6__["PermissionObserver"] },
        { type: _services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_7__["EVoucherService"] },
        { type: _services_merchant_merchant_service__WEBPACK_IMPORTED_MODULE_8__["MerchantService"] }
    ]; };
    MerchantDetailProductComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-merchant-detail-product-product',
            template: __webpack_require__(/*! raw-loader!./merchant-detail-product.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/separated-modules/merchant-products/products/merchant-detail-product/merchant-detail-product.component.html"),
            styles: [__webpack_require__(/*! ./merchant-detail-product.component.scss */ "./src/app/layout/merchant-portal/separated-modules/merchant-products/products/merchant-detail-product/merchant-detail-product.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_product_product_service__WEBPACK_IMPORTED_MODULE_1__["ProductService"],
            _services_category_category_service__WEBPACK_IMPORTED_MODULE_4__["CategoryService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"],
            _services_observerable_permission_observer__WEBPACK_IMPORTED_MODULE_6__["PermissionObserver"],
            _services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_7__["EVoucherService"],
            _services_merchant_merchant_service__WEBPACK_IMPORTED_MODULE_8__["MerchantService"]])
    ], MerchantDetailProductComponent);
    return MerchantDetailProductComponent;
}());



/***/ }),

/***/ "./src/app/layout/merchant-portal/separated-modules/merchant-products/products/merchant-edit-product/merchant-edit-product.component.scss":
/*!************************************************************************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/separated-modules/merchant-products/products/merchant-edit-product/merchant-edit-product.component.scss ***!
  \************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host ::ng-deep .ck-editor__editable_inline {\n  min-height: 500px;\n}\n\n.btn.btn-default {\n  background: white;\n  border: 1px solid #333;\n}\n\n#tooltip #tooltiptext {\n  visibility: hidden;\n  width: 120px;\n  background-color: black;\n  color: #fff;\n  text-align: center;\n  border-radius: 6px;\n  padding: 5px 0;\n  /* Position the tooltip */\n  position: absolute;\n  z-index: 1;\n}\n\n#tooltip:hover #tooltiptext {\n  visibility: visible;\n}\n\n.back_button {\n  margin-top: 12px;\n  margin-left: 40px;\n}\n\n.form {\n  padding: 25px;\n  position: relative;\n  padding-top: 50px;\n}\n\n.form label {\n  color: #757474;\n}\n\n.form .custom-content::-webkit-file-upload-button {\n  visibility: hidden;\n}\n\n.form .custom-content::before {\n  content: \"Choose File\";\n  margin-right: 5px;\n  display: inline-block;\n  border: 1px solid #999;\n  border-radius: 5px;\n  padding: 8px;\n  outline: none;\n  white-space: nowrap;\n  -webkit-user-select: none;\n  cursor: pointer;\n  font-weight: 700;\n  font-size: 10pt;\n  color: black;\n  letter-spacing: 0.5px;\n}\n\n.form .imagepict::before {\n  content: \"Choose Image\";\n}\n\n.form .hashhash .hashtag-item {\n  display: inline-block;\n  margin-right: 5px;\n  background: #eee;\n  padding: 2px 5px;\n  margin-bottom: 5px;\n  border-radius: 5px;\n}\n\n.form .hashhash .hashtag-item::before {\n  content: \"#\";\n  display: inline;\n}\n\n.form .hashhash .hashtagskey-input {\n  display: inline-block;\n  min-width: 75px;\n  margin-bottom: 5px;\n  font-size: 1rem;\n  background: white;\n  border-radius: 5px;\n  outline: none;\n}\n\n.form .hashhash .hashtagskey-input::before {\n  content: \"#\";\n  display: inline;\n}\n\n.form .card {\n  box-shadow: 0 1px 11px 0 rgba(0, 0, 0, 0.2), 0 2px 3px 0 rgba(0, 0, 0, 0.19);\n  margin-bottom: 20px;\n}\n\n.form .card .card-header {\n  background-color: white;\n  margin: 0px;\n  padding: 0px;\n  border-left: 3px solid #2480fb;\n}\n\n.form .card .card-header label {\n  margin: 20px 10px;\n  font-weight: bold;\n  font-size: 20px;\n}\n\n.form .card .image-upload {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  flex-direction: column;\n  border: 3px solid #ebebeb;\n  border-radius: 10px;\n  background-color: #f5f5f5;\n  min-height: 125px;\n  margin: 20px;\n  padding: 50px;\n}\n\n.form .card .image-upload .logo-upload {\n  width: 150px;\n}\n\n.form .card .image-upload .fileupload {\n  background-color: #2480fb;\n  color: white;\n  border-radius: 8px;\n  font-size: 14px;\n  padding: 12px 22px;\n}\n\n.form .card .image-upload .upload-desc {\n  color: #d6d6d6 !important;\n  text-align: center;\n  font-size: 18px;\n}\n\n.form .card .uploaded {\n  border-top: 1px solid #ccc;\n}\n\n.form .upload-container {\n  padding: 20px;\n  border-radius: 18px;\n  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.06);\n  background-color: #ffffff;\n  margin-bottom: 10px;\n}\n\n.form .multiple-image-upload .col-md-8 .form-group {\n  display: flex;\n  justify-content: space-between;\n  align-items: flex-start;\n}\n\n.form .multiple-image-upload .col-md-8 .form-group .label {\n  width: 38%;\n}\n\n.form .multiple-image-upload .col-md-6 .form-group {\n  display: flex;\n  justify-content: space-between;\n  align-items: flex-start;\n}\n\n.form .multiple-image-upload .col-md-6 .form-group .label {\n  width: 60%;\n}\n\n.form .multiple-image-upload .col-md-6 .form-group select.form-control:not([size]):not([multiple]) {\n  height: unset;\n}\n\n.form .multiple-image-upload .section-upload {\n  width: 100%;\n  align-items: center;\n}\n\n.form .multiple-image-upload .section-upload .col-2dot4,\n.form .multiple-image-upload .section-upload .col-sm-2dot4,\n.form .multiple-image-upload .section-upload .col-md-2dot4,\n.form .multiple-image-upload .section-upload .col-lg-2dot4,\n.form .multiple-image-upload .section-upload .col-xl-2dot4 {\n  position: relative;\n  width: 100%;\n  min-height: 1px;\n  padding-right: 15px;\n  padding-left: 15px;\n}\n\n.form .multiple-image-upload .section-upload .col-2dot4 {\n  flex: 0 0 20%;\n  max-width: 20%;\n}\n\n@media (min-width: 540px) {\n  .form .multiple-image-upload .section-upload .col-sm-2dot4 {\n    flex: 0 0 20%;\n    max-width: 20%;\n  }\n}\n\n@media (min-width: 720px) {\n  .form .multiple-image-upload .section-upload .col-md-2dot4 {\n    flex: 0 0 20%;\n    max-width: 20%;\n  }\n}\n\n@media (min-width: 960px) {\n  .form .multiple-image-upload .section-upload .col-lg-2dot4 {\n    flex: 0 0 20%;\n    max-width: 20%;\n  }\n}\n\n@media (min-width: 1140px) {\n  .form .multiple-image-upload .section-upload .col-xl-2dot4 {\n    flex: 0 0 20%;\n    max-width: 20%;\n  }\n}\n\n.form .multiple-image-upload .section-upload .col-md-2dot4 {\n  text-align: center;\n  font-size: 14px;\n}\n\n.form .multiple-image-upload .section-upload .col-md-2dot4 .blue-upload {\n  color: #56a4ff;\n  font-size: 16px;\n  font-weight: bold;\n}\n\n.form .multiple-image-upload .section-upload .col-md-2dot4 .card {\n  margin-top: 3vh;\n  height: 190px;\n  border: none;\n}\n\n.form .multiple-image-upload .section-upload .col-md-2dot4 .card .card-content {\n  height: 100%;\n  position: relative;\n}\n\n.form .multiple-image-upload .section-upload .col-md-2dot4 .card .card-content .upload-desc {\n  color: #bbb !important;\n  text-align: center;\n  font-size: 14px;\n}\n\n.form .multiple-image-upload .section-upload .col-md-2dot4 .card .card-content .logo-upload {\n  width: 100%;\n  background: white;\n}\n\n.form .multiple-image-upload .section-upload .col-md-2dot4 .card .card-content .image {\n  display: block;\n  float: left;\n  height: auto;\n  width: 100%;\n  height: 100%;\n}\n\n.form .multiple-image-upload .section-upload .col-md-2dot4 .card .card-content .image .custom {\n  height: 100%;\n  border-radius: 10px;\n  justify-content: center;\n  align-items: center;\n  display: flex;\n  flex-direction: column;\n}\n\n.form .multiple-image-upload .section-upload .col-md-2dot4 .card .card-content .image .custom .drag-drop {\n  font-size: 10px;\n  text-align: center;\n}\n\n.form .multiple-image-upload .section-upload .col-md-2dot4 .card .card-content .image .custom .set-opacity {\n  opacity: 0.1;\n  height: 60px;\n  width: auto;\n}\n\n.form .multiple-image-upload .section-upload .col-md-2dot4 .card .form-group.uploaded {\n  position: relative;\n  margin-bottom: 0px;\n  height: 100%;\n}\n\n.form .multiple-image-upload .section-upload .col-md-2dot4 .card .form-group.uploaded .remove-icon {\n  position: absolute;\n  right: 0;\n  top: 0;\n  color: red;\n  font-size: 22px;\n}\n\n.form .multiple-image-upload .section-upload .col-md-2dot4 .card .form-group.uploaded .loading {\n  height: 100%;\n}\n\n.form .multiple-image-upload .section-upload .col-md-2dot4 .card .form-group.uploaded .loading .sk-fading-circle {\n  top: 50%;\n}\n\n.form .multiple-image-upload .section-upload .col-md-2dot4 .card .form-group.uploaded .uploaded-image2 {\n  height: 100%;\n}\n\n.form .multiple-image-upload .section-upload .col-md-2dot4 .card .form-group.uploaded .uploaded-image2 .img {\n  padding: 15px;\n  height: 100%;\n  width: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  flex-direction: column;\n  overflow: hidden;\n}\n\n.form .multiple-image-upload .section-upload .col-md-2dot4 .card .form-group.uploaded .uploaded-image2 .img > img {\n  height: 100%;\n}\n\n.form .multiple-image-upload .section-upload .col-md-2dot4 .card .form-group.uploaded .uploaded-image2 .editor-tool {\n  height: 100%;\n}\n\n.form .multiple-image-upload .section-upload .col-md-2dot4 {\n  text-align: center;\n}\n\n.form .multiple-image-upload .section-upload .col-md-2dot4 .card {\n  height: 250px;\n  border: none;\n  border-radius: 8px;\n  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.05);\n  background-color: #ffffff;\n}\n\n.form .multiple-image-upload .section-upload .col-md-2dot4 .card .card-content {\n  height: 100%;\n  position: relative;\n}\n\n.form .multiple-image-upload .section-upload .col-md-2dot4 .card .card-content .image-canvas {\n  width: 80%;\n}\n\n.form .multiple-image-upload .section-upload .col-md-2dot4 .card .card-content .upload-desc {\n  color: #bbb !important;\n  text-align: center;\n  font-size: 14px;\n}\n\n.form .multiple-image-upload .section-upload .col-md-2dot4 .card .card-content .logo-upload {\n  width: 100%;\n  background: white;\n}\n\n.form .multiple-image-upload .section-upload .col-md-2dot4 .card .card-content .image {\n  display: block;\n  float: left;\n  height: auto;\n  width: 100%;\n  height: 100%;\n}\n\n.form .multiple-image-upload .section-upload .col-md-2dot4 .card .card-content .image .custom {\n  height: 100%;\n  border-radius: 10px;\n  justify-content: center;\n  align-items: center;\n  display: flex;\n  flex-direction: column;\n}\n\n.form .multiple-image-upload .section-upload .col-md-2dot4 .card .card-content .image .custom .drag-drop {\n  font-size: 10px;\n  text-align: center;\n}\n\n.form .multiple-image-upload .section-upload .col-md-2dot4 .card .card-content .image .custom .set-opacity {\n  opacity: 0.1;\n  height: 60px;\n  width: auto;\n}\n\n.form .multiple-image-upload .section-upload .col-md-2dot4 .card .form-group.uploaded {\n  position: relative;\n  margin-bottom: 0px;\n  height: 100%;\n}\n\n.form .multiple-image-upload .section-upload .col-md-2dot4 .card .form-group.uploaded .loading {\n  height: 100%;\n}\n\n.form .multiple-image-upload .section-upload .col-md-2dot4 .card .form-group.uploaded .loading .sk-fading-circle {\n  top: 50%;\n}\n\n.form .multiple-image-upload .section-upload .col-md-2dot4 .card .form-group.uploaded .uploaded-image2 {\n  height: 100%;\n}\n\n.form .multiple-image-upload .section-upload .col-md-2dot4 .card .form-group.uploaded .uploaded-image2 .img {\n  height: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n.form .multiple-image-upload .section-upload .col-md-2dot4 .card .form-group.uploaded .uploaded-image2 .img > img {\n  height: 100%;\n}\n\n.form .multiple-image-upload .section-upload .col-md-2dot4 .card .form-group.uploaded .uploaded-image2 .editor-tool {\n  height: 100%;\n}\n\n.form .navbar-menu {\n  overflow-y: scroll;\n  max-height: 250px;\n  margin: 15px 5px;\n}\n\n.form .navbar-menu .category-list .list-group-item {\n  border: none;\n  cursor: pointer;\n}\n\n.form .navbar-menu .category-list .list-group-item .checked-icon {\n  display: inline-flex;\n  justify-content: center;\n  align-items: center;\n  width: 20px;\n  height: 20px;\n  border-radius: 15px;\n  border: 1px solid #ccc;\n  color: #3498db;\n  font-weight: bold;\n  font-size: 14px;\n  margin-right: 10px;\n}\n\n.form .navbar-menu .category-list .list-group-item .checked-icon.false {\n  color: transparent;\n}\n\n.form .uploading-file {\n  opacity: 0.5;\n  -webkit-filter: grayscale(1);\n          filter: grayscale(1);\n}\n\n.form .uploading-file.true {\n  opacity: 1;\n  -webkit-filter: grayscale(0);\n          filter: grayscale(0);\n}\n\n.form .autoSKU {\n  font-size: 12px;\n  margin-left: 15px;\n  display: inline-block;\n}\n\n.form .image-upload {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  flex-direction: column;\n  border: 1px dashed #ccc;\n  min-height: 125px;\n}\n\n.form .uploaded-image {\n  margin: 0px auto;\n  width: auto;\n  padding: 50px;\n  height: auto;\n  cursor: pointer;\n}\n\n.form .uploaded-image .text-fileupload {\n  display: block;\n  text-align: center;\n  cursor: pointer;\n  color: #2480fb;\n  margin-top: 10px;\n}\n\n.form .uploaded-image .img {\n  width: 100%;\n  min-height: 348px;\n  background-size: cover;\n  position: relative;\n  background-position: center center;\n  background-repeat: no-repeat;\n}\n\n.form .uploaded-image .img .editor-tool {\n  background: rgba(0, 0, 0, 0.2);\n  width: 100%;\n  height: 100%;\n  position: relative;\n  opacity: 0;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n.form .uploaded-image .img:hover .editor-tool {\n  opacity: 0.9;\n}\n\n.form .carousel-caption {\n  background: rgba(0, 0, 0, 0.2);\n}\n\n.form .carousel-indicators li {\n  background-color: black !important;\n}\n\n.form .product-type-list {\n  display: inline-block;\n}\n\n.form .product-type-list .product-type {\n  display: inline-flex;\n  padding: 15px;\n  justify-content: center;\n  align-items: center;\n  height: 35px;\n  border-radius: 7px;\n  cursor: pointer;\n  border: 1px solid #ccc;\n  margin-right: 5px;\n  margin-bottom: 5px;\n  transition: ease 0.2s all 0s;\n}\n\n.form .product-type-list .product-type.true {\n  color: white;\n  background-color: #3498db;\n  border-color: #2980b9;\n}\n\n.form .form-control.des {\n  padding-top: 4px;\n  display: inline-flex;\n  padding-bottom: 4px;\n  justify-content: center;\n  align-items: center;\n}\n\n.form .form-control.des input {\n  border: 0px solid transparent;\n  width: calc(100% - 15px);\n}\n\n.form .form-control.des input:focus {\n  outline: none;\n}\n\n.form div.error {\n  background: #e74c3c;\n  color: white;\n  padding: 25px;\n  margin-bottom: 25px;\n}\n\n.form div.new {\n  background: limegreen;\n  color: white;\n  padding: 25px;\n  margin-bottom: 25px;\n}\n\n.form .category-list .list-group-item {\n  cursor: pointer;\n}\n\n.form .category-list .list-group-item .checked-icon {\n  display: inline-flex;\n  justify-content: center;\n  align-items: center;\n  width: 20px;\n  height: 20px;\n  border-radius: 15px;\n  border: 1px solid #ccc;\n  color: #3498db;\n  font-weight: bold;\n  font-size: 14px;\n}\n\n.form .category-list .list-group-item .checked-icon.false {\n  color: transparent;\n}\n\n.form .end-footer .delete {\n  position: relative;\n  float: right;\n  background-color: #e74c3c;\n  opacity: 0.5;\n  border-color: #c0392b;\n}\n\n.form .end-footer .delete .delete-modal {\n  position: absolute;\n  display: none;\n  background: white;\n  border: 1px solid #ccc;\n  border-radius: 10px;\n  padding: 4px 10px;\n  top: 0px;\n  right: 0px;\n  color: #e74c3c;\n  -webkit-transform: translateY(-45px);\n          transform: translateY(-45px);\n}\n\n.form .end-footer .delete .delete-modal .btn-group {\n  border: 1px solid #e74c3c;\n  border-radius: 5px;\n  overflow: hidden;\n}\n\n.form .end-footer .delete .delete-modal .btn-group .no {\n  opacity: 0.5;\n  border-width: 0px;\n}\n\n.form .end-footer .delete .delete-modal .btn-group .yes {\n  background: #e74c3c;\n  border-width: 0px;\n}\n\n.form .end-footer .delete .delete-modal .btn-group .no:hover {\n  opacity: 1;\n}\n\n.form .end-footer .delete:hover,\n.form .end-footer .delete:focus {\n  opacity: 1;\n}\n\n.form .end-footer .delete:focus .delete-modal {\n  display: block;\n}\n\n.form.true {\n  pointer-events: none;\n  /* for \"disabled\" effect */\n  opacity: 0.5;\n  background: #ecf0f1;\n}\n\n.button-edit {\n  position: absolute;\n  margin-left: calc(50% - 100px);\n  background-color: #3498db;\n  color: white;\n  top: 10px;\n}\n\n.varian-button {\n  cursor: pointer;\n}\n\n.varian-image-parent {\n  position: relative;\n}\n\n.varian-table {\n  margin: 40px 0;\n}\n\n.delete-varian-image {\n  position: absolute;\n  right: 22px;\n  top: 22px;\n  color: white;\n  font-size: 24px;\n  cursor: pointer;\n}\n\n.varian-parent {\n  position: relative;\n}\n\n.varian-parent ::ng-deep .input-group {\n  width: 91%;\n}\n\n.varian-parent i {\n  position: absolute;\n  right: 20px;\n  top: 10px;\n}\n\n.add-value {\n  margin-bottom: 20px;\n}\n\n.form-varian {\n  padding-left: 0;\n  padding-right: 0;\n}\n\n.red-alert {\n  border: solid 1px red;\n}\n\n.red-error-alert {\n  color: red;\n  font-size: 12px;\n}\n\n.error-message {\n  background: white;\n  height: 100vh;\n  text-align: center;\n  font-size: 26px;\n  padding-top: 40px;\n}\n\n.error-message .text-message {\n  background: #E74C3C;\n  padding: 20px;\n  margin: 0 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21lcmNoYW50LXBvcnRhbC9zZXBhcmF0ZWQtbW9kdWxlcy9tZXJjaGFudC1wcm9kdWN0cy9wcm9kdWN0cy9tZXJjaGFudC1lZGl0LXByb2R1Y3QvRDpcXGFpc3lhaFxcY2xzXFxhbmd1bGFyLWFkbWluLXNpZy12LjIuMC9zcmNcXGFwcFxcbGF5b3V0XFxtZXJjaGFudC1wb3J0YWxcXHNlcGFyYXRlZC1tb2R1bGVzXFxtZXJjaGFudC1wcm9kdWN0c1xccHJvZHVjdHNcXG1lcmNoYW50LWVkaXQtcHJvZHVjdFxcbWVyY2hhbnQtZWRpdC1wcm9kdWN0LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvbWVyY2hhbnQtcG9ydGFsL3NlcGFyYXRlZC1tb2R1bGVzL21lcmNoYW50LXByb2R1Y3RzL3Byb2R1Y3RzL21lcmNoYW50LWVkaXQtcHJvZHVjdC9tZXJjaGFudC1lZGl0LXByb2R1Y3QuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxpQkFBQTtBQ0NKOztBREVFO0VBQ0UsaUJBQUE7RUFDQSxzQkFBQTtBQ0NKOztBREVFO0VBQ0Usa0JBQUE7RUFDQSxZQUFBO0VBQ0EsdUJBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFFQSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtBQ0FKOztBREdBO0VBQ0ksbUJBQUE7QUNBSjs7QURHQTtFQUNFLGdCQUFBO0VBQ0EsaUJBQUE7QUNBRjs7QURHRTtFQUNFLGFBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0FDQUo7O0FET0k7RUFDRSxjQUFBO0FDTE47O0FEUUk7RUFDRSxrQkFBQTtBQ05OOztBRFNJO0VBQ0Usc0JBQUE7RUFDQSxpQkFBQTtFQUNBLHFCQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx5QkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EscUJBQUE7QUNQTjs7QURVSTtFQUNFLHVCQUFBO0FDUk47O0FEWU07RUFDRSxxQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUNWUjs7QURZTTtFQUNFLFlBQUE7RUFDQSxlQUFBO0FDVlI7O0FEWU07RUFDRSxxQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtBQ1ZSOztBRFlNO0VBQ0UsWUFBQTtFQUNBLGVBQUE7QUNWUjs7QURjSTtFQUNFLDRFQUFBO0VBQ0EsbUJBQUE7QUNaTjs7QURhTTtFQUNFLHVCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSw4QkFBQTtBQ1hSOztBRFlRO0VBQ0UsaUJBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7QUNWVjs7QURjTTtFQUNFLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSxpQkFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0FDWlI7O0FEY1E7RUFDRSxZQUFBO0FDWlY7O0FEZVE7RUFDRSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtBQ2JWOztBRGdCUTtFQUNFLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FDZFY7O0FEa0JNO0VBQ0UsMEJBQUE7QUNoQlI7O0FEbUJJO0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsMkNBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0FDakJOOztBRHFCWTtFQUNJLGFBQUE7RUFDQSw4QkFBQTtFQUNBLHVCQUFBO0FDbkJoQjs7QURvQmdCO0VBQ0ksVUFBQTtBQ2xCcEI7O0FEdUJZO0VBQ0ksYUFBQTtFQUNBLDhCQUFBO0VBQ0EsdUJBQUE7QUNyQmhCOztBRHNCZ0I7RUFDSSxVQUFBO0FDcEJwQjs7QURzQmdCO0VBQ0ksYUFBQTtBQ3BCcEI7O0FEeUJRO0VBRUksV0FBQTtFQUVBLG1CQUFBO0FDekJaOztBRDJCWTs7Ozs7RUFLSSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBQ3pCaEI7O0FENEJZO0VBR0ksYUFBQTtFQUNBLGNBQUE7QUMxQmhCOztBRDZCWTtFQUNJO0lBR0ksYUFBQTtJQUNBLGNBQUE7RUMzQmxCO0FBQ0Y7O0FEOEJZO0VBQ0k7SUFHSSxhQUFBO0lBQ0EsY0FBQTtFQzVCbEI7QUFDRjs7QUQrQlk7RUFDSTtJQUdJLGFBQUE7SUFDQSxjQUFBO0VDN0JsQjtBQUNGOztBRGdDWTtFQUNJO0lBR0ksYUFBQTtJQUNBLGNBQUE7RUM5QmxCO0FBQ0Y7O0FEZ0NZO0VBQ0ksa0JBQUE7RUFDQSxlQUFBO0FDOUJoQjs7QURpQ2dCO0VBQ0ksY0FBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQy9CcEI7O0FEa0NnQjtFQUNJLGVBQUE7RUFDQSxhQUFBO0VBQ0EsWUFBQTtBQ2hDcEI7O0FEa0NvQjtFQUNJLFlBQUE7RUFDQSxrQkFBQTtBQ2hDeEI7O0FEa0N3QjtFQUNJLHNCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FDaEM1Qjs7QURtQ3dCO0VBQ0ksV0FBQTtFQUNBLGlCQUFBO0FDakM1Qjs7QURvQ3dCO0VBQ0ksY0FBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUNsQzVCOztBRG9DNEI7RUFDSSxZQUFBO0VBRUEsbUJBQUE7RUFFQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0FDcENoQzs7QUR1Q2dDO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0FDckNwQzs7QUR3Q2dDO0VBQ0ksWUFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FDdENwQzs7QUQ0Q29CO0VBQ0ksa0JBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUMxQ3hCOztBRDJDd0I7RUFDRSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxNQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7QUN6QzFCOztBRDJDd0I7RUFDSSxZQUFBO0FDekM1Qjs7QUQyQzRCO0VBQ0ksUUFBQTtBQ3pDaEM7O0FENkN3QjtFQUNJLFlBQUE7QUMzQzVCOztBRDZDNEI7RUFDSSxhQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0VBQ0EsZ0JBQUE7QUMzQ2hDOztBRDhDNEI7RUFDSSxZQUFBO0FDNUNoQzs7QURnRDRCO0VBQ0ksWUFBQTtBQzlDaEM7O0FEcURZO0VBQ0ksa0JBQUE7QUNuRGhCOztBRHFEZ0I7RUFFSSxhQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsMkNBQUE7RUFDQSx5QkFBQTtBQ3BEcEI7O0FEdURvQjtFQUNJLFlBQUE7RUFDQSxrQkFBQTtBQ3JEeEI7O0FEdUR3QjtFQUNJLFVBQUE7QUNyRDVCOztBRHdEd0I7RUFDSSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQ3RENUI7O0FEeUR3QjtFQUNJLFdBQUE7RUFDQSxpQkFBQTtBQ3ZENUI7O0FEMER3QjtFQUNJLGNBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FDeEQ1Qjs7QUQwRDRCO0VBQ0ksWUFBQTtFQUVBLG1CQUFBO0VBQ0EsdUJBQUE7RUFFQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtBQzFEaEM7O0FENkRnQztFQUNJLGVBQUE7RUFDQSxrQkFBQTtBQzNEcEM7O0FEOERnQztFQUNJLFlBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQzVEcEM7O0FEa0VvQjtFQUNJLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0FDaEV4Qjs7QURrRXdCO0VBQ0ksWUFBQTtBQ2hFNUI7O0FEa0U0QjtFQUNJLFFBQUE7QUNoRWhDOztBRG9Fd0I7RUFDSSxZQUFBO0FDbEU1Qjs7QURvRTRCO0VBQ0ksWUFBQTtFQUVBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FDbkVoQzs7QURzRTRCO0VBQ0ksWUFBQTtBQ3BFaEM7O0FEd0U0QjtFQUNJLFlBQUE7QUN0RWhDOztBRCtFSTtFQUNFLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtBQzdFTjs7QUQrRVE7RUFDRSxZQUFBO0VBQ0EsZUFBQTtBQzdFVjs7QUQ4RVU7RUFDRSxvQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QUM1RVo7O0FEOEVVO0VBQ0Usa0JBQUE7QUM1RVo7O0FEa0ZJO0VBQ0UsWUFBQTtFQUNBLDRCQUFBO1VBQUEsb0JBQUE7QUNoRk47O0FEbUZJO0VBQ0UsVUFBQTtFQUNBLDRCQUFBO1VBQUEsb0JBQUE7QUNqRk47O0FEbUZJO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0VBQ0EscUJBQUE7QUNqRk47O0FEb0ZJO0VBQ0UsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsaUJBQUE7QUNsRk47O0FEb0ZJO0VBQ0UsZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0FDbEZOOztBRG1GTTtFQUNFLGNBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7QUNqRlI7O0FEb0ZNO0VBQ0UsV0FBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtFQUNBLGtDQUFBO0VBQ0EsNEJBQUE7QUNsRlI7O0FEb0ZRO0VBQ0UsOEJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FDbEZWOztBRHFGTTtFQUNFLFlBQUE7QUNuRlI7O0FEdUZJO0VBQ0UsOEJBQUE7QUNyRk47O0FEd0ZJO0VBQ0Usa0NBQUE7QUN0Rk47O0FEeUZJO0VBQ0UscUJBQUE7QUN2Rk47O0FEd0ZNO0VBQ0Usb0JBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxzQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSw0QkFBQTtBQ3RGUjs7QUR3Rk07RUFDRSxZQUFBO0VBQ0EseUJBQUE7RUFDQSxxQkFBQTtBQ3RGUjs7QUR5Rkk7RUFDRSxnQkFBQTtFQUNBLG9CQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FDdkZOOztBRHdGTTtFQUNFLDZCQUFBO0VBQ0Esd0JBQUE7QUN0RlI7O0FEd0ZNO0VBQ0UsYUFBQTtBQ3RGUjs7QUR5Rkk7RUFDRSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7QUN2Rk47O0FEeUZJO0VBQ0UscUJBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0FDdkZOOztBRDBGTTtFQUNFLGVBQUE7QUN4RlI7O0FEeUZRO0VBQ0Usb0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtBQ3ZGVjs7QUR5RlE7RUFDRSxrQkFBQTtBQ3ZGVjs7QUQ0Rk07RUFDRSxrQkFBQTtFQUNBLFlBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxxQkFBQTtBQzFGUjs7QUQyRlE7RUFDRSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxpQkFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLFFBQUE7RUFDQSxVQUFBO0VBQ0EsY0FBQTtFQUNBLG9DQUFBO1VBQUEsNEJBQUE7QUN6RlY7O0FEMEZVO0VBQ0UseUJBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FDeEZaOztBRHlGWTtFQUNFLFlBQUE7RUFDQSxpQkFBQTtBQ3ZGZDs7QUR5Rlk7RUFDRSxtQkFBQTtFQUNBLGlCQUFBO0FDdkZkOztBRHlGWTtFQUNFLFVBQUE7QUN2RmQ7O0FENEZNOztFQUVFLFVBQUE7QUMxRlI7O0FENkZRO0VBQ0UsY0FBQTtBQzNGVjs7QURpR0U7RUFDRSxvQkFBQTtFQUVBLDBCQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0FDL0ZKOztBRGtHRTtFQUNFLGtCQUFBO0VBQ0EsOEJBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxTQUFBO0FDL0ZKOztBRGtHQTtFQUNFLGVBQUE7QUMvRkY7O0FEa0dBO0VBQ0Usa0JBQUE7QUMvRkY7O0FEa0dBO0VBQ0ksY0FBQTtBQy9GSjs7QURrR0E7RUFDSSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxlQUFBO0FDL0ZKOztBRGtHQTtFQUNJLGtCQUFBO0FDL0ZKOztBRGtHUTtFQUNJLFVBQUE7QUNoR1o7O0FEb0dJO0VBQ0ksa0JBQUE7RUFDQSxXQUFBO0VBQ0EsU0FBQTtBQ2xHUjs7QURzR0E7RUFDSSxtQkFBQTtBQ25HSjs7QURzR0E7RUFDRSxlQUFBO0VBQ0EsZ0JBQUE7QUNuR0Y7O0FEc0dBO0VBQ0UscUJBQUE7QUNuR0Y7O0FEc0dBO0VBQ0UsVUFBQTtFQUNBLGVBQUE7QUNuR0Y7O0FEc0dBO0VBQ0UsaUJBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUNuR0Y7O0FEcUdFO0VBQ0ksbUJBQUE7RUFDQSxhQUFBO0VBQ0EsY0FBQTtBQ25HTiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tZXJjaGFudC1wb3J0YWwvc2VwYXJhdGVkLW1vZHVsZXMvbWVyY2hhbnQtcHJvZHVjdHMvcHJvZHVjdHMvbWVyY2hhbnQtZWRpdC1wcm9kdWN0L21lcmNoYW50LWVkaXQtcHJvZHVjdC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IDo6bmctZGVlcCAuY2stZWRpdG9yX19lZGl0YWJsZV9pbmxpbmUge1xyXG4gICAgbWluLWhlaWdodDogNTAwcHg7XHJcbiAgfVxyXG4gIFxyXG4gIC5idG4uYnRuLWRlZmF1bHR7XHJcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICMzMzM7XHJcbiAgfVxyXG5cclxuICAjdG9vbHRpcCAjdG9vbHRpcHRleHQge1xyXG4gICAgdmlzaWJpbGl0eTogaGlkZGVuO1xyXG4gICAgd2lkdGg6IDEyMHB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDZweDtcclxuICAgIHBhZGRpbmc6IDVweCAwO1xyXG5cclxuICAgIC8qIFBvc2l0aW9uIHRoZSB0b29sdGlwICovXHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB6LWluZGV4OiAxO1xyXG59XHJcblxyXG4jdG9vbHRpcDpob3ZlciAjdG9vbHRpcHRleHQge1xyXG4gICAgdmlzaWJpbGl0eTogdmlzaWJsZTtcclxufVxyXG5cclxuLmJhY2tfYnV0dG9uIHtcclxuICBtYXJnaW4tdG9wOiAxMnB4O1xyXG4gIG1hcmdpbi1sZWZ0OiA0MHB4O1xyXG59XHJcbiAgXHJcbiAgLmZvcm0ge1xyXG4gICAgcGFkZGluZzogMjVweDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHBhZGRpbmctdG9wOiA1MHB4O1xyXG4gICAgLy8gdG9wOiA1dnc7XHJcbiAgXHJcbiAgICBpbnB1dFt0eXBlPVwiZmlsZVwiXSB7XHJcbiAgICAgIC8vIGRpc3BsYXk6IG5vbmU7XHJcbiAgICB9XHJcbiAgXHJcbiAgICBsYWJlbCB7XHJcbiAgICAgIGNvbG9yOiAjNzU3NDc0O1xyXG4gICAgfVxyXG4gIFxyXG4gICAgLmN1c3RvbS1jb250ZW50Ojotd2Via2l0LWZpbGUtdXBsb2FkLWJ1dHRvbiB7XHJcbiAgICAgIHZpc2liaWxpdHk6IGhpZGRlbjtcclxuICAgIH1cclxuICBcclxuICAgIC5jdXN0b20tY29udGVudDo6YmVmb3JlIHtcclxuICAgICAgY29udGVudDogXCJDaG9vc2UgRmlsZVwiO1xyXG4gICAgICBtYXJnaW4tcmlnaHQ6IDVweDtcclxuICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICBib3JkZXI6IDFweCBzb2xpZCAjOTk5O1xyXG4gICAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICAgIHBhZGRpbmc6IDhweDtcclxuICAgICAgb3V0bGluZTogbm9uZTtcclxuICAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAgICAgLXdlYmtpdC11c2VyLXNlbGVjdDogbm9uZTtcclxuICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICBmb250LXdlaWdodDogNzAwO1xyXG4gICAgICBmb250LXNpemU6IDEwcHQ7XHJcbiAgICAgIGNvbG9yOiBibGFjaztcclxuICAgICAgbGV0dGVyLXNwYWNpbmc6IDAuNXB4O1xyXG4gICAgfVxyXG4gIFxyXG4gICAgLmltYWdlcGljdDo6YmVmb3JlIHtcclxuICAgICAgY29udGVudDogXCJDaG9vc2UgSW1hZ2VcIjtcclxuICAgIH1cclxuICBcclxuICAgIC5oYXNoaGFzaCB7XHJcbiAgICAgIC5oYXNodGFnLWl0ZW0ge1xyXG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDVweDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjZWVlO1xyXG4gICAgICAgIHBhZGRpbmc6IDJweCA1cHg7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogNXB4O1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgICAgfVxyXG4gICAgICAuaGFzaHRhZy1pdGVtOjpiZWZvcmUge1xyXG4gICAgICAgIGNvbnRlbnQ6IFwiI1wiO1xyXG4gICAgICAgIGRpc3BsYXk6IGlubGluZTtcclxuICAgICAgfVxyXG4gICAgICAuaGFzaHRhZ3NrZXktaW5wdXQge1xyXG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICBtaW4td2lkdGg6IDc1cHg7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogNXB4O1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMXJlbTtcclxuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICAgICAgb3V0bGluZTogbm9uZTtcclxuICAgICAgfVxyXG4gICAgICAuaGFzaHRhZ3NrZXktaW5wdXQ6OmJlZm9yZSB7XHJcbiAgICAgICAgY29udGVudDogXCIjXCI7XHJcbiAgICAgICAgZGlzcGxheTogaW5saW5lO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgXHJcbiAgICAuY2FyZCB7XHJcbiAgICAgIGJveC1zaGFkb3c6IDAgMXB4IDExcHggMCByZ2JhKDAsIDAsIDAsIDAuMiksIDAgMnB4IDNweCAwIHJnYmEoMCwgMCwgMCwgMC4xOSk7XHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbiAgICAgIC5jYXJkLWhlYWRlciB7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgICAgICAgbWFyZ2luOiAwcHg7XHJcbiAgICAgICAgcGFkZGluZzogMHB4O1xyXG4gICAgICAgIGJvcmRlci1sZWZ0OiAzcHggc29saWQgIzI0ODBmYjtcclxuICAgICAgICBsYWJlbCB7XHJcbiAgICAgICAgICBtYXJnaW46IDIwcHggMTBweDtcclxuICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gIFxyXG4gICAgICAuaW1hZ2UtdXBsb2FkIHtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgICBib3JkZXI6IDNweCBzb2xpZCAjZWJlYmViO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2Y1ZjVmNTtcclxuICAgICAgICBtaW4taGVpZ2h0OiAxMjVweDtcclxuICAgICAgICBtYXJnaW46IDIwcHg7XHJcbiAgICAgICAgcGFkZGluZzogNTBweDtcclxuICAgIFxyXG4gICAgICAgIC5sb2dvLXVwbG9hZCB7XHJcbiAgICAgICAgICB3aWR0aDogMTUwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgXHJcbiAgICAgICAgLmZpbGV1cGxvYWQge1xyXG4gICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzI0ODBmYjtcclxuICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgICAgIHBhZGRpbmc6IDEycHggMjJweDtcclxuICAgICAgICB9XHJcbiAgICBcclxuICAgICAgICAudXBsb2FkLWRlc2Mge1xyXG4gICAgICAgICAgY29sb3I6ICNkNmQ2ZDYgIWltcG9ydGFudDtcclxuICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICBcclxuICAgICAgLnVwbG9hZGVkIHtcclxuICAgICAgICBib3JkZXItdG9wOiAxcHggc29saWQgI2NjYztcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgLnVwbG9hZC1jb250YWluZXIge1xyXG4gICAgICBwYWRkaW5nOiAyMHB4O1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAxOHB4O1xyXG4gICAgICBib3gtc2hhZG93OiAwIDNweCA2cHggMCByZ2JhKDAsIDAsIDAsIDAuMDYpO1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gICAgfVxyXG4gICAgLm11bHRpcGxlLWltYWdlLXVwbG9hZCB7XHJcbiAgICAgICAgLmNvbC1tZC04e1xyXG4gICAgICAgICAgICAuZm9ybS1ncm91cHtcclxuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICAgICAgICAgICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcclxuICAgICAgICAgICAgICAgIC5sYWJlbCB7XHJcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDM4JTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAuY29sLW1kLTYge1xyXG4gICAgICAgICAgICAuZm9ybS1ncm91cHtcclxuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICAgICAgICAgICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcclxuICAgICAgICAgICAgICAgIC5sYWJlbCB7XHJcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDYwJTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHNlbGVjdC5mb3JtLWNvbnRyb2w6bm90KFtzaXplXSk6bm90KFttdWx0aXBsZV0pIHtcclxuICAgICAgICAgICAgICAgICAgICBoZWlnaHQgOiB1bnNldDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgIFxyXG4gICAgICAgIH1cclxuICAgICAgICAuc2VjdGlvbi11cGxvYWQge1xyXG4gICAgICAgICAgICAvLyBtYXJnaW4tdG9wOiA1dmg7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICBcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgXHJcbiAgICAgICAgICAgIC5jb2wtMmRvdDQsXHJcbiAgICAgICAgICAgIC5jb2wtc20tMmRvdDQsXHJcbiAgICAgICAgICAgIC5jb2wtbWQtMmRvdDQsXHJcbiAgICAgICAgICAgIC5jb2wtbGctMmRvdDQsXHJcbiAgICAgICAgICAgIC5jb2wteGwtMmRvdDQge1xyXG4gICAgICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICBtaW4taGVpZ2h0OiAxcHg7XHJcbiAgICAgICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxNXB4O1xyXG4gICAgICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAxNXB4O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgIFxyXG4gICAgICAgICAgICAuY29sLTJkb3Q0IHtcclxuICAgICAgICAgICAgICAgIC13ZWJraXQtYm94LWZsZXg6IDA7XHJcbiAgICAgICAgICAgICAgICAtbXMtZmxleDogMCAwIDIwJTtcclxuICAgICAgICAgICAgICAgIGZsZXg6IDAgMCAyMCU7XHJcbiAgICAgICAgICAgICAgICBtYXgtd2lkdGg6IDIwJTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICBcclxuICAgICAgICAgICAgQG1lZGlhIChtaW4td2lkdGg6IDU0MHB4KSB7XHJcbiAgICAgICAgICAgICAgICAuY29sLXNtLTJkb3Q0IHtcclxuICAgICAgICAgICAgICAgICAgICAtd2Via2l0LWJveC1mbGV4OiAwO1xyXG4gICAgICAgICAgICAgICAgICAgIC1tcy1mbGV4OiAwIDAgMjAlO1xyXG4gICAgICAgICAgICAgICAgICAgIGZsZXg6IDAgMCAyMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgbWF4LXdpZHRoOiAyMCU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgXHJcbiAgICAgICAgICAgIEBtZWRpYSAobWluLXdpZHRoOiA3MjBweCkge1xyXG4gICAgICAgICAgICAgICAgLmNvbC1tZC0yZG90NCB7XHJcbiAgICAgICAgICAgICAgICAgICAgLXdlYmtpdC1ib3gtZmxleDogMDtcclxuICAgICAgICAgICAgICAgICAgICAtbXMtZmxleDogMCAwIDIwJTtcclxuICAgICAgICAgICAgICAgICAgICBmbGV4OiAwIDAgMjAlO1xyXG4gICAgICAgICAgICAgICAgICAgIG1heC13aWR0aDogMjAlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgIFxyXG4gICAgICAgICAgICBAbWVkaWEgKG1pbi13aWR0aDogOTYwcHgpIHtcclxuICAgICAgICAgICAgICAgIC5jb2wtbGctMmRvdDQge1xyXG4gICAgICAgICAgICAgICAgICAgIC13ZWJraXQtYm94LWZsZXg6IDA7XHJcbiAgICAgICAgICAgICAgICAgICAgLW1zLWZsZXg6IDAgMCAyMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgZmxleDogMCAwIDIwJTtcclxuICAgICAgICAgICAgICAgICAgICBtYXgtd2lkdGg6IDIwJTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICBcclxuICAgICAgICAgICAgQG1lZGlhIChtaW4td2lkdGg6IDExNDBweCkge1xyXG4gICAgICAgICAgICAgICAgLmNvbC14bC0yZG90NCB7XHJcbiAgICAgICAgICAgICAgICAgICAgLXdlYmtpdC1ib3gtZmxleDogMDtcclxuICAgICAgICAgICAgICAgICAgICAtbXMtZmxleDogMCAwIDIwJTtcclxuICAgICAgICAgICAgICAgICAgICBmbGV4OiAwIDAgMjAlO1xyXG4gICAgICAgICAgICAgICAgICAgIG1heC13aWR0aDogMjAlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC5jb2wtbWQtMmRvdDQge1xyXG4gICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICBcclxuICAgICAgXHJcbiAgICAgICAgICAgICAgICAuYmx1ZS11cGxvYWQge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOiAjNTZhNGZmO1xyXG4gICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICAgICAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgXHJcbiAgICAgICAgICAgICAgICAuY2FyZCB7XHJcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogM3ZoO1xyXG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogMTkwcHg7XHJcbiAgICAgICAgICAgICAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgICBcclxuICAgICAgICAgICAgICAgICAgICAuY2FyZC1jb250ZW50IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAudXBsb2FkLWRlc2Mge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I6ICNiYmIgIWltcG9ydGFudDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgLmxvZ28tdXBsb2FkIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZDogd2hpdGVcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgLmltYWdlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAuY3VzdG9tIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gYmFja2dyb3VuZC1jb2xvcjojZWVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gYm9yZGVyOiAzcHggc29saWQgI2ViZWJlYjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gcGFkZGluZzogNDBweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuZHJhZy1kcm9wIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAxMHB4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuc2V0LW9wYWNpdHkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvcGFjaXR5OiAwLjE7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogNjBweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IGF1dG87XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICBcclxuICAgICAgICAgICAgICAgICAgICAuZm9ybS1ncm91cC51cGxvYWRlZCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5yZW1vdmUtaWNvbiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIHJpZ2h0OiAwO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIHRvcDogMDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICBjb2xvcjogcmVkO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMjJweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAubG9hZGluZyB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLnNrLWZhZGluZy1jaXJjbGUge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRvcDogNTAlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAudXBsb2FkZWQtaW1hZ2UyIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAuaW1nIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwYWRkaW5nOiAxNXB4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5pbWc+aW1nIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gbWF4LWhlaWdodDogMzIwcHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLmVkaXRvci10b29sIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgXHJcbiAgICAgICAgICAgIC5jb2wtbWQtMmRvdDQge1xyXG4gICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICBcclxuICAgICAgICAgICAgICAgIC5jYXJkIHtcclxuICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAyNTBweDtcclxuICAgICAgICAgICAgICAgICAgICBib3JkZXI6IG5vbmU7XHJcbiAgICAgICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gICAgICAgICAgICAgICAgICAgIGJveC1zaGFkb3c6IDAgM3B4IDZweCAwIHJnYmEoMCwgMCwgMCwgMC4wNSk7XHJcbiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcclxuICAgICAgXHJcbiAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgIC5jYXJkLWNvbnRlbnQge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5pbWFnZS1jYW52YXMge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDgwJTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgLnVwbG9hZC1kZXNjIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiAjYmJiICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5sb2dvLXVwbG9hZCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuaW1hZ2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmbG9hdDogbGVmdDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogYXV0bztcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5jdXN0b20ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiAjZWVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gYm9yZGVyOiAzcHggc29saWQgI2ViZWJlYjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gcGFkZGluZzogNDBweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuZHJhZy1kcm9wIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAxMHB4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuc2V0LW9wYWNpdHkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvcGFjaXR5OiAwLjE7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogNjBweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IGF1dG87XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICBcclxuICAgICAgICAgICAgICAgICAgICAuZm9ybS1ncm91cC51cGxvYWRlZCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAubG9hZGluZyB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLnNrLWZhZGluZy1jaXJjbGUge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRvcDogNTAlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAudXBsb2FkZWQtaW1hZ2UyIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAuaW1nIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5pbWc+aW1nIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAuZWRpdG9yLXRvb2wge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIFxyXG4gICAgLm5hdmJhci1tZW51IHtcclxuICAgICAgb3ZlcmZsb3cteTogc2Nyb2xsO1xyXG4gICAgICBtYXgtaGVpZ2h0OiAyNTBweDtcclxuICAgICAgbWFyZ2luOiAxNXB4IDVweDtcclxuICAgICAgLmNhdGVnb3J5LWxpc3Qge1xyXG4gICAgICAgIC5saXN0LWdyb3VwLWl0ZW0ge1xyXG4gICAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICAgICAgLmNoZWNrZWQtaWNvbiB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1mbGV4O1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgd2lkdGg6IDIwcHg7XHJcbiAgICAgICAgICAgIGhlaWdodDogMjBweDtcclxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMTVweDtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgY29sb3I6ICMzNDk4ZGI7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgICAgIG1hcmdpbi1yaWdodDogMTBweDtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIC5jaGVja2VkLWljb24uZmFsc2Uge1xyXG4gICAgICAgICAgICBjb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgXHJcbiAgICAudXBsb2FkaW5nLWZpbGUge1xyXG4gICAgICBvcGFjaXR5OiAwLjU7XHJcbiAgICAgIGZpbHRlcjogZ3JheXNjYWxlKDEpO1xyXG4gICAgfVxyXG4gIFxyXG4gICAgLnVwbG9hZGluZy1maWxlLnRydWUge1xyXG4gICAgICBvcGFjaXR5OiAxO1xyXG4gICAgICBmaWx0ZXI6IGdyYXlzY2FsZSgwKTtcclxuICAgIH1cclxuICAgIC5hdXRvU0tVIHtcclxuICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICBtYXJnaW4tbGVmdDogMTVweDtcclxuICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgfVxyXG4gIFxyXG4gICAgLmltYWdlLXVwbG9hZCB7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICBib3JkZXI6IDFweCBkYXNoZWQgI2NjYztcclxuICAgICAgbWluLWhlaWdodDogMTI1cHg7XHJcbiAgICB9XHJcbiAgICAudXBsb2FkZWQtaW1hZ2Uge1xyXG4gICAgICBtYXJnaW46IDBweCBhdXRvO1xyXG4gICAgICB3aWR0aDogYXV0bztcclxuICAgICAgcGFkZGluZzogNTBweCA7XHJcbiAgICAgIGhlaWdodDogYXV0bztcclxuICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICAudGV4dC1maWxldXBsb2Fke1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICAgICAgY29sb3I6ICMyNDgwZmI7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICAgXHJcbiAgICAgIH1cclxuICAgICAgLmltZyB7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgbWluLWhlaWdodDogMzQ4cHg7XHJcbiAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyIGNlbnRlcjtcclxuICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gIFxyXG4gICAgICAgIC5lZGl0b3ItdG9vbCB7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuMik7XHJcbiAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAgIG9wYWNpdHk6IDA7XHJcbiAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICAuaW1nOmhvdmVyIC5lZGl0b3ItdG9vbCB7XHJcbiAgICAgICAgb3BhY2l0eTogMC45O1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgXHJcbiAgICAuY2Fyb3VzZWwtY2FwdGlvbiB7XHJcbiAgICAgIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC4yKTtcclxuICAgIH1cclxuICBcclxuICAgIC5jYXJvdXNlbC1pbmRpY2F0b3JzIGxpIHtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2sgIWltcG9ydGFudDtcclxuICAgIH1cclxuICBcclxuICAgIC5wcm9kdWN0LXR5cGUtbGlzdCB7XHJcbiAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgLnByb2R1Y3QtdHlwZSB7XHJcbiAgICAgICAgZGlzcGxheTogaW5saW5lLWZsZXg7XHJcbiAgICAgICAgcGFkZGluZzogMTVweDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIGhlaWdodDogMzVweDtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA3cHg7XHJcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiA1cHg7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogNXB4O1xyXG4gICAgICAgIHRyYW5zaXRpb246IGVhc2UgMC4ycyBhbGwgMHM7XHJcbiAgICAgIH1cclxuICAgICAgLnByb2R1Y3QtdHlwZS50cnVlIHtcclxuICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcclxuICAgICAgICBib3JkZXItY29sb3I6ICMyOTgwYjk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIC5mb3JtLWNvbnRyb2wuZGVzIHtcclxuICAgICAgcGFkZGluZy10b3A6IDRweDtcclxuICAgICAgZGlzcGxheTogaW5saW5lLWZsZXg7XHJcbiAgICAgIHBhZGRpbmctYm90dG9tOiA0cHg7XHJcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICBpbnB1dCB7XHJcbiAgICAgICAgYm9yZGVyOiAwcHggc29saWQgdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgd2lkdGg6IGNhbGMoMTAwJSAtIDE1cHgpO1xyXG4gICAgICB9XHJcbiAgICAgIGlucHV0OmZvY3VzIHtcclxuICAgICAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICBkaXYuZXJyb3Ige1xyXG4gICAgICBiYWNrZ3JvdW5kOiAjZTc0YzNjO1xyXG4gICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgIHBhZGRpbmc6IDI1cHg7XHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDI1cHg7XHJcbiAgICB9XHJcbiAgICBkaXYubmV3IHtcclxuICAgICAgYmFja2dyb3VuZDogbGltZWdyZWVuO1xyXG4gICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgIHBhZGRpbmc6IDI1cHg7XHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDI1cHg7XHJcbiAgICB9XHJcbiAgICAuY2F0ZWdvcnktbGlzdCB7XHJcbiAgICAgIC5saXN0LWdyb3VwLWl0ZW0ge1xyXG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgICAuY2hlY2tlZC1pY29uIHtcclxuICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1mbGV4O1xyXG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgd2lkdGg6IDIwcHg7XHJcbiAgICAgICAgICBoZWlnaHQ6IDIwcHg7XHJcbiAgICAgICAgICBib3JkZXItcmFkaXVzOiAxNXB4O1xyXG4gICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgIGNvbG9yOiAjMzQ5OGRiO1xyXG4gICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5jaGVja2VkLWljb24uZmFsc2Uge1xyXG4gICAgICAgICAgY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmVuZC1mb290ZXIge1xyXG4gICAgICAuZGVsZXRlIHtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNlNzRjM2M7XHJcbiAgICAgICAgb3BhY2l0eTogMC41O1xyXG4gICAgICAgIGJvcmRlci1jb2xvcjogI2MwMzkyYjtcclxuICAgICAgICAuZGVsZXRlLW1vZGFsIHtcclxuICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gICAgICAgICAgcGFkZGluZzogNHB4IDEwcHg7XHJcbiAgICAgICAgICB0b3A6IDBweDtcclxuICAgICAgICAgIHJpZ2h0OiAwcHg7XHJcbiAgICAgICAgICBjb2xvcjogI2U3NGMzYztcclxuICAgICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNDVweCk7XHJcbiAgICAgICAgICAuYnRuLWdyb3VwIHtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2U3NGMzYztcclxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgICAgICAubm8ge1xyXG4gICAgICAgICAgICAgIG9wYWNpdHk6IDAuNTtcclxuICAgICAgICAgICAgICBib3JkZXItd2lkdGg6IDBweDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAueWVzIHtcclxuICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZTc0YzNjO1xyXG4gICAgICAgICAgICAgIGJvcmRlci13aWR0aDogMHB4O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC5ubzpob3ZlciB7XHJcbiAgICAgICAgICAgICAgb3BhY2l0eTogMTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICAuZGVsZXRlOmhvdmVyLFxyXG4gICAgICAuZGVsZXRlOmZvY3VzIHtcclxuICAgICAgICBvcGFjaXR5OiAxO1xyXG4gICAgICB9XHJcbiAgICAgIC5kZWxldGU6Zm9jdXMge1xyXG4gICAgICAgIC5kZWxldGUtbW9kYWwge1xyXG4gICAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIFxyXG4gIC5mb3JtLnRydWV7XHJcbiAgICBwb2ludGVyLWV2ZW50czogbm9uZTtcclxuICBcclxuICAgIC8qIGZvciBcImRpc2FibGVkXCIgZWZmZWN0ICovXHJcbiAgICBvcGFjaXR5OiAwLjU7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZWNmMGYxO1xyXG4gIH1cclxuICBcclxuICAuYnV0dG9uLWVkaXR7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBtYXJnaW4tbGVmdDogY2FsYyg1MCUgLSAxMDBweCk7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgdG9wOiAxMHB4O1xyXG4gIH1cclxuICBcclxuLnZhcmlhbi1idXR0b24ge1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG5cclxuLnZhcmlhbi1pbWFnZS1wYXJlbnQge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxufVxyXG5cclxuLnZhcmlhbi10YWJsZSB7XHJcbiAgICBtYXJnaW46IDQwcHggMDtcclxufVxyXG5cclxuLmRlbGV0ZS12YXJpYW4taW1hZ2Uge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgcmlnaHQ6IDIycHg7XHJcbiAgICB0b3A6IDIycHg7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBmb250LXNpemU6IDI0cHg7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuXHJcbi52YXJpYW4tcGFyZW50e1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG5cclxuICAgIDo6bmctZGVlcCB7IFxyXG4gICAgICAgIC5pbnB1dC1ncm91cCB7XHJcbiAgICAgICAgICAgIHdpZHRoOiA5MSU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGkge1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICByaWdodDogMjBweDtcclxuICAgICAgICB0b3A6IDEwcHg7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5hZGQtdmFsdWUge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxufVxyXG5cclxuLmZvcm0tdmFyaWFuIHtcclxuICBwYWRkaW5nLWxlZnQ6IDA7XHJcbiAgcGFkZGluZy1yaWdodDogMDtcclxufVxyXG5cclxuLnJlZC1hbGVydCB7XHJcbiAgYm9yZGVyOiBzb2xpZCAxcHggcmVkO1xyXG59XHJcblxyXG4ucmVkLWVycm9yLWFsZXJ0IHtcclxuICBjb2xvcjogcmVkO1xyXG4gIGZvbnQtc2l6ZTogMTJweDtcclxufSBcclxuXHJcbi5lcnJvci1tZXNzYWdlIHtcclxuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICBoZWlnaHQ6MTAwdmg7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGZvbnQtc2l6ZToyNnB4O1xyXG4gIHBhZGRpbmctdG9wOiA0MHB4O1xyXG5cclxuICAudGV4dC1tZXNzYWdlIHtcclxuICAgICAgYmFja2dyb3VuZDogI0U3NEMzQztcclxuICAgICAgcGFkZGluZzogMjBweDtcclxuICAgICAgbWFyZ2luOiAwIDEwcHg7XHJcbiAgfVxyXG59IiwiOmhvc3QgOjpuZy1kZWVwIC5jay1lZGl0b3JfX2VkaXRhYmxlX2lubGluZSB7XG4gIG1pbi1oZWlnaHQ6IDUwMHB4O1xufVxuXG4uYnRuLmJ0bi1kZWZhdWx0IHtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGJvcmRlcjogMXB4IHNvbGlkICMzMzM7XG59XG5cbiN0b29sdGlwICN0b29sdGlwdGV4dCB7XG4gIHZpc2liaWxpdHk6IGhpZGRlbjtcbiAgd2lkdGg6IDEyMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcbiAgY29sb3I6ICNmZmY7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgYm9yZGVyLXJhZGl1czogNnB4O1xuICBwYWRkaW5nOiA1cHggMDtcbiAgLyogUG9zaXRpb24gdGhlIHRvb2x0aXAgKi9cbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB6LWluZGV4OiAxO1xufVxuXG4jdG9vbHRpcDpob3ZlciAjdG9vbHRpcHRleHQge1xuICB2aXNpYmlsaXR5OiB2aXNpYmxlO1xufVxuXG4uYmFja19idXR0b24ge1xuICBtYXJnaW4tdG9wOiAxMnB4O1xuICBtYXJnaW4tbGVmdDogNDBweDtcbn1cblxuLmZvcm0ge1xuICBwYWRkaW5nOiAyNXB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHBhZGRpbmctdG9wOiA1MHB4O1xufVxuLmZvcm0gbGFiZWwge1xuICBjb2xvcjogIzc1NzQ3NDtcbn1cbi5mb3JtIC5jdXN0b20tY29udGVudDo6LXdlYmtpdC1maWxlLXVwbG9hZC1idXR0b24ge1xuICB2aXNpYmlsaXR5OiBoaWRkZW47XG59XG4uZm9ybSAuY3VzdG9tLWNvbnRlbnQ6OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiQ2hvb3NlIEZpbGVcIjtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgYm9yZGVyOiAxcHggc29saWQgIzk5OTtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBwYWRkaW5nOiA4cHg7XG4gIG91dGxpbmU6IG5vbmU7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIC13ZWJraXQtdXNlci1zZWxlY3Q6IG5vbmU7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgZm9udC1zaXplOiAxMHB0O1xuICBjb2xvcjogYmxhY2s7XG4gIGxldHRlci1zcGFjaW5nOiAwLjVweDtcbn1cbi5mb3JtIC5pbWFnZXBpY3Q6OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiQ2hvb3NlIEltYWdlXCI7XG59XG4uZm9ybSAuaGFzaGhhc2ggLmhhc2h0YWctaXRlbSB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG4gIGJhY2tncm91bmQ6ICNlZWU7XG4gIHBhZGRpbmc6IDJweCA1cHg7XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xufVxuLmZvcm0gLmhhc2hoYXNoIC5oYXNodGFnLWl0ZW06OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiI1wiO1xuICBkaXNwbGF5OiBpbmxpbmU7XG59XG4uZm9ybSAuaGFzaGhhc2ggLmhhc2h0YWdza2V5LWlucHV0IHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBtaW4td2lkdGg6IDc1cHg7XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbiAgZm9udC1zaXplOiAxcmVtO1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBvdXRsaW5lOiBub25lO1xufVxuLmZvcm0gLmhhc2hoYXNoIC5oYXNodGFnc2tleS1pbnB1dDo6YmVmb3JlIHtcbiAgY29udGVudDogXCIjXCI7XG4gIGRpc3BsYXk6IGlubGluZTtcbn1cbi5mb3JtIC5jYXJkIHtcbiAgYm94LXNoYWRvdzogMCAxcHggMTFweCAwIHJnYmEoMCwgMCwgMCwgMC4yKSwgMCAycHggM3B4IDAgcmdiYSgwLCAwLCAwLCAwLjE5KTtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbn1cbi5mb3JtIC5jYXJkIC5jYXJkLWhlYWRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICBtYXJnaW46IDBweDtcbiAgcGFkZGluZzogMHB4O1xuICBib3JkZXItbGVmdDogM3B4IHNvbGlkICMyNDgwZmI7XG59XG4uZm9ybSAuY2FyZCAuY2FyZC1oZWFkZXIgbGFiZWwge1xuICBtYXJnaW46IDIwcHggMTBweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZvbnQtc2l6ZTogMjBweDtcbn1cbi5mb3JtIC5jYXJkIC5pbWFnZS11cGxvYWQge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgYm9yZGVyOiAzcHggc29saWQgI2ViZWJlYjtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y1ZjVmNTtcbiAgbWluLWhlaWdodDogMTI1cHg7XG4gIG1hcmdpbjogMjBweDtcbiAgcGFkZGluZzogNTBweDtcbn1cbi5mb3JtIC5jYXJkIC5pbWFnZS11cGxvYWQgLmxvZ28tdXBsb2FkIHtcbiAgd2lkdGg6IDE1MHB4O1xufVxuLmZvcm0gLmNhcmQgLmltYWdlLXVwbG9hZCAuZmlsZXVwbG9hZCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMyNDgwZmI7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xuICBmb250LXNpemU6IDE0cHg7XG4gIHBhZGRpbmc6IDEycHggMjJweDtcbn1cbi5mb3JtIC5jYXJkIC5pbWFnZS11cGxvYWQgLnVwbG9hZC1kZXNjIHtcbiAgY29sb3I6ICNkNmQ2ZDYgIWltcG9ydGFudDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDE4cHg7XG59XG4uZm9ybSAuY2FyZCAudXBsb2FkZWQge1xuICBib3JkZXItdG9wOiAxcHggc29saWQgI2NjYztcbn1cbi5mb3JtIC51cGxvYWQtY29udGFpbmVyIHtcbiAgcGFkZGluZzogMjBweDtcbiAgYm9yZGVyLXJhZGl1czogMThweDtcbiAgYm94LXNoYWRvdzogMCAzcHggNnB4IDAgcmdiYSgwLCAwLCAwLCAwLjA2KTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbn1cbi5mb3JtIC5tdWx0aXBsZS1pbWFnZS11cGxvYWQgLmNvbC1tZC04IC5mb3JtLWdyb3VwIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcbn1cbi5mb3JtIC5tdWx0aXBsZS1pbWFnZS11cGxvYWQgLmNvbC1tZC04IC5mb3JtLWdyb3VwIC5sYWJlbCB7XG4gIHdpZHRoOiAzOCU7XG59XG4uZm9ybSAubXVsdGlwbGUtaW1hZ2UtdXBsb2FkIC5jb2wtbWQtNiAuZm9ybS1ncm91cCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XG59XG4uZm9ybSAubXVsdGlwbGUtaW1hZ2UtdXBsb2FkIC5jb2wtbWQtNiAuZm9ybS1ncm91cCAubGFiZWwge1xuICB3aWR0aDogNjAlO1xufVxuLmZvcm0gLm11bHRpcGxlLWltYWdlLXVwbG9hZCAuY29sLW1kLTYgLmZvcm0tZ3JvdXAgc2VsZWN0LmZvcm0tY29udHJvbDpub3QoW3NpemVdKTpub3QoW211bHRpcGxlXSkge1xuICBoZWlnaHQ6IHVuc2V0O1xufVxuLmZvcm0gLm11bHRpcGxlLWltYWdlLXVwbG9hZCAuc2VjdGlvbi11cGxvYWQge1xuICB3aWR0aDogMTAwJTtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5mb3JtIC5tdWx0aXBsZS1pbWFnZS11cGxvYWQgLnNlY3Rpb24tdXBsb2FkIC5jb2wtMmRvdDQsXG4uZm9ybSAubXVsdGlwbGUtaW1hZ2UtdXBsb2FkIC5zZWN0aW9uLXVwbG9hZCAuY29sLXNtLTJkb3Q0LFxuLmZvcm0gLm11bHRpcGxlLWltYWdlLXVwbG9hZCAuc2VjdGlvbi11cGxvYWQgLmNvbC1tZC0yZG90NCxcbi5mb3JtIC5tdWx0aXBsZS1pbWFnZS11cGxvYWQgLnNlY3Rpb24tdXBsb2FkIC5jb2wtbGctMmRvdDQsXG4uZm9ybSAubXVsdGlwbGUtaW1hZ2UtdXBsb2FkIC5zZWN0aW9uLXVwbG9hZCAuY29sLXhsLTJkb3Q0IHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB3aWR0aDogMTAwJTtcbiAgbWluLWhlaWdodDogMXB4O1xuICBwYWRkaW5nLXJpZ2h0OiAxNXB4O1xuICBwYWRkaW5nLWxlZnQ6IDE1cHg7XG59XG4uZm9ybSAubXVsdGlwbGUtaW1hZ2UtdXBsb2FkIC5zZWN0aW9uLXVwbG9hZCAuY29sLTJkb3Q0IHtcbiAgLXdlYmtpdC1ib3gtZmxleDogMDtcbiAgLW1zLWZsZXg6IDAgMCAyMCU7XG4gIGZsZXg6IDAgMCAyMCU7XG4gIG1heC13aWR0aDogMjAlO1xufVxuQG1lZGlhIChtaW4td2lkdGg6IDU0MHB4KSB7XG4gIC5mb3JtIC5tdWx0aXBsZS1pbWFnZS11cGxvYWQgLnNlY3Rpb24tdXBsb2FkIC5jb2wtc20tMmRvdDQge1xuICAgIC13ZWJraXQtYm94LWZsZXg6IDA7XG4gICAgLW1zLWZsZXg6IDAgMCAyMCU7XG4gICAgZmxleDogMCAwIDIwJTtcbiAgICBtYXgtd2lkdGg6IDIwJTtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDcyMHB4KSB7XG4gIC5mb3JtIC5tdWx0aXBsZS1pbWFnZS11cGxvYWQgLnNlY3Rpb24tdXBsb2FkIC5jb2wtbWQtMmRvdDQge1xuICAgIC13ZWJraXQtYm94LWZsZXg6IDA7XG4gICAgLW1zLWZsZXg6IDAgMCAyMCU7XG4gICAgZmxleDogMCAwIDIwJTtcbiAgICBtYXgtd2lkdGg6IDIwJTtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDk2MHB4KSB7XG4gIC5mb3JtIC5tdWx0aXBsZS1pbWFnZS11cGxvYWQgLnNlY3Rpb24tdXBsb2FkIC5jb2wtbGctMmRvdDQge1xuICAgIC13ZWJraXQtYm94LWZsZXg6IDA7XG4gICAgLW1zLWZsZXg6IDAgMCAyMCU7XG4gICAgZmxleDogMCAwIDIwJTtcbiAgICBtYXgtd2lkdGg6IDIwJTtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDExNDBweCkge1xuICAuZm9ybSAubXVsdGlwbGUtaW1hZ2UtdXBsb2FkIC5zZWN0aW9uLXVwbG9hZCAuY29sLXhsLTJkb3Q0IHtcbiAgICAtd2Via2l0LWJveC1mbGV4OiAwO1xuICAgIC1tcy1mbGV4OiAwIDAgMjAlO1xuICAgIGZsZXg6IDAgMCAyMCU7XG4gICAgbWF4LXdpZHRoOiAyMCU7XG4gIH1cbn1cbi5mb3JtIC5tdWx0aXBsZS1pbWFnZS11cGxvYWQgLnNlY3Rpb24tdXBsb2FkIC5jb2wtbWQtMmRvdDQge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbi5mb3JtIC5tdWx0aXBsZS1pbWFnZS11cGxvYWQgLnNlY3Rpb24tdXBsb2FkIC5jb2wtbWQtMmRvdDQgLmJsdWUtdXBsb2FkIHtcbiAgY29sb3I6ICM1NmE0ZmY7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uZm9ybSAubXVsdGlwbGUtaW1hZ2UtdXBsb2FkIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTJkb3Q0IC5jYXJkIHtcbiAgbWFyZ2luLXRvcDogM3ZoO1xuICBoZWlnaHQ6IDE5MHB4O1xuICBib3JkZXI6IG5vbmU7XG59XG4uZm9ybSAubXVsdGlwbGUtaW1hZ2UtdXBsb2FkIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTJkb3Q0IC5jYXJkIC5jYXJkLWNvbnRlbnQge1xuICBoZWlnaHQ6IDEwMCU7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5mb3JtIC5tdWx0aXBsZS1pbWFnZS11cGxvYWQgLnNlY3Rpb24tdXBsb2FkIC5jb2wtbWQtMmRvdDQgLmNhcmQgLmNhcmQtY29udGVudCAudXBsb2FkLWRlc2Mge1xuICBjb2xvcjogI2JiYiAhaW1wb3J0YW50O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbi5mb3JtIC5tdWx0aXBsZS1pbWFnZS11cGxvYWQgLnNlY3Rpb24tdXBsb2FkIC5jb2wtbWQtMmRvdDQgLmNhcmQgLmNhcmQtY29udGVudCAubG9nby11cGxvYWQge1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG59XG4uZm9ybSAubXVsdGlwbGUtaW1hZ2UtdXBsb2FkIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTJkb3Q0IC5jYXJkIC5jYXJkLWNvbnRlbnQgLmltYWdlIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZsb2F0OiBsZWZ0O1xuICBoZWlnaHQ6IGF1dG87XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG59XG4uZm9ybSAubXVsdGlwbGUtaW1hZ2UtdXBsb2FkIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTJkb3Q0IC5jYXJkIC5jYXJkLWNvbnRlbnQgLmltYWdlIC5jdXN0b20ge1xuICBoZWlnaHQ6IDEwMCU7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xufVxuLmZvcm0gLm11bHRpcGxlLWltYWdlLXVwbG9hZCAuc2VjdGlvbi11cGxvYWQgLmNvbC1tZC0yZG90NCAuY2FyZCAuY2FyZC1jb250ZW50IC5pbWFnZSAuY3VzdG9tIC5kcmFnLWRyb3Age1xuICBmb250LXNpemU6IDEwcHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5mb3JtIC5tdWx0aXBsZS1pbWFnZS11cGxvYWQgLnNlY3Rpb24tdXBsb2FkIC5jb2wtbWQtMmRvdDQgLmNhcmQgLmNhcmQtY29udGVudCAuaW1hZ2UgLmN1c3RvbSAuc2V0LW9wYWNpdHkge1xuICBvcGFjaXR5OiAwLjE7XG4gIGhlaWdodDogNjBweDtcbiAgd2lkdGg6IGF1dG87XG59XG4uZm9ybSAubXVsdGlwbGUtaW1hZ2UtdXBsb2FkIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTJkb3Q0IC5jYXJkIC5mb3JtLWdyb3VwLnVwbG9hZGVkIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIGhlaWdodDogMTAwJTtcbn1cbi5mb3JtIC5tdWx0aXBsZS1pbWFnZS11cGxvYWQgLnNlY3Rpb24tdXBsb2FkIC5jb2wtbWQtMmRvdDQgLmNhcmQgLmZvcm0tZ3JvdXAudXBsb2FkZWQgLnJlbW92ZS1pY29uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogMDtcbiAgdG9wOiAwO1xuICBjb2xvcjogcmVkO1xuICBmb250LXNpemU6IDIycHg7XG59XG4uZm9ybSAubXVsdGlwbGUtaW1hZ2UtdXBsb2FkIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTJkb3Q0IC5jYXJkIC5mb3JtLWdyb3VwLnVwbG9hZGVkIC5sb2FkaW5nIHtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuLmZvcm0gLm11bHRpcGxlLWltYWdlLXVwbG9hZCAuc2VjdGlvbi11cGxvYWQgLmNvbC1tZC0yZG90NCAuY2FyZCAuZm9ybS1ncm91cC51cGxvYWRlZCAubG9hZGluZyAuc2stZmFkaW5nLWNpcmNsZSB7XG4gIHRvcDogNTAlO1xufVxuLmZvcm0gLm11bHRpcGxlLWltYWdlLXVwbG9hZCAuc2VjdGlvbi11cGxvYWQgLmNvbC1tZC0yZG90NCAuY2FyZCAuZm9ybS1ncm91cC51cGxvYWRlZCAudXBsb2FkZWQtaW1hZ2UyIHtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuLmZvcm0gLm11bHRpcGxlLWltYWdlLXVwbG9hZCAuc2VjdGlvbi11cGxvYWQgLmNvbC1tZC0yZG90NCAuY2FyZCAuZm9ybS1ncm91cC51cGxvYWRlZCAudXBsb2FkZWQtaW1hZ2UyIC5pbWcge1xuICBwYWRkaW5nOiAxNXB4O1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cbi5mb3JtIC5tdWx0aXBsZS1pbWFnZS11cGxvYWQgLnNlY3Rpb24tdXBsb2FkIC5jb2wtbWQtMmRvdDQgLmNhcmQgLmZvcm0tZ3JvdXAudXBsb2FkZWQgLnVwbG9hZGVkLWltYWdlMiAuaW1nID4gaW1nIHtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuLmZvcm0gLm11bHRpcGxlLWltYWdlLXVwbG9hZCAuc2VjdGlvbi11cGxvYWQgLmNvbC1tZC0yZG90NCAuY2FyZCAuZm9ybS1ncm91cC51cGxvYWRlZCAudXBsb2FkZWQtaW1hZ2UyIC5lZGl0b3ItdG9vbCB7XG4gIGhlaWdodDogMTAwJTtcbn1cbi5mb3JtIC5tdWx0aXBsZS1pbWFnZS11cGxvYWQgLnNlY3Rpb24tdXBsb2FkIC5jb2wtbWQtMmRvdDQge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uZm9ybSAubXVsdGlwbGUtaW1hZ2UtdXBsb2FkIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTJkb3Q0IC5jYXJkIHtcbiAgaGVpZ2h0OiAyNTBweDtcbiAgYm9yZGVyOiBub25lO1xuICBib3JkZXItcmFkaXVzOiA4cHg7XG4gIGJveC1zaGFkb3c6IDAgM3B4IDZweCAwIHJnYmEoMCwgMCwgMCwgMC4wNSk7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG59XG4uZm9ybSAubXVsdGlwbGUtaW1hZ2UtdXBsb2FkIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTJkb3Q0IC5jYXJkIC5jYXJkLWNvbnRlbnQge1xuICBoZWlnaHQ6IDEwMCU7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5mb3JtIC5tdWx0aXBsZS1pbWFnZS11cGxvYWQgLnNlY3Rpb24tdXBsb2FkIC5jb2wtbWQtMmRvdDQgLmNhcmQgLmNhcmQtY29udGVudCAuaW1hZ2UtY2FudmFzIHtcbiAgd2lkdGg6IDgwJTtcbn1cbi5mb3JtIC5tdWx0aXBsZS1pbWFnZS11cGxvYWQgLnNlY3Rpb24tdXBsb2FkIC5jb2wtbWQtMmRvdDQgLmNhcmQgLmNhcmQtY29udGVudCAudXBsb2FkLWRlc2Mge1xuICBjb2xvcjogI2JiYiAhaW1wb3J0YW50O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbi5mb3JtIC5tdWx0aXBsZS1pbWFnZS11cGxvYWQgLnNlY3Rpb24tdXBsb2FkIC5jb2wtbWQtMmRvdDQgLmNhcmQgLmNhcmQtY29udGVudCAubG9nby11cGxvYWQge1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG59XG4uZm9ybSAubXVsdGlwbGUtaW1hZ2UtdXBsb2FkIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTJkb3Q0IC5jYXJkIC5jYXJkLWNvbnRlbnQgLmltYWdlIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZsb2F0OiBsZWZ0O1xuICBoZWlnaHQ6IGF1dG87XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG59XG4uZm9ybSAubXVsdGlwbGUtaW1hZ2UtdXBsb2FkIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTJkb3Q0IC5jYXJkIC5jYXJkLWNvbnRlbnQgLmltYWdlIC5jdXN0b20ge1xuICBoZWlnaHQ6IDEwMCU7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xufVxuLmZvcm0gLm11bHRpcGxlLWltYWdlLXVwbG9hZCAuc2VjdGlvbi11cGxvYWQgLmNvbC1tZC0yZG90NCAuY2FyZCAuY2FyZC1jb250ZW50IC5pbWFnZSAuY3VzdG9tIC5kcmFnLWRyb3Age1xuICBmb250LXNpemU6IDEwcHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5mb3JtIC5tdWx0aXBsZS1pbWFnZS11cGxvYWQgLnNlY3Rpb24tdXBsb2FkIC5jb2wtbWQtMmRvdDQgLmNhcmQgLmNhcmQtY29udGVudCAuaW1hZ2UgLmN1c3RvbSAuc2V0LW9wYWNpdHkge1xuICBvcGFjaXR5OiAwLjE7XG4gIGhlaWdodDogNjBweDtcbiAgd2lkdGg6IGF1dG87XG59XG4uZm9ybSAubXVsdGlwbGUtaW1hZ2UtdXBsb2FkIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTJkb3Q0IC5jYXJkIC5mb3JtLWdyb3VwLnVwbG9hZGVkIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIGhlaWdodDogMTAwJTtcbn1cbi5mb3JtIC5tdWx0aXBsZS1pbWFnZS11cGxvYWQgLnNlY3Rpb24tdXBsb2FkIC5jb2wtbWQtMmRvdDQgLmNhcmQgLmZvcm0tZ3JvdXAudXBsb2FkZWQgLmxvYWRpbmcge1xuICBoZWlnaHQ6IDEwMCU7XG59XG4uZm9ybSAubXVsdGlwbGUtaW1hZ2UtdXBsb2FkIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTJkb3Q0IC5jYXJkIC5mb3JtLWdyb3VwLnVwbG9hZGVkIC5sb2FkaW5nIC5zay1mYWRpbmctY2lyY2xlIHtcbiAgdG9wOiA1MCU7XG59XG4uZm9ybSAubXVsdGlwbGUtaW1hZ2UtdXBsb2FkIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTJkb3Q0IC5jYXJkIC5mb3JtLWdyb3VwLnVwbG9hZGVkIC51cGxvYWRlZC1pbWFnZTIge1xuICBoZWlnaHQ6IDEwMCU7XG59XG4uZm9ybSAubXVsdGlwbGUtaW1hZ2UtdXBsb2FkIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTJkb3Q0IC5jYXJkIC5mb3JtLWdyb3VwLnVwbG9hZGVkIC51cGxvYWRlZC1pbWFnZTIgLmltZyB7XG4gIGhlaWdodDogMTAwJTtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4uZm9ybSAubXVsdGlwbGUtaW1hZ2UtdXBsb2FkIC5zZWN0aW9uLXVwbG9hZCAuY29sLW1kLTJkb3Q0IC5jYXJkIC5mb3JtLWdyb3VwLnVwbG9hZGVkIC51cGxvYWRlZC1pbWFnZTIgLmltZyA+IGltZyB7XG4gIGhlaWdodDogMTAwJTtcbn1cbi5mb3JtIC5tdWx0aXBsZS1pbWFnZS11cGxvYWQgLnNlY3Rpb24tdXBsb2FkIC5jb2wtbWQtMmRvdDQgLmNhcmQgLmZvcm0tZ3JvdXAudXBsb2FkZWQgLnVwbG9hZGVkLWltYWdlMiAuZWRpdG9yLXRvb2wge1xuICBoZWlnaHQ6IDEwMCU7XG59XG4uZm9ybSAubmF2YmFyLW1lbnUge1xuICBvdmVyZmxvdy15OiBzY3JvbGw7XG4gIG1heC1oZWlnaHQ6IDI1MHB4O1xuICBtYXJnaW46IDE1cHggNXB4O1xufVxuLmZvcm0gLm5hdmJhci1tZW51IC5jYXRlZ29yeS1saXN0IC5saXN0LWdyb3VwLWl0ZW0ge1xuICBib3JkZXI6IG5vbmU7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbi5mb3JtIC5uYXZiYXItbWVudSAuY2F0ZWdvcnktbGlzdCAubGlzdC1ncm91cC1pdGVtIC5jaGVja2VkLWljb24ge1xuICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHdpZHRoOiAyMHB4O1xuICBoZWlnaHQ6IDIwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIGNvbG9yOiAjMzQ5OGRiO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG4uZm9ybSAubmF2YmFyLW1lbnUgLmNhdGVnb3J5LWxpc3QgLmxpc3QtZ3JvdXAtaXRlbSAuY2hlY2tlZC1pY29uLmZhbHNlIHtcbiAgY29sb3I6IHRyYW5zcGFyZW50O1xufVxuLmZvcm0gLnVwbG9hZGluZy1maWxlIHtcbiAgb3BhY2l0eTogMC41O1xuICBmaWx0ZXI6IGdyYXlzY2FsZSgxKTtcbn1cbi5mb3JtIC51cGxvYWRpbmctZmlsZS50cnVlIHtcbiAgb3BhY2l0eTogMTtcbiAgZmlsdGVyOiBncmF5c2NhbGUoMCk7XG59XG4uZm9ybSAuYXV0b1NLVSB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgbWFyZ2luLWxlZnQ6IDE1cHg7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbn1cbi5mb3JtIC5pbWFnZS11cGxvYWQge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgYm9yZGVyOiAxcHggZGFzaGVkICNjY2M7XG4gIG1pbi1oZWlnaHQ6IDEyNXB4O1xufVxuLmZvcm0gLnVwbG9hZGVkLWltYWdlIHtcbiAgbWFyZ2luOiAwcHggYXV0bztcbiAgd2lkdGg6IGF1dG87XG4gIHBhZGRpbmc6IDUwcHg7XG4gIGhlaWdodDogYXV0bztcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuLmZvcm0gLnVwbG9hZGVkLWltYWdlIC50ZXh0LWZpbGV1cGxvYWQge1xuICBkaXNwbGF5OiBibG9jaztcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIGNvbG9yOiAjMjQ4MGZiO1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuLmZvcm0gLnVwbG9hZGVkLWltYWdlIC5pbWcge1xuICB3aWR0aDogMTAwJTtcbiAgbWluLWhlaWdodDogMzQ4cHg7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyIGNlbnRlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbn1cbi5mb3JtIC51cGxvYWRlZC1pbWFnZSAuaW1nIC5lZGl0b3ItdG9vbCB7XG4gIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC4yKTtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBvcGFjaXR5OiAwO1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5mb3JtIC51cGxvYWRlZC1pbWFnZSAuaW1nOmhvdmVyIC5lZGl0b3ItdG9vbCB7XG4gIG9wYWNpdHk6IDAuOTtcbn1cbi5mb3JtIC5jYXJvdXNlbC1jYXB0aW9uIHtcbiAgYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwLjIpO1xufVxuLmZvcm0gLmNhcm91c2VsLWluZGljYXRvcnMgbGkge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjayAhaW1wb3J0YW50O1xufVxuLmZvcm0gLnByb2R1Y3QtdHlwZS1saXN0IHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xufVxuLmZvcm0gLnByb2R1Y3QtdHlwZS1saXN0IC5wcm9kdWN0LXR5cGUge1xuICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcbiAgcGFkZGluZzogMTVweDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGhlaWdodDogMzVweDtcbiAgYm9yZGVyLXJhZGl1czogN3B4O1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xuICBtYXJnaW4tYm90dG9tOiA1cHg7XG4gIHRyYW5zaXRpb246IGVhc2UgMC4ycyBhbGwgMHM7XG59XG4uZm9ybSAucHJvZHVjdC10eXBlLWxpc3QgLnByb2R1Y3QtdHlwZS50cnVlIHtcbiAgY29sb3I6IHdoaXRlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5OGRiO1xuICBib3JkZXItY29sb3I6ICMyOTgwYjk7XG59XG4uZm9ybSAuZm9ybS1jb250cm9sLmRlcyB7XG4gIHBhZGRpbmctdG9wOiA0cHg7XG4gIGRpc3BsYXk6IGlubGluZS1mbGV4O1xuICBwYWRkaW5nLWJvdHRvbTogNHB4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5mb3JtIC5mb3JtLWNvbnRyb2wuZGVzIGlucHV0IHtcbiAgYm9yZGVyOiAwcHggc29saWQgdHJhbnNwYXJlbnQ7XG4gIHdpZHRoOiBjYWxjKDEwMCUgLSAxNXB4KTtcbn1cbi5mb3JtIC5mb3JtLWNvbnRyb2wuZGVzIGlucHV0OmZvY3VzIHtcbiAgb3V0bGluZTogbm9uZTtcbn1cbi5mb3JtIGRpdi5lcnJvciB7XG4gIGJhY2tncm91bmQ6ICNlNzRjM2M7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgcGFkZGluZzogMjVweDtcbiAgbWFyZ2luLWJvdHRvbTogMjVweDtcbn1cbi5mb3JtIGRpdi5uZXcge1xuICBiYWNrZ3JvdW5kOiBsaW1lZ3JlZW47XG4gIGNvbG9yOiB3aGl0ZTtcbiAgcGFkZGluZzogMjVweDtcbiAgbWFyZ2luLWJvdHRvbTogMjVweDtcbn1cbi5mb3JtIC5jYXRlZ29yeS1saXN0IC5saXN0LWdyb3VwLWl0ZW0ge1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG4uZm9ybSAuY2F0ZWdvcnktbGlzdCAubGlzdC1ncm91cC1pdGVtIC5jaGVja2VkLWljb24ge1xuICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHdpZHRoOiAyMHB4O1xuICBoZWlnaHQ6IDIwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIGNvbG9yOiAjMzQ5OGRiO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuLmZvcm0gLmNhdGVnb3J5LWxpc3QgLmxpc3QtZ3JvdXAtaXRlbSAuY2hlY2tlZC1pY29uLmZhbHNlIHtcbiAgY29sb3I6IHRyYW5zcGFyZW50O1xufVxuLmZvcm0gLmVuZC1mb290ZXIgLmRlbGV0ZSB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTc0YzNjO1xuICBvcGFjaXR5OiAwLjU7XG4gIGJvcmRlci1jb2xvcjogI2MwMzkyYjtcbn1cbi5mb3JtIC5lbmQtZm9vdGVyIC5kZWxldGUgLmRlbGV0ZS1tb2RhbCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgZGlzcGxheTogbm9uZTtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIHBhZGRpbmc6IDRweCAxMHB4O1xuICB0b3A6IDBweDtcbiAgcmlnaHQ6IDBweDtcbiAgY29sb3I6ICNlNzRjM2M7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNDVweCk7XG59XG4uZm9ybSAuZW5kLWZvb3RlciAuZGVsZXRlIC5kZWxldGUtbW9kYWwgLmJ0bi1ncm91cCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNlNzRjM2M7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cbi5mb3JtIC5lbmQtZm9vdGVyIC5kZWxldGUgLmRlbGV0ZS1tb2RhbCAuYnRuLWdyb3VwIC5ubyB7XG4gIG9wYWNpdHk6IDAuNTtcbiAgYm9yZGVyLXdpZHRoOiAwcHg7XG59XG4uZm9ybSAuZW5kLWZvb3RlciAuZGVsZXRlIC5kZWxldGUtbW9kYWwgLmJ0bi1ncm91cCAueWVzIHtcbiAgYmFja2dyb3VuZDogI2U3NGMzYztcbiAgYm9yZGVyLXdpZHRoOiAwcHg7XG59XG4uZm9ybSAuZW5kLWZvb3RlciAuZGVsZXRlIC5kZWxldGUtbW9kYWwgLmJ0bi1ncm91cCAubm86aG92ZXIge1xuICBvcGFjaXR5OiAxO1xufVxuLmZvcm0gLmVuZC1mb290ZXIgLmRlbGV0ZTpob3Zlcixcbi5mb3JtIC5lbmQtZm9vdGVyIC5kZWxldGU6Zm9jdXMge1xuICBvcGFjaXR5OiAxO1xufVxuLmZvcm0gLmVuZC1mb290ZXIgLmRlbGV0ZTpmb2N1cyAuZGVsZXRlLW1vZGFsIHtcbiAgZGlzcGxheTogYmxvY2s7XG59XG5cbi5mb3JtLnRydWUge1xuICBwb2ludGVyLWV2ZW50czogbm9uZTtcbiAgLyogZm9yIFwiZGlzYWJsZWRcIiBlZmZlY3QgKi9cbiAgb3BhY2l0eTogMC41O1xuICBiYWNrZ3JvdW5kOiAjZWNmMGYxO1xufVxuXG4uYnV0dG9uLWVkaXQge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG1hcmdpbi1sZWZ0OiBjYWxjKDUwJSAtIDEwMHB4KTtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThkYjtcbiAgY29sb3I6IHdoaXRlO1xuICB0b3A6IDEwcHg7XG59XG5cbi52YXJpYW4tYnV0dG9uIHtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG4udmFyaWFuLWltYWdlLXBhcmVudCB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLnZhcmlhbi10YWJsZSB7XG4gIG1hcmdpbjogNDBweCAwO1xufVxuXG4uZGVsZXRlLXZhcmlhbi1pbWFnZSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgcmlnaHQ6IDIycHg7XG4gIHRvcDogMjJweDtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDI0cHg7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuLnZhcmlhbi1wYXJlbnQge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4udmFyaWFuLXBhcmVudCA6Om5nLWRlZXAgLmlucHV0LWdyb3VwIHtcbiAgd2lkdGg6IDkxJTtcbn1cbi52YXJpYW4tcGFyZW50IGkge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAyMHB4O1xuICB0b3A6IDEwcHg7XG59XG5cbi5hZGQtdmFsdWUge1xuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xufVxuXG4uZm9ybS12YXJpYW4ge1xuICBwYWRkaW5nLWxlZnQ6IDA7XG4gIHBhZGRpbmctcmlnaHQ6IDA7XG59XG5cbi5yZWQtYWxlcnQge1xuICBib3JkZXI6IHNvbGlkIDFweCByZWQ7XG59XG5cbi5yZWQtZXJyb3ItYWxlcnQge1xuICBjb2xvcjogcmVkO1xuICBmb250LXNpemU6IDEycHg7XG59XG5cbi5lcnJvci1tZXNzYWdlIHtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGhlaWdodDogMTAwdmg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAyNnB4O1xuICBwYWRkaW5nLXRvcDogNDBweDtcbn1cbi5lcnJvci1tZXNzYWdlIC50ZXh0LW1lc3NhZ2Uge1xuICBiYWNrZ3JvdW5kOiAjRTc0QzNDO1xuICBwYWRkaW5nOiAyMHB4O1xuICBtYXJnaW46IDAgMTBweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/merchant-portal/separated-modules/merchant-products/products/merchant-edit-product/merchant-edit-product.component.ts":
/*!**********************************************************************************************************************************************!*\
  !*** ./src/app/layout/merchant-portal/separated-modules/merchant-products/products/merchant-edit-product/merchant-edit-product.component.ts ***!
  \**********************************************************************************************************************************************/
/*! exports provided: MerchantEditProductComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MerchantEditProductComponent", function() { return MerchantEditProductComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_product_product_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../../services/product/product.service */ "./src/app/services/product/product.service.ts");
/* harmony import */ var ngx_bootstrap_chronos__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-bootstrap/chronos */ "./node_modules/ngx-bootstrap/chronos/fesm5/ngx-bootstrap-chronos.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ckeditor/ckeditor5-build-classic */ "./node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js");
/* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _services_category_category_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../../services/category/category.service */ "./src/app/services/category/category.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_observerable_permission_observer__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../../services/observerable/permission-observer */ "./src/app/services/observerable/permission-observer.ts");
/* harmony import */ var _services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../../../services/e-voucher/e-voucher.service */ "./src/app/services/e-voucher/e-voucher.service.ts");
/* harmony import */ var _services_merchant_merchant_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../../../services/merchant/merchant.service */ "./src/app/services/merchant/merchant.service.ts");
/* harmony import */ var _object_interface_common_object__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../../../object-interface/common.object */ "./src/app/object-interface/common.object.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
// Dear Future Developer, please consider leaving notes on your code, good habits will reduce development time ~ H
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};












var MerchantEditProductComponent = /** @class */ (function () {
    function MerchantEditProductComponent(productService, categoryService, router, route, permission, eVoucherService, merchantService, _location) {
        var _this = this;
        this.productService = productService;
        this.categoryService = categoryService;
        this.router = router;
        this.route = route;
        this.permission = permission;
        this.eVoucherService = eVoucherService;
        this.merchantService = merchantService;
        this._location = _location;
        // images = [1, 2, 3].map(() => `https://picsum.photos/900/500?random&t=${Math.random()}`);
        this.genVoucher = {
            expiry_date: null,
            qty: 1
        };
        this.toggleDelete = false;
        this.errorFile = false;
        this.generateVoucherToggler = 0;
        // edit_status = true
        this.listOfProductType = [{ name: 'Product', active: true }, { name: 'evoucher', active: false }];
        this.listOfRedeemType = [{ name: 'Voucher Link', active: false }, { name: 'Voucher Code', active: false }];
        this.listOfVendorType = [];
        this.listOfMerchant = [];
        this.Editor = _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_4___default.a;
        this.config = {
            toolbar: ['heading', '|', 'bold', 'italic', 'bulletedList', 'numberedList', 'blockQuote'],
            heading: {
                options: [
                    { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
                    { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
                    { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' }
                ]
            }
        };
        this.autoSKU = false;
        this.hashtagsKey = '';
        this.form = {
            type: 'product',
            min_order: 1,
            product_code: '',
            product_name: '',
            product_price: 0,
            product_value: 0,
            variation: {},
            category: 'public',
            status: '',
            price: 0,
            fixed_price: 0,
            qty: 0,
            description: '',
            tnc: '',
            location_for_redeem: '',
            dimensions: {
                width: 0,
                length: 0,
                height: 0
            },
            product_sku: [],
            weight: 1,
            images_gallery: [],
            // weightInGram: 1000,
            pic_file_name: '',
            need_outlet_code: 0,
            applicable_for_ios: false,
        };
        this.uploads = {
            image1: '',
            image2: '',
            image3: '',
            image4: '',
            image5: '',
        };
        this.gallery_label = ['image3', 'image1', 'image2', 'image4', 'image5'];
        this.labels = {
            type: 'product type',
            min_order: 'minimum order',
            product_code: 'product code',
            product_name: 'product name',
            category: 'category',
            price: 'price',
            fixed_price: 'public price',
            qty: 'quantity',
            description: 'description',
            tnc: 'term and conditions',
            dimensions: 'dimensions',
            weight: 'weight'
        };
        this.editype = '';
        this.errorLabel = false;
        this.errorMessage = false;
        this.isFileUploaded = false;
        this.multipleCategories = [];
        this.temp_gallery = [];
        this.showLoading = false;
        this.showLoading2 = false;
        this.showLoading3 = false;
        this.showLoading4 = false;
        this.showLoading5 = false;
        this.productStatus = [
            { label: 'ACTIVE', value: 'ACTIVE' },
            { label: 'INACTIVE', value: 'INACTIVE' }
        ];
        this.varianHeader = [];
        this.dataVarian = [];
        this.dataVarianModel = [];
        this.headerValue = [
            'SKU Code',
            'SKU Price',
            'SKU Value',
            'Stok',
            'Gambar',
            'Status',
        ];
        this.valueVariation = [];
        this.productSku = [];
        this.isLoading = [];
        this.title = '';
        this.photoAlert = false;
        this.productNameAlert = false;
        this.productCodeAlert = false;
        this.descriptionAlert = false;
        this.tncAlert = false;
        this.productPriceAlert = false;
        this.productValueAlert = false;
        this.weightAlert = false;
        this.widthAlert = false;
        this.lengthAlert = false;
        this.heightAlert = false;
        this.varianAlert = false;
        this.varianAlertMessage = '';
        this.categoryAlert = false;
        this.categoryList = [];
        this.varianX = {
            name: '',
            active: false
        };
        this.varianY = {
            name: '',
            active: false
        };
        this.varianTypeX = [];
        this.varianTypeY = [];
        this.fieldVarianTypeX = [];
        this.fieldVarianTypeY = [];
        this.multiVarian = [];
        this.fieldVarianX = {
            alert: false,
            message: ""
        };
        this.fieldVarianY = {
            alert: false,
            message: ""
        };
        this.scrollView = function (id, alert, scroll, objectScroll) {
            if (alert === void 0) { alert = ''; }
            if (scroll === void 0) { scroll = true; }
            if (objectScroll === void 0) { objectScroll = false; }
            if (alert)
                _this[alert] = true;
            var defObject = objectScroll ? objectScroll : {
                behavior: "smooth",
                block: "start",
                inline: "nearest"
            };
            if (scroll)
                document.getElementById(id).scrollIntoView(defObject);
            return false;
        };
        this.eventValidation = function (event) {
            _this.validateMultiVarian(false, false);
        };
        this.validateMultiVarian = function (valReturn, scroll) {
            if (valReturn === void 0) { valReturn = false; }
            if (scroll === void 0) { scroll = true; }
            _this.varianAlertMessage = '*data varian harus lengkap';
            var isFormValid = true;
            _this.fieldVarianTypeX = [];
            _this.fieldVarianTypeY = [];
            _this.varianX.name = _this.varianX.name.toString().trim();
            _this.varianY.name = _this.varianY.name.toString().trim();
            _this.fieldVarianX = !_this.varianX.name ? { alert: true, message: 'field ini tidak boleh kosong' } : { alert: false, message: '' };
            _this.fieldVarianY = _this.varianY.active && !_this.varianY.name ? { alert: true, message: 'field ini tidak boleh kosong' } : { alert: false, message: '' };
            if (!_this.varianX.name || (_this.varianY.active && !_this.varianY.name)) {
                isFormValid = _this.scrollView('varian', 'varianAlert', scroll);
            }
            else {
                _this.varianAlert = false;
            }
            var reg = new RegExp('^[0-9]+$');
            if (_this.varianX.active && reg.test(_this.varianX.name)) {
                _this.fieldVarianX = _this.fieldVarianX.alert ? _this.fieldVarianX : { alert: true, message: 'Tipe varian tidak boleh berupa angka' };
                isFormValid = _this.scrollView('varian', 'varianAlert', scroll);
            }
            else {
                _this.varianAlert = false;
            }
            if (_this.varianY.active && reg.test(_this.varianY.name)) {
                _this.fieldVarianY = _this.fieldVarianY.alert ? _this.fieldVarianY : { alert: true, message: 'Tipe varian tidak boleh berupa angka' };
                isFormValid = _this.scrollView('varian', 'varianAlert', scroll);
            }
            else {
                _this.varianAlert = false;
            }
            if (_this.varianY.active && (_this.varianX.name.toLowerCase() == _this.varianY.name.toLowerCase())) {
                _this.fieldVarianX = _this.fieldVarianX.alert ? _this.fieldVarianX : { alert: true, message: 'Tipe varian tidak boleh memiliki value yang sama' };
                _this.fieldVarianY = _this.fieldVarianY.alert ? _this.fieldVarianY : { alert: true, message: 'Tipe varian tidak boleh memiliki value yang sama' };
                isFormValid = _this.scrollView('varian', 'varianAlert', scroll);
            }
            else {
                _this.varianAlert = false;
            }
            if (_this.varianX.active) {
                _this.varianTypeX.forEach(function (el, idx) {
                    _this.varianTypeX[idx] = el.toString().trim();
                    if (_this.varianTypeX[idx]) {
                        _this.varianAlert = false;
                    }
                    else {
                        _this.fieldVarianTypeX[idx] = {
                            alert: true,
                            message: '*field ini tidak boleh kosong'
                        };
                        isFormValid = _this.scrollView('varian', 'varianAlert', scroll);
                    }
                });
                var foundEqual = false;
                for (var i = 0; i < _this.varianTypeX.length; i++) {
                    var find = false;
                    for (var j = 0; j < _this.varianTypeX.length; j++) {
                        if (i != j) {
                            if (_this.varianTypeX[i].toLowerCase() == _this.varianTypeX[j].toLowerCase()) {
                                find = true;
                                if (!_this.fieldVarianTypeX[j] || _this.fieldVarianTypeX[j].alert == false) {
                                    _this.fieldVarianTypeX[j] =
                                        {
                                            alert: true,
                                            message: '*field tidak boleh memiliki nilai sama'
                                        };
                                    // break;
                                }
                            }
                        }
                        if (find) {
                            foundEqual = true;
                            // break;
                        }
                    }
                }
                if (foundEqual) {
                    isFormValid = _this.scrollView('varian', 'varianAlert', scroll);
                }
                else {
                    _this.varianAlert = false;
                }
            }
            if (_this.varianY.active) {
                _this.varianTypeY.forEach(function (el, idx) {
                    _this.varianTypeY[idx] = el.toString().trim();
                    if (_this.varianTypeY[idx]) {
                        _this.varianAlert = false;
                    }
                    else {
                        _this.fieldVarianTypeY[idx] = {
                            alert: true,
                            message: '*field ini tidak boleh kosong'
                        };
                        isFormValid = _this.scrollView('varian', 'varianAlert', scroll);
                    }
                });
                var foundEqual = false;
                for (var i = 0; i < _this.varianTypeY.length; i++) {
                    var find = false;
                    for (var j = 0; j < _this.varianTypeY.length; j++) {
                        if (i != j) {
                            if (_this.varianTypeY[i].toLowerCase() == _this.varianTypeY[j].toLowerCase()) {
                                find = true;
                                if (!_this.fieldVarianTypeY[j] || _this.fieldVarianTypeY[j].alert == false) {
                                    _this.fieldVarianTypeY[j] =
                                        {
                                            alert: true,
                                            message: '*field tidak boleh memiliki nilai sama'
                                        };
                                    // break;
                                }
                            }
                        }
                    }
                    if (find) {
                        foundEqual = true;
                        // break;
                    }
                }
                if (foundEqual) {
                    isFormValid = _this.scrollView('varian', 'varianAlert', scroll);
                }
                else {
                    _this.varianAlert = false;
                }
            }
            var allSkuCode = [];
            for (var i = 0; i < _this.multiVarian.length; i++) {
                for (var x = 0; x < _this.multiVarian[i].length; x++) {
                    if (!_this.multiVarian[i][x].sku_code) {
                        isFormValid = _this.scrollView('varian', 'varianAlert', scroll);
                    }
                    else if (!_this.multiVarian[i][x].sku_price) {
                        isFormValid = _this.scrollView('varian', 'varianAlert', scroll);
                    }
                    else if (!_this.multiVarian[i][x].sku_value) {
                        isFormValid = _this.scrollView('varian', 'varianAlert', scroll);
                    }
                    else if (!_this.multiVarian[i][x].status) {
                        isFormValid = _this.scrollView('varian', 'varianAlert', scroll);
                    }
                    else if (_this.multiVarian[i][x].qty < 0) {
                        isFormValid = _this.scrollView('varian', 'varianAlert', scroll);
                    }
                    else {
                        _this.varianAlert = false;
                        allSkuCode.push(_this.multiVarian[i][x].sku_code);
                    }
                }
            }
            if (isFormValid && allSkuCode.length > 0) {
                var foundEqual = false;
                for (var i = 0; i < allSkuCode.length; i++) {
                    for (var j = 0; j < allSkuCode.length; j++) {
                        if (i != j) {
                            if (allSkuCode[j].toLowerCase() == allSkuCode[i].toLowerCase()) {
                                foundEqual = true;
                            }
                        }
                        if (foundEqual)
                            break;
                    }
                    if (foundEqual)
                        break;
                }
                if (foundEqual) {
                    isFormValid = _this.scrollView('varian', 'varianAlert', scroll);
                    _this.varianAlertMessage = '*terdapat SKU Code yang sama';
                }
            }
            if (valReturn)
                return isFormValid;
        };
        this.permission.currentPermission.subscribe(function (val) {
            _this.currentPermission = val;
        });
    }
    MerchantEditProductComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.inLoadedProduct();
                console.log('multivarian', this.multiVarian, this.multiVarian.length);
                return [2 /*return*/];
            });
        });
    };
    MerchantEditProductComponent.prototype.setVendorType = function (object) {
        this.listOfVendorType.forEach(function (el) {
            if (el.name == object) {
                el.active = true;
            }
            else {
                el.active = false;
            }
        });
        // object.active = true;
        this.form.voucher_group = object;
    };
    MerchantEditProductComponent.prototype.setVoucherGroup = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    MerchantEditProductComponent.prototype.setRedeemType = function (object) {
        this.listOfRedeemType.forEach(function (el) {
            el.active = false;
        });
        object.active = true;
        this.form.redeem_type = object.name;
    };
    MerchantEditProductComponent.prototype.inLoadedProduct = function () {
        // console.log(this.permission.getCurrentPermission());
        var _this = this;
        try {
            this.route.queryParams.subscribe(function (params) { return __awaiter(_this, void 0, void 0, function () {
                var result, data, i, j, j, i, x, y;
                var _this = this;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            this.productID = params.product_code;
                            return [4 /*yield*/, this.productService.detailProducts(this.productID)];
                        case 1:
                            result = _a.sent();
                            data = result[0];
                            if (Object(_object_interface_common_object__WEBPACK_IMPORTED_MODULE_10__["isString"])(data.hashtags)) {
                                data.hashtags = [];
                            }
                            if (!data.condition) {
                                data.condition = 'new';
                            }
                            if (!data.hashtags) {
                                data.hashtags = [];
                            }
                            this.oldProductCodeName = data.product_code;
                            this.form = {
                                type: data.type,
                                voucher_group: data.voucher_group ? data.voucher_group : null,
                                merchant_username: data.merchant_username ? data.merchant_username : null,
                                min_order: data.min_order,
                                product_code: data.product_code,
                                product_name: data.product_name,
                                category: data.category,
                                price: data.price,
                                fixed_price: data.fixed_price,
                                qty: data.qty,
                                description: data.description,
                                location_for_redeem: data.location_for_redeem,
                                tnc: data.tnc,
                                need_outlet_code: data.need_outlet_code,
                                applicable_for_ios: data.applicable_for_ios,
                                dimensions: {
                                    width: data.dimensions ? data.dimensions.width : 0,
                                    length: data.dimensions ? data.dimensions.length : 0,
                                    height: data.dimensions ? data.dimensions.height : 0
                                },
                                condition: data.condition,
                                weight: data.weight ? data.weight : 0,
                                status: data.status,
                                images_gallery: data.images_gallery,
                                base_url: data.base_url,
                                pic_big_path: data.pic_big_path,
                                pic_small_path: data.pic_small_path,
                                pic_medium_path: data.pic_medium_path,
                                pic_file_name: data.pic_file_name,
                                hashtags: data.hashtags,
                                variation: data.variation,
                            };
                            if (data.type == 'e-voucher') {
                                this.form.sku_code = data.product_sku[0].sku_code;
                            }
                            if ((data.variation == null ||
                                (data.variation &&
                                    ((Object.keys(data.variation).length === 0 &&
                                        data.variation.constructor === Object) ||
                                        (Array.isArray(data.variation) &&
                                            data.variation.length == 0)))) &&
                                data.product_sku &&
                                data.product_sku.length > 0) {
                                this.form.product_value = data.product_sku[0].sku_value;
                                this.form.product_price = data.product_sku[0].sku_price;
                                this.form.qty = data.product_sku[0].qty;
                            }
                            else if (data.product_sku && data.product_sku.length > 0) {
                                this.productSku = data.product_sku;
                            }
                            if (this.form.type == 'product') {
                                this.title = 'Product';
                            }
                            else if (this.form.type == 'voucher') {
                                this.title = 'Voucher Fisik';
                            }
                            else if (this.form.type == 'e-voucher') {
                                this.title = 'E-Voucher';
                            }
                            else if (this.form.type == 'gold') {
                                this.title = 'Gold';
                            }
                            else if (this.form.type == 'top-up') {
                                this.title = 'E-Wallet';
                            }
                            this.productStatus.forEach(function (element, index) {
                                if (element.value == _this.form.status) {
                                    _this.productStatus[index].selected = 1;
                                }
                            });
                            this.editype = data.type;
                            this.loadImageToForm();
                            if (this.form.images_gallery) {
                                this.temp_gallery = this.form.images_gallery;
                            }
                            if (this.currentPermission == 'admin') {
                                this.form.merchant_username = data.merchant_username;
                            }
                            //by default weight is in Kg, need to convert it in gram
                            // this.form.weightInGram = this.form.weight * 1000;
                            this.formPriceReplacer(null, 'price');
                            this.formPriceReplacer(null, 'fixed_price');
                            this.formPriceReplacer(null, 'weight');
                            this.setVoucherGroup();
                            this.afterProductLoaded();
                            if (data.variation && data.variation.length != 0) {
                                Object.entries(data.variation).forEach(function (_a, index) {
                                    var key = _a[0], value = _a[1];
                                    if (index == 0) {
                                        _this.varianX = {
                                            name: key,
                                            active: true
                                        };
                                        _this.varianTypeX = value;
                                        _this.varianTypeY = [''];
                                    }
                                    if (index == 1) {
                                        _this.varianY = {
                                            name: key,
                                            active: true
                                        };
                                        _this.varianTypeY = value;
                                    }
                                });
                                if (this.varianY.active) {
                                    for (i = 0; i < this.varianTypeY.length; i++) {
                                        this.multiVarian[i] = [];
                                        for (j = 0; j < this.varianTypeX.length; j++) {
                                            this.multiVarian[i][j] = this.generateProductSKU();
                                        }
                                    }
                                }
                                else {
                                    this.multiVarian.push([]);
                                    for (j = 0; j < this.varianTypeX.length; j++) {
                                        this.multiVarian[0].push(this.generateProductSKU());
                                    }
                                }
                                for (i = 0; i < data.product_sku.length; i++) {
                                    Object.entries(data.product_sku[i].variant).forEach(function (_a, index) {
                                        var key = _a[0], value = _a[1];
                                        if (_this.varianX.name == key) {
                                            x = _this.varianTypeX.indexOf(value);
                                        }
                                        else if (_this.varianY.name == key) {
                                            y = _this.varianTypeY.indexOf(value);
                                        }
                                    });
                                    if (typeof y == 'number') {
                                        this.multiVarian[y][x] = data.product_sku[i];
                                        this.multiVarian[y][x].isloading = false;
                                    }
                                    else {
                                        this.multiVarian[0][x] = data.product_sku[i];
                                        this.multiVarian[0][x].isloading = false;
                                    }
                                }
                            }
                            return [2 /*return*/];
                    }
                });
            }); });
        }
        catch (error) {
            this.errorLabel = (error.message); //conversion to Error type
            if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
            }
        }
    };
    MerchantEditProductComponent.prototype.clickVarian = function () {
        this.addVariationType();
        // this.form.product_price = 0;
        // this.form.product_value = 0;
    };
    MerchantEditProductComponent.prototype.changeText = function () {
        var _a, _b;
        try {
            this.productSku = [];
            this.isLoading = [];
            for (var i = 0; i < this.dataVarianModel[0].length; i++) {
                if (this.dataVarianModel[1]) {
                    for (var j = 0; j < this.dataVarianModel[1].length; j++) {
                        this.productSku.push({
                            // "sku_code":"TEST1",
                            // "sku_price":100000,
                            // "sku_value":1000,
                            "variant": (_a = {},
                                _a[this.varianHeader[0]] = this.dataVarianModel[0][i],
                                _a[this.varianHeader[1]] = this.dataVarianModel[1][j],
                                _a),
                            // "qty":100,
                            "image_gallery": [
                            // {
                            //     "base_url" : "https://res.cloudinary.com/dqcj36zfd/image/upload/",
                            //     "pic_file_name" : "v1615863583/admin/image/montiss-1.60501f1dce424.jpg",
                            //     "pic_small_path" : "s--g8KpJ7Mu--/c_limit,q_auto:good,w_150/v1/admin/image/montiss-1.60501f1dce424.jpg",
                            //     "pic_medium_path" : "s--dMwcc-P3--/c_limit,q_auto:good,w_600/v1/admin/image/montiss-1.60501f1dce424.jpg",
                            //     "pic_big_path" : "v1615863583/admin/image/montiss-1.60501f1dce424.jpg"
                            // }
                            ],
                        });
                        this.isLoading.push(false);
                    }
                }
                else {
                    this.productSku.push({
                        // "sku_code":"TEST1",
                        // "sku_price":100000,
                        // "sku_value":1000,
                        "variant": (_b = {},
                            _b[this.varianHeader[0]] = this.dataVarianModel[0][i],
                            _b),
                        // "qty":100,
                        "image_gallery": [
                        // {
                        //     "base_url" : "https://res.cloudinary.com/dqcj36zfd/image/upload/",
                        //     "pic_file_name" : "v1615863583/admin/image/montiss-1.60501f1dce424.jpg",
                        //     "pic_small_path" : "s--g8KpJ7Mu--/c_limit,q_auto:good,w_150/v1/admin/image/montiss-1.60501f1dce424.jpg",
                        //     "pic_medium_path" : "s--dMwcc-P3--/c_limit,q_auto:good,w_600/v1/admin/image/montiss-1.60501f1dce424.jpg",
                        //     "pic_big_path" : "v1615863583/admin/image/montiss-1.60501f1dce424.jpg"
                        // }
                        ],
                    });
                    this.isLoading.push(false);
                }
            }
        }
        catch (error) {
            console.log("error", error);
        }
    };
    MerchantEditProductComponent.prototype.addVariationType = function () {
        this.dataVarian.push([
            ''
        ]);
        this.dataVarianModel.push([
            ''
        ]);
        if (this.multiVarian.length < 1) {
            this.varianX.active = true;
            this.addVarianTypeY('');
            this.addVarianTypeX('');
        }
    };
    MerchantEditProductComponent.prototype.activatedVarianY = function () {
        this.varianY.active = true;
        this.validateMultiVarian(false, false);
    };
    MerchantEditProductComponent.prototype.addVarianTypeX = function (fieldX) {
        if (fieldX === void 0) { fieldX = ''; }
        this.varianTypeX.push(fieldX);
        console.log("varian type y", this.varianTypeY);
        for (var i = 0; i < this.varianTypeY.length; i++) {
            this.multiVarian[i].push(this.generateProductSKU(this.varianTypeY[i], fieldX));
            console.log("data Varian", this.multiVarian);
        }
        this.validateMultiVarian(false, false);
    };
    MerchantEditProductComponent.prototype.deleteVarianTypeX = function (index) {
        for (var i = 0; i < this.varianTypeY.length; i++) {
            this.multiVarian[i].splice(index, 1);
        }
        this.varianTypeX.splice(index, 1);
        this.validateMultiVarian(false, false);
    };
    MerchantEditProductComponent.prototype.addVarianTypeY = function (fieldY) {
        if (fieldY === void 0) { fieldY = ''; }
        this.varianTypeY.push(fieldY);
        var arrayY = [];
        for (var i = 0; i < this.varianTypeX.length; i++) {
            arrayY.push(this.generateProductSKU(fieldY, this.varianTypeX[i]));
        }
        this.multiVarian.push(arrayY);
        this.validateMultiVarian(false, false);
    };
    MerchantEditProductComponent.prototype.deleteVarianTypeY = function (index) {
        this.multiVarian.splice(index, 1);
        this.varianTypeY.splice(index, 1);
        this.validateMultiVarian(false, false);
    };
    MerchantEditProductComponent.prototype.deleteAllTypeY = function () {
        if (this.varianX.active) {
            this.multiVarian.splice(1, this.multiVarian.length - 1);
            this.varianTypeY = [''];
            this.varianY.active = false;
            this.varianY.name = '';
            this.fieldVarianX = {
                alert: false,
                message: ''
            };
            this.fieldVarianY = {
                alert: false,
                message: ''
            };
        }
        else {
            this.deleteAllVarian();
        }
        this.validateMultiVarian(false, false);
    };
    MerchantEditProductComponent.prototype.deleteAllTypeX = function () {
        if (this.varianY.active) {
            var tempX = [];
            for (var i = 0; i < this.multiVarian.length; i++) {
                tempX.push(this.multiVarian[i][0]);
            }
            this.multiVarian = [tempX];
            this.varianTypeX = this.varianTypeY.slice();
            this.varianX.name = this.varianY.name;
            this.varianTypeY = [''];
            this.varianY.active = false;
            this.varianY.name = '';
            this.fieldVarianX = {
                alert: false,
                message: ''
            };
            this.fieldVarianY = {
                alert: false,
                message: ''
            };
        }
        else {
            this.deleteAllVarian();
        }
        this.validateMultiVarian(false, false);
    };
    MerchantEditProductComponent.prototype.deleteAllVarian = function () {
        this.varianX = {
            name: '',
            active: false
        };
        this.varianY = {
            name: '',
            active: false
        };
        this.varianTypeX = [];
        this.varianTypeY = [];
        this.multiVarian = [];
        this.fieldVarianX = {
            alert: false,
            message: ''
        };
        this.fieldVarianY = {
            alert: false,
            message: ''
        };
    };
    MerchantEditProductComponent.prototype.generateProductSKU = function (valueY, valueX) {
        if (valueY === void 0) { valueY = ''; }
        if (valueX === void 0) { valueX = ''; }
        return {
            sku_code: '',
            sku_price: '',
            sku_value: '',
            qty: 0,
            image_gallery: [],
            status: 'ACTIVE',
            isloading: false,
        };
    };
    MerchantEditProductComponent.prototype.deleteVariationType = function (indexValue) {
        this.varianHeader.splice(indexValue, 1);
        this.dataVarian.splice(indexValue, 1);
        this.dataVarianModel.splice(indexValue, 1);
    };
    MerchantEditProductComponent.prototype.addVariationValue = function (index) {
        this.dataVarian[index].push('');
        this.dataVarianModel[index].push('');
    };
    MerchantEditProductComponent.prototype.deleteVariationValue = function (index, indexValue) {
        this.dataVarian[index].splice(indexValue, 1);
        this.dataVarianModel[index].splice(indexValue, 1);
    };
    MerchantEditProductComponent.prototype.genVoucherToggler = function (val) {
        this.generateVoucherToggler = val;
    };
    MerchantEditProductComponent.prototype.loadImageToForm = function () {
        var _this = this;
        this.uploads = {
            image1: '',
            image2: '',
            image3: '',
            image4: '',
            image5: '',
        };
        if (this.form.images_gallery) {
            this.form.images_gallery.forEach(function (element, i) {
                var img = _this.gallery_label[i];
                // console.log('element',)
                _this.uploads[img] = element.base_url + element.pic_big_path;
            });
        }
        // if (this.form.images_gallery == undefined) {
        // 	this.uploads['image3'] = this.form.base_url + this.form.pic_big_path
        // 	let temp = {
        // 		base_url: this.form.base_url,
        // 		pic_big_path: this.form.pic_big_path,
        // 		pic_small_path: this.form.pic_small_path,
        // 		pic_medium_path: this.form.pic_medium_path,
        // 		pic_file_name: this.form.pic_file_name,
        // 	};
        // 	this.temp_gallery.push(temp)
        // }
    };
    MerchantEditProductComponent.prototype.afterProductLoaded = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, error_1;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        this.formPriceReplacer(null, 'product_price');
                        this.formPriceReplacer(null, 'product_value');
                        this.formPriceReplacer(null, 'weight');
                        _a = this;
                        return [4 /*yield*/, this.productService.getCategoryLint()];
                    case 1:
                        _a.categoryList = _b.sent();
                        this.categoryList.forEach(function (element) {
                            if (element.type == "product") {
                                _this.categoryProduct = element.category_list;
                            }
                            if (element.type == "voucher") {
                                _this.categoryVoucher = element.category_list;
                            }
                            if (element.type == "e-voucher") {
                                _this.categoryEvoucher = element.category_list;
                            }
                            if (element.type == "gold") {
                                _this.categoryGold = element.category_list;
                            }
                            if (element.type == "top-up") {
                                _this.categoryEwallet = element.category_list;
                            }
                        });
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _b.sent();
                        console.log("error", error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MerchantEditProductComponent.prototype.addHashtags = function (newWord) {
        this.form.hashtags.push(newWord);
    };
    MerchantEditProductComponent.prototype.addHashasdtags = function (newWord) {
        this.form.hashtags.push(newWord);
    };
    MerchantEditProductComponent.prototype.hashtagsKeyDown = function (event) {
        var n = event.target.textContent;
        var key = event.key.toLowerCase();
        // console.log("key", key)
        if (key == ' ' || key == 'enter') {
            if (n.trim() !== '') {
                this.addHashtags(n);
            }
            event.target.textContent = '';
            event.preventDefault();
        }
        if (key == 'backspace' && n == '' && this.form.hashtags.length > 0) {
            this.form.hashtags.pop();
        }
    };
    MerchantEditProductComponent.prototype.productNameInputed = function ($event) {
        if (this.autoSKU == true) {
            var s = this.form.product_name.toLowerCase().replace(/[\s\?\*]/g, '-');
            this.form.product_code = s;
        }
    };
    MerchantEditProductComponent.prototype.deleteThis = function () {
        return __awaiter(this, void 0, void 0, function () {
            var r;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.productService.deleteProduct(this.form.product_code)];
                    case 1:
                        r = _a.sent();
                        if (r.status && this.currentPermission == 'admin') {
                            alert('Your Product ' + this.form.product_name + ' Has been deleted');
                            this.router.navigate(['/administrator/productadmin']);
                            // window.location.reload();
                        }
                        else if (this.currentPermission == 'merchant') {
                            alert('Your Product' + this.form.product_name + ' Has been deleted');
                            this.router.navigate(['/merchant-portal/product-list']);
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    MerchantEditProductComponent.prototype.toggleDeleteConfirm = function (l) {
        this.toggleDelete = l;
    };
    MerchantEditProductComponent.prototype.formPriceReplacer = function ($event, label) {
        if (this.form[label]) {
            if (typeof this.form[label] == 'number')
                this.form[label] = this.form[label].toString();
            var s = this.form[label].toLowerCase().replace(/[^0-9]/g, '');
            var c = parseInt(s);
            if (isNaN(c)) {
                this.form[label] = 0;
            }
            else {
                this.form[label] = c.toLocaleString('en');
            }
        }
    };
    MerchantEditProductComponent.prototype.onFileSelected2 = function (event, img) {
        return __awaiter(this, void 0, void 0, function () {
            var reader, fileMaxSize_1, logo, e_1;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        reader = new FileReader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, , 5]);
                        this.errorFile = false;
                        fileMaxSize_1 = 3000000;
                        Array.from(event.target.files).forEach(function (file) {
                            if (file.size > fileMaxSize_1) {
                                _this.errorFile = 'Maximum File Upload is 3MB';
                            }
                        });
                        this.selectedFile = event.target.files[0];
                        reader.onload = function (event) {
                            _this.selFile = event.target.result;
                        };
                        if (event.target.files[0]) {
                            reader.readAsDataURL(event.target.files[0]);
                        }
                        if (!this.selectedFile) return [3 /*break*/, 3];
                        switch (img) {
                            case 'image1':
                                this.showLoading = true;
                                break;
                            case 'image2':
                                this.showLoading2 = true;
                                break;
                            case 'image3':
                                this.showLoading3 = true;
                                break;
                            case 'image4':
                                this.showLoading4 = true;
                                break;
                            case 'image5':
                                this.showLoading5 = true;
                                break;
                        }
                        return [4 /*yield*/, this.productService.upload(this.selectedFile, this, 'image', function (result) {
                                _this.callImage(result, img);
                            })];
                    case 2:
                        logo = _a.sent();
                        _a.label = 3;
                    case 3: return [3 /*break*/, 5];
                    case 4:
                        e_1 = _a.sent();
                        this.errorLabel = e_1.message; //conversion to Error type
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    MerchantEditProductComponent.prototype.callImage = function (result, img) {
        if (img == 'image3') {
            var product = this.form;
            this.form = __assign({}, product, result);
        }
        // console.log('result upload logo ', result,img);
        this.uploads[img] = result.base_url + result.pic_big_path;
        // this.temp_gallery.push(result)
        var a = this.gallery_label.indexOf(img);
        this.temp_gallery[a] = result;
        // console.log('This detail', this.temp_gallery);
        this.allfalse();
    };
    MerchantEditProductComponent.prototype.deleteImage = function (img) {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
            title: 'Are you sure?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then(function (result) {
            if (result.isConfirmed) {
                var a = _this.gallery_label.indexOf(img);
                _this.uploads[img] = '';
                _this.temp_gallery.splice(a, 1);
                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire('Deleted!', 'Your file has been deleted.', 'success');
                _this.loadImageToForm();
            }
        });
    };
    MerchantEditProductComponent.prototype.allfalse = function () {
        this.showLoading = this.showLoading2 = this.showLoading3
            = this.showLoading4 = this.showLoading5 = false;
    };
    MerchantEditProductComponent.prototype.onImageSelected = function (event) {
        var _this = this;
        this.isFileUploaded = false;
        this.errorLabel = false;
        var fileMaxSize = 3000000; // let say 3Mb
        var reader = new FileReader();
        reader.onload = function (event) {
            _this.selFile = event.target.result;
        };
        reader.readAsDataURL(event.target.files[0]);
        Array.from(event.target.files).forEach(function (file) {
            if (file.size > fileMaxSize) {
                _this.errorLabel = 'maximum file is 3Mb';
            }
        });
        // console.log(event.target.files, this.selFile);
        if (this.errorLabel == false) {
            this.selectedFile = event.target.files[0];
            setTimeout(function () {
                _this.onUpload();
            }, 1500);
        }
    };
    MerchantEditProductComponent.prototype.onFileSelected = function (event) {
        var _this = this;
        this.isFileUploaded = false;
        this.errorLabel = false;
        var fileMaxSize = 3000000; // let say 3Mb
        var reader = new FileReader();
        reader.onload = function (event) {
            _this.selFile = event.target.result;
        };
        reader.readAsDataURL(event.target.files[0]);
        Array.from(event.target.files).forEach(function (file) {
            if (file.size > fileMaxSize) {
                _this.errorLabel = 'maximum file is 3Mb';
            }
        });
        // console.log(event.target.files, this.selFile);
        if (this.errorLabel == false) {
            this.selectedFile = event.target.files[0];
            // setTimeout(() => {
            //   this.uploadFile();
            // }, 1500)
        }
    };
    // onFileSelected(event){
    //   this.errorFile = false;
    //   let fileMaxSize = 3000000;// let say 3Mb
    //   Array.from(event.target.files).forEach((file: any) => {
    //       if(file.size > fileMaxSize){
    //         this.errorFile="maximum file is 3Mb";
    //       }
    //   });
    //   this.selectedFile = event.target.files[0];
    // }
    MerchantEditProductComponent.prototype.onUpload = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_2;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        this.cancel = false;
                        result = void 0;
                        if (!this.selectedFile) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.productService.upload(this.selectedFile, this, 'image', function (result) {
                                _this.callAfterUpload(result);
                            })];
                    case 1:
                        result = _a.sent();
                        _a.label = 2;
                    case 2: return [3 /*break*/, 4];
                    case 3:
                        e_2 = _a.sent();
                        this.errorLabel = e_2.message; //conversion to Error type
                        return [3 /*break*/, 4];
                    case 4: 
                    //   this.onUpload.
                    return [2 /*return*/, false];
                }
            });
        });
    };
    MerchantEditProductComponent.prototype.generateVoucher = function () {
        return __awaiter(this, void 0, void 0, function () {
            var params, e_3;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        if (!(this.genVoucher.expiry_date && this.genVoucher.expiry_date.year)) return [3 /*break*/, 2];
                        params = {
                            expiry_date: this.genVoucher.expiry_date.year +
                                '/' +
                                this.genVoucher.expiry_date.month +
                                '/' +
                                this.genVoucher.expiry_date.day,
                            qty: this.genVoucher.qty,
                            product_code: this.form.product_code,
                            merchant_username: this.form.merchant_username
                        };
                        return [4 /*yield*/, this.eVoucherService.generateEVoucherLint(params).then(function (result) {
                                if (result.result && result.result.status == 'success') {
                                    if (_this.currentPermission == 'admin') {
                                        alert("Voucher has been generated!");
                                        _this.router.navigate(['administrator/evoucheradmin']);
                                    }
                                    else if (_this.currentPermission == 'merchant') {
                                        _this.router.navigate(['merchant-portal/product-list/generated-voucher/list'], {
                                            queryParams: { id: _this.productID }
                                        });
                                    }
                                }
                            })];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2: return [3 /*break*/, 4];
                    case 3:
                        e_3 = _a.sent();
                        this.errorLabel = e_3.message; //conversion to Error type
                        console.log('ERROR', this.errorLabel);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    MerchantEditProductComponent.prototype.generatedVoucher = function () {
        if (this.currentPermission == 'admin') {
            this.router.navigate(['administrator/evoucheradmin'], { queryParams: { id: this.form.product_code } });
        }
        else if (this.currentPermission == 'merchant') {
            this.router.navigate(['merchant-portal/product-list/generated-voucher/list'], {
                queryParams: { id: this.form.product_code }
            });
        }
    };
    MerchantEditProductComponent.prototype.callAfterUpload = function (result) {
        if (result) {
            var product = this.form;
            this.form = __assign({}, product, result);
            var temp = [];
            temp.push(result);
            this.temp_gallery = temp;
            this.isFileUploaded = true;
        }
    };
    MerchantEditProductComponent.prototype.stringToNumber = function (str) {
        if (typeof str == 'number') {
            str = str + '';
        }
        var s = str.toLowerCase().replace(/[^0-9]/g, '');
        return parseInt(s);
    };
    MerchantEditProductComponent.prototype.parseStringToArray = function (str) {
        var splitR;
        if (!Object(_object_interface_common_object__WEBPACK_IMPORTED_MODULE_10__["isString"])(str))
            return [];
        else {
            splitR = str.split(/[\,\.]/);
        }
        return splitR;
    };
    MerchantEditProductComponent.prototype.saveThis = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b, data_1, isFormValid, dataProduct, varianList, i, allProductSKU, i, j, tempProductSKU, e_4, message;
            var _this = this;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _c.trys.push([0, 3, , 4]);
                        if (this.currentPermission == 'merchant' || this.currentPermission == 'admin') {
                            // delete this.form.merchant_username;
                            delete this.form.merchant_group;
                            // this.form.tnc = "no tnc"
                        }
                        if (this.form.type == 'product' || this.form.type == 'voucher' || this.form.type == 'gold' || this.form.type == 'top-up') {
                            delete this.form.location_for_redeem;
                            delete this.form.voucher_group;
                        }
                        data_1 = this.form;
                        isFormValid = true;
                        if (this.form.type != "e-voucher" && this.form.type != "top-up" && (this.form.dimensions.height <= 0 || this.form.dimensions.height == "" || this.form.dimensions.height == "0")) {
                            isFormValid = this.scrollView('weight', 'heightAlert');
                        }
                        else {
                            this.heightAlert = false;
                        }
                        if (this.form.type != "e-voucher" && this.form.type != "top-up" && (this.form.dimensions.length <= 0 || this.form.dimensions.length == "" || this.form.dimensions.length == "0")) {
                            isFormValid = this.scrollView('weight', 'lengthAlert');
                        }
                        else {
                            this.lengthAlert = false;
                        }
                        if (this.form.type != "e-voucher" && this.form.type != "top-up" && (this.form.dimensions.width <= 0 || this.form.dimensions.width == "" || this.form.dimensions.width == "0")) {
                            isFormValid = this.scrollView('weight', 'widthAlert');
                        }
                        else {
                            this.widthAlert = false;
                        }
                        if (this.form.type != "e-voucher" && this.form.type != "top-up" && (this.form.weight <= 0 || this.form.weight == "" || this.form.weight == "0")) {
                            isFormValid = this.scrollView('weight', 'weightAlert');
                        }
                        else {
                            this.weightAlert = false;
                        }
                        if (this.multiVarian && this.multiVarian.length > 0) {
                            isFormValid = this.validateMultiVarian(true);
                        }
                        if ((this.multiVarian && this.multiVarian.length == 0) && (!this.form.product_price || this.form.product_price <= 0 || this.form.product_price == "" || this.form.product_price == "0")) {
                            isFormValid = this.scrollView('price', 'productPriceAlert');
                        }
                        else {
                            this.productPriceAlert = false;
                        }
                        if (this.form.type == "e-voucher" && this.form.tnc == "") {
                            isFormValid = this.scrollView('tnc', 'tncAlert');
                        }
                        else {
                            this.tncAlert = false;
                        }
                        if (this.form.description == "") {
                            this.form.description = this.form.product_name;
                            // isFormValid = this.scrollView('description', 'descriptionAlert');
                        }
                        else {
                            this.descriptionAlert = false;
                        }
                        if (this.form.product_code == "") {
                            isFormValid = this.scrollView('product-information', 'productCodeAlert');
                        }
                        else {
                            this.productCodeAlert = false;
                        }
                        if (this.form.product_name == "") {
                            isFormValid = this.scrollView('product-information', 'productNameAlert');
                        }
                        else {
                            this.productNameAlert = false;
                        }
                        if (this.temp_gallery.length <= 0) {
                            isFormValid = this.scrollView('photo-alert', 'photoAlert');
                        }
                        else {
                            this.photoAlert = false;
                        }
                        if (!(isFormValid == true)) return [3 /*break*/, 2];
                        // this.productSku.forEach((el) => {
                        // 	el.sku_price = this.stringToNumber(el.sku_price);
                        // 	el.sku_value = this.stringToNumber(el.sku_value);
                        // });
                        data_1.voucher_group = this.form.voucher_group;
                        // data.price = this.stringToNumber(data.price);
                        // data.fixed_price = this.stringToNumber(data.fixed_price);
                        // data.category = this.parseStringToArray(data.category);
                        // data.min_order = parseInt(data.min_order);
                        // data.pic_file_name = String(data.pic_file_name);
                        if (data_1.product_price)
                            data_1.product_price = this.stringToNumber(data_1.product_price);
                        if (data_1.product_value)
                            data_1.product_value = this.stringToNumber(data_1.product_value);
                        data_1.weight = this.stringToNumber(data_1.weight);
                        // data.weight = data.weightInGram / 1000;
                        data_1.images_gallery = this.temp_gallery;
                        data_1.dimensions = {
                            length: data_1.type == "e-voucher" ? 0 : parseFloat(data_1.dimensions.length),
                            width: data_1.type == "e-voucher" ? 0 : parseFloat(data_1.dimensions.width),
                            height: data_1.type == "e-voucher" ? 0 : parseFloat(data_1.dimensions.height)
                        };
                        if (data_1.currentPermission == 'admin' && this.form.merchant_username) {
                            data_1.merchant_username = this.form.merchant_username;
                        }
                        data_1.qty = data_1.type == "e-voucher" ? 0 : parseInt(data_1.qty);
                        // data.category = [];
                        data_1.weight = data_1.type == "e-voucher" ? 0 : data_1.weight;
                        this.multipleCategories.forEach(function (el) {
                            if (el.value)
                                data_1.category.push(el.code);
                        });
                        this.productSku.forEach(function (el) {
                            el.sku_price = _this.stringToNumber(el.sku_price);
                            el.sku_value = _this.stringToNumber(el.sku_value);
                        });
                        dataProduct = void 0;
                        varianList = {};
                        for (i = 0; i < this.varianHeader.length; i++) {
                            varianList[this.varianHeader[i]] = this.dataVarianModel[i];
                        }
                        allProductSKU = [];
                        for (i = 0; i < this.multiVarian.length; i++) {
                            for (j = 0; j < this.multiVarian[i].length; j++) {
                                tempProductSKU = this.multiVarian[i][j];
                                delete tempProductSKU.isloading;
                                tempProductSKU.variant = (_a = {},
                                    _a[this.varianX.name] = this.varianTypeX[j],
                                    _a);
                                if (this.varianY.active)
                                    tempProductSKU.variant[this.varianY.name] = this.varianTypeY[i];
                                allProductSKU.push(tempProductSKU);
                            }
                        }
                        // if(data.type == 'product' || data.type == 'voucher') {
                        dataProduct = {
                            product_name: data_1.product_name,
                            product_code: data_1.product_code,
                            // product_price:data.product_price,
                            // product_value:data.product_value,
                            description: data_1.description,
                            variation: varianList,
                            type: data_1.type,
                            category: data_1.category,
                            status: data_1.status,
                            weight: data_1.weight,
                            dimensions: data_1.dimensions,
                            images_gallery: data_1.images_gallery,
                            qty: data_1.qty
                            // product_sku: this.productSku,
                        };
                        // }
                        if (this.varianX.active) {
                            dataProduct.product_sku = allProductSKU;
                            dataProduct.variation = (_b = {},
                                _b[this.varianX.name] = this.varianTypeX,
                                _b);
                            if (this.varianY.active)
                                dataProduct.variation[this.varianY.name] = this.varianTypeY;
                        }
                        else {
                            dataProduct.variation = [];
                            dataProduct.product_value = parseFloat(data_1.product_value);
                            dataProduct.product_price = parseFloat(data_1.product_price);
                            dataProduct.qty = data_1.qty;
                        }
                        if (data_1.type == "e-voucher") {
                            dataProduct.tnc = data_1.tnc;
                        }
                        // if(varianList && ((Object.keys(varianList).length === 0 && varianList.constructor === Object) || (Array.isArray(varianList) && varianList.length == 0))) {
                        // 	dataProduct.product_value = data.product_value;
                        // 	dataProduct.product_price = data.product_price;
                        // 	dataProduct.qty = data.qty;
                        // } else if(this.productSku && this.productSku.length > 0) {
                        // 	dataProduct.product_sku = this.productSku;
                        // }
                        console.log("data product", dataProduct);
                        return [4 /*yield*/, this.productService.updateProductData(dataProduct)];
                    case 1:
                        _c.sent();
                        this.errorLabel = false;
                        sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire("Product/Voucher has been updated!").then(function () {
                            if (_this.currentPermission == 'merchant') {
                                _this.router.navigate(['/merchant-portal/product-list/detail'], {
                                    queryParams: { id: _this.productID }
                                });
                            }
                            else if (_this.currentPermission == 'admin' && data_1.type == "e-voucher") {
                                _this.router.navigate(['administrator/vouchers']);
                            }
                            else if (_this.currentPermission == 'admin') {
                                _this.router.navigate(['administrator/productadmin']);
                            }
                        });
                        _c.label = 2;
                    case 2: return [3 /*break*/, 4];
                    case 3:
                        e_4 = _c.sent();
                        this.errorLabel = e_4.message; //conversion to Error type
                        message = this.errorLabel;
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            message = "Anda tidak memiliki hak akses untuk menggunakan fitur ini";
                        }
                        sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                            icon: 'error',
                            title: message,
                        });
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    MerchantEditProductComponent.prototype.validateForm = function (form) {
        for (var prop in form) {
            if (!form.hasOwnProperty(prop) ||
                prop == 'tnc' ||
                prop == 'category' ||
                prop == 'weight'
                || prop == 'hashtags' || prop == 'images_gallery' || prop == 'description')
                continue;
            if (prop == 'base_url') {
                if (!form[prop]) {
                    throw new Error('Please upload new image for your product ');
                }
            }
            // console.log(prop, form[prop] == undefined, form[prop] == '')
            if (form[prop] == undefined) {
                throw new Error('you need to completing this ' + this.labels[prop]);
            }
            if (Object(_object_interface_common_object__WEBPACK_IMPORTED_MODULE_10__["isString"])(form[prop]) && form[prop].trim() == '')
                throw new Error('you need to completing this input ' + this.labels[prop]);
            if (Object(ngx_bootstrap_chronos__WEBPACK_IMPORTED_MODULE_2__["isArray"])(form[prop]) && form[prop].length == 0) {
                throw new Error('Hi, you need to completing this ' + this.labels[prop]);
            }
        }
    };
    MerchantEditProductComponent.prototype.if = function (listOfProductType) { };
    MerchantEditProductComponent.prototype.toggleCategories = function (index) {
        this.multipleCategories.forEach(function (el) {
            el.value = false;
        });
        this.multipleCategories[index].value = true;
    };
    MerchantEditProductComponent.prototype.uploadFile = function () {
        return __awaiter(this, void 0, void 0, function () {
            var logo, e_5;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        if (!this.selectedFile) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.productService.uploadFile(this.selectedFile, this, 'evoucher', function (result) {
                                _this.callAfterUpload;
                            })];
                    case 1:
                        logo = _a.sent();
                        _a.label = 2;
                    case 2: return [3 /*break*/, 4];
                    case 3:
                        e_5 = _a.sent();
                        this.errorLabel = e_5.message;
                        return [3 /*break*/, 4];
                    case 4:
                        if (this.selectedFile) {
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    // setEdit(){
    //   if(this.edit_status){
    //     this.edit_status = false
    //   }
    //   else{
    //     this.edit_status = true
    //   }
    // }
    MerchantEditProductComponent.prototype.onFileVariant = function (event, image_gallery, index) {
        return __awaiter(this, void 0, void 0, function () {
            var reader, fileMaxSize_2, logo, e_6;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        reader = new FileReader();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, , 5]);
                        this.errorFile = false;
                        fileMaxSize_2 = 3000000;
                        Array.from(event.target.files).forEach(function (file) {
                            if (file.size > fileMaxSize_2) {
                                _this.errorFile = 'Maximum File Upload is 3MB';
                            }
                        });
                        this.selectedFile = event.target.files[0];
                        reader.onload = function (event) {
                            _this.selFile = event.target.result;
                        };
                        if (event.target.files[0]) {
                            reader.readAsDataURL(event.target.files[0]);
                        }
                        if (!this.selectedFile) return [3 /*break*/, 3];
                        // switch (img) {
                        // 	case 'image1':
                        // 		this.showLoading = true;
                        // 		console.log("ini1", img)
                        // 		break;
                        // 	case 'image2':
                        // 		this.showLoading2 = true;
                        // 		console.log("ini2", img)
                        // 		break;
                        // 	case 'image3':
                        // 		this.showLoading3 = true;
                        // 		console.log("ini3", img)
                        // 		break;
                        // 	case 'image4':
                        // 		this.showLoading4 = true;
                        // 		console.log("ini4", img)
                        // 		break;
                        // 	case 'image5':
                        // 		this.showLoading5 = true;
                        // 		console.log("ini5", img)
                        // 		break;
                        // }
                        // let result = {
                        //   "base_url" : "https://res.cloudinary.com/dqcj36zfd/image/upload/",
                        //   "pic_file_name" : "v1615863583/admin/image/montiss-1.60501f1dce424.jpg",
                        //   "pic_small_path" : "s--g8KpJ7Mu--/c_limit,q_auto:good,w_150/v1/admin/image/montiss-1.60501f1dce424.jpg",
                        //   "pic_medium_path" : "s--dMwcc-P3--/c_limit,q_auto:good,w_600/v1/admin/image/montiss-1.60501f1dce424.jpg",
                        //   "pic_big_path" : "v1615863583/admin/image/montiss-1.60501f1dce424.jpg"
                        // }
                        // image_gallery.push(result);
                        this.isLoading[index] = true;
                        return [4 /*yield*/, this.productService.upload(this.selectedFile, this, 'image', function (result) {
                                image_gallery.push(result);
                                _this.isLoading[index] = false;
                            })];
                    case 2:
                        logo = _a.sent();
                        _a.label = 3;
                    case 3: return [3 /*break*/, 5];
                    case 4:
                        e_6 = _a.sent();
                        console.log("error upload", e_6);
                        this.errorLabel = e_6.message; //conversion to Error type
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    MerchantEditProductComponent.prototype.deleteVarianImage = function (image_gallery) {
        sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
            title: 'Are you sure?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then(function (result) {
            if (result.isConfirmed) {
                image_gallery.length = 0;
                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire('Deleted!', 'Your file has been deleted.', 'success');
                // this.loadImageToForm();
            }
        });
    };
    MerchantEditProductComponent.prototype.backClicked = function () {
        this._location.back();
    };
    MerchantEditProductComponent.ctorParameters = function () { return [
        { type: _services_product_product_service__WEBPACK_IMPORTED_MODULE_1__["ProductService"] },
        { type: _services_category_category_service__WEBPACK_IMPORTED_MODULE_5__["CategoryService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"] },
        { type: _services_observerable_permission_observer__WEBPACK_IMPORTED_MODULE_7__["PermissionObserver"] },
        { type: _services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_8__["EVoucherService"] },
        { type: _services_merchant_merchant_service__WEBPACK_IMPORTED_MODULE_9__["MerchantService"] },
        { type: _angular_common__WEBPACK_IMPORTED_MODULE_11__["Location"] }
    ]; };
    MerchantEditProductComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-merchant-edit-product-product',
            template: __webpack_require__(/*! raw-loader!./merchant-edit-product.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/merchant-portal/separated-modules/merchant-products/products/merchant-edit-product/merchant-edit-product.component.html"),
            styles: [__webpack_require__(/*! ./merchant-edit-product.component.scss */ "./src/app/layout/merchant-portal/separated-modules/merchant-products/products/merchant-edit-product/merchant-edit-product.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_product_product_service__WEBPACK_IMPORTED_MODULE_1__["ProductService"],
            _services_category_category_service__WEBPACK_IMPORTED_MODULE_5__["CategoryService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"],
            _services_observerable_permission_observer__WEBPACK_IMPORTED_MODULE_7__["PermissionObserver"],
            _services_e_voucher_e_voucher_service__WEBPACK_IMPORTED_MODULE_8__["EVoucherService"],
            _services_merchant_merchant_service__WEBPACK_IMPORTED_MODULE_9__["MerchantService"],
            _angular_common__WEBPACK_IMPORTED_MODULE_11__["Location"]])
    ], MerchantEditProductComponent);
    return MerchantEditProductComponent;
}());



/***/ }),

/***/ "./src/app/services/observerable/permission-observer.ts":
/*!**************************************************************!*\
  !*** ./src/app/services/observerable/permission-observer.ts ***!
  \**************************************************************/
/*! exports provided: PermissionObserver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PermissionObserver", function() { return PermissionObserver; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PermissionObserver = /** @class */ (function () {
    function PermissionObserver() {
        this.currentPermission = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"]({});
        this.castCurrentPermission = this.currentPermission.asObservable();
        this.api_url = '';
    }
    PermissionObserver.prototype.updateCurrentPermission = function (data) {
        this.currentPermission.next(data);
    };
    PermissionObserver = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root',
        }),
        __metadata("design:paramtypes", [])
    ], PermissionObserver);
    return PermissionObserver;
}());



/***/ })

}]);
//# sourceMappingURL=default~merchant-portal-merchant-portal-module~modules-product-evoucher-evoucher-module~modules-prod~65e48bf7.js.map