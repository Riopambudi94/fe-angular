(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-admin-crr-crr-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin-crr/crr.component.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/admin-crr/crr.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\r\n    <app-page-header [heading]=\"'Customer Retention Rate (CRR)'\" [icon]=\"'fa-table'\" [topBarMenu]=\"topBarMenu\"></app-page-header>\r\n    <div class=\"summary-report row\">\r\n     <div class=\"col-md-12\"  *ngIf=\"analitycsData.crrData.barChartLabels.length\">\r\n            <div class=\"row-per-card\">\r\n                <div class=\"card \" style=\"max-width: 100%;\">\r\n                    <div class=\"card-header\">\r\n                            Customer Retention Rate (CRR) Graph\r\n                    </div>\r\n                    <div class=\"card-body\">\r\n                        <canvas baseChart [datasets]=\"analitycsData.crrData.barChartData\"\r\n                            [labels]=\"analitycsData.crrData.barChartLabels\" [options]=\"barChartOptions\"\r\n                            [legend]=\"barChartLegend\" [chartType]=\"'line'\" (chartHover)=\"chartHovered($event)\"\r\n                            (chartClick)=\"chartClicked($event)\">\r\n                        </canvas>\r\n                    </div>\r\n                    <div class=\"card-footer\">\r\n                            <button class=\"btn btn-info btn-sm\" (click)=\"onUpdateCart(analitycsData.daily)\">Refresh</button>\r\n                        </div> \r\n                </div>\r\n            </div>     \r\n        </div> \r\n\r\n        \r\n\r\n        <div class=\"col-md-6\">\r\n            <div class=\"row\">\r\n                <div class=\"col-md-12\" style=\"margin-bottom:20px; text-align:center;\">\r\n                    <div class=\"card-sum card\" *ngIf=\"data.values\">\r\n                        <div class=\"card-header\">Customer Retention Rate (CRR) Table</div>\r\n                        <div class=\"card-body\">\r\n                            <table style=\"text-align:left;\">\r\n                            <tr>\r\n                                <th>DATE</th>\r\n                                <th>CRR</th>\r\n                                <th>END TOTAL MEMBER</th>\r\n                                <th>START TOTAL MEMBER</th>\r\n                                <th>TRANSACTIONS</th>\r\n                            </tr>\r\n                            <tr *ngFor=\"let dt of dataTable\">\r\n                                <td>{{dt.date}}</td>\r\n                                <td>{{dt.CRR}}</td>\r\n                                <td>{{dt.end_total_member}}</td>\r\n                                <td>{{dt.start_p_total_member}}</td>\r\n                                <td>{{dt.trx_today}}</td>\r\n                            </tr>\r\n                            </table>\r\n                        </div>\r\n                    </div>\r\n                \r\n                </div> \r\n            </div>\r\n        </div>\r\n       \r\n\r\n\r\n\r\n\r\n    </div>\r\n\r\n\r\n\r\n\r\n</div>"

/***/ }),

/***/ "./src/app/layout/modules/admin-crr/crr-routing.module.ts":
/*!****************************************************************!*\
  !*** ./src/app/layout/modules/admin-crr/crr-routing.module.ts ***!
  \****************************************************************/
/*! exports provided: CrrRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrrRoutingModule", function() { return CrrRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _crr_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./crr.component */ "./src/app/layout/modules/admin-crr/crr.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '', component: _crr_component__WEBPACK_IMPORTED_MODULE_2__["CrrComponent"],
    },
];
var CrrRoutingModule = /** @class */ (function () {
    function CrrRoutingModule() {
    }
    CrrRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], CrrRoutingModule);
    return CrrRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin-crr/crr.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/layout/modules/admin-crr/crr.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".summary-report .dark-header {\n  background: #34495e;\n  font-weight: bold;\n  color: white;\n}\n.summary-report .row-per-card {\n  margin-bottom: 20px;\n}\n.summary-report table {\n  border-collapse: collapse;\n  border: 1px solid #ccc;\n  width: 100%;\n  text-align: center;\n}\n.summary-report table th {\n  background: #333;\n  color: white;\n  font-size: 12px;\n  font-weight: normal;\n}\n.summary-report table tr {\n  background: white;\n}\n.summary-report table tr:nth-child(odd) td {\n  background-color: #f0f0f0;\n}\n.summary-report table td, .summary-report table th {\n  border: 1px solid #ccc;\n  padding: 4px 15px;\n}\n.summary-report table td {\n  font-size: 12px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvYWRtaW4tY3JyL0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbW9kdWxlc1xcYWRtaW4tY3JyXFxjcnIuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2FkbWluLWNyci9jcnIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtBQ0FSO0FETUk7RUFDSSxtQkFBQTtBQ0pSO0FETUk7RUFDSSx5QkFBQTtFQUNBLHNCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0FDSlI7QURLUTtFQUNJLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtBQ0haO0FES1E7RUFDSSxpQkFBQTtBQ0haO0FES1E7RUFDSSx5QkFBQTtBQ0haO0FES1E7RUFDSSxzQkFBQTtFQUNBLGlCQUFBO0FDSFo7QURLUTtFQUNJLGVBQUE7QUNIWiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9tb2R1bGVzL2FkbWluLWNyci9jcnIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc3VtbWFyeS1yZXBvcnR7XHJcbiAgICAuZGFyay1oZWFkZXJ7XHJcbiAgICAgICAgYmFja2dyb3VuZDogIzM0NDk1ZTtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICB9XHJcbiAgICAuY2FyZC5zdW17XHJcbiAgICAgICAgLy8gd2lkdGg6IGNhbGMoMTAwJSAvIDMpO1xyXG5cclxuICAgIH1cclxuICAgIC5yb3ctcGVyLWNhcmR7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxuICAgIH1cclxuICAgIHRhYmxle1xyXG4gICAgICAgIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XHJcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgdGh7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICMzMzM7XHJcbiAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0cntcclxuICAgICAgICAgICAgYmFja2dyb3VuZDp3aGl0ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdHI6bnRoLWNoaWxkKG9kZCkgdGR7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmMGYwZjA7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRkLCB0aHtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICAgICAgICAgICAgcGFkZGluZzogNHB4IDE1cHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRke1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEycHg7XHJcblxyXG4gICAgICAgIH1cclxuXHJcbiAgICB9XHJcbn1cclxuLnJvd3tcclxuICAgIFxyXG59IiwiLnN1bW1hcnktcmVwb3J0IC5kYXJrLWhlYWRlciB7XG4gIGJhY2tncm91bmQ6ICMzNDQ5NWU7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogd2hpdGU7XG59XG4uc3VtbWFyeS1yZXBvcnQgLnJvdy1wZXItY2FyZCB7XG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XG59XG4uc3VtbWFyeS1yZXBvcnQgdGFibGUge1xuICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICB3aWR0aDogMTAwJTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLnN1bW1hcnktcmVwb3J0IHRhYmxlIHRoIHtcbiAgYmFja2dyb3VuZDogIzMzMztcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDEycHg7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG59XG4uc3VtbWFyeS1yZXBvcnQgdGFibGUgdHIge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbn1cbi5zdW1tYXJ5LXJlcG9ydCB0YWJsZSB0cjpudGgtY2hpbGQob2RkKSB0ZCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmMGYwZjA7XG59XG4uc3VtbWFyeS1yZXBvcnQgdGFibGUgdGQsIC5zdW1tYXJ5LXJlcG9ydCB0YWJsZSB0aCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIHBhZGRpbmc6IDRweCAxNXB4O1xufVxuLnN1bW1hcnktcmVwb3J0IHRhYmxlIHRkIHtcbiAgZm9udC1zaXplOiAxMnB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/modules/admin-crr/crr.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/layout/modules/admin-crr/crr.component.ts ***!
  \***********************************************************/
/*! exports provided: CrrComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrrComponent", function() { return CrrComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




// import { Http, Response, Headers, RequestOptions, RequestMethod, RequestOptionsArgs, URLSearchParams, ResponseContentType } from '@angular/http';
// import { Observable } from 'rxjs';
// import { map } from "rxjs/operators";
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/catch';
// import 'rxjs/Rx' ;
var CrrComponent = /** @class */ (function () {
    // private options = new RequestOptions(
    //   { headers: new Headers({ 'Content-Type': 'application/json' }) });
    function CrrComponent(OrderhistoryService, sanitizer) {
        this.OrderhistoryService = OrderhistoryService;
        this.sanitizer = sanitizer;
        this.data = [];
        // getCRR : any;
        this.topBarMenu = [];
        this.barChartOptions = {
            scaleShowVerticalLines: false,
            responsive: true,
        };
        this.barChartLabels = [];
        this.barChartType = 'bar';
        this.barChartLegend = true;
        this.optO = { fill: false, borderWidth: 1, };
        this.analitycsData = {
            crrData: {
                barChartData: [
                    __assign({ data: [], label: 'Crr' }, this.optO),
                ],
            },
        };
        // this.firstLoad();
        // this.srcDownload = this.sanitizer.bypassSecurityTrustResourceUrl("");
    }
    CrrComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var currentDate;
            return __generator(this, function (_a) {
                currentDate = new Date();
                console.log(this.barChartLabels);
                // this.onUpdateCart();
                this.firstLoad();
                return [2 /*return*/];
            });
        });
    };
    CrrComponent.prototype.generateChartData = function (dataValues) {
        var barChartLabels = [];
        var newData = [];
        var allData;
        allData = dataValues;
        var total = 0;
        for (var data in allData) {
            // console.log('allData data', allData[data], data)
            barChartLabels.push(data);
            total += parseInt(allData[data].CRR);
            newData.push(allData[data].CRR);
        }
        allData = null;
        return [barChartLabels, newData, total];
    };
    CrrComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, _i, _a, _b, key, element, c, convertedCrrData, clone, e_1;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _c.trys.push([0, 2, , 3]);
                        this.service = this.OrderhistoryService;
                        return [4 /*yield*/, this.OrderhistoryService.getCRR('daily')];
                    case 1:
                        result = _c.sent();
                        this.data = result.result;
                        this.dataTable = [];
                        for (_i = 0, _a = Object.entries(this.data.values); _i < _a.length; _i++) {
                            _b = _a[_i], key = _b[0], element = _b[1];
                            c = element;
                            c.date = key;
                            this.dataTable.push(c);
                        }
                        ;
                        console.log("CRR", this.dataTable);
                        if (this.data.values) {
                            convertedCrrData = this.generateChartData(this.data.values);
                            clone = JSON.parse(JSON.stringify(this.analitycsData.crrData.barChartData));
                            clone[0].data = convertedCrrData[1];
                            // console.log('convertedDataMonthlyPending', convertedDataMonthlyPending);
                            // clone.push({
                            //   data : convertedDataMonthlyPending[1],
                            //   label: "pending",
                            //   fill:false 
                            // })
                            // console.log("convertedDataMonthly clone", clone);
                            // this.barChartLabels = convertedCrrData[0]
                            this.analitycsData.crrData.barChartLabels = convertedCrrData[0];
                            // this.barChartLabels = convertedDataMonthly[0]
                            // this.analitycsData.monthly.barChartLabels = convertedDataMonthly[0]
                            // this.barChartData   = clone;
                            this.analitycsData.crrData.barChartData = clone;
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _c.sent();
                        console.log("this e result", e_1);
                        this.errorLabel = (e_1.message); //conversion to Error type
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    CrrComponent.ctorParameters = function () { return [
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"] },
        { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"] }
    ]; };
    CrrComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-order-history-summary-crr',
            template: __webpack_require__(/*! raw-loader!./crr.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/admin-crr/crr.component.html"),
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()],
            styles: [__webpack_require__(/*! ./crr.component.scss */ "./src/app/layout/modules/admin-crr/crr.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_2__["OrderhistoryService"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"]])
    ], CrrComponent);
    return CrrComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/admin-crr/crr.module.ts":
/*!********************************************************!*\
  !*** ./src/app/layout/modules/admin-crr/crr.module.ts ***!
  \********************************************************/
/*! exports provided: CrrModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrrModule", function() { return CrrModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _crr_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./crr.component */ "./src/app/layout/modules/admin-crr/crr.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _crr_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./crr-routing.module */ "./src/app/layout/modules/admin-crr/crr-routing.module.ts");
/* harmony import */ var _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng2-charts */ "./node_modules/ng2-charts/index.js");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(ng2_charts__WEBPACK_IMPORTED_MODULE_9__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var CrrModule = /** @class */ (function () {
    function CrrModule() {
    }
    CrrModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _crr_routing_module__WEBPACK_IMPORTED_MODULE_7__["CrrRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_3__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__["FormBuilderTableModule"],
                _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__["BsComponentModule"],
                ng2_charts__WEBPACK_IMPORTED_MODULE_9__["ChartsModule"]
            ],
            declarations: [
                _crr_component__WEBPACK_IMPORTED_MODULE_2__["CrrComponent"]
            ]
        })
    ], CrrModule);
    return CrrModule;
}());



/***/ })

}]);
//# sourceMappingURL=modules-admin-crr-crr-module.js.map