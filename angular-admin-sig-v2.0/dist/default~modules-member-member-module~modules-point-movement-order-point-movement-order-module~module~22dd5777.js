(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~modules-member-member-module~modules-point-movement-order-point-movement-order-module~module~22dd5777"],{

/***/ "./src/app/services/points-transaction/points-transaction.service.ts":
/*!***************************************************************************!*\
  !*** ./src/app/services/points-transaction/points-transaction.service.ts ***!
  \***************************************************************************/
/*! exports provided: PointsTransactionService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PointsTransactionService", function() { return PointsTransactionService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _master_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../master.service */ "./src/app/services/master.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var PointsTransactionService = /** @class */ (function () {
    function PointsTransactionService(http, myService) {
        this.http = http;
        this.myService = myService;
        this.api_url = "";
        this.httpReq = false;
        this.api_url = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["api_url"])();
    }
    PointsTransactionService.prototype.addMonths = function (date, months) {
        date.setMonth(date.getMonth() + months);
        return date;
    };
    PointsTransactionService.prototype.getPointstransaction = function (params) {
        // const uri = 'http://192.168.1.198/f3/v1/orderhistory/all';
        // console.log(uri);
        // return this.http.get<Pointstransaction[]>(uri);
        var myToken = localStorage.getItem('tokenlogin');
        var myStatus = localStorage.getItem('isLoggedin');
        if (myStatus == 'true') {
            try {
                if (params) {
                    var httpParams = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]();
                    params.search = Object.assign({}, params.search);
                    params.order_by = Object.assign({}, params.order_by);
                    var arrayParams = Object.assign({}, params);
                    httpParams = httpParams.append('request', JSON.stringify(arrayParams));
                    var httpOpt = {
                        headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                            'Content-Type': 'application/json',
                            'Authorization': myToken,
                        }),
                        params: httpParams
                    };
                    var uri = this.api_url + 'point-transaction/report';
                    console.log("URI points transaction history", uri);
                    return this.http.get(uri, httpOpt);
                }
            }
            catch (error) {
                throw new TypeError(error);
            }
        }
        else {
            localStorage.removeItem('isLoggedin');
            window.location.reload();
        }
    };
    PointsTransactionService.prototype.getPointReportLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log('param', params);
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'point-transaction/report';
                        return [4 /*yield*/, this.myService.get({ request: JSON.stringify(params) }, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_1 = _a.sent();
                        throw new TypeError(error_1);
                    case 4:
                        console.log(result);
                        return [2 /*return*/, result];
                }
            });
        });
    };
    PointsTransactionService.prototype.searchPointReportLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        console.log('params ', params);
                        if (!params.search.start_date && !params.search.end_date) {
                            params.search.start_date = this.addMonths(new Date(), -6);
                            params.search.end_date = new Date();
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'point-transaction/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_2 = _a.sent();
                        throw new TypeError(error_2);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    //POINT TRANSACTION REPORT
    PointsTransactionService.prototype.getPointstransactionReportRequestedLint = function (param) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'points-transaction/all/report?request={"search":{"order_by":{"created_date":-1}, "based_on":"process_number","status":"' + param + '"}}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_3 = _a.sent();
                        throw new TypeError(error_3);
                    case 4: 
                    // console.log(result);
                    return [2 /*return*/, result];
                }
            });
        });
    };
    PointsTransactionService.prototype.getPointstransactionReportProcessLint = function (param1, param2, param3) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'points-transaction/all/report?request={"search":{"order_by":{"created_date":-1}, "based_on":"process_number","status":{"in":["' + param1 + '","' + param2 + '","' + param3 + '"]}}}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        // const url = 'points-transaction/all/report?request={"search":{"order_by":{"created_date":-1}, "based_on":"process_number","status":{"in":["PROCESSED","CANCELED","CANCELLED"]}}}';
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_4 = _a.sent();
                        throw new TypeError(error_4);
                    case 4: 
                    // console.log(result);
                    return [2 /*return*/, result];
                }
            });
        });
    };
    // REPORT APPROVAL
    PointsTransactionService.prototype.getPointstransactionApprovalProcessLint = function (param) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'points-transaction/all/report?request={"search":{"status":"' + param + '"},"current_page":1,"limit_per_page":50}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_5 = _a.sent();
                        throw new TypeError(error_5);
                    case 4: 
                    // console.log(result);
                    return [2 /*return*/, result];
                }
            });
        });
    };
    // SEARCH POINT TRANSACTION REPORT
    PointsTransactionService.prototype.searchPointstransactionProcessReport = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = "points-transaction/all/report?request={\"search\":{\"process_number\":\"" + params + "\"},\"current_page\":1}";
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        // const url = 'points-transaction/all/report'
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_6 = _a.sent();
                        throw new TypeError(error_6);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    //POINT APPROVAL REPORT based on per batch
    PointsTransactionService.prototype.searchPointstransactionApprovalProcessLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_7;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params.search.status = "REQUESTED";
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'points-transaction/all/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_7 = _a.sent();
                        throw new TypeError(error_7);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    PointsTransactionService.prototype.searchPointstransactionApprovalProcessLint2 = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_8;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params.search.status = "PROCESSED";
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'points-transaction/all/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_8 = _a.sent();
                        throw new TypeError(error_8);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    //POINT TRANSACTION REPORT based on per batch
    PointsTransactionService.prototype.searchPointstransactionReportProcessLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_9;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.warn("params", params);
                        params.search.status = "REQUESTED";
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'points-transaction/all/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_9 = _a.sent();
                        throw new TypeError(error_9);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    PointsTransactionService.prototype.searchPointstransactionReportProcessLint2 = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_10;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params.search.status = "PROCESSED";
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'points-transaction/all/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_10 = _a.sent();
                        throw new TypeError(error_10);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    //POINT TRANSACTION REPORT based on member
    PointsTransactionService.prototype.getPointstransactionReportLint = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_11;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'points-transaction/all/report?request={"search":{"based_on":"member"},"current_page":1,"limit_per_page":50}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_11 = _a.sent();
                        throw new TypeError(error_11);
                    case 4: 
                    // console.log(result);
                    return [2 /*return*/, result];
                }
            });
        });
    };
    PointsTransactionService.prototype.searchPointstransactionreportLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_12;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        params.search.based_on = "member";
                        console.log("params", params);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'points-transaction/all/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_12 = _a.sent();
                        throw new TypeError(error_12);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    PointsTransactionService.prototype.uploadPointsCodeBulk = function (file, obj, payload, type) {
        return __awaiter(this, void 0, void 0, function () {
            var myStatus, myToken, fd, url, result;
            var _this = this;
            return __generator(this, function (_a) {
                myStatus = localStorage.getItem('isLoggedin');
                if (myStatus) {
                    myToken = localStorage.getItem('tokenlogin');
                }
                fd = new FormData();
                //fd.append('image', file, file.name)
                fd.append('type', payload.type);
                fd.append('document', file);
                url = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["api_url"])() + 'points-transaction/new?process_type=' + type;
                this.httpReq = this.http.post(url, fd, {
                    headers: Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])(),
                    // headers : new HttpHeaders({
                    //   'Authorization': myToken,
                    //   'app-label': 'retailer_prg'
                    // }),
                    reportProgress: true,
                    observe: 'events'
                }).subscribe(function (event) { return __awaiter(_this, void 0, void 0, function () {
                    var c, prgval;
                    return __generator(this, function (_a) {
                        c = JSON.parse(JSON.stringify(event));
                        if (c.body) {
                            obj.Report = c.body._id;
                            // obj.prodOnUpload = true;
                            if (c.status == 200 && c.body && c.body.result && c.body.result.process_number) {
                                obj.selectedFile = null;
                                obj.startUploading = false;
                                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Your file has been uploaded!', 'Process Number: ' + c.body.result.process_number, 'success');
                            }
                            // obj.prodOnUpload = true;
                        }
                        if (event.type == _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpEventType"].UploadProgress) {
                            prgval = Math.round(event.loaded / event.total * 100);
                            obj.updateProgressBar(prgval);
                            //console.log(obj.cancel);
                            if (prgval == 100) {
                                console.log(event);
                            }
                            if (obj.cancel == true) {
                                this.httpReq.unsubscribe();
                            }
                            // if(prgval == 100){
                            //   if(obj.onUploadSuccess) 
                            //     obj.onUploadSuccess();
                            // }
                            console.log('upload Progress: ' + prgval + "%");
                        }
                        return [2 /*return*/];
                    });
                }); }, function (result) {
                    // console.warn("RESULT", result);
                    if (result.error) {
                        obj.errorFile = result.error.error;
                        obj.cancel = true;
                        obj.selectedFile = null;
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Failed!', result.error.error, 'warning');
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    PointsTransactionService.prototype.processPoint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_13;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        ;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = "points-transaction/process";
                        return [4 /*yield*/, this.myService.post(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_13 = _a.sent();
                        throw new TypeError(error_13);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    PointsTransactionService.prototype.getPointstransactionLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_14;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'point-transaction/report';
                        return [4 /*yield*/, this.myService.get({ request: JSON.stringify(params) }, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_14 = _a.sent();
                        throw new TypeError(error_14);
                    case 4:
                        console.log(result);
                        return [2 /*return*/, result];
                }
            });
        });
    };
    PointsTransactionService.prototype.searchPointstransactionLint = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_15;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        console.log('params ', params);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'point-transaction/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_15 = _a.sent();
                        throw new TypeError(error_15);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    PointsTransactionService.prototype.detailPointstransaction = function (pointstransaction_id) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_16;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'point-transaction/' + pointstransaction_id;
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        // const url = 'point-transaction/report?request={"search": {"_id":"'+pointstransaction_id+'"}}';
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_16 = _a.sent();
                        throw new TypeError(error_16);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    PointsTransactionService.prototype.updatePointstransaction = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_17;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        console.log(params);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'point-transaction/update/' + params._id;
                        return [4 /*yield*/, this.myService.update(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(url, result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_17 = _a.sent();
                        throw new TypeError(error_17);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    // POINT MOVEMENT REPORT BASED ON PRODUCT
    PointsTransactionService.prototype.getPointMovementBasedOnProduct = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_18;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report/sales-order?request={"limit_per_page":50,"current_page":1,"search":{"last_shipping_info":{"in":["on_delivery","delivered","redelivery"]}}}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_18 = _a.sent();
                        throw new TypeError(error_18);
                    case 4: 
                    // console.log(result);
                    return [2 /*return*/, result];
                }
            });
        });
    };
    PointsTransactionService.prototype.searchPointMovementBasedOnProduct = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_19;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        // params.search.status = {
                        //   in:["PROCESSED","COMPLETED"]
                        // };
                        params.search.last_shipping_info = {
                            in: ["on_delivery", "delivered", "redelivery"]
                        };
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeaderRetailer"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'order-invoice/report/sales-order';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_19 = _a.sent();
                        throw new TypeError(error_19);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    // POINT MOVEMENT REPORT BASED ON ORDER
    PointsTransactionService.prototype.getPointMovementBasedOnOrder = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_20;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'point-transaction/report?request={"limit_per_page":50,"current_page":1,"search":{"based_on":"invoice"}}';
                        return [4 /*yield*/, this.myService.get(null, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        return [3 /*break*/, 4];
                    case 3:
                        error_20 = _a.sent();
                        throw new TypeError(error_20);
                    case 4: 
                    // console.log(result);
                    return [2 /*return*/, result];
                }
            });
        });
    };
    PointsTransactionService.prototype.searchPointMovementBasedOnOrder = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var result, customHeaders, url, error_21;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params.search.based_on = "invoice";
                        customHeaders = Object(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["getTokenHeader"])();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        url = 'point-transaction/report';
                        return [4 /*yield*/, this.myService.searchResult(params, url, customHeaders)];
                    case 2:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_21 = _a.sent();
                        throw new TypeError(error_21);
                    case 4: return [2 /*return*/, result];
                }
            });
        });
    };
    PointsTransactionService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] },
        { type: _master_service__WEBPACK_IMPORTED_MODULE_3__["MasterService"] }
    ]; };
    PointsTransactionService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root',
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _master_service__WEBPACK_IMPORTED_MODULE_3__["MasterService"]])
    ], PointsTransactionService);
    return PointsTransactionService;
}());



/***/ })

}]);
//# sourceMappingURL=default~modules-member-member-module~modules-point-movement-order-point-movement-order-module~module~22dd5777.js.map