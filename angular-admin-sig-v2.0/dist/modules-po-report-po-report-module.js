(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-po-report-po-report-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/modules/po-report/po-report.component.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/modules/po-report/po-report.component.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div >\r\n    <app-page-header [heading]=\"'&nbsp;'\" [icon]=\"'fa-table'\">PO Number Report</app-page-header>\r\n    <div *ngIf=\"Orderhistoryallhistory && orderhistoryallhistoryDetail==false && mci_project == false && !errorMessage\">\r\n        <app-form-builder-table \r\n        [table_data]=\"Orderhistoryallhistory\"\r\n        [searchCallback]=\"[service, 'searchReportBasedOnPO',this]\" \r\n        [tableFormat]=\"tableFormat\"\r\n        [total_page]=\"totalPage\"\r\n        >\r\n        </app-form-builder-table>\r\n    </div>\r\n\r\n    <div *ngIf=\"Orderhistoryallhistory && orderhistoryallhistoryDetail==false && mci_project == true && !errorMessage\">\r\n        <app-form-builder-table \r\n        [table_data]=\"Orderhistoryallhistory\"\r\n        [searchCallback]=\"[service, 'searchReportBasedOnPO',this]\" \r\n        [tableFormat]=\"tableFormat2\"\r\n        [total_page]=\"totalPage\"\r\n        >\r\n        </app-form-builder-table>\r\n    </div>\r\n\r\n    <div *ngIf=\"errorMessage\" class=\"error-message\">\r\n          <div class=\"text-message\">\r\n          <i class=\"fas fa-ban\"></i>\r\n          {{errorMessage}}\r\n          </div>\r\n    </div>\r\n</div>\r\n  "

/***/ }),

/***/ "./src/app/layout/modules/po-report/po-report-routing.module.ts":
/*!**********************************************************************!*\
  !*** ./src/app/layout/modules/po-report/po-report-routing.module.ts ***!
  \**********************************************************************/
/*! exports provided: POReportRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "POReportRoutingModule", function() { return POReportRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _po_report_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./po-report.component */ "./src/app/layout/modules/po-report/po-report.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '', component: _po_report_component__WEBPACK_IMPORTED_MODULE_2__["POReportComponent"]
    },
];
var POReportRoutingModule = /** @class */ (function () {
    function POReportRoutingModule() {
    }
    POReportRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], POReportRoutingModule);
    return POReportRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/modules/po-report/po-report.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/layout/modules/po-report/po-report.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".error-message {\n  background: white;\n  height: 100vh;\n  text-align: center;\n  font-size: 26px;\n  padding-top: 10px;\n}\n.error-message .text-message {\n  background: #E74C3C;\n  padding: 20px;\n  margin: 0 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L21vZHVsZXMvcG8tcmVwb3J0L0Q6XFxhaXN5YWhcXGNsc1xcYW5ndWxhci1hZG1pbi1zaWctdi4yLjAvc3JjXFxhcHBcXGxheW91dFxcbW9kdWxlc1xccG8tcmVwb3J0XFxwby1yZXBvcnQuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xheW91dC9tb2R1bGVzL3BvLXJlcG9ydC9wby1yZXBvcnQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQ0NKO0FEQ0k7RUFDSSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxjQUFBO0FDQ1IiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbW9kdWxlcy9wby1yZXBvcnQvcG8tcmVwb3J0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmVycm9yLW1lc3NhZ2Uge1xyXG4gICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICBoZWlnaHQ6MTAwdmg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6MjZweDtcclxuICAgIHBhZGRpbmctdG9wOiAxMHB4O1xyXG5cclxuICAgIC50ZXh0LW1lc3NhZ2Uge1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICNFNzRDM0M7XHJcbiAgICAgICAgcGFkZGluZzogMjBweDtcclxuICAgICAgICBtYXJnaW46IDAgMTBweDtcclxuICAgIH1cclxufSIsIi5lcnJvci1tZXNzYWdlIHtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGhlaWdodDogMTAwdmg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAyNnB4O1xuICBwYWRkaW5nLXRvcDogMTBweDtcbn1cbi5lcnJvci1tZXNzYWdlIC50ZXh0LW1lc3NhZ2Uge1xuICBiYWNrZ3JvdW5kOiAjRTc0QzNDO1xuICBwYWRkaW5nOiAyMHB4O1xuICBtYXJnaW46IDAgMTBweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/modules/po-report/po-report.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/layout/modules/po-report/po-report.component.ts ***!
  \*****************************************************************/
/*! exports provided: POReportComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "POReportComponent", function() { return POReportComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/orderhistory/orderhistory.service */ "./src/app/services/orderhistory/orderhistory.service.ts");
/* harmony import */ var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../assets/json/content.json */ "./src/assets/json/content.json");
var _assets_json_content_json__WEBPACK_IMPORTED_MODULE_2___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../../../assets/json/content.json */ "./src/assets/json/content.json", 1);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var POReportComponent = /** @class */ (function () {
    function POReportComponent(orderhistoryService) {
        this.orderhistoryService = orderhistoryService;
        this.PointMovementOrder = [];
        this.tableFormat = {
            title: 'PO Number Report',
            label_headers: [
                { label: 'Request Date', visible: true, type: 'date', data_row_name: 'request_date' },
                { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
                { label: 'PO Number', visible: true, type: 'value_po', data_row_name: 'po_no' },
                { label: 'ID Toko', visible: true, type: 'string', data_row_name: 'id_toko' },
                { label: 'Nama Toko', visible: true, type: 'string', data_row_name: 'nama_pemilik' },
                { label: 'Nama Pemilik', visible: true, type: 'form_nama_pemilik', data_row_name: 'member_detail' },
                { label: 'Total Quantity', visible: true, type: 'string', data_row_name: 'total_quantity' },
                { label: 'Total Price', visible: true, type: 'string', data_row_name: 'sum_total' },
                { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
            ],
            row_primary_key: 'order_id',
            formOptions: {
                row_id: 'order_id',
                this: this,
                result_var_name: 'Orderhistoryallhistory',
                detail_function: [this, 'callDetail'],
            },
            show_checkbox_options: true
        };
        this.tableFormat2 = {
            title: 'PO Number Report',
            label_headers: [
                { label: 'Request Date', visible: true, type: 'date', data_row_name: 'request_date' },
                { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
                { label: 'PO Number', visible: true, type: 'value_po', data_row_name: 'po_no' },
                { label: 'ID Pelanggan', visible: true, type: 'string', data_row_name: 'id_toko' },
                { label: 'Nama Pelanggan', visible: true, type: 'string', data_row_name: 'nama_pemilik' },
                { label: 'Total Quantity', visible: true, type: 'string', data_row_name: 'total_quantity' },
                { label: 'Total Price', visible: true, type: 'string', data_row_name: 'sum_total' },
                { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
            ],
            row_primary_key: 'order_id',
            formOptions: {
                row_id: 'order_id',
                this: this,
                result_var_name: 'Orderhistoryallhistory',
                detail_function: [this, 'callDetail'],
            },
            show_checkbox_options: true
        };
        this.form_input = {};
        this.errorLabel = false;
        this.totalPage = 0;
        // SalesRedemptionDetail: any = false;
        this.orderhistoryallhistoryDetail = false;
        this.errorMessage = false;
        this.Orderhistoryallhistory = [];
        this.mci_project = false;
        this.contentList = _assets_json_content_json__WEBPACK_IMPORTED_MODULE_2__;
    }
    POReportComponent.prototype.ngOnInit = function () {
        this.firstLoad();
    };
    POReportComponent.prototype.firstLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var program_1, _this_1, result, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(' stock movement');
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        program_1 = localStorage.getItem('programName');
                        _this_1 = this;
                        this.contentList.filter(function (element) {
                            if (element.appLabel == program_1) {
                                if (element.type == "reguler") {
                                    _this_1.mci_project = true;
                                }
                                else {
                                    _this_1.mci_project = false;
                                }
                            }
                        });
                        this.service = this.orderhistoryService;
                        return [4 /*yield*/, this.orderhistoryService.getReportBasedOnPO()];
                    case 2:
                        result = _a.sent();
                        this.totalPage = result.total_page;
                        this.Orderhistoryallhistory = result.values;
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        this.errorLabel = (e_1.message); //conversion to Error type
                        if (this.errorLabel.includes("Unauthorized") || this.errorLabel.includes("unauthorized") || this.errorLabel.includes("access not permitted")) {
                            this.errorMessage = "Anda tidak memiliki hak untuk mengakses halaman ini.";
                        }
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    POReportComponent.prototype.callDetail = function (SalesRedemption_id) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                try {
                    // let result: any;
                    // this.service    = this.PointsTransactionService;
                    // result          = await this.PointsTransactionService.detailPointstransaction(SalesRedemption_id);
                    // console.log(result);
                    // this.SalesRedemptionDetail = result.result[0];
                }
                catch (e) {
                    this.errorLabel = (e.message); //conversion to Error type
                }
                return [2 /*return*/];
            });
        });
    };
    POReportComponent.prototype.backToHere = function (obj) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                obj.SalesRedemptionDetail = false;
                return [2 /*return*/];
            });
        });
    };
    POReportComponent.prototype.addMonths = function (date, months) {
        date.setMonth(date.getMonth() + months);
        return date;
    };
    POReportComponent.ctorParameters = function () { return [
        { type: _services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_1__["OrderhistoryService"] }
    ]; };
    POReportComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-po-report',
            template: __webpack_require__(/*! raw-loader!./po-report.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/modules/po-report/po-report.component.html"),
            styles: [__webpack_require__(/*! ./po-report.component.scss */ "./src/app/layout/modules/po-report/po-report.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_orderhistory_orderhistory_service__WEBPACK_IMPORTED_MODULE_1__["OrderhistoryService"]])
    ], POReportComponent);
    return POReportComponent;
}());



/***/ }),

/***/ "./src/app/layout/modules/po-report/po-report.module.ts":
/*!**************************************************************!*\
  !*** ./src/app/layout/modules/po-report/po-report.module.ts ***!
  \**************************************************************/
/*! exports provided: POReportModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "POReportModule", function() { return POReportModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _po_report_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./po-report-routing.module */ "./src/app/layout/modules/po-report/po-report-routing.module.ts");
/* harmony import */ var _po_report_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./po-report.component */ "./src/app/layout/modules/po-report/po-report.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../component-libs/form-builder-table/form-builder-table.module */ "./src/app/component-libs/form-builder-table/form-builder-table.module.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../bs-component/bs-component.module */ "./src/app/layout/modules/bs-component/bs-component.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var POReportModule = /** @class */ (function () {
    function POReportModule() {
    }
    POReportModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _po_report_routing_module__WEBPACK_IMPORTED_MODULE_2__["POReportRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_4__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["NgbModule"],
                _component_libs_form_builder_table_form_builder_table_module__WEBPACK_IMPORTED_MODULE_6__["FormBuilderTableModule"],
                _bs_component_bs_component_module__WEBPACK_IMPORTED_MODULE_8__["BsComponentModule"]
            ],
            declarations: [_po_report_component__WEBPACK_IMPORTED_MODULE_3__["POReportComponent"]],
        })
    ], POReportModule);
    return POReportModule;
}());



/***/ })

}]);
//# sourceMappingURL=modules-po-report-po-report-module.js.map