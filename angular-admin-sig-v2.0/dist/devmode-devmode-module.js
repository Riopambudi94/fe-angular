(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["devmode-devmode-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/devmode/devmode.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/devmode/devmode.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/devmode/devmode-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/devmode/devmode-routing.module.ts ***!
  \***************************************************/
/*! exports provided: DevmodeRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DevmodeRoutingModule", function() { return DevmodeRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _devmode_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./devmode.component */ "./src/app/devmode/devmode.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    { path: '', component: _devmode_component__WEBPACK_IMPORTED_MODULE_2__["DevmodeComponent"] }
];
var DevmodeRoutingModule = /** @class */ (function () {
    function DevmodeRoutingModule() {
    }
    DevmodeRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], DevmodeRoutingModule);
    return DevmodeRoutingModule;
}());



/***/ }),

/***/ "./src/app/devmode/devmode.component.scss":
/*!************************************************!*\
  !*** ./src/app/devmode/devmode.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Rldm1vZGUvZGV2bW9kZS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/devmode/devmode.component.ts":
/*!**********************************************!*\
  !*** ./src/app/devmode/devmode.component.ts ***!
  \**********************************************/
/*! exports provided: DevmodeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DevmodeComponent", function() { return DevmodeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DevmodeComponent = /** @class */ (function () {
    function DevmodeComponent(router) {
        this.router = router;
    }
    DevmodeComponent.prototype.ngOnInit = function () {
        try {
            localStorage.setItem('devmode', 'active');
            this.router.navigateByUrl('/login');
        }
        catch (e) {
        }
    };
    DevmodeComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] }
    ]; };
    DevmodeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-devmode',
            template: __webpack_require__(/*! raw-loader!./devmode.component.html */ "./node_modules/raw-loader/index.js!./src/app/devmode/devmode.component.html"),
            styles: [__webpack_require__(/*! ./devmode.component.scss */ "./src/app/devmode/devmode.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], DevmodeComponent);
    return DevmodeComponent;
}());



/***/ }),

/***/ "./src/app/devmode/devmode.module.ts":
/*!*******************************************!*\
  !*** ./src/app/devmode/devmode.module.ts ***!
  \*******************************************/
/*! exports provided: DevmodeModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DevmodeModule", function() { return DevmodeModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _devmode_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./devmode-routing.module */ "./src/app/devmode/devmode-routing.module.ts");
/* harmony import */ var _devmode_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./devmode.component */ "./src/app/devmode/devmode.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var DevmodeModule = /** @class */ (function () {
    function DevmodeModule() {
    }
    DevmodeModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_devmode_component__WEBPACK_IMPORTED_MODULE_3__["DevmodeComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _devmode_routing_module__WEBPACK_IMPORTED_MODULE_2__["DevmodeRoutingModule"]
            ]
        })
    ], DevmodeModule);
    return DevmodeModule;
}());



/***/ })

}]);
//# sourceMappingURL=devmode-devmode-module.js.map