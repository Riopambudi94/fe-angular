(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~merchant-portal-merchant-portal-module~modules-banner-banner-module~modules-evouchersalesrep~4891dc5f"],{

/***/ "./src/app/object-interface/common.object.ts":
/*!***************************************************!*\
  !*** ./src/app/object-interface/common.object.ts ***!
  \***************************************************/
/*! exports provided: productsTableFormat, evoucherListDetail, evoucherListDetailAdmin, eVouchersTableFormat, eVouchersSalesReportTableFormat, orderHistoriesTableFormat, salesOrderTableFormat, merchantSalesOrderTableFormat, isString, isArray, isNumber, isInt */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "productsTableFormat", function() { return productsTableFormat; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "evoucherListDetail", function() { return evoucherListDetail; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "evoucherListDetailAdmin", function() { return evoucherListDetailAdmin; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "eVouchersTableFormat", function() { return eVouchersTableFormat; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "eVouchersSalesReportTableFormat", function() { return eVouchersSalesReportTableFormat; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "orderHistoriesTableFormat", function() { return orderHistoriesTableFormat; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "salesOrderTableFormat", function() { return salesOrderTableFormat; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "merchantSalesOrderTableFormat", function() { return merchantSalesOrderTableFormat; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isString", function() { return isString; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isArray", function() { return isArray; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isNumber", function() { return isNumber; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isInt", function() { return isInt; });
var productsTableFormat = function (_this) {
    var t = {
        title: 'Products Data',
        label_headers: [
            { label: 'Created Date', visible: true, type: 'date', data_row_name: 'created_date' },
            { label: 'Merchant Name', visible: true, type: 'string', data_row_name: 'merchant_username' },
            { label: 'Product Name', visible: true, type: 'string', data_row_name: 'product_name' },
            { label: 'Product Code', visible: true, type: 'string', data_row_name: 'product_code' },
            { label: 'Category', visible: true, type: 'string', data_row_name: 'category' },
            { label: 'Price', visible: true, type: 'string', data_row_name: 'price' },
            { label: 'Fixed Price', visible: true, type: 'string', data_row_name: 'fixed_price' },
            { label: 'Discount', visible: true, type: 'string', data_row_name: 'discount_value' },
            { label: 'Quantity', visible: true, type: 'string', data_row_name: 'qty' },
            { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
            { label: 'Approved', visible: true, type: 'string', data_row_name: 'approved' },
        ],
        row_primary_key: '_id',
        formOptions: {
            row_id: '_id',
            addForm: true,
            this: _this,
            result_var_name: 'Products',
            detail_function: [_this, 'callDetail'],
            customButtons: [
                { label: 'Approve', func: function (f) { _this.approval(f); } },
            ]
        },
        show_checkbox_options: true
    };
    return t;
};
var evoucherListDetail = function (_this) {
    var t = {
        title: 'Evoucher List Detail',
        label_headers: [
            // {label: 'Voucher Code', visible: true, type: 'string', data_row_name: 'ev_voucher_code'},
            { label: 'Expiry Date', visible: true, type: 'date', data_row_name: 'expiry_date' },
            { label: 'Merchant Username', visible: true, type: 'string', data_row_name: 'merchant_username' },
            { label: 'approved', visible: true, type: 'string', data_row_name: 'active' },
        ],
        row_primary_key: '_id',
        formOptions: {
            row_id: '_id',
            addForm: true,
            this: _this,
            result_var_name: 'evoucherDetail',
            detail_function: [_this, 'callDetail'],
            customButtons: [
                { label: 'Activate', func: function (f) { _this.evoucherStatus(f); } },
                { label: 'Deactivate', func: function (f) { _this.evoucherStatus(f); } },
            ]
        },
        show_checkbox_options: true
    };
    return t;
};
var evoucherListDetailAdmin = function (_this) {
    var t = {
        title: 'Evoucher List Detail',
        label_headers: [
            // {label: 'Voucher Code', visible: true, type: 'string', data_row_name: 'ev_voucher_code'},
            { label: 'Expiry Date', visible: true, type: 'date', data_row_name: 'expiry_date' },
            { label: 'Merchant Username', visible: true, type: 'string', data_row_name: 'merchant_username' },
            { label: 'approved', visible: true, type: 'string', data_row_name: 'active' },
        ],
        row_primary_key: '_id',
        formOptions: {
            row_id: '_id',
            addForm: true,
            this: _this,
            result_var_name: 'evoucherDetail',
            detail_function: [_this, 'callDetail'],
            customButtons: [
                { label: 'Approve', func: function (f) { _this.approval(f); } },
            ]
        },
        show_checkbox_options: true
    };
    return t;
};
var eVouchersTableFormat = function (_this) {
    var t = {
        title: 'E-Vouchers Data',
        label_headers: [
            // { label: 'Active Total', visible: true, type: 'string', data_row_name: 'active_total' },
            // // { label: 'E-Voucher Code', visible: true, type: 'string', data_row_name: 'ev_product_code' },
            // { label: 'Expired Date', visible: true, type: 'string', data_row_name: 'expiry_date' },
            // { label: 'Quantity In Total', visible: true, type: 'string', data_row_name: 'qty_in_total' },
            // { label: 'Quantity Out Total', visible: true, type: 'string', data_row_name: 'qty_out_total' },
            // { label: 'Total', visible: true, type: 'string', data_row_name: 'total' },
            { label: 'Expiry Date', visible: true, type: 'date', data_row_name: 'expiry_date' },
            { label: 'Available Stock', visible: true, type: 'string', data_row_name: 'qty_available' },
            { label: 'Bought by User', visible: true, type: 'string', data_row_name: 'qty_sold' },
            { label: 'Used by User', visible: true, type: 'string', data_row_name: 'qty_used' },
            { label: 'Inactive Stock', visible: true, type: 'string', data_row_name: 'qty_inactive' },
            { label: 'Created Date', visible: false, type: 'string', data_row_name: 'qty_inactive' },
        ],
        row_primary_key: '_id',
        formOptions: {
            row_id: '_id',
            addForm: true,
            this: _this,
            result_var_name: 'generatedVouchers',
            detail_function: [_this, 'callDetail'],
            customButtons: [
                { label: 'Approval', func: function (f) { _this.approval(f); } },
            ]
        },
        show_checkbox_options: true
    };
    return t;
};
var eVouchersSalesReportTableFormat = function (_this) {
    var t = {
        title: 'E-Vouchers Sales Report',
        label_headers: [
            { label: 'Product Code', visible: true, type: 'string', data_row_name: 'ev_product_code' },
            { label: 'Product Name', visible: true, type: 'string', data_row_name: 'product_name' },
            { label: 'Merchant Username', visible: false, type: 'string', data_row_name: 'merchant_username' },
            { label: 'Stock', visible: true, type: 'string', data_row_name: 'stock' },
            { label: 'Bought', visible: true, type: 'string', data_row_name: 'bought' },
            { label: 'Used', visible: true, type: 'string', data_row_name: 'redeem' },
        ],
        row_primary_key: '_id',
        formOptions: {
            row_id: '_id',
            addForm: true,
            this: _this,
            result_var_name: 'evouchers',
            detail_function: [_this, 'callDetail'],
            customButtons: [
                { label: 'Approve', func: function (f) { _this.approval(f); } },
            ]
        },
        show_checkbox_options: true
    };
    return t;
};
function orderHistoriesTableFormat(_this) {
    var t = {
        title: 'Order All History Detail Page',
        label_headers: [
            { label: 'Order date', visible: true, type: 'date', data_row_name: 'created_date' },
            { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
            { label: 'User Name', visible: true, type: 'string', data_row_name: 'user_name' },
            { label: 'User Email', visible: true, type: 'string', data_row_name: 'email' },
            { label: 'Cell Phone', visible: true, type: 'string', data_row_name: 'cell_phone' },
            // { label: 'Product Code', visible: false, type: 'string', data_row_name: 'product_code' },
            // { label: 'Product Name', visible: false, type: 'string', data_row_name: 'product' },
            // { label: 'Quantity', visible: false, type: 'string', data_row_name: 'qty' },
            // { label: 'Price', visible: false, type: 'string', data_row_name: 'price' },
            // { label: 'Discounts LO Points', visible: false, type: 'string', data_row_name: 'discounts' },
            // { label: 'Delcost', visible: false, type: 'string', data_row_name: 'delcost' },
            // { label: 'Unique Code', visible: false, type: 'string', data_row_name: 'unique_amount' },
            { label: 'Total Price', visible: true, type: 'string', data_row_name: 'total_price' },
            { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
            // { label: 'User Id', visible: false, type: 'string', data_row_name: 'user_id' },
            // { label: 'Payment Method', visible: false, type: 'string', data_row_name: 'payment_method' },
            // { label: 'Courier', visible: false, type: 'string', data_row_name: 'shipping_info' },
            // { label: 'Bank Name', visible: false, type: 'string', data_row_name: 'bank_name' },
            { label: 'Unique Amount', visible: true, type: 'string', data_row_name: 'unique_amount' },
        ],
        row_primary_key: '_id',
        formOptions: {
            row_id: '_id',
            this: _this,
            result_var_name: 'Orderhistoryallhistory',
            detail_function: [_this, 'callDetail'],
        },
        show_checkbox_options: true
    };
    return t;
}
function salesOrderTableFormat(_this) {
    var t = {
        title: 'Order All History Detail Page',
        label_headers: [
            { label: 'Product Name', visible: true, type: 'string', data_row_name: 'product_name' },
            { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
            { label: 'Quantity', visible: true, type: 'string', data_row_name: 'qty' },
            { label: '@Price', visible: true, type: 'string', data_row_name: 'price' },
            { label: 'Discount Price', visible: true, type: 'string', data_row_name: 'discount_price' },
            { label: 'Total Price', visible: true, type: 'string', data_row_name: 'total_price' },
            { label: 'Status', visible: true, type: 'string', data_row_name: 'status' },
            { label: 'User Email', visible: true, type: 'string', data_row_name: 'user_email' },
            { label: 'Order date', visible: true, type: 'date', data_row_name: 'order_date' },
            { label: 'User Name', visible: true, type: 'string', data_row_name: 'username' },
            { label: 'Payment Method', visible: true, type: 'string', data_row_name: 'payment_method' },
            { label: 'Bank Name', visible: true, type: 'string', data_row_name: 'bank' }
        ],
        row_primary_key: '_id',
        formOptions: {
            row_id: '_id',
            this: _this,
            result_var_name: 'Orderhistoryallhistory',
            detail_function: [_this, 'callDetail'],
        },
        show_checkbox_options: true
    };
    return t;
}
function merchantSalesOrderTableFormat(_this) {
    var t = {
        title: 'Merchant Sales Order',
        label_headers: [
            { label: 'Order ID', visible: true, type: 'string', data_row_name: 'order_id' },
            { label: 'Product Name', visible: true, type: 'string', data_row_name: 'product_name' },
            { label: 'Quantity', visible: true, type: 'string', data_row_name: 'quantity' },
            { label: 'Price', visible: true, type: 'string', data_row_name: 'price' },
            { label: 'Total Price', visible: true, type: 'string', data_row_name: 'total_price' },
            { label: 'Bank', visible: true, type: 'string', data_row_name: 'bank' }
        ],
        row_primary_key: '_id',
        formOptions: {
            row_id: '_id',
            this: _this,
            result_var_name: 'allData',
            detail_function: [_this, 'callDetail']
        },
        show_checkbox_options: true
    };
    return t;
}
function isString(variable) {
    return typeof (variable) == 'string';
}
function isArray(variable) {
    return variable instanceof Array;
}
function isNumber(variable) {
    return typeof (variable) == 'number';
}
function isInt(variable) {
    return typeof (variable) == 'number' && Number.isInteger(variable);
}


/***/ })

}]);
//# sourceMappingURL=default~merchant-portal-merchant-portal-module~modules-banner-banner-module~modules-evouchersalesrep~4891dc5f.js.map